<?php

namespace GM\Modules\Processing;

class StateTable
{
    const NEW_STATE = "new";
    const RESERVED_STATE = "reserved";
    const PRINTED_STATE = "printed";
    const EXPENDED_STATE = "expended";
}