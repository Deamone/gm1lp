<?php

namespace GM\Modules\Processing;

class EventTable
{
    const RESERVE_EVENT = "reserve";
    const PRINT_EVENT = "print";
    const EXPEND_EVENT = "expend";
}