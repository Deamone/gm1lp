<?php

namespace GM\Modules\Processing;

class ErrorCodes
{
    const DATABASE_CONNECT_ERROR = 1;
    const DATABASE_QUERY_ERROR = 2;
    const CODE_DOES_NOT_EXIST = 3;
    const ACTION_DOES_NOT_EXIST = 4;
    const CASHBACK_NOT_PROVIDED = 5;
    const ACTION_EXPIRED = 6;
    const INVALID_FEE = 7;
    const INVALID_NAME = 8;
    const INVALID_PHONE = 9;
    const USER_DOES_NOT_EXIST = 10;
    const INVALID_AMOUNT = 11;
    const FORBIDDEN_TRANSITION = 12;
    const WRONG_EVENT = 13;
    const INSUFFICIENT_FUNDS = 14;
}