<?php

namespace GM\Modules\Processing;

class Action
{
    /**
     * @var int
     */
    private $fee = 0;
    /**
     * @var int
     */
    private $user_id = 0;
    /**
     * @var User
     */
    private $user = null;

    /**
     * @param \mysqli $db
     * @param int $id
     * @param bool $expireDateCheck
     * @throws ProcessingError
     */
    public function __construct($db, $id, $expireDateCheck = true)
    {
        $id = (int)$id;
        $query = $db->query("SELECT `cost`, `user_id`, `end` FROM `nx_actions` WHERE `action_id` = $id");
        if (mysqli_num_rows($query) == 0) {
            throw new ProcessingError(ErrorCodes::ACTION_DOES_NOT_EXIST);
        }
        $action = $query->fetch_assoc();
        $cost = @unserialize(@$action['cost']);
        if (@$cost['type'] != 'cashback') {
            throw new ProcessingError(ErrorCodes::CASHBACK_NOT_PROVIDED);
        }
        if ($expireDateCheck && @$action['end'] < time()) {
            throw new ProcessingError(ErrorCodes::ACTION_EXPIRED);
        }
        if (@$cost['fee'] <= 0) {
            throw new ProcessingError(ErrorCodes::INVALID_FEE);
        }
        $this->fee = $cost['fee'];
        $this->user_id = $action['user_id'];
    }

    /**
     * @param \mysqli $db
     * @return Action $this
     */
    public function fetchUser($db)
    {
        $this->user = new User($db, $this->user_id);
        return $this;
    }

    /**
     * @return int
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @return User
     */
    public function &getUser()
    {
        return $this->user;
    }
}