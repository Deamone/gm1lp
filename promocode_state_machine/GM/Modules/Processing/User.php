<?php

namespace GM\Modules\Processing;

class User
{
    /**
     * @var int
     */
    private $id = 0;
    /**
     * @var email
     */
    private $email = null;
    /**
     * @var float
     */
    private $money = 0;
    /**
     * @var float
     */
    private $cashback_fee = 0;

    /**
     * @param \mysqli $db
     * @param int $id
     * @throws ProcessingError
     */
    public function __construct($db, $id)
    {
        $id = (int)$id;
        $query = $db->query("SELECT `id`, `login`, `money`, `cashback_fee` FROM `users` JOIN `tariff` ON(`users`.`level` = `tariff`.`level`) WHERE `users`.`id` = $id");
        if (mysqli_num_rows($query) == 0) {
            throw new ProcessingError(ErrorCodes::USER_DOES_NOT_EXIST);
        }
        $user = $query->fetch_assoc();
        $this->id = (int)$user['id'];
        $this->email = $user['login'];
        $this->cashback_fee = (float)$user['cashback_fee'];
        $this->money = (float)$user['money'];
    }

    /**
     * @return float
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * @return float
     */
    public function getCashbackFee()
    {
        return $this->cashback_fee;
    }

    /**
     * @param \mysqli $db
     * @param int $amount
     * @param int $fee
     * @throws ProcessingError
     */
    public function charge($db, $amount, $fee){
        $chargeAmount = $amount * ($fee / 100.) * ((100. + $this->cashback_fee) / 100.);
        if($this->money < $chargeAmount){
            throw new ProcessingError(ErrorCodes::INSUFFICIENT_FUNDS);
        }
        if(!$db->query("UPDATE `users` SET `money` = `money` - $chargeAmount WHERE `users`.`id` = $this->id")){
            throw new ProcessingError(ErrorCodes::DATABASE_QUERY_ERROR);
        }
    }

    /**
     * @param \mysqli $db
     * @param int $amount
     * @param int $fee
     * @throws ProcessingError
     */
    public function deposit($db, $amount, $fee){
        $depositAmount = $amount * ($fee / 100.);
        if(!$db->query("UPDATE `users` SET `money` = `money` + $depositAmount WHERE `users`.`id` = $this->id")){
            throw new ProcessingError(ErrorCodes::DATABASE_QUERY_ERROR);
        }
    }

    /**
     * @param string $code
     * @param int $amount
     * @param int $fee
     */
    public function notify($code, $amount, $fee){
        $headers = 'From: noreply@gm1lp.ru' . "\r\n" .
            'Reply-To: noreply@gm1lp.ru' . "\r\n" .
            'X-Mailer: PHP/' . phpversion() . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

        mail($this->email, 'Получите свой кэшбэк!', "Поздравляем! Вы можете получить кэшбэк в системе GM1LP на сумму' . ($amount * ($fee / 100.)) . 'рублей!<br />
Для этого перейдите <a href='http://deamoned.com/?code=$code'>по ссылке</a>.", $headers);
    }
}