<?php

namespace GM\Modules\Processing;

class PromoCode
{
    /** @var int */
    private $id = null;
    /** @var string */
    private $first_name = null;
    /** @var string */
    private $last_name = null;
    /** @var string */
    private $phone = null;
    /** @var int  */
    private $uid = null;
    /** @var User */
    private $user = null;
    /** @var int */
    private $action_id = null;
    /** @var Action */
    private $action = null;
    /** @var string */
    private $code = null;
    /** @var string */
    private $state = null;
    /** @var int */
    private $amount = null;
    /** @var \mysqli */
    private $db = null;

    /**
     * @param array $arguments
     * @throws ProcessingError
     */
    private function __construct($arguments){
        foreach ($arguments as $key => $value){
            $this->$key = $value;
        }
        $this->__connect();
        $this->__init();
    }

    /**
     * @throws ProcessingError
     */
    private function __connect(){
        $host = "127.0.0.1";
        $user = "boxed";
        $password = "3C8h3W2v";
        $db = "new_1lp";
        $port = 3307;
        $this->db = @mysqli_connect($host, $user, $password, $db, $port);
        if(!$this->db){
            throw new ProcessingError(ErrorCodes::DATABASE_CONNECT_ERROR);
        }
        if(!$this->db->query("SET NAMES 'utf8'") ||
            !$this->db->query("SET CHARACTER SET utf8") ||
            !$this->db->query("SET CHARACTER_SET_CONNECTION=utf8") ||
            !$this->db->query("SET SQL_MODE = ''") ||
            !$this->db->set_charset("utf8")){
            throw new ProcessingError(ErrorCodes::DATABASE_QUERY_ERROR);
        }
    }

    /**
     * @throws ProcessingError
     */
    private function __init(){
        $this->state = null;
        if(!is_null($this->code)){
            $object = $this->fetch(['code' => $this->code]);
            if(is_null($object))
                throw new ProcessingError(ErrorCodes::CODE_DOES_NOT_EXIST);
            $this->action = new Action($this->db, $object['action_id'], false);
            $this->user = new User($this->db, $object['uid']);
        }
        else {
            $this->action = new Action($this->db, $this->action_id);
            if(is_null($this->first_name) || is_null($this->last_name)){
                throw new ProcessingError(ErrorCodes::INVALID_NAME);
            }
            if(is_null($this->phone)){
                throw new ProcessingError(ErrorCodes::INVALID_PHONE);
            }
            $object = $this->fetch([
                "first_name" => $this->first_name,
                "last_name" => $this->last_name,
                "phone" => $this->phone,
                "action_id" => $this->action_id
            ]);
            if (is_null($object)) {
                $this->user = new User($this->db, $this->uid);
                $object = $this->create([
                    "first_name" => $this->first_name,
                    "last_name" => $this->last_name,
                    "phone" => $this->phone,
                    "uid" => $this->uid,
                    "action_id" => $this->action_id,
                    "code" => uniqid(),
                    "fee" => $this->action->getFee()
                ]);
            } else {
                $this->id = $object['id'];
                if(!empty($this->amount)){
                    $this->set('amount', $this->amount);
                    $object['amount'] = $this->amount;
                }
                $this->user = new User($this->db, $object['uid']);
            }
        }
        foreach ($object as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @param array $object
     * @return array|null
     */
    private function fetch($object){
        $object['disabled'] = false;
        $query = $this->db->query("SELECT id, first_name, last_name, phone, uid, action_id, code, state, fee, amount FROM promocodes WHERE "
            . implode(" AND ", array_map([$this, 'escape_expression'], array_keys($object), array_values($object))));
        if(mysqli_num_rows($query) > 0)
            return $query->fetch_assoc();
        else
            return null;
    }

    /**
     * @param array $object
     * @return array|null
     */
    private function create($object){
        $this->db->query("INSERT INTO promocodes(" . implode(', ', array_map([$this, 'escape_left'], array_keys($object)))
            . ") VALUES(" . implode(', ', array_map([$this, 'escape_right'], array_values($object))) . ");");
        return $this->fetch($object);
    }

    /**
     * @param string $left
     * @return string
     */
    private function escape_left($left){
        return "`{$this->db->real_escape_string($left)}`";
    }

    /**
     * @param string $right
     * @return string
     */
    private function escape_right($right){
        if(is_bool($right))
            return $right ? "true" : "false";
        elseif(is_numeric($right))
            return "$right";
        else
            return "'{$this->db->real_escape_string($right)}'";
    }

    /**
     * @param string $left
     * @param string $right
     * @return string
     */
    private function escape_expression($left, $right){
        return "{$this->escape_left($left)} = {$this->escape_right($right)}";
    }

    /**
     * @param string $code
     * @return PromoCode
     * @throws ProcessingError
     */
    public static function createByCode($code){
        return new PromoCode(['code' => $code]);
    }

    /**
     * @param string $first_name
     * @param string $last_name
     * @param string $phone
     * @param int $action_id
     * @param int $amount
     * @return PromoCode
     * @throws ProcessingError
     */
    public static function createByData($first_name, $last_name, $phone, $action_id, $amount){
        return new PromoCode(['first_name' => $first_name, 'last_name' => $last_name, 'phone' => $phone, 'action_id' => $action_id, 'amount' => $amount]);
    }

    /**
     * @param string $first_name
     * @param string $last_name
     * @param string $phone
     * @param int $uid
     * @param int $action_id
     * @return PromoCode
     * @throws ProcessingError
     */
    public static function createNew($first_name, $last_name, $phone, $uid, $action_id){
        return new PromoCode(['first_name' => $first_name, 'last_name' => $last_name, 'phone' => $phone, 'uid' => $uid, 'action_id' => $action_id, 'created_at' => now()]);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @throws ProcessingError
     */
    private function set($key, $value){
        if(!$this->db->query("UPDATE `promocodes` SET `$key` = '$value' WHERE `id` = {$this->id};")){
            throw new ProcessingError(ErrorCodes::DATABASE_QUERY_ERROR);
        }
    }

    /**
     * @param array $args
     * @throws ProcessingError
     */
    private function reserveEvent($args = []){
        $this->set('reserved_at', now());
    }

    /**
     * @param array $args
     * @throws ProcessingError
     */
    private function printEvent($args = []){
        if (is_null($this->amount) || $this->amount < 0){
            throw new ProcessingError(ErrorCodes::INVALID_AMOUNT);
        }
        $this->action->fetchUser($this->db)->getUser()->charge($this->db, $this->amount, $this->action->getFee());
        $this->user->notify($this->code, $this->amount, $this->action->getFee());
        $this->set('printed_at', now());
    }

    /**
     * @param array $args
     * @throws ProcessingError
     */
    private function expendEvent($args = []){
        $this->user->deposit($this->db, $this->amount, $this->action->getFee());
        $this->set('disabled', true);
        $this->set('expended_at', now());
    }

    private static $state_transition_table = [
        null => [
            EventTable::RESERVE_EVENT => null,
            EventTable::PRINT_EVENT => null,
            EventTable::EXPEND_EVENT => null,
        ],
        StateTable::NEW_STATE => [
            EventTable::RESERVE_EVENT => StateTable::RESERVED_STATE,
            EventTable::PRINT_EVENT => null,
            EventTable::EXPEND_EVENT => null,
        ],
        StateTable::RESERVED_STATE => [
            EventTable::RESERVE_EVENT => StateTable::RESERVED_STATE,
            EventTable::PRINT_EVENT => StateTable::PRINTED_STATE,
            EventTable::EXPEND_EVENT => null,
        ],
        StateTable::PRINTED_STATE => [
            EventTable::RESERVE_EVENT => null,
            EventTable::PRINT_EVENT => null,
            EventTable::EXPEND_EVENT => StateTable::EXPENDED_STATE,
        ],
        StateTable::EXPENDED_STATE => [
            EventTable::RESERVE_EVENT => null,
            EventTable::PRINT_EVENT => null,
            EventTable::EXPEND_EVENT => null,
        ]
    ];

    /**
     * @param string $event
     * @param array $args
     * @throws ProcessingError
     */
    function next($event, $args = []){
        if(!array_key_exists($event, self::$state_transition_table[$this->state])){
            throw new ProcessingError(ErrorCodes::WRONG_EVENT);
        }
        $next_state = self::$state_transition_table[$this->state][$event];
        if(is_null($next_state)) {
            throw new ProcessingError(ErrorCodes::FORBIDDEN_TRANSITION);
        }
        $this->{$event . "Event"}($args);
        $this->set("state", $next_state);
    }
}

/**
 * @return false|string
 */
function now(){
    date_default_timezone_set("Europe/Moscow");
    return date("Y-m-d H:i:s");
}