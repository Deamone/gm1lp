<?php

spl_autoload_register(function($class) {
    $parts = explode('\\', $class);
    require $_SERVER['DOCUMENT_ROOT'] . "/" . implode("/", $parts) . '.php';
});

use GM\Modules\Processing\ProcessingError;
use GM\Modules\Processing\PromoCode;

try {
    if (isset($_REQUEST['code']))
        $code = PromoCode::createByCode($_REQUEST['code']);
    else {
        if (isset($_REQUEST['uid'])) {
            $code = PromoCode::createNew(
                @$_REQUEST['first_name'],
                @$_REQUEST['last_name'],
                @$_REQUEST['phone'],
                $_REQUEST['uid'],
                @$_REQUEST['action_id']
            );
        } else
            $code = PromoCode::createByData(
                @$_REQUEST['first_name'],
                @$_REQUEST['last_name'],
                @$_REQUEST['phone'],
                @$_REQUEST['action_id'],
                isset($_REQUEST['amount']) ? $_REQUEST['amount'] : 0
            );
    }
    $code->next($_REQUEST['event'], isset($_REQUEST['args']) ? $_REQUEST['args'] : []);
    die('0');
} catch (ProcessingError $error){
    die("{$error->getCode()}");
}