<?php
session_start();

$stage = isset($_REQUEST['stage']) ? $_REQUEST['stage'] : "0";
$code = @$_REQUEST['code'];
if($stage === "1") {
    if(!array_key_exists("csrf_token", $_COOKIE) || !array_key_exists("csrf_token", $_SESSION) || $_SESSION["csrf_token"] !== $_COOKIE["csrf_token"]) {
        $stage = "0";
    } else {
        $query = http_build_query(['code' => $code]);
        $curl = curl_init("http://promocode_state_machine.local:9080/?$query&event=expend");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if(curl_exec($curl) !== '0') {
            $stage = "0";
        } else {
            header("Location: https://gm1lp.ru");
            die();
        }
    }
}
$csrf_token = uniqid();
$_SESSION["csrf_token"] = $csrf_token;
session_write_close();
setcookie("csrf_token", $csrf_token);

?><!DOCTYPE html>
<html>
<head>
    <title>Активация промо-кода</title>
    <link rel="stylesheet" href="/style.css" />
</head>
<body>
<div class="header-line">
    <img src="/logo1.png" />
</div>
<p class="center">
    Данный сайт используется системой GM (GM LTD - <a href="https://gm1lp.ru/company">www.gm1lp.ru/company</a>)<br>
        для активации уникальных кодов с сервиса <a href="http://gmcard.ru">gmcard.ru</a>
</p>
<div class="center form">
    Если у Вас есть уникальный код, введите его ниже:<br/>
    <form method="POST">
        <div class="inline-block">
            <label>
                <input type="text" name="code" placeholder="6V9C3d242KPDM" value="<?php echo $code;?>" />
            </label>
            <input type="hidden" name="stage" value="1"/>
            <input type="submit" class="go" value="GO"/>
        </div>
    </form>
</div>
<div class="center">
    <div class="inline-block footer-line-d">D</div>
    <div class="footer-line-deamoned">
        DEAMONED<br>
        <a href="/old.php">предыдущая версия сайта</a>
    </div>
</div>
</body>
</html>