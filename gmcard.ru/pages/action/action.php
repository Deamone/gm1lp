<?php
use GM\GMCard\DAO\Action;
use GM\Modules\Session\Session;

$_USER = $USER;
if (!isset($SYSTEM['get']['id'])) {
    redirect();
}
$id = $SYSTEM['get']['id'];
$data = Action::getById($id);
if (!$data->getActionId()) {
    redirect();
}

if($data->getCost()->isCashback()) {
    if ($data->getUserId() == Session::get_instance()->get_uid()) {
        $type = 'promocode';
    } else {
        $type = 'coupon';
    }
} else {
    $type = 'none';
}

if (isAjax()) {
    if (isset($_POST['like'])) {
        upLikeAction($data->getActionId());
        $data = Action::getById($id);
        echo $data->getLike();
    }
    if (isset($_GET['sc'])) {
        $actions = Action::get(false, 16);
        shuffle($actions);
        $stop = 0;

        $cssStyle = 'a_discount_ping';
        $j = 0;
        $all = 0;
        $lines = 0;
        foreach ($actions as $k => $action) {
            include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/discount.php';
        }
    }
    exit;
}

$data->getSite()->stripSchema();
$time = $data->getCreated();
header_($data->getTitle(), '', '', ''
    . '<link rel="stylesheet" type="text/css" href="/css/bjqs.css">'
    . '<link rel="stylesheet" type="text/css" href="/css/style_action.css"/>'
    . '<link rel="stylesheet" type="text/css" href="/css/style_external.css"/>'
    . '<script type="text/javascript" src="/js/bjqs-1.3.min.js"></script>'
    . '<link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
            <link rel="stylesheet" href="/css/jquery.Jcrop.min.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.countdown.js"></script>', 'action_view');
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="action_wrapper">
            <div class="a_left_block">
                <div class="a_slider">
                    <div id="action_slider">
                        <ul class="bjqs">
                            <?php if (isset($data->getImages()[1])) {
                                echo '<li><a href="#"><img src="/' . $data->getImages()[1] . '" title="title"></a></li>';
                            }
                            if (isset($data->getImages()[2])) {
                                echo '<li><a href="#"><img src="/' . $data->getImages()[2] . '" title="title"></a></li>';
                            }
                            if (isset($data->getImages()[3])) {
                                echo '<li><a href="#"><img src="/' . $data->getImages()[3] . '" title="title"></a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                    <?php if($data->getCost()->isDiscount()){?>
                        <div class="a_discount <?php echo $data->getCost()->getCssClass();?>">
                            <div class="a_discount_label">скидка до</div>
                            <?php if($data->getCost()->isPercent()){?>
                                <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $data->getCost()->getDiscount(); ?></span> <span class="a_discount_per">%</span></div>
                            <?php } ?>
                            <?php if($data->getCost()->isRaw()){?>
                                <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $data->getCost()->getDiscount(); ?> руб.</span></div>
                            <?php }?>
                        </div>
                    <?php }?>
                </div>
                <?php if (isset($data->getImages()[4])) {
                    echo '<div class="attachments">';
                    foreach ($data->getImages() as $k => $img) {
                        if ($k < 4 || $k > 10) {
                            continue;
                        }
                        if (!$img || $img == '/') {
                            continue;
                        }
                        echo '<a class="attachment fancybox" rel="dop" href="/' . $img . '"><img src="/' . $img . '"></a>';
                    }

                    echo '</div>';
                }
                ?>

                <div class="a_socials al_block">
                    <div class="a_socials_icons">
                        <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                        <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir" data-yashareTheme="counter" data-yashareLink="http://<?php echo $_SERVER['HTTP_HOST'] . '/action/action?id=' . $data->getActionId(); ?>"></div>
                    </div>

                    <div class="a_like">
                        <button class="btn btn-color-orange btn-size-small btn-icon btn-icon-heart rounded_3">Мне нравится</button>
                        <span class="a_like_amount"><?php echo $data->getLike(); ?></span>
                    </div>
                    <div class="clear"></div>
                </div>
                <?php if (isset($data->getImages()[12]) && !empty($data->getImages()[12])) {
                    #echo '<h2>Видео</h2>';
                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data->getImages()[12], $match)) {
                        $video_id = $match[1];

                        echo '<div class="video"><iframe width="724" height="479" src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0" allowfullscreen></iframe></div>';
                    }
                }
                ?>
                <div class="a_description al_block last">

                    <p>
                        <?php echo nl2br($data->getCondition()); ?>
                    </p>

                    <ul class="a_our_socials">
                        <?php
                        if ($data->getSite()->getGm()) {
                            echo '<li class="iconSocs">' . soc_ico('gm', $data->getSite()->getGm()) . '</li>';
                        }
                        if ($data->getSite()->getVk()) {
                            echo '<li class="iconSocs">' . soc_ico('vk', $data->getSite()->getVk()) . '</li>';
                        }
                        if ($data->getSite()->getFb()) {
                            echo '<li class="iconSocs">' . soc_ico('fb', $data->getSite()->getFb()) . '</li>';
                        }
                        if ($data->getSite()->getOk()) {
                            echo '<li class="iconSocs">' . soc_ico('od', $data->getSite()->getOk()) . '</li>';
                        }
                        if ($data->getSite()->getTw()) {
                            echo '<li class="iconSocs">' . soc_ico('tw', $data->getSite()->getTw()) . '</li>';
                        }
                        if ($data->getSite()->getYt()) {
                            echo '<li class="iconSocs">' . soc_ico('yo', $data->getSite()->getYt()) . '</li>';
                        }
                        if ($data->getSite()->getIg()) {
                            echo '<li class="iconSocs">' . soc_ico('ia', $data->getSite()->getIg()) . '</li>';
                        }
                        if ($data->getSite()->getPr()) {
                            echo '<li class="iconSocs">' . soc_ico('pt', $data->getSite()->getPr()) . '</li>';
                        }
						if ($data->getSite()->getGoogleplus()) {
							echo '<li class="iconSocs">' . soc_ico('googleplus', $data->getSite()->getGoogleplus()) . '</li>';
						}
                        if ($data->getSite()->getMain()) {
                            echo '<li>Наш сайт: <a href="//' . $data->getSite()->getMain() . '" target="_blank">' . $data->getSite()->getMain() . '</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="a_right_block">
                <div class="a_time ar_block" id="a_time">
                    <span>До конца акции</span> <span class="a_time_days">4 дня</span> <span class="a_time_time">15:13:15</span>
                </div>

                <div class="a_discount_description ar_block">
                    <p>
                        <?php echo $data->getTitle(); ?>
                    </p>

                    <?php if($data->getCost()->isDiscount()){?>
                        <!-- <div class="a_price">
                            <span class="a_price_old">
                                <?php echo $data->getCost()->getCost(); ?>
                            </span>
                            <span class="a_price_new">
                                <?php echo $data->getCost()->getFinalCost(); ?> руб.
                            </span>
                        </div> -->
                    <?php } else {?>
                        <div class="a_price cashback-plug"><?php echo $data->getCost()->getFee();?>%</div>
                    <?php } ?>
                    <form action="//<?php echo $data->getSite()->getMain(); ?>" target="_blank" class="inline-block">
                        <button class="btn rounded_3">Перейти на сайт</button>
                    </form>
                    <?php switch($type):
                        case 'promocode': ?>
                            <button class="btn rounded_4" onclick="openm();" style="margin-left: 40px;">Напечатать промо-код</button>
                            <?php break;
                        case 'coupon': ?>
                            <button class="btn rounded_4" onclick="openm();">Скачать купон</button>
                            <?php break;
                        case 'none':
                        default: ?>
                            <button class="btn rounded_4">Купон не нужен</button>
                        <?php endswitch; ?>
                </div>

                <script type="text/javascript">
                    jQuery(function($) {
                        ymaps.ready(init);
                        function init() {
                            var myMap = new ymaps.Map("map", {
                                center: [<?php echo $data->getAddress()->getLatitude();?>, <?php echo $data->getAddress()->getLongitude();?>],
                                zoom: 7
                            });
                            myMap.geoObjects.add(new ymaps.Placemark([<?php echo $data->getAddress()->getLatitude();?>, <?php echo $data->getAddress()->getLongitude();?>]));
                            <?php foreach ($data->getDaddress() as $address){?>
                            myMap.geoObjects.add(new ymaps.Placemark([<?php echo $address->getLatitude();?>, <?php echo $address->getLongitude();?>]));
                            <?php }?>
                        }
                    });
                </script>

                <div id="map" class="a_discount_map ar_block" style="height: 200px;"></div>

                <div class="a_contacts ar_block last">
                    <h2>Контакты</h2>

                    <h3>Адрес:</h3>
                    <span><?php echo nl2br($data->getAddress()->getAddress()); ?></span>

                    <h3>График работы:</h3>
                    <span><?php echo nl2br($data->getAddress()->getGraph()); ?></span>

                    <h3>Телефоны:</h3>
                    <span><?php echo nl2br($data->getAddress()->getPhone()) ?></span>

                    <h3>Сайт:</h3>
                    <?php echo '<a href="//' . $data->getAddress()->getSite() . '" target="_blank">' . $data->getAddress()->getSite() . '</a>'; ?>

                    <?php

                    foreach ($data->getDaddress() as $i => $address) {
                        if ($address->getAddress() || $address->getPhone() || $address->getGraph() || $address->getSite()) {
                            echo '<hr/>';
                        }
                        if ($address->getAddress()) {
                            echo '<h3>Адрес:</h3>
                    <span>' . nl2br($address->getAddress()) . '</span>';
                        }
                        if ($address->getGraph()) {
                            echo '<h3>График работы:</h3>
                    <span>' . nl2br($address->getGraph()) . '</span>';
                        }
                        if ($address->getPhone()) {
                            echo '<h3>Телефоны:</h3>
                    <span>' . nl2br($address->getPhone()) . '</span>';
                        }
                        if ($address->getSite()) {
                            echo '<h3>Сайт:</h3>';
                            echo '<a href="//' . $address->getSite() . '" target="_blank">' . $address->getSite() . '</a>';
                        }
                    } ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <h2>Другие акции</h2>

        <div id="smActions">
            <?php
            ?>




            <!--div class="action_small_wrapper action_double_wrapper action_text_wrapper">
                <p class="large">У вас на сайте или в магазине часто<br />проходят розыгрыши ценных подарков?</p>

                <p>Подключитесь к GM, у нас есть те,<br />кто с радостью примет в них участие!</p>

                <p>Благодаря полностью автоматизированному<br />сервису конкурсов, на это уйдет несколько минут</p>
            </div-->
        </div>

        <div class="clear"></div>

        <div class="action_more_btn">
            <div class="action_more_btn_wrapper">
                <button>Показать еще</button>
            </div>
        </div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('.a_like button').click(function () {
                $.post('?id=<?php echo $id; ?>', 'like=1', function (data) {
                    $('.a_like_amount').html(data);
                });
            });


            $('#counter').countdown({
                timestamp: '<?php echo $data->getEnd(); ?>000',
                start: '<?php
        if (time() < mktime(0, 0, 0, 7, 1, 2015)) {# STOP
            $time = $data->getCreated();
        }else{$time = time();}echo $time;
        ?>000',
                callback: function (a, b, c, d, timer) {
                    var day = '';
                    if (a) {
                        day = ' <span class="a_time_days">' + a + ' ' + declOfNum(a, ['день', 'дня', 'дней']) + '</span> ';
                    }
                    if (b < 10) {
                        b = '0' + b;
                    }
                    if (c < 10) {
                        c = '0' + c;
                    }
                    if (d < 10) {
                        d = '0' + d;
                    }
                    $('#a_time').html('<span>До конца акции</span>' + day + '<span class="a_time_time">' + b + ':' + c + ':' + d + '</span>');
                    <?php
                    if (time() < mktime(0, 0, 0, 7, 1, 2015)) { //STOP
                        echo 'clearInterval(timer);';
                    }
                    ?>
                }
            });
        });
    </script>
    <script>
        $(".action_small_wrapper img").load(function () {
            var imageHeight = $(this).height();
            console.log(imageHeight);
            if (imageHeight < 208) {
                $(this).css({'height': '100%'});
            }
            //$(this).parent().append(imageHeight);
        });
        var loader = false;
        function loadMoreAction() {
            if (!loader) {
                loader = true;
                var data = {
                    'count': $('#smActions .action_small_wrapper').size(),
                    'lines': $('#smActions .action_small_wrapper[data-lines]:last').attr('data-lines'),
                    'all': $('#smActions .action_small_wrapper[data-lines]:last').attr('data-all')
                };
                $.get('/', data, function (data) {
                    //$(data).html();
                    $('#smActions .action_small_wrapper[data-lines]:last').after($(data).clone());
                    $('.action_small_wrapper').hover(function () {
                        $(this).children('.a_detailed').stop().slideDown('fast');
                        $(this).children('.a_description').hide();
                    }, function () {
                        $(this).children('.a_detailed').stop().slideUp('fast');
                        $(this).children('.a_description').show();
                    });
                    runCountDown();
                    loader = false;
                    if (!data) {
                        $('.action_more_btn_wrapper').parent().hide();
                    }
                });
            }
        }
        $(document).ready(function () {
            $('.action_more_btn_wrapper').click(function () {
                loadMoreAction();
                return false;
            });
            $.post('?id=<?php echo $id; ?>&sc', '', function (data) {
                $('#smActions').html(data);
            });
			clickable($('.a_description p'));
        });
    </script>
<?php

if($type == 'coupon' && !$_USER) { ?>
    <div class="try-popup">
        <div class="try-popup__content">
            <div class="try-popup__close-btn" onclick="$.fancybox.close()"></div>
            Для скачивания купонов необходимо авторизоваться на сайте либо зарегистрироваться. Это бесплатно и займет не более 2-х минут.
        </div>
    </div>
<?php } else { ?>
    <div class="substrate" id="open" style="display: none">
        <div class="wrap_modal">
            <div class="modal_content">
                <div class="modal_header">
                    <img src="/images/tipok.png" class="tipok">
                    <a href="#" id="close" onclick="closem();"><img src="/images/close.png"></a>
                </div>
                <div class="clearfix"></div>
                <p class="zagolovok">На кого <?php if($type == 'coupon'):?>сформировать<?php else:?>был сформирован<?php endif;?> купон?</p>
                <form action="/action/<?php echo $type;?>" method="post">
                    <input type="hidden" name="action_id" value="<?php echo $id;?>">
                    <span id="ego_vel">Его величество</span>
                    <div class="onoffswitch">
                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch"
                               onclick="ceking();" checked>
                        <label class="onoffswitch-label" for="myonoffswitch"></label>
                    </div>
                    <span id="ee_vel">Ее величество</span>
                    <div class="clearfix"></div>
                    <div class="line"></div>
                    <div class="center_form">
                        <p>Имя<?php if($type == 'promocode'):?> [как на купоне]<?php endif;?></p>
                        <input type="text" name="first_name" placeholder="Иван">
                        <p>Отчество (можно фамилию)<?php if($type == 'promocode'):?> [как на купоне]<?php endif;?></p>
                        <input type="text" name="last_name" placeholder="Иванов">
                        <p>Как с вами связаться?<?php if($type == 'promocode'):?> [как на купоне]<?php endif;?></p>
                        <input type="text" id="maska" name="phone" placeholder="+ 7 (777) 777 - 77 - 77">
                        <?php if($type == 'promocode'):?><p>Сумма, на которую начислить кэшбэк</p>
                        <input type="number" name="amount" min="0"><?php endif;?>
                    </div>
                    <p class="sog">
                        <span>*</span>Мы обещаем что не будем присылать вам СПАМ и рекламные СМС,</br>
                        согласно пункту 1 ст.3 Федерального закона от 13.03.2006 N 38-Ф3 «О</br>
                        рекламе»
                    </p>
                    <input type="submit" value="<?php if($type == 'coupon'):?>Напечатать купон<?php else:?>Выслать промо-код<?php endif;?>" class="button">
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
<?php }

footer();
