<?php
use GM\GMCard\DAO\Action;

$_USER = $USER;
$q = urldecode($_GET['qSearch']);
if (isset($_GET['qCategories']) && !empty($_GET['qCategories'])) {
    $qCat = explode(',', urldecode($_GET['qCategories']));
    #var_dump($qCat);
    if (isset($qCat[0]) && $qCat[0] == -1) {
        $qCat = array(-1);
    }
} else {
    $qCat = array();
}

header_('Поиск акций', '', '', ''
    . '<link rel="stylesheet" type="text/css" href="/css/bjqs.css">'
    . '<link rel="stylesheet" type="text/css" href="/css/style_action.css"/>'
    . '<link rel="stylesheet" type="text/css" href="/css/style_external.css"/>'
    . '<script type="text/javascript" src="/js/bjqs-1.3.min.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.countdown.js"></script>', 'action_view');

$actions = Action::find($qCat, $q, 27);
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <?php
        $time = time();
        shuffle($actions);
        /** @var Action[] $active */
        $active = array();
        /** @var Action[] $unactive */
        $unactive = array();
        foreach ($actions as $action) {
            if ($action->getEnd() > $time) {
                $active[] = $action;
                continue;
            }
            $unactive[] = $action;
        }
        if (!$actions) {
            echo '<h2 style="margin-top: 0;">Сожалеем, но акции удовлетворяющие запросу не найдены.</h2>';
        } elseif (!$qCat) {
            echo '<h2 style="margin-top: 0;">Вы не указали категорию. Посмотрите все акции вашего города.</h2>';
        } else {
            if ($active) {
                echo '<h2 style="margin-top: 0;">Список активных акций по запросу &quot;' . $q . '&quot;</h2>';
                echo '<div>';
                $stop = 0;

                $cssStyle = 'a_discount_ping';
                $j = 0;
                $all = 0;
                $lines = 0;
                $time = time();
                foreach ($active as $k => $action) {
                    include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/discount.php';
                }
                echo '</div>';
            }
            if ($unactive) {
                echo '<br style="clear: both;"/><h2 style="margin-top: 0;">Список завершенных акций по запросу &quot;' . $q . '&quot;</h2>';
                echo '<div>';
                $stop = 0;

                $cssStyle = 'a_discount_ping';
                $j = 0;
                $all = 0;
                $lines = 0;
                $time = time();
                foreach ($unactive as $k => $action) {
                    include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/discount.php';
                }
                echo '</div>';
            }
        }
        ?>
        <div>
            <?php


            ?>




            <!--div class="action_small_wrapper action_double_wrapper action_text_wrapper">
                <p class="large">У вас на сайте или в магазине часто<br />проходят розыгрыши ценных подарков?</p>

                <p>Подключитесь к GM, у нас есть те,<br />кто с радостью примет в них участие!</p>

                <p>Благодаря полностью автоматизированному<br />сервису конкурсов, на это уйдет несколько минут</p>
            </div-->
        </div>

        <div class="clear"></div>

        <!--div class="action_more_btn">
            <div class="action_more_btn_wrapper">
                <button>Показать еще</button>
            </div>
        </div-->
    </div>
    <!--main-->
    <script>
        $(".action_small_wrapper img").load(function () {
            var imageHeight = $(this).height();
            console.log(imageHeight);
            if (imageHeight < 208) {
                $(this).css({'height': '100%'});
            }
            //$(this).parent().append(imageHeight);
        });
    </script>
<?php
footer();
