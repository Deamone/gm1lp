﻿<?php
use GM\GMCard\DAO\Action;

$_USER = $USER;

header_('gmcard.ru', '', '', ''
    . '', 'external action_list');

$actions = Action::getCouponsByUid($USER['id']);
/** @var Action[] $act */
$act = array();
/** @var Action[] $unact */
$unact = array();
$time = time();
foreach ($actions as $action) {
    if ($action->getEnd() > $time) {
        $act[] = $action;
    } else {
        $unact[] = $action;
    }
}
?>
	<link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/css/jquery.Jcrop.min.css" type="text/css" media="screen" />
	<script type="text/javascript" src="/fancybox/jquery.fancybox.js"></script>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <h1>Мои акции</h1>
            <?php
            if (!$act && !$unact) {
                echo '<h3 class="blue">У вас нет акций с купонами</h3>';
            }
            if ($act) {
                echo '<h3 class="blue">Активные акции с купонами</h3>';
            }
            foreach ($act as $action) {
                $images = $action->getImages();
                $cost = $action->getCost();
                $cssStyle = $cost->getCssClass();
                include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/actions.php';
            }
            ?>
            <div class="clear"></div>


            <?php
            if ($unact) {
                echo '<h3 class="orange">Завершенные</h3>';
            }
            foreach ($unact as $action) {
                $images = $action->getImages();
                $cost = $action->getCost();
                $cssStyle = $cost->getCssClass();
                include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/actions.php';
            }
            ?>
        </div>

        <div class="right_block">
            <ul class="right_menu">
                <li><a class="rounded_3" href="/user/lk">Личный кабинет</a></li>
                <li><a class="rounded_3" href="/action/create">Запустить акцию</a></li>
                <li><a class="rounded_3" href="/action/my">Мои акции</a></li>
                <li class="active"><a class="rounded_3" href="/action/my_coupons">Мои акции с купонами</a></li>
                <li><a class="rounded_3" href="/my_coupons">Мои купоны</a></li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
			$("#various1").fancybox({
				'width'				: '500px',
				'height'			: '222px',
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'closeBtn'          : true,
				
				'tpl': {
					'wrap'     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
					'image'    : '<img class="fancybox-image" src="{href}" alt="" />',
					'iframe'   : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>',
					'error'    : '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
					'closeBtn' : '<a title="Close" class="closes fancybox-item fancybox-close" href="javascript:;"></a>',
					'next'     : '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
					'prev'     : '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>',
					'loading'  : '<div id="fancybox-loading"><div></div></div>'
				}
			});
			
            $('.action_small_wrapper').hover(function () {
                $('.a_description').show();
                return false;
            }, function () {
                return false;
            });
        });
    </script>
<?php
footer();
