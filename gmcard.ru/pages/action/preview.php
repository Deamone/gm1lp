<?php
$_USER = $USER;

if (isset($SYSTEM['get']['token'])) {
    $token = $SYSTEM['get']['token'];
    if (!file_exists('tmp/' . $token . '.txt')) {
        exit('Превью недоступно');
    }
    $data = unserialize(file_get_contents('tmp/' . $token . '.txt'));
} else {
    exit('Превью недоступно');
}
foreach ($data['site'] as $k => $site) {
    $data['site'][$k] = preg_replace('/http(.?)(\/{1,})/i', '', $data['site'][$k]);
}
header_('Предпросмотр акции', '', '', ''
    . '<link rel="stylesheet" type="text/css" href="/css/bjqs.css">'
    . '<link rel="stylesheet" type="text/css" href="/css/style_action.css"/>' .
    ''

    . '<script type="text/javascript" src="/js/bjqs-1.3.min.js"></script>', 'action_view');
if(isset($_GET['nx'])){
    var_dump($data);
    exit;
}
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="action_wrapper">
            <div class="a_left_block">
                <div class="a_slider">
                    <div id="action_slider">
                        <ul class="bjqs">
                            <?php if ($data['chooseImage1__']) {
                                echo '<li><a href="#"><img src="/' . $data['chooseImage1__'] . '" title="title"></a></li>';
                            }
                            if ($data['chooseImage3__']) {
                                echo '<li><a href="#"><img src="/' . $data['chooseImage2__'] . '" title="title"></a></li>';
                            }
                            if ($data['chooseImage2__']) {
                                echo '<li><a href="#"><img src="/' . $data['chooseImage3__'] . '" title="title"></a></li>';
                            }
                            if(!$data['chooseImage2__'] && !$data['chooseImage3__'] && $data['chooseImage1__']){
                                echo '<li><a href="#"><img src="/' . $data['chooseImage1__'] . '" title="title"></a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="a_discount">
                        <div class="a_discount_label">скидка до</div>
                        <?php if($data['cost']['discount_type'] == 'percent'){?>
                            <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $data['cost']['discount']; ?></span> <span class="a_discount_per">%</span></div>
                        <?php } else {?>
                            <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $data['cost']['discount']; ?> руб.</span></div>
                        <?php }?>
                    </div>
                </div>
                <?php if (isset($data['images'])) {
                    echo '<div class="attachments">';
                    foreach ($data['images'] as $k => $img) {
                        if ($k > 10) {
                            continue;
                        }
                        if (!$img || $img == '/') {
                            continue;
                        }
                        echo '<a class="attachment fancybox" rel="dop" href="' . $img . '"><img src="../' . $img . '"></a>';
                    }

                    echo '</div>';
                }
                ?>
                <div class="a_socials al_block">
                    <div class="a_socials_icons">
                        <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                        <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir" data-yashareTheme="counter"></div>
                    </div>

                    <div class="a_like">
                        <button class="btn btn-color-orange btn-size-small btn-icon btn-icon-heart rounded_3">Мне нравится</button>
                        <span class="a_like_amount">1258</span>
                    </div>
                    <div class="clear"></div>
                </div>
                <?php if (isset($data['yt']) && !empty($data['yt'])) {
                    #echo '<h2>Видео</h2>';
                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data['yt'], $match)) {
                        $video_id = $match[1];

                        echo '<div class="video"><iframe width="724" height="479" src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0" allowfullscreen></iframe></div>';
                    }
                }
                ?>
                <div class="a_description al_block last">
                    <p>
                        <?php echo nl2br($data['condition']); ?>
                    </p>

                    <ul class="a_our_socials">
                        <?php
                        if ($data['site']['gm']) {
                            echo '<li class="iconSocs">' . soc_ico('gm', $data['site']['gm']) . '</li>';
                        }
                        if ($data['site']['vk']) {
                            echo '<li class="iconSocs">' . soc_ico('vk', $data['site']['vk']) . '</a></li>';
                        }
                        if ($data['site']['fb']) {
                            echo '<li class="iconSocs">' . soc_ico('fb', $data['site']['fb']) . '</a></li>';
                        }
                        //
                        if ($data['site']['ok']) {
                            echo '<li class="iconSocs">' . soc_ico('od', $data['site']['ok']) . '</a></li>';
                        }
                        if ($data['site']['tw']) {
                            echo '<li class="iconSocs">' . soc_ico('tw', $data['site']['tw']) . '</a></li>';
                        }
                        if ($data['site']['yt']) {
                            echo '<li class="iconSocs">' . soc_ico('yo', $data['site']['yt']) . '</a></li>';
                        }
                        if ($data['site']['ig']) {
                            echo '<li class="iconSocs">' . soc_ico('ia', $data['site']['ig']) . '</a></li>';
                        }
                        if ($data['site']['pr']) {
                            echo '<li class="iconSocs">' . soc_ico('pt', $data['site']['pr']) . '</a></li>';
                        }
                        if ($data['site']['over']) {
                            echo '<li>Наш сайт: <a href="//' . $data['site']['main'] . '" target="_blank">' . $data['site']['main'] . '</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="a_right_block">
                <div class="a_time ar_block">
                    <span>До конца акции</span> <span class="a_time_days">4 дня</span> <span class="a_time_time">15:13:15</span>
                </div>

                <div class="a_discount_description ar_block">
                    <p>
                        <?php echo nl2br($data['title']); ?>
                    </p>

                    <div class="a_price"><span class="a_price_old"><?php echo $data['cost']['cost']; ?></span> <span class="a_price_new"><?php echo $data['cost']['new']; ?> руб.</span></div>
                    <form action="<?php echo $data['site']['main']; ?>">
                        <button class="btn rounded_3">Перейти на сайт</button>
                    </form>
                </div>

                <div class="a_contacts ar_block last">
                    <h2>Контакты</h2>

                    <h3>Адрес:</h3>
                    <span><?php echo nl2br($data['address']['address']); ?></span>

                    <h3>График работы:</h3>
                    <span><?php echo nl2br($data['address']['graph']); ?></span>

                    <h3>Телефоны:</h3>
                    <span><?php echo nl2br($data['address']['phone']); ?></span>

                    <h3>Сайт:</h3>
                    <?php echo '<a href="//' . $data['address']['site'] . '" target="_blank">' . $data['address']['site'] . '</a>'; ?>

                    <?php

                    foreach ($data['daddress'] as $i => $data_) {
                        if ($data_[0]) {
                            echo '<h3>Адрес:</h3>
                    <span>' . nl2br($data_['address']) . '</span>';
                        }
                        if ($data_[1]) {
                            echo '<h3>График работы:</h3>
                    <span>' . nl2br($data_['graph']) . '</span>';
                        }
                        if ($data_[2]) {
                            echo '<h3>Телефоны:</h3>
                    <span>' . nl2br($data_['phone']) . '</span>';
                        }
                        if ($data_[3]) {
                            echo '<h3>Сайт:</h3>';
                            echo '<a href="//' . $data_['site'] . '" target="_blank">' . $data_['site'] . '</a>';
                        }
                    } ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <h2>Другие акции</h2>

        <div>
            <div class="action_small_wrapper first">
                <div class="a_image">
                    <a href="#"><img src="/images/actions/action1.jpg"/></a>
                </div>
                <div class="a_description">Обувь известных спортивных брендов</div>
                <div class="a_price"><span class="a_price_old">300</span> <span class="a_price_new">150 руб.</span></div>

                <div class="a_discount a_discount_orange">
                    <div class="a_discount_label">скидка до</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">50</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>

            <div class="action_small_wrapper">
                <div class="a_image">
                    <a href="#"><img src="/images/actions/action2.jpg"/></a>
                </div>
                <div class="a_description">Парикмахерские услуги в студии красоты Юлии Рудых</div>
                <div class="a_price"><span class="a_price_old">1500</span> <span class="a_price_new">1950 руб.</span></div>

                <div class="a_discount a_discount_lgreen">
                    <div class="a_discount_label">скидка до</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>

            <div class="action_small_wrapper">
                <div class="a_image">
                    <a href="#"><img src="/images/actions/action3.jpg"/></a>
                </div>
                <div class="a_description">Сушилки для обуви Dr. Dry и осушители Sorbis для защиты салон...</div>
                <div class="a_price"><span class="a_price_old">300</span> <span class="a_price_new">150 руб.</span></div>

                <div class="a_discount a_discount_dgreen">
                    <div class="a_discount_label">скидка до</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>

            <div class="action_small_wrapper">
                <div class="a_image">
                    <a href="#"><img src="/images/actions/action4.jpg"/></a>
                </div>
                <div class="a_description">Целый день посещения аквапарка «Фэнтази Парк»</div>
                <div class="a_price"><span class="a_price_old">900</span> <span class="a_price_new">1700 руб.</span></div>

                <div class="a_discount a_discount_ping">
                    <div class="a_discount_label">скидка до</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>

            <div class="action_small_wrapper first">
                <div class="a_image">
                    <a href="#"><img src="/images/actions/action5.jpg"/></a>
                </div>
                <div class="a_description">7 дней проживания с питанием в санатории «Солнечный»...</div>
                <div class="a_price"><span class="a_price_old">6000</span> <span class="a_price_new">3000 руб.</span></div>

                <div class="a_discount a_discount_dorange">
                    <div class="a_discount_label">скидка до</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>

            <div class="action_small_wrapper">
                <div class="a_image">
                    <a href="#"><img src="/images/actions/action6.jpg"/></a>
                </div>
                <div class="a_description">Полный курс обучения вождению автомобиля в автошколе при МГТУ...</div>
                <div class="a_price"><span class="a_price_old">900</span> <span class="a_price_new">1700 руб.</span></div>

                <div class="a_discount a_discount_borange">
                    <div class="a_discount_label">скидка до</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>

            <div class="action_small_wrapper action_small_info">
                <p class="large">У вас на сайте или в магазине часто<br/>проходят розыгрыши ценных подарков?</p>

                <p>Подключитесь к GM, у нас есть те,<br/>кто с радостью примет в них участие!</p>

                <p>Благодаря полностью автоматизированному<br/>сервису конкурсов, на это уйдет несколько минут</p>
            </div>

            <div class="action_small_wrapper first">
                <div class="a_image">
                    <a href="#"><img src="/images/actions/action5.jpg" height="100"/></a>
                </div>
                <div class="a_description">7 дней проживания с питанием в санатории «Солнечный»...</div>
                <div class="a_price"><span class="a_price_old">6000</span> <span class="a_price_new">3000 руб.</span></div>

                <div class="a_discount a_discount_dorange">
                    <div class="a_discount_label">скидка до</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>

            <div class="action_small_wrapper">
                <div class="a_image">
                    <a href="#"><img src="/images/actions/action5.jpg" height="100" width="285"/></a>
                </div>
                <div class="a_description">7 дней проживания с питанием в санатории «Солнечный»...</div>
                <div class="a_price"><span class="a_price_old">6000</span> <span class="a_price_new">3000 руб.</span></div>

                <div class="a_discount a_discount_dorange">
                    <div class="a_discount_label">скидка до</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="action_more_btn">
            <div class="action_more_btn_wrapper">
                <button>Показать еще</button>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#counter').countdown({
                timestamp: '<?php echo $data['end']; ?>000',
                start: '<?php
        if (time() < mktime(0, 0, 0, 7, 1, 2015)) {# STOP
            $time = $data['created'];
        }else{$time = time();}echo $time;
        ?>000',
                callback: function (a, b, c, d, timer) {
                    var day = '';
                    if (a) {
                        day = ' <span class="a_time_days">' + a + ' ' + declOfNum(a, ['день', 'дня', 'дней']) + '</span> ';
                    }
                    if (b < 10) {
                        b = '0' + b;
                    }
                    if (c < 10) {
                        c = '0' + c;
                    }
                    if (d < 10) {
                        d = '0' + d;
                    }
                    $('#a_time').html('<span>До конца акции</span>' + day + '<span class="a_time_time">' + b + ':' + c + ':' + d + '</span>');
                    <?php
                    if (time() < mktime(0, 0, 0, 7, 1, 2015)) { //STOP
                        echo 'clearInterval(timer);';
                    }
                    ?>
                }
            });
        });
    </script>
    <!--main-->
<?php
footer();
