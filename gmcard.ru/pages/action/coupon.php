<?php

use GM\GMCard\DAO\Action;
use GM\Modules\CouponPrinter\CouponPrinter;
use GM\Modules\Processing\Processing;
use GM\Modules\Processing\ProcessingError;

date_default_timezone_set("Europe/Moscow");
$action_id = @$SYSTEM['post']['action_id'];
$first_name = $SYSTEM['post']['first_name'];
$last_name = $SYSTEM['post']['last_name'];
$phone = str_replace([" ", "-", "(", ")"], "", $SYSTEM['post']['phone']);
$uid = @$USER['id'];

try {
    (new Processing())->request('reserve', ['action_id' => $action_id, 'first_name' => $first_name, 'last_name' => $last_name, 'phone' => $phone, 'uid' => $uid]);
    $action = Action::getById($action_id);
    CouponPrinter::printClientCoupon($action->getCost()->getFee(), $action_id, date("d.m.Y", $action->getEnd()), $action->getTitle(), "$first_name $last_name", $phone);
} catch (ProcessingError $error){
    header("Location: http://gmcard.ru/action/action?id=$action_id");
}