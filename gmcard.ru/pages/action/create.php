<?php

use GM\GMCard\DAO\Action;

$q = query("SELECT * FROM `tariff`;");
$tariffs = $q['rows'];

$prices = array();

foreach($tariffs as $tariff){
    $prices[$tariff['level']] = floatval($tariff['competition_price']);
}

$_USER = $USER;
$price = $prices[$USER['level']];
$edit = false;
if (isset($_GET['edit'])) {
    $action_id = (int)$_GET['edit'];
    $action_edit = Action::getById($action_id);
    $edit = true;
	if(($_USER['id']!=41 && $_USER['id']!=19279) || !$action_edit){
		if ($action_edit->getUserId() != $_USER['id']) {
			redirect('/action/my');
		}
	}
} else {
    $action_edit = new Action();
}

if (isset($_GET['delete'])) {
	deleteAction($_GET['delete']);
	redirect('/action/my');
}

if (isset($_GET['up'])) {
	uppingAction($_GET['up']);
	redirect('/action/my');
}

//$USER['money'] = 9999999;
header_('Создание акции', '', '', ''
    . '<script type="text/javascript" src="/js/vendor/jquery.ui.widget.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.fileupload.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>'
    . '<link rel="stylesheet" href="/css/chosen.css">'
    . '<script src="/js/chosen.jquery.js" type="text/javascript"></script>'
    . '<script src="/js/chosen.ajaxaddition.jquery.js" type="text/javascript"></script>
	<link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
	<script type="text/javascript" src="/fancybox/jquery.fancybox.js"></script>');

function is_url($url)
{
    $url = trim($url, '/');
    return preg_match('|^http(s)?://[а-яa-z0-9-_]+(.[а-яa-z0-9-_]+)+(.[а-яa-z0-9-_]+)(:[0-9_]+)?(/.)?$|iu', $url);
}

if (isAjax()) {
    $json = array();
    $errorsArray = [];
    $errorsString = "";

    if (isset($SYSTEM['get']['img'])) {
        echo prepareImg($SYSTEM['post']);
        exit();
    }
    if (isset($SYSTEM['get']['upload'])) {
        $file = $_FILES['file'];
        $info = getimagesize($file["tmp_name"]);
        if(!preg_match("/^image/", $info['mime'])){
            http_response_code(400);
            exit(json_encode(array('error' => 'Загруженный файл имеет неподдерживаемый формат.')));
        }

        $json['file'] = 'tmp/' . time() . '.' . getExtension($file["name"]);
        if (isset($SYSTEM['get']['dop'])) {
            $_ = 'tmp/' . time() . '.jpg';
            move_uploaded_file($file["tmp_name"], $json['file']);
            # create_img($json['file'], $_, 960, 529);
            #$json['file'] = $_;
        } else {
            $_ = 'tmp/' . time() . '.jpg';
            move_uploaded_file($file["tmp_name"], $json['file']);

            #create_img($json['file'], $_, 960, 529);
            #$json['file'] = $_;
        }
#        create_thumbnail($json['file'], $json['file'], 960, 529, "height");

        exit(json_encode($json));
    }

    $preview = $SYSTEM['post']['preview'];
    if (!$preview && !$SYSTEM['post']['agreement']) {
        $errorsString = 'Необходимо принять соглашение<br/>';
    }

    $action = new Action($SYSTEM['post'], false);
    if(($id = $action->save($errorsArray, $errorsString, $preview)) === false) {
        if(!empty($errorsArray)){
            $json["errors"] = $errorsArray;
            if ($preview) {
                $json['errors']['#previewAction'] = array('Найдены ошибки', 'bottom');
            }
        }
        if(!empty($errorsString)){
            if(empty($errorsArray)){
                $json["errors"] = array();
            }
            if ($preview) {
                $json['errors']['#previewAction'] = array('Найдены ошибки', 'bottom');
            } else {
                $json['errors']['#runAction'] = array($errorsString . 'Найдены ошибки', 'bottom');
            }
        }
    } else {
        if ($preview) {
            $time = time();
            file_put_contents('tmp/' . $time . '.txt', serialize($SYSTEM['post']));
            $json['eval'][] = 'window.open("http://gmcard.ru/action/preview?token=' . $time . '");';
        } else {
            $json['eval'][] = 'window.location.href="/action/action?id=' . $id . '"';
        }
    }
    exit(json_encode($json));
}
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <h1>Запустить акцию</h1>

            <form id="formAction" action="" method="POST">
                <?php if($edit){?>
                    <input name="action_id" type="hidden" value="<?php echo $action_edit->getActionId()?>" />
                <?php } ?>
                <div class="content_block rounded_8">
                    <h3 class="first">Название акции</h3>
                    <input autocomplete="off" type="text" name="title" class="inp inp_full rounded_3" placeholder="Введите заголовок" maxlength="64" value="<?php echo $action_edit->getTitle(); ?>"/>

                    <?php if($USER['id'] == 41){?>
                        <input type="checkbox" name="special_condition" value="1"<?php if($action_edit->isSpecialCondition())echo" checked";?>/>
                    <?php } else { ?>
                        <input type="hidden" name="special_condition" value="<?php echo (int)$action_edit->isSpecialCondition(); ?>" />
                    <?php }?>

                    <div style="position: relative;float: right;margin-top: -35px;text-align: right;padding-right: 10px;">
                        <span id="countS" style="background-color: #0066cc;color: #FFF;padding: 5px;">64</span>
                    </div>

                    <h3>
                        Вы можете выбрать до 3-х различных фотографий для загрузки<br/>
                        <span class="details">(Для загрузки обязательна только одна фотография, остальные по желанию)</span>
                        <br/>
                        <div style="text-align: center;">
                            <a href="#" onclick="openm();">У меня нет фото, что делать?</a>
                        </div>
                    </h3>

                    <div>
                        <div class="action_photo_block">
                            <button class="btn rounded_8" img-src="/images/test_image.png" id="chooseImage1">Выберите файл</button>
                            <button style="display: none;" img-src="/images/avatar-main.jpg" id="chooseImage1_"></button>
                            <span class="photo_file">Файл не выбран</span>
                            <span class="photo_req">Обложка акции<br/>(обязательно)</span>
                        </div>

                        <div class="action_photo_block">
                            <button class="btn rounded_8" id="chooseImage2">Выберите файл</button>
                            <button style="display: none;" img-src="/images/avatar-main.jpg" id="chooseImage2_"></button>
                            <span class="photo_file">Файл не выбран</span>
                            <span class="photo_req">Не обязательно</span>
                        </div>

                        <div class="action_photo_block last">
                            <button class="btn rounded_8" id="chooseImage3">Выберите файл</button>
                            <button style="display: none;" img-src="/images/avatar-main.jpg" id="chooseImage3_"></button>
                            <span class="photo_file">Файл не выбран</span>
                            <span class="photo_req">Не обязательно</span>
                        </div>
                        <div class="clear"></div>
                        <input id="fileupload" type="file" name="file" style="display: none;">
                        <input id="fileupload2" type="file" name="file" style="display: none;">
                        <input id="chooseImage1__" type="hidden" name="chooseImage1__" value="<?php echo @$action_edit->getImages()[1]; ?>">
                        <input id="chooseImage1___" type="hidden" name="chooseImage1___" value="<?php echo @$action_edit->getImages()[11]; ?>">
                        <input id="chooseImage2__" type="hidden" name="chooseImage2__" value="<?php echo @$action_edit->getImages()[2]; ?>">
                        <input id="chooseImage3__" type="hidden" name="chooseImage3__" value="<?php echo @$action_edit->getImages()[3]; ?>">
                        <br/>
                        <span class="details">Вы можете выбрать дополнительные фотографии(необязательно):</span>

                        <div class="attachments input">
                            <a href="#" class="dop" data-img="/<?php echo @$action_edit->getImages()[4]; ?>"><img src="/<?php echo @$action_edit->getImages()[4] ? @$action_edit->getImages()[4] : 'images/no-image.png'; ?>"/></a>
                            <a href="#" class="dop" data-img="/<?php echo @$action_edit->getImages()[5]; ?>"><img src="/<?php echo @$action_edit->getImages()[5] ? @$action_edit->getImages()[5] : 'images/no-image.png'; ?>"/></a>
                            <a href="#" class="dop" data-img="/<?php echo @$action_edit->getImages()[6]; ?>"><img src="/<?php echo @$action_edit->getImages()[6] ? @$action_edit->getImages()[6] : 'images/no-image.png'; ?>"/></a>
                            <a href="#" class="dop" data-img="/<?php echo @$action_edit->getImages()[7]; ?>"><img src="/<?php echo @$action_edit->getImages()[7] ? @$action_edit->getImages()[7] : 'images/no-image.png'; ?>"/></a>
                            <a href="#" class="dop" data-img="/<?php echo @$action_edit->getImages()[8]; ?>"><img src="/<?php echo @$action_edit->getImages()[8] ? @$action_edit->getImages()[8] : 'images/no-image.png'; ?>"/></a>
                        </div>
                        <div style="display: none" id="inpForImg"></div>
                        <br/>
                        <span class="details">И ссылку на видео youtube(необязательно):</span>
                        <input autocomplete="off" type="text" name="yt" class="inp inp_full rounded_3" placeholder="Пример: https://www.youtube.com/embed/h9eWY6m2HAM" maxlength="64" value="<?php echo @$action_edit->getImages()[12]; ?>"/>
                    </div>

                </div>

                <div class="content_block rounded_8">
                    <h3 class="first">Описание акции:</h3>
                    <textarea name="condition" class="inp inp_full rounded_3" rows="5"
                              placeholder="Опишите свою акцию более детально, что вы продаете или предлагаете, подарок, а также прочие условия..."><?php echo $action_edit->getCondition(); ?></textarea>

                    <h3>
                        Укажите ссылки на ваш сайт, а также на ваши сообщества в социальных сетях:<br/>
                        <span class="details">(Не обязательный пункт)</span>
                    </h3>

                    <div class="site_block">
                        <div class="site_block_label"><label>Наш сайт: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[main]" class="inp_full" type="text" value="<?php echo $action_edit->getSite()->getMain(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в GM: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[gm]" class="inp_full" type="text" value="<?php echo $action_edit->getSite()->getGm(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы ВКонтакте: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[vk]" class="inp_full" type="text" value="<?php echo $action_edit->getSite()->getVk(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы на Facebook: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[fb]" class="inp_full" type="text" value="<?php echo $action_edit->getSite()->getFb(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Одноклассниках: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[ok]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getOk(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Twitter: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[tw]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getTw(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы на Youtube: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[yt]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getYt(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Instagram: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[ig]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getIg(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Pinterest: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[pr]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getPr(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в LinkedIn: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[linkedin]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getLinkedin(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Foursquare: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[foursquare]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getFoursquare(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Tumblr: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[tumblr]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getTumblr(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Google+: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[googleplus]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getGoogleplus(); ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Email: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[email]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $action_edit->getSite()->getEmail(); ?>"></div>
                    </div>

                </div>

                <div class="content_block rounded_8">
                    <div class="column_half">
                        <h3 class="first">
                            Укажите на какое количество дней запускается акция, максимум 99 дней:<br/>
                            <span class="details">(по умолчанию 10 дней)</span>
                        </h3>

                        <div class="days_block">
                            <img src="/images/ico_clock.png" />
                            <?php $days = ceil(($action_edit->getEnd() - time()) / (60 * 60 * 24));
                            if($days < 0)$currentPrice = 0; else $currentPrice = $price * $days;
                            if($action_edit->isDesire())$currentPrice *= Action::BANNER_MULTIPLIER;?>
                            <input type="hidden" name="currentPrice" value="<?php echo $currentPrice;?>" />
                            <input id="days" name="day" type="number" class="inp inp-days rounded_3" size="2" value="<?php if($days < 0) echo 0; else echo $days; ?>" min="1" max="99" />
                            <label for="days">дней</label>
                        </div>
                    </div>

                    <div class="column_half">
                        <h3 class="first">
                            Желаете ли вы запустить акцию в баннере, в самом верху сайта главной страницы?<br/>
                            <span class="details">(От этого зависит стоимость запуска акции, по умолчанию стоит “Да, желаю”)</span>
                        </h3>

                        <div class="styled_checkbox desire_checkbox">
                            <label for="desire_y">Да, желаю</label> <input type="radio" id="desire_y" name="desire" value="1" <?php echo $action_edit->isDesire() ? 'checked' : ''; ?>>
                            <label for="desire_n">Нет, не желаю</label> <input type="radio" id="desire_n" name="desire" value="0" <?php echo !$action_edit->isDesire() ? 'checked' : ''; ?>>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="content_block rounded_8 cb_worktime">
                    <h3 class="first">Укажите график работы / телефоны / сайт при наличии / сумму скидки</h3>

                    <div class="column_half left">
                        <h4>График работы:</h4>
                        <textarea name="address[graph]" class="inp rounded_3" id="time_textarea" placeholder="пн-сб: с 9:00 до 20:00 вс: с 10:00 до 18:00" rows="2"><?php echo $action_edit->getAddress()->getGraph(); ?></textarea>

                        <h4>Телефоны:</h4>
                        <input name="address[phone]" type="text" class="inp rounded_3" placeholder="8-800-2000-600-700" value="<?php echo $action_edit->getAddress()->getPhone(); ?>">

                        <h4>Адрес:</h4>
                        <input name="address[address]" type="text" class="inp rounded_3" placeholder="г.Москва, пр. Мира, 82" value="<?php echo $action_edit->getAddress()->getAddress(); ?>">

                        <h4>Сайт:</h4>
                        <input name="address[site]" type="text" class="inp rounded_3" placeholder="www.example.com" value="<?php echo $action_edit->getAddress()->getSite(); ?>">

                        <div class="dopAddress hide">
                            <h3>Дополнительный адрес (необязательно)</h3>
                            <h4>График работы:</h4>
                            <textarea name="daddress[0][graph]" class="inp rounded_3" id="time_textarea" placeholder="пн-сб: с 9:00 до 20:00 вс: с 10:00 до 18:00" rows="2"><?php if(isset($action_edit->getDaddress()[0]))echo $action_edit->getDaddress()[0]->getGraph(); ?></textarea>
                            <h4>Телефоны:</h4>
                            <input name="daddress[0][phone]" type="text" class="inp rounded_3" placeholder="8-800-2000-600-700" value="<?php if(isset($action_edit->getDaddress()[0]))echo $action_edit->getDaddress()[0]->getPhone(); ?>">

                            <h4>Адрес:</h4>
                            <input name="daddress[0][address]" type="text" class="inp rounded_3" placeholder="г.Москва, пр. Мира, 82" value="<?php if(isset($action_edit->getDaddress()[0]))echo htmlspecialchars($action_edit->getDaddress()[0]->getAddress()); ?>">

                            <h4>Сайт:</h4>
                            <input name="daddress[0][site]" type="text" class="inp rounded_3" placeholder="www.example.com" value="<?php if(isset($action_edit->getDaddress()[0]))echo $action_edit->getDaddress()[0]->getSite(); ?>">
                            <?php if(isset($action_edit->getDaddress()[0])){?>
                                <input name="daddress[0][longitude]" type="hidden" value="<?php echo $action_edit->getDaddress()[0]->getLongitude();?>" />
                                <input name="daddress[0][latitude]" type="hidden" value="<?php echo $action_edit->getDaddress()[0]->getLatitude();?>" />
                            <?php } ?>
                        </div>
                        <div class="dopAddress hide">
                            <h3>Дополнительный адрес (необязательно)</h3>
                            <h4>График работы:</h4>
                            <textarea name="daddress[1][graph]" class="inp rounded_3" id="time_textarea" placeholder="пн-сб: с 9:00 до 20:00 вс: с 10:00 до 18:00" rows="2"><?php if(isset($action_edit->getDaddress()[1]))echo $action_edit->getDaddress()[1]->getGraph(); ?></textarea>
                            <h4>Телефоны:</h4>
                            <input name="daddress[1][phone]" type="text" class="inp rounded_3" placeholder="8-800-2000-600-700" value="<?php if(isset($action_edit->getDaddress()[1]))echo $action_edit->getDaddress()[1]->getPhone(); ?>">

                            <h4>Адрес:</h4>
                            <input name="daddress[1][address]" type="text" class="inp rounded_3" placeholder="г.Москва, пр. Мира, 82" value="<?php if(isset($action_edit->getDaddress()[1]))echo htmlspecialchars($action_edit->getDaddress()[1]->getAddress()); ?>">

                            <h4>Сайт:</h4>
                            <input name="daddress[1][site]" type="text" class="inp rounded_3" placeholder="www.example.com" value="<?php if(isset($action_edit->getDaddress()[1]))echo $action_edit->getDaddress()[1]->getSite(); ?>">
                            <?php if(isset($action_edit->getDaddress()[1])){?>
                                <input name="daddress[1][longitude]" type="hidden" value="<?php echo $action_edit->getDaddress()[1]->getLongitude();?>" />
                                <input name="daddress[1][latitude]" type="hidden" value="<?php echo $action_edit->getDaddress()[1]->getLatitude();?>" />
                            <?php } ?>
                        </div>
                        <div class="dopAddress hide">
                            <h3>Дополнительный адрес (необязательно)</h3>
                            <h4>График работы:</h4>
                            <textarea name="daddress[2][graph]" class="inp rounded_3" id="time_textarea" placeholder="пн-сб: с 9:00 до 20:00 вс: с 10:00 до 18:00" rows="2"><?php if(isset($action_edit->getDaddress()[2]))echo $action_edit->getDaddress()[2]->getGraph(); ?></textarea>
                            <h4>Телефоны:</h4>
                            <input name="daddress[2][phone]" type="text" class="inp rounded_3" placeholder="8-800-2000-600-700" value="<?php if(isset($action_edit->getDaddress()[2]))echo $action_edit->getDaddress()[2]->getPhone(); ?>">

                            <h4>Адрес:</h4>
                            <input name="daddress[2][address]" type="text" class="inp rounded_3" placeholder="г.Москва, пр. Мира, 82" value="<?php if(isset($action_edit->getDaddress()[2]))echo htmlspecialchars($action_edit->getDaddress()[2]->getAddress()); ?>">

                            <h4>Сайт:</h4>
                            <input name="daddress[2][site]" type="text" class="inp rounded_3" placeholder="www.example.com" value="<?php if(isset($action_edit->getDaddress()[2]))echo $action_edit->getDaddress()[2]->getSite(); ?>">
                            <?php if(isset($action_edit->getDaddress()[2])){?>
                                <input name="daddress[2][longitude]" type="hidden" value="<?php echo $action_edit->getDaddress()[2]->getLongitude();?>" />
                                <input name="daddress[2][latitude]" type="hidden" value="<?php echo $action_edit->getDaddress()[2]->getLatitude();?>" />
                            <?php } ?>
                        </div>
                        <a href="#" id="addAddress">Добавить еще один адрес</a>

                    </div>
                    <div class="column_half right">
                        <div class="price_block">
                            <!-- Rounded switch -->
                            <span class="span-switch selected discount">Скидка</span>
                            <label class="switch">
                                <input name="cost[type]" value="cashback" type="checkbox"<?php if($action_edit->getCost()->isCashback())echo" checked";?> />
                                <div class="slider round"></div>
                            </label>
                            <span class="span-switch cashback">Кэшбэк</span>
                        </div>
                        <div class="price_block action_cost discount"<?php if($action_edit->getCost()->isCashback()){?> style="display: none;"<?php }?>>
                            <div class="price_block_label"><label>Ваш товар/услуга стоит:</label></div>
                            <div class="price_block_value"><input name="cost[cost]" type="number" class="inp rounded_3" value="<?php echo $action_edit->getCost()->getCost(); ?>"<?php if($action_edit->getCost()->isCashback()){?> disabled<?php }?> /> руб</div>
                        </div>
                        <div class="price_block action_cost discount"<?php if($action_edit->getCost()->isCashback()){?> style="display: none;"<?php }?>>
                            <div class="price_block_label"><label>Вы планируете скидку в</label></div>
                            <div class="price_block_value">
                                <input name="cost[discount]" type="number" class="inp rounded_3" value="<?php echo $action_edit->getCost()->getDiscount(); ?>"<?php if($action_edit->getCost()->isCashback()){?> disabled<?php }?> />
                                <select style="font-size: 16px !important;" name="cost[discount_type]" class="procentik"<?php if($action_edit->getCost()->isCashback()){?> disabled<?php }?>>
                                    <option value="percent"<?php if($action_edit->getCost()->isPercent())echo " selected";?>>%</option>
                                    <option value="raw"<?php if($action_edit->getCost()->isRaw())echo " selected";?>>руб</option>
                                </select>
                            </div>
                        </div>
                        <div class="price_block action_cost discount"<?php if($action_edit->getCost()->isCashback()){?> style="display: none;"<?php }?>>
                            <div class="price_block_label"><label>Стоимость вашего товара/ услуги со скидкой составит</label></div>
                            <div class="price_block_value"><input name="new" disabled type="number" class="inp rounded_3" value="<?php echo $action_edit->getCost()->getFinalCost(); ?>"/> руб</div>
                        </div>
                        <p class="opis action_cost cashback"<?php if($action_edit->getCost()->isDiscount()){?> style="display: none;"<?php }?>>
                            <label><input name="cost[fee]" type="number" class="inp rounded_3" value="<?php echo $action_edit->getCost()->getFee();?>" min="0" max="100"<?php if($action_edit->getCost()->isDiscount()){?> disabled<?php }?> /><span>% от<br>суммы<br>чека</span></label>
                            Какой % от суммы чека вы обещаете вернуть клиенту, на его счет, если он воспользуется вашей услугой? Минимум 1%<br />
							<br /> <b>ВАЖНО!</b> <br />
							На % от суммы чека, который вы обещаете вернуть клиенту - GM наложит свой дополнительный %. Изначально запуск в данном режиме бесплатен, а списание средств с вашего счета произойдет только тогда, когда клиент воспользуется вашей услугой и рассчитается с вами.
                        </p>
                        <a class="action_cost cashback"<?php if($action_edit->getCost()->isDiscount()){?> style="display: none;"<?php }?> href="https://gm1lp.ru/help/about_gmcard" style="display:inline-block;font-size: 14px;margin-bottom: 30px;" target="_blank">Подробнее о суммах и комиссиях</a>
                        <p class="opis action_cost cashback"<?php if($action_edit->getCost()->isDiscount()){?> style="display: none;"<?php }?>>Премиум аккаунт <a class="action_cost cashback" href="https://gm1lp.ru/page/tariff" target="_blank">и другие тарифы</a></p>
                        <p class="dop action_cost cashback"<?php if($action_edit->getCost()->isDiscount()){?> style="display: none;"<?php }?>>Дополнительно</p>
                        <p class="min_dop action_cost cashback"<?php if($action_edit->getCost()->isDiscount()){?> style="display: none;"<?php }?>>
                            (Мы не рекомендуем заполнять данное поле, т.к вы заранее
                            должны учесть этот момент. Если клиент пришел к вам с купоном,
                            значит, он уже решил что-то у вас купить. В любом случае мы
                            предоставляем вам право выбора)
                        </p>
                        <div class="main_ck action_cost cashback"<?php if($action_edit->getCost()->isDiscount()){?> style="display: none;"<?php }?>>
                            <label>
                                <input type="checkbox" name="" checked disabled >
                                <span style="position: relative;bottom: 13px;"></span>
                            </label>
                            <span style="display: inline-block">
                                Купон действует<br>
                                при заказе от:
                            </span>
                            <label style="float: right;">
                                <input name="cost[min_order_sum]" type="number" class="inp rounded_3" value="<?php echo $action_edit->getCost()->getMinOrderSum();?>" min="0" style="padding: 12px 3px;" <?php if($action_edit->getCost()->isDiscount()){?> disabled<?php }?>/>р.
                            </label>
                        </div>
                        <p class="opsianie_bottom">
                            <span>* -</span>
                            Если вы выбрали режим купона, то не забудьте<br>
                            описать условие более подробно в описании акции.<br>
                            В режими скидки рассказывается о самой<br>
                            компании
                        </p>
                    </div>
                    <div class="clear"></div>

                    <h3>
                        Если ваша акция доступна только в определенные дни и определенное время суток, то укажите:<br />
                        <span class="details">(отсчет таймера не прекратится, но пользователи будут видеть, что акция доступна
                        в определенные дни и определенное время суток)</span>
                    </h3>
                    <div class="new_day">
                        <label class="new_label1">
                            <input type="checkbox" name="days_enabled" value="1"<?php if($action_edit->isDaysEnabled())echo" checked";?> onchange="if(this.checked)document.getElementsByName('days[]').forEach(function(i){i.disabled = false;}); else document.getElementsByName('days[]').forEach(function(i){i.disabled = true;});"/>
                            <span></span>
                            Акция доступна в
                        </label>
                        <?php foreach([1 => "ПН", 2 => "ВТ", 3 => "СР", 4 => "ЧТ", 5 => "ПТ", 6 => "СБ", 7 => "ВС"] as $index => $name){ ?>
                            <label class="pn">
                                <input type="checkbox" name="days[]" value="<?php echo $index;?>"<?php if(in_array($index, $action_edit->getDays()))echo" checked";?><?php if(!$action_edit->isDaysEnabled())echo" disabled";?> />
                                <span><?php echo $name; ?></span>
                            </label>
                        <?php }?>
                    </div>
                    <div class="new_day new_day2">
                        <label class="new_label1">
                            <input type="checkbox" name="time[enabled]" value="1"<?php if($action_edit->getTime()->isEnabled())echo " checked";?>  onchange="if(this.checked)Array.prototype.slice.call(document.getElementsByClassName('vremya')).forEach(function(i){i.disabled = false;}); else Array.prototype.slice.call(document.getElementsByClassName('vremya')).forEach(function(i){i.disabled = true;});" />
                            <span></span>
                            Акция доступна с
                        </label>
                        <label>
                            <input type="number" name="time[fromHour]" class="vremya" placeholder="00" min="0" max="23" value="<?php echo $action_edit->getTime()->getFromHour();?>"<?php if(!$action_edit->getTime()->isEnabled())echo " disabled";?>> :
                            <input type="number" name="time[fromMinute]" class="vremya" placeholder="00" min="0" max="59" value="<?php echo $action_edit->getTime()->getFromMinute();?>"<?php if(!$action_edit->getTime()->isEnabled())echo " disabled";?>> до
                            <input type="number" name="time[toHour]" class="vremya" placeholder="00" min="0" max="23" value="<?php echo $action_edit->getTime()->getToHour();?>"<?php if(!$action_edit->getTime()->isEnabled())echo " disabled";?>> :
                            <input type="number" name="time[toMinute]" class="vremya" placeholder="00" min="0" max="59" value="<?php echo $action_edit->getTime()->getToMinute();?>"<?php if(!$action_edit->getTime()->isEnabled())echo " disabled";?>></label>

                    </div>
                    <h3>
                        Укажите город, в котором будет проходить данная акция, если акция будет проходить в нескольких городах,
                        то укажите их по порядку <span class="details">(не более 20)</span>
                    </h3>
                    <select name="city[]" data-placeholder="Город или несколько городов..." class="chosen-select city" multiple style="width:728px;" tabindex="4">
                        <?php
                        if ($edit) {
                            foreach ($action_edit->getCity() as $cid) {

                                $_ = getCityById($cid);
                                echo '<option value="' . $cid . '" selected>' . $_['text'] . '</option>';
                            }
                        }
                        ?>
                    </select>

                    <h3>
                        Укажите ТРЦ, в котором будет проходить данная акция, если акция будет проходить в нескольких ТРЦ, то укажите их по порядку <span class="details">(не более 20)</span>. Необязательный пункт.
                    </h3>
                    <select name="trc[]" data-placeholder="Список ТРЦ" class="chosen-select trc" multiple style="width:728px;" tabindex="4">
                        <?php
                        if ($edit) {
                            foreach ($action_edit->getTrc() as $cid) {

                                $_ = getTrc($cid);
                                echo '<option value="' . $cid . '" selected>' . $_['title'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="content_block rounded_8"><h3 class="first"> Выберите наиболее подходящую вам категорию, в которой будет отражаться ваша акция: <span class="details">(Обязательный пункт, минимум одна категория с подкатегорией)</span></h3>

                    <div class="column_third column_categories">
                        <div class="categories_checkboxes styled_checkbox">
                            <?php foreach (getGroups() as $index => $group){?>
                                <label for="category_<?php echo $index;?>"><?php echo $group['label'];?></label>
                                <input type="radio" class="rounded_5" id="category_<?php echo $index;?>" name="categories" value="<?php echo $index;?>">
                            <?php } ?>
                        </div>
                    </div>

                    <?php foreach (getGroups() as $index => $group){?>
                        <div id="tab_<?php echo $index;?>" class="tab_hide">
                            <?php
                            $totalHeight = 0;
                            $firstColumn = [];
                            $secondColumn = [];
                            $secondColumnHeight = 0;
                            $secondColumnFull = false;
                            foreach($group['categories'] as $category){
                                $totalHeight += count($category['subcategories']) + 1;
                            }
                            foreach(array_reverse($group['categories']) as $category) {
                                if (!$secondColumnFull && 2 * ($secondColumnHeight + count($category['subcategories']) + 1) < $totalHeight) {
                                    $secondColumnHeight += count($category['subcategories']) + 1;
                                    $secondColumn[] = $category;
                                } else {
                                    $secondColumnFull = true;
                                    $firstColumn[] = $category;
                                }
                            } ?>
                            <div class="column_third subcategory">
                                <?php foreach(array_reverse($firstColumn) as $category){?>
                                    <div class="accessories_checkboxes styled_checkbox">
                                        <label for="cat<?php echo $category['index'];?>"><?php echo $category['label'];?></label>
                                        <input type="checkbox" class="checkbox_category" name="cat[]" id="cat<?php echo $category['index'];?>" value="<?php echo $category['index'];?>"/>
                                        <?php foreach($category['subcategories'] as $subcategory){?>
                                            <label for="cat<?php echo $subcategory['index'];?>" data-cat="<?php echo $index;?>"><?php echo $subcategory['label'];?></label>
                                            <input type="checkbox" name="cat[]" id="cat<?php echo $subcategory['index'];?>" value="<?php echo $subcategory['index'];?>"/>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="column_third subcategory">
                                <?php foreach(array_reverse($secondColumn) as $category){?>
                                    <div class="accessories_checkboxes styled_checkbox">
                                        <label for="cat<?php echo $category['index'];?>"><?php echo $category['label'];?></label>
                                        <input type="checkbox" class="checkbox_category" name="cat[]" id="cat<?php echo $category['index'];?>" value="<?php echo $category['index'];?>"/>
                                        <?php foreach($category['subcategories'] as $subcategory){?>
                                            <label for="cat<?php echo $subcategory['index'];?>" data-cat="<?php echo $index;?>"><?php echo $subcategory['label'];?></label>
                                            <input type="checkbox" name="cat[]" id="cat<?php echo $subcategory['index'];?>" value="<?php echo $subcategory['index'];?>"/>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="clear"></div>
                </div>

                <div class="action_result">
                    <div class="column_third">
                        <div class="amount_label">Ваш баланс</div>
                        <div class="amount_value"><span id="actMyBalance"><?php echo $USER['money']; ?></span> руб.</div>
                    </div>
                    <div class="column_third">
                        <div class="amount_label">Стоимость акции составит</div>
                        <div class="amount_value"><span id="actCost">9999</span> руб.</div>
                    </div>
                    <div class="column_third column_preview">
                        <button id="previewAction" class="btn btn-icon btn-browser rounded_8"><span>Предпросмотр акции</span></button>
                    </div>
                    <div class="clear"></div>

                    <div class="styled_checkbox checkbox_agreement">
                        <label for="agreement">С <a href="http://gm1lp.ru/help/agree" target="_blank">условиями пользования сервисом</a> и стоимостью ознакомлен и полностью согласен</label> <input type="checkbox" name="agreement" id="agreement">
                    </div>

                    <button id="runAction" class="btn inp_full btn-big rounded_8">Запустить акцию</button>
                </div>
                <input type="hidden" id="preview" name="preview" value="0"/>
            </form>
            <style>
                .closes {
                    top: 20px!important;
                }
            </style>
            <?php include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/no_photo_popup.php';?>
        </div>

        <div class="right_block">
            <ul class="right_menu">
                <li><a class="rounded_3" href="/user/lk">Личный кабинет</a></li>
                <li class="active"><a class="rounded_3" href="/action/create">Запустить акцию</a></li>
                <li><a class="rounded_3" href="/action/my">Мои акции</a></li>
                <li><a class="rounded_3" href="/action/my_coupons">Мои акции с купонами</a></li>
                <li><a class="rounded_3" href="/my_coupons">Мои купоны</a></li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="/js/action_create.js"></script>
    <script>
        var curImage = null;
        var clickForPopup = false;
        //var
        $(document).ready(function () {
            $('#addAddress').click(function () {
                $('.dopAddress.hide:first').removeClass('hide');

                if (!$('.dopAddress.hide').size()) {
                    $(this).hide();
                }
                return false;
            });
            $('input[name="title"]').keyup(function () {
                var v = $(this).val();
                $('#countS').text(64 - v.length);
            });
            $('form#formAction').submit(function () {
                $('#inpForImg').html('');
                var j = 0;
                $('.attachments a').each(function (i) {
                    if ($(this).attr('data-img')) {
                        $('#inpForImg').append('<input type="hidden" name="images[' + (j++) + ']" value="' + $(this).attr('data-img') + '" />');
                    }
                });
                $.post('', $(this).serialize(), function (data) {
                    console.log(data);
                    $('#preview').val('0');
                    var errors = parseErrors(data);
                    var success = parseSuccess(data);
                    var eval = parseEval(data);
                }, 'json');
                return false;
            });
            $('#runAction').click(function () {
                $('form#formAction').submit();
                return false;
            });
            $('#previewAction').click(function () {
                $('#preview').val('1');
                $('form#formAction').submit();
                return false;
            });
            var dop = null;
            $('.dop').click(function () {
                dop = $(this);
                $('#fileupload2').click();
                return false;
            });
            $('#fileupload2').fileupload({
                url: '?upload&dop',
                dataType: 'json',
                done: function (e, data) {
                    dop.attr('data-img', '/' + data.result.file);
                    dop.find('img').attr('src', '/' + data.result.file);
                    console.log(data);
                },
                error: function (e,data) {
                    alert(e.responseJSON ? e.responseJSON.error : e.responseText);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
            ////
            $('#fileupload').fileupload({
                url: '?upload',
                dataType: 'json',
                done: function (e, data) {
                    curImage.attr('img-src', '/' + data.result.file);
                    curImage.click();
                    console.log(data);
                },
                error: function (e,data) {
                    alert(e.responseJSON ? e.responseJSON.error : e.responseText);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
            $('#chooseImage1,#chooseImage2,#chooseImage3').click(function () {
                curImage = $('#' + $(this).attr('id') + '_');
                $('#fileupload').click();
                return false;
            });
            $('input[name^="cost"]').change(function () {
                var disc_type = $('select[name="cost[discount_type]"]').val();
                var current = $('input[name="cost[cost]"]');
                var discount = $('input[name="cost[discount]"]');
                var new_ = $('input[name="new"]');
                var c;
                if(disc_type == 'percent'){
                    c = parseFloat(current.val()) * (100 - parseFloat(discount.val())) / 100;
                } else {
                    c = parseFloat(current.val()) - parseFloat(discount.val());
                }
                new_.val(c.toFixed(2));
            });
            $('#desire_y,#desire_n,#days').change(function () {
                cost();
            });
            cost();
            /*$(".chosen-select").chosen({
             disable_search_threshold: 10,
             max_selected_options: 5,
             no_results_text: "Город не найден"
             });*/
            //
            var cities = [];
            $('.chosen-select.city').ajaxChosen({
                dataType: 'json',
                type: 'POST',
                url: '/util/city'
            }, {
                loadingImg: '/images/loading.gif',
                minLength: 1
            });
            $('.chosen-select.trc').ajaxChosen({
                dataType: 'json',
                type: 'POST',
                url: '/util/trc',
                data: {'cities': cities}
            }, {
                generateUrl: function (q) {
                    var cities = $('.chosen-select.city').val();
                    return '/util/trc?cities=' + (cities != null ? cities.join(',') : '');
                },
                loadingImg: '/images/loading.gif',
                minLength: 5
            });
            $('.chosen-select.city').change(function () {
                if ($(this).find('option:selected').size() > 20) {
                    alert('Разрешено указывать до 20 городов');
                    $(this).find('option:selected:last').remove();
                    $('.chosen-select.city').trigger("chosen:updated");
                } else {
                    var cities = $('.chosen-select.city').val();
                    $.post('/util/trc?cities=' + (cities != null ? cities.join(',') : ''), {
                        data: {
                            "q": ' '
                        }
                    }, function (data) {
                        $('.chosen-select.trc option').each(function () {
                            //$(this).remove();
                        });
                        $.each(data.results, function (i, e) {
                            console.log(i, e);
							if($('.chosen-select.trc').find('option[value="'+e.id+'"]').length){

							}else{
								$('.chosen-select.trc').append('<option value="' + e.id + '">' + e.text + '</option>');
							}
                        });
                        $('.chosen-select.trc').trigger("chosen:updated");
                    }, 'json');

                    console.log('test');
                }
            });
            $('.chosen-select.trc+.chosen-container input').click(function () {

                if (!$('.chosen-select.city').val()) {
                    alert('Необходимо выбрать хотя бы один город');
                    $('.chosen-select.trc').trigger("chosen:updated");
                }
            });
            ///

            /*$('.checkbox_category').click(function () {
             if ($(this).hasClass('checked')) {
             $(this).parent().find('li').each(function (i) {
             if (i == 0 || $(this).hasClass('checked')) {
             return;
             }
             $(this).click();
             });
             } else {
             $(this).parent().find('li').each(function (i) {
             if (i == 0 || !$(this).hasClass('checked')) {
             return;
             }
             $(this).click();
             });
             }
             });*/
            $.each($('.clothing_checkboxes.styled_checkbox, .accessories_checkboxes.styled_checkbox'), function () {
                $(this).find('ul li:first').click(function () {
                    var checked = $(this).parent().find('.checked');
                    if (!$(this).hasClass('checked') && checked.size()) {
                        $.each(checked, function () {
                            $(this).click();
                        });
                    }
                    if ($(this).hasClass('checked') && checked.size() <= 1) {
                        $.each($(this).parent().find('li'), function (i) {
                            if (i == 0) {
                                return;
                            }
                            $(this).click();
                        });
                    }
                    console.log(checked.size());
                });
                ///

                $(this).find('ul li:gt(0)').click(function () {
                    var firstLi = $(this).parent().find('li:first');
                    if ($(this).hasClass('checked') && !firstLi.hasClass('checked')) {
                        firstLi.click();
                    }
                    var checked = $(this).parent().find('.checked');
                    if (checked.size() <= 1 && firstLi.hasClass('checked')) {
                        firstLi.click();
                    }
                });
                ////
            });
        })
        ;
        function callbackChoose(a, b, c) {
            var file = curImage.attr('img-src');
            var post = {
                'file': file,
                'coords': b
            };
            $.post('?img', post, function (data) {
                $('#' + a + '_').val(data);
            });
            curImage.next().html('Файл загружен');
            if (c !== undefined) {
                var post = {
                    'file': file,
                    'coords': c,
                    'mini': 0
                };
                $.post('?img', post, function (data) {
                    $('#' + a + '__').val(data);
                });
            }
            //console.log(a, b, c);
        }
        function cost() {
            var summ = parseInt($('#days').val()) * <?php echo $price;?>;
            if ($('[name=desire]:checked').val() > 0) {
                summ = summ * <?php echo Action::BANNER_MULTIPLIER;?>;
            }
            summ -= parseFloat($('[name=currentPrice]').val());
            $('#actCost').html(summ.toFixed(2));
        }
        <?php if($edit):
        echo 'var cats=['.implode(',', $action_edit->getCat()).'];'
        ?>
        $.each(cats, function (e, v) {
            console.log(v);
            $('label[for=cat' + v + ']').click();
        });
        $('.photo_file').text('Файл выбран');
        <?php endif; ?>
    </script>
	<script>
        $(document).ready(function () {
			$("#various1").fancybox({
				'width'				: '500px',
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'closeBtn'          : true,

				'tpl': {
					'wrap'     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner" style="box-sizing: border-box;border: 10px solid gray;background-color: #FFF;"></div></div></div></div>',
					'image'    : '<img class="fancybox-image" src="{href}" alt="" />',
					'iframe'   : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>',
					'error'    : '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
					'closeBtn' : '<a title="Close" class="closes fancybox-item fancybox-close" href="javascript:;"></a>',
					'next'     : '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
					'prev'     : '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>',
					'loading'  : '<div id="fancybox-loading"><div></div></div>'
				}
			});

            $('.action_small_wrapper').hover(function () {
                $('.a_description').show();
                return false;
            }, function () {
                return false;
            });
        });
    </script>
<?php
footer();

