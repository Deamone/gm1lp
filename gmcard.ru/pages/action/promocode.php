<?php

use GM\GMCard\DAO\Action;
use GM\Modules\Processing\Processing;
use GM\Modules\Processing\ProcessingError;

$action_id = @$SYSTEM['post']['action_id'];
$action = Action::getById($action_id);
if($action->getActionId() && @$USER['id'] != $action->getUserId()) {
    header("Location: http://gmcard.ru/action/action?id=$action_id");
    exit;
}

try {
    (new Processing())->request('print', ['first_name' => @$SYSTEM['post']['first_name'], 'last_name' => @$SYSTEM['post']['last_name'],
        'phone' => str_replace([" ", "-", "(", ")"], "", @$SYSTEM['post']['phone']), 'action_id' => $action_id, 'amount' => @$SYSTEM['post']['amount']]);
    die("Промо-код успешно выслан.<br /><a href=\"http://gmcard.ru/action/action?id=$action_id\">Вернуться назад.</a>");
} catch (ProcessingError $error){
    die("Ошибка: {$error->getDescription()}<br /><a href=\"http://gmcard.ru/action/action?id=$action_id\">Вернуться назад.</a>");
}