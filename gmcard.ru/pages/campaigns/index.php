<?php
try {
    $admitad = new AdmitadHelper();
    $all = $admitad->getCampaignsAndCouponsForStartPage();
    renderPage($all);

} catch (Exception $e) {
    die ('ERROR: ' . $e->getMessage());
}

function renderPage($context)
{
    $loader = new Twig_Loader_Filesystem('templates/campaigns');
    $twig = new Twig_Environment($loader);
    $moneyFilter = function ($string) {
        $new_string = $string;
        $new_string = str_replace("USD", "$", $new_string);
        $new_string = str_replace("RUB", "₽", $new_string);
        return $new_string;
    };
    $cashbackFilter = function ($string) {
        $isRangeCashback = strpos($string, "-");
        if ($isRangeCashback) {
            $maxCashback = substr($string, strpos($string, "-") + 1, strlen($string) - 1);
            return 'до ' . $maxCashback;
        }
        return $string;
    };
    $paymentFilter = new Twig_Filter("paymentFilter", function ($string) use ($cashbackFilter, $moneyFilter) {
        $new_string = $moneyFilter($string);
        $new_string = $cashbackFilter($new_string);
        return $new_string;
    });
    $boxClassFilter = new Twig_Filter("boxClassFilter", function ($class, $index) {
        if (in_array($index % 12, array(1, 5, 9, 11))) {
            return $class . "-long";
        }
        return $class;
    });
    $twig->addFilter($paymentFilter);
    $twig->addFilter($boxClassFilter);
    $template = $twig->load('index.twig');
    header_('Gmcard.ru', '', '', '', '');
    echo $template->render($context);
    footer();
}

?>

