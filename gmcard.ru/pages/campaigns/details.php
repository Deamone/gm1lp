<?php
use GM\Modules\Session\Session;
try {
    $selected_campaign_id = str_replace("/campaigns/details/", "", $_SERVER['REQUEST_URI']);
    $admitad = new AdmitadHelper();
    $campaigns = $admitad->cacheAllCampaigns();
    $selected_campaign = null;
    foreach ($campaigns as $campaign) {
        if ($campaign->id == $selected_campaign_id) {
            $selected_campaign = $campaign;
        }
    }
    if ($selected_campaign == null) {
        $selected_campaign = $campaigns[0];
    }
    $selected_campaign->coupons = $admitad->getCouponsForCampaign($selected_campaign);
    renderPage(array(
        'campaigns' => $campaigns,
        'selected_campaign' => $selected_campaign,
        'user' => Session::get_instance()->get_uid()
    ));

} catch (Exception $e) {
    die ('ERROR: ' . $e->getMessage());
}

function renderPage($context)
{
    $loader = new Twig_Loader_Filesystem('templates/campaigns');
    $twig = new Twig_Environment($loader);
    $moneyFilter = function ($string) {
        $new_string = $string;
        $new_string = str_replace("USD", "$", $new_string);
        $new_string = str_replace("RUB", "₽", $new_string);
        return $new_string;
    };
    $cashbackFilter = function ($string) {
        $isRangeCashback = strpos($string, "-");
        if ($isRangeCashback) {
            $maxCashback = substr($string, strpos($string, "-") + 1, strlen($string) - 1);
            return 'до ' . $maxCashback;
        }
        return $string;
    };
    $paymentFilter = new Twig_Filter("paymentFilter", function ($string) use ($cashbackFilter, $moneyFilter) {
        $new_string = $moneyFilter($string);
        $new_string = $cashbackFilter($new_string);
        return $new_string;
    });
    $twig->addFilter($paymentFilter);
    $template = $twig->load('details.twig');
    header_('Gmcard.ru', '', '', '', '');
    echo $template->render($context);
    footer();
}
