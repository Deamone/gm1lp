<?php
use GM\Modules\Session\Session;
try {
    $selected_campaign_id = str_replace("/campaigns/details/", "", $_SERVER['REQUEST_URI']);
    $admitad = new AdmitadHelper();
    $user_id = Session::get_instance()->get_uid();
    $stats = $admitad->getStatistics($user_id,'11.05.2017', '12.05.2017');
    renderPage(array(
        'stats' => $stats,
        'user' => $user_id
    ));

} catch (Exception $e) {
    die ('ERROR: ' . $e->getMessage());
}

function renderPage($context)
{
    $loader = new Twig_Loader_Filesystem('templates/campaigns');
    $twig = new Twig_Environment($loader);
    $template = $twig->load('stats.twig');
    echo $template->render($context);
}
