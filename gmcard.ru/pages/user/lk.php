<?php
$_USER = $USER;

header_('gmcard.ru', '', '', ''
        . '', 'external cabinet');
?>
<!--main-->
<div id="main" class="main_block centred">
    <div class="left_block">
        <h1>Личный кабинет</h1>

        <div class="cabinet_wrapper rounded_5">
            <div class="cab_block cab_avatar">
                <div class="cab_logo"><a href="//gm1lp.ru/page/tariff" target="blank"><img src="<?php echo iconVer($USER); ?>" width="48"></a></div>
                <img class="circled" src="//gm1lp.ru<?php echo '/uploads/avatars/' . $USER['ava']; ?>" width="156" />
            </div>

            <div class="cab_block cab_info">
                <div class="cinfo_block">
                    <div class="cinfo_name"><?php echo $USER['name'] . ' ' . $USER['fam']; ?></div>

                    <!--div class="cinfo_age"><a href="#">27 лет</a></div>

                    <div class="cinfo_general">
                        <span>Место работы:</span> <a href="#">GM</a><br />
                        <span>Место проживания:</span> <a href="#">Чебоксары</a><br />
                        <span>Страна:</span> <a href="#">Россия</a><br />
                        <span>Не женат</span>
                    </div-->
                </div>

                <div class="cinfo_block cinfo_block_balance">
                    <div class="cinfo_balance_label">Баланс</div>
                    <span class="cinfo_balance"><?php echo $USER['money']; ?> руб.</span> <a href="#" onclick="confirm('Пополнение счета происходит на главном сайте. Подтверждаете переход?') ? window.location.href = 'http://gm1lp.ru/user/profile' : '';
                            return false;">Пополнить</a>
                </div>

                <div class="cinfo_block last">
                    <form action="/action/create">
                        <button class="btn rounded_8">Запустить акцию</button>
                    </form>
                </div>
            </div>

            <div class="cab_edit_btn"><a href="#" onclick="confirm('Редактирование профиля происходит на главном сайте.  Подтверждаете переход?') ? window.location.href = 'http://gm1lp.ru/user/profile' : '';
                    return false;">Редактировать</a></div>

            <div class="clear"></div>
        </div>
    </div>

    <div class="right_block">
        <ul class="right_menu">
            <li class="active"><a class="rounded_3" href="/user/lk">Личный кабинет</a></li>
            <li><a class="rounded_3" href="/action/create">Запустить акцию</a></li>
            <li><a class="rounded_3" href="/action/my">Мои акции</a></li>
            <li><a class="rounded_3" href="/action/my_coupons">Мои акции с купонами</a></li>
            <li><a class="rounded_3" href="/my_coupons">Мои купоны</a></li>
        </ul>
    </div>

    <div class="clear"></div>
</div>
<!--main-->
<?php
footer();
