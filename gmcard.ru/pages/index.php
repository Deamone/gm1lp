<?php
use GM\GMCard\DAO\Action;

if ($USER) {
    updateUserById($USER['id'], array('online'), array(time()));
}
header_('GM Сard', '', '', ''
    . '<link rel="stylesheet" type="text/css" href="/css/bjqs.css">'
    . '<script type="text/javascript" src="/js/bjqs-1.3.min.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.countdown.js"></script>', 'external action_main');
$offset = isset($_GET['count']) ? $_GET['count'] - 1 : -1;
$actions = Action::get(isset($_GET['category']) ? $_GET['category'] : 0, 20, $offset + 1);

/** @var Action[] $desire */
$desire = array();
$time = time();
$stop = '';
if (isAjax()) {
    $cssStyle = 'a_discount_ping';
    $j = $offset - 3;
    $kk = $offset;
    $all = $_GET['all'];
    $lines = $_GET['lines'];
    foreach ($actions as $k => $action) {
        include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/discount.php';
    }
    exit;
}
if(isset($_GET['nx'])){
    print_r($actions);
}
foreach ($actions as $action) {
    if ($action->isDesire()) {
        $desire[] = $action;
    }
}
if (count($desire) == 1) {
    $desire[] = $desire[0];
}
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="action_wrapper" style="<?php
        if (!$desire) {
            echo 'display:none;';
        }
        ?>">
            <div id="main_slider">
                <ul class="bjqs">
                    <?php
                    foreach ($desire as $action) {
                        if (time() < mktime(0, 0, 0, 7, 1, 2015)) {
                            $time = $action->getCreated();
                            $stop = ' data-stop="1"';
                        }
                        $images = $action->getImages();
                        $cost = $action->getCost();
                        include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/action.php';
                    }?>
                </ul>
            </div>
        </div>

        <div id="smActions">
            <?php
            reset($actions);
            $cssStyle = 'a_discount_ping';
            $j = 0;
            $all = 0;
            $lines = 0;
            foreach ($actions as $k => $action) {
                include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/discount.php';
            }
            ?>


        </div>
        <div>
            <div class="action_small_wrapper action_double_wrapper action_text_wrapper">
                <p class="large">У вас на сайте или в магазине часто<br/>проходят розыгрыши ценных подарков?</p>

                <p>Подключитесь к GM, у нас есть те,<br/>кто с радостью примет в них участие!</p>

                <p>Благодаря полностью автоматизированному<br/>сервису конкурсов, на это уйдет несколько минут</p>
            </div>
        </div>

        <div class="clear"></div>


        <?php echo count($actions) >= 20 ? '<div class="action_more_btn">
        <div class="action_more_btn_wrapper">
            <button>Показать еще</button>
        </div>
        
    </div>' : ''; ?>
    </div>
    <!--main-->
    <script>
        $(".action_small_wrapper img").load(function () {
            var imageHeight = $(this).height();
            console.log(imageHeight);
            if (imageHeight < 208) {
                $(this).css({'height': '100%'});
            }
            //$(this).parent().append(imageHeight);
        });
        var loader = <?php echo count($actions)>=20?'false':'true'; ?>;
        function loadMoreAction() {
            if (!loader) {
                loader = true;
                var data = {
                    'count': $('#smActions .action_small_wrapper').size(),
                    'lines': $('#smActions .action_small_wrapper[data-lines]:last').attr('data-lines'),
                    'all': $('#smActions .action_small_wrapper[data-lines]:last').attr('data-all')
                };
                $.get('/?category=<?php echo isset($_GET['category'])?$_GET['category']:0; ?>', data, function (data) {
                    //$(data).html();
                    $('#smActions .action_small_wrapper[data-lines]:last').after($(data).clone());
                    $('.action_small_wrapper').hover(function () {
                        $(this).children('.a_detailed').stop().slideDown('fast');
                        $(this).children('.a_description').hide();
                    }, function () {
                        $(this).children('.a_detailed').stop().slideUp('fast');
                        $(this).children('.a_description').show();
                    });
                    runCountDown();
                    loader = false;
                    if (!data) {
                        $('.action_more_btn_wrapper').parent().hide();
                    }
                });
            }
        }
        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() >= $(document).height() - 10) {
                    loadMoreAction();
                }
            });
            $('.action_more_btn_wrapper').click(function () {
                loadMoreAction();
                return false;
            });
        });
    </script>
<?php
footer();
