<?php

if (isset($_GET['code'])) {
    $code = $_GET['code'];
    $answer = file_get_contents('https://oauth.vk.com/access_token?client_id=' . VK_ID . '&client_secret=' . VK_KEY . '&code=' . $code . '&redirect_uri=' . VK_URL.'_link');
    $json = json_decode($answer, true);
    if (isset($json['error']) || !isset($json['access_token'])) {
        exit('Авторизация завершилась неудачей #13345 <a href="/">на главную</a>');
    }
    $uid = (int) $json['user_id'];
    $token = $json['access_token'];
    $time = time();
    $q = query("SELECT * FROM `new_1lp`.`users` WHERE `vk_uid`='$uid' AND `id`!='".$USER['id']."';");

    if ($q['num_rows']) {
        ///updateUserById($q['row']['id'], array('online'), array($time));
        $user_id = $q['row']['id'];
        $user['login'] = $q['row']['login'];
        exit('Данный аккаунт привязан к другому пользователю системы <a href="/">на главную</a>');
    } else {
//        $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get?user_ids=' . $json['user_id'] . '&fields=domain,relation,music,movies,books,games,quotes,sex,bdate,city,country,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max,photo_max_orig,online,online_mobile,lists,domain,has_mobile,contacts,connections,site,education,universities,schools,can_post,can_see_all_posts,can_see_audio,can_write_private_message,status,last_seen,relation,relatives,counters,screen_name,timezone&v=5.21&access_token=' . $token), true);
//        $audios = json_decode(file_get_contents('https://api.vk.com/method/audio.get?owner_id=' . $json['user_id'] . '&count=300&v=5.21&access_token=' . $token), true);
               
        updateUserById($USER['id'], array('vk_uid','vk_ac'), array($json['user_id'],$token));
        redirect('/settings');
    }
} elseif (isset($_GET['error'])) {
    exit('Авторизация завершилась неудачей #16123 <a href="/">на главную</a>');
} else {
    header('location: https://oauth.vk.com/authorize?client_id=' . VK_ID . '&scope=notify,photos,video,audio,offline&redirect_uri=' . VK_URL.'_link' . '&response_type=code&v=5.21');
}
