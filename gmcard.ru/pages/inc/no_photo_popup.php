<div class="substrate" id="open" style="display: none">
    <div class="modal_box" id="modal_box">
        <div class="modal_header">
            <p>У меня нет фото, что делать?</p>
            <a href="#" id="close_modal" onclick="closem();"><img src="/images/close.png"></a>
            <div class="clearfix"></div>
        </div>
        <div class="content_modal">
            <p class="top_content">
                Вы можете воспользоватся сервисом “Google Картинки” и поискать</br>
                изображения там.Важным параметром является то, что изображение</br>
                должно быть не мение 800x600 пикселей.
            </p>
            <p class="bold_text">
                Ниже мы сделали несколько примеров:
            </p>
            <div class="col_modal col_modal1">
                <ul>
                    <li><a href="http://vk.cc/5ow480" target="_blank">Запчасти</a></li>
                    <li><a href="http://vk.cc/5ow4rD" target="_blank">Одежда</a></li>
                    <li><a href="http://vk.cc/5ow4Aj" target="_blank">Ремонт компьютеров</a></li>
                </ul>
            </div>
            <div class="col_modal">
                <ul>
                    <li><a href="http://vk.cc/5ow4Jc" target="_blank">Юридическая помощь</a></li>
                    <li><a href="http://vk.cc/5ow5Dh" target="_blank">Spa процедуры</a></li>
                    <li><a href="http://vk.cc/5ow6qf" target="_blank">Стоматология </a></li>
                </ul>
            </div>
            <div class="col_modal">
                <ul>
                    <li><a href="http://vk.cc/5ow6UC" target="_blank">Мебель</a></li>
                    <li><a href="http://vk.cc/5ow87H" target="_blank">Сауны</a></li>
                    <li><a href="http://vk.cc/5ow8TH" target="_blank">Маникюр</a></li>
                    <li><a href="http://vk.cc/5owagq" target="_blank">Шугаринг</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <p class="dim_text">
                Вашего товара нет в примерах? Ничего страшного, просто откройте любой пример и</br>
                измените ключевое слово запроса или нажмите на кнопку ниже.
            </p>
            <a href="http://vk.cc/5owe0B" target="_blank" class="no_examples">Нет примеров</a>
            <p class="text_bottom">
                <span>Остались вопросы?</span> Посетите наш раздел <a href="#">"GM СПРАВКА"</a> в котором можно найти</br>
                различную полезную информацию,или же увидеть те задачи,на которыми мы</br>
                работаем в данный момент.
            </p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>