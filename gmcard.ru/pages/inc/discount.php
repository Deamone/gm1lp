<?php
if (time() < mktime(0, 0, 0, 7, 1, 2015)) {
    $time = $action->getCreated();
    $stop = ' data-stop="1"';
} else {
    $stop = ' data-stop="0"';
    $time = time();
}
$cost = $action->getCost();
$cssStyle = $cost->getCssClass();
$images = $action->getImages();
if(isset($images[11])) {
    $image_ = $images[11];
}
$css = '';
$kkk = $k;
if(isset($kk)){
    $kkk += $kk;
}
if ($kkk < 4) {
    if (($kkk) % 4 == 0) {
        $css .= ' first';
    }
    if ($kkk % 4 == 0 && $kkk != 0) {
        $css .= ' action_double_wrapper ';
        if(isset($images[1])) {
            $image_ = $images[1];
        }
    }
} else {
    if ($all >= 3) {
        $lines++;
        $all = 0;
    }
    if ($lines % 3 == 0) {
        if ($j % 3 == 0) {
            $css .= ' first ';
        }
        if ($j % 3 == 0) {
            $css .= 'action_double_wrapper ';
            if(isset($images[1])) {
                $image_ = $images[1];
            }
        }
        $css .= '' . $lines . ' %3 ';
    } elseif ($lines % 2 == 0) {
        if ($j % 3 == 0) {
            $css .= ' first ';
        }
        if ($all == 2) {
            $css .= 'action_double_wrapper ';
            if(isset($images[1])) {
                $image_ = $images[1];
            }
        }
        $css .= '' . $lines . ' %2 ';
    } else {
        if ($j % 3 == 0) {
            $css .= ' first ';
        }
        if ($j % 2 == 0) {
            $css .= 'action_double_wrapper ';
            if(isset($images[1])) {
                $image_ = $images[1];
            }
        }
        $css .= '' . $lines . ' %1 ';
    }
    $css .= ' j' . $j;
    $j++;
    $all++;
}
/* if ($k % 4 == 0 && $k < 5) {
  $css .= ' first';
  $lastFirst = 0;
  } elseif (($k - 4) % 3 == 0 && $k > 5) {
  $css .= ' first';
  $lastFirst = 0;
  } else {
  $lastFirst++;
  } */
?>
<div class="action_small_wrapper <?php echo $css;?>" data-lines="<?php echo @$lines;?>" data-all="<?php echo @$all;?>">
    <?php $days_enabled = $action->isDaysEnabled();
    $days = $action->getDays();
    $dayNames = [];
    foreach([1 => 'ПН', 2 => 'ВТ', 3 => 'СР', 4 => 'ЧТ', 5 => 'ПТ', 6 => 'СБ', 7 => 'ВС'] as $index => $name){
        if(in_array($index, $days)) $dayNames[] = $name;
    }
    $action_time = $action->getTime();
    $disabled = false;
    $time_enabled = $action_time->isEnabled();
    $message = [];
    if($days_enabled){
        $today = 7 - (7 - date("w")) % 7;
        if(!in_array($today, $days)) {
            $disabled = true;
        }
        $message[] = "по " . implode(", ", $dayNames);
    }
    if($time_enabled) {
        $from_hour = $action_time->getFromHour();
        $from_minute = $action_time->getFromMinute();
        $to_hour = $action_time->getToHour();
        $to_minute = $action_time->getToMinute();
        $hour = date("H");
        $minute = date("i");
        if($hour > $to_hour || $hour < $from_hour || $hour == $from_hour && $minute < $from_minute || $hour == $to_hour && $minute > $to_minute){
            $disabled = true;
        }
        $message[] = "с " . sprintf('%02d', $from_hour) .":" . sprintf('%02d', $from_minute) . " до " . sprintf('%02d', $to_hour) . ":" . sprintf('%02d', $to_minute);
    }
    if($disabled){?>
        <?php $colors = [
            "128,0,0", // red
            "192,64,128", // pink
            "128,128,0", // yellow
            "0,0,0", // black
            "128,0,192", // purple
            "0,128,64", // green
        ]; $color = $colors[rand(0, count($colors) - 1)]; ?>
        <div class="action_unavailable" style="background-color: rgba(<?php echo $color;?>,0.75)">
            <div class="wrapper">
                Акция доступна<br />
                <?php echo implode("<br />", $message);?>
            </div>
        </div>
    <?php }?>
    <div class="a_image">
        <a href="/action/action?id=<?php echo $action->getActionId();?>"><img src="/<?php echo isset($image_) ? $image_ : 'images/no-image.png';?>" /></a>
    </div>
    <div class="a_detailed">
        <div class="a_detailed_link">
            <a href="/action/action?id=<?php echo $action->getActionId();?>"><?php echo nl2br(mb_substr($action->getCondition(), 0, 155));?></a>
        </div>
        <div class="a_detailed_toolbox actionTime" <?php echo $stop;?> data-ctime="<?php echo $time;?>" data-etime="<?php echo $action->getEnd();?>">
            <div class="a_time">4 дня 15:13:15</div>
            <div class="a_like"><?php echo $action->getLike();?></div>
        </div>
    </div>

    <div class="a_description"><?php echo $action->getTitle();?></div>

    <?php if($cost->isDiscount()){?>
        <!-- <div class="a_price">
            <span class="a_price_old">
                <?php echo $cost->getCost(); ?>
            </span>
            <span class="a_price_new">
                <?php echo $cost->getFinalCost(); ?> руб.
            </span>
        </div> -->
        <div class="a_discount <?php echo $cssStyle;?>">
            <div class="a_discount_label">скидка до</div>
            <?php if($cost->isPercent()){?>
                <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $cost->getDiscount(); ?></span> <span class="a_discount_per">%</span></div>
            <?php } ?>
            <?php if($cost->isRaw()){?>
                <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $cost->getDiscount(); ?> руб.</span></div>
            <?php }?>
        </div>
    <?php } else {?>
        <div class="a_price cashback-plug cashback-plug-small"><?php echo $cost->getFee();?>%</div>
    <?php } ?>


    <?php if($action->isSpecialCondition()){?>
        <div class="special"></div>
    <?php } ?>
</div>
