<script src="/js/open_close.js"></script>
<script src="/js/jquery.maskedinput.min.js"></script>
<script>
    $(function() {
        //задание заполнителя с помощью параметра placeholder
        $("#maska").mask("+7 (999) 999-9999");
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#searchForm').submit(function () {
            var values = '';
            $('#popup_searchlocation :checked').each(function () {
                values += $(this).val() + ',';
            });
            values = values.substr(0, values.length - 1);
            $('#qCategories').val(values);
        });
        $('.top_block .tb_amount').each(function (){
            if($(this).text()=='0'){
                $(this).hide();
            }
        });
    });
</script>
<script src="https://api-maps.yandex.ru/2.1/?lang=tr_TR" type="text/javascript"></script>
</body>
</html>