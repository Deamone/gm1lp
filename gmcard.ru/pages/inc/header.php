<?php
if (isset($_GET['qCategories'])) {
    $qCat = explode(',', urldecode($_GET['qCategories']));
    $qCat = array_flip($qCat);
} else {
    $qCat = array();
}

$counterContacts_ = '';
if($USER) {
    $counterContacts = getCounterContacts($USER['id']);
    foreach ($counterContacts as $k => $contact) {
        if ($contact[1] != 0) {
            unset($counterContacts[$k]);
            continue;
        }
        $contact = getUserById($contact[0]);
        $counterContacts_ .= '<div class="popup_row">
                            <div class="popup_col_img"><a href="//gm1lp.ru/id' . $contact['id'] . '"><img class="rounded_3" src="//gm1lp.ru' . '/uploads/avatars/' . $contact['ava'] . '" width="59" height="59"></a></div>
                            <div class="popup_col_text">
                                <span class="title"><a href="#">' . $contact['name'] . ' ' . $contact['fam'] . '</a></span>
                                <button class="btn btn-size-small rounded_3 addToContact" data-id="' . $contact['id'] . '">Установить контакт</button>
                                <button class="btn btn-size-small rounded_3 btn-color-pink unContact" data-id="' . $contact['id'] . '">Отклонить заявку</button>
                            </div>
                        </div>';
    }
    $notices = getNotices(0, 3, true);
    $cNotices = getCountNotices(true);
}
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo isset($title) ? $title : ''; ?></title>

    <meta charset="utf-8">
    <meta name="description" content="<?php echo isset($desc) ? $desc : 'GM Card'; ?>"/>
    <meta name="keywords" content="<?php echo isset($key) ? $key : 'GM Card'; ?>"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.png" type="image/png">

    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

    <!--link rel="stylesheet" type="text/css" href="//gm1lp.ru/css/style.css"/-->
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style2.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style_action.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style_external.css"/>
    <link rel="stylesheet" type="text/css" href="/css/jquery.Jcrop.min.css">
    <link rel="stylesheet" type="text/css" href="/fancybox/jquery.fancybox.css">


    <link rel="stylesheet" href="/css/chosen.css">
    <link rel="stylesheet" href="/css/new.css" />

    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/js/placeholders.jquery.min.js"></script>

    <script type="text/javascript" src="/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/js/jquery.Jcrop.min.js"></script>

    <script type="text/javascript" src="/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/js/jquery.cookie.js"></script>

    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript" src="/js/nx.js"></script>
    <script type="text/javascript" src="/fancybox/jquery.fancybox.js"></script>
    <?php
    echo $head;
    ?>
    <link rel="stylesheet" type="text/css" href="/css/nx.css">
</head>
<body class="action <?php echo $body_class; ?>">
<!--header-->
<div id="header">
    <div class="main_block centred header_wrapper">
        <div class="main_block top_block">
            <div class="tb_part tb_sep_r tb_logo">
                <a href="/"><img src="/images/logo_small.png"><span>Скидки</span></a>
            </div>

            <div popup-target="popup_services" popup-pos="right" class="item_popup_menu tb_part tb_btn tb_services">
                <span>Все сервисы GM</span>
            </div>

			<div class="item_popup_menu tb_part tb_services" style="padding-top: 6px;"><!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?117"></script>
<script type="text/javascript">
  VK.init({apiId: 5079120, onlyWidgets: true});
</script>
<!-- Put this div tag to the place, where the Like block will be -->
<div id="vk_like"></div>
<script type="text/javascript">
VK.Widgets.Like("vk_like", {type: "button"});
</script></div>
            <?php
            if ($USER):
                ?>
                <div popup-target="popup_profile" popup-pos="left" class="item_popup_menu tb_part_r tb_btn tb_profile"><img src="//gm1lp.ru<?php echo '/uploads/avatars/' . $USER['ava']; ?>" class="rounded_3 mini-avatar"/></div>
                <div popup-target="popup_friends" popup-pos="left"
                     class="item_popup_menu tb_part_r tb_btn tb_ico tb_sep_r tb_friends">
                    <div class="tb_amount"><?php echo count($counterContacts); ?></div>
                </div>
                <div popup-target="popup_notice" popup-pos="left"
                     class="item_popup_menu tb_part_r tb_btn tb_ico tb_sep_r tb_notice">
                    <div class="tb_amount"><?php echo $cNotices; ?></div>
                </div>
                <div popup-target="popup_messages" popup-pos="left"
                     class="item_popup_menu tb_part_r tb_btn tb_ico tb_sep_l tb_sep_r tb_messages">
                    <div class="tb_amount"></div>

                </div>
                <div class="tb_part_r tb_actbtn">
                    <form action="/action/create">
                        <button class="btn btn-color-lblue btn-size-small rounded_5">Запустить акцию</button>
                    </form>
                </div>
                <div class="tb_part_r tb_lcabinet"><a href="/user/lk">Личный кабинет</a></div>
            <?php else: ?>
                <script type="text/javascript">
                    jQuery(function($){
                        $.ajax({
                            data: {authorize: true},
                            dataType: 'json',
                            method: 'POST',
                            success: function(data){
                                if(data.status == 'OK'){
                                    window.location.href = '/?session=' + data.session + '&hash=' + data.hash;
                                }
                            },
                            url: 'https://gm1lp.ru/',
                            xhrFields: {
                                // withCredentials: true
                            }
                        });
                    });
                </script>
                <div class="item_popup_menu tb_part_r tb_profile"><img src="/images/not-authorized-logo.png"/></div>

                <div popup-target="popup_registration" popup-pos="left" class="tb_part_r tb_sep_r item_popup_menu tb_actbtn">
                    <button class="btn btn-color-lblue btn-size-small rounded_5">Регистрация</button>
                </div>
                <div class="tb_part_r tb_lcabinet tb_btn"><a href="//gm1lp.ru/page/login?referer=gmcard.ru">Вход</a></div>

                <div class="tb_part_r tb_sep_r tb_actbtn">
                    <button class="btn btn-color-lyellow btn-size-small rounded_5">Запустить акцию</button>
                </div>
            <?php endif; ?>
            <!-- POPUPS -->
            <!--  -->
            <div id="popup_searchlocation" class="popup popup_searchlocation styled_checkbox">
                <input type="checkbox" name="slocation[]" id="sl_-1" class="checkbox_all" value="-1" <?php echo isset($qCat[-1]) ? ' checked' : ''; ?>> <label for="sl_-1">Везде</label>
                <?php foreach(getGroups() as $index => $group){?>
                    <input type="checkbox" name="slocation[]" id="sl_<?php echo $index;?>" class="checkbox_all" value="<?php echo $index;?>" <?php echo isset($qCat[$index]) ? ' checked' : ''; ?>> <label for="sl_<?php echo $index;?>"><?php echo $group['label'];?></label>
                <?php } ?>
            </div>

            <div id="popup_city" class="popup popup_searchlocation styled_checkbox">
                <input type="radio" name="city" id="city_0" value="0"> <label for="city_0">Все</label>
                <?php
                $city = getCity();
                foreach ($city as $_) {
                    echo '<input type="radio" name="city" id="city_' . $_['cid'] . '" value="' . $_['cid'] . '"> <label for="city_' . $_['cid'] . '">' . $_['name'] . '</label>';
                }
                ?>
                <input type="radio" name="city" id="city_01" value="01"> <label for="city_01">Выбрать другой город</label>
            </div>

            <?php
            if ($USER):
                ?>
                <!-- Profile -->
                <div class="popup tb_popup popup_profile" id="popup_profile">
                    <div class="popup_row header">
                        <div class="popup_left">
                            <img src="//gm1lp.ru<?php echo '/uploads/avatars/' . $USER['ava']; ?>" class="mini-avatar">
                            <span><?php echo $USER['name'] . ' ' . $USER['fam']; ?></span>
                        </div>
                        <div class="popup_right"><a href="/page/logout">Выход</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row">
                        <div class="popup_left"><span class="popup_ico popup_ico_profile">Профиль</span></div>
                        <div class="popup_right"><a href="#" onclick="confirm('Редактирование профиля происходит на главном сайте.  Подтверждаете переход?') ? window.location.href = 'http://gm1lp.ru/user/profile' : '';
                                        return false;">Редактировать</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row">
                        <div class="popup_left"><span class="popup_ico popup_ico_balance">Баланс</span> <span class="popup_hlighted"><?php echo $USER['money']; ?> руб.</span></div>
                        <div class="popup_right"><a href="#" onclick="confirm('Пополнение счета происходит на главном сайте. Подтверждаете переход?') ? window.location.href = 'http://gm1lp.ru/user/profile' : '';
                                        return false;">Оперировать</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row">
                        <div class="popup_left"><span class="popup_ico popup_ico_balance">Учетная запись</span> <span class="popup_hlighted">«<?php echo isVer($USER); ?>»</span></div>
                        <div class="popup_right"><a href="//gm1lp.ru/page/tariff" target="_blank">Повысить</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row">
                        <div class="popup_left"><span class="popup_ico popup_ico_settings">Настройки и конфиденциальность</span></div>
                        <div class="popup_right"><a href="//gm1lp.ru/private">Проверить</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row last">
                        <div class="popup_left"><span class="popup_ico popup_ico_help">Справка</span></div>
                        <div class="popup_right"><a href="//gm1lp.ru/help/index">Получить помощь</a></div>
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Notices -->
                <div class="popup tb_popup popup_notice" id="popup_notice">
                    <?php
                    if (!$cNotices) {
                        echo 'Новых новостей нет';
                    }
                    for ($i = 0; $i < $cNotices; $i++) {
                        $group = getGroup($notices[$i]['user_id']);
                        $text = strip_tags($notices[$i]['text']);
                        if (mb_strlen($text) > 256) {
                            $text = mb_substr($text, 0, 256) . '...';
                        }
                        $text = trim($text);
                        if (empty($text)) {
                            $text = '<b>файл</b>';
                        }
                        echo '<div class="popup_row ' . ($i + 1 == $cNotices ? 'last' : '') . '">
                    <a class="cover" href="https://gm1lp.ru/feed#post' . $notices[$i]['wall_id'] . '"></a>
                    <div class="popup_toolbox"><!--a href="#"><img src="/images/icon-popup-close.png"/></a--></div>
                    <div class="popup_col_img"><a href="https://gm1lp.ru/' . $group['url'] . '"><img class="rounded_3 width48" src="https://gm1lp.ru//uploads/g_avatars/' . $group['ava'] . '"></a>
                    </div>
                    <div class="popup_col_text">
                        <span class="title"><a href="https://gm1lp.ru/feed#post' . $notices[$i]['wall_id'] . '">' . $group['title'] . '</a></span><br/>
                        <span class="info">Опубликовал(а):</span> <a href="https://gm1lp.ru/feed#post' . $notices[$i]['wall_id'] . '">' . $text . '</a>
                    </div>
                </div>';
                    }
                    ?>
                </div>

                <!-- Messages -->
                <div class="popup tb_popup popup_messages" id="popup_messages">
                </div>
                <!-- Friends -->
                <div class="popup tb_popup popup_friends" id="popup_friends">
                    <?php echo $counterContacts_ ? $counterContacts_ : 'Входящих заявок нет'; ?>
                </div>
            <?php else: ?>
                <!-- Registration -->
                <div class="popup tb_popup popup_registration" id="popup_registration">
                    <p class="reg_label">Зарегистрироваться</p>

                    <p>
                        У нас единая система сервисов, а т.е., для всех сервисов системы GM действует 1 логин и 1 пароль,
                        которые вы указываете при регистрации в системе GM, на главном ресурсе - www.gm1lp.ru
                    </p>

                    <form action="//gm1lp.ru/page/login?referer=gmcard.ru%2Fpage%2Flogin" style="display: inline-block;">
                        <button class="btn rounded_3">Регистрация</button>
                    </form>
                    <button class="btn btn-type-link btn-style-dotted" onclick="$('body').click();"><span>Пройду позже</span></button>
                </div>

                <!-- Login -->
                <div class="popup tb_popup popup_auth" id="popup_auth">
                    <label>Логин</label>
                    <input type="text" class="inp inp_full inp-size-small rounded_3" placeholder="Ваша почта"/>

                    <label>Пароль</label>
                    <input type="text" class="inp inp_full inp-size-small rounded_3" placeholder="Ваш пароль"/>

                    <button class="btn rounded_3">Войти</button>
                    <button class="btn btn-type-link btn-style-dotted btn-forget"><span>Не помню пароль</span></button>
                </div>
            <?php endif; ?>
            <!-- Services -->
            <div class="popup tb_popup popup_services" id="popup_services">
                <div class="popup_row">
                    <div class="popup_col_label">GM1LP</div>
                    <div class="popup_col_text">
                        <a href="//gm1lp.ru/">вернуться домой</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_col_label">Скидки</div>
                    <div class="popup_col_text">
                        <a href="//gmcard.ru/">посмотреть</a> или <a href="//gmcard.ru/action/create">запустить</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_col_label">Конкурсы</div>
                    <div class="popup_col_text">
                        <a href="//rightlike.ru/">участвовать</a> или <a href="//rightlike.ru/action/create">создать</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_col_label">Задания</div>
                    <div class="popup_col_text">
                        <a href="//goodmoneys.ru">заработать</a> или <a href="//goodmoneys.ru">дать задание</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Блэк лист</div>
                    <div class="popup_col_text">
                        проверить себя или исполнителя
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">P2P</div>
                    <div class="popup_col_text">
                        одолжить денег под % или погасить долг
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Бартер</div>
                    <div class="popup_col_text">
                        услуга за услугу, товар за товар
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Краудфандинг</div>
                    <div class="popup_col_text">
                        спонсировать или привлечь средства
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Аренда</div>
                    <div class="popup_col_text">
                        сдать или взять вещь в аренду
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row last disabled">
                    <div class="popup_col_label">ВКонкурсе</div>
                    <div class="popup_col_text">
                        выбрать победителя
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <!-- Menus -->
            <?php foreach(getGroups() as $group){?>
                <div class="popup popup_menu" id="popup_menu_<?php echo $group['anchor'];?>">
                    <?php
                    $totalHeight = 0;
                    $firstColumn = [];
                    $secondColumn = [];
                    $thirdColumn = [];
                    $secondColumnHeight = 0;
                    $thirdColumnHeight = 0;
                    $secondColumnFull = false;
                    $thirdColumnFull = false;
                    foreach($group['categories'] as $category){
                        $totalHeight += count($category['subcategories']) + 1;
                    }
                    foreach(array_reverse($group['categories']) as $category) {
                        if (!$thirdColumnFull && 3 * ($thirdColumnHeight + count($category['subcategories']) + 1) < $totalHeight || count($thirdColumn) < 1) {
                            $thirdColumnHeight += count($category['subcategories']) + 1;
                            $thirdColumn[] = $category;
                        } elseif (!$secondColumnFull && 3 * ($secondColumnHeight + count($category['subcategories']) + 1) < $totalHeight || count($secondColumn) < 1) {
                            $thirdColumnFull = true;
                            $secondColumnHeight += count($category['subcategories']) + 1;
                            $secondColumn[] = $category;
                        } else {
                            $thirdColumnFull = true;
                            $secondColumnFull = true;
                            $firstColumn[] = $category;
                        }
                    } ?>
                    <div class="popup_menu_arrow"></div>
                    <div class="column">
                        <?php $first = true; foreach(array_reverse($firstColumn) as $category){?>
                            <h3 class="cat_link<?php if($first){echo " first"; $first = false;}?>"><a href="/?category=<?php echo $category['index'];?>"><?php echo $category['label'];?></a></h3>
                            <?php if(count($category['subcategories']) > 0){?>
                                <ul class="subcat_links">
                                    <?php foreach($category['subcategories'] as $subcategory){?>
                                        <li><a href="/?category=<?php echo $subcategory['index'];?>"><?php echo $subcategory['label'];?></a></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="column">
                        <?php $first = true; foreach(array_reverse($secondColumn) as $category){?>
                            <h3 class="cat_link<?php if($first){echo " first"; $first = false;}?>"><a href="/?category=<?php echo $category['index'];?>"><?php echo $category['label'];?></a></h3>
                            <?php if(count($category['subcategories']) > 0){?>
                                <ul class="subcat_links">
                                    <?php foreach($category['subcategories'] as $subcategory){?>
                                        <li><a href="/?category=<?php echo $subcategory['index'];?>"><?php echo $subcategory['label'];?></a></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="column">
                        <?php $first = true; foreach(array_reverse($thirdColumn) as $category){?>
                            <h3 class="cat_link<?php if($first){echo " first"; $first = false;}?>"><a href="/?category=<?php echo $category['index'];?>"><?php echo $category['label'];?></a></h3>
                            <?php if(count($category['subcategories']) > 0){?>
                                <ul class="subcat_links">
                                    <?php foreach($category['subcategories'] as $subcategory){?>
                                        <li><a href="/?category=<?php echo $subcategory['index'];?>"><?php echo $subcategory['label'];?></a></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="clear"></div>
                </div>
            <?php } ?>

            <div class="image_crop_block" id="image_crop">
                <div class="ic_image"></div>
                <div class="ic_toolbox">
                    <div class="ic_info">
                        <span class="ic_steps"><span id="step_cur">1</span> из <span id="step_amount">2</span></span>
                        <span class="ic_steps_text" id="step_info">Выберите основное изображение</span>
                    </div>
                    <div class="ic_btns">
                        <button class="btn btn-color-white btn-size-small rounded_3" id="cancel_btn">Отмена</button>
                        <button class="btn btn-size-small rounded_3" id="next_btn">Далее</button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <script src="/js/baron.js"></script>
            <div id="popup_trc" class="popup popup_searchlocation styled_checkbox">
                <?php
                $city = isset($_COOKIE['city']) ? (int)$_COOKIE['city'] : 0;
                $trc = getTrcByCity($city);
                if (count($trc) > 5) {
                    echo '<input type="text" class="search" placeholder="Начните вводить название">';
                }
                echo '<div class="scroller">';
                echo '<input data-title="Все" type="radio" name="trc" id="trc_0" value="0"> <label for="trc_0">Все</label>';
                foreach ($trc as $_) {
                    echo '<input data-title="' . $_['title'] . '" type="radio" name="trc" id="trc_' . $_['trc_id'] . '" value="' . $_['trc_id'] . '"> <label for="trc_' . $_['trc_id'] . '"><b>' . $_['title'] . '</b><br/><span>' . $_['address'] . '</span>' . $_['worktime'] . '</label>';
                }
                echo '<div class="scroller__track"><div class="scroller__bar"></div></div>';
                echo '</div>';
                ?>
            </div>
            <script>
                $(document).on('keyup', '#popup_trc .search', function (e) {
                    var $q = $(this).val().toLowerCase();
                    console.log(e);
                    $('#popup_trc ul li').each(function (v) {
                        var name = $(this).find('b').text().toLowerCase();
                        if (!$q) {
                            $(this).removeClass('hide');
                            return;
                        }
                        if (name.indexOf($q) === -1) {
                            $(this).addClass('hide');
                        } else {
                            $(this).removeClass('hide');
                        }
                    });
                });
                $(function () {
                    $('.scroller').baron({
                        bar: '.scroller__bar',
                        track: '.scroller__track'
                    });
                });
            </script>
        </div>
    </div>
</div>
<!--header-->
<!--navigation-->
<div id="navigation" class="main_block centred">
    <div class="top_toolbox rounded_15">
        <div class="tt_block">
            <ul class="top_menu">
                <li><a href="/">Главная</a></li>
                <li><a href="/howto">Как это работает</a></li>
                <li><a href="//gm1lp.ru/page/tariff" target="_blank">Цены</a></li>
                <li><a href="https://gm1lp.ru/help/agree" target="_blank">Правила</a></li>
            </ul>
            <div class="clear"></div>
        </div>

        <div class="tt_block tt_search">
            <form action="/action/search" method="GET" id="searchForm">
                <input autocomplete="off" name="qSearch" type="text" class="search_left" placeholder="Что вы хотите найти" value="<?php echo isset($_GET['qSearch']) ? $_GET['qSearch'] : ''; ?>"/>
                <input type="hidden" name="qCategories" value="" id="qCategories"/>
                <button popup-target="popup_searchlocation" popup-pos="left" class="item_popup_menu btn btn-searchlocation"><span>Везде</span></button>
                <button class="btn btn-icon btn-icon-search btn-color-lyellow rounded_3">Найти</button>
            </form>
        </div>

    </div>
    <div class="sort">
        <h2>Сортировка</h2>

        <div class="tt_block_r tt_cities">
            <label>Ваш город</label>
            <button popup-target="popup_city" popup-pos="left" class="item_popup_menu btn btn-location rounded_8"><span id="city_text">Чебоксары</span></button>
        </div>
        <?php if ($trc): ?>
            <div class="tt_block_r tt_trc">
                <label>Показать только</label>
                <button popup-target="popup_trc" popup-pos="right" class="item_popup_menu btn btn-location rounded_8"><span id="trc_text">Выбрать ТРЦ</span></button>
            </div>
        <?php endif; ?>
    </div>

    <div>
        <ul class="large_menu">
            <?php foreach(getGroups() as $group){?>
                <li class="menu_<?php echo $group['anchor'];?> item_popup_menu" popup-target="popup_menu_<?php echo $group['anchor'];?>" popup-pos="center"
                    popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#"><?php echo $group['label'];?></a></li>
            <?php } ?>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<!--navigation-->

