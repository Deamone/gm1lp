<li>
    <div>
        <div class="a_left_block" style="background-image: url(/<?php echo $images[1];?>); background-size: cover;">
            <!--<img src="" title="title"> -->
            <?php if($cost->isDiscount()){?>
                <div class="a_discount">
                    <div class="a_discount_label">скидка до</div>
                    <?php if($cost->isPercent()){?>
                        <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $cost->getDiscount(); ?></span> <span class="a_discount_per">%</span></div>
                    <?php } ?>
                    <?php if($cost->isRaw()){?>
                        <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $cost->getDiscount(); ?> руб.</span></div>
                    <?php }?>
                </div>
            <?php }?>
        </div>

        <div class="a_right_block">
            <div class="a_right_block_wrapper">
                <div class="a_time ar_block desireTime" <?php echo $stop;?> data-ctime="<?php echo $time;?>" data-etime="<?php echo $action->getEnd();?>">
                    <span>До конца акции</span> <span class="a_time_days">4 дня</span> <span class="a_time_time">15:13:15</span>
                </div>
                <div class="a_like"><?php echo $action->getLike();?></div>
                <div class="a_discount_description ar_block">
                    <p><?php echo $action->getTitle();?></p>
                    <?php if($action->getCost()->isDiscount()){?>
                        <!-- <div class="a_price">
                            <span class="a_price_old">
                                <?php echo $action->getCost()->getCost(); ?>
                            </span>
                            <span class="a_price_new">
                                <?php echo $action->getCost()->getFinalCost(); ?> руб.
                            </span>
                        </div> -->
                    <?php } else {?>
                        <div class="a_price cashback-plug cashback-plug-long">
                            <?php echo $action->getCost()->getFee();?>%
                        </div>
                    <?php } ?>
                    <form action="/action/action" method="get" style="text-align: center">
                        <input type="hidden" name="id" value="<?php echo $action->getActionId();?>" />
                        <button class="btn btn-color-bblue rounded_20">Перейти</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</li>