<div class="action_edit_wrapper">
    <div class="action_small_wrapper">
        <div class="a_image">
            <a href="/action/action?id=<?php echo $action->getActionId();?>"><img src="/<?php echo isset($images[11]) ? $images[11] : 'images/no-image.png';?>" /></a>
        </div>
        <div class="a_description"><?php echo nl2br(mb_substr($action->getTitle(), 0, 155));?>...</div>

        <?php if($cost->isDiscount()){?>
            <!-- <div class="a_price">
                <span class="a_price_old">
                    <?php echo $cost->getCost();?>
                </span>
                <span class="a_price_new">
                    <?php echo $cost->getFinalCost();?> руб.
                </span>
            </div> -->
            <div class="a_discount <?php echo $cssStyle;?>">
                <div class="a_discount_label">скидка</div>
                <?php if($cost->isPercent()){?>
                    <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $cost->getDiscount();?></span> <span class="a_discount_per">%</span></div>
                <?php }?>
                <?php if($cost->isRaw()){?>
                    <div class="a_discount_bottom"><span class="a_discount_amount"><?php echo $cost->getDiscount();?> руб.</span></div>
                <?php } ?>
            </div>
        <?php } else {?>
            <div class="a_price cashback-plug cashback-plug-small"><?php echo $cost->getFee();?>%</div>
        <?php } ?>
    </div>

    <div class="actions">
        <div class="ae_edit">
            <a href="/action/create?edit=<?php echo $action->getActionId();?>">
                <img src="/images/edit.png">
            </a>
        </div>
        <div class="ae_remove">
            <a id="various1" href="#delete<?php echo $action->getActionId();?>">
                <img src="/images/remove.png">
            </a>
        </div>
        <div class="ae_remove">
            <a id="various1" href="#pickup<?php echo $action->getActionId();?>">
                <img src="/images/up.png">
            </a>
        </div>
    </div>
    <div class="clear"></div>

    <div class="ae_toolbox">
        <div class="ae_date"><?php echo date('d.m.Y H:i:s');?></div>
        <div class="ae_like"><?php echo $action->getLike();?></div>
    </div>

    <div id="pickup<?php echo $action->getActionId();?>" style="display:none">
        <div class="modal" style="background-image: url('/images/modal.png'); height: 222px; width: 500px;">
            <div class="title">Поднять</div>
            <div class="text">
                Вы действительно хотите поднять свою акцию на первое место?
                <br/>
                <br/>
                <a href="/action/create?up=<?php echo $action->getActionId();?>" class="btn_new ">
                    Потверждаю
                </a>
                <a href="javascript:$.fancybox.close();" class="btn_new silver">
                    Отмена
                </a>
            </div>
        </div>
    </div>

    <div id="delete<?php echo $action->getActionId();?>" style="display:none">
        <div class="modal" style="background-image: url('/images/modal.png'); height: 222px; width: 500px;">
            <div class="title">Удаление</div>
            <div class="text">
                Вы действительно хотите удалить свою акцию безвозвратно?
                <br/>
                <br/>
                <a href="/action/create?delete=<?php echo $action->getActionId();?>" class="btn_new ">
                    Потверждаю
                </a>
                <a href="javascript:$.fancybox.close();" class="btn_new silver">
                    Отмена
                </a>
            </div>
        </div>
    </div>
</div>