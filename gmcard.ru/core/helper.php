<?php

use GM\Modules\ImageLib\ImageLib;

mb_internal_encoding("UTF-8");
if (!class_exists('Memcache')) {

    class Memcache
    {

        function addServer($x, $y)
        {

        }

        function set($x, $y)
        {

        }

        function get($x)
        {

        }

        function connect()
        {

        }

    }

}

function getExtension($filename)
{
    return strtolower(substr(strrchr($filename, '.'), 1));
}

header('Content-type: text/html; charset=utf-8');
$_SESSION = array();
$user = array();
$pmc = new Memcache;
$pmc->addServer('localhost', 11209);

function redirect($url = '/')
{
    header('location: ' . $url);
    exit;
}

function login_form()
{
    global $user;
    if ($user) {
        return '<a href="/">Добро пожаловать, ' . $user['first_name'] . '</a>';
    } else {
        return '<a href="/auth_vk">Войти через Вконтакте</a>';
    }
}

function isAjax()
{
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        return true;
    }
    return false;
}

function getUserById($id)
{
    global $pmc;
    $id = (int)$id;
    #$_ = $pmc->get('user' . $id);
    #if (!$_) {
    $q = query("SELECT * FROM `users` WHERE `id`='$id';");
    $_ = $q['row'];
    #$pmc->set('user' . $id, $_);
    #}

    return $_;
}

/**
 *
 * @global Memcache $pmc
 * @param type $id
 * @return array (to, from) - входящие пользователю, исходящие
 */
function updateUserById($id, $keys, $values)
{
    global $pmc;
    $id = (int)$id;
    $sql = '';
    $_ = $pmc->get('user' . $id);

    $count = count($keys);
    for ($i = 0; $i < $count; $i++) {
        $sql .= ",`" . $keys[$i] . "`='" . query_escape($values[$i]) . "'";
        if ($_) {
            $_[$keys[$i]] = $values[$i];
        }
    }
    query("UPDATE `users` SET " . substr($sql, 1) . " WHERE `id`='$id';");
    if ($_) {
        $pmc->set('user' . $id, $_);
    }
    return $_;
}

function addPost($onwer_id, $post, $user = false)
{
    global $USER;
    if (!$user) {
        $user = $USER['id'];
    }
    $user = (int)$user;
    $onwer_id = (int)$onwer_id;
    $post = query_escape($post);
    if (empty($post)) {
        return;
    }
    query("INSERT INTO `nx_wall` (`owner_id`,`text`,`user_id`,`created`)VALUES('$onwer_id','$post','" . $user . "','" . time() . "');");
}

function isVer($USER, $asLevel = false)
{
    $status = '';
    $time = time();
    if ($USER['level'] == 0) {
        $status = 'Новичок';
        $level = 0;
    } elseif (($USER['level'] == 1 && $USER['pro_end'] > $time)) {
        $status = 'Мастер';
        $level = 1;
    } elseif (($USER['level'] == 2 && $USER['pro_end'] > $time)) {
        $status = 'Профессионал';
        $level = 2;
    } elseif (($USER['level'] == 3 && $USER['pro_end'] > $time)) {
        $status = 'Эксперт';
        $level = 3;
    } else {
        $status = 'Новичок';
        $level = 0;
        /* $status = 'ПРО ';
          if ($USER['pro_end'] - $time > 24 * 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 24 / 3600) . ' дней';
          } elseif ($USER['pro_end'] - $time > 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 3600) . ' часов';
          } elseif ($USER['pro_end'] - $time > 60) {
          $status .= (int) (($USER['pro_end'] - $time) / 60) . ' минут';
          } elseif ($USER['level'] > 2) {
          $status .= 'навсегда';
          } else {
          $status .= (int) ($USER['pro_end'] - $time) . ' секунд';
          } */
    }

    return !$asLevel ? $status : $level;
}

function iconVer($USER)
{
    $status = '';
    $time = time();
    if ($USER['level'] == 0) {
        $status = '//gm1lp.ru/images/tariffs/tariff_new.png';
    } elseif (($USER['level'] == 1 && $USER['pro_end'] > $time)) {
        $status = '//gm1lp.ru/images/tariffs/tariff_master.png';
    } elseif (($USER['level'] == 2 && $USER['pro_end'] > $time)) {
        $status = '//gm1lp.ru/images/tariffs/tariff_pro.png';
    } elseif (($USER['level'] == 3 && $USER['pro_end'] > $time)) {
        $status = '//gm1lp.ru/images/tariffs/tariff_expert.png';
    } else {
        $status = '//gm1lp.ru/images/tariffs/tariff_new.png';
        /* $status = 'ПРО ';
          if ($USER['pro_end'] - $time > 24 * 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 24 / 3600) . ' дней';
          } elseif ($USER['pro_end'] - $time > 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 3600) . ' часов';
          } elseif ($USER['pro_end'] - $time > 60) {
          $status .= (int) (($USER['pro_end'] - $time) / 60) . ' минут';
          } elseif ($USER['level'] > 2) {
          $status .= 'навсегда';
          } else {
          $status .= (int) ($USER['pro_end'] - $time) . ' секунд';
          } */
    }

    return $status;
}

function delDialogsCookie()
{
    $keys = array_keys($_COOKIE);
    $c = count($keys);
    for ($i = 0; $i < $c; $i++) {
        if (strpos($keys[$i], 'dialog_') !== false) {
            setcookie($keys[$i], '0', time() - 100, '/');
            unset($_COOKIE[$keys[$i]]);
        }
    }
}

function moneyForRef($referal)
{
    $referal = (int)$referal;
    query("UPDATE `users` SET `money`=`money`+1 WHERE `id`='$referal';");
}

function getCity($id = false, $offset = 0, $count = 30)
{
    if ($id === false) {
        $q = query("SELECT * FROM `nx_city` ORDER BY `city_id` ASC LIMIT {$offset},{$count};");

        return $q['rows'];
    } else {
        $id = (int)$id;
        $q = query("SELECT * FROM `nx_city` WHERE `city_id`='{$id}'");

        return $q['row'];
    }
}

function getCityByName($city = '', $country = false, $offset = 0, $count = 8000)
{
    $city = query_escape($city);
    $country = (int)$country;

    if (!$country) {
        $q = query("SELECT `c`.`id` as `id`,CONCAT(`c`.`city`,', ', `cy`.`country`,', ', `r`.`region`)  as `text`
FROM `city` as `c` 
INNER JOIN `country_region` as `cr` 
INNER JOIN `region_city` as `rc` ON `rc`.`id_region`=`cr`.`id_region` AND `c`.`id`=`rc`.`id_city`
INNER JOIN `country` as `cy` ON `cr`.`id_country`=`cy`.`id`
INNER JOIN `region` as `r` ON `r`.`id`=`rc`.`id_region`
WHERE `c`.`city` collate cp1251_general_ci like '%{$city}%' GROUP BY `c`.`id`, `cy`.`id` ORDER BY `id` ASC LIMIT {$offset},{$count};");
    } else {
        $q = query("SELECT `c`.`id` as `id`,`c`.`city` as `text` 
FROM `city` as `c` 
INNER JOIN `country_region` as `cr` ON `cr`.`id_country`='{$country}' 
INNER JOIN `region_city` as `rc` ON `rc`.`id_region`=`cr`.`id_region` AND `c`.`id`=`rc`.`id_city`
WHERE `c`.`city` collate cp1251_general_ci like '%{$city}%' GROUP BY `c`.`id` ORDER BY `id` ASC LIMIT {$offset},{$count};");
    }

    return $q['rows'];
}

function getCityById($id)
{
    $id = (int)$id;
    if (!$id) {
        return array('text' => 'Все');
    }

    $q = query("SELECT `c`.`id` as `id`,CONCAT(`c`.`city`,', ', `cy`.`country`,', ', `r`.`region`)  as `text`
FROM `city` as `c`
INNER JOIN `country_region` as `cr`
INNER JOIN `region_city` as `rc` ON `rc`.`id_region`=`cr`.`id_region` AND `c`.`id`=`rc`.`id_city`
INNER JOIN `country` as `cy` ON `cr`.`id_country`=`cy`.`id`
INNER JOIN `region` as `r` ON `r`.`id`=`rc`.`id_region`
WHERE `c`.`id`='$id';");
    return $q['row'];
}

function create_thumbnail($original_file, $thumb_file, $new_w, $new_h, $autoSize = 'none')
{
    //GD check
    if (!function_exists('gd_info')) {
        // ERROR - Invalid image
        return 'GD support is not enabled';
    }
    $info = getimagesize($original_file);
    // Create src_img
    if ($info['mime'] == 'image/jpeg') {
        $src_img = imagecreatefromjpeg($original_file);
    } else if ($info['mime'] == 'image/png') {
        $src_img = imagecreatefrompng($original_file);
    } else if ($info['mime'] == 'image/gif') {
        $src_img = imagecreatefromgif($original_file);
    } else {
        return -1;
    }
    if ($thumb_file == false) {
        imagepng($src_img, $thumb_file);
        imagedestroy($src_img);
        return;
    }

    $src_width = imageSX($src_img); //$src_width
    $src_height = imageSY($src_img); //$src_height

    if ($src_width < $new_w || $src_height < $new_h) {
        imagejpeg($src_img, $thumb_file);
        imagedestroy($src_img);
        return 1;
    }

    if ($src_height > $src_width && $autoSize == 'auto') {
        $autoSize = 'width';
    } elseif ($src_height < $src_width && $autoSize == 'auto') {
        $autoSize = 'height';
    }
    if ($new_h === false) {
        $new_h = $src_height;
    }
    if ($new_w === false) {
        $new_w = $src_width;
    }
    $src_w = $src_width;
    $src_h = $src_height;
    $src_x = 0;
    $src_y = 0;
    $dst_w = $new_w;
    $dst_h = $new_h;
    $src_ratio = $src_w / $src_h;
    $dst_ratio = $new_w / $new_h;

    switch ($autoSize) {
        case "width":
            // AUTO WIDTH
            $dst_w = $dst_h * $src_ratio;
            break;
        case "height":
            // AUTO HEIGHT
            $dst_h = $dst_w / $src_ratio;
            break;
        case "none":
            // If proportion of source image is wider then proportion of thumbnail image, then use full height of source image and crop the width.
            if ($src_ratio > $dst_ratio) {
                // KEEP HEIGHT, CROP WIDTH
                $src_w = $src_h * $dst_ratio;
                $src_x = floor(($src_width - $src_w) / 2);
            } else {
                // KEEP WIDTH, CROP HEIGHT
                $src_h = $src_w / $dst_ratio;
                $src_y = floor(($src_height - $src_h) / 2);
            }
            break;
        case "none2":
            // If proportion of source image is wider then proportion of thumbnail image, then use full height of source image and crop the width.
            if ($src_ratio > $dst_ratio) {
                // KEEP HEIGHT, CROP WIDTH
                $src_w = $src_h * $dst_ratio;
                $src_x = floor(($src_width - $src_w) / 2);
            } else {
                // KEEP WIDTH, CROP HEIGHT
                $src_h = $src_w / $dst_ratio;
                $src_y = floor(($src_height - $src_h) / 2);
            }
            break;
    }

    $dst_img = imagecreatetruecolor($dst_w, $dst_h);

    // PNG THUMBS WITH ALPHA PATCH
    // Turn off alpha blending and set alpha flag
    imagealphablending($dst_img, false);
    imagesavealpha($dst_img, true);


    imagecopyresampled($dst_img, $src_img, 0, 0, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);


    imagejpeg($dst_img, $thumb_file, 99);

    imagedestroy($dst_img);
    imagedestroy($src_img);

    return 1;
    /* $info = getimagesize($path); //получаем размеры картинки и ее тип
      $size = array($info[0], $info[1]); //закидываем размеры в массив
      //В зависимости от расширения картинки вызываем соответствующую функцию
      if ($info['mime'] == 'image/png') {
      $src = imagecreatefrompng($path); //создаём новое изображение из файла
      } else if ($info['mime'] == 'image/jpeg') {
      $src = imagecreatefromjpeg($path);
      } else if ($info['mime'] == 'image/gif') {
      $src = imagecreatefromgif($path);
      } else {
      return false;
      }

      $thumb = imagecreatetruecolor($width, $height); //возвращает идентификатор изображения, представляющий черное изображение заданного размера
      $src_aspect = $size[0] / $size[1]; //отношение ширины к высоте исходника
      $thumb_aspect = $width / $height; //отношение ширины к высоте аватарки

      if ($src_aspect < $thumb_aspect) {        //узкий вариант (фиксированная ширина)      $scale = $width / $size[0];         $new_size = array($width, $width / $src_aspect);        $src_pos = array(0, ($size[1] * $scale - $height) / $scale / 2); //Ищем расстояние по высоте от края картинки до начала картины после обрезки   } else if ($src_aspect > $thumb_aspect) {
      //широкий вариант (фиксированная высота)
      $scale = $height / $size[1];
      $new_size = array($height * $src_aspect, $height);
      $src_pos = array(($size[0] * $scale - $width) / $scale / 2, 0); //Ищем расстояние по ширине от края картинки до начала картины после обрезки
      } else {
      //другое
      $new_size = array($width, $height);
      $src_pos = array(0, 0);
      }

      $new_size[0] = max($new_size[0], 1);
      $new_size[1] = max($new_size[1], 1);

      imagecopyresampled($thumb, $src, 0, 0, $src_pos[0], $src_pos[1], $new_size[0], $new_size[1], $size[0], $size[1]);
      //Копирование и изменение размера изображения с ресемплированием

      if ($save === false) {
      return imagepng($thumb); //Выводит JPEG/PNG/GIF изображение
      } else {
      return imagepng($thumb, $save); //Сохраняет JPEG/PNG/GIF изображение
      } */
}

function prepareImg($data)
{
    global $SYSTEM;

    $path = substr(strchr($data['file'], '/tmp/'), 5);

    $file = str_replace('..', '', 'tmp/' . $path);
    $type = getExtension($file);
    switch ($type) {
        case 'jpg':
        case 'jpeg':
            $src_func = 'imagecreatefromjpeg';
            $write_func = 'imagejpeg';
            $image_quality = 100;
            break;
        case 'gif':
            $src_func = 'imagecreatefromgif';
            $write_func = 'imagegif';
            $image_quality = null;
            break;
        case 'png':
            $src_func = 'imagecreatefrompng';
            $write_func = 'imagejpeg';
            $image_quality = 100;
            $type = 'jpg';
            break;
        default:
            return false;
    }
    $src_img = $src_func($file);
    $coords = $data['coords'];

    if (!isset($coords['x1'])) {
        $coords['x1'] = $coords['x'];
        $coords['y1'] = $coords['y'];
    }

    $new_width = $coords['x2'] - $coords['x1'];
    $new_height = $coords['y2'] - $coords['y1'];

    $new_img = imagecreatetruecolor($new_width, $new_height);
    switch ($type) {
        case 'gif':
        case 'png':
            imagecolortransparent($new_img, imagecolorallocate($new_img, 0, 0, 0));
            imagealphablending($new_img, false);
            imagesavealpha($new_img, true);
            break;
    }

    $new_file_path = 'tmp/' . microtime(true) . mt_rand(0, 999) . '.' . $type;

    list($width, $height) = getimagesize($file);
    $success = imagecopyresampled(
            $new_img, $src_img, 0, 0, $coords['x'], $coords['y'], $new_width, $new_height, $coords['w'], $coords['h']
        ) && $write_func($new_img, $new_file_path, $image_quality);
    if (isset($SYSTEM['post']['mini'])) {
        create_img($new_file_path, $new_file_path, 285, 178);
    } else {
        create_img($new_file_path, $new_file_path, 724, 453);
    }
    return $new_file_path;
}

function uppingAction($action_id) {
    query("UPDATE `nx_actions` SET `upping`=UNIX_TIMESTAMP() WHERE `action_id`='$action_id';");

    return $action_id;
}

function deleteAction($id)
{
	query("DELETE FROM `nx_actions` WHERE `action_id`='$id';");
}

function upLikeAction($id)
{
    query("UPDATE `nx_actions` SET `like`=`like`+1 WHERE `action_id`='{$id}'");
}

function getHardcodedGroups()
{
    return [
        [
            "index" => 84,
            "anchor" => "forher",
            "label" => "Для неё",
            "categories" => [
                ["index" => 1, "label" => "Одежда", "subcategories" => [
                    ["index" => 2, "label" => "Большие размеры"],
                    ["index" => 3, "label" => "Брюки"],
                    ["index" => 4, "label" => "Верхняя одежда"],
                    ["index" => 5, "label" => "Джемперы, свитеры"],
                    ["index" => 6, "label" => "Джинсы"],
                    ["index" => 7, "label" => "Костюмы, пиджаки"],
                    ["index" => 8, "label" => "Кофты, водолазки"],
                    ["index" => 9, "label" => "Одежда для дома"],
                    ["index" => 10, "label" => "Платья"],
                    ["index" => 11, "label" => "Спорт"],
                    ["index" => 12, "label" => "Толстовки"],
                    ["index" => 13, "label" => "Туники"],
                    ["index" => 14, "label" => "Футболки, поло"],
                    ["index" => 15, "label" => "Шорты"],
                    ["index" => 16, "label" => "Юбки"],
                    ["index" => 17, "label" => "Блузки, рубашки"],
                ]],
                ["index" => 41, "label" => "Аксессуары", "subcategories" => [
                    ["index" => 42, "label" => "Бижутерия"],
                    ["index" => 43, "label" => "Головные уборы"],
                    ["index" => 44, "label" => "Зонты"],
                    ["index" => 45, "label" => "Кошельки"],
                    ["index" => 46, "label" => "Очки"],
                    ["index" => 47, "label" => "Перчатки"],
                    ["index" => 48, "label" => "Платки, шарфы"],
                    ["index" => 49, "label" => "Ремни"],
                    ["index" => 50, "label" => "Сумки, клатчи"],
                    ["index" => 51, "label" => "Часы"],
                    ["index" => 335, "label" => "Золотые украшения"],
                    ["index" => 336, "label" => "Серьги"],
                    ["index" => 337, "label" => "Кольца"],
                    ["index" => 338, "label" => "Цепи и браслеты"],
                    ["index" => 339, "label" => "Цепочки"],
                    ["index" => 340, "label" => "Шкатулки"],
                    ["index" => 341, "label" => "Броши"],
                    ["index" => 342, "label" => "Колье"],
                    ["index" => 343, "label" => "Подвески"],
                    ["index" => 344, "label" => "Кресты и иконки"],
                ]],
                ["index" => 19, "label" => "Обувь", "subcategories" => [
                    ["index" => 20, "label" => "Сапоги"],
                    ["index" => 21, "label" => "Ботинки"],
                    ["index" => 22, "label" => "Балетки"],
                    ["index" => 23, "label" => "Босоножки"],
                    ["index" => 24, "label" => "Ботильоны"],
                    ["index" => 25, "label" => "Кеды, кроссовки"],
                    ["index" => 26, "label" => "Мокасины"],
                    ["index" => 27, "label" => "Тапочки"],
                    ["index" => 28, "label" => "Туфли"],
                    ["index" => 29, "label" => "Шлепанцы, сандалии"],
                ]],
                ["index" => 30, "label" => "Для авто", "subcategories" => [
                    ["index" => 31, "label" => "Запчасти и аксессуары"],
                    ["index" => 32, "label" => "Навигаторы"],
                    ["index" => 33, "label" => "Тонировка"],
                    ["index" => 34, "label" => "Карбоновая пленка"],
                    ["index" => 35, "label" => "Регистраторы"],
                    ["index" => 36, "label" => "Сигнализация"],
                ]],
                ["index" => 345, "label" => "Спорт", "subcategories" => [
                    ["index" => 346, "label" => "Абонементы"],
                    ["index" => 347, "label" => "Тренажерные залы"],
                    ["index" => 348, "label" => "Плавание"],
                    ["index" => 349, "label" => "Танцы"],
                    ["index" => 350, "label" => "Пластика"],
                    ["index" => 351, "label" => "Спортивное питание"],
                    ["index" => 352, "label" => "Аминокислоты"],
                    ["index" => 353, "label" => "Трежеры"],
                    ["index" => 354, "label" => "Спортивное оборудование"],
                    ["index" => 355, "label" => "Одежда для тренировок"],
                    ["index" => 356, "label" => "Индивидуальная защита <br> от травм"],
                    ["index" => 357, "label" => "Индивидуальный тренер"],
                ]],
                ["index" => 37, "label" => "Белье", "subcategories" => [
                    ["index" => 38, "label" => "Колготки, носки"],
                    ["index" => 39, "label" => "Купальники"],
                    ["index" => 40, "label" => "Эротическое белье"],
                ]],
                ["index" => 18, "label" => "Прочее", "subcategories" => []],
            ]
        ],
        [
            "index" => 441,
            "anchor" => "forhim",
            "label" => "Для него",
            "categories" => [
                ["index" => 52, "label" => "Одежда", "subcategories" => [
                    ["index" => 53, "label" => "Брюки"],
                    ["index" => 54, "label" => "Верхняя одежда"],
                    ["index" => 55, "label" => "Джемперы, свитеры"],
                    ["index" => 56, "label" => "Джинсы"],
                    ["index" => 57, "label" => "Костюмы, пиджаки"],
                    ["index" => 58, "label" => "Одежда для дома"],
                    ["index" => 59, "label" => "Рубашки"],
                    ["index" => 60, "label" => "Спорт"],
                    ["index" => 61, "label" => "Толстовки"],
                    ["index" => 62, "label" => "Футболки, поло"],
                    ["index" => 63, "label" => "Шорты"],
                ]],
                ["index" => 83, "label" => "Аксессуары", "subcategories" => [
                    ["index" => 360, "label" => "Бижутерия"],
                    ["index" => 85, "label" => "Головные уборы"],
                    ["index" => 86, "label" => "Зонты"],
                    ["index" => 361, "label" => "Кошельки"],
                    ["index" => 87, "label" => "Очки"],
                    ["index" => 88, "label" => "Перчатки"],
                    ["index" => 362, "label" => "Платки, шарфы"],
                    ["index" => 89, "label" => "Портмоне"],
                    ["index" => 90, "label" => "Ремни"],
                    ["index" => 91, "label" => "Сумки, портфели"],
                    ["index" => 92, "label" => "Часы"],
                    ["index" => 363, "label" => "Золотые украшения"],
                    ["index" => 364, "label" => "Серьги"],
                    ["index" => 365, "label" => "Кольца"],
                    ["index" => 366, "label" => "Цепи и браслеты"],
                    ["index" => 367, "label" => "Цепочки"],
                    ["index" => 368, "label" => "Шкатулки"],
                    ["index" => 369, "label" => "Броши"],
                    ["index" => 370, "label" => "Калье"],
                    ["index" => 371, "label" => "Подвески"],
                    ["index" => 372, "label" => "Кресты и иконки"],
                ]],
                ["index" => 65, "label" => "Обувь", "subcategories" => [
                    ["index" => 66, "label" => "Ботинки"],
                    ["index" => 67, "label" => "Кеды, кроссовки"],
                    ["index" => 68, "label" => "Мокасины"],
                    ["index" => 69, "label" => "Сапоги"],
                    ["index" => 70, "label" => "Тапочки"],
                    ["index" => 71, "label" => "Туфли"],
                    ["index" => 72, "label" => "Шлепанцы, сандалии"],
                ]],
                ["index" => 73, "label" => "Для авто", "subcategories" => [
                    ["index" => 74, "label" => "Запчасти и аксессуары"],
                    ["index" => 75, "label" => "Навигаторы"],
                    ["index" => 76, "label" => "Тонировка"],
                    ["index" => 77, "label" => "Карбоновая пленка"],
                    ["index" => 78, "label" => "Регистраторы"],
                    ["index" => 79, "label" => "Сигнализация"],
                ]],
                ["index" => 534, "label" => "Спорт", "subcategories" => [
                    ["index" => 373, "label" => "Абонементы"],
                    ["index" => 374, "label" => "Тренажерные залы"],
                    ["index" => 375, "label" => "Плавание"],
                    ["index" => 376, "label" => "Танцы"],
                    ["index" => 377, "label" => "Пластика"],
                    ["index" => 378, "label" => "Спортивное питание"],
                    ["index" => 379, "label" => "Аминокислоты"],
                    ["index" => 380, "label" => "Тренажеры"],
                    ["index" => 381, "label" => "Спортивное оборудование"],
                    ["index" => 382, "label" => "Одежда для тренировок"],
                    ["index" => 383, "label" => "Индивидуальная защита<br> от травм"],
                    ["index" => 384, "label" => "Индивидуальный тренер"],
                ]],
                ["index" => 80, "label" => "Белье", "subcategories" => [
                    ["index" => 81, "label" => "Нижнее белье"],
                    ["index" => 358, "label" => "Эротическое белье"],
                    ["index" => 359, "label" => "Плавки"],
                    ["index" => 82, "label" => "Носки"],
                ]],
                ["index" => 64, "label" => "Прочее", "subcategories" => []],
            ]
        ],
        [
            "index" => 442,
            "anchor" => "forchildren",
            "label" => "Детям",
            "categories" => [
                ["index" => 124, "label" => "Обувь для мальчиков", "subcategories" => [
                    ["index" => 125, "label" => "Ботинки, полуботинки"],
                    ["index" => 126, "label" => "Валенки"],
                    ["index" => 127, "label" => "Кеды"],
                    ["index" => 128, "label" => "Кроссовки"],
                    ["index" => 129, "label" => "Мокасины"],
                    ["index" => 130, "label" => "Орто-обувь"],
                    ["index" => 131, "label" => "Пинетки, первый шаг"],
                    ["index" => 132, "label" => "Резиновые сапоги"],
                    ["index" => 133, "label" => "Сандалии"],
                    ["index" => 134, "label" => "Сапоги"],
                    ["index" => 135, "label" => "Сланцы"],
                    ["index" => 136, "label" => "Тапочки"],
                    ["index" => 137, "label" => "Туфли"],
                    ["index" => 138, "label" => "Угги, унты"],
                ]],
                ["index" => 150, "label" => "Обувь для девочек", "subcategories" => [
                    ["index" => 151, "label" => "Ботинки, полуботинки"],
                    ["index" => 152, "label" => "Валенки"],
                    ["index" => 153, "label" => "Кеды"],
                    ["index" => 154, "label" => "Кроссовки"],
                    ["index" => 155, "label" => "Мокасины"],
                    ["index" => 156, "label" => "Орто-обувь"],
                    ["index" => 157, "label" => "Пинетки, первый шаг"],
                    ["index" => 158, "label" => "Резиновые сапоги"],
                    ["index" => 159, "label" => "Сандалии, босоножки"],
                    ["index" => 160, "label" => "Сапоги"],
                    ["index" => 161, "label" => "Сланцы"],
                    ["index" => 162, "label" => "Тапочки, балетки"],
                    ["index" => 163, "label" => "Туфли"],
                    ["index" => 164, "label" => "Угги, унты"],
                ]],
                ["index" => 99, "label" => "Игрушки", "subcategories" => [
                    ["index" => 100, "label" => "Деревянные игрушки"],
                    ["index" => 101, "label" => "Игровые наборы"],
                    ["index" => 102, "label" => "Конструкторы"],
                    ["index" => 103, "label" => "Куклы и аксессуары"],
                    ["index" => 104, "label" => "Машинки, ж/д дороги"],
                    ["index" => 105, "label" => "Музыкальные игрушки"],
                    ["index" => 106, "label" => "Мягкие игрушки"],
                    ["index" => 107, "label" => "Наборы для творчества"],
                    ["index" => 108, "label" => "Настольные игры"],
                    ["index" => 109, "label" => "Радиоуправляемые игрушки"],
                    ["index" => 110, "label" => "Развивающие игрушки"],
                    ["index" => 111, "label" => "Роботы, трансформеры"],
                ]],
                ["index" => 140, "label" => "Одежда для девочек", "subcategories" => [
                    ["index" => 141, "label" => "Верхняя одежда"],
                    ["index" => 142, "label" => "Белье, носки, колготки"],
                    ["index" => 143, "label" => "Джинсы, брюки"],
                    ["index" => 144, "label" => "Для дома и отдыха"],
                    ["index" => 145, "label" => "Костюмы, комбинезоны"],
                    ["index" => 146, "label" => "Кофты, джемперы, свитеры"],
                    ["index" => 147, "label" => "Платья, блузки"],
                    ["index" => 148, "label" => "Футболки, топы"],
                    ["index" => 149, "label" => "Юбки, шорты"],
                ]],
                ["index" => 115, "label" => "Одежда для мальчиков", "subcategories" => [
                    ["index" => 116, "label" => "Рубашки"],
                    ["index" => 117, "label" => "Белье, носки, колготки"],
                    ["index" => 118, "label" => "Верхняя одежда"],
                    ["index" => 119, "label" => "Джинсы, брюки"],
                    ["index" => 120, "label" => "Для дома и отдыха"],
                    ["index" => 121, "label" => "Костюмы, комбинезоны"],
                    ["index" => 122, "label" => "Кофты, джемперы, свитеры"],
                    ["index" => 123, "label" => "Футболки, шорты"],
                ]],
                ["index" => 93, "label" => "Новорожденным", "subcategories" => [
                    ["index" => 94, "label" => "Боди, песочники"],
                    ["index" => 95, "label" => "Верхняя одежда"],
                    ["index" => 96, "label" => "Комбинезоны"],
                    ["index" => 97, "label" => "Ползунки, штанишки"],
                    ["index" => 98, "label" => "Распашонки, кофты"],
                ]],
                ["index" => 139, "label" => "Аксессуары", "subcategories" => []],
                ["index" => 114, "label" => "Спорт", "subcategories" => []],
                ["index" => 112, "label" => "Для детской", "subcategories" => []],
                ["index" => 113, "label" => "Прочее", "subcategories" => []],
            ]
        ],
        [
            "index" => 443,
            "anchor" => "forhome",
            "label" => "Для дома",
            "categories" => [
                ["index" => 187, "label" => "Кухня", "subcategories" => [
                    ["index" => 188, "label" => "Полотенца и фартуки"],
                    ["index" => 189, "label" => "Скатерти"],
                    ["index" => 190, "label" => "Сковороды"],
                    ["index" => 191, "label" => "Столовые приборы"],
                    ["index" => 192, "label" => "Хранение"],
                    ["index" => 193, "label" => "Шторы и занавески"],
                    ["index" => 194, "label" => "Ванная комната"],
                    ["index" => 195, "label" => "Декоративные мелочи"],
                    ["index" => 196, "label" => "Для стирки, глажки и сушки"],
                    ["index" => 197, "label" => "Для уборки"],
                    ["index" => 198, "label" => "Коврики"],
                    ["index" => 199, "label" => "Полотенца"],
                    ["index" => 200, "label" => "Штора для ванной"],
                    ["index" => 201, "label" => "Кастрюли и казаны"],
                    ["index" => 202, "label" => "Кухонная утварь и аксессуары"],
                    ["index" => 203, "label" => "Наборы посуды"],
                    ["index" => 204, "label" => "Наборы посуды для сервировки"],
                    ["index" => 205, "label" => "Ножи и разделочные доски"],
                    ["index" => 206, "label" => "Овощерезки и терки"],
                ]],
                ["index" => 174, "label" => "Гостиная", "subcategories" => [
                    ["index" => 175, "label" => "Вазы, кашпо и горшки"],
                    ["index" => 176, "label" => "Декоративные подушки"],
                    ["index" => 177, "label" => "Декоретто и фотообои"],
                    ["index" => 178, "label" => "Зеркала"],
                    ["index" => 179, "label" => "Картины и постеры"],
                    ["index" => 180, "label" => "Покрывала и пледы"],
                    ["index" => 181, "label" => "Рулонные шторы и жалюзи"],
                    ["index" => 182, "label" => "Стильные мелочи"],
                    ["index" => 183, "label" => "Фоторамки"],
                    ["index" => 184, "label" => "Часы"],
                    ["index" => 185, "label" => "Шкатулки"],
                    ["index" => 186, "label" => "Шторы и гардины"],
                ]],
                ["index" => 222, "label" => "Текстиль", "subcategories" => [
                    ["index" => 223, "label" => "Декоративные подушки"],
                    ["index" => 224, "label" => "Детское постельное белье"],
                    ["index" => 225, "label" => "Наматрасники"],
                    ["index" => 226, "label" => "Одеяла"],
                    ["index" => 227, "label" => "Подушки"],
                    ["index" => 228, "label" => "Покрывала и пледы"],
                    ["index" => 229, "label" => "Постельное белье"],
                    ["index" => 230, "label" => "Рулонные шторы и жалюзи"],
                    ["index" => 231, "label" => "Шторы и гардины"],
                ]],
                ["index" => 207, "label" => "Мебель, свет и дача", "subcategories" => [
                    ["index" => 208, "label" => "Дача"],
                    ["index" => 209, "label" => "Мебель"],
                    ["index" => 210, "label" => "Свет"],
                ]],
                ["index" => 218, "label" => "Гардеробная", "subcategories" => [
                    ["index" => 219, "label" => "Системы для хранения белья"],
                    ["index" => 220, "label" => "Системы для хранения обуви"],
                    ["index" => 221, "label" => "Системы хранения для одежды"],
                ]],
                ["index" => 165, "label" => "Спальня", "subcategories" => [
                    ["index" => 166, "label" => "Детское постельное белье"],
                    ["index" => 167, "label" => "Наматрасники"],
                    ["index" => 168, "label" => "Одеяла"],
                    ["index" => 169, "label" => "Подушки"],
                    ["index" => 170, "label" => "Покрывала и пледы"],
                    ["index" => 171, "label" => "Постельное белье"],
                    ["index" => 172, "label" => "Рулонные шторы и жалюзи"],
                    ["index" => 173, "label" => "Шторы и гардины"],
                ]],
                ["index" => 211, "label" => "Ванная комната", "subcategories" => [
                    ["index" => 212, "label" => "Декоративные мелочи"],
                    ["index" => 213, "label" => "Для стирки, глажки и сушки"],
                    ["index" => 214, "label" => "Для уборки"],
                    ["index" => 215, "label" => "Коврики"],
                    ["index" => 216, "label" => "Полотенца"],
                    ["index" => 217, "label" => "Штора для ванной"],
                ]],
                ["index" => 535, "label" => "Все для ремонта", "subcategories" => [
                    ["index" => 399, "label" => "Обои"],
                    ["index" => 400, "label" => "Люстры"],
                    ["index" => 401, "label" => "Краска"],
                    ["index" => 402, "label" => "Расходные материалы"],
                    ["index" => 403, "label" => "Кафель"],
                    ["index" => 404, "label" => "Забор"],
                    ["index" => 405, "label" => "Счетчики"],
                    ["index" => 406, "label" => "Отделочные материалы"],
                    ["index" => 407, "label" => "Деревянные срубы"],
                    ["index" => 408, "label" => "Решетки на окна"],
                    ["index" => 409, "label" => "Двери"],
                    ["index" => 410, "label" => "Пластиковые окна"],
                    ["index" => 411, "label" => "Инструменты"],
                    ["index" => 412, "label" => "Станки"],
                    ["index" => 413, "label" => "Прочее"],
                ]],
                ["index" => 232, "label" => "Прочее", "subcategories" => [
                    ["index" => 385, "label" => "Корм для животных"],
                    ["index" => 386, "label" => "Корм для собак"],
                    ["index" => 387, "label" => "Корм для кошек"],
                    ["index" => 388, "label" => "Корм для аквариумных рыбок"],
                    ["index" => 389, "label" => "Живой корм"],
                    ["index" => 390, "label" => "Корм для морских свинок"],
                    ["index" => 391, "label" => "Корм для грызунов"],
                    ["index" => 392, "label" => "Корм для птиц"],
                    ["index" => 393, "label" => "Принадлежности для животных"],
                    ["index" => 394, "label" => "Игрушки для животных"],
                    ["index" => 395, "label" => "Аквариумы"],
                    ["index" => 396, "label" => "Растения для аквариума"],
                    ["index" => 397, "label" => "Принадлежности для аквариумов"],
                    ["index" => 398, "label" => "Лекарства для животных"],
                ]],
            ]
        ],
        [
            "index" => 444,
            "anchor" => "techs",
            "label" => "Техника",
            "categories" => [
                ["index" => 233, "label" => "Электроника", "subcategories" => [
                    ["index" => 234, "label" => "Аксессуары"],
                    ["index" => 235, "label" => "Аудио, Видео, Фото"],
                    ["index" => 236, "label" => "Для авто"],
                    ["index" => 237, "label" => "Планшеты, Ноутбуки"],
                    ["index" => 238, "label" => "Телефоны"],
                ]],
                ["index" => 239, "label" => "Техника", "subcategories" => [
                    ["index" => 240, "label" => "Для дома"],
                    ["index" => 241, "label" => "Для красоты и здоровья"],
                    ["index" => 242, "label" => "Для кухни"],
                ]],
                ["index" => 243, "label" => "Прочее", "subcategories" => []],
            ]
        ],
        [
            "index" => 445,
            "anchor" => "services",
            "label" => "Услуги",
            "categories" => [
                ["index" => 278, "label" => "Красота", "subcategories" => [
                    ["index" => 279, "label" => "Уход за волосами"],
                    ["index" => 280, "label" => "Эпиляция"],
                    ["index" => 281, "label" => "Уход за лицом"],
                    ["index" => 282, "label" => "SPA, массаж, уход за телом"],
                    ["index" => 283, "label" => "Маникюр, педикюр"],
                    ["index" => 284, "label" => "Другое"],
                ]],
                ["index" => 285, "label" => "Здоровье", "subcategories" => [
                    ["index" => 286, "label" => "Стоматология"],
                    ["index" => 287, "label" => "Диагностика, обследование"],
                    ["index" => 416, "label" => "Лечение зубов под наркозом"],
                    ["index" => 417, "label" => "Лечение зубов под закисью азота"],
                    ["index" => 418, "label" => "Исправление прикуса"],
                    ["index" => 419, "label" => "Ортодонтия"],
                    ["index" => 420, "label" => "Компьютерное моделирование протезов"],
                    ["index" => 421, "label" => "Имплантация, протезирование"],
                    ["index" => 422, "label" => "3D-компьютерная томография зубов"],
                    ["index" => 423, "label" => "Ортопантомография"],
                    ["index" => 424, "label" => "Лечение заболеваний десен"],
                    ["index" => 425, "label" => "Лечение под микроскопом"],
                    ["index" => 426, "label" => "Отбеливание зубов"],
                    ["index" => 427, "label" => "Детская стоматология"],
                    ["index" => 428, "label" => "Хирургия"],
                    ["index" => 429, "label" => "Гинекология"],
                    ["index" => 430, "label" => "Венерические заболевания"],
                    ["index" => 431, "label" => "Услуги ветеринара"],
                    ["index" => 432, "label" => "Животный доктор"],
                    ["index" => 288, "label" => "Другое"],
                ]],
                ["index" => 514, "label" => "Для авто", "subcategories" => [
                    ["index" => 433, "label" => "Чип-тюнинг"],
                    ["index" => 434, "label" => "Сход-развал"],
                    ["index" => 435, "label" => "Балансировка колес"],
                    ["index" => 436, "label" => "Шиномонтаж"],
                    ["index" => 437, "label" => "Лакокрасочные услуги"],
                    ["index" => 438, "label" => "Ремонт кузова"],
                    ["index" => 439, "label" => "Перетяжка салона"],
                    ["index" => 440, "label" => "Диагностика"],
                    ["index" => 468, "label" => "Мойка"],
                    ["index" => 563, "label" => "Экспресс-мойка"],
                    ["index" => 564, "label" => "Полировка кузова"],
                    ["index" => 565, "label" => "Жидкая полировка"],
                    ["index" => 566, "label" => "Горячий или холодный воск"],
                    ["index" => 567, "label" => "Мойка и полировка салона"],
                    ["index" => 568, "label" => "Чернение резины"],
                    ["index" => 569, "label" => "Кондиционер кожи и др."],
                    ["index" => 570, "label" => "Прочее"],
                ]],
                ["index" => 515, "label" => "Сайты, продвижение, реклама", "subcategories" => [
                    ["index" => 491, "label" => "Дизайн сайтов"],
                    ["index" => 492, "label" => "Дизайн логотипов"],
                    ["index" => 493, "label" => "Верстка"],
                    ["index" => 494, "label" => "Разработка сайтов"],
                    ["index" => 495, "label" => "Разработка одностраничников"],
                    ["index" => 496, "label" => "SMM"],
                    ["index" => 497, "label" => "SEO"],
                    ["index" => 498, "label" => "SMO"],
                    ["index" => 499, "label" => "Оформление сообществ в соц., сетях"],
                    ["index" => 500, "label" => "Баннеры"],
                    ["index" => 501, "label" => "Брендирование"],
                    ["index" => 502, "label" => "Сервера"],
                    ["index" => 503, "label" => "Облачные хранилища"],
                    ["index" => 504, "label" => "Устранение уязвимостей"],
                    ["index" => 505, "label" => "Подключение модулей"],
                    ["index" => 506, "label" => "Написание программ"],
                    ["index" => 507, "label" => "Настройка Яндекс.Директ"],
                    ["index" => 508, "label" => "Настройка Google AdWords"],
                    ["index" => 509, "label" => "Настройка Google Analytics"],
                    ["index" => 510, "label" => "Обучение Photoshop"],
                    ["index" => 511, "label" => "Обучение CorelDRAW"],
                    ["index" => 512, "label" => "Обучение в других программах"],
                ]],
                ["index" => 513, "label" => "Свадьба", "subcategories" => [
                    ["index" => 451, "label" => "Свадебный фотограф"],
                    ["index" => 452, "label" => "Свадебный видеооператор"],
                    ["index" => 453, "label" => "Тамада"],
                    ["index" => 454, "label" => "Оформление зала"],
                    ["index" => 455, "label" => "Платья"],
                    ["index" => 456, "label" => "Костюмы"],
                    ["index" => 457, "label" => "Сопутствующие товары"],
                    ["index" => 458, "label" => "Свадебные аксессуары"],
                    ["index" => 459, "label" => "Свадебные товары для невесты"],
                    ["index" => 460, "label" => "Свадебные товары для жениха"],
                    ["index" => 461, "label" => "Заказ автобомиля"],
                    ["index" => 462, "label" => "Организация торжеств"],
                    ["index" => 463, "label" => "Развлечения для гостей"],
                    ["index" => 464, "label" => "Свадебный кортеж"],
                    ["index" => 465, "label" => "Цветы на свадьбу"],
                    ["index" => 466, "label" => "Свадебный танец"],
                    ["index" => 467, "label" => "Проведение фейерверков"],
                ]],
                ["index" => 293, "label" => "Ремонт", "subcategories" => [
                    ["index" => 571, "label" => "Ремонт компьютеров"],
                    ["index" => 469, "label" => "Ремонт ноутбуков"],
                    ["index" => 470, "label" => "Ремонт телефонов"],
                    ["index" => 471, "label" => "Восстановление информации"],
                    ["index" => 472, "label" => "Переустановка Windows"],
                    ["index" => 473, "label" => "Установка программ"],
                    ["index" => 474, "label" => "Удаление вирусов"],
                ]],
                ["index" => 516, "label" => "Юридические услуги", "subcategories" => [
                    ["index" => 536, "label" => "Земельное право"],
                    ["index" => 537, "label" => "Семейное право"],
                    ["index" => 538, "label" => "Трудовое право"],
                    ["index" => 539, "label" => "Уголовное право"],
                    ["index" => 540, "label" => "Налоговое право"],
                    ["index" => 541, "label" => "Жилищное право"],
                    ["index" => 542, "label" => "Гражданское право"],
                    ["index" => 543, "label" => "Корпоративное право"],
                    ["index" => 544, "label" => "Хозяйственное право"],
                    ["index" => 545, "label" => "Наследственное право"],
                    ["index" => 546, "label" => "Административное право"],
                    ["index" => 547, "label" => "Защита прав потребителей"],
                    ["index" => 548, "label" => "Оформление гражданства РФ"],
                    ["index" => 549, "label" => "Гражданский, уголовный <br>и арбитражный процесс"],
                    ["index" => 550, "label" => "Регистрация индивидуальных предпринимателей<br> и юридических лиц"],
                    ["index" => 551, "label" => "Составление полного пакета документов"],
                    ["index" => 552, "label" => "Предоставление документов в <br>регистрирующие органы"],
                    ["index" => 553, "label" => "Получение Свидетельства<br> о внесении записи в ЕГРЮЛ"],
                    ["index" => 554, "label" => "Получение кодов статистики в Облкомстате"],
                    ["index" => 555, "label" => "Регистрация изменений в учредительные <br>документы юридических лиц"],
                    ["index" => 556, "label" => "Реорганизация юридических лиц"],
                    ["index" => 557, "label" => "Ликвидация юридических лиц"],
                    ["index" => 558, "label" => "Прекращение деятельности (ликвидация)<br> индивидуальных предпринимателей"],
                    ["index" => 559, "label" => "Открытие расчетных счетов"],
                    ["index" => 560, "label" => "Регистрационные услуги"],
                    ["index" => 561, "label" => "Юридические консультации"],
                    ["index" => 562, "label" => "Составлению договоров, исковых <br>заявлений, иных документов"],
                ]],
                ["index" => 517, "label" => "По дому", "subcategories" => [
                    ["index" => 475, "label" => "Уборка квартир, домов"],
                    ["index" => 476, "label" => "Ремонт квартир"],
                    ["index" => 477, "label" => "Монтажные работы"],
                    ["index" => 478, "label" => "Полный цикл работ"],
                    ["index" => 479, "label" => "Ремонт техники"],
                    ["index" => 480, "label" => "Проведение труб"],
                    ["index" => 481, "label" => "Сантехнические работы"],
                    ["index" => 482, "label" => "Облицовка кафелем и керамогранитом<br> стен и пола"],
                    ["index" => 483, "label" => "Малярно-штукатурные работы"],
                    ["index" => 484, "label" => "Электромонтажные работы"],
                    ["index" => 485, "label" => "Возведение из гипсокартона одноуровневых <br>и многоуровневых потолков"],
                    ["index" => 486, "label" => "Выравнивание стен, пола, потолка под маяк"],
                    ["index" => 487, "label" => "Штукатурные работы <br>(шпатлевка стен, потолков)"],
                    ["index" => 488, "label" => "Монтаж перегородок"],
                    ["index" => 489, "label" => "Стяжка пола, наливной пол"],
                    ["index" => 490, "label" => "Настил ламината, линолеума, <br>коврового покрытия"],
                ]],
                ["index" => 294, "label" => "Разное", "subcategories" => []],
                ["index" => 292, "label" => "Обучение", "subcategories" => []],
                ["index" => 290, "label" => "Концерты", "subcategories" => []],
                ["index" => 289, "label" => "Развлечения", "subcategories" => []],
                ["index" => 291, "label" => "Фитнес", "subcategories" => []],
                ["index" => 518, "label" => "Тату", "subcategories" => []],
                ["index" => 519, "label" => "Пирсинг", "subcategories" => []],
                ["index" => 520, "label" => "Фотоуслуги", "subcategories" => []],
                ["index" => 521, "label" => "Видеоуслуги", "subcategories" => []],
                ["index" => 522, "label" => "Свидания на крышах", "subcategories" => []],
                ["index" => 523, "label" => "Организация встреч", "subcategories" => []],
                ["index" => 524, "label" => "Гостиницы", "subcategories" => []],
                ["index" => 525, "label" => "Сауны", "subcategories" => []],
                ["index" => 526, "label" => "Аренда квартир", "subcategories" => []],
                ["index" => 527, "label" => "Хостелы", "subcategories" => []],
                ["index" => 528, "label" => "Встреча в аэропорту", "subcategories" => []],
                ["index" => 529, "label" => "Охрана", "subcategories" => []],
                ["index" => 530, "label" => "Сопровождение", "subcategories" => []],
                ["index" => 531, "label" => "Вооруженное сопровождение", "subcategories" => []],
                ["index" => 532, "label" => "Обеспечение безопасности", "subcategories" => []],
            ]
        ],
        [
            "index" => 446,
            "anchor" => "beauty",
            "label" => "Красота",
            "categories" => [
                ["index" => 252, "label" => "Уход за телом", "subcategories" => [
                    ["index" => 253, "label" => "Антицеллюлитные средства"],
                    ["index" => 254, "label" => "Дезодоранты"],
                    ["index" => 255, "label" => "Депиляция"],
                    ["index" => 256, "label" => "Питательные средства"],
                    ["index" => 257, "label" => "Средства для загара"],
                    ["index" => 258, "label" => "Средства для очищения"],
                    ["index" => 259, "label" => "Средства для увлажнения"],
                ]],
                ["index" => 268, "label" => "Уход за лицом", "subcategories" => [
                    ["index" => 269, "label" => "Питательные средства"],
                    ["index" => 270, "label" => "Средства для контура глаз"],
                    ["index" => 271, "label" => "Средства для очищения"],
                    ["index" => 272, "label" => "Средства для увлажнения"],
                    ["index" => 273, "label" => "Средства против старения"],
                ]],
                ["index" => 260, "label" => "Уход за волосами", "subcategories" => [
                    ["index" => 261, "label" => "Кондиционеры для волос"],
                    ["index" => 262, "label" => "Маски для волос"],
                    ["index" => 263, "label" => "Фиксация и защита волос"],
                    ["index" => 264, "label" => "Шампуни для волос"],
                ]],
                ["index" => 244, "label" => "Парфюмерия", "subcategories" => [
                    ["index" => 245, "label" => "Женская парфюмерия"],
                    ["index" => 246, "label" => "Мужская парфюмерия"],
                    ["index" => 247, "label" => "Унисекс парфюмерия"],
                    ["index" => 414, "label" => "Косметика"],
                    ["index" => 415, "label" => "Краски"],
                ]],
                ["index" => 248, "label" => "Макияж", "subcategories" => [
                    ["index" => 249, "label" => "Для глаз"],
                    ["index" => 250, "label" => "Для лица"],
                    ["index" => 251, "label" => "Для ногтей"],
                ]],
                ["index" => 274, "label" => "Гигиена", "subcategories" => [
                    ["index" => 275, "label" => "Средства гигиены"],
                ]],
                ["index" => 276, "label" => "18+", "subcategories" => []],
                ["index" => 277, "label" => "Прочее", "subcategories" => []],
                ["index" => 265, "label" => "Техника для красоты", "subcategories" => []],
                ["index" => 266, "label" => "Красота и здоровье", "subcategories" => []],
                ["index" => 267, "label" => "Спорт", "subcategories" => []],
            ]
        ],
        [
            "index" => 447,
            "anchor" => "hotels",
            "label" => "Отели",
            "categories" => [
                ["index" => 305, "label" => "Другие города", "subcategories" => []],
                ["index" => 304, "label" => "Юг России", "subcategories" => []],
                ["index" => 306, "label" => "Украина", "subcategories" => []],
                ["index" => 307, "label" => "Казахстан", "subcategories" => []],
                ["index" => 309, "label" => "Другие страны", "subcategories" => []],
                ["index" => 308, "label" => "Беларусь", "subcategories" => []],
                ["index" => 303, "label" => "Урал", "subcategories" => []],
                ["index" => 301, "label" => "Поволжье", "subcategories" => []],
                ["index" => 296, "label" => "Санкт-Петербург и область", "subcategories" => []],
                ["index" => 295, "label" => "Москва и область", "subcategories" => []],
                ["index" => 297, "label" => "Золотое кольцо", "subcategories" => []],
                ["index" => 298, "label" => "Алтай", "subcategories" => []],
                ["index" => 300, "label" => "Балтика", "subcategories" => []],
                ["index" => 299, "label" => "Байкал", "subcategories" => []],
                ["index" => 302, "label" => "Сибирь", "subcategories" => []],
            ]
        ],
        [
            "index" => 448,
            "anchor" => "tours",
            "label" => "Туры",
            "categories" => [
                ["index" => 317, "label" => "Англия", "subcategories" => []],
                ["index" => 315, "label" => "Чехия", "subcategories" => []],
                ["index" => 319, "label" => "Швейцария", "subcategories" => []],
                ["index" => 321, "label" => "Франция", "subcategories" => []],
                ["index" => 320, "label" => "США", "subcategories" => []],
                ["index" => 316, "label" => "Испания", "subcategories" => []],
                ["index" => 310, "label" => "Россия", "subcategories" => []],
                ["index" => 311, "label" => "Египет", "subcategories" => []],
                ["index" => 312, "label" => "Турция", "subcategories" => []],
                ["index" => 313, "label" => "Италия", "subcategories" => []],
                ["index" => 314, "label" => "Греция", "subcategories" => []],
                ["index" => 533, "label" => "Германия", "subcategories" => []],
                ["index" => 318, "label" => "Другие страны", "subcategories" => []],
            ]
        ],
        [
            "index" => 449,
            "anchor" => "shops",
            "label" => "Магазины",
            "categories" => [
                ["index" => 324, "label" => "Подарки", "subcategories" => []],
                ["index" => 322, "label" => "Скидки", "subcategories" => []],
                ["index" => 323, "label" => "Распродажи", "subcategories" => []],
            ]
        ],
        [
            "index" => 450,
            "anchor" => "bars",
            "label" => "Кафе/бары",
            "categories" => [
                ["index" => 331, "label" => "Меню и напитки", "subcategories" => []],
                ["index" => 332, "label" => "Воки", "subcategories" => []],
                ["index" => 333, "label" => "Фуршеты", "subcategories" => []],
                ["index" => 334, "label" => "Другая кухня", "subcategories" => []],
                ["index" => 330, "label" => "Скидки на барную карту", "subcategories" => []],
                ["index" => 329, "label" => "Суши", "subcategories" => []],
                ["index" => 325, "label" => "Скидки", "subcategories" => []],
                ["index" => 326, "label" => "Подарки", "subcategories" => []],
                ["index" => 327, "label" => "Скидки на все меню", "subcategories" => []],
                ["index" => 328, "label" => "Скидки на все меню и напитки", "subcategories" => []],
            ]
        ],
    ];
}

function getGroups(){
    $groups = query("SELECT `index`, `anchor`, `label` FROM `nx_categories` WHERE `parent` IS NULL ORDER BY `order`")['rows'];
    foreach($groups as &$group){
        $group['index'] = (int)$group['index'];
        $categories = query("SELECT `index`, `label` FROM `nx_categories` WHERE `parent` = {$group['index']} ORDER BY `order`")['rows'];
        foreach($categories as &$category){
            $category['index'] = (int)$category['index'];
            $subcategories = query("SELECT `index`, `label` FROM `nx_categories` WHERE `parent` = {$category['index']} ORDER BY `order`")['rows'];
            foreach($subcategories as &$subcategory) {
                $subcategory['index'] = (int)$subcategory['index'];
            }
            $category['subcategories'] = $subcategories;
        }
        $group['categories'] = $categories;
    }
    return $groups;
}

function insertCategories(){
    $groupOrder = 0;
    foreach(getHardcodedGroups() as $group){
        if(query("SELECT * FROM `nx_categories` WHERE `index` = {$group['index']}")['num_rows'] == 0){
            query("INSERT INTO `nx_categories`(`index`,`anchor`,`label`,`parent`,`order`) VALUES({$group['index']},'{$group['anchor']}','{$group['label']}',NULL,{$groupOrder});");
            $groupOrder ++;
        }
        $categoryOrder = 0;
        foreach($group['categories'] as $category){
            if(query("SELECT * FROM `nx_categories` WHERE `index` = {$category['index']}")['num_rows'] == 0){
                query("INSERT INTO `nx_categories`(`index`,`label`,`parent`,`order`) VALUES({$category['index']},'{$category['label']}',{$group['index']},{$categoryOrder});");
                $categoryOrder ++;
            }
            $subcategoryOrder = 0;
            foreach($category['subcategories'] as $subcategory){
                if(query("SELECT * FROM `nx_categories` WHERE `index` = {$subcategory['index']}")['num_rows'] == 0){
                    query("INSERT INTO `nx_categories`(`index`,`label`,`parent`,`order`) VALUES({$subcategory['index']},'{$subcategory['label']}',{$category['index']},{$subcategoryOrder});");
                    $subcategoryOrder ++;
                }
            }
        }
    }
}

function getGroupId($groupIndex){
    return query("SELECT `index` FROM `nx_categories` WHERE `parent` IS NULL ORDER BY `order`")['rows'][$groupIndex]['index'];
}

function getSubcategories($categoryId){
    $subcategories = [];
    foreach(query("SELECT `index` FROM `nx_categories` WHERE `parent` = $categoryId")['rows'] as $row){
        $subcategories[] = $row['index'];
    }
    return $subcategories;
}

function sortByCount($f1, $f2)
{
    if (count($f1) < count($f2))
        return 1;
    elseif (count($f1) > count($f2))
        return -1;
    else
        return 0;
}

function forOwnerReferal($owner_id, $amount)
{
    $owner_id = (int)$owner_id;
    $amount = (float)$amount;
    static $costs = array(5, 15, 40, 70);

    $user = getUserById($owner_id);
    if (!$amount || !$owner_id || !$user || $user['referal']) {
        return false;
    }
    $level = isVer($user, true);
    if (!isset($costs[$level])) {
        return false;
    }
    $proc = $costs[$level];
    $amount = ($amount / 100) * $proc;

    query("INSERT INTO `nx_balance_log` (`user_id`,`amount`,`type`,`created`,`comment`) VALUES('$owner_id','$amount','1','" . time() . "','Отчисления от реферала');");

    updateUserById($user['id'], array('money'), array(($user['money'] + $amount)));

    return true;
}

function getCounterContacts($user_id)
{
    $user_id = (int)$user_id;
    $cache = cache_get('ccontacts.' . $user_id);
    if (!$cache) {
        $q = query("SELECT * FROM `nx_contacts` WHERE `contact_id`='{$user_id}' AND `counter`=0;");
        $r = $q['rows'];
        $w = array();
        foreach ($r as $contact) {
            $w[$contact['owner_id']] = array($contact['owner_id'], $contact['counter']);
        }
        $r = $w;
        cache_set('ccontacts.' . $user_id, $r);
    } else {
        $r = $cache;
    }
    return $r;
}

function getNotices($offset = 0, $limit = 3, $onlyNew = false)
{
    global $USER;
    $uid = $USER['id'];
    $offset = (int)$offset;
    $limit = (int)$limit;
    $user_groups = getUserGroups($USER['id'], 'all', false, false);
    if (!$user_groups) {
        return array();
    }
    $groups = array();
    foreach ($user_groups as $group) {
        $groups[] = $group['nabs'];
    }
    $sql = '';
    if ($onlyNew) {
        $sql = " AND `wall_id`>'" . $USER['last_wid'] . "'";
    }
    $q = query("SELECT * FROM `nx_wall` WHERE `user_id` IN (" . implode(',', $groups) . ") AND `repost`=0 AND `deleted`=0 AND `owner_id`!='$uid' $sql ORDER BY `created` DESC LIMIT $offset,$limit");

    return $q['rows'];
}


function getCountNotices($onlyNew = false)
{
    global $USER;
    $uid = $USER['id'];
    $user_groups = getUserGroups($USER['id'], 'all', false, false);
    if (!$user_groups) {
        return 0;
    }
    $groups = array();
    foreach ($user_groups as $group) {
        $groups[] = $group['nabs'];
    }
    $sql = '';
    if ($onlyNew) {
        $sql = " AND `wall_id`>'" . $USER['last_wid'] . "'";
    }
    $q = query("SELECT COUNT(*) as `count` FROM `nx_wall` WHERE `user_id` IN (" . implode(',', $groups) . ") AND `repost`=0 AND `deleted`=0 AND `owner_id`!='$uid' $sql ORDER BY `created` DESC");

    return $q['row']['count'];
}


function getUserGroups($user_id, $type, $onlyAdmin = false, $getGroups = true)
{
    $user_id = (int)$user_id;
    $type = (int)$type;
    $q = query("SELECT *, `group_id`*-1 as `nabs` FROM `nx_members` WHERE `user_id`='$user_id' AND `type`>=0 ORDER BY `created` DESC;");
    $groups = array();
    if (!$getGroups) {
        return $q['rows'];
    }
    foreach ($q['rows'] as $group) {
        $group = getGroup($group['group_id'], $type);
        if ($group) {
            $groups[] = $group;
        }
    }
    return $groups;
}

function getGroup($group_id, $onlyType = false)
{
    $group_id = (int)abs($group_id);
    if ($onlyType !== false) {
        $onlyType = (int)$onlyType;
        $q = query("SELECT * FROM `nx_groups` WHERE `group_id`='$group_id' AND `type`='$onlyType';");
    } else {
        $q = query("SELECT * FROM `nx_groups` WHERE `group_id`='$group_id';");
    }
    return $q['row'];
}


function image_check_memory_usage($img, $max_breedte, $max_hoogte)
{
    if (file_exists($img)) {
        $K64 = 65536; // number of bytes in 64K
        $memory_usage = memory_get_usage();
        $memory_limit = abs(intval(str_replace('M', '', ini_get('memory_limit')) * 1024 * 1024));
        $image_properties = getimagesize($img);
        $image_width = $image_properties[0];
        $image_height = $image_properties[1];
        $image_bits = $image_properties['bits'];
        $image_memory_usage = $K64 + ($image_width * $image_height * ($image_bits) * 2);
        $thumb_memory_usage = $K64 + ($max_breedte * $max_hoogte * ($image_bits) * 2);
        $memory_needed = intval($memory_usage + $image_memory_usage + $thumb_memory_usage);

        if ($memory_needed > $memory_limit) {
            ini_set('memory_limit', (intval($memory_needed / 1024 / 1024) + 5) . 'M');
            if (ini_get('memory_limit') == (intval($memory_needed / 1024 / 1024) + 5) . 'M') {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function fix_strtoupper($str)
{
    if (function_exists('mb_strtoupper')) {
        return mb_strtoupper($str);
    } else {
        return strtoupper($str);
    }
}

/**
 * Correct strtolower handling
 *
 * @param  string $str
 *
 * @return  string
 */
function fix_strtolower($str)
{
    if (function_exists('mb_strtoupper')) {
        return mb_strtolower($str);
    } else {
        return strtolower($str);
    }
}

function create_img($imgfile, $imgthumb, $newwidth, $newheight = null, $option = "crop")
{
    $timeLimit = ini_get('max_execution_time');
    set_time_limit(30);
    $result = false;
    if (image_check_memory_usage($imgfile, $newwidth, $newheight)) {
        $magicianObj = new ImageLib($imgfile);
        $magicianObj->resizeImage($newwidth, $newheight, $option);
        if (strpos($imgthumb, '.png') !== false) {
            $magicianObj->saveImage($imgthumb, 9);
        } else {
            $magicianObj->saveImage($imgthumb, 100);
        }
        $result = true;
    }
    set_time_limit($timeLimit);

    return $result;
}


function soc_ico($soc, $url)
{
    static $socs = array(
        'vk' => '<i class="pe-so-vk pe-color"></i>',
        'fb' => '<i class="pe-so-facebook pe-color"></i>',
        'tw' => '<i class="pe-so-twitter pe-color"></i>',
        'pt' => '<i class="pe-so-pinterest pe-color"></i>',
        'od' => '<i class="pe-so-odnolassniki pe-color"></i>',
        'yo' => '<i class="pe-so-youtube-1 pe-color"></i>',
        'ia' => '<i class="pe-so-instagram pe-color"></i>',
        'gm' => '<i class="pe-so-gm pe-color"></i>',
    );


    return '<a class="iconSoc" href="//' . $url . '" target="_blank">' . $socs[$soc] . '</a>';
}

/******************************/
function getTrc($id = false, $offset = 0, $count = 300)
{
    $offset = (int)$offset;
    $count = (int)$count;
    if ($id === false) {
        $q = query("SELECT * FROM `trc_list` ORDER BY `title` ASC LIMIT {$offset},{$count};");

        return $q['rows'];
    } else {
        $id = (int)$id;
        $q = query("SELECT * FROM `trc_list` WHERE `trc_id`='{$id}'");

        return $q['row'];
    }
}

function getTrcByCity($id)
{
    $id = (int)$id;
    $q = query("SELECT * FROM `trc_list` WHERE `city`='{$id}'");

    return $q['rows'];
}

function getTrcByCityAndName($id = array(), $title = '')
{
    $ids = array();
    if (!is_array($id)) {
        $t = $id;
        $id = array($t);
    }
    foreach ($id as $id_) {
        $ids[] = (int)$id_;
    }
    $ids = implode(',', $ids);
    $title = query_escape($title);
    $q = query("SELECT `trc_id` as `id`,`title` as `text` FROM `trc_list` WHERE `city` IN ({$ids}) AND `title` LIKE '%$title%'");

    return $q['rows'];
}

function getPromocode($action_id, $first_name, $last_name, $phone){
    $action_id = (int)$action_id;
    $first_name = query_escape($first_name);
    $last_name = query_escape($last_name);
    $phone = query_escape($phone);
    $q = query("SELECT `code`, `first_name`, `last_name`, `phone`,`amount` FROM `promocodes` WHERE `disabled` = FALSE
AND `action_id` = $action_id AND `first_name` = '$first_name' AND `last_name` = '$last_name' AND `phone` = '$phone';");
    return $q['row'];
}