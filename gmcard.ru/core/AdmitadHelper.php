<?php

/**
 * Created by PhpStorm.
 * User: V.lab
 * Date: 30.04.2017
 * Time: 17:54
 */
require_once 'StashCachePaths.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/core/StashCachePaths.php';

use GM\Modules\Session\Session;


class AdmitadHelper
{
    protected $api;
    protected $scope = 'advcampaigns advcampaigns_for_website coupons_for_website websites statistics';
    protected $pool;

    public function __construct()
    {
        $driver = new Stash\Driver\FileSystem(array('path' => $_SERVER['DOCUMENT_ROOT'] . '/tmp/gmcard/cache/admitadAPI/'));
        $this->pool = new Stash\Pool($driver);
        $this->api = $this->createApi();
    }

    public function getStatisticsForCampaign($userID, $date_start, $date_end, $website, $campaign)
    {
        $response = $this->api->get('/statistics/sub_ids/',
            array(
                'subid' => $userID,
                'date_start' => $date_start,
                'date_end' => $date_end,
                'website' => $website,
                'campaign' => $campaign,
                'limit' => 500
            ))->getResult()->results;
        return $response;
    }

    public function getStatistics($userID, $date_start, $date_end)
    {
        $response = $this->api->get('/statistics/sub_ids/',
            array(
                'subid' => $userID,
                'date_start' => $date_start,
                'date_end' => $date_end,
                'limit' => 500
            ))->getResult()->results;
        return $response;
    }

    public function getCouponsForCampaign($campaign)
    {
        $item = $this->pool->getItem(StashCachePaths::campaign . '/' . $campaign->id);
        if ($item->isMiss()) {
            $response = $this->api->
            get('/coupons/website/' . $campaign->website->id . '/',
                array(
                    'campaign' => $campaign->id,
                    'limit' => 500
                ))->getResult();
            $coupons = $response->results;
            foreach ($coupons as $coupon) {
                $coupon->image = $campaign->image;
            }
            $item->expiresAfter(7200);
            $item->lock();
            $item->set($coupons);
            $this->pool->save($item);
        }
        return $item->get();
    }

    public function cacheAllCampaigns()
    {
        $websites = $this->getWebsites()->results;
        $campaigns = array();
        foreach ($websites as $website) {
            $website_campaigns = $this->cacheCampaign($website);
            $campaigns = array_merge($campaigns, $website_campaigns);
        }
        return $campaigns;
    }

    public function cacheCampaign($website)
    {
        $item = $this->pool->getItem(StashCachePaths::all_campaigns . '/' . $website->id);
        if ($item->isMiss()) {
            $total_campaigns_count = $this->getCampaignsCount($website);
            $fetched_campaigns_count = 0;
            $limit = 500;
            $all_campaigns = array();
            do {

                $response = $this->api->get('/advcampaigns/website/' . $website->id . '/',
                    array(
                        'connection_status' => 'active',
                        'limit' => $limit,
                        'offset' => $fetched_campaigns_count
                    ))->getResult();

                $campaigns = $response->results;
                foreach ($campaigns as $campaign) {
                    $campaign->action = $this->findActionWithMaxCashback($campaign->actions);
                    $campaign->website = $website;
                }

                $all_campaigns = array_merge($all_campaigns, $campaigns->getArrayCopy());
                $fetched_campaigns_count += $limit;
            } while ($fetched_campaigns_count < $total_campaigns_count);

            $item->expiresAfter(3600);
            $item->lock();
            $item->set($all_campaigns);
            $this->pool->save($item);
        }
        return $item->get();
    }

    public function getCampaignsAndCouponsForStartPage()
    {
        $campaigns = $this->cacheAllCampaigns();
        $coupons = array();
        $popular_campaigns = array();
        foreach ($campaigns as $campaign) {
            if (intval($campaign->rating > 5)) {
                array_push($popular_campaigns, $campaign);
            }
            $campaign_coupons = $this->getCouponsForCampaign($campaign)->getArrayCopy();
            $coupons = array_merge($coupons, $campaign_coupons);
        }
        $result = array(
            'campaigns' => $campaigns,
            'popular_campaigns' => $popular_campaigns,
            'coupons' => $coupons,
            'user' => Session::get_instance()->get_uid());
        return $result;
    }

    protected function findActionWithMaxCashback($actions)
    {
        $maxCashback = 0;
        $result = null;
        foreach ($actions as $action) {
            if ($action->type === 'sale') {
                $cashback = $this->cashbackReducer($action->payment_size);
                if ($cashback > $maxCashback) {
                    $result = $action;
                    $maxCashback = $cashback;
                }
            }
        }
        return $result;
    }

    /**
     * Метод используется как вспомогательная функция для вычисления максимального кешбека.
     * @param $string string Строка с кэшбеком, полученная от API admitad
     * @return integer В случае, если строка с кешбеком содержит диапазон - возвращается максимальная граница.
     * Если кешбек задан одним процентным значением - возвращается значение кешбека
     * В ином случае - 0
     */
    protected function cashbackReducer($string)
    {
        if (substr($string, -1) !== '%') {
            return $string;
        }
        $isRangeCashback = strpos($string, "-");
        if ($isRangeCashback) {
            $maxCashback_start = strpos($string, "-") + 1;
            $maxCashback_length = strlen($string) - (strlen($string) - $maxCashback_start) - 1;
            $maxCashback = substr($string, $maxCashback_start, $maxCashback_length);
            return intval($maxCashback);
        }
        $cashback = substr($string, 0, -1);
        return intval($cashback);
    }

    protected function getCampaignsCount($website)
    {
        $response = $this->api->get('/advcampaigns/website/' . $website->id . '/',
            array(
                'connection_status' => 'active',
                'limit' => 1
            ))->getResult();
        return $response->_meta->count;
    }

    protected function getWebsites()
    {
        $result = $this->api->get('/websites/')->getResult();
        return $result;
    }

    protected function createApi()
    {
        $api = new Admitad\Api\Api();
        $authorizeResult = $api->authorizeClient(ADMITAD_CLIENT_ID, ADMITAD_SECRET, $this->scope)->getResult();
        $api = new Admitad\Api\Api($authorizeResult['access_token']);
        return $api;
    }
}