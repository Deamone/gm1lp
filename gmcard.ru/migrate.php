<?php
require_once './config.php';
require './core/lang/ru.php';
require './core/php-init.php';

function up()
{
    foreach (query("SELECT * FROM `nx_actions`")['rows'] as $action) {
        $time = unserialize($action['time']);
        $from = @$time['from'];
        $to = @$time['to'];
        if(@$from['minutes']) {
            $time['fromMinute'] = $from['minutes'];
        } else {
            $time['fromMinute'] = 0;
        }
        if(@$from['hours']) {
            $time['fromHour'] = $from['hours'];
        } else {
            $time['fromHour'] = 0;
        }
        if(@$to['minutes']) {
            $time['toMinute'] = $to['minutes'];
        } else {
            $time['toMinute'] = 59;
        }
        if(@$to['hours']) {
            $time['toHour'] = $to['hours'];
        } else {
            $time['toHour'] = 23;
        }
        unset($time['from']);
        unset($time['to']);
        $time = serialize($time);

        $cat = serialize(explode(",", trim($action['cat'], ",")));
        $city = serialize(explode(",", trim($action['city'], ",")));
        $trc = serialize(explode(",", trim($action['trc'], ",")));
        $days = serialize(explode(",", trim($action['days'], ",")));

        $site = unserialize($action['site']);
        $address = serialize([
            'graph' => $action['graph'],
            'phone' => $action['phone'],
            'address' => $action['address'],
            'site' => @$site['over'],
            'longitude' => $action['longitude'],
            'latitude' => $action['latitude']
        ]);
        unset($site['over']);
        $site = serialize($site);

        $daddress = [];

        foreach (unserialize($action['daddress']) as $item) {
            if(@$item[2]){
                $daddress[] = [
                    'graph' => $item[0],
                    'phone' => $item[1],
                    'address' => $item[2],
                    'site' => $item[3],
                    'longitude' => @$item[4],
                    'latitude' => @$item[5]
                ];
            }
        }
        $daddress = serialize($daddress);

        query("UPDATE `nx_actions`
        SET `time` = '$time', `cat` = '$cat', `city` = '$city', `trc` = '$trc', `days` = '$days', `site` = '$site', `address` = '$address', `daddress` = '$daddress'
        WHERE `action_id` = {$action['action_id']}");
    }
}

up();