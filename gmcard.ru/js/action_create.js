jQuery(function($){
    $('input[name="cost[type]"]').on('change', function(e){
        var input = $(e.target),
            value = input.filter(':checked').val(),
            selected = value ? "cashback" : "discount",
            other = value ? "discount" : "cashback";
        input.parent().siblings('.span-switch').each(function(i, item){
            if($(item).is("." + selected))
                $(item).addClass('selected');
            else
                $(item).removeClass('selected');
        }).parent().siblings('.action_cost').each(function(i, item){
            if($(item).is("." + selected)) {
                $(item).show().find('[name^="cost"]').removeAttr('disabled');
            }
            if($(item).is("." + other)) {
                $(item).hide().find('[name^="cost"]').attr('disabled', 'disabled');
            }
        });
    }).trigger('change');
});