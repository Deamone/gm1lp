if ((typeof $.fn.bjqs) != 'function') {
    $.fn.bjqs = function () {
    };
}
var KEY = window.KEY = {
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    DEL: 8,
    TAB: 9,
    RETURN: 13,
    ENTER: 13,
    ESC: 27,
    PAGEUP: 33,
    PAGEDOWN: 34,
    SPACE: 32,
    CTRL: 17,
    BACKSPACE: 8
};

if (!window.jt)
    jt = {};
jt['cookies'] = '1.0.0';

cookies = {
    cookies: null,
    set: function (name, value, days) {
        if (!this.cookies)
            this.init();
        this.cookies[name] = value;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else
            var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    get: function (name) {
        if (!this.cookies)
            this.init();
        return this.cookies[name];
    },
    init: function () {
        this.cookies = {};
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].split("=");
            if (c.length == 2) {
                this.cookies[c[0].trim()] = c[1].trim();
                //this.cookies[c[0].match(/^[\s]*([^\s]+?)$/i)[1]] = c[1].match(/^[\s]*([^\s]+?)$/i)[1];
            }
        }
    }
};
function clickable(codeElems) {
    //var codeElems = $(".code"); //jQuery
    for (var c = 0; c < codeElems.length; c++) {
        var content = codeElems[c].innerHTML;
        if (content.indexOf('<noindex>') !== -1) {
            continue;
        }
        content = content.replace(/\<br/gi, "\n<br");
        var pattern = /((https?\:\/\/|ftp\:\/\/)|(www\.))(\S+)(\w{2,4})?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi;
        var pattern = /([-a-zA-Z0-9@:%_\+.~#?&\/\/=]{2,256}\.[a-z]{2,4}\b(\/?[-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)?)/gi;
        content = content.replace(pattern, function (url) {
            nice = url;
            if (url.match('^https?:\/\/'))
                nice = nice.replace(/^https?:\/\//i, '')
            else
                url = 'http://' + url;
            /* Вовзращаем кликабельный урл, завернутый в nofollow и яндексовый noindex: */
            return '<noindex><a target="_blank" rel="nofollow" href="' + url + '">' + nice.replace(/^www./i, '') + '</a></noindex>';
        });
        codeElems[c].innerHTML = content;
    }
}
function serialize(mixed_value) {
    //  discuss at: http://phpjs.org/functions/serialize/
    // original by: Arpad Ray (mailto:arpad@php.net)
    // improved by: Dino
    // improved by: Le Torbi (http://www.letorbi.de/)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net/)
    // bugfixed by: Andrej Pavlovic
    // bugfixed by: Garagoth
    // bugfixed by: Russell Walker (http://www.nbill.co.uk/)
    // bugfixed by: Jamie Beck (http://www.terabit.ca/)
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net/)
    // bugfixed by: Ben (http://benblume.co.uk/)
    //    input by: DtTvB (http://dt.in.th/2008-09-16.string-length-in-bytes.html)
    //    input by: Martin (http://www.erlenwiese.de/)
    //        note: We feel the main purpose of this function should be to ease the transport of data between php & js
    //        note: Aiming for PHP-compatibility, we have to translate objects to arrays
    //   example 1: serialize(['Kevin', 'van', 'Zonneveld']);
    //   returns 1: 'a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}'
    //   example 2: serialize({firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'});
    //   returns 2: 'a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}'

    var val, key, okey,
        ktype = '',
        vals = '',
        count = 0,
        _utf8Size = function (str) {
            var size = 0,
                i = 0,
                l = str.length,
                code = '';
            for (i = 0; i < l; i++) {
                code = str.charCodeAt(i);
                if (code < 0x0080) {
                    size += 1;
                } else if (code < 0x0800) {
                    size += 2;
                } else {
                    size += 3;
                }
            }
            return size;
        };
    _getType = function (inp) {
        var match, key, cons, types, type = typeof inp;

        if (type === 'object' && !inp) {
            return 'null';
        }
        if (type === 'object') {
            if (!inp.constructor) {
                return 'object';
            }
            cons = inp.constructor.toString();
            match = cons.match(/(\w+)\(/);
            if (match) {
                cons = match[1].toLowerCase();
            }
            types = ['boolean', 'number', 'string', 'array'];
            for (key in types) {
                if (cons == types[key]) {
                    type = types[key];
                    break;
                }
            }
        }
        return type;
    };
    type = _getType(mixed_value);

    switch (type) {
        case 'function':
            val = '';
            break;
        case 'boolean':
            val = 'b:' + (mixed_value ? '1' : '0');
            break;
        case 'number':
            val = (Math.round(mixed_value) == mixed_value ? 'i' : 'd') + ':' + mixed_value;
            break;
        case 'string':
            val = 's:' + _utf8Size(mixed_value) + ':"' + mixed_value + '"';
            break;
        case 'array':
        case 'object':
            val = 'a';
            /*
             if (type === 'object') {
             var objname = mixed_value.constructor.toString().match(/(\w+)\(\)/);
             if (objname == undefined) {
             return;
             }
             objname[1] = this.serialize(objname[1]);
             val = 'O' + objname[1].substring(1, objname[1].length - 1);
             }
             */

            for (key in mixed_value) {
                if (mixed_value.hasOwnProperty(key)) {
                    ktype = _getType(mixed_value[key]);
                    if (ktype === 'function') {
                        continue;
                    }

                    okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
                    vals += this.serialize(okey) + this.serialize(mixed_value[key]);
                    count++;
                }
            }
            val += ':' + count + ':{' + vals + '}';
            break;
        case 'undefined':
        // Fall-through
        default:
            // if the JS object has a property which contains a null value, the string cannot be unserialized by PHP
            val = 'N';
            break;
    }
    if (type !== 'object' && type !== 'array') {
        val += ';';
    }
    return val;
}

function tip(data_) {
    if (data_.parent().hasClass('tip_wrap')) {
        data_.parent().empty().before(data_);
        data_.parent().find('tip_wrap').remove();
    }


    /*if (data_.parent().parent().hasClass('tip_wrap')) {
     console.log(data_.parent().parent().html());
     var q = data_.parent().parent().html();
     data_.parent().parent().empty().before(q);
     }*/
    var data = data_.attr("data-tip");
    var mood = data_.attr("data-mood");
    var pos = data_.attr("data-position");

    if (!data) {
        return false;
    }

    if (data_.is("select")) {
        if (!data_.parent().parent().hasClass('tip_wrap')) {
            data_.parent().wrap("<div class='tip_wrap'></div>");
        } else {
            data_.parent().parent().find('.tooltip').remove();
        }
    } else {
        if (!data_.parent().hasClass('tip_wrap')) {
            data_.wrap("<div class='tip_wrap'></div>");
        } else {
            data_.parent().find('.tooltip').remove();
        }
    }
    var innerjeight = data_.innerHeight();
    var innerwidth = data_.innerWidth();
    var outerheight = data_.outerHeight(true);


    var mTop = data_.css("margin-top");
    var mLeft = data_.css("margin-left");
    var oSimg = data_.position();
    var topposition = parseInt(mTop, 10);
    var leftposition = parseInt(mLeft, 10);

    if (pos == "bottom") {
        topposition = topposition + innerjeight + 8;
        direction = "top";
    } else if (pos == "right") {
        if (data_.hasClass('round_button')) {
            leftposition += 10;
            topposition -= 6;
        }
        leftposition = leftposition + innerwidth + 10;
        direction = "top"; // nx old top
    } else if (pos == "top") {
        topposition = outerheight - parseInt(mTop, 10) + 8;
        direction = "bottom";
    }

    // console.log($(this).parent());

    data_.after("<div class='tooltip " + mood + " " + pos + "' style='" + direction + ": " + topposition + "px; left: " + leftposition + "px;'>" + data + "<div class='triangle'></div></div>");

    var k = data_.next();

    function nt() {
        $(k).hide();
        $(this).unbind('click');
    }

    data_.next().bind('click', nt);
    data_.bind('click', nt);
}
function parseErrors(data, separator, return_) {
    if (data.errors === undefined) {
        return;
    }
    if (separator === undefined) {
        var separator = "\n";
    }
    if (return_ === undefined) {
        return_ = false;
    }
    var errors = '';
    var i = '';
    var text = '';
    var position = 'right';
    if ((typeof data.errors) == 'string') {
        data.errors = {0: data.errors};
    }
    for (i in data.errors) {
        if ($(i).size()) {
            console.log();
            if ((typeof data.errors[i]) === 'object') {
                text = data.errors[i][0];
                position = data.errors[i][1];
            } else {
                text = data.errors[i];
                position = 'right';
            }
            $(i).addClass('tip').attr('data-position', position).attr('data-mood', 'negative').attr('data-tip', text);
            tip($(i));
            $(i).addClass('error').bind('click', function () {
                $(this).removeClass('error');
                /*if ($(this).next().hasClass('tooltip')) {
                 $(this).next().remove();
                 }*/
                var dep = 4;
                var container = $(this);
                while (dep > 0) {
                    if (container.hasClass('tip_wrap')) {
                        console.log(dep, container);
                        container.find('.tooltip').remove();
                        break;
                    }
                    container = container.parent();
                    dep--;
                }
                if ($(this).parent().hasClass('tip_wrap')) {
                    $(this).parent().empty().before($(this));
                    $(this).parent().find('tip_wrap').remove();
                    $(this).focus();
                }
            });
        }
        errors = errors + text + separator;
    }

    return return_ ? errors : null;
}
function parseSuccess(data, separator) {
    if (data.success === undefined) {
        return;
    }
    if (separator === undefined) {
        var separator = "\n";
    }
    var success = '';
    var i = '';
    if ((typeof data.success) == 'string') {
        data.success = {0: data.success};
    }
    for (i in data.success) {
        if ($(i).size()) {
            $(i).addClass('success').bind('click', function () {
                $(this).removeClass('success');
            });
        }
        success = success + data.success[i] + separator;
    }

    return success;
}
function parseEval(data) {
    if (data.eval === undefined) {
        return;
    }
    if ((typeof data.eval) == 'string') {
        data.eval = {0: data.eval};
    }
    var i = '';
    for (i in data.eval) {
        try {
            eval(data.eval[i]);
        } catch (e) {
            console.log('eval fail', e);
        }
    }
}
function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}


var city = cookies.get('city') ? cookies.get('city') : 0;
var city_name = cookies.get('city_name') ? cookies.get('city_name') : 0;
var trc = cookies.get('trc') ? cookies.get('trc') : 0;
var trc_name = cookies.get('trc_name') ? cookies.get('trc_name') : 0;
$(document).ready(function () {
    if (!$('#popup_city li[input-id="city_' + city + '"]').size()) {
        $('#city_text').text(city_name);
        $('#popup_city ul').append('<li input-id="city_' + city + '" class="checkbox_wrapper checked">' + city_name + '</li>');
        $('#popup_city ul:before').append('<input type="radio" name="city" id="city_' + city + '" value="' + city + '"> <label for="city_' + city + '" style="display: none;">' + city_name + '</label>');
    }
    if (!$('#popup_trc li[input-id="trc_' + trc + '"]').size()) {
        $('#trc_text').text(trc_name);
        $('#popup_trc ul').append('<li input-id="trc_' + city + '" class="checkbox_wrapper checked">' + trc_name + '</li>');
        $('#popup_trc ul:before').append('<input type="radio" name="trc" id="trc_' + trc + '" value="' + trc + '"> <label for="trc_' + trc + '" style="display: none;">' + trc_name + '</label>');
    }else{
        $('#trc_text').text(trc_name);
    }
    $('#popup_city li[input-id="city_' + city + '"]').click();
    $('#popup_trc li[input-id="trc_' + trc + '"]').click();
    $('#popup_city input').change(function () {
        if ($(this).val() == '01') {
            window.location.href = '/country';
            return false;
        }
        console.log($(this).val());
        cookies.set('city', $(this).val(), 30);
        cookies.set('city_name', $('#city_text').text(), 30);
        console.log($('#city_' + $(this).val() + '').html());
        window.location.reload();
    });
    $('#popup_trc input').change(function () {
        if ($(this).val() == '01') {
            window.location.href = '/country';
            return false;
        }
        console.log($(this).val());
        cookies.set('trc', $(this).val(), 30);
        cookies.set('trc_name', $(this).data('title'), 30);
        console.log($('#trc_' + $(this).val() + '').html());
        window.location.reload();
    });
    if ($('.desireTime').size()) {
        $('.desireTime').each(function () {
            var desire = $(this);
            stop = 0;
            if (desire.attr('data-stop') !== undefined) {
                stop = 1;
            }
            $(this).countdown({
                timestamp: desire.attr('data-etime') + '000',
                start: desire.attr('data-ctime') + '000',
                callback: function (a, b, c, d, timer) {
                    var day = '';
                    if (b < 10) {
                        b = '0' + b;
                    }
                    if (c < 10) {
                        c = '0' + c;
                    }
                    if (d < 10) {
                        d = '0' + d;
                    }
                    if (a) {
                        desire.find('.a_time_days').html(' ' + a + ' ' + declOfNum(a, ['день', 'дня', 'дней']) + ' ');
                    } else {
                        desire.find('.a_time_days').html(' ');
                    }
                    desire.find('.a_time_time').html('' + b + ':' + c + ':' + d + '');
                    if (stop) {
                        clearInterval(timer);
                    }
                }
            });
        });
    }
    if ($('.actionTime').size()) {
        runCountDown();
    }

    $('input[name="categories"]').change(function () {
        $('.tab_hide').hide();
        $('#tab_' + $(this).val()).show();
    });

    $('.addToContact2').on('click', function () {
        var id = $(this).attr('data-id');
        var vthis = $(this);
        $.post('//gm1lp.ru/contacts?act=add', 'id=' + id, function (data) {
            if (data == 1) {
                vthis.removeClass('addToContact');
                vthis.attr('href', '#');
                vthis.text('Заявка принята');
            }
            if (data == 2) {
                vthis.removeClass('addToContact');
                vthis.attr('href', '#');
                vthis.text('Заявка отправлена');
            }
            vthis.unbind('click');
        });

        return false;
    });
    function removeContact() {
        var id = $(this).attr('data-id');
        var vthis = $(this);
        $.post('//gm1lp.ru/contacts?act=remove', 'id=' + id, function (data) {
            vthis.removeClass('unContact');
            vthis.attr('href', '#');
            vthis.attr('data-status', 3);
            vthis.text('Заявка отклонена');
            vthis.unbind('click');
            resetContact();
        });

        return false;

    }

    function addContact() {
        var id = $(this).attr('data-id');
        var vthis = $(this);
        $.post('//gm1lp.ru/contacts?act=add', 'id=' + id, function (data) {
            /***
             * 1 = Заяка принята
             * 2 = Заявка отправлена
             */
            if (data == 1) {
                vthis.removeClass('addToContact');
                vthis.attr('href', '#');
                vthis.text('Заявка принята');
                vthis.attr('data-status', 1);
            }
            if (data == 2) {
                vthis.removeClass('addToContact');
                vthis.attr('href', '#');
                vthis.text('Заявка отправлена');
                vthis.attr('data-status', 0);
            }
            resetContact();
            vthis.unbind('click');
        });

        return false;
    }

    $('.unContact').on('click', removeContact);
    ////////////////////////

    $('.addToContact').on('click', addContact);
    function resetContact() {
        $('.icontact').each(function () {
            var status = $(this).attr('data-status');
            if (status === undefined) {
                return;
            }
            $this = $(this);
            $(this).unbind('mouseenter').unbind('mouseleave');
            switch (parseInt(status)) {
                case 1:
                    $(this).text('Контакт установлен');
                    $(this).hover(function () {
                        $(this).unbind('click');
                        $(this).bind('click', removeContact);
                        $(this).text('Удалить контакт');
                    }, function () {
                        $(this).unbind('click');
                        $(this).text('Контакт установлен');
                    });
                    break;
                case 0:
                    $(this).text('Отменить заявку');
                    $(this).hover(function () {
                        $(this).unbind('click');
                        $(this).bind('click', removeContact);
                        $(this).text('Отменить заявку');
                    }, function () {
                        $(this).unbind('click');
                        $(this).text('Отменить заявку');
                    });
                    break;
                case -1:
                    $(this).text('Установить контакт');
                    $(this).unbind('click');
                    $(this).bind('click', addContact);
                    break;
                case 3:
                    $(this).text('Заявка отклонена');
                    $(this).unbind('click');
                    $(this).hover(function () {
                        $this.text('Заявка отклонена');
                    }, function () {
                        $this.text('Заявка отклонена');
                    });
                    break;
            }
        });
    }

    resetContact();

    $('.openDialog').on('click', function () {
        var id = $(this).attr('data-id');
        if (id === undefined) {
            return;
        }
        window.location.href = '//gm1lp.ru/message?id=' + id;

        return false;
    });
    if ($.fancybox !== undefined) {
        $('.fancybox').fancybox({
            'padding': [20, 20, 20, 20],
            'beforeShow': function () {
                //$('.fancybox-skin').after('<div class="loading">Идет загрузка</div>');
            },
            'afterShow': function () {
                /*$this = this.element;

                 $.post('/album/album-ajax', 'photo_id='+$this.attr('data-photo-id'), function (data){
                 $('.fancybox-skin').after(data);
                 $('.fancybox-skin').parent().find('.loading').remove();
                 });*/
                return false;
            }
        });
    }
    $(document).on('change', 'input[name^=site]', function () {
        var val = $(this).val();
        val = val.replace(/http(.?):\/\//i, '');
        if (val.indexOf('www')) {
            val = val.replace('www.', '');
        }
        $(this).val(val);
    });
});


function updateNoticeMessage() {
    $counter = $('.tb_messages .tb_amount');
    //$counter.hide();
    jQuery.support.cors = true;
    $.ajax({
        xhrFields: {
            // withCredentials: true
        },
        data: "",
        type: "POST",
        dataType: 'json',
        url: "https://gm1lp.ru/messages?notice",
        crossDomain: true
    }).done(function (data) {
        if (data.counter && data.counter != parseInt($counter.text())) {
            $counter.text(data.counter).show();
        }
        if (data.counter = 0) {
            $counter.text(data.counter).hide();
        }
        try {
            var length = Math.min(data.notice.length, 7);
            for (var i = 0; i < length; i++) {
                var notice = data.notice[i];
                var text = notice.message;
                var tmp = text.replace(/<.*?>/g, '');
                if (tmp.length > 121) {
                    text = text.substring(0, 121) + '...';
                }
                if ($('#popup_messages div[data-id="' + notice.id + '"]').size()) {
                    var time = parseInt($('#popup_messages div[data-id="' + notice.id + '"]').attr('data-time'));
                    var flag = parseInt($('#popup_messages div[data-id="' + notice.id + '"]').attr('data-flag'));
                    if (notice.time > time || notice.flags != flag) {
                        $('#popup_messages div[data-id="' + notice.id + '"]').remove();
                    } else {
                        continue;
                    }
                }
                $('#popup_messages').append('<div data-id="' + notice.id + '" data-flag="' + notice.flags + '" data-time="' + notice.time + '" class="popup_row sendMessageMiniDialog' + (i + 1 == length ? ' last ' : '') + (/*notice.flags == 6 || */notice.flags == 2 ? ' hlighted' : '') + '"><div class="popup_col_img"><a href="https://gm1lp.ru/message?id=' + notice.id + '"><img class="rounded_3" src="https://gm1lp.ru' + notice.ava + '" width="59" height="59"></a></div><div class="popup_col_text"> <span class="title"><a href="https://gm1lp.ru/id' + notice.id + '">' + notice.name + '</a></span><br><span class="text">' + text + '</span><br><span class="date" data-s_time="' + notice.time + '"></span> </div></div>');
            }
        } catch (e) {
        }
    });
    /*$.post('//gm1lp.ru/messages?notice', '', function (data) {

     }, 'json');*/
}
updateNoticeMessage();
setInterval(updateNoticeMessage, 5000);
function runCountDown() {
    $('.actionTime').each(function () {
        var action = $(this);
        stop = 0;
        if (action.attr('data-stop') !== undefined) {
            stop = 1;
        }
        if (action.attr('data-countdown-run') == 1) {
            return false;
        }
        $(this).countdown({
            timestamp: action.attr('data-etime') + '000',
            start: action.attr('data-ctime') + '000',
            callback: function (a, b, c, d, timer) {
                var day = '';
                if (a) {
                    day = ' ' + a + ' ' + declOfNum(a, ['день', 'дня', 'дней']) + ' ';
                }
                if (b < 10) b = '0' + b;
                if (c < 10) c = '0' + c;
                if (d < 10) d = '0' + d;
                action.find('.a_time').html(day + '' + b + ':' + c + ':' + d + '');
                action.attr('data-countdown-run', 1);
                if (stop) {
                    clearInterval(timer);
                }
            }
        });
    });
}
/*
 var i = 1, j = 0;
 var x = 0, y = 0, z = 1, arr = {};

 var sql = "INSERT INTO `nx_discount_cat` (`cat_id`,`name`) VALUES";
 var repl = '';
 function runCountDown() {
 $('.actionTime').each(function () {
 var action = $(this);
 stop = 0;
 if (action.attr('data-stop') !== undefined) {
 stop = 1;
 }
 if (action.attr('data-countdown-run') == 1) {
 return false;
 }
 $(this).countdown({
 timestamp: action.attr('data-etime') + '000',
 start: action.attr('data-ctime') + '000',
 callback: function (a, b, c, d, timer) {
 var day = '';
 if (a) {
 day = ' ' + a + ' ' + declOfNum(a, ['день', 'дня', 'дней']) + ' ';
 }
 action.find('.a_time').html(day + '' + b + ':' + c + ':' + d + '');
 action.attr('data-countdown-run', 1);
 if (stop) {
 clearInterval(timer);
 }
 }
 });
 });
 }
 function catLink() {
 $menu = $('.popup.popup_menu');
 $menu.each(function () {
 x = 0;
 $maincat = $(this).find('.cat_link a');
 $subcat = $('.subcat_links a');
 arr[z] = {};
 arr[z][0] = [z, $('li[popup-target="' + $(this).attr('id') + '"] a').html()];
 $maincat.each(function () {
 sql = sql + "('" + (i) + "','" + $(this).html() + "'),";

 $subcat = $(this).parent().next().find('li');
 arr[z][++x] = {};
 arr[z][x][j] = [i++, $(this).html()];
 repl = repl + '$q = str_replace(\'' + $(this).attr('href') + '\',\'' + '/?category=' + (i - 1) + '\', $q);' + "\n";
 $(this).attr('href', '/?category=' + (i - 1) + '');
 $subcat.each(function () {
 var a = $(this).find('a');
 arr[z][x][i] = [i, a.html()];
 repl = repl + '$q = str_replace(\'' + a.attr('href') + '\',\'' + '/?category=' + (i) + '\', $q);' + "\n";
 a.attr('href', '/?category=' + i);
 sql = sql + "('" + (i++) + "','" + a.html() + "'),";
 });
 });
 z++;
 });

 $.post('/?vq=1', arr, function () {
 });
 console.log(sql);
 }*/
