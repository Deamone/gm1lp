<?php

namespace GM\GMCard\DAO;

use GM\Modules\Session\Session;


class Action
{
    const BANNER_MULTIPLIER = 3;

    /**
     * @var int
     */
    private $action_id;
    /**
     * @var string
     */
    private $title = "";
    /**
     * @var string
     */
    private $condition = "";
    /**
     * @var Site
     */
    private $site = null;
    /**
     * @var int
     */
    private $end = 864000;
    /**
     * @var bool
     */
    private $desire = false;
    /**
     * @var Address
     */
    private $address = null;
    /**
     * @var Address[]
     */
    private $daddress = array();
    /**
     * @var Cost
     */
    private $cost = null;
    /**
     * @var int
     */
    private $created = 0;
    /**
     * @var int
     */
    private $like = 0;
    /**
     * @var string[]
     */
    private $images = array();
    /**
     * @var int
     */
    private $user_id = 0;
    /**
     * @var int[]
     */
    private $cat = array();
    /**
     * @var int[]
     */
    private $city = array();
    /**
     * @var int[]
     */
    private $trc = array();
    /**
     * @var int
     */
    private $upping = 0;
    /**
     * @var bool
     */
    private $special_condition = false;
    /**
     * @var bool
     */
    private $days_enabled = false;
    /**
     * @var int[]
     */
    private $days = array();
    /**
     * @var Time
     */
    private $time = null;

    /**
     * @var int
     */
    private $day = 0;

    /**
     * @var Action|null
     */
    private $action = null;

    /**
     * @param int $cat
     * @param bool $limit
     * @param int $offset
     * @return Action[]
     */
    public static function get($cat, $limit = false, $offset = 0){
        $time = time();
        $city = isset($_COOKIE['city']) ? (int)$_COOKIE['city'] : 0;
        $trc = isset($_COOKIE['trc']) && $_COOKIE['trc'] ? (int)$_COOKIE['trc'] : '';
        $cat = (int)$cat;
        $offset = (int)$offset;
        if ($limit !== false) {
            $limit = " LIMIT {$offset}, " . (int)$limit;
        } else {
            $limit = '';
        }
        $sql = '';
        if($trc){
            $sql = "AND (`trc` LIKE '%\"{$trc}\"%')";
        }
        if (!$cat) {
            $q = query("SELECT * FROM `nx_actions` WHERE `end`>{$time} AND (`city` like '%\"{$city}\"%') $sql ORDER BY upping DESC, `created` DESC " . $limit);
        } else {
            $q = query("SELECT * FROM `nx_actions` WHERE `end`>{$time} AND (`city` like '%\"{$city}\"%') $sql AND `cat` like '%\"{$cat}\"%' ORDER BY `created` DESC " . $limit);
        }
        $actions = [];
        foreach($q['rows'] as $action){
            $actions[] = new Action($action);
        }
        return $actions;
    }

    /**
     * @param int $id
     * @return Action[]
     */
    public static function getByUid($id){
        $id = (int)$id;
        $q = query("SELECT * FROM `nx_actions` WHERE `user_id`='{$id}'");
        $actions = [];
        foreach($q['rows'] as $action){
            $actions[] = new Action($action);
        }
        return $actions;
    }

    /**
     * @param int $id
     * @return Action[]
     */
    public static function getCouponsByUid($id){
        $id = (int)$id;
        $q = query("SELECT * FROM `nx_actions` WHERE `user_id`='{$id}' AND `cost` LIKE '%s:4:\\\"type\\\";s:8:\\\"cashback\\\"%'");
        $actions = [];
        foreach($q['rows'] as $action){
            $actions[] = new Action($action);
        }
        return $actions;
    }

    /**
     * @param int $id
     * @param bool $limit
     * @param int $offset
     * @return Action[]
     */
    public static function getWithCouponsByUid($id, $limit = false, $offset = 0)
    {
        $id = (int)$id;
        $offset = (int)$offset;
        if ($limit !== false) {
            $limit = " LIMIT {$offset}, " . (int)$limit;
        } else {
            $limit = '';
        }

        $q = query("SELECT `nx_actions`.* FROM `nx_actions` JOIN `promocodes` ON(`promocodes`.`action_id` = `nx_actions`.`action_id`)
WHERE `promocodes`.`uid`='{$id}' AND `promocodes`.`disabled` = FALSE ORDER BY `upping` DESC, `created` DESC $limit");
        $actions = [];
        foreach($q['rows'] as $action){
            $actions[] = new Action($action);
        }
        return $actions;
    }

    /**
     * @param int[] $cat
     * @param string $q
     * @param int|bool $limit
     * @return Action[]
     */
    public static function find($cat = array(), $q, $limit = false){
        $time = time();
        $city = isset($_COOKIE['city']) ? (int)$_COOKIE['city'] : 0;
        $trc = isset($_COOKIE['trc']) && $_COOKIE['trc'] ? (int)$_COOKIE['trc'] : '';
        $q = query_escape($q);
        if (!$q) {
            return array();
        }
        if ($limit !== false) {
            $limit = " LIMIT 0, " . $limit;
        } else {
            $limit = '';
        }
        $sql = '';
        if($trc){
            $sql = "AND (`trc` LIKE '%\"{$trc}\"%')";
        }
        if (empty($cat) || ($cat[0] == -1)) {
            $qw = query("SELECT * FROM `nx_actions` WHERE  (`city` like '%\"{$city}\"%') $sql AND (`title` LIKE '%{$q}%' OR `condition` LIKE '%{$q}%') ORDER BY `end` DESC" . $limit);
        } else {
            $categories = [];
            foreach ($cat as $groupIndex) {
                foreach(getSubcategories(getGroupId($groupIndex)) as $subcategory){
                    $categories[] = " `cat` LIKE '%\"{$subcategory}\"%' ";
                }
            }
            $qw = query("SELECT * FROM `nx_actions` WHERE (`city` like '%\"{$city}\"%') $sql AND (" . implode(' OR ', $categories) . ") AND (`title` LIKE '%{$q}%' OR `condition` LIKE '%{$q}%') ORDER BY `end` DESC " . $limit);
            #echo "SELECT * FROM `nx_actions` WHERE `end`>{$time} AND (`city` like '%\"{$city}\"%') AND (".  implode(' OR ', $cat).") AND (`title` LIKE '%{$q}%' OR `condition` LIKE '%{$q}%') ".$limit;
        }
        $actions = [];
        foreach($qw['rows'] as $action){
            $actions[] = new Action($action);
        }
        return $actions;
    }

    /**
     * @param $action_id
     * @return Action
     */
    public static function getById($action_id){
        $action_id = (int)$action_id;
        $q = query("SELECT * FROM `nx_actions` WHERE `action_id`='{$action_id}'");
        return new Action($q['row']);
    }

    /**
     * Action constructor.
     * @param array|null $array
     * @param bool $serialized
     */
    public function __construct($array = null, $serialized = true){
        if(empty($array)) {
            $this->site = new Site();
            $this->address = new Address();
            $this->cost = new Cost();
            $this->time = new Time();
        } else {
            $this->action_id = (int)@$array['action_id'];
            $this->title = @$array['title'];
            $this->condition = @$array['condition'];
            $this->desire = (bool)@$array['desire'];
            $this->special_condition = @$array['special_condition'];
            $this->days_enabled = (bool)@$array['days_enabled'];

            $this->site = @new Site(@$array['site'], $serialized);
            $this->address = @new Address(@$array['address'], $serialized);
            $this->daddress = @Address::newArray(@$array['daddress'], $serialized);
            $this->cost = @new Cost(@$array['cost'], $serialized);
            $this->time = @new Time(@$array['time'], $serialized);

            if($serialized) {
                $this->images = unserialize(@$array['images']);
                $this->cat = unserialize(@$array['cat']);
                $this->city = unserialize(@$array['city']);
                $this->trc = unserialize(@$array['trc']);
                $this->days = unserialize(@$array['days']);
                if(empty($this->days)){
                    $this->days = array();
                }

                $this->created = (int)@$array['created'];
                $this->end = (int)@$array['end'];
                $this->upping = (int)@$array['upping'];
                $this->like = (int)@$array['like'];
                $this->user_id = (int)@$array['user_id'];
            } else {
                $this->images = @$array['images'];
                $this->cat = @$array['cat'];
                $this->city = @$array['city'];
                $this->trc = @$array['trc'];
                $this->days = @$array['days'];

                $this->day = (int)@$array['day'];
            }
        }
    }

    /**
     * @param $errorsArray
     * @param $errorsString
     * @param bool $preview
     * @return int
     */
    public function save(&$errorsArray, &$errorsString, $preview = true){
        if(!empty($errorsArray) || !empty($errorsString)){
            return false;
        }
        $user_id = Session::get_instance()->get_uid();

        if($user_id <= 0){
            $errorsString .= 'Недостаточно прав<br/>';
            return false;
        }
        if($this->action_id > 0) {
            $this->action = Action::getById($this->action_id);
            if(!$this->action->action_id || $this->action->user_id <= 0 || $this->action->user_id != $user_id){
                $errorsString .= 'Недостаточно прав<br/>';
                return false;
            }
        }
        if ($this->day < 1 || $this->day > 99) {
            $errorsArray['input[name="day"]'] = array('Минимум 1 день, максимум 99', 'bottom');
        }
        if(!$this->chargeUser()) {
            $errorsString .= 'Баланса недостаточно<br/>';
            return false;
        }
        if(!$this->validate($errorsArray, $errorsString)){
            return false;
        }
        if($preview) {
            return 0;
        }
        if(array_search("0", $this->city) === false) {
            $this->city[] = "0";
        }

        $this->chargeUser(true);

        $time = time();
        $data = [
            # "`action_id`" => $this->action_id,
            "`title`" => "'" . query_escape($this->title) . "'",
            "`condition`" => "'" . query_escape($this->condition) . "'",
            "`desire`" => $this->desire ? "true" : "false",
            "`special_condition`" => $this->special_condition ? "true" : "false",
            "`days_enabled`" => $this->days_enabled ? "true" : "false",

            "`site`" => "'$this->site'",
            "`address`" => "'$this->address'",
            "`daddress`" => "'" . BaseSerialized::serializeArray($this->daddress). "'",
            "`cost`" => "'$this->cost'",
            "`time`" => "'$this->time'",

            "`images`" => "'" . query_escape(serialize($this->images)) . "'",
            "`cat`" => "'" . query_escape(serialize($this->cat)) . "'",
            "`city`" => "'" . query_escape(serialize($this->city)) . "'",
            "`trc`" => "'" . query_escape(serialize($this->trc)) . "'",
            "`days`" => "'" . query_escape(serialize($this->days)) . "'",

            "`upping`" => $time,
            "`created`" => $time,
            "`end`" => $time + $this->day * 24 * 60 * 60,
            # "`like`" => $this->like,
            # "`user_id`" => $this->user_id,
        ];
        if ($this->action_id) {
            $query = implode(",", array_map(function($k,$v){return "$k = $v";}, array_keys($data), array_values($data)));
            query("UPDATE `nx_actions` SET $query WHERE `action_id`=$this->action_id;");
        } else {
            $data["`like`"] = "$this->like";
            $data["`user_id`"] = "$user_id";

            $keys = implode(",", array_keys($data));
            $values = implode(",", array_values($data));
            query("INSERT INTO `nx_actions`($keys) VALUES($values)");
            $this->action_id = query_lastInsertId();
        }
        return (int)$this->action_id;
    }

    /**
     * @param bool $real
     * @return bool
     */
    private function chargeUser($real = false){
        $q = query("SELECT * FROM `tariff`;");
        $tariffs = $q['rows'];
        $prices = array();
        $__USER = getUserById(Session::get_instance()->get_uid());

        foreach($tariffs as $tariff){
            $prices[$tariff['level']] = floatval($tariff['competition_price']);
        }
        $price = $prices[$__USER['level']];

        $currentPrice = 0;
        if(!empty($this->action) && $this->action->action_id > 0){
            $currentPrice = ceil(($this->action->end - time()) / (60 * 60 * 24)) * $price;
            if($currentPrice < 0){
                $currentPrice = 0;
            }
            if($this->action->desire){
                $currentPrice *= Action::BANNER_MULTIPLIER;
            }
        }

        $newPrice = $this->day * $price;
        if($this->desire){
            $newPrice *= Action::BANNER_MULTIPLIER;
        }
        $deltaPrice = $newPrice - $currentPrice;

        if($deltaPrice > $__USER['money']){
            return false;
        }
        if($real) {
            updateUserById($__USER['id'], array('money'), array($__USER['money'] - $deltaPrice));
            if ($deltaPrice > 0) {
                forOwnerReferal($__USER['referal'], $deltaPrice);
            }
        }
        return true;
    }

    /**
     * @param $errorsArray
     * @param $errorsString
     * @return bool
     */
    private function validate(&$errorsArray, &$errorsString){
        if (strlen($this->title) < 3) {
            $errorsArray['input[name="title"]'] = array('Короткое название', 'bottom');
        }
        if (strlen($this->condition) < 3) {
            $errorsArray['textarea[name="condition"]'] = array('Опишите более подробно', 'bottom');
        }
        if (count($this->cat) < 2) {
            $errorsString .= 'Необходимо выбрать больше категорий<br/>';
        }
        if (count($this->city) < 1) {
            $errorsString .= 'Город выбран некорректно<br/>';
        } elseif (count($this->city) > 20) {
            $errorsString .= 'Запрещено выбирать больше 20 городов<br/>';
        }
        if(!$this->time->validate($errorsArray, $errorsString)){
            return false;
        }
        if(!$this->site->validate($errorsArray, $errorsString)){
            return false;
        }
        if(!$this->address->validate($errorsArray, $errorsString)){
            return false;
        }
        if(!$this->cost->validate($errorsArray, $errorsString)){
            return false;
        }
        if(!$this->validateDaddress($errorsArray, $errorsString)){
            return false;
        }
        if(!$this->validateImages($errorsArray, $errorsString)){
            return false;
        }

        return empty($errorsArray) && empty($errorsString);
    }

    /**
     * @param $errorsArray
     * @param $errorsString
     * @return bool
     */
    private function validateImages(&$errorsArray, &$errorsString)
    {
        global $SYSTEM;
        $dopImages = $SYSTEM['post']['images'];
        $images = array();
        if ($dopImages) {
            $i = 4;
            foreach ($dopImages as $img) {
                $img = trim($img, '/');

                if (empty($img) || $img == '/') {
                    continue;
                }
                $t = getExtension($img);
                $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                copy($img, $name);
                $images[$i++] = $name;
            }
        }

        if (!empty($SYSTEM['post']['chooseImage1__'])) {
            $info = getimagesize($_SERVER['DOCUMENT_ROOT'] . "/" . $SYSTEM['post']['chooseImage1__']);
            if (preg_match("/^image/", $info['mime'])) {
                $t = getExtension($SYSTEM['post']['chooseImage1__']);
                $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                copy($SYSTEM['post']['chooseImage1__'], $name);
                $images[1] = $name;
            }
            if (preg_match("/^image/", $info['mime'])) {
                $t = getExtension($SYSTEM['post']['chooseImage1___']);
                $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                copy($SYSTEM['post']['chooseImage1___'], $name);
                $images[11] = $name;
            }
        }
        if (!empty($SYSTEM['post']['chooseImage2__'])) {
            $info = getimagesize($_SERVER['DOCUMENT_ROOT'] . "/" . $SYSTEM['post']['chooseImage2__']);
            if (preg_match("/^image/", $info['mime'])) {
                $t = getExtension($SYSTEM['post']['chooseImage2__']);
                $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                copy($SYSTEM['post']['chooseImage2__'], $name);
                $images[2] = $name;
            }
        }
        if (!empty($SYSTEM['post']['chooseImage3__'])) {
            $info = getimagesize($_SERVER['DOCUMENT_ROOT'] . "/" . $SYSTEM['post']['chooseImage3__']);
            if (preg_match("/^image/", $info['mime'])) {
                $t = getExtension($SYSTEM['post']['chooseImage3__']);
                $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                copy($SYSTEM['post']['chooseImage3__'], $name);
                $images[3] = $name;
            }
        }
        $yt = $SYSTEM['post']['yt'];
        $video_id = '';
        if ($yt && (!is_url($yt) || strpos('you', $yt) === false)) {
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $yt, $match)) {
                $video_id = $match[1];
            } else {
                $errorsString .= 'Была указана ссылка не на видео с yotube<br/>';
            }
        } else {
            $yt = '';
        }
        $images[12] = $yt;

        $this->images = $images;

        return empty($errorsArray) && empty($errorsString);
    }

    /**
     * @param $errorsArray
     * @param $errorsString
     * @return bool
     */
    private function validateDaddress(&$errorsArray, &$errorsString){
        foreach ($this->daddress as $i => &$address){
            if(!$address->getAddress()){
                unset($address);
            } else {
                $address->validate($errorsArray, $errorsString, "daddress[$i][2]");
            }
        }
        return empty($errorsArray) && empty($errorsString);
    }

    /**
     * @return int
     */
    public function getActionId()
    {
        return $this->action_id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @return Site
     */
    public function &getSite()
    {
        return $this->site;
    }

    /**
     * @return int
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @return bool
     */
    public function isDesire()
    {
        return $this->desire;
    }

    /**
     * @return Address
     */
    public function &getAddress()
    {
        return $this->address;
    }

    /**
     * @return Address[]
     */
    public function &getDaddress()
    {
        return $this->daddress;
    }

    /**
     * @return Cost
     */
    public function &getCost()
    {
        return $this->cost;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return int
     */
    public function getLike()
    {
        return $this->like;
    }

    /**
     * @return \string[]
     */
    public function &getImages()
    {
        return $this->images;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return \int[]
     */
    public function &getCat()
    {
        return $this->cat;
    }

    /**
     * @return \int[]
     */
    public function &getCity()
    {
        return $this->city;
    }

    /**
     * @return \int[]
     */
    public function &getTrc()
    {
        return $this->trc;
    }

    /**
     * @return int
     */
    public function getUpping()
    {
        return $this->upping;
    }

    /**
     * @return bool
     */
    public function isSpecialCondition()
    {
        return $this->special_condition;
    }

    /**
     * @return bool
     */
    public function isDaysEnabled()
    {
        return $this->days_enabled;
    }

    /**
     * @return \int[]
     */
    public function &getDays()
    {
        return $this->days;
    }

    /**
     * @return Time
     */
    public function &getTime()
    {
        return $this->time;
    }
}