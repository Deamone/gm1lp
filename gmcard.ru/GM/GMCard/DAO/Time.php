<?php

namespace GM\GMCard\DAO;


class Time extends BaseSerialized
{
    /**
     * @var bool $enabled
     */
    protected $enabled = false;
    /**
     * @var int $from_hour
     */
    protected $fromHour = 0;
    /**
     * @var int $from_minute
     */
    protected $fromMinute = 0;
    /**
     * @var int $to_hour
     */
    protected $toHour = 23;
    /**
     * @var int $to_minute
     */
    protected $toMinute = 59;

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return int
     */
    public function getFromHour()
    {
        return $this->fromHour;
    }

    /**
     * @return int
     */
    public function getFromMinute()
    {
        return $this->fromMinute;
    }

    /**
     * @return int
     */
    public function getToHour()
    {
        return $this->toHour;
    }

    /**
     * @return int
     */
    public function getToMinute()
    {
        return $this->toMinute;
    }

    public function validate(&$errorsArray, &$errorsString)
    {
        if($this->enabled) {
            if ($this->fromHour < 0 || $this->fromHour > 23) {
                $errorsArray['input[name="time[fromHour]"]'] = array('Поле может принимать значение от 0 до 23', 'bottom');
            }
            if ($this->fromMinute < 0 || $this->fromMinute > 59) {
                $errorsArray['input[name="time[fromMinute]"]'] = array('Поле может принимать значение от 0 до 59', 'bottom');
            }
            if ($this->toHour < 0 || $this->toHour > 23) {
                $errorsArray['input[name="time[toHour]"]'] = array('Поле может принимать значение от 0 до 23', 'bottom');
            }
            if ($this->toMinute < 0 || $this->toMinute > 59) {
                $errorsArray['input[name="time[toMinute]"]'] = array('Поле может принимать значение от 0 до 59', 'bottom');
            }
            if ($this->toHour < $this->fromHour || $this->toHour == $this->fromHour && $this->toMinute <= $this->fromMinute) {
                $errorsArray['input[name="time[toHour]"]'] = array('Время конца должно быть больше времени начала', 'bottom');
            }
        }
        return empty($errorsArray) && empty($errorsString);
    }
}