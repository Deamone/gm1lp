<?php

namespace GM\GMCard\DAO;


class Site extends BaseSerialized
{
    /**
     * @var string
     */
    protected $main = '';
    /**
     * @var string
     */
    protected $gm = '';
    /**
     * @var string
     */
    protected $vk = '';
    /**
     * @var string
     */
    protected $fb = '';
    /**
     * @var string
     */
    protected $ok = '';
    /**
     * @var string
     */
    protected $tw = '';
    /**
     * @var string
     */
    protected $yt = '';
    /**
     * @var string
     */
    protected $ig = '';
    /**
     * @var string
     */
    protected $pr = '';
    /**
     * @var string
     */
    protected $linkedin = '';
    /**
     * @var string
     */
    protected $foursquare = '';
    /**
     * @var string
     */
    protected $tumblr = '';
    /**
     * @var string
     */
    protected $googleplus = '';
    /**
     * @var string
     */
    protected $email = '';

    /**
     * @return string
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * @return string
     */
    public function getGm()
    {
        return $this->gm;
    }

    /**
     * @return string
     */
    public function getVk()
    {
        return $this->vk;
    }

    /**
     * @return string
     */
    public function getFb()
    {
        return $this->fb;
    }

    /**
     * @return string
     */
    public function getOk()
    {
        return $this->ok;
    }

    /**
     * @return string
     */
    public function getTw()
    {
        return $this->tw;
    }

    /**
     * @return string
     */
    public function getYt()
    {
        return $this->yt;
    }

    /**
     * @return string
     */
    public function getIg()
    {
        return $this->ig;
    }

    /**
     * @return string
     */
    public function getPr()
    {
        return $this->pr;
    }

    /**
     * @return string
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * @return string
     */
    public function getFoursquare()
    {
        return $this->foursquare;
    }

    /**
     * @return string
     */
    public function getTumblr()
    {
        return $this->tumblr;
    }

    /**
     * @return string
     */
    public function getGoogleplus()
    {
        return $this->googleplus;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $errorsArray
     * @param $errorsString
     * @return bool
     */
    public function validate(&$errorsArray, &$errorsString)
    {
        foreach(["main","gm","vk","fb","ok","tw","yt","ig","pr","linkedin","foursquare","tumblr","googleplus","email"] as $site) {
            //if ($this->{$site} && !filter_var('http://' . $this->{$site}, FILTER_VALIDATE_URL)) {
            if ($this->{$site} && !is_url('http://' . $this->{$site})) {
                $errorsArray["input[name=\"site[$site]\"]"] = array('Имеет некорректный формат', 'bottom');
            }
        }
        return empty($errorsArray) && empty($errorsString);
    }

    public function stripSchema(){
        foreach(["main","gm","vk","fb","ok","tw","yt","ig","pr","linkedin","foursquare","tumblr","googleplus","email"] as $site) {
            $this->{$site} = preg_replace('/http(.?)(\/{1,})/i', '', $this->{$site});
        }
    }
}