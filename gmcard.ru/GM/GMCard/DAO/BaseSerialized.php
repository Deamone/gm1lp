<?php

namespace GM\GMCard\DAO;


class BaseSerialized
{
    /**
     * BaseSerialized constructor.
     * @param string|array|null $raw
     * @param bool $serialized
     */
    public function __construct($raw = null, $serialized = true){
        if($serialized && !empty($raw)){
            $raw = unserialize($raw);
        }
        if(!empty($raw)) {
            foreach (get_class_vars(get_class($this)) as $key => $default) {
                if (array_key_exists($key, $raw)) {
                    $this->{$key} = $raw[$key];
                }
            }
        }
    }

    /**
     * @param string|array|null $raw
     * @param bool $serialized
     * @return array
     */
    public static function newArray($raw = null, $serialized = true){
        if($serialized && !empty($raw)){
            $raw = unserialize($raw);
        }
        $array = [];
        if(!empty($raw)) {
            foreach ($raw as $item) {
                $class = get_called_class();
                $array[] = new $class($item, false);
            }
        }
        return $array;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $raw = [];
        foreach(get_class_vars(get_class($this)) as $key => $default){
            $raw[$key] = $this->{$key};
        }
        return query_escape(serialize($raw));
    }

    /**
     * @return string
     */
    public function __toStringUnescaped()
    {
        $raw = [];
        foreach(get_class_vars(get_class($this)) as $key => $default){
            $raw[$key] = $this->{$key};
        }
        return serialize($raw);
    }

    /**
     * @param BaseSerialized[] $array
     * @return string
     */
    public static function serializeArray($array){
        $raw = [];
        foreach($array as $item){
            $raw[] = unserialize($item->__toStringUnescaped());
        }
        return query_escape(serialize($raw));
    }
}