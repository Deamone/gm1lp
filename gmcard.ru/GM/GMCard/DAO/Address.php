<?php

namespace GM\GMCard\DAO;


use GM\Modules\YandexQuerier\YandexQuerier;


class Address extends BaseSerialized
{
    /**
     * @var string
     */
    protected $graph = '';
    /**
     * @var string
     */
    protected $phone = '';
    /**
     * @var string
     */
    protected $address = '';
    /**
     * @var string
     */
    protected $site = '';
    /**
     * @var float
     */
    protected $longitude = 0;
    /**
     * @var float
     */
    protected $latitude = 0;

    /**
     * @return string
     */
    public function getGraph()
    {
        return $this->graph;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    public function validate(&$errorsArray, &$errorsString, $index = "address[address]"){
        $addressValidation = (new YandexQuerier())->query($this->address);
        if ($addressValidation === null) {
            $errorsArray[$index] = array('Яндекс не знает такого адреса', 'bottom');
        } else {
            $this->address = $addressValidation['address'];
            $this->longitude = $addressValidation['longitude'];
            $this->latitude = $addressValidation['latitude'];
        }
        return empty($errorsArray) && empty($errorsString);
    }
}