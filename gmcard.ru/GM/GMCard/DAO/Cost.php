<?php

namespace GM\GMCard\DAO;


class Cost extends BaseSerialized
{
    /**
     * @var string
     */
    protected $type = 'discount';
    /**
     * @var string
     */
    protected $discount_type = 'percent';
    /**
     * @var int
     */
    protected $cost = 1000;
    /**
     * @var int
     */
    protected $discount = 50;
    /**
     * @var int
     */
    protected $fee = 10;
    /**
     * @var int
     */
    protected $min_order_sum = 0;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getDiscountType()
    {
        return $this->discount_type;
    }

    /**
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @return int
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @return int
     */
    public function getMinOrderSum()
    {
        return $this->min_order_sum;
    }

    /**
     * @return int
     */
    public function getFinalCost(){
        if($this->isPercent()){
            return (int)($this->cost * (100. - $this->discount) / 100.);
        }
        if($this->isRaw()){
            return $this->cost - $this->discount;
        }
        return 0;
    }

    /**
     * @return int
     */
    public function getPercentDiscount(){
        if($this->isPercent()){
            return $this->discount;
        }
        if($this->isRaw()){
            return (int)($this->discount * 100. / $this->cost);
        }
        return 0;
    }

    /**
     * @return int
     */
    public function getRawDiscount(){
        if($this->isPercent()){
            return (int)($this->cost * $this->discount / 100.);
        }
        if($this->isRaw()){
            return $this->discount;
        }
        return 0;
    }

    /**
     * @return bool
     */
    public function isDiscount(){
        return $this->type == 'discount';
    }

    /**
     * @return bool
     */
    public function isCashback(){
        return $this->type == 'cashback';
    }

    /**
     * @return bool
     */
    public function isPercent(){
        return $this->isDiscount() && $this->discount_type == 'percent';
    }

    /**
     * @return bool
     */
    public function isRaw(){
        return $this->isDiscount() && $this->discount_type == 'raw';
    }

    /**
     * @return string
     */
    public function getCssClass(){
        if(!$this->isDiscount()) {
            return '';
        }
        $disc = $this->getPercentDiscount();
        if ($disc >= 84) {
            return 'a_discount_ping';
        }
        if ($disc >= 72) {
            return 'a_discount_blue';
        }
        if ($disc >= 60) {
            return 'a_discount_yellow';
        }
        if ($disc >= 48) {
            return 'a_discount_dgreen';
        }
        if ($disc >= 36) {
            return 'a_discount_lgreen';
        }
        if ($disc >= 24) {
            return 'a_discount_borange';
        }
        if ($disc >= 12) {
            return 'a_discount_dorange';
        }
        if ($disc >= 0) {
            return 'a_discount_orange';
        }
        return 'a_discount_ping';
    }

    /**
     * @return bool
     */
    public function validate(&$errorsArray, &$errorsString){
        if($this->isDiscount()){
            if(!$this->isPercent() && !$this->isRaw()){
                $errorsArray['input[name="cost[discount_type]"]'] = array('Некорректный тип скидки', 'bottom');
            }
            if(empty($this->cost)){
                $errorsArray['input[name="cost[cost]"]'] = array('Не указана стоимость услуги/товара', 'bottom');
            }
            if($this->cost < 0){
                $errorsArray['input[name="cost[cost]"]'] = array('Стоимость услуги/товара не может быть отрицательной', 'bottom');
            }
            if(empty($this->discount)){
                $errorsArray['input[name="cost[discount]"]'] = array('Не указана величина скидки', 'bottom');
            }
            if($this->discount < 0){
                $errorsArray['input[name="cost[discount]"]'] = array('Скидка не может быть отрицательной', 'bottom');
            }
            if($this->isPercent() && $this->discount > 100){
                $errorsArray['input[name="cost[discount]"]'] = array('Скидка не может быть больше 100%', 'bottom');
            }
            if($this->isRaw() && $this->discount > $this->cost){
                $errorsArray['input[name="cost[discount]"]'] = array('Скидка не может быть больше стоимости услуги/товара', 'bottom');
            }
        }
        if($this->isCashback()){
            if(empty($this->fee)){
                $errorsArray['input[name="cost[fee]"]'] = array('Не указана ставка кэшбэка', 'bottom');
            }
            if($this->fee < 0){
                $errorsArray['input[name="cost[fee]"]'] = array('Ставка кэшбэка не может быть отрицательной', 'bottom');
            }
            if($this->fee > 100){
                $errorsArray['input[name="cost[fee]"]'] = array('Ставка кэшбэка не может быть больше 100%', 'bottom');
            }
            if($this->min_order_sum < 0){
                $errorsArray['input[name="cost[min_order_sum]"]'] = array('Минимальная сумма заказа не может быть отрицательной', 'bottom');
            }
        }
        return empty($errorsArray) && empty($errorsString);
    }
}