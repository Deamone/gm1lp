<?php

namespace GM\Modules\YandexQuerier;


use GM\Modules\BaseModule;

class YandexQuerier extends BaseModule
{
    public function query($query){
        $curl = curl_init("https://geocode-maps.yandex.ru/1.x/?" . http_build_query(['format' => 'json', 'geocode' => $query, 'results' => 1, 'key' => 'AIw3f1gBAAAAJamHMwIAadqBk4opQLKgNKGFjs_6zPF7b5MAAAAAAAAAAACQYxras0NQBkZcndcJs-qTPFnOGg==']));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($curl), true)['response']['GeoObjectCollection'];
        $meta = $result['metaDataProperty']['GeocoderResponseMetaData'];
        if($meta['found'] > 0) {
            $data = $result['featureMember'][0]['GeoObject'];
            list($longitude, $latitude) = explode(" ", $data['Point']['pos']);
            $address = $data['metaDataProperty']['GeocoderMetaData']['text'];
            return ['longitude' => $longitude, 'latitude' => $latitude, 'address' => $address];
        } else {
            return null;
        }
    }
}