<?php

namespace GM\Modules\CouponPrinter;

use GM\Modules\BaseModule;
use GM\Modules\PhpQrCode\QRCode;


class CouponPrinter extends BaseModule
{
    private $pngPath;
    private $resource;
    private $fontFile;
    private $angle;

    function __construct($pngPath, $fontFile, $angle = 0.)
    {
        parent::__construct();
        $this->pngPath = $pngPath;
        $this->resource = imagecreatefrompng("$this->resourcesPath/$this->pngPath");
        $this->fontFile = "$this->resourcesPath/$fontFile";
        $this->angle = $angle;
    }

    function printLine($size, $x, $y, $text, $align = "top_left", $bold = false, $red = 0, $green = 0, $blue = 0){
        list($w, $h) = $this->computeDimensions($size, $text);
        list($x, $y) = $this->computeCoordinates($w, $h, $x, $y, $align);
        $color = imagecolorexact($this->resource, $red, $green, $blue);
        imagefttext($this->resource, $size, $this->angle, $x, $y, $color, $this->fontFile, $text);
        if($bold){
            imagefttext($this->resource, $size, $this->angle, $x + 1, $y, $color, $this->fontFile, $text);
            imagefttext($this->resource, $size, $this->angle, $x, $y + 1, $color, $this->fontFile, $text);
        }
    }

    function printLines($size, $x, $y, $text, $align = "top_left", $bold = false, $red = 0, $green = 0, $blue = 0, $h_padding = 0, $line_height = 1.2){
        $string = "";
        $v_margin = 0;
        foreach (preg_split('/\s+/', $text) as $word) {
            list($w, $h) = $this->computeDimensions($size, "$string $word");
            if($w > imagesx($this->resource) - 2 * $h_padding){
                $this->printLine($size, $x, $y + $v_margin, $string, $align, $bold, $red, $green, $blue);
                $string = $word;
                $v_margin += $line_height * $h;
            } else {
                $string .= " $word";
            }
        }
        $this->printLine($size, $x, $y + $v_margin, $string, $align, $bold, $red, $green, $blue);
    }

    function computeDimensions($size, $text){
        list( , $left_bottom_y, , , , , , $left_top_y) = imageftbbox($size, $this->angle, $this->fontFile, "Рр");
        list( , , , , $right_top_x, , $left_top_x, ) = imageftbbox($size, $this->angle, $this->fontFile, $text);
        return [$right_top_x - $left_top_x, $left_bottom_y - $left_top_y];
    }

    function computeCoordinates($w, $h, $x, $y, $align){
        list($v_align, $h_align) = explode("_", $align);
        switch($v_align){
            case "top":
                $y += $h;
                break;
            case "middle":
                $y += (imagesy($this->resource) + $h) / 2;
                break;
            case "bottom":
                $y = imagesy($this->resource) - $y;
                break;
            default:
        }
        switch($h_align){
            case "right":
                $x = imagesx($this->resource) - $x - $w;
                break;
            case "center":
                $x += (imagesx($this->resource) - $w) / 2;
                break;
            case "left":
            default:
                break;
        }
        return [$x, $y];
    }

    function printQr($text, $x = 0, $y = 0, $align = "top_left"){
        $image = QRCode::png($text, true);
        $w = imagesx($image);
        $h = imagesy($image);
        list($x, $y) = $this->computeCoordinates($w, $h, $x, $y, $align);
        imagecopy($this->resource, $image, $x, $y, 0, 0, $w, $h);
    }

    function output(){
        header("Content-Transfer-Encoding: binary");
        header("Content-Description: File Transfer");
        header("Content-Type: image/png");
        header("Content-Disposition: attachment; filename={$this->pngPath}");
        imagepng($this->resource);
        imagedestroy($this->resource);
    }

    static function printClientCoupon($fee, $action_id, $expire_date, $title, $name, $phone){
        $printer = new CouponPrinter("client_coupon.png", "DejaVuSans.ttf");
        $printer->printLine(28, 300, 156, "—" . number_format($fee, 0, ".", " "), "top_right", true);
        $printer->printLine(11.5, -5, 228, "gmcard.ru/action/action?id=$action_id", "top_center", false, 64, 64, 255);
        $printer->printLine(11.5, -5, 253, "Действителен до $expire_date", "top_center", false, 128, 128, 128);
        $printer->printLines(22, -5, 293, $title, "top_center", true, 0, 0, 0, 62);
        $printer->printLine(11.5, 62, 433, "Купон выдан:", "top_left", false, 128, 128, 128);
        $printer->printLine(11.5, 62, 433, "Телефон для связи:", "top_right", false, 128, 128, 128);
        $printer->printLine(18, 60, 458, $name, "top_left", true);
        $printer->printLine(18, 60, 458, $phone, "top_right", true);
        $printer->printLine(11.5, -5, 493, "Условия действия скидки смотрите на сайте акции", "top_center");
        $printer->printLines(12, -5, 533, "При покупке/оказании услуги обменяйте этот купон на купон с уникальным
        промо-кодом, который необходимо ввести для получения кэшбэка", "top_center", true, 0, 0, 0, 62);
        $printer->printQr("http://gmcard.ru/action/action?id=$action_id", 50, 160, "bottom_right");
        $printer->output();
    }

    static function printPromoCodeCoupon($promo_code, $name, $phone, $phone_amount){
        $printer = new CouponPrinter("promo_code_coupon.png", "DejaVuSans.ttf");
        $printer->printLine(28, 380, 157, number_format($phone_amount, 0, ".", " "), "top_right", true);
        $printer->printLine(28, -5, 268, "Промо-код:", "top_center", true);
        $printer->printLine(36, -5, 328, $promo_code, "top_center", true, 64, 64, 255);
        $printer->printLine(11.5, 62, 433, "Купон выдан:", "top_left", false, 128, 128, 128);
        $printer->printLine(11.5, 62, 433, "Телефон для связи:", "top_right", false, 128, 128, 128);
        $printer->printLine(18, 60, 458, $name, "top_left", true);
        $printer->printLine(18, 60, 458, $phone, "top_right", true);
        $printer->printLines(11.5, -5, 533, "Для активации промо-кода и получения кэшбэка, перейдите по ссылке
        из QR-кода либо введите промо-код на сайте:", "top_center", true, 0, 0, 0, 62);
        $printer->printLine(22, -5, 583, "gmcard.ru/promo_code/input", "top_center", true, 64, 64, 255);
        $printer->printQr("http://gmcard.ru/promo_code/input?code=$promo_code", 50, 160, "bottom_right");
        $printer->output();
    }
}