<?php

namespace GM\Modules;


class BaseModule
{
    protected $resourcesPath;

    public function __construct()
    {
        $this->resourcesPath = $_SERVER['DOCUMENT_ROOT'] . "/GM/Resources";
    }
}