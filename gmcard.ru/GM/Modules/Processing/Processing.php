<?php

namespace GM\Modules\Processing;


class Processing
{
    /**
     * @param string $event
     * @param array $params
     * @return bool
     * @throws ProcessingError
     */
    public function request($event, $params){
        $params['event'] = $event;
        $curl = curl_init("http://promocode_state_machine.local:9080/?" . http_build_query($params));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        if($result !== '0')
            throw new ProcessingError((int)$result);
        return true;
    }
}