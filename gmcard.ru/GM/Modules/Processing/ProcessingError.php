<?php

namespace GM\Modules\Processing;

class ProcessingError extends \Exception
{
    public function __construct($code = 0)
    {
        $this->code = $code;
    }

    public function getDescription(){
        switch ($this->code){
            case ErrorCodes::DATABASE_CONNECT_ERROR:
                return "Ошибка подключения к базе данных";
            case ErrorCodes::DATABASE_QUERY_ERROR:
                return "Ошибка запроса к базе данных";
            case ErrorCodes::CODE_DOES_NOT_EXIST:
                return "Промо-код не существует";
            case ErrorCodes::ACTION_DOES_NOT_EXIST:
                return "Акция не существует";
            case ErrorCodes::CASHBACK_NOT_PROVIDED:
                return "Кэшбэк по акции недоступен";
            case ErrorCodes::ACTION_EXPIRED:
                return "Срок действия акции истек";
            case ErrorCodes::INVALID_FEE:
                return "Неверная ставка кэшбэка";
            case ErrorCodes::INVALID_NAME:
                return "Неверное имя";
            case ErrorCodes::INVALID_PHONE:
                return "Неверный номер телефона";
            case ErrorCodes::USER_DOES_NOT_EXIST:
                return "Пользователь не существует";
            case ErrorCodes::INVALID_AMOUNT:
                return "Неверная сумма";
            case ErrorCodes::FORBIDDEN_TRANSITION:
                return "Попытка осуществить запрещенный переход";
            case ErrorCodes::WRONG_EVENT:
                return "Попытка выполнить несуществующее действие";
            case ErrorCodes::INSUFFICIENT_FUNDS:
                return "Недостаточно средств";
            default:
                return "Неизвестная ошибка";
        }
    }
}