<?php

if (isset($_GET['resizeAlbums'])) {
    updateSizeAlbums();
    exit('12');
}
$itsI = false;
$_USER = array();
if (isset($_GET['id'])) {
    $album = getUserAlbum($_GET['id']);

    if ($album['user_id'] < 0) {
        $itsI = false;
        $iUser = true;
        $iMember = false;

        $_USER = $USER;
        $group = getGroup($album['user_id']);
        #$_USER = getUserById($group['owner_id']);
        if (!$group) {
            redirect('/groups');
        }
        $members = getMembersInGroup($group['group_id']);
        if (isset($members['group'][10][$USER['id']]) || isset($members['group'][11][$USER['id']])) {
            $itsI = true;
            $owner = 1;
        }
        if (isset($members[$USER['id']])) {
            $iMember = true;
        }
    } else {
        $itsI = $album['user_id'] == $USER['id'] ? 1 : 0;
    }


    if (!$album || !$itsI) {
        redirect('/albums');
    }
}

$owner = 1;
if (!$_USER) {
    $_USER = $USER;
}
$album_id = (int)$_GET['id'];
$mask = $album['mask'];
if ($mask & ALBUM_ALLOW_SEE) {
    $see = 1;
} elseif ($mask & ALBUM_ALLOW_FRIENDS_SEE) {
    $see = 2;
} else {
    $see = 0;
}

/**************************/
if (isset($_FILES['files'])) {
    $id = addPhotoInAlbum($album_id, 1, $album['user_id']);
    if (!file_exists('uploads/albums/' . $album_id)) {
        mkdir('uploads/albums/' . $album_id);
    }
    if ((create_thumbnail($_FILES['files']['tmp_name'], 'uploads/albums/' . $album_id . '/thumb' . $id . '.jpg', 110, 110)) < 1) {
        removePhoto($album_id, $id);
        exit('0' . $_FILES['files']['name']);
    } else {
        create_thumbnail($_FILES['files']['tmp_name'], 'uploads/albums/' . $album_id . '/' . $id . '.jpg', 1440, 900, 'height');
        create_thumbnail($_FILES['files']['tmp_name'], 'uploads/albums/' . $album_id . '/preview' . $id . '.jpg', 360, 190, 'height');
    }

    exit($id . '');
}

/************************/
if (isset($_POST['name']) && isset($_POST['see']) && isset($_POST['desc']) && $owner) {
    $error = '';
    if (empty($_POST['name'])) {
        $error .= $_LANG['ALBUMS_ERROR_EMPTY_NAME'];
    }

    $mask = 0;

    if ($_POST['see'] == 1) {
        $mask |= ALBUM_ALLOW_SEE;
    } elseif ($_POST['see'] == 2) {
        $mask |= ALBUM_ALLOW_FRIENDS_SEE;
    }
    /*
        if ($_POST['comment'] == 1) {
            $mask |= ALBUM_ALLOW_COMMENT;
        } elseif ($_POST['comment'] == 2) {
            $mask |= ALBUM_ALLOW_FRIENDS_COMMENT;
        }*/
    if (!$error) {
        echo 'Сохранено!';
        updateAlbum($_GET['id'], $_POST['name'], $_POST['desc'], $mask);
    } else {
        echo $error;
    }
    exit;
}

if (isset($_POST['desc'])) {
    foreach ($_POST['desc'] as $photo_id => $comment) {
        if (!$comment) {
            continue;
        }
        updatePhoto($photo_id, $comment, $album_id);
    }
    exit('Сохранено');
}
header_('Альбом ' . $album['title'], '', '', '<script type="text/javascript" src="/js/vendor/jquery.ui.widget.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.fileupload.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>', ' body-albums');

$myVideos = getVideos($USER['id']);
$count_videos = count($myVideos);

$myContacts = getContacts($_USER['id']);
$cMyContacts = getCountContacts($_USER['id']);

# Right
$priv_photo = isPrivate($USER['id'], $USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($USER['id'], $USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($USER['id'], $USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($USER['id'], $USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right

$albums = getUserAlbums($_USER['id']);
$count_albums = count($albums);
$last_album_photo = '/images/no_scrn_mov.jpg';
if ($albums && isset($albums[0]['last_photo'])) {
    $last_album_photo = '/uploads/albums/' . $albums[0]['album_id'] . '/preview' . $albums[0]['last_photo']['photo_id'] . '.jpg';
}

$last_photo = '/images/no_scrn_mov.jpg';
foreach ($album['photos'] as $photo) {
    if ($photo['file'] == 1) {
        $last_photo = '/uploads/albums/' . $album_id . '/preview' . $photo['photo_id'] . '.jpg';
        break;
    }
}

?>
    <!--main-->
    <form id="fileupload" action="?" method="POST" enctype="multipart/form-data" style="display: none">
        <input type="file" id="userfile" name="files" multiple="multiple"/>
    </form>
    <form id="fileupload2" action="?" method="POST" enctype="multipart/form-data" style="display: none">
        <input type="file" id="userfile2" name="files"/>
        <input type="hidden" name="home" value="1"/>
    </form>
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <img src="/images/arrow.png"> <a href="/albums?id=<?php echo $album['user_id']; ?>"> другие альбомы </a>
            </div>
            <div class="block_header">
                <h2>Редактировать альбом</h2>
                <?php echo $itsI ? '<a href="/album?id=' . $album_id . '&remove" class="actRight">Удалить альбом</a>' : ''; ?>
                <div class="clear"></div>
            </div>
            <div class="block_content" style="background-color: transparent;padding-right: 20px;">
                <div class="left_form">
                    <form method="post" id="formAlbum">
                        <span>Название альбома</span>
                        <input type="text" name="name" value="<?php echo $album['title']; ?>"/>
                        <span>Комментарий к альбому</span>
                        <textarea name="desc"><?php echo $album['desc']; ?></textarea>

                        <span class="w" <?php echo $album['user_id'] < 0 ? 'style="display:none;"' : ''; ?>>Кому виден этот альбом </span>

                        <div class="styled-select" <?php echo $album['user_id'] < 0 ? 'style="display:none;"' : ''; ?>>
                            <select name="see">
                                <option value="1"<?php echo $see == 1 ? ' selected' : ''; ?>>Всем пользователям</option>
                                <option value="2"<?php echo $see == 2 ? ' selected' : ''; ?>>Только друзьям</option>
                                <option value="0"<?php echo $see == 0 ? ' selected' : ''; ?>>Только мне</option>
                            </select>
                        </div>
                        <br/>
                        <button class="btn rounded_3">Сохранить</button>
                    </form>
                    <br/>
                    <br/>
                </div>
                <div class="right_form">
                    <div class="overflow"><img src="<?php echo $last_photo; ?>" class="rounded_3"/></div>
                    <a href="#" onclick="$('#userfile2').click(); return false;">Загрузить обложку</a>
                </div>

                <div class="button_add_photo rounded_5">Добавить фотографии</div>
                <form method="post" id="formPhotos">
                    <div class="photos set">
                        <?php foreach ($album['photos'] as $photo) {
                            echo '<div class="photo" id="i' . $photo['photo_id'] . '">
                        <img src="/uploads/albums/' . $album_id . '/thumb' . $photo['photo_id'] . '.jpg" class="rounded_3"/>
                        <div class="comm">
                            <a href="#" class="delete">Удалить</a>
                            <span>Комментарий к фотографии</span>
                            <textarea name="desc[' . $photo['photo_id'] . ']">' . $photo['desc'] . '</textarea>
                        </div>
                        </div>';
                        }
                        ?>
                    </div>
                    <br/>
                    <?php if ($album['photos']) {
                        echo '<button class="btn rounded_3">Сохранить</button>';
                    }
                    ?>
                </form>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($USER['id'], $priv_companies);
            echo show_main_communities($_USER['id'], $priv_communities);
            echo show_main_album($USER['id'], $priv_photo);
            echo show_main_videos($USER['id'], $priv_video);
            echo show_main_audios($USER['id'], $priv_audios);
            ?>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('.button_add_photo').click(function () {
                $('#userfile').click();
            });
            $(document).on('click', '.delete', function () {
                $this = $(this).parent().parent();
                $.post('/album?id=<?php echo $album_id; ?>&remove=1&pid=' + $this.attr('id'), '', function (data) {
                    $this.remove();
                });
                return false;
            });
            $('#fileupload').fileupload({
                url: '/album?id=<?php echo $album_id; ?>',
                dataType: 'html',
                progressall: function (e, data) {
                    fileUpload = false;
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('.progress').html(progress + '%');
                },
                done: function (e, data) {
                    console.log(data.result);
                    fileUpload = true;
                    if (data.result == 0 || data.result.substr(0, 1) == 0) {
                        alert(data.result.substr(1) + ': Запрещенный формат/размер');
                    } else {
                        var html = '<div class="photo" id="i' + data.result + '"><img src="/uploads/albums/<?php echo $album_id; ?>/thumb' + data.result + ".jpg?" + Math.random(1000) + '" /><div class="comm"><a href="#" class="delete">Удалить</a><span>Комментарий к фотографии</span><textarea name="desc[' + data.result + ']"></textarea></div></div>';
                        if ($('.photos .photo').size()) {
                            $('.photos .photo:first').before(html);//);
                        } else {
                            $('.photos').append(html);
                        }
                    }
                }
            });
            $('#fileupload2').fileupload({
                url: '?id=<?php echo $album_id; ?>',
                dataType: 'html',
                progressall: function (e, data) {
                    fileUpload = false;
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('.progress').html(progress + '%');
                },
                done: function (e, data) {
                    console.log(data.result);
                    fileUpload = true;
                    if (data.result == 0 || data.result.substr(0, 1) == 0) {
                        alert(data.result.substr(1) + ': Запрещенный формат/размер');
                    } else {
                        $('.right_form img').attr('src', '/uploads/albums/<?php echo $album_id; ?>/preview' + data.result + ".jpg?" + Math.random(1000) + '');
                    }
                }
            });
            $('form#formAlbum').submit(function () {
                $.post('?id=<?php echo $album_id; ?>&edit=1', $(this).serialize(), function (data) {
                    alert(data);
                });
                return false;
            });
            $('form#formPhotos').submit(function () {
                $.post('?id=<?php echo $album_id; ?>&edit=1', $(this).serialize(), function (data) {
                    alert(data);
                });
                return false;
            });
        });
    </script>
<?php footer(); ?>