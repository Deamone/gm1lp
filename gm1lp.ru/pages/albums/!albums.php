<?php
if (isset($_GET['create'])) {
    createAlbum($USER['id']);
    redirect('/albums');
}

$itsI = false;
$_USER = array();
if (isset($_GET['id'])) {
    $_USER = getUserById($_GET['id']);
}
if ($_USER == $USER || !$_USER) {
    $_USER = $USER;
    $itsI = true;
}

header_('Альбомы пользователя ' . $_USER['name'], '', '', ''
        . '<script type="text/javascript" src="/application/views/site/js/jquery.fileupload.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jquery.fileupload-process.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jquery.fileupload-video.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jquery.iframe-transport.js"></script>
        <script type="text/javascript" src="/application/views/site/js/nx.js"></script>
');

$albums = getUserAlbums($_USER['id']);
$c = count($albums);
if (isset($_GET['albums']) && isAjax()) {
    $json = array();
    $json[] = $c;
    for ($i = 0; $i < $c; $i++) {
        $json[] = array(
            'title' => $albums[$i]['title'],
            'preview' => (isset($albums[$i]['last_photo']) ? $albums[$i]['last_photo']['file'] : '/uploads/albums/main_foto.png'),
            'id' => $albums[$i]['album_id']
        );
    }

    exit(json_encode($json));
}
echo '<div class="container-menu-bottom"></div>
<!-- END container-menu -->
<div class="page-fotoalbum-top-side">
<div>' . $_USER['name'] . ' фото <span>всего <span id="sizes2">' . $c . '</span> альбомов</span></div>';
if ($itsI) {
    echo '<a href="?create" class="create-album">Создать альбом</a>';
}
echo '</div><!-- page-top-side -->';
?>
<div id="content" class="albums-page">

    <div class="content">
        <ul>
            <li>	
                <?php
                $size = 0;
                for ($i = 0; $i < $c; $i++) {
                    echo '						
                    <div>
                        <div class="img-wrap">
                            <a href="/album?id=' . $albums[$i]['album_id'] . '"><img src="' . (isset($albums[$i]['last_photo']) ? $albums[$i]['last_photo']['file'] : '/uploads/albums/main_foto.png') . '" height="146" alt="' . $albums[$i]['title'] . '"></a>
                        </div>
                        <a href="/album?id=' . $albums[$i]['album_id'] . '" class="title">' . $albums[$i]['title'] . '</a>
                        <label for="a' . $albums[$i]['album_id'] . '" href="/album?id=' . $albums[$i]['album_id'] . '" class="counter"><span class="count">' . $albums[$i]['size'] . '</span> фотографий</label>
                        ' . ($itsI ? '<a href="/album?id=' . $albums[$i]['album_id'] . '&edit" class="edit">Редактировать альбом</a>' : '') . '</div>
                    ' . ($itsI ? '<form action="/album?id=' . $albums[$i]['album_id'] . '" method="post" enctype="multipart/form-data" style="display:none;" id="form7345">
                        <input type="file" name="file" class="photo-file" id="a-' . $albums[$i]['album_id'] . '" multiple onclick="">
                    </form>' : '') . '
                ';
                    $size += $albums[$i]['size'];
                }
                echo "<script>$('#sizes').html($size);</script>";
                ?>
            </li>
            <!--li>							
                <div>
                    <div class="img-wrap">
                        <a href="/page/let/foto?id=58"><img src="/uploads/albums/main_foto.png" height="146" alt="Новый альбом"></a>
                    </div>
                    <a href="/page/let/foto?id=58" class="title">Новый альбом</a>
                    <label for="upload-to-album58" href="/page/let/foto?id=58" class="counter">0 фотографий</label>
                    <a href="/page/let/foto?id=58&red_alb=58" class="edit">Редактировать альбом</a>                                </div>
                <form action="/page/let/albom?alb=58" method="post" enctype="multipart/form-data" style="display:none;">
                    <input type="hidden" name="alb" value="58" >
                    <input type="file" name="foto" class="photo-file" id="upload-to-album58">
                </form>
            </li-->	        
        </ul>
    </div><!-- .content -->





    <div class="clearfix"></div><!-- .clearfix -->

</div>
</div><!-- END CONTAINER -->
<script>
    var fileUpload = true;
    var $info;
    var id = 0;
    $(document).ready(function() {
        var $this = $('.photo-file');
        $(document).on('click', '.counter', function() {
            id = ($(this).attr('for')).replace('a', '');
            $('.photo-file#a-' + id).fileupload({
                url: '/album?upload=1&id=' + id,
                dataType: 'json',
                autoUpload: false,
                progressall: function(e, data) {
                    fileUpload = false;
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#loading').html(progress + '%');
                },
                fail: function(e, data) {
                    $('#nx-info').html('Ошибка при загрузке одного из файлов');
                    console.log(data);
                    alert('');
                    setTimeout(function() {
                        $('#nx-info').html('').hide();
                    }, 3000);
                },
                done: function(e, data) {
                    fileUpload = true;
                    var files = data.result.files;
                    var error = '';
                    var p = '';
                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        if (file.error !== undefined) {
                            $('#nx-info').append('<br/>Файл не был загружен');
                        } else {
                            $info.find('.count').html(parseInt($info.find('.count').html()) + 1);
                            $info.find('.img-wrap img').attr('src', file);
                            $('#nx-info').append('<br/>Файл загружен');
                        }
                    }/*
                     if (error.length) {
                     alert(error);
                     }*/
                    setTimeout(function() {
                        $('#nx-info').html('').hide();
                    }, 3000);
                    $('.brfiles').append(p);
                },
                add: function(e, data) {
                    $('#nx-info').html('Загрузка <span id="loading"></span>').show();
                    var uploadErrors = [];
                    var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                    if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                        $('#nx-info').html('Запрещенный тип файла');
                        uploadErrors.push('1');
                    }
                    if (data.originalFiles[0] !== undefined && data.originalFiles[0]['size'] > 3 * 1024 * 1024) {
                        $('#nx-info').html('Файл превышает лимит');
                        uploadErrors.push('2');
                        console.log(data.originalFiles[0]['size']);
                    }
                    if (uploadErrors.length > 0) {

                    } else {
                        data.submit();
                    }
                }
            });
            $('.photo-file#a-' + id).click();
        });
        $(document).on('click', '.photo-file', function() {
            $info = $(this).parent().prev();
        });
        $(document).on('submit', '#form7345', function() {
            return false;
        });
        //$this
    });
</script>
<?php footer(); ?>