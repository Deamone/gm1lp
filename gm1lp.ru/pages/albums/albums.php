<?php
$itsI = false;
$_USER = array();
if (isset($_GET['id']) && $_GET['id'] < 0) {
    $iUser = true;
    $iMember = false;

    $_USER = $USER;
    $group = getGroup($_GET['id']);
    #$_USER = getUserById($group['owner_id']);
    if (!$group) {
        redirect('/groups');
    }
    $members = getMembersInGroup($group['group_id']);
    if (isset($members['group'][10][$USER['id']]) || isset($members['group'][11][$USER['id']])) {
        $itsI = true;
    }
    if (isset($members[$USER['id']])) {
        $iMember = true;
    }
    $pid = $group['group_id'] * -1;
} else {
    if (isset($_GET['id'])) {
        $_USER = getUserById($_GET['id']);
    }
    if ($_USER['id'] == $USER['id'] || !$_USER) {
        $_USER = $USER;
        $itsI = true;
    }
    $pid = $_USER['id'];
}

if (isset($_GET['create']) && $itsI) {
    createAlbum($pid);
    if ($pid < 0) {
        redirect('/albums?id=' . $pid);
    }
    redirect('/albums');
}
$myVideos = getVideos($USER['id']);
$count_videos = count($myVideos);

$myContacts = getContacts($_USER['id']);
$cMyContacts = getCountContacts($_USER['id']);

# Right
$priv_photo = isPrivate($USER['id'], $USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($USER['id'], $USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($USER['id'], $USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($USER['id'], $USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right

$albums = getUserAlbums($pid);
$c = count($albums);
if (isset($_GET['albums']) && isAjax()) {
    $json = array();
    $json[] = $c;
    for ($i = 0; $i < $c; $i++) {
        $json[] = array(
            'title' => $albums[$i]['title'],
            'preview' => (isset($albums[$i]['last_photo']) ? $albums[$i]['last_photo']['file'] : '/uploads/albums/main_foto.png'),
            'id' => $albums[$i]['album_id']
        );
    }

    exit(json_encode($json));
}
$count_albums = count($albums);
$last_album_photo = '/images/no_scrn_mov.jpg';
if ($albums && isset($albums[0]['last_photo'])) {
    $last_album_photo = '/uploads/albums/' . $albums[0]['album_id'] . '/preview' . $albums[0]['last_photo']['photo_id'] . '.jpg';
}
if ($pid < 0) {
    header_('Фотоальбомы', '', '', ''
        . '
        <script type="text/javascript" src="/application/views/site/js/nx.js"></script>
', 'body-albums');
} else {
    header_($itsI ? 'Мои фотоальбомы' : 'Альбомы пользователя ' . $_USER['name'], '', '', ''
        . '
        <script type="text/javascript" src="/application/views/site/js/nx.js"></script>
', 'body-albums');
}
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <?php if ($pid < 0) {
                    echo '<img src="/images/arrow.png"> <a href="/group?id=' . ($pid * -1) . '">вернуться назад</a>';
                } else {
                    echo '<img src="/images/arrow.png"> <a href="/' . $_USER['url'] . '">вернуться назад</a>';
                }
                ?>
            </div>
            <div class="block_header">
                <h2>Фотоальбомы</h2>
                <?php
                if ($pid < 0) {
                    echo $itsI ? '<a href="?create&id=' . $pid . '" class="actRight">Создать альбом</a><div class="hiddenAct">w</div>' : '';
                } else {
                    echo $itsI ? '<a href="?create" class="actRight">Создать альбом</a><div class="hiddenAct">w</div>' : '';
                }
                ?>

                <div class="clear"></div>
            </div>
            <div class="block_content">
                <?php
                foreach ($albums as $album) {
                    $last_photo = '';
                    if ($album['last_photo']) {
                        $last_photo = '/uploads/albums/' . $album['album_id'] . '/preview' . $album['last_photo']['photo_id'] . '.jpg';
                    } else {
                        $last_photo = '/images/no_scrn_mov.jpg';
                    }
                    echo '<div class="album">
                    ' . ($itsI ? '<div class="act">
                        <a href="#" class="openAct"><img src="/images/set.png"/></a>
                        <div class="hiddenAct">
                            <a href="/album?id=' . $album['album_id'] . '">Добавить фотографии</a>
                            <a href="/album/set?id=' . $album['album_id'] . '">Настройки</a>
                            <a href="/album?id=' . $album['album_id'] . '&remove">Удалить альбом</a>
                        </div>
                    </div>' : '') . '
                    <div class="preview rounded_3 no_round_bottom">
                        <a href="/album?id=' . $album['album_id'] . '"><img src="' . $last_photo . '"/></a>
                    </div>
                    <div class="desc rounded_3 no_round_top">
                        <a href="/album?id=' . $album['album_id'] . '">' . $album['title'] . '</a>
                        <span class="gray">' . declOfNum($album['size'], array('фотогафия', 'фотографии', 'фотографий')) . '</span>
                    </div>
                </div>';
                }
                ?>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($USER['id'], $priv_companies);
            echo show_main_communities($_USER['id'], $priv_communities);
            echo show_main_album($USER['id'], $priv_photo);
            echo show_main_videos($USER['id'], $priv_video);
            echo show_main_audios($USER['id'], $priv_audios);
            ?>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $(document).on('click', '.openAct', function () {
                $(this).next().show();
                return false;
            });
            $('body').click(function () {
                $('.hiddenAct').hide();
            });
        });
    </script>
<?php footer(); ?>