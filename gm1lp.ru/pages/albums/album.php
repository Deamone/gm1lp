<?php

if (isset($_GET['resizeAlbums'])) {
    updateSizeAlbums();
    exit('12');
}

if (isAjax() && isset($_POST['id'])) {
    $photo = getPhoto($_POST['id']);
    $owner = $photo['album']['user_id'] == $USER['id'];


    if (isset($_POST['add_like'])) {
        likePhoto($_POST['id'], $_POST['aid']);
        exit();
    } elseif (isset($_POST['remove']) && $owner) {
        removePhoto($photo['photo_id'], $_POST['desc'], $_POST['aid']);
        exit('1');
    } elseif (isset($_POST['set_desc']) && $owner) {
        updatePhoto($photo['photo_id'], $_POST['desc'], $_POST['aid']);
        exit('2');
    } elseif (isset($_POST['add_comment'])) {
        addCommentPhoto($photo['photo_id'], $_POST['text'], $_POST['aid']);
        exit('<div class="comment-item">
                            <div class="user-pic">
                                <a href="/' . $USER['url'] . '"><img src="/uploads/avatars/' . $USER['ava'] . '" alt=""></a>
                            </div>
                            <div class="com">
                                <a href="/' . $USER['url'] . '" class="name">' . $USER['name'] . '</a>
                                <p>' . $_POST['text'] . '</p>
                                <span>' . date('d.m.Y H:i:s', time()) . '</span>
                            </div>
                        </div>');
    }

    if (!$photo) {
        exit('<script>showMessage("Документ не найден", 2500);</script>');
    }
    $modal = '<div id="photo-block-shadow" style="/*display: none;*/">
        <div id="photo-block" class="modal-win3">
            <a class="close-href" href="#" onclick=";return false;">Закрыть</a>
            <div id="photo-block-img">
                <img src="' . $photo['file'] . '" alt="" id="opened-photo">
            </div>
            <div class="photo-block-desc">
            ' .
        ($owner ? '<a href="#" onclick=\'$("#formDescPhoto").show();$("#photo-desc").hide();$(this).hide();return false;\' id="a_red" style="padding-left: 10px;">Редактировать описание</a>
                <div class="left" id="red_foto" >
                    <form id="formDescPhoto" style="display:none;">
                        <textarea id="desc_photo" name="desc" cols="55">' . $photo['desc'] . '</textarea>
                        <input type="submit" value="СОХРАНИТЬ" style="float: right;" onclick=\'$("#a_red").show(); $("#formDescPhoto").hide();$("#photo-desc").text($(this).prev().val()).show();\'>
                    </form>
                    <span id="photo-block-date"></span>
                    <span id="photo-desc">' . $photo['desc'] . '</span>
                </div>' : $photo['desc'])
        . '<div class="right" id="like_photo">
                    <span>Нравится</span><a href="javascript:" OnClick="" >G</a><b id="like_foto" >' . $photo['likes'] . '</b>
                </div>
            </div>
            <br/>' . date('d.m.Y H:i:s', $photo['created']) . '
            <div class="photo-comments">
                <div class="left">';
    $comments = getCommentsPhoto($photo['photo_id']);
    $c = count($comments);
    for ($i = 0; $i < $c; $i++) {
        $modal .= '<div class="comment-item">
                            <div class="user-pic">
                                <a href="/' . $comments[$i]['user']['url'] . '"><img src="/uploads/avatars/' . $comments[$i]['user']['ava'] . '" alt=""></a>
                            </div>
                            <div class="com">
                                <a href="/' . $comments[$i]['user']['url'] . '" class="name">' . $comments[$i]['user']['name'] . '</a>
                                <p>' . $comments[$i]['message'] . '</p>
                                <span>' . date('d.m.Y H:i:s', $comments[$i]['created']) . '</span>
                            </div>
                        </div>';
    }
    $modal .= '<form action="/foto" method="post" id="add-comment">
                        <h2>Ваш комментарий</h2>
                        <textarea name="text" id="com_photo"></textarea>
                        <input type="submit" value="ОТПРАВИТЬ">
                    </form>
                </div>
                <div class="right">
                    <span>Альбом</span>
                    <a id="photo-block-url" href="/album?id=' . $photo['album']['album_id'] . '">' . $photo['album']['title'] . '</a>
                    <!--a href="/page/let/foto_comment?id=51&obl=1">Сделать обложкой альбома</a-->
                    ' . ($owner ? '<a href="#" id="remove-photo">Удалить</a>' : '')
        . '</div>
            </div>
            <a href="/page/let/foto?id=58" class="exit_modal" onclick="exit_modal();
                    return false;"></a>
        </div>
    </div>
    <script>
        $(document).on("click",".close-href",function (){
            $("#photo-block-shadow,.exit_modal").remove();
            return false;
        });
        $(document).on("submit","#formDescPhoto",function (){
            $.post("/album", "set_desc=1&id=' . $photo['photo_id'] . '&aid=' . $photo['album_id'] . '&"+$(this).serialize(), function(data, textStatus) {
                
            });
            showMessage("Сохранено!",2000);
            return false;
        });
        $(document).on("click","#remove-photo",function (){
            $.post("/album", "remove=1&id=' . $photo['photo_id'] . '&aid=' . $photo['album_id'] . '", function(data, textStatus) {
                
            });
            $(".close-href").click();
            showMessage("Фотография удалена!",2000);
            return false;
        });
        $(document).on("submit","#add-comment",function (){
            $.post("/album", "add_comment=1&id=' . $photo['photo_id'] . '&aid=' . $photo['album_id'] . '&"+$(this).serialize(), function(data, textStatus) {
                $(".photo-comments .left .comment-item:first").before(data);
                $("#com_photo").val("");
                //showMessage("Комментарий успешно размещен!",2000);
            });
            return false;
        });
        $(document).on("click", "#like_photo", function(e) {
            var id = parseInt($(this).attr("data-id"));
            var ilike = parseInt($(this).attr("data-ilike"));
            var count = parseInt($("#like_foto").html());
            if (!ilike) {
                $("#like_foto").html(count + 1);
                $(this).attr("data-ilike", 1)
            } else {
                $("#like_foto").html(count - 1);
                $(this).attr("data-ilike", 0)
            }
            $.post("/album", "add_like=1&id=' . $photo['photo_id'] . '&aid=' . $photo['album_id'] . '", function(data, textStatus) {
            }
            );
        });
    </script>';
    echo $modal;
    exit();
}


$itsI = false;
$_USER = array();
if (isset($_GET['id'])) {
    $album = getUserAlbum($_GET['id']);
    if ($album['user_id'] < 0) {
        $itsI = false;
        $iUser = true;
        $iMember = false;

        $_USER = $USER;
        $group = getGroup($album['user_id']);
        #$_USER = getUserById($group['owner_id']);
        if (!$group) {
            redirect('/groups');
        }
        $members = getMembersInGroup($group['group_id']);
        if (isset($members['group'][10][$USER['id']]) || isset($members['group'][11][$USER['id']])) {
            $itsI = true;
            $owner = 1;
        }
        if (isset($members[$USER['id']])) {
            $iMember = true;
        }
    } else {
        $_USER = getUserById($album['user_id']);

        if ($_USER['id'] == $USER['id']) {
            $_USER = $USER;
            $itsI = true;
        }
        $owner = $album['user_id'] == $USER['id'] ? 1 : 0;
    }
}

if (!$album) {
    redirect('/albums');
}

$album_id = (int)$_GET['id'];
if (isset($_GET['remove']) && $owner) {
    if (!isset($_GET['pid'])) {
        removeAlbum($album['album_id']);
        redirect('/albums?id=' . $album['user_id']);
    } else {
        $_GET['pid'] = str_replace('i', '', $_GET['pid']);
        removePhoto($album_id, (int)$_GET['pid']);
    }
    exit('1');
}

/**************************/
if (isset($_FILES['files']) && $itsI) {
    $id = addPhotoInAlbum($album_id, '', $album['user_id']);
    if (!file_exists('uploads/albums/' . $album_id)) {
        mkdir('uploads/albums/' . $album_id);
    }
    if ((create_thumbnail($_FILES['files']['tmp_name'], 'uploads/albums/' . $album_id . '/thumb' . $id . '.jpg', 110, 110)) < 1) {
        removePhoto($album_id, $id);
        exit('0' . $_FILES['files']['name']);
    } else {
        create_thumbnail($_FILES['files']['tmp_name'], 'uploads/albums/' . $album_id . '/' . $id . '.jpg', 1440, 900, 'height');
    }

    exit($id . '');
}

/************************/
if (isset($_POST['name']) && isset($_POST['see']) && isset($_POST['comment']) && isset($_POST['desc']) && $owner) {
    $error = '';
    if (empty($_POST['name'])) {
        $error .= $_LANG['ALBUMS_ERROR_EMPTY_NAME'];
    }

    $mask = 0;

    if ($_POST['see'] == 1) {
        $mask |= ALBUM_ALLOW_SEE;
    } elseif ($_POST['see'] == 2) {
        $mask |= ALBUM_ALLOW_FRIENDS_SEE;
    }

    if ($_POST['comment'] == 1) {
        $mask |= ALBUM_ALLOW_COMMENT;
    } elseif ($_POST['comment'] == 2) {
        $mask |= ALBUM_ALLOW_FRIENDS_COMMENT;
    }
    if (!$error) {
        echo json_encode(array('success' => 1));
        updateAlbum($_GET['id'], $_POST['name'], $_POST['desc'], $mask);
    } else {
        echo json_encode(array('error' => $error));
    }
    exit;
}

header_('Альбом ' . $album['title'], '', '', '<script type="text/javascript" src="/js/vendor/jquery.ui.widget.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.fileupload.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>
            <link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js"></script>', ' body-albums');

$myVideos = getVideos($USER['id']);
$count_videos = count($myVideos);

$myContacts = getContacts($_USER['id']);
$cMyContacts = getCountContacts($_USER['id']);


# Right
$priv_photo = isPrivate($USER['id'], $_USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($USER['id'], $_USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($USER['id'], $_USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($USER['id'], $_USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right

$albums = getUserAlbums($_USER['id']);
$count_albums = count($albums);
$last_album_photo = '/images/no_scrn_mov.jpg';
if ($albums && isset($albums[0]['last_photo'])) {
    $last_album_photo = '/uploads/albums/' . $albums[0]['album_id'] . '/preview' . $albums[0]['last_photo']['photo_id'] . '.jpg';
}
if ($itsI) {
    echo '
        <form id="fileupload" action="?" method="POST" enctype="multipart/form-data" style="display: none">
            <input type="file" id="userfile" name="files" multiple="multiple" />
        </form>';
}
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <img src="/images/arrow.png"> <a href="/albums?id=<?php echo $album['user_id']; ?>"> другие альбомы </a>
            </div>
            <div class="block_header">
                <h2>Фотографии</h2>
                <?php echo $itsI ? '<a href="/album/set?id=' . $album_id . '" class="actRight">Редактировать альбом</a>' : ''; ?>
                <div class="clear"></div>
            </div>
            <div class="block_content" style="background-color: transparent;padding-right: 20px;">
                <?php if ($itsI) {
                    echo '<div class="button_add_photo rounded_5">Добавить фотографии</div>';
                } ?>
                <div class="photos">
                    <?php foreach ($album['photos'] as $photo) {
                        echo '<div class="photo rounded_3" id="i' . $photo['photo_id'] . '">
                        '.($owner?'<a href="#" class="delete"><img src="/images/remove.png"/></a>':'').'
                        <a class="fancybox" rel="photos" data-photo-id="' . $photo['photo_id'] . '" href="/uploads/albums/' . $album_id . '/' . $photo['photo_id'] . '.jpg"><img src="/uploads/albums/' . $album_id . '/thumb' . $photo['photo_id'] . '.jpg"/></a>
                    </div>';
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($USER['id'], $priv_companies);
            echo show_main_communities($USER['id'], $priv_communities);
            echo show_main_album($USER['id'], $priv_photo);
            echo show_main_videos($USER['id'], $priv_video);
            echo show_main_audios($USER['id'], $priv_audios);
            ?>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {

            $('.fancybox').fancybox({
                'padding': [20, 20, 0, 20],
                'beforeShow': function () {
                    $this = this.element;

                    $.post('/album/album-ajax', 'photo_id=' + $this.attr('data-photo-id'), function (data) {
                        $('.fancybox-skin').after(data);
                    });
                    return false;
                }
            });
            $('.button_add_photo').click(function () {
                $('#userfile').click();
            });
            $(document).on('click', '.delete', function () {
                $this = $(this).parent();
                $.post('?id=<?php echo $album_id; ?>&remove=1&pid=' + $this.attr('id'), '', function (data) {
                    $this.remove();
                });
                return false;
            });
            $('#fileupload').fileupload({
singleFileUploads: true,
                url: '?id=<?php echo $album_id; ?>',
                dataType: 'html',
                progressall: function (e, data) {
                    fileUpload = false;
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('.progress').html(progress + '%');
                },
                done: function (e, data) {
                    $.fancybox.update();
                    console.log(data.result);
                    fileUpload = true;
                    if (data.result == 0 || data.result.substr(0, 1) == 0) {
                        alert(data.result.substr(1) + ': Запрещенный формат/размер');
                    } else {
                        var html = '<div class="photo rounded_3" id="i' + data.result + '"><a href="#" class="delete"><img src="/images/remove.png"> </a><a class="fancybox" rel="photos" href="/uploads/albums/<?php echo $album_id; ?>/' + data.result + ".jpg?" + Math.random(1000) + '"><img src="/uploads/albums/<?php echo $album_id; ?>/thumb' + data.result + ".jpg?" + Math.random(1000) + '" /></a></div>';
                        if ($('.photos .photo').size()) {
                            $('.photos .photo:first').before(html);//);
                        } else {
                            $('.photos').append(html);
                        }
                    }
                }
            });
        });
    </script>
<?php footer(); ?>
