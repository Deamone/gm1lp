<?php
if (!$USER) {
    exit('<script>$(".fancybox-skin").css({"padding":"20px"})</script>');
}
if (isAjax() && isset($_POST['photo_id'])) {
    $photo_id = (int)$_POST['photo_id'];
    $photo = getPhoto($photo_id);
    $owner = $photo['album']['user_id'] == $USER['id'];

    if ($photo) {
        $album_id = $_POST['aid'] = $photo['album_id'];
        $album = getUserAlbum($album_id);
    } else {
        exit('<script>$(".fancybox-skin").css({"padding":"20px"})</script>');
    }

    if (isset($_POST['add_like'])) {
        $like = likePhoto($photo_id, $album_id);
        if (!$like) {
            $photo['likes'] += 1;
        } else {
            $photo['likes'] -= 1;
        }
        exit(showPhoto($photo_id, $photo, $album));
    } elseif (isset($_POST['remove']) && $owner) {
        removePhoto($photo['photo_id'], $_POST['desc'], $_POST['aid']);
        exit('1');
    } elseif (isset($_POST['set_desc']) && $owner) {
        updatePhoto($photo['photo_id'], $_POST['desc'], $_POST['aid']);
        exit('2');
    } elseif (isset($_POST['add_comment'])) {
        $cid = addCommentPhoto($photo['photo_id'], $_POST['text'], $_POST['aid']);
        exit(getPhotoComment($cid));
    }elseif(isset($_POST['commentLike'])){
        likePhotoComment($_POST['commentLike']);
        exit;
    }

    if (!$photo) {
        exit('<script>showMessage("Документ не найден", 2500);</script>');
    }
    echo '<div class="photoWrap">' . showPhoto($photo_id, $photo, $album) . '</div>';
    $modal = "
    <script>
        $(document).ready(function (){
         if ($('#myEmojiFieldPhoto').size()) {
                window.kemojiComment = KEmoji.init('myEmojiFieldPhoto', {});
                $(document).on('keyup', '#myEmojiFieldPhoto .KEmoji_Input div[contenteditable=true]', function () {
                    console.log($(this).parent().parent().prev());
                    $(this).parent().parent().prev().val($(this).html());
                });
                $(document).on('click', '#myEmojiFieldPhoto .KEmoji_Input div[contenteditable=true]', function () {
                    $('#myEmojiFieldPhoto').addClass('h146');
                });
/*                $('#myEmojiFieldPhoto.short').click(function (){
                    $('.photo-comments .wall_post_btn').show();
                    $(this).removeClass('short');
                });*/
            };
            $('.fancybox-opened').css({'padding':'0 0 20px 0'});
        });
        resetTime();
    </script>
    ";
    echo $modal;
    exit();
}

?>