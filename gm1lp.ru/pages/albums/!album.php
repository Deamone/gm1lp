<?php

if(isset($_GET['resizeAlbums'])){
    updateSizeAlbums();
    exit('12');
}

if (isAjax() && isset($_POST['id'])) {
    $photo = getPhoto($_POST['id']);
    $owner = $photo['album']['user_id'] == $USER['id'];



    if (isset($_POST['add_like'])) {
        likePhoto($_POST['id'], $_POST['aid']);
        exit();
    } elseif (isset($_POST['remove']) && $owner) {
        removePhoto($photo['photo_id'], $_POST['desc'], $_POST['aid']);
        exit('1');
    } elseif (isset($_POST['set_desc']) && $owner) {
        updatePhoto($photo['photo_id'], $_POST['desc'], $_POST['aid']);
        exit('2');
    } elseif (isset($_POST['add_comment'])) {
        addCommentPhoto($photo['photo_id'], $_POST['text'], $_POST['aid']);
        exit('<div class="comment-item">
                            <div class="user-pic">
                                <a href="/' . $USER['url'] . '"><img src="/uploads/avatars/' . $USER['ava'] . '" alt=""></a>
                            </div>
                            <div class="com">
                                <a href="/' . $USER['url'] . '" class="name">' . $USER['name'] . '</a>
                                <p>' . $_POST['text'] . '</p>
                                <span>' . date('d.m.Y H:i:s', time()) . '</span>
                            </div>
                        </div>');
    }

    if (!$photo) {
        exit('<script>showMessage("Документ не найден", 2500);</script>');
    }
    $modal = '<div id="photo-block-shadow" style="/*display: none;*/">
        <div id="photo-block" class="modal-win3">
            <a class="close-href" href="#" onclick=";return false;">Закрыть</a>
            <div id="photo-block-img">
                <img src="' . $photo['file'] . '" alt="" id="opened-photo">
            </div>
            <div class="photo-block-desc">
            ' .
            ($owner ? '<a href="#" onclick=\'$("#formDescPhoto").show();$("#photo-desc").hide();$(this).hide();return false;\' id="a_red" style="padding-left: 10px;">Редактировать описание</a>
                <div class="left" id="red_foto" >
                    <form id="formDescPhoto" style="display:none;">
                        <textarea id="desc_photo" name="desc" cols="55">' . $photo['desc'] . '</textarea>
                        <input type="submit" value="СОХРАНИТЬ" style="float: right;" onclick=\'$("#a_red").show(); $("#formDescPhoto").hide();$("#photo-desc").text($(this).prev().val()).show();\'>
                    </form>
                    <span id="photo-block-date"></span>
                    <span id="photo-desc">'.$photo['desc'].'</span>
                </div>' : $photo['desc'])
            . '<div class="right" id="like_photo">
                    <span>Нравится</span><a href="javascript:" OnClick="" >G</a><b id="like_foto" >' . $photo['likes'] . '</b>
                </div>
            </div>
            <br/>' . date('d.m.Y H:i:s', $photo['created']) . '
            <div class="photo-comments">
                <div class="left">';
    $comments = getCommentsPhoto($photo['photo_id']);
    $c = count($comments);
    for ($i = 0; $i < $c; $i++) {
        $modal .= '<div class="comment-item">
                            <div class="user-pic">
                                <a href="/' . $comments[$i]['user']['url'] . '"><img src="/uploads/avatars/' . $comments[$i]['user']['ava'] . '" alt=""></a>
                            </div>
                            <div class="com">
                                <a href="/' . $comments[$i]['user']['url'] . '" class="name">' . $comments[$i]['user']['name'] . '</a>
                                <p>' . $comments[$i]['message'] . '</p>
                                <span>' . date('d.m.Y H:i:s', $comments[$i]['created']) . '</span>
                            </div>
                        </div>';
    }
    $modal .= '<form action="/foto" method="post" id="add-comment">
                        <h2>Ваш комментарий</h2>
                        <textarea name="text" id="com_photo"></textarea>
                        <input type="submit" value="ОТПРАВИТЬ">
                    </form>
                </div>
                <div class="right">
                    <span>Альбом</span>
                    <a id="photo-block-url" href="/album?id=' . $photo['album']['album_id'] . '">' . $photo['album']['title'] . '</a>
                    <!--a href="/page/let/foto_comment?id=51&obl=1">Сделать обложкой альбома</a-->
                    ' . ($owner ? '<a href="#" id="remove-photo">Удалить</a>' : '')
            . '</div>
            </div>
            <a href="/page/let/foto?id=58" class="exit_modal" onclick="exit_modal();
                    return false;"></a>
        </div>
    </div>
    <script>
        $(document).on("click",".close-href",function (){
            $("#photo-block-shadow,.exit_modal").remove();
            return false;
        });
        $(document).on("submit","#formDescPhoto",function (){
            $.post("/album", "set_desc=1&id=' . $photo['photo_id'] . '&aid=' . $photo['album_id'] . '&"+$(this).serialize(), function(data, textStatus) {
                
            });
            showMessage("Сохранено!",2000);
            return false;
        });
        $(document).on("click","#remove-photo",function (){
            $.post("/album", "remove=1&id=' . $photo['photo_id'] . '&aid=' . $photo['album_id'] . '", function(data, textStatus) {
                
            });
            $(".close-href").click();
            showMessage("Фотография удалена!",2000);
            return false;
        });
        $(document).on("submit","#add-comment",function (){
            $.post("/album", "add_comment=1&id=' . $photo['photo_id'] . '&aid=' . $photo['album_id'] . '&"+$(this).serialize(), function(data, textStatus) {
                $(".photo-comments .left .comment-item:first").before(data);
                $("#com_photo").val("");
                //showMessage("Комментарий успешно размещен!",2000);
            });
            return false;
        });
        $(document).on("click", "#like_photo", function(e) {
            var id = parseInt($(this).attr("data-id"));
            var ilike = parseInt($(this).attr("data-ilike"));
            var count = parseInt($("#like_foto").html());
            if (!ilike) {
                $("#like_foto").html(count + 1);
                $(this).attr("data-ilike", 1)
            } else {
                $("#like_foto").html(count - 1);
                $(this).attr("data-ilike", 0)
            }
            $.post("/album", "add_like=1&id=' . $photo['photo_id'] . '&aid=' . $photo['album_id'] . '", function(data, textStatus) {
            }
            );
        });
    </script>';
    echo $modal;
    exit();
}





$itsI = false;
$_USER = array();
if (isset($_GET['id'])) {
    $album = getUserAlbum($_GET['id']);
    if (!$album) {
        redirect('/albums');
    } elseif (!($album['mask'] & ALBUM_ALLOW_SEE) && $USER['id'] != $album['user_id']) {
        redirect('/albums?id=' . $album['user_id']);
    }
}

if (!$_USER) {
    $_USER = $USER;
    $itsI = true;
}
$owner = $album['user_id']==$USER['id']?1:0;
if(isset($_GET['remove']) && $owner){
    removeAlbum($album['album_id']);
    redirect('/albums?id=' . $album['user_id']);
    exit('1');
}


if (isset($_GET['upload']) && isAjax() && $owner) {
    $json = array('files' => array());
    $max_image_size = 3 * 1024 * 1024;
    $valid_types = array("gif", "jpg", "png", "jpeg");
    if (isset($_FILES["file"])) {
        if (is_uploaded_file($_FILES['file']['tmp_name'])) {
            $filename = $_FILES['file']['tmp_name'];
            $ext = substr($_FILES['file']['name'], 1 + strrpos($_FILES['file']['name'], "."));
            if (filesize($filename) > $max_image_size) {
                $json['files'][0]['error'] = 'error: big size';
            } elseif (!in_array($ext, $valid_types)) {
                $json['files'][0]['error'] = 'error: invalid type';
            } else {
                if (!file_exists('uploads/albums/' . (int) $_GET['id'])) {
                    mkdir('uploads/albums/' . (int) $_GET['id']);
                }
                if (move_uploaded_file($filename, 'uploads/albums/' . (int) $_GET['id'] . '/' . md5($filename) . '.' . $ext)) {
                    $json['files'][0] = '/uploads/albums/' . (int) $_GET['id'] . '/' . md5($filename) . '.' . $ext;
                    addPhotoInAlbum($_GET['id'], $json['files'][0]);
                } else {
                    $json['files'][0]['error'] = 'upload fail';
                }
            }
        } else {
            $json['files'][0]['error'] = 'upload fail2';
        }
    }

    echo json_encode($json);
    exit;
}

if (isset($_POST['name']) && isset($_POST['see']) && isset($_POST['comment']) && isset($_POST['desc']) && $owner) {
    $error = '';
    if (empty($_POST['name'])) {
        $error .= $_LANG['ALBUMS_ERROR_EMPTY_NAME'];
    }

    $mask = 0;

    if ($_POST['see'] == 1) {
        $mask |= ALBUM_ALLOW_SEE;
    } elseif ($_POST['see'] == 2) {
        $mask |= ALBUM_ALLOW_FRIENDS_SEE;
    }

    if ($_POST['comment'] == 1) {
        $mask |= ALBUM_ALLOW_COMMENT;
    } elseif ($_POST['comment'] == 2) {
        $mask |= ALBUM_ALLOW_FRIENDS_COMMENT;
    }
    if (!$error) {
        echo json_encode(array('success' => 1));
        updateAlbum($_GET['id'], $_POST['name'], $_POST['desc'], $mask);
    } else {
        echo json_encode(array('error' => $error));
    }
    exit;
}

header_('Альбом пользователя ' . $_USER['name']);


echo '<div class="container-menu-bottom"></div>
<!-- END container-menu -->
<div class="page-fotoalbum-top-side">
<div>' . $album['title'] . ' <span>всего ' . $album['size'] . ' фотографий</span></div><a href="/albums?id=' . $album['user_id'] . '">К альбомам</a>';
echo '</div><!-- page-top-side -->';
if (isset($_GET['edit']) && $owner) {
    $mask = $album['mask'];
    $q = 0;
    $q_ = 0;
    if ($mask & ALBUM_ALLOW_SEE) {
        $see = $_LANG['ALBUMS_ACCESS_ALL'];
        $q = 1;
    } elseif ($mask & ALBUM_ALLOW_FRIENDS_SEE) {
        $see = $_LANG['ALBUMS_ACCESS_FRIENDS'];
        $q = 2;
    } else {
        $see = $_LANG['ALBUMS_ACCESS_I'];
    }

    if ($mask & ALBUM_ALLOW_COMMENT) {
        $comm = $_LANG['ALBUMS_ACCESS_ALL'];
        $q_ = 1;
    } elseif ($mask & ALBUM_ALLOW_FRIENDS_COMMENT) {
        $comm = $_LANG['ALBUMS_ACCESS_FRIENDS'];
        $q_ = 2;
    } else {
        $comm = $_LANG['ALBUMS_ACCESS_I'];
    }

    echo '<div id="content" class="fotoalbum-page">

    <div class="album-info">
        <div class="edit-album">
            <div class="left">
                <div class="album-bg">
                    <div class="album-wrap">
                        <img src="' . (isset($album['photos'][0]) ? $album['photos'][0]['file'] : '/uploads/albums/main_foto.png') . '" alt="">
                    </div>
                </div>
                <a href="/album?id=' . $album['album_id'] . '&remove">Удалить</a>
            </div>
            <div class="right">
                <h2>Редактирование альбома</h2>
                <form action="/album?id=' . $album['album_id'] . '&edit" method="post"  id="form90" >
                    <input type="file" id="sel-alb-cov" style="display:none;">
                    <span>Название альбома</span>
                    <input type="text" value="' . $album['title'] . '" name="name" autocomplete="off" >
                    <input type="hidden" id="vidit"  value="' . $q . '" name="see" >
                    <input type="hidden" id="comment" value="' . $q_ . '" name="comment" >
                    <span>Комментарий:</span>
                    <textarea name="desc" >' . $album['desc'] . '</textarea>
                    <div>
                        Кому виден этот фотоальбом: 
                        <div class="lightopen"><span id="vidit2" >' . $see . '</span>
                            <ul class="toopen" for="#sel-vis">
                                <li><a href="javascript:" OnClick="$(\'#vidit\').val(\'1\'); $(\'#vidit2\').empty().append(\'Все пользователи\');" val="1">Все пользователи</a></li>
                                <li><a href="javascript:"  OnClick="$(\'#vidit\').val(\'2\');$(\'#vidit2\').empty().append(\'Только друзья\');" val="2">Только друзья</a></li>
                                <li><a href="javascript:"  OnClick="$(\'#vidit\').val(4);$(\'#vidit2\').empty().append(\'Никто\');" val="4">Никто</a></li>
                            </ul>
                        </div>

                    </div>
                    <div>
                        Кто может комментировать фотоальбом: 
                        <div class="lightopen"><span id="comment2" >' . $comm . '</span>
                            <ul class="toopen" for="#sel-com">
                                <li><a href="javascript:" OnClick="$(\'#comment\').val(\'1\');
                                        $(\'#comment2\').empty().append(\'Все пользователи\');" val="1">Все пользователи</a></li>
                                <li><a href="javascript:"  OnClick="$(\'#comment\').val(\'2\');
                                        $(\'#comment2\').empty().append(\'Только друзья\');" val="2">Только друзья</a></li>
                                <li><a href="javascript:"  OnClick="$(\'#comment\').val(4);
                                        $(\'#comment2\').empty().append(\'Никто\');" val="4">Никто</a></li>
                            </ul>
                        </div>
                    </div>
                    <input type="hidden" id="sel-vis">
                    <input type="hidden" id="sel-com">
                    <input type="submit" class="edit-posts" value="Сохранить">
                </form>
            </div>
            <div class="clear"></div>
        </div>

    </div>

    <div class="clearfix"></div><!-- .clearfix -->

    <!--hr>

    <div class="add-foto-in-album">
        <label class="add-post" for="put-in-alb">ДОБАВИТЬ ФОТОГРАФИИ</a>
            <form  method="post" enctype="multipart/form-data" action="/page/let/foto?id=58&red_alb=58&load=1" style="display: none;" >
                <input type="file" name="1" id="put-in-alb" class="photo-file">

                <input type="hidden" name="alb" value="58" >
            </form>
    </div>

    <div class="clearfix"></div>

    <hr--><!-- HR -->


    <!--div class="content">
        <div class="edit-album">
            <form action="/page/let/foto?id=58&red_alb=58&foto=1" method="post">

                <div class="left">
                    <div class="photo-bg">
                        <div class="photo-wrap">
                            <img src="/upload/img/foto/9153f067270f92e556bdd01555235b36.png" />
                        </div>
                    </div>
                </div>
                <div class="right">
                    <h2>Редактирование фотографий</h2>

                    <span>Комментарий к фотографии:</span>
                    <textarea name="51[text]"></textarea>
                </div>
                <div class="clear"></div>


                <div class="center">
                    <input type="submit" class="edit-posts" value="Сохранить">
                </div>
            </form>
        </div>
    </div--><!-- .content --><div class="clearfix"></div><!-- .clearfix -->

</div>
</div><!-- END CONTAINER -->';
} else {
    $c = count($album['photos']);
    echo '<div id="content" class="fotoalbum-page">
                    
                    <div class="album-info">
                        <div class="ai-name">
                            <span>Название альбома</span>
                            <div>' . $album['title'] . '</div>
                        </div>
                        <div class="ai-date">
                            <span>Дата альбома</span>
                            <div>' . date('d.m.y', $album['created']) . '</div>
                        </div>
                        <div class="ai-count">
                            <span>Кол-во фотографий</span>
                            <div>'.$album['size'].'</div>
                        </div>
                        <!--div class="ai-comments">
                            <span>Кол-во комментов</span>
                            <div>0</div>
                        </div-->
						                    </div>
                    
                    <div class="clearfix"></div><!-- .clearfix -->
                    
                    <hr><!-- HR -->
                                        
                    <div class="content">
                        <ul>					<!--li-->';
    
    $n=0;
    $c_ = count($album['photos']);
    foreach ($album['photos'] as $photo) {
        $n++;
        if (($n==1 || (int)(($n-1)/4)==(($n-1)/4))) {
            echo '<li>';
        }
        echo '<div data-pid="' . $photo['photo_id'] . '" class="nx-photo-file">
                                    <a href="#"> <img src="' . $photo['file'] . '" /></a>
                                    <a href="#!"></a>
                                    <div class="meta">
                                        <div class="comments">' . $photo['comments'] . '</div>
                                        <div class="date">' . date('d.m.Y', $photo['created']) . '</div>
                                        <a href="#" class="like">' . $photo['likes'] . '</a>
                                        <div class="clearfix"></div><!-- .clearfix -->
                                    </div>
								 </div>';
        if ($n==$c_ || (int)(($n)/4)==(($n)/4)) {
            echo '</li>';
        }
    }
    echo '
							<!--/li-->	
                        </ul>
                   
                    </div><!-- .content -->


		
		
	
          <div class="clearfix"></div><!-- .clearfix -->

                </div>
            </div><!-- END CONTAINER -->

            <div class="clearfix"></div><!-- .clearfix -->
';
}
?>
<script>
    $(document).ready(function() {
        $(document).on('click', '.edit-posts', function(e) {

            $but = messLoading('.edit-posts', 'value', 'Идет обработка');
            $.post($('#form90').attr('action'), $('#form90').serialize(), function(data) {
                console.log(data);
                $but.fire();
                if (data.success !== undefined) {
                    showMessage('Сохранено')
                } else {
                    showMessage(data.error);
                }
            }, 'json');
            e.preventDefault();
            return false;
        });
        /*$(document).on('click', 'div[data-pid]', function(e) {
         var src = $(this).find('img').attr('src');
         var likes = $(this).find('.like').html();
         var comments = $(this).find('.comments').html();
         var date = $(this).find('.date').html();
         var album_name = "<?php echo $album['title']; ?>";
         var album_id = "<?php echo $album['album_id']; ?>";
         
         $('#photo-block-img img').attr('src', src);
         $('#photo-block-date').html(date);
         $('#like_foto').html(likes);
         $('#add-comment').attr('action', '/foto?id=' + album_id);
         $('#photo-block-url').attr('href', '/album?id=' + album_id).html(album_name);
         $('#photo-block-shadow').show();
         console.log(src);
         return false;
         });*/
    });
</script>

<?php footer(); ?>