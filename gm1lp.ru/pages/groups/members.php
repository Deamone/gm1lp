<?php
/**
 * =====================================================
 * created 21.01.2015 NotusX
 * -----------------------------------------------------
 * http://notusx.ru/
 * -----------------------------------------------------
 * Copyright (c) 2014,2015 NotusX
 * =====================================================
 * Данный код защищен авторскими правами
 * =====================================================
 * @author NotusX
 * -----------------------------------------------------
 * Файл: contacts.php
 * -----------------------------------------------------
 * Назначение: Не задано
 * =====================================================
 */

$itsI = false;
$iUser = true;
$iMember = false;

$_USER = $USER;

if (isset($_GET['id'])) {
    $group = getGroup($_GET['id']);
    #$_USER = getUserById($group['owner_id']);
    if (!$group) {
        redirect('/groups');
    }
}
$members = getMembersInGroup($group['group_id']);
if (isset($members['group'][10][$USER['id']]) || isset($members['group'][11][$USER['id']])) {
    $itsI = true;
}
if (isset($members[$USER['id']])) {
    $iMember = true;
}
if ($itsI && isset($_POST['pow']) && $USER['id']==$group['owner_id']) {
    $pow = $_POST['pow'];
    $id = $_POST['id'];

    if (!isset($members[$id]) || $id == $group['owner_id']) {
        exit();
    }
    if ($pow) {
        updateMemberInGroup($group['group_id'], $id, 3);
    } else {
        updateMemberInGroup($group['group_id'], $id, 1);
    }
    exit('1');
}
header_($group['type'] == 0 ? 'Подписчики' : 'Сотрудники', '', '', ''
    . '
', 'body-search body-contacts');


$myContacts = getContacts($USER['id']);
$cMyContacts = getCountContacts($USER['id']);
if (!$itsI) {
    #shuffle($members);
}


$citys = '';
$citys_ = array();
$countrys = '';
$countrys_ = array();
$contacts = array();
$group_ = $members['group'];
unset($members['group']);
unset($members[$USER['id']]);
foreach ($members as $k => $contact) {
    $contacts[$k]['user'] = $user = getUserById($k);
    $country = getCountry($user['country']);
    $city = getCity($user['city']);
    $contacts[$k]['city'] = array($user['city'], $city['city']);
    $contacts[$k]['country'] = array($user['country'], $country['country']);

    if ($city['city'] && !isset($citys_[$city['city']])) {
        $citys .= '<option value="' . $user['city'] . '">' . $city['city'] . '</option>';
        $citys_[$city['city']] = 1;
    }
    if ($country['country'] && !isset($countrys_[$country['country']])) {
        $countrys .= '<option value="' . $user['country'] . '">' . $country['country'] . '</option>';
        $countrys_[$country['country']] = 1;
    }
}
#print_r($contacts);
if (!$citys) {
    $citys = '<option value="0" selected>Не указано</option>';
}
if (!$countrys) {
    $countrys = '<option value="0" selected>Не указано</option>';
}

# Right
$priv_photo = isPrivate($_USER['id'], $_USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($_USER['id'], $_USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($_USER['id'], $_USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($_USER['id'], $_USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <img src="/images/arrow.png"> <a href="/group?id=<?php echo $group['group_id']; ?>">вернуться назад</a>
            </div>
            <div class="block_search">
                <form action="search">
                    <div class="block_field">
                        Страна<br/>

                        <div class="styled-select">
                            <select name="country" id="country" style="max-width: 175px;">
                                <option value="0" selected="selected">Все страны</option>
                                <?php echo $countrys; ?>
                            </select>
                        </div>
                    </div>
                    <div class="block_field">
                        Город<br/>

                        <div class="styled-select">
                            <select name="city" id="city" style="max-width: 175px;">
                                <option value="0" selected="selected">Все города</option>
                                <?php echo $citys; ?>
                            </select>
                        </div>
                    </div>
                    <div class="block_field">
                        Пол<br/>

                        <div class="styled-select">
                            <select name="gender" id="gender">
                                <option value="male">Мужской</option>
                                <option value="female">Женский</option>
                            </select>
                        </div>
                    </div>
                    <div class="block_field">
                        Год рождения<br/>

                        <div class="styled-select">
                            <select name="year1" id="year1">
                                <?php
                                for ($i = 1967; $i <= 2014; $i++) {
                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <span class="yline">&HorizontalLine;</span>

                        <div class="styled-select">
                            <select name="year2" id="year2">
                                <?php
                                for ($i = 1968; $i <= 2014; $i++) {
                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <input name="query" type="text" class="search_left" id="searchy" autocomplete="off" value=""
                           placeholder="Фамилия, Имя"/>
                </form>
            </div>
            <div class="block_header" style="display: block">
                <h2><?php echo $group['type'] == 0 ? 'Подписчики' : 'Сотрудники'; ?></h2>

                <div class="clear"></div>
            </div>

            <div class="block_content">
                <?php
                reset($contacts);
                $time = time();
                foreach ($contacts as $contact) {
                    $online = '<img class="online_ico" src="/images/offline_png.png" />';
                    if (($time - $contact['user']['online']) < 15 * 60) {
                        $online = '<img class="online_ico" src="/images/online_png.png" />';
                    }
                    $user = $contact['user'];
                    $y = 0;
                    if ($user['date'] && is_numeric($user['date'])) {
                        $y = date('Y', $user['date']);
                    }
                    $pow = '';
                    if($group['owner_id']==$USER['id']) {
                        if ($members[$user['id']]['type'] > 1) {
                            $pow = '<button class="btn1 btn1w gRaz" data-id="' . $user['id'] . '">Разжаловать</button>';
                        } else {
                            $pow = '<button class="btn1 btn1w gPow" data-id="' . $user['id'] . '">Повысить</button>';
                        }
                    }
                    echo '<div class="contact" data-year="' . $y . '" data-name="' . $user['name'] . ' ' . $user['fam'] . '" data-city="' . $contact['city'][0] . '" data-country="' . $contact['country'][0] . '" data-gender="' . $user['gender'] . '">
                <div class="left">
                    <a href="/?id=' . $user['id'] . '"><img class="rounded_3" src="' . '/uploads/avatars/' . $user['ava'] . '" /></a>
                </div>
                <div class="right">
                    <a href="/?id=' . $user['id'] . '">' . $user['name'] . ' ' . $user['fam'] . '</a> ' . $online . '
                    <br/>
                    <form action="/message?id=' . $user['id'] . '" method="post"><button class="btn1">Написать сообщение</button></form><form method="post" action="/contacts?id=' . $user['id'] . '"><button class="btn1">Посмотреть контакты</button></form>' . $pow . '
                </div>
            </div>';
                }
                ?>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($_USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($_USER['id'], $priv_companies);
            echo show_main_communities($_USER['id'], $priv_communities);
            echo show_main_album($_USER['id'], $priv_photo);
            echo show_main_videos($_USER['id'], $priv_video);
            echo show_main_audios($_USER['id'], $priv_audios);
            ?>
        </div>

        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('#country').change(function () {
                var $val = $(this).val().toLowerCase();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country').toLowerCase();
                    var city = $(this).attr('data-city').toLowerCase();
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year');

                    if ($val == '0' && (city == $('#city').val() || $('#city').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if ($val != country) {
                            $(this).hide();
                        }
                        else if ((city == $('#city').val() || $('#city').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            $('#city').change(function () {
                var $val = $(this).val().toLowerCase();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country').toLowerCase();
                    var city = $(this).attr('data-city').toLowerCase();
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year');

                    if ($val == '0' && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if ($val != city) {
                            /*console.log(city, $('#city').val());
                             console.log(country, $('#country').val());
                             console.log(gender, $('#gender').val());*/
                            $(this).hide();
                        }
                        else if ((country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            $('#gender').change(function () {
                var $val = $(this).val().toLowerCase();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country').toLowerCase();
                    var city = $(this).attr('data-city').toLowerCase();
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year').toLowerCase();

                    if ($val == '0' && (city == $('#city').val() || $('#city').val() == 0) && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if ($val != gender) {
                            /*console.log(city, $('#city').val());
                             console.log(country, $('#country').val());
                             console.log(gender, $('#gender').val());*/
                            $(this).hide();
                        }
                        else if ((city == $('#city').val() || $('#city').val() == 0) && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            $('#year1,#year2').change(function () {
                var $val = $('#year1').val();
                var $val2 = $('#year2').val();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country');
                    var city = $(this).attr('data-city');
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year').toLowerCase();
                    console.log(year, $val, $val2);
                    if (year > $val && year < $val2 && (city == $('#city').val() || $('#city').val() == 0) && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if (year < $val || year > $val2) {
                            /*console.log(city, $('#city').val());
                             console.log(country, $('#country').val());
                             console.log(gender, $('#gender').val());*/
                            $(this).hide();
                        }
                        else if (year > $val && year < $val2 && (city == $('#city').val() || $('#city').val() == 0) && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            $('#searchy').keyup(function () {
                var $val = $(this).val().toLowerCase();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country').toLowerCase();
                    var city = $(this).attr('data-city').toLowerCase();
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year');

                    if ($val == '0' && (country == $('#country').val() || $('#country').val() == 0) && (city == $('#city').val() || $('#city').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if (name.indexOf($val) == '-1') {
                            $(this).hide();
                        }
                        else if ((country == $('#country').val() || $('#country').val() == 0) && (city == $('#city').val() || $('#city').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            //searchy

            $(document).on('click', '.gPow', function () {
                $this = $(this);
                var id = $this.data('id');

                $.post('', 'pow=1&id=' + id, function (data) {
                    $this.removeClass('gPow').addClass('gRaz');
                    $this.text('Разжаловать');
                    if (confirm("Пользователь повышен, перейти к настройке?")) {
                        window.location.href = "/group/set?id=<?php echo $group['group_id']; ?>";
                    }
                });
                return false;
            });

            $(document).on('click', '.gRaz', function () {
                $this = $(this);
                var id = $this.data('id');

                $.post('', 'pow=0&id=' + id, function (data) {
                    $this.removeClass('gRaz').addClass('gPow');
                    $this.text('Повысить');
                });
                return false;
            });
        });
    </script>
<?php
footer();
