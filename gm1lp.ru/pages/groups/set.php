<?php


$itsI = false;
$iUser = true;
$iMember = false;

$_USER = $USER;

if (isset($_GET['id'])) {
    $group = getGroup($_GET['id']);
    #$_USER = getUserById($group['owner_id']);
    if (!$group) {
        redirect('/groups');
    }
}
$members = getMembersInGroup($group['group_id']);
if (isset($members['group'][10][$USER['id']]) || isset($members['group'][10][$USER['id']]) || isset($members['group'][11][$USER['id']])) {
    $itsI = true;
}
if (isset($members[$USER['id']])) {
    $iMember = true;
}
if (!$group['url']) {
    $group['url'] = 'group' . $group['group_id'];
}
/************************/
if (isset($_POST['contacts'])) {
    $contacts_ = $_POST['contacts'];
    $i = $members[$USER['id']];
    foreach ($contacts_ as $id => $contact) {
        if (!isset($members[$id]) || $members[$id] < 3) {
            continue;
        }
        if ($USER['id'] != $id && $i['type'] <= $member['type'] && $i['type'] != 11) {
            continue;
        }
        $role = isset($_POST['role'][$id]) ? $_POST['role'][$id] : '';
        $type = isset($_POST['type'][$id]) ? $_POST['type'][$id] : '';

        updateMemberInGroup($group['group_id'], $id, $type, $role, $contact);
    }
    exit('Сохранено!');
}
/************************/
if (isset($_POST['title']) && isset($_POST['desc']) && $itsI) {
    $error = '';
    if (empty($_POST['title'])) {
        $error .= 'Укажите название';
    }
    if (!empty($_POST['site'])) {
        if ($parts = parse_url($_POST['site'])) {
            if (!isset($parts["scheme"])) {
                $_POST['site'] = "http://" . $_POST['site'];
            }
        }
        if (!filter_var($_POST['site'], FILTER_VALIDATE_URL)) {
            $error .= 'Сайт указан некорректно';
        } else {
            $_POST['site'] = preg_replace('/http(.?):\/\//i', '', $_POST['site']);
        }
    }
    if (isset($_POST['founding'])) {
        if (!strtotime($_POST['founding'])) {
            $error .= 'Дата основания указана некорректно';
        } else {
            $_POST['founding'] = strtotime($_POST['founding']);
        }
    }


    if ($group['url'] != $_POST['url']) {
        if (preg_match('/id(\d+)/i', $_POST['url']) && $_POST['url'] != 'id' . $group['group_id']) {
            $error .= 'Запрещенный Красивый ID';
        } elseif (preg_match('/group(\d+)/i', $_POST['url']) && $_POST['url'] != 'group' . $group['group_id']) {
            $error .= 'Запрещенный Красивый ID';
        } elseif (!preg_match('/^[a-z0-9_-]{5,64}$/i', $_POST['url'])) {
            $error .= 'Красивый ID может состоять из букв, цифр, дефисов и подчёркиваний. Длина от 5 до 64 символов';
        } elseif (($u = getUserByUrl($_POST['url'])) || (($g = getGroupByUrl($_POST['url'])) && $g['group_id'] != $group['group_id'])) {
            $error .= 'Красивый ID уже занят';
        } else {
            updateGroupUrl($group['group_id'], $_POST['url']);
        }
    }

    if (!$error) {
        updateGroupSettings($group['group_id'], $_POST['title'], $_POST['desc'], $_POST['area'], $_POST['country'], $_POST['city'], $_POST['site'], $_POST['founding']);
        exit('$("#alertHeader").text("Успешно!");$("#alertMessage").text("Данные успешно сохранены!"); showModal(".alertMessage");');
    } else {
        exit('alert("' . str_replace('"', "'", $error) . '");');
    }
}

header_('' . $group['title'], '', '', '', ' body-albums');
$myContacts = getContacts($_USER['id']);
$cMyContacts = getCountContacts($_USER['id']);

# Right
$priv_photo = isPrivate($USER['id'], $USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($USER['id'], $USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($USER['id'], $USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($USER['id'], $USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right

$group['founding'] = date('d.m.Y', $group['founding']);
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <img src="/images/arrow.png"> <a href="/group?id=<?php echo $group['group_id']; ?>"> назад</a>
            </div>
            <div class="block_header">
                <h2>Настройки</h2>

                <div class="clear"></div>
            </div>
            <div class="block_content" style="background-color: transparent;padding-right: 20px;">
                <div class="left_form">
                    <form method="post" id="formAlbum">
                        <span>Название</span>
                        <input type="text" name="title" value="<?php echo $group['title']; ?>"/>
                        <span>Сфера деятельности</span>
                        <input type="text" name="area" value="<?php echo $group['area']; ?>"/>
                        <span>Страна</span>
                        <input type="text" name="country" value="<?php echo $group['country']; ?>"/>
                        <span>Город</span>
                        <input type="text" name="city" value="<?php echo $group['city']; ?>"/>
                        <span>Сайт</span>
                        <input type="text" name="site" value="<?php echo $group['site']; ?>"/>

                        <div class="relative">
                            <span>Красивый ID</span>
                            <input type="text" name="url" data-group-id="<?php echo $group['group_id']; ?>" data-url="<?php echo $group['url']; ?>" value="<?php echo $group['url']; ?>"/>

                            <div class="url"></div>
                        </div>
                        <span>Описание</span>
                        <textarea name="desc"><?php echo $group['desc']; ?></textarea>
                        <?php if ($group['type']): ?>
                            <span>Дата основания</span>
                            <input type="text" name="founding" value="<?php echo $group['founding']; ?>"/>
                        <?php endif; ?>
                        <!--span class="w">Кому виден этот альбом </span>

                        <div class="styled-select">
                            <select name="see">
                                <option value="1"<?php echo $see == 1 ? ' selected' : ''; ?>>Всем пользователям</option>
                                <option value="2"<?php echo $see == 2 ? ' selected' : ''; ?>>Только друзьям</option>
                                <option value="0"<?php echo $see == 0 ? ' selected' : ''; ?>>Только мне</option>
                            </select>
                        </div-->
                        <br/>
                        <br/>
                        <button class="btn rounded_3">Сохранить</button>
                    </form>
                    <br/>
                    <br/>
                </div>
            </div>
            <div class="block_header">
                <h2>Администрация</h2>

                <div class="clear"></div>
            </div>
            <div class="block_content" style="background-color: transparent;padding-right: 20px;">
                <div class="left_form userAdmin">
                    <form method="post" id="formAdmins">
                        <?php
                        unset($members['group']);
                        $i = $members[$USER['id']];
                        foreach ($members as $member) {
                            if ($member['type'] < 3) {
                                continue;
                            }
                            $user = getUserById($member['user_id']);
                            $access = true;
                            if ($USER['id'] != $member['user_id'] && $i['type'] <= $member['type'] && $i['type'] != 11) {
                                $access = false;
                            }
                            echo '<div class="user">
                            <div class="left">
                                <a href="/?id=' . $user['id'] . '" target="_blank"><img width="48" class="rounded_3"
                                                          src="/uploads/avatars/' . $user['ava'] . '"></a>
                                <a href="/?id=' . $user['id'] . '" target="_blank">' . $user['name'] . ' ' . $user['fam'] . '</a>
                            </div>
                            <div class="right">
                                ' . ($access ? '<a href="#" class="settAdmin" ' . $access . '>Настройки</a>

                                <div class="ns">
                                    Тип:
                                    <div class="styled-select"><select name="type[' . $user['id'] . ']">
                                            ' . ($member['type'] != 11 ? '<option value="3" ' . ($member['type'] == 3 ? 'selected' : '') . '>Контакт</option>
                                            <option value="10" ' . ($member['type'] == 10 ? 'selected' : '') . '>Администратор</option>' : '<option value="11" selected>Создатель</option>') . '

                                        </select></div>
                                    <br/>
                                    Роль: <input name="role[' . $user['id'] . ']" type="text" maxlength="32" size="32" value="' . $member['role'] . '"/><br/>
                                    <span>Контакты:</span><textarea name="contacts[' . $user['id'] . ']">' . $member['contacts'] . '</textarea>
                                </div>' : '') . '
                            </div>
                        </div>
                        <div class="clearfix"></div><br/>';
                        }
                        ?>
                        <button class="btn rounded_3">Сохранить</button>
                    </form>
                    <br/>
                    <br/>
                </div>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($_USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($_USER['id'], $priv_companies);
            echo show_main_communities($_USER['id'], $priv_communities);
            echo show_main_album($_USER['id'], $priv_photo);
            echo show_main_videos($_USER['id'], $priv_video);
            echo show_main_audios($_USER['id'], $priv_audios);
            ?>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('form#formAlbum').submit(function () {
                $.post('', $(this).serialize(), function (data) {
                    eval(data);
                });
                return false;
            });
            $('form#formAdmins').submit(function () {
                $.post('', $(this).serialize(), function (data) {
                    eval(data);
                });
                return false;
            });
        });
    </script>
<?php footer(); ?>