<?php


$itsI = true;
$iUser = true;

$_USER = $USER;

/************************/
if (isset($_POST['title']) && isset($_POST['desc']) && $itsI) {
    $error = '';
    if (empty($_POST['title'])) {
        $error .= 'Укажите название';
    }

    if (!$error) {
        $id = createGroup($_POST['title'], $_POST['type'], $_POST['area'], $_POST['country'], $_POST['city'], $_POST['desc']);
        echo '-' . $id;
    } else {
        echo $error;
    }
    exit;
}
header_('Создание нового сообщества', '', '', '', ' body-albums');
$myContacts = getContacts($_USER['id']);
$cMyContacts = getCountContacts($_USER['id']);

# Right
$priv_photo = isPrivate($USER['id'], $USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($USER['id'], $USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($USER['id'], $USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($USER['id'], $USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right

?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <img src="/images/arrow.png"> <a href="/"> назад</a>
            </div>
            <div class="block_header">
                <h2>Новое сообщество</h2>

                <div class="clear"></div>
            </div>
            <div class="block_content" style="background-color: transparent;padding-right: 20px;">
                <b>Чем Компании отличаются от Сообществ?</b>
                <div>Компания имеет прямое отношение к существующей компании в мире. В компании в основном вступают сами сотрудники, а сообщества создаются просто для поддержки пользователей и информационной поддержки. Помимо этого, у компании можно указывать ключевые навыки, чем занимается компания, а у сообществ такой функции нет.</div>
                <br/><br/>
                <div class="left_form">
                    <form method="post" id="formAlbum">
                        <span>Название</span>
                        <input type="text" name="title" value=""/>
                        <span>Сфера деятельности</span>
                        <input type="text" name="area" value=""/>
                        <span>Страна</span>
                        <input type="text" name="country" value=""/>
                        <span>Город</span>
                        <input type="text" name="city" value=""/>
                        <span>Описание</span>
                        <textarea name="desc"></textarea>
                        <span class="w">Тип </span>

                        <div class="styled-select">
                            <select name="type">
                                <option value="0">Сообщество</option>
                                <option value="1">Компания</option>
                            </select>
                        </div>
                        <br/>
                        <br/>
                        <button class="btn rounded_3">Создать</button>
                    </form>
                    <br/>
                    <br/>
                </div>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($_USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($_USER['id'], $priv_companies);
            echo show_main_communities($_USER['id'], $priv_communities);
            echo show_main_album($_USER['id'], $priv_photo);
            echo show_main_videos($_USER['id'], $priv_video);
            echo show_main_audios($_USER['id'], $priv_audios);
            ?>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('form#formAlbum').submit(function () {
                $.post('', $(this).serialize(), function (data) {
                    if (data.substr(0, 1) == '-') {
                        window.location.href = '/group?id=' + data.replace('-','');
                    } else {
                        alert(data);
                    }
                });
                return false;
            });
        });
    </script>
<?php footer(); ?>