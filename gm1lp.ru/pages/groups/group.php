<?php
$itsI = false;
$iUser = true;
$iMember = false;

$class = ' noti';
if (!$USER || !$USER['id']) {
#    redirect('/page/login');
    $USER = array(
        'ava' => '../../images/avatar-main.jpg'
    );
    $iUser = false;
}
$_USER = $USER;
$iUser = true;
$id = isset($_GET['_route_']) ? query_escape($_GET['_route_']) : '';
if (isset($_GET['id'])) {
    $group = getGroup($_GET['id']);
    #$_USER = getUserById($group['owner_id']);
    if (!$group) {
        redirect('/groups');
    }
} elseif ($id && ($_ = getGroupByUrl($id))) {
    $group = $_;
} else {
    redirect('/groups');
}
$members = getMembersInGroup($group['group_id']);
if (isset($members['group'][10][$USER['id']]) || isset($members['group'][11][$USER['id']])) {
    $itsI = true;
}
if (isset($members[$USER['id']])) {
    $iMember = true;
}
if (isset($_GET['join'])) {
    if (!$_GET['join'] && $iMember && $group['owner_id'] != $USER['id']) {
        removeMemberInGroup($group['group_id'], $USER['id']);
    } elseif ($_GET['join'] && !$iMember) {
        addMemberInGroup($group['group_id'], $USER['id']);
    }
    if (!isAjax()) {
        redirect('/group?id=' . $group['group_id']);
    }
    exit('1');
}

if ($itsI) {
    $class = '';
    if (isAjax()) {
        $json = array();
        if (isset($SYSTEM['get']['upload'])) {

            $file = $_FILES['file'];
            $ext = getExtension($file["name"]);
            if (strpos($ext, 'ph')!==false || strpos($ext, 'png') !== false) {
                exit($json);
            }
            if (isset($SYSTEM['get']['iavatar'])) {
                $iavatar = $SYSTEM['get']['iavatar'];
                $file = prepareImg($_POST);
                $file_name = str_replace('tmp/', '', $file);
                if ($iavatar) {
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/g_avatars/';
                    copy($file, $path . $file_name);
                    create_thumbnail($path . $file_name, $path . $file_name, 320, 320, "none");
                    //updateUserById($USER['id'], array('ava'), array($file_name));
                    updateGroupAva($group['group_id'], $file_name);
                    exit('' . $file_name);
                } else {
                    $file = prepareImg($_POST);
                    //$file = str_replace('..', '', 'tmp/' . $path);
                    $file_name = str_replace('tmp/', '', $file);
                    $file_name = $group['group_id'] . '.png';
                    $new_name = $group['group_id'] . '.png';
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/g_slide/';
                    copy($file, $path . $file_name);
                    #exit('' . $file_name);
                    #crop($path . $file_name, $path . $new_name);
                    create_thumbnail($path . $file_name, $path . $new_name, 1181, 478, "width");
                    exit('' . $file_name);
                }

            } elseif (isset($SYSTEM['get']['avatar'])) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/g_avatars/';
                $time = time();
                if ($SYSTEM['get']['avatar'] == 1) {
                    /*move_uploaded_file($file["tmp_name"], $path . '' . $json['file']);
                    create_thumbnail($path . '' . $json['file'], $path . '' . $json['file'], 156, 156, "none");

                    updateUserById($USER['id'], array('ava'), array($json['file']));*/
                } else {
                    $file = $_FILES['file'];
                    $json['file'] = 'tmp/' . time() . '.' . getExtension($file["name"]);
                    move_uploaded_file($file["tmp_name"], $json['file']);

                    //create_thumbnail($json['file'], $json['file'], 960, 529, "height");
                    exit(json_encode($json));
                }
            } else {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/g_slide/';
                $time = time();
                if ($SYSTEM['get']['slide'] == 1) {
                    /*$json['file'] = $USER['id'] . '.';
                    move_uploaded_file($file["tmp_name"], $path . '' . $json['file'] . getExtension($file["name"]));
                    create_thumbnail($path . '' . $json['file'] . getExtension($file["name"]), $path . '' . $json['file'] . '.jpg', 1181, 470, "none");
                    $json['file'] .= getExtension($file["name"]);*/
                } else {
                    $file = $_FILES['file'];
                    $json['file2'] = 'tmp/' . time() . '.png';
                    $json['file'] = 'tmp/' . time() . '.' . getExtension($file["name"]);
                    move_uploaded_file($file["tmp_name"], $json['file']);
                    create_thumbnail($json['file'], $json['file'], 1376, 529, "height");
                    exit(json_encode($json));
                }
            }
            exit(json_encode($json));
        }
    }
}
if (isAjax() && $iUser) {
    if (isset($_GET['del_post']) || isset($_POST['del_post'])) {
        $id = (int)$_POST['id'];
        $post = getPost($id);
        if ($post && ($post['user_id'] == $USER['id'] || $post['owner_id'] == $USER['id'] || $itsI)) {
            removePost($id);
        }
        exit('удаляем');
    } elseif (isset($_GET['del_comment'])) {
        $pid = (int)$_POST['pid'];
        $cid = (int)$_POST['cid'];
        $post = getPost($pid);
        $comments = query("SELECT `user_id` FROM `nx_wall_comments` WHERE `post_id`='" . $pid . "' AND `cid`='" . $cid . "';");

        if ($post && ($post['owner_id'] == $USER['id'] || $USER['id']) == $comments['row']['user_id'] || $itsI) {
            query("DELETE FROM `nx_wall_comments` WHERE `post_id`='" . $pid . "' AND `cid`='" . $cid . "';");
        }
        exit('удаляем');
    } elseif (isset($_POST['add_post']) && $iUser && $iMember) {
        $post = $SYSTEM['post']['text'];
        $file = $SYSTEM['post']['file'];
        if ($file) {
            foreach ($file as $k => $file_) {
                $name = str_replace('mini_', '', substr($file_, strrpos($file_, '/')));
                if (!file_exists('uploads/wall/' . $group['group_id'] * -1 . '/' . $name)) {
                    unset($file[$k]);
                } else {
                    $file[$k] = 'uploads/wall/' . $group['group_id'] * -1 . '/' . $name;
                }
            }
        }
        $fromGroup = isset($_POST['fromGroup']) && $itsI ? $group['group_id'] * -1 : false;
        $hide_data = $SYSTEM['post']['hide_data'];
        preg_match_all('/\[(\w+)=(\d+)\]/i', $hide_data, $arr);
        $hide_data = implode('', $arr[0]);
        addPost($group['group_id'] * -1, $post . $hide_data, $fromGroup, serialize($file), $fromGroup);
        exit(outputPost(getPost(query_lastInsertId())));
    } elseif (isset($_POST['all_comments'])) {
        $wid = $SYSTEM['post']['wid'];
        exit(outputCommentsPost($wid, true));
    } elseif ((isset($_GET['hide_photo']) || isset($_POST['hide_photo'])) && $itsI) {
        $pid = $SYSTEM['post']['pid'];
        $uid = $group['group_id'] * -1;
        query("UPDATE `nx_photos` SET `show_home`=0 WHERE `user_id`='$uid' AND `photo_id`='$pid';");
        exit('1');
    } elseif (isset($_POST['wall_offset'])) { ///////////////////////// TARGET
        $offset = $SYSTEM['post']['wall_offset'];
        $posts = getPosts($group['group_id'] * -1, $offset, 5, true);
        foreach ($posts as $post) {
            echo outputPost($post, true, true);
        }
        exit();
    } elseif (isset($_POST['add_comment']) && $iUser && $iMember) {
        $post = $SYSTEM['post']['text'];
        $wid = $SYSTEM['post']['wid'];

        $fromGroup = isset($_POST['fromGroup']) && $itsI ? $group['group_id'] * -1 : false;
        addPostComment($wid, $post, $fromGroup);
        exit(outputCommentsPost($wid));
    } elseif (isset($_POST['get_post'])) {
        $id = $_POST['get_post'];
        $post = getPost($id);
        echo $post['text'];
        exit();
    } elseif (isset($_POST['edit_post'])) {
        $id = $_POST['id'];
        $post = getPost($id);
        if ($post && $post['user_id'] == $USER['id'] || $itsI) {
            updatePost($id, $_POST['post']);
        }
        exit('1');
    } elseif (isset($_POST['add_like']) && $iUser) {
        $id = $_POST['id'];
        if (isset($_POST['comment'])) {
            likeComment($id);

            exit('2');
        } else {
            likePost($id);
        }

        exit('1');
    } elseif (isset($_POST['repost']) && $iUser) {
        $id = $_POST['id'];
        $t = rePost($id);

        exit('' . $t);
    }
    /** .Wall * */
    #?like=get&wid='+wall_id
    if (isset($_GET['like']) && isset($_GET['wid']) && $iUser) {
        $likes = getLikesPost($_GET['wid'], false, 9999, true);
        exit(json_encode($likes));
    }
    if (isset($_GET['like']) && isset($_GET['cid']) && $iUser) {
        $likes = getLikesComments($_GET['cid'], false, 999);
        exit(json_encode($likes));
    }
    if ($_USER['id']) {
        $json = array();
        if (isset($SYSTEM['get']['uploadWall'])) {
            $file = $_FILES['file'];
            $ext = getExtension($file["name"]);
            if (strpos($ext, 'ph')!==false || strpos($ext, 'png') !== false) {
                exit($json);
            }
            $uid = $SYSTEM['get']['uid'];
            $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/wall/' . $uid . '/';
            if (!file_exists($path)) {
                mkdir($path);
            }
            $time = time();
            $ext = getExtension($file["name"]);
            $json['file'] = $time . '.' . $ext;
            move_uploaded_file($file["tmp_name"], $path . '' . $json['file']);
            create_thumbnail($path . '' . $json['file'], $path . 'mini_' . $json['file'], 71, 71, "none");
            $json['file'] = 'mini_' . $json['file'];

            exit(json_encode($json));
        }

        if (isset($SYSTEM['get']['uploadWallDoc'])) {
            $file = $_FILES['file'];
            $ext = getExtension($file["name"]);
            if (strpos($ext, 'ph')!==false || strpos($ext, 'png') !== false) {
                exit($json);
            }
            $uid = $USER['id'];
            $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/docs/' . $uid . '/';
            if (!file_exists($path)) {
                mkdir($path);
            }
            $time = time();
            $ext = getExtension($file["name"]);
            if (strpos($ext, 'ph')!==false || strpos($ext, 'png') !== false) {
                exit;
            }
            $file['name'] = str_replace('.' . $ext, '', $file['name']);
            $json['file'] = translit($file['name']) . '.' . $ext;
            move_uploaded_file($file["tmp_name"], $path . '' . $json['file']);

            $docId = addDoc($json['file']);
            $json['doc'] = $docId;
            $json['image'] = 0;
            if (is_image($path . $json['file'])) {
                $json['image'] = 1;
            }

            exit(json_encode($json));
        }
    }
}

/********************************************/
/********************************************/
/********************************************/
$photos = getUserPhotos($group['group_id'] * -1, false, true);
$posts = getPosts($group['group_id'] * -1, 0, 5, true);
$c = count($posts);
upSeeInGroup($group['group_id']);
if ($group['type']) {
    $skills = getUserSkills($group['group_id'] * -1, true);
}
header_($group['title'], '', '', ''
    . '<script type="text/javascript" src="/js/vendor/jquery.ui.widget.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="/js/jquery.fileupload.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>
            <link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
            <link rel="stylesheet" href="/css/jquery.Jcrop.min.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="image_src" href="/uploads/g_avatars/' . $group['ava'] . '">
    <meta property="og:image" content="/uploads/g_avatars/' . $group['ava'] . '" />

', 'body-main');
$slide_img = '/images/slide.jpg';
if (file_exists('uploads/g_slide/' . $group['group_id'] . '.png')) {
    $slide_img = '/uploads/g_slide/' . $group['group_id'] . '.png';
}
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="main_slider">
            <div class="slider_toolbox<?php echo $class; ?>"><a href="#"><img src="/images/icon-toolbox-pencil.png"></a>
            </div>
            <div class="slider_info bg" style="background-image: url(<?php echo $slide_img; ?>);">></div>

            <div class="slider_info">
                <?php echo iconStatus($group['status']); ?>
                <div class="sinfo_img">
                    <div class="sinfo_img_toolbox<?php echo $class; ?>"><a href="#"><img
                                src="/images/icon-toolbox-photo.png"></a></div>
                    <div class="avaTariff" style="position: relative; display: inline-block;">
                        <img class="circled" src="<?php echo '/uploads/g_avatars/' . $group['ava']; ?>" id="avatar"/>
                    </div>
                </div>

                <div class="sinfo_name"><?php echo $group['title']; ?></div>

                <div class="sinfo_age">
                    <?php if ($group['type'] == 0) {
                        echo 'Сообщество';
                    } else {
                        echo 'Компания';
                    } ?>
                </div>

                <!--span>Место работы:</span> <a href="#">GM</a><br />
                <span>Место учебы:</span> <a href="#">МГОУ</a><br /-->
                <?php
                if ($group['area']) {
                    echo '<span>Сфера деятельности:</span> <a href="#">' . $group['area'] . '</a><br />';
                }
                if ($group['city']) {
                    echo '<span>Место нахождения:</span> <a href="#">' . $group['city'] . '</a><br />';
                }
                if ($group['country']) {
                    echo '<span>Страна:</span> <a href="#">' . $group['country'] . '</a><br />';
                }
                if ($group['site']) {
                    echo '<span>Сайт:</span> <a href="//' . $group['site'] . '" target="_blank">' . $group['site'] . '</a><br />';
                }
                #if ($group['members']) {
                #echo '<span>Сотрудников:</span> ' . $group['members'] . '<br />';
                #}
                if ($group['founding']) {
                    if ($group['type']) {
                        echo '<span>Дата основания:</span> ' . sym_date(date('d m Y', $group['founding']), ' ') . '<br />';
                        echo '<span>В GM с</span> ' . sym_date(date('d m Y', $group['created']), ' ') . '<br />';
                    } else {
                        echo '<span>В GM с</span> ' . sym_date(date('d m Y', $group['created']), ' ') . '<br />';
                    }
                }
                ?>
                <div class="sinfo_amount">
                    <span
                        class="sinfo_contacts"><?php echo $group['type'] == 0 ? declOfNum($group['members'], array(' подписчик', ' подписчика', ' подписчиков')) : declOfNum($group['members'], array(' отслеживает', ' отслеживают', ' отслеживают')); ?></span>
                    <span><?php echo declOfNum($group['see'], array(' просмотр', ' просмотра', ' просмотров')); ?></span>
                </div>
            </div>
            <?php
            echo '<div class="slideNx" style="background-image: url(' . $slide_img . ');"></div>';
            if (!$itsI && $iUser) {
                if ($iMember) {
                    echo '<div class="cont"><form method="post" action="/group?id=' . $group['group_id'] . '&join=0"><button id="sendMoney" class="rounded_3" style="background-position: 14px -43px;">Отписаться</button></form></div>';
                } else {
                    if ($group['type'] == 1) {
                        echo '<div class="cont"><form method="post" action="/group?id=' . $group['group_id'] . '&join=1"><button id="sendMoney" class="rounded_3" style="background-position: 14px -43px;">Отслеживать</button></form></div>';
                    } else {
                        echo '<div class="cont"><form method="post" action="/group?id=' . $group['group_id'] . '&join=1"><button id="sendMoney" class="rounded_3" style="background-position: 14px -43px;">Подписаться</button></form></div>';
                    }
                }
            }
            ?>


            <input id="fileupload" type="file" name="file" style="display: none;">
            <input id="fileuploadForWall" type="file" name="file" style="display: none;">
            <input id="fileuploadForDialog" type="file" name="file" style="display: none;">
        </div>

        <div class="left_block">
            <?php if ($photos[1]): ?>
                <div class="block_header">
                    <h2>Фотографии</h2>
        <span class="counter rounded_3"><a
                href="/albums?id=-<?php echo $group['group_id']; ?>"><?php echo $photos[0]; ?></a></span>

                    <div class="clear"></div>
                </div>

                <div class="block_content">
                    <ul class="photos_main" style="float: left; height: 113px">
                        <?php $max = 100;
                        foreach ($photos[1] as $photo) {
                            if ($max-- <= 0) {
                                break;
                            }
                            echo '<li>
                ' . ($itsI ? '<div class="pmain_toolbox"><a href="#" class="hideHomePhoto" data-photo-id="' . $photo['photo_id'] . '"><img src="/images/icon-toolbox-close.png"></a></div>' : '') . '
                <a data-photo-id="' . $photo['photo_id'] . '" href="/uploads/albums/' . $photo['album_id'] . '/' . $photo['photo_id'] . '.jpg" class="fancybox" rel="photos"><img class="rounded_3" src="/uploads/albums/' . $photo['album_id'] . '/thumb' . $photo['photo_id'] . '.jpg" /></a>
            </li>';
                        } ?>
                    </ul>
                    <div class="clear"></div>
                </div>
            <?php endif; ?>
            <?php if ($group['desc'] || $itsI): ?>
                <div class="block_header">
                    <h2>Описание</h2>
                    <?php if ($itsI) {
                        echo '<a class="toolbox" href="/group/set?id=' . $group['group_id'] . '">Редактировать</a>';
                    } ?>
                    <div class="clear"></div>
                </div>
                <div class="block_content">
                    <p><?php echo nl2br($group['desc']); ?></p>
                </div>
            <?php endif; ?>
            <?php if ($group['type']): ?>
                <div class="block_header">
                    <h2>Предоставляемые услуги</h2>
                    <?php if ($itsI) {
                        echo '<a class="toolbox" href="/skills?id=' . ($group['group_id'] * -1) . '">Редактировать</a>';
                    } ?>
                    <div class="clear"></div>
                </div>
                <div class="block_content">

                    <!--Skills -->

                    <table class="skills_table">
                        <?php foreach ($skills as $i => $skill) {
                            echo showSkill($skill['skill_id'], $skill, $i);
                        }
                        ?>
                    </table>
                    <p>
                        <?php
                        if (count($skills) > 5) {
                            echo 'Компания предоставляет еще ' . declOfNum(count($skills) - 5, array('услугу', 'услуги', 'услуг')) . ' <a href="#" id="showAllSkills" class="dotted">Показать</a>';
                        }
                        ?>
                    </p>

                </div>
            <?php endif; ?>
            <div class="wall_wrapper">
                <?php if ($USER['id'] && $iMember): ?>
                    <div class="wall_post_comment">
                        <div class="wall_post_profile">
                            <img class="width46" src="<?php echo '/uploads/avatars/' . $USER['ava']; ?>">
                        </div>
                        <div class="wall_post_content">
                            <form action="" method="post" id="wallPost">
                <textarea style="display: none;" class="rounded_3" name="text" rows="1"
                          placeholder="Что у Вас нового?"></textarea>

                                <div id="myEmojiField"></div>

                                <div class="wall_post_btn">
                                    <button class="btn rounded_3">Отправить</button>

                                    <?php if ($itsI) {
                                        echo '<label><input type="checkbox" value="1" name="fromGroup" /> От имени ' . ($group['type'] ? 'компании' : 'сообщества') . '</label><br/>';
                                    }
                                    ?>
                                    <div class="wall_post_btn_right">
                                        <a href="#" id="uploadWall" class="wall_icon wall_icon_photo normal"></a>
                                        <a href="#" id="uploadWallVideo" data-id="-<?php echo $group['group_id']; ?>" class="wall_icon wall_icon_video normal"></a>
                                        <a href="#" id="uploadWallDoc" class="wall_icon wall_icon_file normal"></a>
                                    </div>

                                    <div>
                                        <div class="clear"></div>
                                        <ul class="photos_main" id="uploadList">
                                        </ul>
                                    </div>
                                </div>
                                <input type="hidden" class="hide_data" name="hide_data"/>
                            </form>
                        </div>
                    </div>
                <?php
                endif;
                foreach ($posts as $post) {
                    //if (!$post['repost']) {
                    echo outputPost($post, $iMember, true, $itsI ? $group['type'] + 1 : false);
                    //}
                }
                ?>
            </div>
        </div>

        <div class="right_block">

            <?php
            echo show_membersInGroup($group, $members, false, $itsI, $group['type']);
            #echo show_main_contacts($_USER['id'], $cMyContacts, $myContacts, $priv_contact);
            #echo show_main_companies($_USER['id'], $priv_companies);
            #echo show_main_communities($_USER['id'], $priv_communities);
            echo show_main_album($group['group_id'] * -1, true, $itsI);
            echo show_main_videos($group['group_id'] * -1, true, $itsI);
            #echo show_main_audios($group['id'] * -1, true);
            echo show_ContactsInGroup($group, $members, false, $itsI, $group['type']);
            ?>

        </div>

        <div class="clear"></div>
        <div class="crop" id="divCrop">
            <h1>Выберите область</h1>
            <img src="" id="imgCrop"/>

            <div>
                <button id="cancelCrop">Отменить</button>
                <button id="appendCrop">Сохранить</button>
            </div>
        </div>
    </div>
    <!--main-->
    <script>
        //if ($(window).scrollTop() + $(window).height() >= $('.files').height()-$('.files img:last').height()/*$(document).height() - 700*/) {

        var getVar = null;
        var lockLoad = false;
        var coords;
        var avatar = false;
        var miniDialog = true;
        $(document).ready(function () {
            $(window).scroll(function () {
                var bo = $(window).scrollTop();
                if (bo >= $('.wall_wrapper').scrollTop() + $('.left_block').height() - 300) {
                    console.log('load_more_post');
                    if (!lockLoad) {
                        lockLoad = true;
                        var offset = $('div[data-wid]').size();
                        $.post('', 'wall_offset=' + offset, function (data) {
                            $('.wall_wrapper').append(data);
                            $('.wall_post[data-wid]').each(function () {
                                console.log($(this).find('.wall_text span').size());
                                if ($(this).find('.wall_text span').size() == 2) {
                                    clickable($(this).find('.wall_text span:last'));
                                } else {
                                    clickable($(this).find('.wall_text'));
                                }
                            });
                            $('.wall_post[data-wid] .wall_post_comments .wall_post_content').each(function () {
                                clickable($(this).find('.wall_post_message p'));
                            });
                            doInitCommentEmoji();
                            lockLoad = false;
                        });
                    }
                }
            });
            //////////////////////////////////
            $(document).ready(function () {
                $('.fancybox').fancybox({
                    'padding': [20, 20, 0, 20],
                    'beforeShow': function () {
                        $('.fancybox-skin').after('<div class="loading">Идет загрузка</div>');
                    },
                    'afterShow': function () {
                        $this = this.element;

                        $.post('/album/album-ajax', 'photo_id=' + $this.attr('data-photo-id'), function (data) {
                            $('.fancybox-skin').after(data);
                            $('.fancybox-skin').parent().find('.loading').remove();
                        });
                        return false;
                    }
                });
            });
            $('.sinfo_img_toolbox a').click(function () {
                getVar = 'upload=1&avatar=2&id=<?php echo $group['group_id']; ?>';
                $('#fileupload').fileupload({
                    url: '?' + getVar,
                    dataType: 'json',
                    done: function (e, data) {
                        avatar = true;
                        if (data.result.error === undefined) {
                            //curImage.attr('img-src', data.result.file);
                            //curImage.click();
                            //window.location.reload();
                            $('#divCrop').show();
                            $img = $('#imgCrop');
                            $img.attr('src', '/' + data.result.file);
                            $img.Jcrop({
                                minSize: [320, 320],
                                setSelect: [0, 0, 320, 320],
                                aspectRatio: 1 / 1,
                                onSelect: function (c) {
                                    // Get coordionates
                                    coords = c;
                                }
                            });
                            $('#divCrop').center(true, false);
                        } else {
                            alert(data.result.error);
                        }
                        console.log(data);
                    },
                    progressall: function (e, data) {

                    }
                }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
                $('#fileupload').click();
                return false;
            });
            $('#cancelCrop').click(function () {
                $img = $('#imgCrop');
                JcropAPI = $img.data('Jcrop');
                JcropAPI.destroy();
                $img.attr('style', '');
                $('#divCrop').hide();
                return false;
            });
            $('#appendCrop').click(function () {
                console.log(coords);
                var post = {
                    'file': $('#imgCrop').attr('src'),
                    'coords': coords
                };
                $.post('?upload=2&id=<?php echo $group['group_id']; ?>&iavatar=' + (avatar ? 1 : 0), post, function (data) {
                    console.log(data);
                    window.location.reload();
                });
                $('#cancelCrop').click();
                return false;
            });
            $('.slider_toolbox a').click(function () {
                getVar = 'upload=1&slide=2&id=<?php echo $group['group_id']; ?>';
                $('#fileupload').fileupload({
                    url: '?' + getVar,
                    dataType: 'json',
                    done: function (e, data) {
                        avatar = false;
                        if (data.result.error === undefined) {
                            //curImage.attr('img-src', data.result.file);
                            //curImage.click();
                            //window.location.reload();
                            $('#divCrop').show();
                            $img = $('#imgCrop');
                            $img.attr('src', '/' + data.result.file);
                            $img.Jcrop({
                                minSize: [918, 478],
                                setSelect: [0, 0, 918, 478],
                                aspectRatio: 918 / 478,
                                onSelect: function (c) {
                                    // Get coordionates
                                    coords = c;
                                }
                            });
                            $img.load(function () {
                                $('#divCrop').center(true, false);
                            });
                        } else {
                            alert(data.result.error);
                        }
                        console.log(data);
                    },
                    progressall: function (e, data) {

                    }
                }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
                $('#fileupload').click();
                return false;
            });

            $(document).on('click', '.mphotos a', function () {
                var file = $(this).attr('data-file');
                $(this).parent().remove();
                $('.mfooter form input[value="' + file + '"]').remove();
                if (!$('.mfooter form input').size()) {
                    $('.mphotos').hide();
                }
                return false;
            });
            /*********************************************/
            $(document).on('click', '#wallPost button', function () {
                $('#wallPost').find('textarea[name="text"]').val(kemoji1.getValue(KEmoji.HTML_VALUE));
                if (!$.trim($('#wallPost').find('textarea[name="text"]').val()) && !$('#uploadList li').size()) {
                    console.log('nini');
                    return false;
                }
                $.post('', $('#wallPost').serialize() + '&add_post=1', function (data) {
                    $('#wallPost input').val('');
                    $('#wallPost textarea').val('');
                    $('.wall_wrapper .wall_post_comment:first').after(data);
                    clickable($('.wall_post:first').find('.wall_text'));
                    if ($('.wall_post:first').find('div[data-video]').size()) {
                        preview_video($('.wall_post:first').find('div[data-video]'));
                    }
                    $('.wall_post:first .wallComment textarea').show();

                    $('.grid_wall_images img').load(function () {
                        normalizeWallPost();
                    });
                    $('#uploadList').html('');
                    $('.KEmoji_Input div[contenteditable]').html('');
                    console.log(data);
                });
                return false;
            });
            $(document).on('click', '.wallComment button', function () {
                $wall = $(this).parent().parent().parent();

                $kemoji = $wall.find('div[emoji-id]').attr('emoji-id');
                $kemoji = kemoji5[parseInt($kemoji) - 1];
                $wall.find('textarea[name="text"]').val($kemoji.getValue(KEmoji.HTML_VALUE));

                if (!$.trim($wall.find('form textarea[name="text"]').val())) {
                    return false;
                }
                $.post('', $wall.find('form').serialize() + '&add_comment=1', function (data) {
                    //$('.wallComment input').val('');
                    $('.wallComment textarea').val('');
                    $wall.find('.KEmoji_Input div[contenteditable]').html('');
                    //$('.wall_wrapper .wall_post_comment:first').after(data);
                    $('.uploadListComment').html('');
                    var wid = $wall.find('input:last').val();
                    console.log(wid, $wall);
                    $('.wall_post[data-wid="' + wid + '"] .wall_post_comments')[0].outerHTML = data;
                    clickable($('.wall_post[data-wid="' + wid + '"] .wall_post_comments .wall_post_content:first .wall_post_message p'));
                    console.log($(data).html(), wid);
                });
                return false;
            });
            function normalizeWallPost() {
                var min = 186;
                $('.wall_post .grid_wall_images img').each(function () {
                    if (min > $(this).height()) {
                        min = $(this).height();
                    }
                });
                $('.wall_post .grid_wall_images a').each(function () {
                    $(this).height(min)
                });
            }

            $('.grid_wall_images img').load(function () {
                normalizeWallPost();
            });

        });
        $(document).on('click', '#uploadWall', function () {
            if ($('#uploadList li').size() >= 5) {
                return false;
            }
            $('#fileuploadForWall').fileupload({
                url: '?uploadWall&uid=<?php echo $group['group_id']*-1; ?>',
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.error === undefined) {
                        //curImage.attr('img-src', data.result.file);
                        //curImage.click();
                        console.log(data.result);
                        $('#uploadList').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
                        + '<a href="#" onlick="return false;"><img class="rounded_3 width71" src="/uploads/wall/<?php echo $group['group_id']*-1; ?>/' + data.result.file + '" /></a>'
                        + '<input type="hidden" name="file[]" value="/uploads/wall/<?php echo $group['group_id']*-1; ?>/' + data.result.file + '" /></li>');
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);

                }
                ,
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

            $('#fileuploadForWall').click();

            return false;
        });
        $(document).on('click', '.uploadWallComment', function () {
            $wall = $(this).parent().parent();
            $('#fileuploadForWall').fileupload({
                url: '?uploadWall&uid=<?php echo $group['group_id']*-1; ?>',
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.error === undefined) {
                        //curImage.attr('img-src', data.result.file);
                        //curImage.click();
                        console.log(data.result);
                        $wall.find('.uploadListComment').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
                        + '<a href="#" onlick="return false;"><img class="rounded_3 width71" src="/uploads/wall/<?php echo $group['group_id']*-1; ?>/' + data.result.file + '" /></a>'
                        + '<input type="hidden" name="file[]" value="/uploads/wall/<?php echo $group['group_id']*-1; ?>/' + data.result.file + '" /></li>');
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);

                }
                ,
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

            $('#fileuploadForWall').click();

            return false;
        });

        /*$(document).on('click', '.wall_wrapper .wall_icon_counter,.wall_wrapper .wall_icon_counter_y', function () {
            console.log('like');
            if ($(this).hasClass('wall_icon_counter_y')) {
                $(this).addClass('wall_icon_counter');
                $(this).removeClass('wall_icon_counter_y');
            } else {
                $(this).addClass('wall_icon_counter_y');
                $(this).removeClass('wall_icon_counter');
            }
            if ($(this).hasClass('lcomment')) {
                var id = $(this).attr('data-cid');
                var wid = $(this).attr('data-wid');
                console.log(id);
                $.post("/", "add_like=1&id=" + id + '&comment=2', function (data, textStatus) {
                    listLike(id, undefined, true, wid);
                });
                return false;
            }
            var id = $(this).parent().parent().parent().parent().attr('data-wid');
            console.log(id);
            $.post("/", "add_like=1&id=" + id, function (data, textStatus) {
                listLike(id, undefined, false);
            });

            return false;
        });*/
        $(document).on('click', '.wall_wrapper .wall_icon_repost_y', function () {
            alert('Запись уже опубликована');
            return false;
        });
        $(document).on('click', '.wall_wrapper .wall_icon_repost,.wall_wrapper .wall_icon_repost', function () {
            console.log('repost');
            if ($(this).hasClass('wall_icon_repost_y')) {
                $(this).addClass('wall_icon_repost');
                $(this).removeClass('wall_icon_repost_y');
            } else {
                $(this).addClass('wall_icon_repost_y');
                $(this).removeClass('wall_icon_repost');
            }
            $this = $(this);
            var id = $(this).parent().parent().parent().parent().attr('data-wid');
            console.log(id);
            $.post("/", "repost=1&id=" + id, function (data, textStatus) {
                if (data == '1') {
                    $this.text(parseInt($this.text()) + 1);
                    alert('Запись была опубликована на Вашу стену');
                } else {

                }
                console.log(data);
                //listLike(id, undefined, false);
            });

            return false;
        });
        $(document).on('click', '.delWall', function () {
            $post = $(this).parent().parent().parent().parent();
            $.post("", "del_post=1&id=" + $(this).attr('data-post_id'), function (data, textStatus) {
                $post.remove();
            });
            return false;
        });
/*
        function listLike(wall_id, likes, notAva) {
            if (likes === undefined && !notAva) {
                $.post('?like=get&wid=' + wall_id, '', function (data) {
                    console.log(data);
                    $('.wall_post[data-wid="' + wall_id + '"] .wall_icon_counter_y, .wall_post[data-wid="' + wall_id + '"] .wall_icon_counter').text(Object.keys(data).length);
                    var list = '';
                    if (!notAva) {
                        $.each(data, function (i, item) {
                            if (i > 3) {
                                return;
                            }
                            list = list + '<li><a href="/id' + item[0] + '"><img class="width20" src="/uploads/avatars/' + item[1] + '" /></a></li>';
                        });
                    }
                    $('.wall_post[data-wid="' + wall_id + '"] .wall_icon_counter_y, .wall_post[data-wid="' + wall_id + '"] .wall_icon_counter').next().html(list);
                }, 'json');
            } else {
                $.post('?like=get&cid=' + wall_id, '', function (data) {
                    console.log(data);
                    $('.wall_post_comments .wall_post[data-cid="' + wall_id + '"] .wall_icon_counter_y, .wall_post_comments .wall_post[data-cid="' + wall_id + '"] .wall_icon_counter').text(Object.keys(data).length);
                }, 'json');
            }
        }*/
    </script>
<?php
footer();
