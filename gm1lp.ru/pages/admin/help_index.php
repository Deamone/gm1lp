<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>GM Админка</title>
        <!-- Bootstrap core CSS -->
        <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="/admin/js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="container">
            <!-- Static navbar -->
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Меню</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/admin/index">GM1LP</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="/admin/index">Админка</a></li>
                            <li class="active"><a href="/admin/help_index">Страницы справочника</a></li>
                            <!--li><a href="#">Contact</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li-->
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/">Вернуться на сайт</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </nav>
            <table class="table">
                <tr>
                    <th width="90%">Страница</th>
                    <th>Действия</th>
                </tr>
                <?php
                $pages = getPages();
                foreach ($pages as $page) {
                    echo '
        <tr>
            <td>' . $page['title'] . '</td>
            <td><a href="/admin/help_page_edit?id=' . $page['page_id'] . '"><span class="glyphicon glyphicon-pencil"></span></a> <a href="/admin/help_page_remove?id=' . $page['page_id'] . '"><span class="glyphicon glyphicon-remove"></span></a></td>
        </tr>';
                }
                ?>
            </table>
            <form action="/admin/help_page_create" class="pull-right">
                <button class="btn btn-primary">Создать новую</button>
            </form>
        </div> <!-- /container -->
        <script>
            $(document).ready(function () {
                $('footer').remove();
            });
        </script>
    </body>
</html>
