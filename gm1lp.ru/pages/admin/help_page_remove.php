<?php

if (!isset($_GET['id'])) {
    redirect('/admin/help_index');
}
$page = getPageById($_GET['id']);
if (!$page) {
    redirect('/admin/help_index');
}
removePage($page['page_id']);
redirect('/admin/help_index');
