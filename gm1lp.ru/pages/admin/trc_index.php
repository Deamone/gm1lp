<?php

$cities = array(
    74297 => 'Чебоксары',
    71711 => 'Москва'
);


$trc_id = isset($_GET['trc']) ? (int)$_GET['trc'] : 0;
$curCity = isset($_GET['city']) ? (int)$_GET['city'] : 0;
$remove = isset($_GET['remove']) ? (int)$_GET['remove'] : 0;
$title = '';
$city = 0;
$address = '';
$worktime = '';
$metro = '';

$alert = '';
if ($remove) {
    removeTrc($trc_id);
    redirect('/admin/trc_index');
}
if (count($_POST)) {
    $title = $_POST['title'];
    $city = $_POST['city'];
    $address = $_POST['address'];
    $worktime = $_POST['worktime'];
    $metro = $_POST['metro'];

    if (!$title || !$city || !$address) {
        $alert = 'Нужно заполнить все поля';
    } else {
        if ($trc_id) {
            updateTrc($trc_id, $title, $city, $address, $worktime, $metro);
        } else {
            addTrc($title, $city, $address, $worktime, $metro);
        }
        redirect('/admin/trc_index');

    }
}
if ($trc_id) {
    $trc = getTrcById($trc_id);
    $title = $trc['title'];
    $city = $trc['city'];
    $address = $trc['address'];
    $worktime = $trc['worktime'];
    $metro = $trc['metro'];
}

function getTrc($city = 0)
{
    $city = (int)$city;
    if (!$city) {
        $q = query("SELECT * FROM `trc_list` WHERE 1 ORDER BY `title` ASC");
    } else {
        $q = query("SELECT * FROM `trc_list` WHERE `city`='$city' ORDER BY `title` ASC");
    }

    return $q['rows'];
}

function getTrcById($trc_id)
{
    $trc_id = (int)$trc_id;
    $q = query("SELECT * FROM `trc_list` WHERE `trc_id`='$trc_id';");


    return $q['row'];
}

function addTrc($title, $city, $address, $worktime, $metro)
{
    $title = query_escape($title);
    $city = (int)$city;
    $address = query_escape($address);
    $worktime = query_escape($worktime);
    $metro = query_escape($metro);

    query("INSERT INTO `trc_list` (`title`,`city`,`address`,`worktime`,`metro`) VALUES ('$title','$city','$address','$worktime','$metro')");
}

function updateTrc($trc_id, $title, $city, $address, $worktime, $metro)
{
    $trc_id = (int)$trc_id;
    $title = query_escape($title);
    $city = (int)$city;
    $address = query_escape($address);
    $worktime = query_escape($worktime);
    $metro = query_escape($metro);

    query("UPDATE `trc_list` SET `title`='$title',`city`='$city',`address`='$address',`worktime`='$worktime',`metro`='$metro' WHERE `trc_id`='$trc_id';");

}

function removeTrc($trc_id)
{
    $trc_id = (int)$trc_id;

    query("DELETE FROM `trc_list` WHERE `trc_id`='$trc_id';");
}

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>GM Админка</title>
    <!-- Bootstrap core CSS -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/tinymce/tinymce.min.js"></script>
</head>

<body>
<div class="container">
    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Меню</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/admin/index">GM1LP</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/admin/index">Админка</a></li>
                    <li class="active"><a href="/admin/trc_index">Список ТРЦ</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/">Вернуться на сайт</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
    </nav>
    <?php
    if ($alert) {
        echo '<div class="alert alert-danger" role="alert">' . $alert . '</div>';
    }
    ?>
    <form action="" method="post">
        <fieldset>
            <label for="title">Название ТРЦ</label>

            <div class="form-group">
                <input class="form-control" id="title" name="title" value="<?php echo $title; ?>" placeholder="Название">
            </div>
            <label for="city">Город</label>

            <div class="form-group">
                <select name="city" id="city" class="form-control">
                    <?php
                    foreach ($cities as $id => $city_name) {
                        echo '<option value="' . $id . '"' . ($city == $id ? ' selected' : '') . '>' . $city_name . '</option>';
                    }
                    ?>
                </select>
            </div>
            <label for="address">Адрес</label>

            <div class="form-group">
                <input class="form-control" id="address" name="address" value="<?php echo $address; ?>" placeholder="Адрес">
            </div>
            <label for="worktime">Время работы</label>

            <div class="form-group">
                <textarea class="form-control" id="worktime" name="worktime" placeholder="Время работы"><?php echo $worktime; ?></textarea>
            </div>
            <label for="metro">Метро</label>

            <div class="form-group">
                <input class="form-control" id="metro" name="metro" value="<?php echo $metro; ?>" placeholder="Метро">
            </div>
        </fieldset>
        <button class="btn btn-primary">Сохранить</button>
    </form>
    <table class="table">
        <thead>
        <tr>
            <th>Название</th>
            <th>Город</th>
            <th>Адрес</th>
            <th>Время работы</th>
            <th>Метро</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?php $trc_list = getTrc($curCity);
        foreach ($trc_list as $trc) {
            echo '<tr>
        <td>' . $trc['title'] . '</td>
        <td><a href="?city=' . $trc['city'] . '">' . $cities[$trc['city']] . '</a></td>
        <td>' . $trc['address'] . '</td>
        <td>' . $trc['worktime'] . '</td>
        <td>' . $trc['metro'] . '</td>
        <td><a href="?trc=' . $trc['trc_id'] . '"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;<a href="?trc=' . $trc['trc_id'] . '&remove" onclick="if(!confirm(\'Удалить?\')){return false;}"><i class="glyphicon glyphicon-remove"></i></a></td>
</tr>';
        } ?>
        </tbody>
    </table>
</div>
<!-- /container -->
<script>
    $(document).ready(function () {
        $('footer').remove();
    });
</script>
</body>
</html>