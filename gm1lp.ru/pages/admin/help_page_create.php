<?php
$alias = '';
$title = '';
$content = '';
$alert = '';
if (count($_POST)) {
    $alias = $_POST['alias'];
    $title = $_POST['title'];
    $content = $_POST['content'];

    if (!$alias || !$title || !$content) {
        $alert = 'Нужно заполнить все поля';
    } else {
        $page = getPageByAlias($alias);
        if ($page) {
            $alert = 'Данный алиас уже используется';
        } else {
            addPage($alias, $title, $content);
            redirect('/admin/help_index');
        }
    }
}
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>GM Админка</title>
        <!-- Bootstrap core CSS -->
        <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="/admin/js/bootstrap.min.js"></script>
        <script src="/admin/js/tinymce/tinymce.min.js"></script>
    </head>

    <body>
        <div class="container">
            <!-- Static navbar -->
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Меню</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/admin/index">GM1LP</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="/admin/index">Админка</a></li>
                            <li class="active"><a href="/admin/help_index">Страницы справочника</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/">Вернуться на сайт</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </nav>
            <?php
            if ($alert) {
                echo '<div class="alert alert-danger" role="alert">' . $alert . '</div>';
            }
            ?>
            <form action="/admin/help_page_create" method="post">
                <fieldset>
                    <label for="alias">Алиас</label>
                    <div class="form-group input-group">
                        <span class="input-group-addon">/help/</span>
                        <input class="form-control" id="alias" name="alias" value="<?php echo $alias; ?>" placeholder="Алиас">
                    </div>
                    <div class="form-group">
                        <label for="title">Заголовок</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Заголовок страницы" value="<?php echo $title; ?>">
                    </div>
                    <div class="form-group">
                        <label for="content">Контент страницы</label>
                        <textarea name="content" id="content"><?php echo $content; ?></textarea>
                    </div>
                </fieldset>
                <button class="btn btn-primary">Сохранить</button>
            </form>
        </div> <!-- /container -->
        <script>
            $(document).ready(function () {
                $('footer').remove();

                tinymce.init({
                    selector: "textarea#content",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor jbimages"
                    ],
                    content_css: "/css/style_help.css",
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | print preview media fullpage | forecolor backcolor emoticons",
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ],
                    cleanup_on_startup: false,
                    trim_span_elements: false,
                    verify_html: false,
                    cleanup: false,
                    convert_urls: false,
                    apply_source_formatting: false,
                    remove_linebreaks: false,
                    force_p_newlines: false,
                    force_br_newlines: false

                });
            });
        </script>
    </body>
</html>