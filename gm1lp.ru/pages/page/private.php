<?php
$json['eval'] = array();
$i = true;
$_USER = $USER;
$error = '';
if (isset($_GET['setEmail'])) {
    $code = $_GET['code'];
    $p = base64_decode($_GET['p']);

    if ($code == md5($_USER['password'] . $_USER['id'] . $_USER['login'] . $p)) {
        updateUserById($_USER['id'], array('login'), array($p));
        exit('<script>alert("Адрес успешно изменен!");window.location.href="/private";</script>');
    }

}
if (isAjax()) {
    $post = $_POST;
    $keys = array();
    $values = array();
    if (!empty($post['oldPassword'])) {
        if (enc_passw($post['oldPassword']) != $_USER['password']) {
            exit('alert("Старый пароль введен не верно");');
        } elseif ($post['password'] != $post['rePassword'] || empty($post['password'])) {
            exit('alert("Пароли не совпадают");');
        } else {
            $keys[] = 'password';
            $values[] = enc_passw($post['password']);
        }
    }

    if (isset($post['private']) && is_array($post['private'])) {
        $private = 0;
        foreach ($post['private'] as $private_) {
            $private = $private | $private_;
        }
        $keys[] = 'private';
        $values[] = $private;
    }

    if (isset($post['private2']) && is_array($post['private2'])) {
        $private = 0;
        foreach ($post['private2'] as $private_) {
            $private = $private | $private_;
        }
        $keys[] = 'private2';
        $values[] = $private;
    }

    if (isset($post['private3']) && is_array($post['private3'])) {
        $private = 0;
        foreach ($post['private3'] as $private_) {
            $private = $private | $private_;
        }
        $keys[] = 'private3';
        $values[] = $private;
    }

    if (!empty($_POST['email'])) {
        $user = getUserByEmail($_POST['email']);
        if ($user['id'] != $_USER['id']) {
            exit('alert("Данный Email пренадлежит другому пользователю");');
        } elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $_POST['email'])) {
            exit('alert("Проверьте E-mail");');
        } else {
            $email = 1;
            $url = 'http://' . $_SERVER['HTTP_HOST'] . '/private?setEmail&code=' . md5($_USER['password'] . $_USER['id'] . $_USER['login'] . $_POST['email']) . '&p=' . base64_encode($_POST['email']);
            mail($_USER['email'], 'Подтверждение изменения E-mail адреса', 'Перейдите по ссылке что бы изменить E-mail: ' . $url);
            if (!$keys) {
                exit('alert("Для смены адреса, пожалуйста перейдите по ссылке отправленной на вашу почту");');
            }
        }
    }

    if (!$keys) {
        $json['eval'][] = 'alert("Нет информации для сохранения");';
    } else {
        updateUserById($_USER['id'], $keys, $values);
    }
        $json['eval'][] = '$("#alertHeader").text("Успешно!");$("#alertMessage").text("Данные успешно сохранены!' . (isset($email) ? "<br/>Для смены адреса, пожалуйста перейдите по ссылке отправленной на вашу почту" : '') . '"); showModal(".alertMessage");';

    exit(implode(' ', $json['eval']));
#    exit('alert("Данные успешно сохранены' . (isset($email) ? "\nДля смены адреса, пожалуйста перейдите по ссылке отправленной на вашу почту" : '') . '");');
}
header_('Конфиденциальность', '', '', ''
    . '');
$private = $_USER['private'];
$private2 = $_USER['private2'];
$private3 = $_USER['private3'];
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <form id="formPrivate">
                <div class="div_left">
                    <h1 class="prof_h1">Конфиденциальность</h1>

                    <div class="prof_blok_left">
                        <h2>Изменить пароль</h2>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Введите старый пароль</p>
                            <input type="text" name="oldPassword"/>
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Изменение Email</p>
                            <input type="text" name="email" placeholder="Введите новый Email"/>
                        </div>
                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Введите новый пароль</p>
                            <input type="text" name="password"/>
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Новый пароль еще раз</p>
                            <input type="text" name="rePassword"/>
                        </div>
                    </div>
                    <div class="prof_blok_left prof_blok_puktW50pr" style="padding-bottom: 30px">
                        <h2>Конфиденциальность</h2>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит мое семейное положение</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private[]">
                                        <option value="' . SP_ALL . '" ' . ($private & SP_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . SP_C . '" ' . ($private & SP_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . SP_I . '" ' . ($private & SP_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит мой номер мобильного телефона</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private[]">
                                        <option value="' . PHONE_ALL . '" ' . ($private & PHONE_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . PHONE_C . '" ' . ($private & PHONE_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . PHONE_I . '" ' . ($private & PHONE_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит мое место проживания</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private[]">
                                        <option value="' . LOCATION_ALL . '" ' . ($private & LOCATION_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . LOCATION_C . '" ' . ($private & LOCATION_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . LOCATION_I . '" ' . ($private & LOCATION_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит мой возраст</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private3[]">
                                        <option value="' . AGE_ALL . '" ' . ($private3 & AGE_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . AGE_C . '" ' . ($private3 & AGE_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . AGE_I . '" ' . ($private3 & AGE_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит мое место работы и должность</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private[]">
                                        <option value="' . WORK_ALL . '" ' . ($private & WORK_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . WORK_C . '" ' . ($private & WORK_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . WORK_I . '" ' . ($private & WORK_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто может писать мне личные сообщения</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private[]">
                                        <option value="' . MESSAGE_ALL . '" ' . ($private & MESSAGE_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . MESSAGE_C . '" ' . ($private & MESSAGE_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . MESSAGE_I . '" ' . ($private & MESSAGE_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит список моих контактов</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private2[]">
                                        <option value="' . CONTACT_ALL . '" ' . ($private2 & CONTACT_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . CONTACT_C . '" ' . ($private2 & CONTACT_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . CONTACT_I . '" ' . ($private2 & CONTACT_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит список моих компаний и сообществ</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private2[]">
                                        <option value="' . COMPANY_ALL . '" ' . ($private2 & COMPANY_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . COMPANY_C . '" ' . ($private2 & COMPANY_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . COMPANY_I . '" ' . ($private2 & COMPANY_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит чужие записи на моей стене</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private2[]">
                                        <option value="' . EWALL_ALL . '" ' . ($private2 & EWALL_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . EWALL_C . '" ' . ($private2 & EWALL_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . EWALL_I . '" ' . ($private2 & EWALL_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто может оставлять записи на моей стене</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private2[]">
                                        <option value="' . WALL_ALL . '" ' . ($private2 & WALL_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . WALL_C . '" ' . ($private2 & WALL_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . WALL_I . '" ' . ($private2 & WALL_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто может комментировать записи на моей стене</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private2[]">
                                        <option value="' . WALLC_ALL . '" ' . ($private2 & WALLC_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . WALLC_C . '" ' . ($private2 & WALLC_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . WALLC_I . '" ' . ($private2 & WALLC_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит комментарии к записям на моей стене</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private3[]">
                                        <option value="' . WALLSC_ALL . '" ' . ($private3 & WALLSC_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . WALLSC_C . '" ' . ($private3 & WALLSC_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . WALLSC_I . '" ' . ($private3 & WALLSC_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит мои фотоальбомы</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private3[]">
                                        <option value="' . PHOTO_ALL . '" ' . ($private3 & PHOTO_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . PHOTO_C . '" ' . ($private3 & PHOTO_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . PHOTO_I . '" ' . ($private3 & PHOTO_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Кто видит список моих видеозаписей</p>

                            <div class="styled-select styled-select_4">
                                <?php
                                echo '<select name="private3[]">
                                        <option value="' . VIDEO_ALL . '" ' . ($private3 & VIDEO_ALL ? 'selected' : '') . '>Все пользователи</option>
                                        <option value="' . VIDEO_C . '" ' . ($private3 & VIDEO_C ? 'selected' : '') . '>Только мои контакты</option>
                                        <option value="' . VIDEO_I . '" ' . ($private3 & VIDEO_I ? 'selected' : '') . '>Никто</option>
                                    </select>';
                                ?>
                            </div>
                        </div>

                    </div>

                    <a class="prof_butt nxPrivate" href="#">Сохранить</a>

                </div>
            </form>
        </div>

        <div class="right_block">
            <!-- Contacts -->
            <div class="r_bl_menu">
                <ul>
                    <li><a href="/settings">Профиль</a></li>
                    <li><a href="/skills">Навыки</a></li>
                    <li><a href="/private" class="akt">Конфиденциальность</a></li>
                    <li><a href="/secure">Безопасность</a></li>
                    <li><a href="/balance">Баланс</a></li>
                </ul>
            </div>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('#formPrivate').submit(function () {
                $.post('', $(this).serialize(), function (data) {
                    eval(data);
                });
                return false;
            });
            $('.nxPrivate').click(function () {
                $('#formPrivate').submit();
                return false;
            });
        });
        startCheckAuth();
    </script>
<?php
footer();

