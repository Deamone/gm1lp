<?php
/**
 * =====================================================
 * created 21.01.2015 NotusX
 * -----------------------------------------------------
 * http://notusx.ru/
 * -----------------------------------------------------
 * Copyright (c) 2014,2015 NotusX
 * =====================================================
 * Данный код защищен авторскими правами
 * =====================================================
 * @author NotusX
 * -----------------------------------------------------
 * Файл: contacts.php
 * -----------------------------------------------------
 * Назначение: Не задано
 * =====================================================
 */
if (isAjax()) {
    if (isset($_POST['id']) && $_GET['act'] == 'add') {

        $myContacts = getContacts($USER['id']);
        if (!isset($myContacts[$_POST['id']])) {
            $r = addContact($_POST['id']);
        }
        exit($r . '');
    }
    if (isset($_POST['id']) && $_GET['act'] == 'remove') {

        //$myContacts = getContacts($USER['id']);
        $r = removeContact($_POST['id']);
        exit($r . '');
    }
}
if (!$USER) {
    //redirect('/page/login');
}
$_USER = $USER;
$itsI = true;
$class = ' noti';
$_ = false;
$id = isset($_GET['_route_']) ? str_replace('contacts', '', query_escape($_GET['_route_'])) : '';

if ($id) {
    $_ = getUserByUrl($id);
} elseif (isset($_GET['id'])) {
    $_ = getUserById((int)$_GET['id']);
} elseif (!isAjax() && !$USER) {
    redirect($USER['url']);
}
if ($_) {
    $_USER = $_;
    if ($_['id'] != $USER['id']) {
        $itsI = false;
    }
}

header_($itsI ? 'Мои контакты' : 'Контакты пользователя ' . $_USER['name'], '', '', ''
    . '
', 'body-search body-contacts');

$myVideos = getVideos($USER['id']);
$count_videos = count($myVideos);

$myContacts = getContacts($USER['id']);
$cMyContacts = getCountContacts($USER['id']);
if (!$itsI) {
    shuffle($myContacts);
}

$contacts = getContacts($_USER['id']);
$albums = getUserAlbums($USER['id']);
$count_albums = count($albums);
$last_album_photo = '/images/no_scrn_mov.jpg';
if ($albums && isset($albums[0]['last_photo'])) {
    $last_album_photo = '/uploads/albums/' . $albums[0]['album_id'] . '/preview' . $albums[0]['last_photo']['photo_id'] . '.jpg';
}
$citys = '';
$citys_ = array();
$countrys = '';
$countrys_ = array();
foreach ($contacts as $k => $contact) {
    if ($contact[1] != 1) {
        continue;
    }
    $contacts[$k]['user'] = $user = getUserById($contact[0]);
    $country = getCountry($user['country']);
    $city = getCity($user['city']);
    $contacts[$k]['city'] = array($user['city'], $city['city']);
    $contacts[$k]['country'] = array($user['country'], $country['country']);

    if ($city['city'] && !isset($citys_[$city['city']])) {
        $citys .= '<option value="' . $user['city'] . '">' . $city['city'] . '</option>';
        $citys_[$city['city']] = 1;
    }
    if ($country['country'] && !isset($countrys_[$country['country']])) {
        $countrys .= '<option value="' . $user['country'] . '">' . $country['country'] . '</option>';
        $countrys_[$country['country']] = 1;
    }
}
#print_r($contacts);
if (!$citys) {
    $citys = '<option value="0" selected>Не указано</option>';
}
if (!$countrys) {
    $countrys = '<option value="0" selected>Не указано</option>';
}


$myVideos = getVideos($USER['id']);
$count_videos = count($myVideos);

$myContacts_ = getContacts($USER['id']);
$cMyContacts_ = getCountContacts($USER['id']);

# Right
$priv_photo = isPrivate($USER['id'], $USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts_);
$priv_video = isPrivate($USER['id'], $USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts_);
$priv_contact = isPrivate($USER['id'], $USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts_);
$priv_companies = $priv_communities = isPrivate($USER['id'], $USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts_);
$priv_audios = true;
# .Right
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <img src="/images/arrow.png"> <a href="/">вернуться назад</a>
            </div>
            <div class="block_search">
                <form action="search">
                    <div class="block_field">
                        Страна<br/>

                        <div class="styled-select">
                            <select name="country" id="country" style="max-width: 175px;">
                                <option value="0" selected="selected">Все страны</option>
                                <?php echo $countrys; ?>
                            </select>
                        </div>
                    </div>
                    <div class="block_field">
                        Город<br/>

                        <div class="styled-select">
                            <select name="city" id="city" style="max-width: 175px;">
                                <option value="0" selected="selected">Все города</option>
                                <?php echo $citys; ?>
                            </select>
                        </div>
                    </div>
                    <div class="block_field">
                        Пол<br/>

                        <div class="styled-select">
                            <select name="gender" id="gender">
                                <option value="male">Мужской</option>
                                <option value="female">Женский</option>
                            </select>
                        </div>
                    </div>
                    <div class="block_field">
                        Год рождения<br/>

                        <div class="styled-select">
                            <select name="year1" id="year1">
                                <?php
                                for ($i = 1967; $i <= 2014; $i++) {
                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <span class="yline">&HorizontalLine;</span>

                        <div class="styled-select">
                            <select name="year2" id="year2">
                                <?php
                                for ($i = 1968; $i <= 2014; $i++) {
                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <input name="query" type="text" class="search_left" id="searchy" autocomplete="off" value=""
                           placeholder="Фамилия, Имя"/>
                </form>
            </div>
            <div class="block_header" style="display: block">
                <h2>Контакты</h2>
                <div style="float: right;padding-top: 17px;padding-right: 10px;"><a href="/invites" target="_blank">Список приглашенных мною</a></div>
                <div class="clear"></div>
            </div>

            <div class="block_content">
                <?php
                reset($contacts);
                $time = time();
                foreach ($contacts as $contact) {
                    if ($contact[1] != 1) {
                        continue;
                    }
                    $online = '<img class="online_ico" src="/images/offline_png.png" />';
                    if (($time - $contact['user']['online']) < 15 * 60) {
                        $online = '<img class="online_ico" src="/images/online_png.png" />';
                    }
                    $user = $contact['user'];
                    $y = 0;
                    if ($user['date'] && is_numeric($user['date'])) {
                        $y = date('Y', $user['date']);
                    }
                    echo '<div class="contact" data-year="' . $y . '" data-name="' . $user['name'] . ' ' . $user['fam'] . '" data-city="' . $contact['city'][0] . '" data-country="' . $contact['country'][0] . '" data-gender="' . $user['gender'] . '">
                <div class="left">
                    <a href="/?id=' . $user['id'] . '"><img class="rounded_3" src="' . '/uploads/avatars/' . $user['ava'] . '" /></a>
                </div>
                <div class="right">
                    <a href="/?id=' . $user['id'] . '">' . $user['name'] . ' ' . $user['fam'] . '</a> ' . $online . '
                    <br/>
                    <form action="/message?id=' . $user['id'] . '" method="post"><button class="btn1">Написать сообщение</button></form><form method="post" action="/contacts?id=' . $user['id'] . '"><button class="btn1">Посмотреть контакты</button></form><button class="btn1 btn1w">Перевести средства</button>

                    ' . ($itsI ? '<a href="#" class="icontact" data-status="1" data-id="' . $user['id'] . '">Убрать из контактов</a>' : '') . '
                </div>
            </div>';
                }
                ?>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($USER['id'], $cMyContacts_, $myContacts, $priv_contact);
            echo show_main_companies($USER['id'], $priv_companies);
            echo show_main_communities($USER['id'], $priv_communities);
            echo show_main_album($USER['id'], $priv_photo);
            echo show_main_videos($USER['id'], $priv_video);
            echo show_main_audios($USER['id'], $priv_audios);
            ?>
        </div>

        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('#country').change(function () {
                var $val = $(this).val().toLowerCase();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country').toLowerCase();
                    var city = $(this).attr('data-city').toLowerCase();
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year');

                    if ($val == '0' && (city == $('#city').val() || $('#city').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if ($val != country) {
                            $(this).hide();
                        }
                        else if ((city == $('#city').val() || $('#city').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            $('#city').change(function () {
                var $val = $(this).val().toLowerCase();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country').toLowerCase();
                    var city = $(this).attr('data-city').toLowerCase();
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year');

                    if ($val == '0' && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if ($val != city) {
                            /*console.log(city, $('#city').val());
                             console.log(country, $('#country').val());
                             console.log(gender, $('#gender').val());*/
                            $(this).hide();
                        }
                        else if ((country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            $('#gender').change(function () {
                var $val = $(this).val().toLowerCase();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country').toLowerCase();
                    var city = $(this).attr('data-city').toLowerCase();
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year').toLowerCase();

                    if ($val == '0' && (city == $('#city').val() || $('#city').val() == 0) && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if ($val != gender) {
                            /*console.log(city, $('#city').val());
                             console.log(country, $('#country').val());
                             console.log(gender, $('#gender').val());*/
                            $(this).hide();
                        }
                        else if ((city == $('#city').val() || $('#city').val() == 0) && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            $('#year1,#year2').change(function () {
                var $val = $('#year1').val();
                var $val2 = $('#year2').val();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country');
                    var city = $(this).attr('data-city');
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year').toLowerCase();
                    console.log(year, $val, $val2);
                    if (year > $val && year < $val2 && (city == $('#city').val() || $('#city').val() == 0) && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if (year < $val || year > $val2) {
                            /*console.log(city, $('#city').val());
                             console.log(country, $('#country').val());
                             console.log(gender, $('#gender').val());*/
                            $(this).hide();
                        }
                        else if (year > $val && year < $val2 && (city == $('#city').val() || $('#city').val() == 0) && (country == $('#country').val() || $('#country').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            $('#searchy').keyup(function () {
                var $val = $(this).val().toLowerCase();
                $.each($('.contact'), function () {
                    var country = $(this).attr('data-country').toLowerCase();
                    var city = $(this).attr('data-city').toLowerCase();
                    var gender = $(this).attr('data-gender');
                    var name = $(this).attr('data-name').toLowerCase();
                    var year = $(this).attr('data-year');

                    if ($val == '0' && (country == $('#country').val() || $('#country').val() == 0) && (city == $('#city').val() || $('#city').val() == 0) && gender == $('#gender').val()) {
                        $(this).show();
                    } else {
                        if (name.indexOf($val) == '-1') {
                            $(this).hide();
                        }
                        else if ((country == $('#country').val() || $('#country').val() == 0) && (city == $('#city').val() || $('#city').val() == 0) && gender == $('#gender').val()) {
                            $(this).show();
                        }
                    }
                });
            });
            //searchy
        });
    </script>
<?php
footer();
