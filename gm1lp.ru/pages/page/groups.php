<?php
/**
 * =====================================================
 * created 21.01.2015 NotusX
 * -----------------------------------------------------
 * http://notusx.ru/
 * -----------------------------------------------------
 * Copyright (c) 2014,2015 NotusX
 * =====================================================
 * Данный код защищен авторскими правами
 * =====================================================
 * @author NotusX
 * -----------------------------------------------------
 * Файл: contacts.php
 * -----------------------------------------------------
 * Назначение: Не задано
 * =====================================================
 */

if (!$USER) {
    redirect('/page/login');
}
$itsI = false;
if (isset($_GET['id'])) {
    $_ = getUserById($_GET['id']);
    $_USER = $_;
} else {
    $_USER = $USER;
}
if ($_USER['id'] == $USER['id']) {
    $itsI = true;
}
$types = array('Сообщества', 'Компании');
$type = 0;
if (isset($_GET['type'])) {
    if (isset($types[$_GET['type']])) {
        $type = $_GET['type'];
    }
}

header_($itsI ? 'Мои ' . $types[$type] : $types[$type] . ' пользователя ' . $_USER['name'], '', '', ''
    . '
', 'body-search body-contacts');

$myContacts = getContacts($USER['id']);
$cMyContacts = getCountContacts($USER['id']);

# Right
$priv_photo = isPrivate($USER['id'], $USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($USER['id'], $USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($USER['id'], $USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($USER['id'], $USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <img src="/images/arrow.png"> <a href="/">вернуться назад</a>
            </div>
            <div class="block_search">
                <form action="/group/create">
                    <input type="text" class="search_left" id="searchy" autocomplete="off" value=""
                           placeholder="Поиск" style="width: 625px"/>
                    <button class="btn rounded_3">Создать</button>
                </form>
            </div>
            <div class="block_header" style="display: block">
                <h2><?php echo $types[$type]; ?></h2>

                <div class="clear"></div>
            </div>

            <div class="block_content">
                <?php
                $groups = getUserGroups($_USER['id'], $type);
                $time = time();
                foreach ($groups as $group) {
                    echo '<div class="contact" data-name="' . $group['title'] . '">
                <div class="left">
                    <a href="/' . $group['url'] . '"><img class="rounded_3" src="' . '/uploads/g_avatars/' . $group['ava'] . '" /></a>
                </div>
                <div class="right groupsList">
                    ' . ($itsI && $group['owner_id']!=$USER['id'] ? '<a href="/group?id=' . $group['group_id'] . '&join=0" class="groupUnJoin">Покинуть</a>' : '') . '
                    <a href="/' . $group['url'] . '" class="groupTitle">' . $group['title'] . '</a><br/>
                    <span class="groupArea">' . $group['area'] . '</span><br/>
                    <span class="groupMembers">' . declOfNum($group['members'], array('подписчик', 'подписчика', 'подписчиков')) . '</span>
                </div>
            </div>';
                }
                ?>
            </div>
        </div>

        <div class="right_block">

            <?php
            echo show_main_contacts($USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($USER['id'], $priv_companies);
            echo show_main_communities($_USER['id'], $priv_communities);
            echo show_main_album($USER['id'], $priv_photo);
            echo show_main_videos($USER['id'], $priv_video);
            echo show_main_audios($USER['id'], $priv_audios);
            ?>
        </div>

        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('#searchy').keyup(function () {
                var $val = $(this).val().toLowerCase();
                $.each($('.contact'), function () {
                    var name = $(this).attr('data-name').toLowerCase();

                    if ($val == '') {
                        $(this).show();
                    } else {
                        if (name.indexOf($val) == '-1') {
                            $(this).hide();
                        } else {
                            $(this).show();
                        }
                    }
                });
            });
            $('.groupUnJoin').click(function () {
                $.post($(this).attr('href'), '', function (data) {
                });
                $(this).parent().parent().remove();

                return false;
            });
            //searchy
        });
    </script>
<?php
footer();
