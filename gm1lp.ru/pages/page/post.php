﻿<?php
$iUser = true;
if (!$USER) {
#    redirect('/page/login');
    $USER = array(
        'ava' => 'avatar-main.jpg'
    );
    $iUser = false;
}
$_USER = $USER;
$itsI = true;
$class = ' noti';
$_ = false;

$age = '';
if ($_USER['date'] && is_numeric($_USER['date'])) {
    $d = date('d.m.Y', $_USER['date']);
    $age = declOfNum((getAgeFromDate($d)), array(' год', ' года', ' лет'));
}
$citys = getCity2($_USER['city']);
$countrys = getCountry2($_USER['country']);
$photos = getUserPhotos($_USER['id'], false, true);

$cMyContacts = getCountContacts($_USER['id']);
$myContacts = getContacts($_USER['id']);

# Right
$priv_photo = isPrivate($_USER['id'], $_USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($_USER['id'], $_USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($_USER['id'], $_USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($_USER['id'], $_USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right
$priv_age = isPrivate($_USER['id'], $_USER['private3'], AGE_ALL, AGE_C, $myContacts);
$priv_sp = isPrivate($_USER['id'], $_USER['private'], SP_ALL, SP_C, $myContacts);
$priv_work = isPrivate($_USER['id'], $_USER['private'], WORK_ALL, WORK_C, $myContacts);
$priv_loc = isPrivate($_USER['id'], $_USER['private'], LOCATION_ALL, LOCATION_C, $myContacts);
$priv_phone = isPrivate($_USER['id'], $_USER['private'], PHONE_ALL, PHONE_C, $myContacts);
$priv_wall = isPrivate($_USER['id'], $_USER['private2'], WALL_ALL, WALL_C, $myContacts);
$priv_ewall = isPrivate($_USER['id'], $_USER['private2'], EWALL_ALL, EWALL_C, $myContacts);
$priv_wallc = isPrivate($_USER['id'], $_USER['private2'], WALLC_ALL, WALLC_C, $myContacts);
$priv_wallsc = isPrivate($_USER['id'], $_USER['private3'], WALLSC_ALL, WALLSC_C, $myContacts);

setcookie('wall', $priv_wall ? 1 : 0, time() + time(), '/');
setcookie('ewall', $priv_ewall ? 1 : 0, time() + time(), '/');
setcookie('wallc', $priv_wallc ? 1 : 0, time() + time(), '/');
setcookie('wallsc', $priv_wallsc ? 1 : 0, time() + time(), '/');

$post = getPost($_GET['id']);
$c = count($post);

$skills = getUserSkills($_USER['id'], true);
///
$contactStatus = -1;
if (!$itsI) {
    $realMyContact = getContacts($USER['id']);
    if (isset($realMyContact[$_USER['id']])) {
        if ($realMyContact[$_USER['id']][1]) {
            $contactStatus = 1;
        } else {
            $contactStatus = 0;
        }
    }
}
header_($_USER['name'] . ' ' . $_USER['fam'], '', '', ''
    . '<script type="text/javascript" src="/js/vendor/jquery.ui.widget.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="/js/jquery.fileupload.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>
            <link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
            <link rel="stylesheet" href="/css/jquery.Jcrop.min.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="image_src" href="/uploads/avatars/' . $_USER['ava'] . '">
    <meta property="og:image" content="/uploads/avatars/' . $_USER['ava'] . '" />

', 'body-main');
$slide_img = '/images/slide.jpg';
if (file_exists('uploads/slide/' . $_USER['id'] . '.png')) {
    $slide_img = '/uploads/slide/' . $_USER['id'] . '.png';
}
?>
<style>
#footer .main_block{ background: none;}
</style>
<!--main-->
<div id="main" class="main_block centred" style="width: 790px;">

<div class="wall_wrapper">
<?php
    

    echo outputPost($post, $priv_wallc, $priv_wallsc);
?>
    </div>

<div class="clear"></div>
</div>
<div class="crop" id="divCrop">
    <h1>Выберите область</h1>
    <img src="" id="imgCrop" />
    <div>
        <button id="cancelCrop">Отменить</button>
        <button id="appendCrop">Сохранить</button>
    </div>
</div>
<!--main-->
<script>
    //if ($(window).scrollTop() + $(window).height() >= $('.files').height()-$('.files img:last').height()/*$(document).height() - 700*/) {

    var getVar = null;
    var lockLoad = false;
    var coords;
    var avatar = false;
    var miniDialog=true;
	
    $(document).ready(function () {
		/*
        $(window).scroll(function () {
            var bo = $(window).scrollTop();
            if (bo >= $('.wall_wrapper').scrollTop() + $('.left_block').height() - 300) {
                console.log('load_more_post');
                if (!lockLoad) {
                    lockLoad = true;
                    var offset = $('div[data-wid]').size();
                    $.post('', 'wall_offset=' + offset, function (data) {
                        $('.wall_wrapper').append(data);
                        $('.wall_post[data-wid]').each(function () {
                            console.log($(this).find('.wall_text span').size());
                            if ($(this).find('.wall_text span').size() == 2) {
                                clickable($(this).find('.wall_text span:last'));
                            } else {
                                clickable($(this).find('.wall_text'));
                            }
                        });
                        $('.wall_post[data-wid] .wall_post_comments .wall_post_content').each(function () {
                            clickable($(this).find('.wall_post_message p'));
                        });
                        doInitCommentEmoji();
                        lockLoad = false;
                    });
                }
            }
        });
		*/
        //////////////////////////////////
        $(document).ready(function () {
            $('.fancybox').fancybox({
            'padding': [20,20,0,20],
            'beforeShow': function (){
                $('.fancybox-skin').after('<div class="loading">Идет загрузка</div>');
            },
            'afterShow': function (){
                $this = this.element;

                $.post('/album/album-ajax', 'photo_id='+$this.attr('data-photo-id'), function (data){
                    $('.fancybox-skin').after(data);
                    $('.fancybox-skin').parent().find('.loading').remove();
                });
                    return false;
                }
            });
        });
        if (!$('#shortAbout table tr').size()) {
            $('#shortAbout').hide();
            $('#shortAboutH').hide();
        }
        $('.sinfo_img_toolbox a').click(function () {
            getVar = 'upload=1&avatar=2';
            $('#fileupload').fileupload({
                url: '?' + getVar,
                dataType: 'json',
                done: function (e, data) {
                    avatar = true;
                    if (data.result.error === undefined) {
                        //curImage.attr('img-src', data.result.file);
                        //curImage.click();
                        //window.location.reload();
                        $('#divCrop').show();
                        $img = $('#imgCrop');
                        $img.attr('src', '/'+data.result.file);
                                $img.Jcrop({
                                    minSize: [320, 320],
                                    setSelect: [0, 0, 320, 320],
                                      aspectRatio: 1 / 1,
                                    onSelect: function (c) {
                                        // Get coordionates
                                        coords = c;
                                    }
                                });
                        $img.load(function (){
                         $('#divCrop').center(true, false);
                        });
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
            $('#fileupload').click();
            return false;
        });
        $('#cancelCrop').click(function (){
            $img = $('#imgCrop');
            JcropAPI = $img.data('Jcrop');
            JcropAPI.destroy();
            $img.attr('style', '');
            $('#divCrop').hide();
            return false;
        });
        $('#appendCrop').click(function (){
            console.log(coords);
                var post = {
                'file': $('#imgCrop').attr('src'),
                'coords': coords
            };
            $.post('/?upload=2&iavatar='+(avatar?1:0), post, function (data){
                console.log(data);
                window.location.reload();
            });
            $('#cancelCrop').click();
            return false;
        });
        $('.slider_toolbox a').click(function () {
            getVar = 'upload=1&slide=2';
            $('#fileupload').fileupload({
                url: '?' + getVar,
                dataType: 'json',
                done: function (e, data) {
                    avatar=false;
                    if (data.result.error === undefined) {
                        //curImage.attr('img-src', data.result.file);
                        //curImage.click();
                        //window.location.reload();
                        $('#divCrop').show();
                        $img = $('#imgCrop');
                        $img.attr('src', '/'+data.result.file);
                                $img.Jcrop({
                                    minSize: [918, 478],
                                    setSelect: [0, 0, 918, 478],
                                      aspectRatio: 918 / 478,
                                    onSelect: function (c) {
                                        // Get coordionates
                                        coords = c;
                                    }
                                });
                        $img.load(function (){
                         $('#divCrop').center(true, false);
                        });
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
            $('#fileupload').click();
            return false;
        });
        $('.addphoto').click(function () {
            getVar = 'addPhoto=1&id='+current_dialog_id;
            $('#fileuploadForDialog').fileupload({
                url: '/message?' + getVar,
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.error === undefined) {
                        $('.mphotos a').each(function (){
                            $(this).click();
                        });
                        $('.mphotos').html('').append('<div><img src="/uploads/messages/'+data.result.file+'" /><a data-file="'+data.result.file+'" href="#"></a>');
                        $('.mfooter form').append('<input type="hidden" name="img[]" value="'+data.result.file+'"/>');
                        $('.mphotos').show();
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
            $('#fileuploadForDialog').click();
            return false;
        });
        $(document).on('click', '.mphotos a', function (){
            var file = $(this).attr('data-file');
            $(this).parent().remove();
            $('.mfooter form input[value="'+file+'"]').remove();
            if(!$('.mfooter form input').size()){
                 $('.mphotos').hide();
            }
            return false;
        });
        /*********************************************/
        $(document).on('click', '#wallPost button', function () {
            $('#wallPost').find('textarea[name="text"]').val(kemoji1.getValue(KEmoji.HTML_VALUE));
            if (!$.trim($('#wallPost').find('textarea[name="text"]').val()) && !$('#uploadList li').size()) {
                console.log('nini');
                return false;
            }
            $.post('', $('#wallPost').serialize() + '&add_post=1', function (data) {
                $('#wallPost input').val('');
                $('#wallPost textarea').val('');
                $('.wall_wrapper .wall_post_comment:first').after(data);
                clickable($('.wall_post:first').find('.wall_text'));
                if($('.wall_post:first').find('div[data-video]').size()){
                preview_video($('.wall_post:first').find('div[data-video]'));
                }
                $('.wall_post:first .wallComment textarea').show();

                $('')

                $('.grid_wall_images img').load(function (){
                    normalizeWallPost();
                });
                $('#uploadList').html('');
                $('.KEmoji_Input div[contenteditable]').html('');
                $('#myEmojiField').css({height:'auto'});
                $('#myEmojiField').removeClass('h146');

            });
            return false;
        });
        $(document).on('click', '.wallComment button', function () {
            $wall = $(this).parent().parent().parent();

            $kemoji = $wall.find('div[emoji-id]').attr('emoji-id');
            $kemoji = kemoji5[parseInt($kemoji) - 1];
            $wall.find('textarea[name="text"]').val($kemoji.getValue(KEmoji.HTML_VALUE));

            if (!$.trim($wall.find('form textarea[name="text"]').val())) {
                return false;
            }
            $.post('', $wall.find('form').serialize() + '&add_comment=1', function (data) {
                //$('.wallComment input').val('');
                $('.wallComment textarea').val('');
                $wall.find('.KEmoji_Input div[contenteditable]').html('');
                //$('.wall_wrapper .wall_post_comment:first').after(data);
                $('.uploadListComment').html('');
                var wid = $wall.find('input').val();
                $('.wall_post[data-wid="' + wid + '"] .wall_post_comments')[0].outerHTML = data;
                clickable($('.wall_post[data-wid="' + wid + '"] .wall_post_comments .wall_post_content:first .wall_post_message p'));
            });
            return false;
        });
        function normalizeWallPost() {
            var min = 186;
            $('.wall_post .grid_wall_images img').each(function () {
                if (min > $(this).height()) {
                    min = $(this).height();
                }
            });
            $('.wall_post .grid_wall_images a').each(function () {
                $(this).height(min)
            });
        }
        $('.grid_wall_images img').load(function (){
            normalizeWallPost();
        });

    });
    $(document).on('click', '.uploadWallComment', function () {
        $wall = $(this).parent().parent();
        $('#fileuploadForWall').fileupload({
            url: '?uploadWall&uid=<?php echo $_USER['id']; ?>',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.error === undefined) {
                    //curImage.attr('img-src', data.result.file);
                    //curImage.click();
                    console.log(data.result);
                    $wall.find('.uploadListComment').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
                            + '<a href="#" onlick="return false;"><img class="rounded_3 width71" src="/uploads/wall/<?php echo $_USER['id']; ?>/' + data.result.file + '" /></a>'
                            + '<input type="hidden" name="file[]" value="/uploads/wall/<?php echo $_USER['id']; ?>/' + data.result.file + '" /></li>');
                } else {
                    alert(data.result.error);
                }
                console.log(data);

            }
            ,
            progressall: function (e, data) {

            }
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

        $('#fileuploadForWall').click();

        return false;
    });


    $(document).on('click', '.wall_wrapper .wall_icon_repost_y', function () {
        alert('Запись уже опубликована');
        return false;
    });
    $(document).on('click', '.wall_wrapper .wall_icon_repost,.wall_wrapper .wall_icon_repost', function () {
        console.log('repost');
        if ($(this).hasClass('wall_icon_repost_y')) {
            $(this).addClass('wall_icon_repost');
            $(this).removeClass('wall_icon_repost_y');
        } else {
            $(this).addClass('wall_icon_repost_y');
            $(this).removeClass('wall_icon_repost');
        }
        $this = $(this);
        var id = $(this).parent().parent().parent().parent().attr('data-wid');
        console.log(id);
        $.post("/", "repost=1&id=" + id, function (data, textStatus) {
            if (data == '1') {
                $this.text(parseInt($this.text()) + 1);
                alert('Запись была опубликована на Вашу стену');
            } else {

            }
            console.log(data);
            //listLike(id, undefined, false);
        });

        return false;
    });
</script>
<?php
footer();
