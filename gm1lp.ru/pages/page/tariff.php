<?php
if (!$USER) {
    redirect('/page/login');
}

$_USER = $USER;

$tariffs = array();
$prices = array();

$q = query("SELECT * FROM `tariff`;");
foreach($q['rows'] as $tariff) {
    $tariffs[$tariff['level']] = $tariff;
    $prices[$tariff['level']] = array();
    for($i = 0; $i < 12; $i ++) {
        switch($i) {
            case 0:
            case 1:
                $prices[$tariff['level']][$i] = floatval($tariff['discount_1-2']);
                break;
            case 2:
            case 3:
            case 4:
                $prices[$tariff['level']][$i] = floatval($tariff['discount_3-5']);
                break;
            case 5:
            case 6:
            case 7:
                $prices[$tariff['level']][$i] = floatval($tariff['discount_6-8']);
                break;
            case 8:
            case 9:
            case 10:
                $prices[$tariff['level']][$i] = floatval($tariff['discount_9-11']);
                break;
            case 11:
                $prices[$tariff['level']][$i] = floatval($tariff['discount_12']);
                break;
        }
    }
}

if (isset($_POST['level'])) {
    $e = explode('_', $_POST['level']);
    if (count($e) != 2 || !isset($prices[$e[0]])) {
        echo 'alert("Ошибка")';
    } else {
        $level = $e[0];
        $i = (int)$e[1] + 1;

        $disc = $tariffs[$level]['activation_price'] * $i;
        $cost = $disc * (1 - $prices[$level][$i - 1] / 100);

        $time = time();
        if ($_USER['pro_end'] > $time && $_USER['level'] == $level) {
            $time = $_USER['pro_end'];
        }
        $pro_end = $time + (60 * 60 * 24 * 30) * $i;
        if ($_USER['money'] < $cost) {
            echo '$(".mtClose").click();showModal(".error");';
        } else {
            $uid = $_USER['id'];
            $amount = $cost * -1;
            $stat = isVer(array('pro_end' => $pro_end, 'level' => $level));
            $time = declOfNum($i, array('месяц', 'месяца', 'месяцев'));
            query("INSERT INTO `nx_balance_log` (`user_id`,`amount`,`type`,`created`,`comment`) VALUES('$uid','$amount','1','" . time() . "','Покупка статуса &quot;$stat&quot; на $time');");

            updateUserById($_USER['id'], array('money', 'level', 'pro_end'), array(($_USER['money'] - $cost), $level, $pro_end));
            forOwnerReferal($_USER['referal'], $cost);
            echo '$(".mtClose").click();showModal(".success");';
        }
    }

    exit();

}
$cMyContacts = getCountContacts($_USER['id']);
header_('Тарифы', '', '', ''
    . '<link rel="stylesheet" type="text/css" href="/css/style_tariffs.css"/>');
?>
    <!--main-->
    <div id="main" class="main_block centred tariffs" xmlns="http://www.w3.org/1999/html">
        <div class="main_slider">
            <div class="slider_text">
                <p class="st_large">
                    <span class="st_highlight"><?php echo $USER['name']; ?></span>, станьте<br/>
                    профессионалом продаж <br/>
                    в социальных сетях
                </p>

                <ul class="st_list">
                    <li>Связывайтесь с серьезными людьми, принимающими решения</li>
                    <li>Будьте экспертом в своем деле и контактируйте с экспертами</li>
                    <li>Будьте в ТОПе поиска имея приоритет над другими</li>
                    <li>Налаживайте и обзаведитесь сильными связями и контактами</li>
                </ul>
                <form action="/people/" target="_blank">
                    <button class="btn rounded_3">Подробнее</button>
                </form>
            </div>
            <div class="slider_info">
                <?php echo iconStatus($_USER['status']); ?>
                <div class="sinfo_img">
                    <div class="avaTariff" style="position: relative; display: inline-block;">
                        <img class="circled" src="<?php echo '/uploads/avatars/' . $_USER['ava']; ?>" id="avatar"/>
                        <img class="verIco" src="<?php echo iconVer($_USER); ?>"/>
                    </div>
                </div>

                <div class="sinfo_name"><?php echo $_USER['name'] . ' ' . $_USER['fam']; ?></div>

                <div class="sinfo_age"><?php echo $age; ?></div>

                <!--span>Место работы:</span> <a href="#">GM</a><br />
                <span>Место учебы:</span> <a href="#">МГОУ</a><br /-->
                <?php
                $citys = getCity2($_USER['city']);
                $countrys = getCountry2($_USER['country']);
                if ($_USER['mwork']) {
                    echo '<span>Место работы:</span> <a href="#">' . $_USER['mwork'] . '</a><br />';
                }
                if ($_USER['dwork']) {
                    echo '<span>Должность:</span> <a href="#">' . $_USER['dwork'] . '</a><br />';
                }
                if ($_USER['city']) {
                    echo '<span>Место проживания:</span> <a href="#">' . $citys['name'] . '</a><br />';
                }
                if ($_USER['country']) {
                    echo '<span>Страна:</span> <a href="#">' . $countrys['name'] . '</a><br />';
                }
                if ($_USER['relation']) {
                    $q = $_USER['gender'] == 'male' ? 2 : 1;
                    echo '<a href="#">' . $_LANG['PROFILE_RELATION'][$q][$_USER['relation']] . '</a>';
                }
                ?>
                <div class="sinfo_amount">
                    <span
                        class="sinfo_contacts"><?php echo declOfNum($cMyContacts['count'], array(' контакт', ' контакта', ' контактов')); ?></span>
                    <span><?php echo declOfNum($_USER['see'], array(' просмотр', ' просмотра', ' просмотров')); ?></span>
                </div>
            </div>
        </div>

        <table class="tariff_table" cellpadding="0" cellspacing="0">
            <?php $curLevel = isVer($USER, true);?>
            <tr>
                <th class="tf_td_info">Сравните планы</th>
                <?php foreach($tariffs as $tariff):?>
                    <th class="tf_td_value">
                        <?php if ($curLevel == $tariff['level']):?><div class="current_plan">Ваш текущий<br/>план</div><?php endif;?>
                        <img src="<?php echo $tariff['image_url'];?>"><br/><?php echo $tariff['title'];?>
                    </th>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info"></td>
                <?php foreach($tariffs as $tariff):?>
                    <?php if($tariff['activation_price'] == 0):?>
                        <td class="tf_td_value"></td>
                    <?php else:?>
                        <td class="tf_td_btn tf_td_mix">
                            <span class="large_price"><span class="large_price_amount"><?php echo $tariff['activation_price'];?></span> руб</span><br/>в месяц<br/>
                            <?php if ($curLevel <= $tariff['level']):?><button class="btn btn-size-small rounded_3 toggleMt" data-id="<?php echo $tariff['level'];?>">Активировать прямо сейчас!</button><?php endif;?>
                            <form action="/help/exp" target="_blank" style="display: none;"><button class="btn btn-size-small btn-color-yellow rounded_3">Получить бесплатно навсегда</button></form>
                        </td>
                    <?php endif;?>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Запросы о знакомстве</span>
                    Знакомьтесь с сотрудниками нужных компаний через ваши контакты в GM1LP
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <?php if($tariff['acquaintance_requests'] == -1):?>
                        <td class="tf_td_value infinity">∞</td>
                    <?php else:?>
                        <td class="tf_td_value"><?php echo $tariff['acquaintance_requests'];?></td>
                    <?php endif;?>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Участие в конкурсах</span>
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <?php if($tariff['competition_participations'] == -1):?>
                        <td class="tf_td_value infinity">∞</td>
                    <?php else:?>
                        <td class="tf_td_value"><?php echo $tariff['competition_participations'];?></td>
                    <?php endif;?>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Полные профили</span>
                    Просматривайте полные профили всех людей в вашей сети – 1-го, 2-го и 3-го уровня.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['full_profiles']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">GM перевод средств и комиссия</span>
                    Переводите средства друзьям и коллегам, а так же исполнителям - моментально
                    и без лишних хлопот. Здесь указана комиссия за перевод средств другому
                    пользователю.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value"><?php echo $tariff['gm_transfer_fee'];?>%</td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">GM перевод средств по СБР и комиссия</span>
                    СБР - Сделка Без Риска, что значит что в случае спорного решения, GM примет активное участие в
                    разбирательстве вплоть до суда
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value"><?php echo $tariff['gm_sbr_transfer_fee'];?>%</td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Комиссия за вывод средств</span>
                    Т.к., GM не берет комиссию со сделок на GoodMoneys, то это делается при выводе
                    средств.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value"><?php echo $tariff['gm_withdrawal_fee'];?>%</td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Приоритет в поиске</span>
                    Чем выше уровень, тем больше шансов стать
                    исполнителем очередного заказа. Мастер не может иметь приоритет над профессионалом, а профессионал
                    над экспертом.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['search_priority']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Оффлайн задания</span>
                    Подразумевают тот тип заказа, для выполнения которого нужно выйти из дома, например «помочь по
                    дому», «купить и
                    доставить» и т.д.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['offline_tasks']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Онлайн задания свыше 2 руб.</span>
                    Регистрации в играх, на сайтах установка приложений, платные комментарии и т.д.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['tasks_gt_2']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Онлайн задания свыше 20 руб.</span>
                    Регистрации в играх, на сайтах установка приложений, платные комментарии и т.д.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['tasks_gt_20']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Онлайн задания свыше 50 руб.</span>
                    Регистрации в играх, на сайтах установка приложений, платные комментарии и т.д.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['tasks_gt_50']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Онлайн задания свыше 999 руб.</span>
                    Регистрации в играх, на сайтах установка приложений, платные комментарии и т.д.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['tasks_gt_999']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Стоимость запуска скидочного предложения или конкурса</span>
                    Указана стоимость за 1 день проведения акции. Запуск в баннере (шапке сайта)
                    будет стоить в 3 дороже.
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value"><?php echo $tariff['competition_price'];?> руб.</td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Комиссия сайта в режиме кэшбэка.</span>
                    Суммы комиссий при использовании режима "кэшбэк".
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value"><?php echo $tariff['cashback_fee'];?>%</td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Отчисления от приглашенных</span>
                    <!--Переводите средства друзьям и коллегам, а так же исполнителям - моментально
                    и без лишних хлопот. Здесь указана комиссия за перевод средств другому
                    пользователю.-->
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value"><?php echo $tariff['invited_charge'];?>%</td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Эмблема «Мастер»</span>
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['master_emblem']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Эмблема «Профессионал»</span>
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['professional_emblem']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr>
                <td class="tf_td_info">
                    <span class="tf_header">Эмблема «Эксперт»</span>
                </td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_value <?php if($tariff['expert_emblem']):?>tf_td_checked<?php endif;?>"></td>
                <?php endforeach;?>
            </tr>
            <tr class="tf_td_mix">
                <td></td>
                <?php foreach($tariffs as $tariff):?>
                    <td class="tf_td_btn">
                        <?php if($curLevel <= $tariff['level'] && $tariff['level'] > 0):?>
                            <button class="btn btn-size-small rounded_3 toggleMt" data-id="<?php echo $tariff['level'];?>">Активировать прямо сейчас!</button>
                            <form action="/help/exp" target="_blank"><button class="btn btn-size-small btn-color-yellow rounded_3" style="display: none;">Получить бесплатно навсегда</button></form>
                        <?php endif;?>
                    </td>
                <?php endforeach;?>
            </tr>
            <tr>
                <?php for($i = 0; $i <= count($tariffs); $i ++):?>
                    <td></td>
                <?php endfor;?>
            </tr>
        </table>
        <?php foreach($tariffs as $tariff):?>
            <div class="modal_tariff tariff<?php echo $tariff['level'];?>">
                <div class="mt-header">
                    Получение статуса
                    <a href="#" class="mtClose"><img src="/images/mt-close.png"/></a>
                </div>
                <div class="mt-body">
                    <div class="mt-content">
                        <img src="<?php echo $tariff['image_url'];?>"/><br/>
                        <div style="color: #0f1731; font-size: 27px;"><?php echo $tariff['title'];?></div>
                        <br/>
                        <form action="" style="display: inline-block;">
                            <?php for($i = 0; $i < 12; $i ++):?>
                                <?php $disc = $prices[$tariff['level']][$i] > 0 ? $tariff['activation_price'] * ($i + 1) : 0;?>
                                <?php $cost = $tariff['activation_price'] * ($i + 1) * (1 - $prices[$tariff['level']][$i] / 100);?>
                                <div class="input_block radiobox">
                                    <label for="t<?php echo "{$tariff['level']}_$i";?>">
                                        <input type="radio" name="level" value="<?php echo "{$tariff['level']}_$i";?>" id="t<?php echo "{$tariff['level']}_$i";?>" />
                                        <i></i> Получить на <?php echo declOfNum($i + 1, array('месяц', 'месяца', 'месяцев'));?>:<?php if ($disc):?> <s><?php echo $disc;?></s><?php endif;?> <b><?php echo $cost;?> руб.</b>
                                    </label>
                                </div>
                            <?php endfor;?>
                            <br/>
                            <button class="btn">Получить</button>
                        </form>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
        <div class="modal_tariff error">
            <div class="mt-header">
                Получение статуса
                <a href="#" class="mtClose"><img src="/images/mt-close.png"/></a>
            </div>
            <div class="mt-body">
                <div class="mt-content">
                    <div>Ошибка!</div>
                    <small>У вас недостаточно средств</small>
                    <br/>
                    <br/>
                    <button class="btn red closeError" onclick="closeError(); return false;">ОК</button>
                    <br/>
                    <br/>
                    <a href="/balance">Пополнить баланс</a>
                </div>
            </div>
        </div>
        <div class="modal_tariff success">
            <div class="mt-header">
                Получение статуса
                <a href="#" class="mtClose"><img src="/images/mt-close.png"/></a>
            </div>
            <div class="mt-body">
                <div class="mt-content">
                    <div>Успешно!</div>
                    <small>Вы успешно приобрели новый уровень</small>
                    <br/>
                    <br/>
                    <button class="btn blue closeSuccess"
                            onclick="closeError();window.location.reload(); return false;">Отлично
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--main-->
<?php
footer();
