<?php
$json['eval'] = array();
$i = true;
$_USER = $USER;
$error = '';

if (isAjax()) {
    $post = $_POST;
    $keys = array();
    $values = array();
    $save_phone = false;
    if (isset($_POST['newCode']) && $_POST['newCode'] == 1) {
        if (isset($_COOKIE['timeCode'])) {
            $time = (int)$_COOKIE['timeCode'] - time();
        } else {
            $time = TIMECODE;
            setcookie('timeCode', time() + $time, time() + $time, '/', '.' . $_SERVER['HTTP_HOST']);
            $json['eval'][] = '$("#textErrCode_").html(\'<div id="textErrCode"><span id="newCode">Выслать новый код</span> можно будет через <span id="newCodeTime">' . $time . '</span>сек.</div>\');timeCode=' . $time . '; startNewCode();';
            $json['eval'][] = '$("#auth_block3").show(); $("#auth_phone_code1").attr({"data-tip":"Новый код был отправлен","data-mood":"positive","data-position":"bottom"}); tip($("#auth_phone_code1"));';
            $code = mt_rand(111111, 999999);
            sendMessage($_POST['auth_phone'], 'Cod dlya podtverzhdeniya: ' . $code);
            updateUserById($_USER['id'], array('tmp'), array($code));
        }
    } elseif ($_POST['auth_phone_code1']) {
        if ($_POST['auth_phone_code1'] != $_USER['tmp']) {
            $json['eval'][] = '$("#auth_phone_code1").attr({"data-tip":"Указан неверный код","data-mood":"negative","data-position":"bottom"}); tip($("#auth_phone_code1"));';
        } else {
            $keys[] = 'auth';
            $values[] = 1;
            $keys[] = 'auth_phone';
            $values[] = $_POST['auth_phone'];
        }
    } else {
        if (isset($_POST['auth'])) {
            if (!$_POST['auth']) {
                $keys[] = 'auth';
                $values[] = 0;
            } elseif ($_POST['auth'] && !$_POST['auth_phone']) {
                $json['eval'][] = '$("#auth_phone").attr({"data-tip":"Укажите телефон","data-mood":"positive","data-position":"bottom"}); tip($("#auth_phone"));';
            } elseif ($_POST['auth'] && !checkPhoneNumber($_POST['auth_phone'])) {
                $json['eval'][] = '$("#auth_phone").attr({"data-tip":"Укажите телефон в формате 89293953102","data-mood":"positive","data-position":"bottom"}); tip($("#auth_phone"));';
            } elseif ($_POST['auth'] && $_POST['auth_phone'] && !$_USER['auth_phone']) {

                $code = mt_rand(111111, 999999);

                $ret = sendMessage($_POST['auth_phone'], 'Cod dlya podtverzhdeniya: ' . $code);
                if ($ret) {
                    updateUserById($_USER['id'], array('tmp'), array($code));

                    if (isset($_COOKIE['timeCode'])) {
                        $time = (int)$_COOKIE['timeCode'] - time();
                    } else {
                        $time = TIMECODE;
                        setcookie('timeCode', time() + $time, time() + $time, '/', '.' . $_SERVER['HTTP_HOST']);
                    }
                    $json['eval'][] = '$("#textErrCode_").html(\'<div id="textErrCode"><span id="newCode">Выслать новый код</span> можно будет через <span id="newCodeTime">' . $time . '</span>сек.</div>\');timeCode=' . $time . '; startNewCode();';
                    $json['eval'][] = '$("#auth_phone").attr("readonly","readonly");$("#auth_block3").show(); $("#auth_phone_code1").attr({"data-tip":"Пожалуйста, подтвердите свой номер. <br/>На него был отправлен код, пожалуйста укажите его","data-mood":"positive","data-position":"bottom"}); tip($("#auth_phone_code1"));';
                } else {
                    $json['eval'][] = '$("#auth_phone").attr({"data-tip":"Укажите телефон в формате 89293953102","data-mood":"positive","data-position":"bottom"}); tip($("#auth_phone"));';
                }
            } elseif ($_POST['auth'] && $_POST['auth_phone'] != $_USER['auth_phone']) {
                $json['eval'][] = '$("#auth_block3").show(); $("#auth_phone_code1").attr({"data-tip":"Номера различаются, для изменения номера, свяжитесь с администрацией","data-mood":"positive","data-position":"bottom"}); tip($("#auth_phone_code1"));';

            } else {
                $save_phone = true;
            }
        }
    }

    if ($keys) {
        $json['eval'][] = '$("#auth_block3").hide();$("#alertHeader").text("Успешно!");$("#alertMessage").text("Данные успешно сохранены!"); showModal(".alertMessage");';
        updateUserById($_USER['id'], $keys, $values);
    }
    exit(implode(' ', $json['eval']));
#    exit('alert("Данные успешно сохранены' . (isset($email) ? "\nДля смены адреса, пожалуйста перейдите по ссылке отправленной на вашу почту" : '') . '");');
}
header_('Конфиденциальность', '', '', ''
    . '');
$private = $_USER['private'];
$private2 = $_USER['private2'];
$private3 = $_USER['private3'];
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <form id="formPrivate">
                <div class="div_left">
                    <h1 class="prof_h1">Конфиденциальность</h1>

                    <div class="prof_blok_left prof_blok_puktW50pr">
                        <h2>Безопасность</h2>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Двухфакторная аутентификация</p>

                            <div class="styled-select">
                                <select name="auth" id="auth">
                                    <?php
                                    if ($USER['auth'] == 1) {
                                        echo '<option value="1" selected="selected">Включена</option>';
                                        echo '<option value="0">Выключена</option>';
                                    } else {
                                        echo '<option value="1">Включена</option>';
                                        echo '<option value="0" selected="selected">Выключена</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt" id="auth_block">
                            <p>Номер телефона</p>
                            <input type="text" id="auth_phone" name="auth_phone"
                                   placeholder="Номер телефона, например: 79293953102"
                                   value="<?php echo $_USER['auth_phone']; ?>"/>
                        </div>
                    </div>
                    <div class="prof_blok_left prof_blok_puktW50pr" id="auth_block3">
                        <div class="prof_blok_pukt">
                            <p>Код подтверждения</p>
                            <input type="text" id="auth_phone_code1" name="auth_phone_code1" placeholder="Укажите код"
                                   value=""/><br/>

                            <div id="textErrCode_"></div>
                        </div>
                    </div>

                    <a class="prof_butt nxPrivate" href="#">Сохранить</a>

                </div>
            </form>
        </div>

        <div class="right_block">
            <!-- Contacts -->
            <div class="r_bl_menu">
                <ul>
                    <li><a href="/settings">Профиль</a></li>
                    <li><a href="/skills">Навыки</a></li>
                    <li><a href="/private">Конфиденциальность</a></li>
                    <li><a href="/secure" class="akt">Безопасность</a></li>
                    <li><a href="/balance">Баланс</a></li>
                </ul>
            </div>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('#formPrivate').submit(function () {
                $.post('', $(this).serialize(), function (data) {
                    eval(data);
                });
                return false;
            });
            $('.nxPrivate').click(function () {
                $('#formPrivate').submit();
                return false;
            });
        });
        startCheckAuth();
    </script>
<?php
footer();

