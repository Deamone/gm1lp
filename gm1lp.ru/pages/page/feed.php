<?php
$iUser = true;
if (!$USER) {
#    redirect('/page/login');
    $USER = array(
        'ava' => 'avatar-main.jpg'
    );
    $iUser = false;
}
$_USER = $USER;
$itsI = true;
$class = ' noti';
$_ = false;

if (!isset($_USER['id']) && !$iUser) {
    redirect('/page/login');
}

if ($itsI) {
    $class = '';
}
if (isAjax()) {
    if (isset($_POST['wall_offset'])) { ///////////////////////// TARGET
        $offset = $SYSTEM['post']['wall_offset'];
        $posts = getNotices($offset, 5);
        foreach ($posts as $post) {
            echo outputPost($post, isset($_COOKIE['wallc']) ? $_COOKIE['wallc'] : 0, isset($_COOKIE['wallsc']) ? $_COOKIE['wallsc'] : 0);
        }
        exit();
    }
    /** .Wall * */
    #?like=get&wid='+wall_id
    if (isset($_GET['like']) && isset($_GET['wid']) && $iUser) {
        $likes = getLikesPost($_GET['wid'], false, 9999, true);
        exit(json_encode($likes));
    }
    if (isset($_GET['like']) && isset($_GET['cid']) && $iUser) {
        $likes = getLikesComments($_GET['cid'], false, 999);
        exit(json_encode($likes));
    }
}
/* * ******************* */


$cMyContacts = getCountContacts($_USER['id']);
$myContacts = getContacts($_USER['id']);

# Right
$priv_photo = isPrivate($_USER['id'], $_USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($_USER['id'], $_USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($_USER['id'], $_USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($_USER['id'], $_USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right
$priv_age = isPrivate($_USER['id'], $_USER['private3'], AGE_ALL, AGE_C, $myContacts);
$priv_sp = isPrivate($_USER['id'], $_USER['private'], SP_ALL, SP_C, $myContacts);
$priv_work = isPrivate($_USER['id'], $_USER['private'], WORK_ALL, WORK_C, $myContacts);
$priv_loc = isPrivate($_USER['id'], $_USER['private'], LOCATION_ALL, LOCATION_C, $myContacts);
$priv_phone = isPrivate($_USER['id'], $_USER['private'], PHONE_ALL, PHONE_C, $myContacts);
$priv_wall = isPrivate($_USER['id'], $_USER['private2'], WALL_ALL, WALL_C, $myContacts);
$priv_ewall = isPrivate($_USER['id'], $_USER['private2'], EWALL_ALL, EWALL_C, $myContacts);
$priv_wallc = isPrivate($_USER['id'], $_USER['private2'], WALLC_ALL, WALLC_C, $myContacts);
$priv_wallsc = isPrivate($_USER['id'], $_USER['private3'], WALLSC_ALL, WALLSC_C, $myContacts);

setcookie('wall', $priv_wall ? 1 : 0, time() + time(), '/');
setcookie('ewall', $priv_ewall ? 1 : 0, time() + time(), '/');
setcookie('wallc', $priv_wallc ? 1 : 0, time() + time(), '/');
setcookie('wallsc', $priv_wallsc ? 1 : 0, time() + time(), '/');


$posts = getNotices(0, 1);
$c = getCountNotices();
if (isset($posts[0])) {
    updateUserById($USER['id'], array('last_wid'), array($posts[0]['wall_id']));
    $USER['last_wid'] = $posts[0]['wall_id'];
}
header_('Новостная лента', '', '', ''
    . '<script type="text/javascript" src="/js/vendor/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="/js/jquery.fileupload.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>
            <link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="image_src" href="/uploads/avatars/' . $_USER['ava'] . '">
    <meta property="og:image" content="/uploads/avatars/' . $_USER['ava'] . '" />

', 'body-main');

$posts = getNotices(0, 3);
$c = getCountNotices();
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <input id="fileupload" type="file" name="file" style="display: none;">
        <input id="fileuploadForWall" type="file" name="file" style="display: none;">
        <input id="fileuploadForDialog" type="file" name="file" style="display: none;">


        <div class="left_block">


            <div class="wall_wrapper">
                <?php
                if (!$posts) {
                    echo '<h1 style="text-align: center">У вас пока нет новостей</h1>';
                }
                foreach ($posts as $post) {
                    //if (!$post['repost']) {
                    echo outputPost($post, $priv_wallc, $priv_wallsc);
                    //}
                }
                ?>
            </div>
        </div>

        <div class="right_block">

            <?php
            echo show_main_contacts($_USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($_USER['id'], $priv_companies);
            echo show_main_communities($_USER['id'], $priv_communities);
            echo show_main_album($_USER['id'], $priv_photo);
            echo show_main_videos($_USER['id'], $priv_video);
            echo show_main_audios($_USER['id'], $priv_audios);
            ?>

        </div>

        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        //if ($(window).scrollTop() + $(window).height() >= $('.files').height()-$('.files img:last').height()/*$(document).height() - 700*/) {

        var getVar = null;
        var lockLoad = false;
        var coords;
        var avatar = false;
        var miniDialog = true;
        $(document).ready(function () {
            $(window).scroll(function () {
                var bo = $(window).scrollTop();
                if (bo >= $('.wall_wrapper').scrollTop() + $('.left_block').height() - 300) {
                    console.log('load_more_post');
                    if (!lockLoad) {
                        lockLoad = true;
                        var offset = $('div[data-wid]').size();
                        $.post('', 'wall_offset=' + offset, function (data) {
                            $('.wall_wrapper').append(data);
                            $('.wall_post[data-wid]').each(function () {
                                console.log($(this).find('.wall_text span').size());
                                if ($(this).find('.wall_text span').size() == 2) {
                                    clickable($(this).find('.wall_text span:last'));
                                } else {
                                    clickable($(this).find('.wall_text'));
                                }
                            });
                            $('.wall_post[data-wid] .wall_post_comments .wall_post_content').each(function () {
                                clickable($(this).find('.wall_post_message p'));
                            });
                            doInitCommentEmoji();
                            lockLoad = false;
                        });
                    }
                }
            });
            //////////////////////////////////
            $(document).ready(function () {
                $('.fancybox').fancybox({
                    'padding': [20, 20, 0, 20],
                    'beforeShow': function () {
                        $('.fancybox-skin').after('<div class="loading">Идет загрузка</div>');
                    },
                    'afterShow': function () {
                        $this = this.element;

                        $.post('/album/album-ajax', 'photo_id=' + $this.attr('data-photo-id'), function (data) {
                            $('.fancybox-skin').after(data);
                            $('.fancybox-skin').parent().find('.loading').remove();
                        });
                        return false;
                    }
                });
            });

            /*********************************************/
            $(document).on('click', '.wallComment button', function () {
                $wall = $(this).parent().parent().parent();

                $kemoji = $wall.find('div[emoji-id]').attr('emoji-id');

                $kemoji = kemoji5[parseInt($kemoji) - 1];
                $wall.find('textarea[name="text"]').val($kemoji.getValue(KEmoji.HTML_VALUE));

                if (!$.trim($wall.find('form textarea[name="text"]').val())) {
                    return false;
                }
                $.post('/', $wall.find('form').serialize() + '&add_comment=1', function (data) {
                    //$('.wallComment input').val('');
                    $('.wallComment textarea').val('');
                    $wall.find('.KEmoji_Input div[contenteditable]').html('');
                    //$('.wall_wrapper .wall_post_comment:first').after(data);
                    $('.uploadListComment').html('');
                    var wid = $wall.find('input').val();
                    $('.wall_post[data-wid="' + wid + '"] .wall_post_comments')[0].outerHTML = data;
                    clickable($('.wall_post[data-wid="' + wid + '"] .wall_post_comments .wall_post_content:first .wall_post_message p'));
                });
                return false;
            });
            function normalizeWallPost() {
                var min = 186;
                $('.wall_post .grid_wall_images img').each(function () {
                    if (min > $(this).height()) {
                        min = $(this).height();
                    }
                });
                $('.wall_post .grid_wall_images a').each(function () {
                    $(this).height(min)
                });
            }

            $('.grid_wall_images img').load(function () {
                normalizeWallPost();
            });

        });
        $(document).on('click', '#uploadWall', function () {
            if ($('#uploadList li').size() >= 5) {
                return false;
            }
            $('#fileuploadForWall').fileupload({
                url: '/?uploadWall&uid=<?php echo $_USER['id']; ?>',
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.error === undefined) {
                        //curImage.attr('img-src', data.result.file);
                        //curImage.click();
                        console.log(data.result);
                        $('#uploadList').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
                        + '<a href="#" onlick="return false;"><img class="rounded_3 width71" src="/uploads/wall/<?php echo $_USER['id']; ?>/' + data.result.file + '" /></a>'
                        + '<input type="hidden" name="file[]" value="/uploads/wall/<?php echo $_USER['id']; ?>/' + data.result.file + '" /></li>');
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);

                }
                ,
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

            $('#fileuploadForWall').click();

            return false;
        });
        $(document).on('click', '#uploadWallDoc', function () {
            if ($('#uploadList li').size() >= 5) {
                return false;
            }
            $('#fileuploadForWall').fileupload({
                url: '/?uploadWallDoc',
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.error === undefined) {
                        //curImage.attr('img-src', data.result.file);
                        //curImage.click();
                        console.log(data.result);
                        $('#uploadList').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();removeHideData(\'doc\', ' + data.result.doc + ');return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
                        + '<a href="#" onlick="return false;" class="doc">' + (data.result.image ? '<img class="rounded_3 width71" src="/uploads/docs/<?php echo $_USER['id']; ?>/' + data.result.file + '" />' : data.result.file) + '</a>'
                        + '<input type="hidden"/></li>');
                        addHideData('doc', data.result.doc);
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);

                }
                ,
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

            $('#fileuploadForWall').click();

            return false;
        });
        $(document).on('click', '.uploadWallComment', function () {
            $wall = $(this).parent().parent();
            $('#fileuploadForWall').fileupload({
                url: '/?uploadWall&uid=<?php echo $_USER['id']; ?>',
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.error === undefined) {
                        //curImage.attr('img-src', data.result.file);
                        //curImage.click();
                        console.log(data.result);
                        $wall.find('.uploadListComment').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
                        + '<a href="#" onlick="return false;"><img class="rounded_3 width71" src="/uploads/wall/<?php echo $_USER['id']; ?>/' + data.result.file + '" /></a>'
                        + '<input type="hidden" name="file[]" value="/uploads/wall/<?php echo $_USER['id']; ?>/' + data.result.file + '" /></li>');
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);

                }
                ,
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

            $('#fileuploadForWall').click();

            return false;
        });


        $(document).on('click', '.wall_wrapper .wall_icon_repost_y', function () {
            alert('Запись уже опубликована');
            return false;
        });
        $(document).on('click', '.wall_wrapper .wall_icon_repost,.wall_wrapper .wall_icon_repost', function () {
            console.log('repost');
            if ($(this).hasClass('wall_icon_repost_y')) {
                $(this).addClass('wall_icon_repost');
                $(this).removeClass('wall_icon_repost_y');
            } else {
                $(this).addClass('wall_icon_repost_y');
                $(this).removeClass('wall_icon_repost');
            }
            $this = $(this);
            var id = $(this).parent().parent().parent().parent().attr('data-wid');
            console.log(id);
            $.post("/", "repost=1&id=" + id, function (data, textStatus) {
                if (data == '1') {
                    $this.text(parseInt($this.text()) + 1);
                    alert('Запись была опубликована на Вашу стену');
                } else {

                }
                console.log(data);
                //listLike(id, undefined, false);
            });

            return false;
        });
        $(document).on('click', '.delWall', function () {
            $post = $(this).parent().parent().parent().parent();
            $.post("/", "del_post=1&id=" + $(this).attr('data-post_id'), function (data, textStatus) {
                $post.remove();
            });
            return false;
        });
    </script>
<?php
footer();
