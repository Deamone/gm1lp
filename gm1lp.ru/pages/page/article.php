<?php
$route = str_replace('/', '', substr($route, 8));
//SELECT `c`.`url`,`lt`.`text` FROM `category` as `c` INNER JOIN `lang-text` as `lt` ON `c`.`id`=`lt`.`id` WHERE `lt`.`col`='text' AND `lt`.`table`='category' AND LENGTH(`lt`.`text`)>100 AND `c`.`url`<=0
$article = getArticle($route);
if (!$article) {
    redirect('/');
}

header_($article['title'], '', '', '');
?>
<div class="container-menu-bottom"></div>
<!-- END container-menu -->	
<div class="page-my-diary-top-side">
    <div><?php echo $article['title']; ?></div><a href="/">Моя страница</a>
</div><!-- page-top-side -->

<div id="content" class="my-diary-page">
    <div class="content">
        <div class="head"></div>
        <div class="center">  

            <div class="contentside">
                <div class="diary-post">
                    <h3><?php echo $article['title']; ?></h3>
                    <?php echo $article['text']; ?>


                    <!--div class="post-meta">
                        <div class="date">20.04.2014</div>

                        <div class="clearfix"></div><!-- .clearfix ->
                    </div-->
                    <div class="post-meta">

                        <div class="clearfix"></div><!-- .clearfix -->
                    </div>
                </div><!-- diary-post -->
                <div class="pagination">

                </div><!-- pagination -->

            </div>
            <div class="clearfix"></div><!-- .clearfix -->
        </div>
        <div class="clearfix"></div><!-- .clearfix -->
    </div>
    <div class="bottom"></div>




    <div class="clearfix"></div><!-- .clearfix -->

</div>
</div><!-- END CONTAINER -->


<script>
    $(document).ready(function() {

    });
</script>
<?php
footer();
