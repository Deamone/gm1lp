<?php
$mess = '';

if (isset($_POST['login']) && !empty($_POST['login'])) {
    $user = getUserByEmail($_POST['login']);
    if (!$user) {
        $mess = 'Пользователь не найден';
    } else {
        $pass = substr($user['password'], 25);
        $mess = 'Ссылка для восстановления отправлена на Вашу почту, если письмо не приходит, попробуйте проверить папку "Спам".';

        mail($user['login'], 'Восстановление пароля GM1LP', 'Ваш новый пароль: ' . $pass . ' . Для установки нового пароля перейдите по ссылке:'
                . 'http://gm1lp.ru/page/new_pass?uid=' . $user['id'] . '&hash=' . md5($user['password']) . '

Данные действуют для всех сервисов системы GM1LP.
-
Никому и никогда не сообщайте свой пароль.');
    }
} elseif (isset($_GET['uid']) && isset($_GET['hash'])) {
    $user = getUserById($_GET['uid']);
    if (!$user) {
        $mess = 'Произошла ошибка. Возможно ссылка устарела';
    } elseif (md5($user['password']) != $_GET['hash']) {
        $mess = 'Произошла ошибка. Возможно ссылка устарела';
    } else {
        $pass = substr($user['password'], 25);
        updateUserById($_GET['uid'], array('password'), array(md5($pass)));
        $mess = 'Новый пароль успешно установлен! <a href="/">Войти</a>';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Смена пароля</title>
        <meta http-equiv="Content-Type" content="text/html; charset=Windows-1251">
        <script type="text/javascript" src="/application/views/site/js/jq.js"></script>
        <script type="text/javascript" src="/application/views/site/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jcarousellite.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jquery.mousewheel.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script src="/application/views/site/js/common.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/style.css" />
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/fonts.css" />
    </head>
    <body>
        <div id="autority" class="modal-win">
            <div class="modal-win-cont">
                <h2 class="modal-win-title">Сброс пароля</h2>
                <hr>
                <form method="post" action="/page/new_pass" class="auth-l">
                    <div class="placeholder">
                        <label for="change-pass-email" class="text">Укажите E-mail</label>
                        <input type="text" name="login" id="change-pass-email" class="textField">
                    </div>
                    <input type="submit" class="change-pass" value="Сброс пароля">
                    <a href="/" class="back">Вернуться</a>
                </form>
                <div class="auth-r">
                    <!--h2 class="rs-title">Вы также можете войти через социальные сети</h2-->
                    <p class="rs-advice"><?php echo $mess; ?></p>
                </div>
                <div class="clear"></div>
                <hr>
                <p class="modal-win-bottom">
                    <a href="/page/register">Не зарегестрированы?</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="/page/register">Регистрируйся прямо сейчас!</a>
                </p>
            </div>
        </div>
    </body>
</html>