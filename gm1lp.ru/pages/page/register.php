<?php
$status = 0;
$message = '';
if (isset($_POST['register'])) {
    $email = query_escape($_POST['email']);
    $fam = query_escape(ucfirst(strtolower($_POST['last_name'])));
    $name = query_escape(ucfirst(strtolower($_POST['first_name'])));
    $password = $_POST['password'];
    $repassword = $_POST['password2'];

    $error = "";
    if (empty($email) || !preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $email)) {
        $error .= $_LANG['REG_INVALID_EMAIL'];
    } else {
        $q = query("SELECT `id` FROM `new_1lp`.`users` WHERE `login`='$email';");
        if ($q['num_rows']) {
            $error .= $_LANG['REG_INVALID_EMAIL_2'];
        }
    }
    if (empty($fam) || !preg_match("/^[А-Яа-я\-]+/i", $fam)) {
        $error .= $_LANG['REG_INVALID_LASTNAME'];
    }
    if (empty($name) || !preg_match("/^[А-Яа-я\-]+/i", $name)) {
        $error .= $_LANG['REG_INVALID_FIRSTNAME'];
    }
    if (empty($password) || empty($repassword) || $password != $repassword) {
        $error .= $_LANG['REG_INVALID_PASSWORD'];
    }

    if ($error) {
        $message = $error;
    } else {
        $referal = 0;
        if (isset($_COOKIE['referal'])) {
            $referal = (int) $_COOKIE['referal'];
            moneyForRef($referal);
        }
        query("INSERT INTO `new_1lp`.`users` (`name`,`fam`,`password`,`login`,`activate`,`created`,`referal`) VALUES('$name','$fam','" . md5($password) . "','$email','1','" . time() . "','$referal');");
        $uid = query_lastInsertId();
        updateUserById($uid, array('url'), array('id' . $uid));
        $status = 1;
        $message = $_LANG['REG_DONE'];
        mail($email, 'Спасибо за регистрацию', 'Благодарим Вас за регистрацию в системе GM1LP.

Ваш логин: ' . $email . '
Ваш пароль: ' . $password . '

Данные действуют для всех сервисов системы GM1LP.
-
Никому и никогда не сообщайте свой пароль.');
        /* addPost($uid, 'Уважаемый пользователь, благодарим Вас за регистрацию в системе GM!
          Пожалуйста, обратите внимание на то, что администраторы и модераторы системы GM, никогда и ни при каких обстоятельствах не просят ваш логин и пароль от учетной записи, а так ни при каких обстоятельствах не просят перевести им средства!
          На данный момент главный сайт системы GM, на котором Вы сейчас находитесь – проходит полное изменение, для улучшения работы всей системы и ее скорости. На данный момент по этой причине, некоторые ссылки и возможности на главном сайте системы GM, могут не работать, но скоро мы это исправим. Остальные сайты системы, работают в полном объеме и уже сейчас Вы можете ими воспользоваться. Для этого нажмите на кнопку «Мир GM», которую Вы можете увидеть в верхнем меню, справа.
          Бесплатный ПРО Аккаунт на 1 год Вы можете активировать совершенно бесплатно, здесь: www.gm1lp.ru/veripro
          www.goodmoneys.ru – Вы можете запускать различные задания, как срочные, так и не очень. Сервис поможет Вам быстро найти нужного исполнителя, а так же Вы сами можете стать исполнителем и зарабатывать на этом. Так же если Вы занимаетесь разработкой приложений для мобильных устройств, Вы так же можете запустить задание, целью которого будут скачивание вашего приложения, а так же его оценка в рейтинге и положительные отзывы для выхода в ТОП скачиваемых приложений.
          www.gmcard.ru – Вы можете запускать различные скидочные акции за пару минут, привлекая тем самым новых потенциальных клиентов. Сам сайт является полностью автоматизированным сайтом-купонатором.
          www.rightlike.ru – Автоматизированный сайт для компаний, которые могут проводить на нем рекламные акции, путем розыгрыша какого-нибудь подарка. В этом случае пользователю нужно сделать ряд условий, которые задает компания. Это могут быть вступления в паблики в социальных сетях, а так же репосты и т.д. (Просто накрутка участников строго запрещена).
          Остальные сервисы системы GM, пока находятся в доработке, но они обязательно скоро станут Вам доступны в меню «Мир GM».
          При пополнении счета от 500 руб., Вас ждет подарок – мы установим на Ваш сайт SSL протокол на 3 месяца - совершенно бесплатно. Сайты, где установлен SSL, вызывает большее доверие потенциальных клиентов и говорит о высокой репутации компании.
          С правилами системы Вы можете ознакомиться здесь: www.gm1lp.ru/privacy
          С условиями пользования системы GM, здесь: www.gm1lp.ru/terms
          Узнать о всех сервисах GM, об их возможностях и функционале, Вы можете прочитать здесь: www.gm1lp.ru /answers
          С уважением, команда GM.', 1); */
    }
    if (isAjax()) {
        echo $status . '<!>' . $message;
        exit;
    }
} else {
    $email = '';
    $fam = '';
    $name = '';
}
if (isset($_COOKIE['referer'])) {
    $referer = $_COOKIE['referer'];
} else {
    $referer = '/';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Регистрация</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/style.css" />
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/fonts.css" />
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/lightbox.css" />
        <!--<script type="text/javascript" src="/application/views/site/js/jq.js"></script>-->
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="/application/views/site/js/lightbox-2.6.min.js"></script>
        <script type="text/javascript" src="/application/views/site/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jcarousellite.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jquery.mousewheel.min.js"></script>
        <script src="/application/views/site/js/common.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
            var referer = '<?php echo $referer; ?>';
        </script>
    </head>
    <body>
        <div id="register">
            <img src="/application/views/site/img/reg-logo.png" alt="" class="reg-logo">
            <h2>Один аккаунт. Весь мир GM!</h2>
            <h3>Один аккаунт для всех сервисов GM</h3>
            <div class="servises">
                <img src="/application/views/site/img/smlogo1.png" alt="">
                <img src="/application/views/site/img/smlogo2.png" alt="">
                <img src="/application/views/site/img/smlogo3.png" alt="">
                <img src="/application/views/site/img/smlogo4.png" alt="">
                <img src="/application/views/site/img/smlogo5.png" alt="">
                <img src="/application/views/site/img/smlogo6.png" alt="">
                <img src="/application/views/site/img/smlogo7.png" alt="">
                <img src="/application/views/site/img/smlogo8.png" alt="">
                <img src="/application/views/site/img/smlogo9.png" alt="">
                <img src="/application/views/site/img/smlogo10.png" alt="">
                <img src="/application/views/site/img/smlogo11.png" alt="">
            </div>
            <!--h2>Контролируйте всё!</h2-->
            <!--h4>Зарегистрируйся сейчас и участвуй в розыгрыше крутых подарков от GM!</h4-->
            <div id="autority" class="modal-win">
                <div class="modal-win-cont">
                    <h2 class="modal-win-title">Регистрация</h2>
                    <hr>
                    <form  action="/page/register" method="post" class="registration" id="reg-l">
                        <div class="reg-l">
                            <div class="placeholder">
                                <label for="reg-email" class="text">Укажите E-mail...</label>
                                <input type="text" name="email" id="reg-email" class="textField" value="<?php echo $email; ?>">
                            </div>
                            <div class="placeholder">
                                <label for="reg-surname" class="text">Укажите фамилию</label>
                                <input type="text" name="last_name" id="reg-surname" class="textField" value="<?php echo $fam; ?>">
                            </div>
                            <div class="placeholder">
                                <label for="reg-name" class="text">Укажите имя</label>
                                <input type="text" name="first_name" id="reg-name" class="textField" value="<?php echo $name; ?>">
                            </div>
                            <div class="placeholder">
                                <label for="reg-pass" class="text">Укажите пароль...</label>
                                <input type="password" name="password" id="reg-pass" class="textField">
                            </div>
                            <div class="placeholder">
                                <label for="reg-rpass" class="text">Повторите пароль...</label>
                                <input type="password" name="password2" id="reg-rpass" class="textField">
                            </div>
                        </div>
                        <div class="reg-r">
                            <h2 class="rs-title"></h2>
                            <p class="rs-advice" id="reg-status"><?php echo $message; ?></p>

                            <!--div style="font-weight: bold">
                                <a href="/auth/vk">Регистрация через <img src="/img/vk.png" alt="Вконтакте" width="150" title="Вконтакте" style="padding-left: 5px;"/></a>
                            </div-->
                        </div>
                        <div class="clear"></div>
                        <hr>
                        <p class="send-reg">
                            <input type="hidden" name="register" value="1">
                            <input type="submit" name="register" class="reg" id="reg-enter" value="Регистрация">
                            <a href="/" class="cancel">Отменить</a>
                        </p>
                    </form>
                </div>
            </div>
            <!--h2>Бросьте вызов всему!</h2>

            <p class="italic">
                Зарегистрируйся сейчас и получи Премиум Аккаунт на 1 год - бесплатно!
                <br>Регистрируйся и забирай его уже сейчас!
                <br>Количество ограничено!
                <br>Всего 1 000 000!
            </p-->
            <div class="ind-rep">
                <div class="rep-inner">
                    <img src="/application/views/site/images/rep-ins.jpg"/>
                </div>
            </div>
            <h2>Удобный и мобильный GM!</h2>
            <img src="/application/views/site/images/mobil.png" alt="" class="mobil-gm">
        </div>



        <div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div>
        <script src="/application/views/site/js/nx.js"></script>
    </body>
</html>
