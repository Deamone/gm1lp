<?php
$gmpro = array(
    1 => 10,
    3 => 25,
    7 => 50,
    30 => 150,
    90 => 350,
    180 => 550,
    365 => 800
);
if ($_GET['ver'] == 1 && $USER['money'] >= 50 && $USER['level'] == 0) {
    $keys = array('money', 'level');
    $values = array($USER['money'] - 50, '1');
    updateUserById($USER['id'], $keys, $values);
    /** Неведомая хрень **
      if ($USER['referal']>0)
      {
      $ref = new table('users',$user->referal);
      if ($ref->level==0) $ref+=100;
      if ($ref->level==1) $ref+=300;
      if ($ref->level==2 && $ref->pro_end>time() ) $ref+=600;
      }* * */
    redirect('/' . $USER['url']);
} elseif ($_GET['gm'] > 0 && $USER['money'] >= $gmpro[$_GET['gm']] && $gmpro[$_GET['gm']] > 0) {

    $keys = array('money', 'level', 'pro_end');
    if ($USER['pro_end'] < time()) {
        $USER['pro_end'] = time();
    }
    $values = array($USER['money'] - $gmpro[$_GET['gm']], '2', $USER['pro_end'] + $_GET['gm'] * 24 * 3600);

    updateUserById($USER['id'], $keys, $values);
    /** Неведомая хрень **
      if ($user->referal > 0) {
      $ref = new table('users', $user->referal);
      if ($ref->level == 0)
      $ref+=100;
      if ($ref->level == 1)
      $ref+=500;
      if ($ref->level == 2 && $ref->pro_end > time())
      $ref+=800;
      }* */
    redirect('/' . $USER['url']);
}
if ($USER['level'] == 1 && isset($_GET['getPro'])) {
    if ($USER['pro_end'] < time()) {
        $USER['pro_end'] = time();
    }
    updateUserById($USER['id'], array('level', 'pro_end'), array('2', $USER['pro_end'] + 365 * 24 * 3600));
    $USER['level'] = 2;
    $USER['pro_end'] = 365 * 24 * 3600;
}


header_('Верификация и ПРО', '', '', '<script type="text/javascript" src="/application/views/site/js/nx.js"></script>
');
?>
<div class="container-menu-bottom"></div>
<!-- END container-menu --> 
<div class="page-my-information-top-side">
    <div>Отличия статусов</div>
    <!--<a href="#">Моя страница</a>-->
</div><!-- page-top-side -->

<div id="content" class="status-dif">
    <div class="green-panel">
        <p>На данный момент все сервисы системы GM работают абсолютно бесплатно.</p>
        <p>Альфа-тестирование объявляется открытым!</p>
        <p>ПРО Аккаунты на 1 год так же раздаются бесплатно, их количество ограничено.</p>
        <p>Перейти на другие сервисы системы GM можно через кнопку &laquo;Мир GM&raquo;, которая находится в верхнем меню справа.</p>
    </div>
    <div class="green-panel">
        <p>Верификация платная, единоразовая - 50 руб.</p>
        <p>После получения и привязки к аккаунту банковской карты "Good Moneys Банк Gold", средства будут возвращены на карту.</p>
        <p>Так же всем пользователям, кто пополнил баланс от 500 руб., установка SSL (Протокол HTTPS) на 3 месяца в подарок. Внесенные средства в дальнейшем необходимо будет потратить.</p>
    </div>
    На сайте присутствует 3 типа пользователей, а именно:
    <br>
    <p>
        <strong>-Простой пользователь</strong>, который зарегистрировался на сайте, ему доступны все сервисы системы, но их возможности ограничены.
    </p>
    <p>
        <strong>-ВЕР</strong> (Верифицированный пользователь)
        Данному пользователю предоставляется больше возможностей, т. к. верификация подразумевает доверие, но все же возможности этого пользователя не безграничны. Верификация платная, но в любом случае, при подключении карты "Good Moneys Банк Gold" средства за верификацию будут возвращены назад. Платная верификация связана с тем, чтобы оградить систему от фейковых аккаунтов.
    </p>
    <p>
        <strong>-ПРО</strong> (Профессиональный пользователь, который использует систему максимально возможно).
        К данному пользователю максимальное доверие, а значит и возможности системы ему предоставлены максимальные.
    </p>
    <div class="statuses">
        <div class="stat-it">
            <h2><strong>Простой</strong>пользователь</h2>
            <img src="/application/views/site/img/simple.png" class="margin20" alt="">
            <ul>
                <li>Начисление “Спасибо”:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Ввод и вывод средств:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Участие в конкурсах:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Участие в скидках:<span>90 дней</span></li>
                <li>Карта GM Card World:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>Карта Good Moneys Банк:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>Заявки на кредит<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>Поиск инвистиций:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Поиск наставника:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Инвестирование:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>Наставничество:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>Другие проекты GM:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Работа в компании GM:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>Оффлайн задания:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>Лимит заданий в сутки:<span>5</span></li>
                <li class="margin20"><span class="max-price-text">Максимальная оплата за 1 задания на GoodMoneys:</span>
                    <span class="max-price-val">20<span class="max-price-cur">GM</span></span>
                </li>
                <li>Скидки системы GM:</li>
                <li>-на запуск заданий:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>-на запуск конкурсов:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>-на запуск в скидках:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li class="margin20">-по GM Card World: <span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>“Спасибо от GM”:<span></li>
                <li>-х5 больше “Спасибо”<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li class="margin20">-х15 больше “Спасибо”<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li class="margin20">Покупка “Спасибо от GM”:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>GM перевод друзьям:</li>
                <li>-перевод “Спасибо от GM”:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>-перевод средств <span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
            </ul>
        </div>
        <div class="stat-it">
            <h2><strong>Верифицированный</strong> пользователь</h2>
            <img src="/application/views/site/img/ver-us.png" class="margin20" alt="">
            <ul>
                <li>Начисление “Спасибо”:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Ввод и вывод средств:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Участие в конкурсах:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Участие в скидках:<span>90 дней</span></li>
                <li>Карта GM Card World:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Карта Good Moneys Банк:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Заявки на кредит<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Поиск инвистиций:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Поиск наставника:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Инвестирование:<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li>Наставничество:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Другие проекты GM:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Работа в компании GM:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Оффлайн задания:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Лимит заданий в сутки:<span>50</span></li>
                <li class="margin20"><span class="max-price-text">Максимальная оплата за 1 задания на GoodMoneys:</span>
                    <span class="max-price-val">200<span class="max-price-cur">GM</span></span>
                </li>
                <li>Скидки системы GM:</li>
                <li>-на запуск заданий:<span>-20%</span></li>
                <li>-на запуск конкурсов:<span>-20%</span></li>
                <li>-на запуск в скидках:<span>-20%</span></li>
                <li class="margin20">-по GM Card World: <span>-20%</span></li>
                <li>“Спасибо от GM”:<span></li>
                <li>-х5 больше “Спасибо”<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li class="margin20">-х15 больше “Спасибо”<span><img src="/application/views/site/img/rcross.png" alt=""></span></li>
                <li class="margin20">Покупка “Спасибо от GM”:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>GM перевод друзьям:</li>
                <li>-перевод “Спасибо от GM”:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>-перевод средств <span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <?php
                if (!$USER['level']) {
                    echo '<a href="#modal-win-1" class="open-modal">Получить</a>';
                } else {
                    echo '<a href="#" style="font-size: 13px;background: gray;border-radius: 5px;padding-top: 2px;" onclick="return false;">Пройдена</a>';
                }
                ?>
            </ul>
        </div>
        <div class="stat-it">
            <h2><strong>Про</strong> пользователь</h2>
            <img src="/application/views/site/img/pro-us.png" class="margin20" alt="">
            <ul>
                <li>Начисление “Спасибо”:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Ввод и вывод средств:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Участие в конкурсах:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Участие в скидках:<span>90 дней</span></li>
                <li>Карта GM Card World:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Карта Good Moneys Банк:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Заявки на кредит<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Поиск инвистиций:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Поиск наставника:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Инвестирование:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Наставничество:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Другие проекты GM:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Работа в компании GM:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Оффлайн задания:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>Лимит заданий в сутки:<span>-</span></li>
                <li class="margin20"><span class="max-price-text">Максимальная оплата за 1 задания на GoodMoneys:</span><span class="max-price-inf">&infin;</span></li>

                <li>Скидки системы GM:</li>
                <li>-на запуск заданий:<span>-25%</span></li>
                <li>-на запуск конкурсов:<span>-25%</span></li>
                <li>-на запуск в скидках:<span>-25%</span></li>
                <li class="margin20">-по GM Card World: <span>-50%</span></li>
                <li>“Спасибо от GM”:<span></li>
                <li>-х5 больше “Спасибо”<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li class="margin20">-х15 больше “Спасибо”<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li class="margin20">Покупка “Спасибо от GM”:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>GM перевод друзьям:</li>
                <li>-перевод “Спасибо от GM”:<span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <li>-перевод средств <span><img src="/application/views/site/img/ggalka.png" alt=""></span></li>
                <?php
                if (!$USER['level']) {
                    echo '<a href="#" style="font-size: 13px;background: gray;border-radius: 5px;padding-top: 2px;" onclick="return false;">Необходима верификация</a>'
                    . '<li style="text-align: center;background: yellow;padding: 3px;margin-top:10px;">Пройдите верификацию уже сейчас и получите ПРО Аккаунт на 1 год совершенно бесплатно. Количество бесплатных пакетов ПРО Аккаунтов ограничено.</li>';
                } elseif ($USER['level'] == 2) {
                    echo '<a href="#modal-win-2" class="open-modal">получить</a>';
                } elseif ($USER['level'] < 2 || $USER['pro_end'] <= time()) {
                    echo '<a href="?getPro" style="font-size: 13px;background: green;border-radius: 5px;padding-top: 2px;">активировать на 1 год бесплатно</a>'
                    . '<li style="text-align: center;background: yellow;padding: 3px;margin-top:10px;">Пройдите верификацию уже сейчас и получите ПРО Аккаунт на 1 год совершенно бесплатно. Количество бесплатных пакетов ПРО Аккаунтов ограничено.</li>';
                } else {
                    echo '<a href="#" style="font-size: 13px;background: gray;border-radius: 5px;padding-top: 2px;" onclick="return false;">Получен</a>'
                    . '<li style="text-align: center;background: yellow;padding: 3px;margin-top:10px;">Подарочный ПРО Аккаунт на 1 год бесплатно был успешно активирован ранее.</li>';
                }
                ?>

            </ul>
        </div>
    </div>
    <h3>
        Подробнее:
    </h3>
    <p>GM - внутренняя валюта, 1 GM равен 1 рублю</p>
    <table class="dif">
        <tr>
            <td>Стоимость статуса<br>"Верифицрованный"</td>
            <td>Условия получения:</td>
            <td>Стоимость статуса "ПРО"</td>
            <td>Условия получения:</td>
        </tr>
        <tr>
            <td>50 GM <br>(выдается навсегда)</td>
            <td>
                100% заполненный профиль:
                <br>-Имя и Фамилия
                <br>-Аватара
                <br>-Наличие 10 друзей
                <br>-Почта
            </td>
            <td>
                1 день: 10 GM
                <br>3 дня: 25 GM
                <br>7 дней: 50 GM
                <br>1 месяц:  150 GM
                <br>3 месяца: 350 GM
                <br>6 месяцев: 550 GM
                <br>12 месяцев 800 GM
            </td>
            <td>
                Статус верифицированного 
                <br>пользователя
                <br>
                <br>100% заполненный профиль:
                <br>-Имя и Фамилия
                <br>-Аватара
                <br>-Наличие 10 друзей
                <br>-Почта
            </td>
        </tr>
    </table>
    <div class="bordered">
        <hr>
        <h4>Стоимость карты GM Card World: 100 GM</h4>
        <p>
            (кроме красивых и уникальных номеров)
            <br>О преимуществах карты вы можете узнать, посмотрев различия между статусами выше 
        </p>
        <div class="center">
            <img src="/application/views/site/img/cards.png" alt="">
            <span>
                <strong>Карта GM Card World Gold</strong>
                Красивые и уникальные номера
            </span>
         <!--   <span><a href="http://gm1lp.ru/page/art/gmcards" target="_blank">Подробнее...</a></span> -->
            <a class="href-button-green" href="http://gm1lp.ru/page/art/gmcards" target="_blank">                    
                <span class="button-green">
                    Подробнее
                </span>
            </a>
        </div>
    </div>
    <div class="bordered">
        <hr>
        <h4>Покупка “Спасибо от GM”</h4>
        <p>
            “Спасибо от GM” можно купить: 1GM - 1 “Спасибо от GM”
        </p>
        <h4>Бесплатное начисление “Спасибо от GM”</h4>
        <div class="half-sized">
            <table class="spaced">
                <tr>
                    <td>Действие</td>
                    <td>Количество 
                        “Спасибо от GM”</td>
                    <td>При х5 </td>
                    <td>При х5 и х15 
                        (х20) </td>
                </tr>
                <tr>
                    <td>Вход на любой сайт GM: <br>(Действует 1 раз в сутки)</td>
                    <td>1</td>
                    <td>5</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>Прошли верификацию</td>
                    <td>1</td>
                    <td>5</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>Провели друга по своей <br>реферальной ссылке:</td>
                    <td>1</td>
                    <td>5</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>Ваш друг прошел верификацию:</td>
                    <td>2</td>
                    <td>10</td>
                    <td>40</td>
                </tr>
                <tr>
                    <td>Выполнили GM задание: <br>Online/Offline</td>
                    <td>2</td>
                    <td>10</td>
                    <td>40</td>
                </tr>
                <tr>
                    <td>Приняли участие в конкурсе: <br>Right like</td>
                    <td>2</td>
                    <td>10</td>
                    <td>40</td>
                </tr>
                <tr>
                    <td>Приобрели GM Card World</td>
                    <td>5</td>
                    <td>25</td>
                    <td>100</td>
                </tr>
            </table>
            <p>
                Количество поощрений со временем будет увеличено.
            </p>
            <!--	 <a href="http://gm1lp.ru/page/art/thanksgm">Подробнее...</a>-->
            <a class="href-button-green" href="http://gm1lp.ru/page/art/thanksgm" target="_blank">                    
                <span class="button-green">
                    Подробнее
                </span>
            </a>
        </div>
    </div>
    <div class="bordered">
        <hr>
        <h4>Куда потратить “Спасибо от GM” </h4>

        <div class="clearfix"></div><!-- .clearfix -->

        <table id="concurs">
            <tr>
                <td colspan="6">
                    <h2 class="page-title">
                        Запуск конкурсов на Right Like
                        <a href="http://www.rightlike.ru" target="_blank">www.rightlike.ru</a>
                    </h2>
                    <h3 class="small-title">
                        Стоимость запуска простого конкурса 
                        <br>на 1 день 
                    </h3>
                </td>
            </tr>   
            <tr class="top">
                <td>

                </td>
                <td>
                    <div class="img">
                        <img src="/application/views/site/img/av-reg.png">
                    </div>
                    <p>
                        Простой
                        <br>пользователь
                    </p>    
                </td>
                <td>
                    <div class="img">
                        <img src="/application/views/site/img/av-ver.png">
                    </div>
                    <p>
                        Верифицированный
                        <br>пользователь
                    </p>
                </td>
                <td>
                    <div class="img">
                        <img src="/application/views/site/img/av-pro.png">
                    </div>
                    <p>
                        ПРО пользователь
                    </p>
                </td>
                <td>
                    <div class="img">
                        <img src="/application/views/site/img/av-ver-card.png">
                    </div>
                    <p>
                        Верифицированный +
                        <br>GM Card World
                    </p>
                </td>
                <td>
                    <div class="img">
                        <img src="/application/views/site/img/av-pro-card.png">
                    </div>
                    <p>
                        ПРО +
                        <br>GM Card World
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        В валюте GM
                    </p>
                    <img src="/application/views/site/img/gm-coins.png">
                </td>
                <td>
                    50
                </td>
                <td>
                    40
                </td>
                <td>
                    37,5
                </td>
                <td>
                    30
                </td>
                <td>
                    12,5
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        В "Спасибо от GM"
                    </p>
                    <img src="/application/views/site/img/gm-thanks.png">
                </td>
                <td>
                    2000
                </td>
                <td>
                    1000
                </td>
                <td>
                    500
                </td>
                <td>
                    200
                </td>
                <td>
                    75
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <h2 class="page-title">
                        Запуск конкурсов на  GM Card
                        <a href="http://www.gmcard.ru" target="_blank">www.gmcard.ru</a>
                    </h2>
                    <h3 class="small-title">
                        Стоимость запуска конкурса в баннере (шапке сайта) 
                        <br>на 1 день 
                    </h3>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        В валюте GM
                    </p>
                    <img src="/application/views/site/img/gm-coins.png">
                </td>
                <td>
                    150
                </td>
                <td>
                    120
                </td>
                <td>
                    112,5
                </td>
                <td>
                    90
                </td>
                <td>
                    37,5
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        В "Спасибо от GM"
                    </p>
                    <img src="/application/views/site/img/gm-thanks.png">
                </td>
                <td>
                    6000
                </td>
                <td>
                    3000
                </td>
                <td>
                    1500
                </td>
                <td>
                    600
                </td>
                <td>
                    250
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <h3 class="small-title">
                        Стоимость запуска простой акции 
                        <br>на 1 день 
                    </h3>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        В валюте GM
                    </p>
                    <img src="/application/views/site/img/gm-coins.png">
                </td>
                <td>
                    6
                </td>
                <td>
                    4,8
                </td>
                <td>
                    4,5
                </td>
                <td>
                    3,6
                </td>
                <td>
                    1,5
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        В "Спасибо от GM"
                    </p>
                    <img src="/application/views/site/img/gm-thanks.png">
                </td>
                <td>
                    1000
                </td>
                <td>
                    500
                </td>
                <td>
                    250
                </td>
                <td>
                    100
                </td>
                <td>
                    33
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <h3 class="small-title">
                        Стоимость запуска акции в баннере (шапке сайта) 
                        <br>на 1 день 
                    </h3>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        В валюте GM
                    </p>
                    <img src="/application/views/site/img/gm-coins.png">
                </td>
                <td>
                    18
                </td>
                <td>
                    14,4
                </td>
                <td>
                    13,5
                </td>
                <td>
                    10,8
                </td>
                <td>
                    4,8
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        В "Спасибо от GM"
                    </p>
                    <img src="/application/views/site/img/gm-thanks.png">
                </td>
                <td>
                    3000
                </td>
                <td>
                    1500
                </td>
                <td>
                    750
                </td>
                <td>
                    300
                </td>
                <td>
                    99
                </td>
            </tr>
        </table>
        <div id="concurs-desc">
            В <strong>GM заданиях</strong>  <a href="http://www.goodmoneys.ru" target="_blank">www.goodmoneys.ru</a> система скидок не требуется, так как оплата производится
            <br>непосредственно исполнителю. Никаких дополнительных комиссий на сайте нет, но стоит
            <br>учесть то, что если вы запускаете сложную задачу, но при этом назначаете низкую оплату,
            <br>то пользователи скорее всего просто проигнорируют вашу задачу.
            <br>
            <br>Уважайте друг друга!  
        </div>   

        <div class="clearfix"></div><!-- .clearfix -->



    </div>
    <div class="bordered">
        <hr>
        <h4>Работа в компании ООО “ДжиЭм”:</h4>
        <div class="half-sized">
            <img src="/application/views/site/img/gm-us.png" alt="" class="right">
            <br>В команду GM требуются:
            <br>-старшие администраторы
            <br>-администраторы
            <br>-модераторы
            <br>-сотрудники технической поддержки
            <br>
            <br> Работа в режиме online.
            <br>Зарплата от 55 тыс. рублей в мес.
            <br>
            <br>
            <br>
            <!--      <a href="http://gm1lp.ru/page/art/jobs">Подробнее...</a> -->
            <a class="href-button-green" href="http://gm1lp.ru/page/art/jobs" target="_blank">                    
                <span class="button-green">
                    Подробнее
                </span>
            </a>
        </div>
    </div>
    <div class="bordered">
        <hr>
        <h4>Вывод заработанных средств:</h4>
        <p>
            Вывод средств происходит в течении 72 часов через систему автовыплат, только на WebMoney/
        </p>
        <p>
            При наличии карты Good Moneys Банк, средства можно выводить на карту и будут доступны для снятия 
            в любом банкомате России, а также и всего мира. 
        </p>
        <div class="center">
            <img src="/application/views/site/img/cards-2.png" alt="">
            <span>
                <strong>Карта Good Moneys Банк Gold:</strong>
                Кредиты/ Вклады/ Вывод средств
            </span>
            <a class="href-button-green" href="http://gm1lp.ru/page/art/gmcards" target="_blank">                    
                <span class="button-green">
                    Подробнее
                </span>
            </a>
        </div>

    </div><!-- END CONTAINER -->





    <div class="clearfix"></div><!-- .clearfix -->

</div>

</div><!-- END CONTAINER -->


<script>
    $(document).ready(function() {

    });
</script>
<?php
footer();
