<?php
$i = true;
$_USER = $USER;
#$text = new Memcache;
#$text->connect("localhost", 11201);
if (isset($_GET['del_link_vk']) && $USER['vk_uid']) {
    updateUserById($USER['id'], array('vk_uid'), array(''));
    $USER['vk_uid'] = $_USER['vk_uid'] = '';
}
///
$error = '';
if (count($_POST) && isAjax()) {
    $keys = array('name', 'fam', 'gender', 'region', 'relation', 'tel', 'skype', 'site', 'vuz', 'scool', 'muz', 'films', 'books', 'games', 'sports', 'kul', 'cit');
    $values = array($_POST['name'], $_POST['fam'], $_POST['gender'], $_POST['region'], $_POST['relation'], $_POST['tel'], $_POST['skype'], $_POST['site'], $_POST['vuz'], $_POST['scool'], $_POST['muz'], $_POST['films'], $_POST['books'], $_POST['games'], $_POST['sports'], $_POST['kul'], $_POST['cit']);
    if ($USER['url'] != $_POST['url']) {
        if (getUserByUrl($_POST['url'])) {
            $error .= 'alert("Адрес уже занят другим пользователем");';
        }
    }
    $e = explode('.', $_POST['date']);
    if (count($e) != 3) {
        $error .= 'Дата рождения указана не верно. Пример: 21.06.1990';
    } elseif (!checkdate($e[1], $e[0], $e[2])) {
        $error .= 'Дата рождения указана не верно. Пример: 21.06.1990';
    } else {
        $e[0] = (int) $e[0];
        $e[1] = (int) $e[1];
        $e[2] = (int) $e[2];
        $keys[] = 'date';
        $values[] = implode('.', $e);
    }
    updateUserById($USER['id'], $keys, $values);
    //$USER = getUserById($USER['id']);
    if ($error) {
        exit('alert("' . str_replace('"', "'", $error) . '");');
    } else {
        $keys[] = 'url';
        $values[] = $_POST['url'];
    }
    exit('alert("Данные успешно сохранены");');
}
header_('Настройки', '', '', ''
        . '<script type="text/javascript" src="/application/views/site/js/jquery.fileupload.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jquery.fileupload-process.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jquery.fileupload-video.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jquery.iframe-transport.js"></script>
        <script type="text/javascript" src="/application/views/site/js/nx.js"></script>
');
?>

<!--main-->
<div id="main" class="main_block centred">
    <div class="left_block">
        <form>
            <div class="div_left">
                <h1 class="prof_h1">Профиль</h1>
                <!--1-->
                <div class="prof_blok_left">
                    <h2>Коротко о главном</h2>
                    <div class="prof_blok_pukt prof_blok_pukt_left">
                        <p>День рождения</p>
                        <div class="styled-select styled-select_1">
                            <select>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>
                        <div class="styled-select styled-select_2">
                            <select>
                                <option value="Январь">Январь</option>
                                <option value="Февраль">Февраль</option>
                                <option value="Март">Март</option>
                            </select>
                        </div>
                        <div class="styled-select styled-select_3">
                            <select>
                                <option value="1990">1990</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                            </select>
                        </div>
                    </div>
                    <div class="prof_blok_pukt">
                        <p>Семейное положение</p>
                        <div class="styled-select">
                            <select>
                                <option value="Холост">Холост</option>
                                <option value="Женат">Женат</option>
                                <option value="Замужем">Замужем</option>
                            </select>
                        </div>
                    </div>
                    <div class="prof_blok_pukt prof_blok_pukt_left">
                        <p>Место работы</p>
                        <input placeholder="Введите место" type="text">
                    </div>
                    <div class="prof_blok_pukt">
                        <p>Должность</p>
                        <input  placeholder="Введите должность" type="text">
                    </div>
                    <div class="prof_blok_pukt prof_blok_pukt_left">
                        <p>Опыт работы на текущей должности</p>
                        <input placeholder="1 год" type="text">
                    </div>
                    <div class="prof_blok_pukt">
                        <p>Общий стаж работы</p>
                        <div class="styled-select">
                            <select>
                                <option value="1 год">1 год</option>
                                <option value="2 года">2 года</option>
                                <option value="3 года">3 года</option>
                                <option value="4 года">4 года</option>
                                <option value="5 лет">5 лет</option>
                            </select>
                        </div>
                    </div>
                </div>

                <!--2-->
                <div class="prof_blok_left">
                    <h2>Контактные данные</h2>
                    <div class="prof_blok_pukt prof_blok_pukt_left">
                        <p>Страна</p>
                        <div class="styled-select">
                            <select>
                                <option value="Россия">Россия</option>
                                <option value="Украина">Украина</option>
                            </select>
                        </div>
                    </div>
                    <div class="prof_blok_pukt">
                        <p>Город</p>
                        <div class="styled-select">
                            <select>
                                <option value="Москва">Москва</option>
                                <option value="Барнаул">Барнаул</option>
                            </select>
                        </div>
                    </div>
                    <div class="prof_blok_pukt prof_blok_pukt_left">
                        <p>Мобильный телефон</p>
                        <input placeholder="Ваш телефон" type="text">
                    </div>
                    <div class="prof_blok_pukt">
                        <p>Электронная почта</p>
                        <input placeholder="Ваша почта" type="text">
                    </div>
                    <div class="prof_blok_pukt prof_blok_pukt_left">
                        <p>Skype</p>
                        <input placeholder="Ваш скайп" type="text">
                    </div>
                    <div class="prof_blok_pukt">
                        <p>Сайт</p>
                        <input placeholder="Ваш сайт" type="text">
                    </div>
                </div>

                <!--3-->
                <div class="prof_blok_left">
                    <h2>Образование</h2>
                    <div class="prof_blok_pukt prof_blok_pukt_left">
                        <p>Тип заведения</p>
                        <div class="styled-select">
                            <select>
                                <option value="Высшее учебное заведение">Высшее учебное заведение</option>
                                <option value="Среднее специльное учебное заведение">Среднее специльное учебное заведение</option>
                            </select>
                        </div>
                    </div>
                    <div class="prof_blok_pukt">
                        <p>Учебное заведение</p>
                        <div class="styled-select">
                            <select>
                                <option value="ЧПИ(филиал МГОУ)">ЧПИ(филиал МГОУ)</option>
                                <option value="ЧПУ(филиал МГОУ)">ЧПУ(филиал МГОУ)</option>
                            </select>
                        </div>
                    </div>
                    <div class="prof_blok_pukt punkt_widh">
                        <p>Факультет</p>
                        <textarea placeholder="Введите факультет"></textarea>
                    </div>
                    <div class="prof_blok_pukt prof_blok_pukt_left">
                        <p>Форма обучения</p>
                        <div class="styled-select">
                            <select>
                                <option value="Заочное отделение">Заочное отделение</option>
                                <option value="Очное отделение">Очное отделение</option>
                            </select>
                        </div>
                    </div>
                    <div class="prof_blok_pukt">
                        <p>Статус</p>
                        <div class="styled-select">
                            <select>
                                <option value="Выпускник (магистр)">Выпускник (магистр)</option>
                                <option value="Выпускник">Выпускник</option>
                            </select>
                        </div>
                    </div>
                    <div class="prof_blok_pukt">
                        <p>Период учебы</p>
                        <p class="p_span">с</p>
                        <div class="styled-select p_span styled-select_2">
                            <select>
                                <option value="2004">2004</option>
                                <option value="2005">2005</option>
                            </select>
                        </div>
                        <p class="p_span">до</p>
                        <div class="styled-select p_span styled-select_2">
                            <select>
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                            </select>
                        </div>
                    </div>
                </div>

                <!--4-->
                <div class="prof_blok_left">
                    <h2>Языки</h2>
                    <div class="prof_blok_pukt punkt_widh">
                        <p>Какие языки вы знаете?</p>
                        <textarea placeholder="Перечислети через запятую"></textarea>
                    </div>  
                </div>

                <!--5-->
                <div class="prof_blok_left">
                    <h2>Интересы</h2>
                    <div class="prof_blok_pukt punkt_widh">
                        <p>Музыка</p>
                        <textarea></textarea>
                    </div>  
                    <div class="prof_blok_pukt punkt_widh">
                        <p>Книги</p>
                        <textarea></textarea>
                    </div>  
                    <div class="prof_blok_pukt punkt_widh">
                        <p>Фильмы</p>
                        <textarea></textarea>
                    </div>  
                    <div class="prof_blok_pukt punkt_widh">
                        <p>Спорт</p>
                        <textarea></textarea>
                    </div>  
                </div>

                <a class="prof_butt" href="#">Сохранить</a>


            </div> 
        </form>  
    </div>

    <div class="right_block">
        <!-- Contacts -->
        <div class="r_bl_menu">
            <ul>
                <li><a href="#" class="akt">Профиль</a></li>
                <li><a href="#">Навыки</a></li>
                <li><a href="#">Конфиденциальность</a></li>
                <li><a href="#">Баланс</a></li>
            </ul>
        </div>
    </div>


    <div class="clear"></div>
</div>
<!--main-->

<!---------------------------------------------------------------------->
<div class="container-menu-bottom"></div>
<!-- END container-menu --><div class="page-my-information-top-side">
    <div>Личная информация<span>доступна всем пользователям</span></div>
    <!--<a href="#">Моя страница</a>-->
</div><!-- page-top-side -->
<div id="content" class="my-inf-page">
    <p align="center" style="color: green;" > </p>
    <?php
    echo '<form action="/settings" method="post" id="my-information" class="nxSet">
        <p class="desc">
            Все поля заполняются по желанию, 
            <br>однако учтите, что друзьям будет легче 
            <br>вас найти, если информация будет полной
        </p>
        <ul>
            <li><a href="">Основная информация</a></li>
            <li><a href="">Настройки приватности</a></li>
            <li><a href="">Черный список</a></li>
            <!--li><a href="">Изменить пароль</a></li>
            <li><a href="">Красивый ID</a></li-->
        </ul>
        <div>
            <h2>Основная информация</h2>
            <p>
                <span>Адрес страницы</span>
                <input name="url" value="' . $USER['url'] . '" type="text">
            </p>
            <p>
                <span>Имя</span>
                <input name="name" value="' . $USER['name'] . '" type="text">
            </p>
            <p>
                <span>Фамилия</span>
                <input name="fam" value="' . $USER['fam'] . '" type="text">
            </p>
            <p>
                <span>Пол</span>
                <select name="gender" id="">';

    foreach ($_LANG['PROFILE_GENDER'] as $k => $v) {
        echo '<option  value="' . $k . '" ' . ($k == $USER['gender'] ? 'selected' : '') . '>' . $v . '</option>';
    }
    echo '</select>
            </p>
            <p>
                <span>Город</span>
                <input  name="region" value="' . $USER['city'] . '" type="text">
            </p>

            <p>
                <span>Семейное положение</span>
                <select name="relation" id="">
                ';
    foreach ($_LANG['PROFILE_RELATION'][$USER['gender']] as $k => $v) {
        echo '<option  value="' . $k . '" ' . ($k == $USER['relation'] ? 'selected' : '') . '>' . $v . '</option>';
    }
    echo '</select>
            </p>
            <p>
                <span>Дата рождения</span>
                <input name="date" value="' . $USER['date'] . '" type="text">
            </p>
            <p>
                <span>Мобильный телефон</span>
                <input name="tel" value="' . $USER['tel'] . '" type="text">
            </p>

            <p>
                <span>Skype</span>
                <input name="skype" value="' . $USER['skype'] . '" type="text">
            </p> 
            <p>
                <span>Сайт</span>
                <input name="site" value="' . $USER['site'] . '" type="text">
            </p> 
        </div>
        <hr>
        <div>
            <h2>Образование</h2>


            <p>
                <span>ВУЗ</span>
                <input name="vuz" value="' . $USER['vuz'] . '" type="text">
            </p>
            <p>
                <span>Школа</span>
                <input name="scool" value="' . $USER['scool'] . '" type="text">
            </p>
        </div>
        <hr>


        <div class="my-int">
            <h2>Интересы</h2>
            <p>
                <span>Музыка</span>
                <input name="muz" value="' . $USER['muz'] . '" type="text">
            </p>
            <p>
                <span>Фильмы</span>
                <input name="films" value="' . $USER['films'] . '" type="text">
            </p>
            <p>
                <span>Книги</span>
                <input name="books" value="' . $USER['books'] . '" type="text">
            </p>
            <p>
                <span>Игры</span>
                <input name="games" value="' . $USER['games'] . '" type="text">
            </p>
            <p>
                <span>Спорт</span>
                <input name="sports" value="' . $USER['sports'] . '" type="text">
            </p>
            <p>
                <span>Кулинария</span>
                <input name="kul" value="' . $USER['kul'] . '" type="text">
            </p>
            <p>
                <span>Цитата</span>
                <input name="cit" value="' . $USER['cit'] . '" type="text">
            </p>
        </div>
        <hr>
        <div class="my-int">
            <h2>Социальные сети</h2>
            <p>
                <span>Вконтакте</span>
                ' . (!$USER['vk_uid'] ? '<a href="/auth/vk_link">привязать</a>' : '<a href="#" id="vk_ac">определение...</a> <a href="?del_link_vk=1">отвязать</a>') . '
            </p>
        </div>
        <hr> 
        <input type="submit" value="Сохранить">
    </form>

    <form action="/page/let/redprof" method="post" id="my-information">
        <div>
            <h2>Сменить пароль</h2>
            <p>
                <span>Новый пароль</span>
                <input name="pass1"  type="text">
            </p>
            <p>
                <span>Повторите пароль</span>
                <input name="pass2"   type="text">
            </p>
        </div>
        <input type="submit" value="Сохранить">
    </form>';
    ?>




    <div class="clearfix"></div><!-- .clearfix -->

</div>
</div><!-- END CONTAINER -->
<script>
    $(document).ready(function () {
        if ($('#vk_ac').size()) {
            VK.Api.call('users.get', {uids: user.vk_uid}, function (r) {
                if (r.response) {
                    $('#vk_ac').html(r.response[0].first_name + ' ' + r.response[0].last_name).attr('href', '//vk.com/id' + user.vk_uid);
                }
            });
        }
        $('.nxSet').submit(function (e) {
            $.post('/settings', $(this).serialize(), function (data) {
                eval(data);
            });

            return false;
        });
    });
</script>
<?php
footer();

mwork
dwork
owork
swork
langs
