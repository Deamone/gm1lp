<?php
$itsI = false;
$_USER = array();
if (isset($_GET['id'])) {
    $_USER = getUserById($_GET['id']);
}
if (!$_USER || $_USER == $USER) {
    $_USER = $USER;
    $itsI = true;
}

header_('Друзья пользователя ' . $_USER['name'], '', '', ''
        . '<script type="text/javascript" src="/application/views/site/js/nx.js"></script>
');
$friends = getUserFriends($_USER['id']);
$count = count($friends[0]);
?>
<div class="container-menu-bottom"></div>
<div id="content" class="user-page">
    <form action="/">
        <div class="page-friends-top-side">
            <div>
                Всего друзей
                <span><?php echo $count; ?></span>
            </div>
            <div class="search-bar">
                <input type="text" name="search" placeholder="Введите имя и фамилию друга" autocomplete="off">
                <span></span>
                <input type="submit" value="Искать друзей">
            </div>
        </div>
        <div class="friends-left">
            <div class="friends-items">
                <?php
                $count_ = count($friends[1]);
                if ($itsI && $count_) {
                    echo '<h3>Заявки в друзья</h3>';
                    $f_users = array_keys($friends[1]);
                    for ($i = 0; $i < $count_; $i++) {
                        $_ = getUserById($f_users[$i]);
                        $online = '';
                        if ($_['online'] > time() - 900) {
                            $online = '<span>live</span>';
                        }
                        if ($_['date'] && $_['date'] != '0000-00-00') {
                            $e = explode('.', $_['date']);
                            $age = date('Y') - $e[2];
                            if (!$e[2]) {
                                $e = explode('-', $_['date']);
                                $age = date('Y') - $e[0];
                            }
                        }
                        echo '
                <div class="friends-item" data-city="' . $_['city'] . '" data-name="' . $_['name'] . '" data-fam="' . $_['fam'] . '" data-gender="' . $_['gender'] . '" data-age="' . $age . '">
                    <div class="avatar">
                        <a href="/?id=' . $_['id'] . '"><img src="/uploads/avatars/' . $_['ava'] . '"></a>
                    </div>
                    <div class="friends-item-right">
                        <ul>
                            <li><a href="/message?id=' . $_['id'] . '">Написать сообщение</a></li>
                            <li><a href="/friends?id=' . $_['id'] . '">Посмотреть друзей</a></li>
                            <li><a href="/send_gm?id=' . $_['id'] . '" oclick="return false;">Перевести GM</a></li>

                            <li><a href="/?id=' . $_['id'] . '&add_friend" data-id="' . $_['id'] . '" class="pConfirmFriend">Принять предложение</a></li>      
                        </ul>
                        <p class="friend-name">' . $_['fam'] . ' ' . $_['name'] . '</p>
                        <b>' . $_['city'] . '</b>' . $online . '
                    </div>
                    <div class="clear"></div>                                
                </div>';
                    }
                    if ($count) {
                        echo '<h3>Все друзья</h3>';
                    }
                }
                $f_users = array_keys($friends[0]);
                for ($i = 0; $i < $count; $i++) {
                    $_ = getUserById($f_users[$i]);
                    $online = '';
                    if ($_['online'] > time() - 900) {
                        $online = '<span>live</span>';
                    }
                    if ($_['date'] && $_['date'] != '0000-00-00') {
                        $e = explode('.', $_['date']);
                        $age = date('Y') - $e[2];
                        if (!$e[2]) {
                            $e = explode('-', $_['date']);
                            $age = date('Y') - $e[0];
                        }
                    }
                    echo '
                <div class="friends-item" data-city="' . $_['city'] . '" data-name="' . $_['name'] . '" data-fam="' . $_['fam'] . '" data-gender="' . $_['gender'] . '" data-age="' . $age . '">
                    <div class="avatar">
                        <a href="/?id=' . $_['id'] . '"><img src="/uploads/avatars/' . $_['ava'] . '"></a>
                    </div>
                    <div class="friends-item-right">
                        <ul>
                            <li><a href="/message?id=' . $_['id'] . '">Написать сообщение</a></li>
                            <li><a href="/friends?id=' . $_['id'] . '">Посмотреть друзей</a></li>
                            <li><a href="/send_gm?id=' . $_['id'] . '" oclick="return false;">Перевести GM</a></li>

                            <li><a href="/?id=' . $_['id'] . '&del_friend" data-id="' . $_['id'] . '" class="pDelFriend">Удалить</a></li>       
                        </ul>
                        <p class="friend-name">' . $_['fam'] . ' ' . $_['name'] . '</p>
                        <b>' . $_['city'] . '</b>' . $online . '
                    </div>
                    <div class="clear"></div>                                
                </div>';
                }
                ?>

            </div>
        </div>
        <div class="friends-right">
            <div class="search-param">						
                <label for="">Город</label>
                <select name="region" id="">
                    <option value="">Любой</option>
                    <option value="Москва">Москва</option>
                    <option value="Санкт-Петербург">Санкт-Петербург</option>
                    <option value="Адыгея">Адыгея</option>
                    <option value="Алтайский край">Алтайский край</option>
                    <option value="Амурская обл.">Амурская обл.</option>
                    <option value="Архангельская обл.">Архангельская обл.</option>
                    <option value="Астраханская обл.">Астраханская обл.</option>
                    <option value="Башкортостан(Башкирия)">Башкортостан(Башкирия)</option>
                    <option value="Белгородская обл.">Белгородская обл.</option>
                    <option value="Брянская обл.">Брянская обл.</option>
                    <option value="Бурятия">Бурятия</option>
                    <option value="Владимирская обл.">Владимирская обл.</option>
                    <option value="Волгоградская обл.">Волгоградская обл.</option>
                    <option value="Вологодская обл.">Вологодская обл.</option>
                    <option value="Воронежская обл.">Воронежская обл.</option>
                    <option value="Дагестан">Дагестан</option>
                    <option value="Еврейская обл.">Еврейская обл.</option>
                    <option value="Ивановская обл.">Ивановская обл.</option>
                    <option value="Иркутская обл.">Иркутская обл.</option>
                    <option value="Кабардино-Балкария">Кабардино-Балкария</option>
                    <option value="Калининградская обл.">Калининградская обл.</option>
                    <option value="Калмыкия">Калмыкия</option>
                    <option value="Калужская обл.">Калужская обл.</option>
                    <option value="Камчатская обл.">Камчатская обл.</option>
                    <option value="Карелия">Карелия</option>
                    <option value="Кемеровская обл.">Кемеровская обл.</option>
                    <option value="Кировская обл.">Кировская обл.</option>
                    <option value="Коми">Коми</option>
                    <option value="Костромская обл.">Костромская обл.</option>
                    <option value="Краснодарский край">Краснодарский край</option>
                    <option value="Красноярский край">Красноярский край</option>
                    <option value="Курганская обл.">Курганская обл.</option>
                    <option value="Курская обл.">Курская обл.</option>
                    <option value="Липецкая обл.">Липецкая обл.</option>
                    <option value="Магаданская обл.">Магаданская обл.</option>
                    <option value="Марий Эл">Марий Эл</option>
                    <option value="Мордовия">Мордовия</option>
                    <option value="Мурманская обл.">Мурманская обл.</option>
                    <option value="Нижегородская (Горьковская)">Нижегородская (Горьковская)</option>
                    <option value="Новгородская обл.">Новгородская обл.</option>
                    <option value="Новосибирская обл.">Новосибирская обл.</option>
                    <option value="Омская обл.">Омская обл.</option>
                    <option value="Оренбургская обл.">Оренбургская обл.</option>
                    <option value="Орловская обл.">Орловская обл.</option>
                    <option value="Пензенская обл.">Пензенская обл.</option>
                    <option value="Пермский край">Пермский край</option>
                    <option value="Приморский край">Приморский край</option>
                    <option value="Псковская обл.">Псковская обл.</option>
                    <option value="Ростовская обл.">Ростовская обл.</option>
                    <option value="Рязанская обл.">Рязанская обл.</option>
                    <option value="Самарская обл.">Самарская обл.</option>
                    <option value="Саратовская обл.">Саратовская обл.</option>
                    <option value="Саха (Якутия)">Саха (Якутия)</option>
                    <option value="Сахалин">Сахалин</option>
                    <option value="Свердловская обл.">Свердловская обл.</option>
                    <option value="Северная Осетия">Северная Осетия</option>
                    <option value="Смоленская обл.">Смоленская обл.</option>
                    <option value="Ставропольский край">Ставропольский край</option>
                    <option value="Тамбовская обл.">Тамбовская обл.</option>
                    <option value="Татарстан">Татарстан</option>
                    <option value="Тверская обл.">Тверская обл.</option>
                    <option value="Томская обл.">Томская обл.</option>
                    <option value="Тува (Тувинская Респ.)">Тува (Тувинская Респ.)</option>
                    <option value="Тульская обл.">Тульская обл.</option>
                    <option value="Тюменская обл. и Ханты-Мансийский АО">Тюменская обл. и Ханты-Мансийский АО</option>
                    <option value="Удмуртия">Удмуртия</option>
                    <option value="Ульяновская обл.">Ульяновская обл.</option>
                    <option value="Уральская обл.">Уральская обл.</option>
                    <option value="Хабаровский край">Хабаровский край</option>
                    <option value="Хакасия">Хакасия</option>
                    <option value="Челябинская обл.">Челябинская обл.</option>
                    <option value="Чечено-Ингушетия">Чечено-Ингушетия</option>
                    <option value="Читинская обл.">Читинская обл.</option>
                    <option value="Чувашия">Чувашия</option>
                    <option value="Чукотский АО">Чукотский АО</option>
                    <option value="Ямало-Ненецкий АО">Ямало-Ненецкий АО</option>
                    <option value="Ярославская обл.">Ярославская обл.</option>
                    <option value="Карачаево-Черкесская Республика">Карачаево-Черкесская Республика</option>
                    <option value="Украина">Украина</option>
                    <option value="Беларусь">Беларусь</option>
                </select>
                <label for="">Возраст</label>
                <select name="age1" id="" class="sel-age">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="35">35</option>
                    <option value="36">36</option>
                    <option value="37">37</option>
                    <option value="38">38</option>
                    <option value="39">39</option>
                    <option value="40">40</option>
                    <option value="41">41</option>
                    <option value="42">42</option>
                    <option value="43">43</option>
                    <option value="44">44</option>
                    <option value="45">45</option>
                    <option value="46">46</option>
                    <option value="47">47</option>
                    <option value="48">48</option>
                    <option value="49">49</option>
                    <option value="50">50</option>
                    <option value="51">51</option>
                    <option value="52">52</option>
                    <option value="53">53</option>
                    <option value="54">54</option>
                    <option value="55">55</option>
                    <option value="56">56</option>
                    <option value="57">57</option>
                    <option value="58">58</option>
                    <option value="59">59</option>
                    <option value="60">60</option>
                    <option value="61">61</option>
                    <option value="62">62</option>
                    <option value="63">63</option>
                    <option value="64">64</option>
                    <option value="65">65</option>
                    <option value="66">66</option>
                    <option value="67">67</option>
                    <option value="68">68</option>
                    <option value="69">69</option>
                    <option value="70">70</option>
                    <option value="71">71</option>
                    <option value="72">72</option>
                    <option value="73">73</option>
                    <option value="74">74</option>
                    <option value="75">75</option>
                    <option value="76">76</option>
                    <option value="77">77</option>
                    <option value="78">78</option>
                    <option value="79">79</option>
                    <option value="80">80</option>
                    <option value="81">81</option>
                    <option value="82">82</option>
                    <option value="83">83</option>
                    <option value="84">84</option>
                    <option value="85">85</option>
                    <option value="86">86</option>
                    <option value="87">87</option>
                    <option value="88">88</option>
                    <option value="89">89</option>
                    <option value="90">90</option>
                    <option value="91">91</option>
                    <option value="92">92</option>
                    <option value="93">93</option>
                    <option value="94">94</option>
                    <option value="95">95</option>
                    <option value="96">96</option>
                    <option value="97">97</option>
                    <option value="98">98</option>
                    <option value="99">99</option>
                    <option value="100">100</option>
                </select>
                -
                <select name="age2" id="" class="sel-age">
                    <option value="100">100</option>
                    <option value="99">99</option>
                    <option value="98">98</option>
                    <option value="97">97</option>
                    <option value="96">96</option>
                    <option value="95">95</option>
                    <option value="94">94</option>
                    <option value="93">93</option>
                    <option value="92">92</option>
                    <option value="91">91</option>
                    <option value="90">90</option>
                    <option value="89">89</option>
                    <option value="88">88</option>
                    <option value="87">87</option>
                    <option value="86">86</option>
                    <option value="85">85</option>
                    <option value="84">84</option>
                    <option value="83">83</option>
                    <option value="82">82</option>
                    <option value="81">81</option>
                    <option value="80">80</option>
                    <option value="79">79</option>
                    <option value="78">78</option>
                    <option value="77">77</option>
                    <option value="76">76</option>
                    <option value="75">75</option>
                    <option value="74">74</option>
                    <option value="73">73</option>
                    <option value="72">72</option>
                    <option value="71">71</option>
                    <option value="70">70</option>
                    <option value="69">69</option>
                    <option value="68">68</option>
                    <option value="67">67</option>
                    <option value="66">66</option>
                    <option value="65">65</option>
                    <option value="64">64</option>
                    <option value="63">63</option>
                    <option value="62">62</option>
                    <option value="61">61</option>
                    <option value="60">60</option>
                    <option value="59">59</option>
                    <option value="58">58</option>
                    <option value="57">57</option>
                    <option value="56">56</option>
                    <option value="55">55</option>
                    <option value="54">54</option>
                    <option value="53">53</option>
                    <option value="52">52</option>
                    <option value="51">51</option>
                    <option value="50">50</option>
                    <option value="49">49</option>
                    <option value="48">48</option>
                    <option value="47">47</option>
                    <option value="46">46</option>
                    <option value="45">45</option>
                    <option value="44">44</option>
                    <option value="43">43</option>
                    <option value="42">42</option>
                    <option value="41">41</option>
                    <option value="40">40</option>
                    <option value="39">39</option>
                    <option value="38">38</option>
                    <option value="37">37</option>
                    <option value="36">36</option>
                    <option value="35">35</option>
                    <option value="34">34</option>
                    <option value="33">33</option>
                    <option value="32">32</option>
                    <option value="31">31</option>
                    <option value="30">30</option>
                    <option value="29">29</option>
                    <option value="28">28</option>
                    <option value="27">27</option>
                    <option value="26">26</option>
                    <option value="25">25</option>
                    <option value="24">24</option>
                    <option value="23">23</option>
                    <option value="22">22</option>
                    <option value="21">21</option>
                    <option value="20">20</option>
                    <option value="19">19</option>
                    <option value="18">18</option>
                    <option value="17">17</option>
                    <option value="16">16</option>
                    <option value="15">15</option>
                    <option value="14">14</option>
                    <option value="13">13</option>
                    <option value="12">12</option>
                    <option value="11">11</option>
                    <option value="10">10</option>
                    <option value="9">9</option>
                    <option value="8">8</option>
                    <option value="7">7</option>
                    <option value="6">6</option>
                    <option value="5">5</option>
                    <option value="4">4</option>
                    <option value="3">3</option>
                    <option value="2">2</option>
                    <option value="1">1</option>
                </select>
                <label for="">Пол</label>
                <input type="radio" name="gender" value="0">Не важно
                <br><input type="radio" name="gender" value="1">Мужской
                <br><input type="radio" name="gender" value="2">Женский
                <hr>
            </div>
        </div>
    </form>





    <div class="clearfix"></div><!-- .clearfix -->

</div>
</div>
<script>
    $(document).ready(function() {
        $(document).on('submit', 'form', function() {
            return false;
        });
        $(document).on('change', 'input[name=gender]', function() {
            var $gender = parseInt($(this).val());
            $('.friends-item').each(function(v) {
                if (!$gender) {
                    $(this).removeClass('hideGender');
                    return;
                }
                if ($(this).attr('data-gender') != $gender) {

                    $(this).addClass('hideGender');
                } else {
                    $(this).removeClass('hideGender');
                }
            });
        });

        $(document).on('change', 'select[name=region]', function() {
            var $city = $(this).val();
            $('.friends-item').each(function(v) {
                if (!$city) {
                    $(this).removeClass('hideCity');
                    return;
                }
                if ($(this).attr('data-city') != $city) {
                    $(this).addClass('hideCity');
                } else {
                    $(this).removeClass('hideCity');
                }
            });
        });



        $(document).on('change', '.sel-age', function() {
            var $age1 = parseInt($('select[name=age1]').val());
            var $age2 = parseInt($('select[name=age2]').val());
            $('.friends-item').each(function(v) {
                var age = parseInt($(this).attr('data-age'));
                if (age > $age2 || age < $age1) {
                    $(this).addClass('hideAge');
                } else {
                    $(this).removeClass('hideAge');
                }
            });
        });

        $(document).on('keyup', 'input[name=search]', function(e) {
            var $name = $(this).val().toLowerCase();
            console.log(e);
            $('.friends-item').each(function(v) {
                var name = $(this).attr('data-name').toLowerCase();
                var fam = $(this).attr('data-fam').toLowerCase();
                if (!$name) {
                    $(this).removeClass('hideName');
                    return;
                }
                if (name.indexOf($name) === -1 && fam.indexOf($name) === -1) {
                    $(this).addClass('hideName');
                    return;
                } else {
                    $(this).removeClass('hideName');
                }
            });
        });

        FRIENDS
        $(document).on('click', 'a.pDelFriend', function(e) {
            $this = $(this);
            $.get('/?id=' + $this.attr('data-id') + '&del_friend', '', function(data) {
                showMessage('Вы отказались дружить', 2500);
                $this.html('Добавить в друзья').attr('href', '?add_friend');
                $this.parent().attr('id', 'pAddFriend');
            });
            e.preventDefault();
            return false;
        });
        $(document).on('click', 'a.pConfirmFriend', function(e) {
            $this = $(this);
            $.get('/?id=' + $this.attr('data-id') + '&add_friend', '', function(data) {
                showMessage('Теперь вы друзья', 2500);
                $this.html('Удалить из друзей').attr('href', '?del_friend');
                $this.parent().attr('id', 'pDelFriend');
            });
            e.preventDefault();
            return false;
        });
    });
</script>
<?php
footer();
