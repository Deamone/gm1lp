<?php
if (isset($SYSTEM['get']['eid']) && isset($SYSTEM['get']['ehash'])) {
    $user = getUserById($SYSTEM['get']['eid']);
    if ($user && !$user['confirmed'] && md5($user['login']) == $SYSTEM['get']['ehash']) {
        updateUserById($user['id'], array('confirmed'), array('1'));
		redirect('/');
        exit('Спасибо! Аккаунт успешно активирован!<a href="/">Авторизация</a>');
    }
}
if (isset($SYSTEM['get']['pid']) && isset($SYSTEM['get']['ehash'])) {
    $user = getUserById($SYSTEM['get']['pid']);
    if ($user && md5($user['password']) == $SYSTEM['get']['ehash']) {
        updateUserById($user['id'], array('password'), array(md5($SYSTEM['get']['np'])));
        exit('Пароль успешно изменен!<a href="/">Авторизация</a>');
    }
}

if (isAjax()) {

    if (isset($SYSTEM['post']['forgot']) && isset($SYSTEM['post']['email'])) {
        $json = array();
        if (empty($SYSTEM['post']['email'])) {
            $json['errors']['#forgot_go'] = array('Укажите адрес эл. почты', 'bottom');
        }
        $user = getUserByEmail($SYSTEM['post']['email']);
        if (!isset($json['errors']) && $user) {
            $user_id = $user['id'];
            $email = $SYSTEM['post']['email'];
            $password = gen_password(6);
            $url = 'https://gm1lp.ru/page/login?pid=' . $user_id . '&ehash=' . md5($user['password']) . '&np=' . $password;
            /*$message = "
<html><head><title>Восстановление пароля на GM1LP</title></head><body>
<p>
Ваш логин: " . $email . "<br/>
Ваш <b>Новый</b> пароль: " . $password . "<br/><br/>
Для завершения установки пароля, пройдите
пожалуйста по ссылке: <a href=\"{$url}\">{$url}</a><br/>
Ваш GM!<br/>
-<br/>
Никому и никогда не сообщайте свой пароль.</p>
</body></html>";*/
            $message = '<html>
<head>
    <style>
        td.grey {
            color: #808080;
        }

        td.h {
            font-weight: bold;
        }

        td.header {
            font-weight: bold;
            color: #808080;
            border-bottom: 1px solid #A0A0A0;
        }

        td.header a {
            text-decoration: none;
            color: #808080;
            font-weight: bold;
        }

        td.header img {
            vertical-align: middle;
        }

        td {
            padding: 10px 0;
        }
    </style>
</head>
<body>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
        <td class="header"><a href="http://gm1lp.ru"><img src="http://gm1lp.ru/images/logo_small.png"> GM1LP</a></td>
    </tr>
    <tr>
        <td class="h">
            Восстановление пароля в GM1LP
        </td>
    </tr>
    <tr>
        <td>
            Вами был создан запрос на восстановления пароля на GM1LP. Если это не вы делали запрос, то просто проигнорируйте это письмо.
            <br/><br/>
            Ваш логин: ' . $email . '<br/>
            Ваш пароль: ' . $password . '
            <br/><br/>
            Ваш профиль на GM1LP: '.$user['name'].' '.$user['fam'].'. Вы можете настроить, изменить его данные, указать навыки и т.д., перейдя по ссылке:
            <br/><br/>
            <a href="' . $url . '">Завершить восстановление и перейти в профиль</a>
        </td>
    </tr>
    <tr>
        <td class="grey">
            С наилучшими пожеланиями,
            Команда GM1LP
        </td>
    </tr>
</table>
</body>
</html>';
            sender($SYSTEM['post']['email'], 'Восстановление пароля на GM1LP', $message);
            $json['eval'][] = '$("#forgot_go").attr({"data-tip":"Пожалуйста проверьте свою электронную почту для завершения восстановления пароля","data-mood":"positive","data-position":"top"}); $("#forgot_form input").attr("disabled","disabled");tip($("#forgot_go"));';
        } else {
            $json['errors']['#forgot_go'] = array('Пользователь не найден', 'bottom');
        }
        exit(json_encode($json));
    }

    /***
     **/
    if (isset($SYSTEM['post']['go']) && isset($SYSTEM['post']['login']) && isset($SYSTEM['post']['password'])) {
        $json = array();
        if (empty($SYSTEM['post']['login']) || empty($SYSTEM['post']['password'])) {
            $json['errors']['#aut_go'] = array('Укажите данные', 'top');
        }
        $user = getUserByEmail($SYSTEM['post']['login']);
        $auth = false;
        if (!isset($json['errors']) && $user && $user['password'] == md5($SYSTEM['post']['password'])) {
            $url = '/id' . $user['id'];
            if (isset($_POST['referer'])) {
                $_POST['referer'] = str_replace(array('http://', 'https://'), array('', ''), $_POST['referer']);
                $url = 'http://' . $_POST['referer'] . '';
            }
            if (!$user['auth'] || !$user['tel']) {
                $json['eval'][] = '$("#aut_go").attr({"data-tip":"Вы успешно авторизированы! Идет переадресация..","data-mood":"positive","data-position":"bottom"}); $("#aut_form input").attr("disabled","disabled");tip($("#aut_go")); var audio = new Audio(\'/audio/login.mp3\'); audio.play(); setTimeout(function (){window.location.href="' . $url . '";}, 3000);';
                $auth = true;
            } else {
                if (isset($SYSTEM['post']['code']) && $SYSTEM['post']['code'] == $user['tmp']) {
                    $json['eval'][] = '$("#aut_go2").attr({"data-tip":"Вы успешно авторизированы! Идет переадресация..","data-mood":"positive","data-position":"bottom"}); $("form input").attr("disabled","disabled");tip($("#aut_go2")); var audio = new Audio(\'/audio/login.mp3\'); audio.play(); setTimeout(function (){window.location.href="' . $url . '";}, 3000);';
                    $auth = true;
                } elseif (isset($SYSTEM['post']['code'])) {
                    if (isset($_COOKIE['timeCode'])) {
                        $time = (int)$_COOKIE['timeCode'] - time();
                    } else {
                        $time = TIMECODE;
                        setcookie('timeCode', time() + $time, time() + $time, '/');
                    }
                    $json['errors']['#aut_go2'] = array('<div id="textErrCode">Указан неверный код.<br/><span id="newCode">Выслать новый код</span> можно будет через <span id="newCodeTime">' . $time . '</span>сек.</div>', 'bottom');
                    $json['eval'] [] = 'timeCode=' . $time . '; startNewCode();';
                } else {
                    $eval = '';
                    if(isset($SYSTEM['post']['newCode'])){
                        $json['eval'][] = '$("#auth_code").attr({"data-tip":"Мы отправили новый код, пожалуйста, укажите его","data-mood":"positive","data-position":"bottom"}); tip($("#auth_code"));';
                    }
                    $json['eval'][] = '$("#aut_form").hide().next().show(); $("#aut_login2").val($("#aut_login").val());$("#aut_password2").val($("#aut_password").val());';
                    if (isset($_COOKIE['timeCode'])) {
                        $time = (int)$_COOKIE['timeCode'] - time();
                    } else {
                        $time = TIMECODE;
                        setcookie('timeCode', time() + $time, time() + $time, '/');
                    }
                    $code = mt_rand(111111, 999999);
                    sendMessage($user['auth_phone'], 'Vash cod: ' . $code);

                    updateUserById($user['id'], array('tmp'), array($code));
                }
            }
            if ($auth) {
                updateUserById($user['id'], array('online'), array(time()));
                Session::get_instance()->create($user['id'], isset($SYSTEM['post']['member']) ? null : 2592000);
            }
        } else {
            $json['errors']['#aut_go'] = array('Пользователь не найден', 'top');
        }
        exit(json_encode($json));
    }


    $json = array();
    if (empty($SYSTEM['post']['fname'])) {
        $json['errors']['#reg_fname'] = array('Пожалуйста укажите свое имя', 'top');
    } elseif (empty($SYSTEM['post']['lname'])) {
        $json['errors']['#reg_lname'] = array('Пожалуйста укажите свою фамилию', 'top');
    } else {
        if (empty($SYSTEM['post']['email'])) {
            $json['errors']['#reg_email'] = array('Пожалуйста укажите E-mail', 'top');
        } else {
            if (!filter_var($SYSTEM['post']['email'], FILTER_VALIDATE_EMAIL)) {
                $json['errors']['#reg_email'] = array('Пожалуйста укажите валидный E-mail', 'top');
            } else {
                $user = getUserByEmail($SYSTEM['post']['email']);
                if ($user) {
                    $json['errors']['#reg_email'] = array($user['confirmed'] ? 'Пользователь с таким адресом уже зарегистрирован' : 'Пользователь с таким адресом уже зарегистрирован и ожидает активации', 'top');
                } else {
                    if (empty($SYSTEM['post']['password']) || strlen($SYSTEM['post']['password']) < 6) {
                        $json['errors']['#reg_password'] = array('Пароль должен содержать минимум 6 символов', 'top');
                    }
                }
            }
        }
    }

    if (!isset($json['errors'])) {
		
        $user_id = addUser($SYSTEM['post']['email'], $SYSTEM['post']['password'], $SYSTEM['post']['fname'], $SYSTEM['post']['lname'], $SYSTEM['post']['code']);
        updateUserById($user_id, array('url'), array('id' . $user_id));
        
		updateUserById($user_id, array('online'), array(time()));
		Session::get_instance()->create($user_id, 2592000);
		
		
		$url = 'http://gm1lp.ru/page/login?eid=' . $user_id . '&ehash=' . md5($SYSTEM['post']['email']);
        /*$message = "
<html><head><title>Подтвердите регистрацию на GM1LP</title></head><body>
<p>Благодарим Вас за регистрацию в системе GM1LP.<br/>
Ваш логин: " . $SYSTEM['post']['email'] . "<br/>
Ваш пароль: " . $SYSTEM['post']['password'] . "<br/>
данные действуют для всех сервисов системы GM1LP. Это предварительная регистрация, после
редизайна и обновления системы, мы обязательно вышлем вам приглашение, где вы сможете 
воспользоваться всеми возможностями системы абсолютно бесплатно!<br/>
Для завершения предварительной регистрации и активации вашей учетной записи, пройдите 
пожалуйста по ссылке: <a href=\"{$url}\">{$url}</a><br/>
Ваш GM!<br/>
-<br/>
Никому и никогда не сообщайте свой пароль.</p>
</body></html>";*/
        $message = '<html>
<head>
    <style>
        td.grey {
            color: #808080;
        }

        td.h {
            font-weight: bold;
        }

        td.header {
            font-weight: bold;
            color: #808080;
            border-bottom: 1px solid #A0A0A0;
        }

        td.header a {
            text-decoration: none;
            color: #808080;
            font-weight: bold;
        }

        td.header img {
            vertical-align: middle;
        }

        td {
            padding: 10px 0;
        }
    </style>
</head>
<body>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
        <td class="header"><a href="http://gm1lp.ru"><img src="http://gm1lp.ru/images/logo_small.png"> GM1LP</a></td>
    </tr>
    <tr>
        <td class="h">
            Ваш Аккаунт в GM1LP успешно создан
        </td>
    </tr>
    <tr>
        <td>
            Вы создали Аккаунт в GM1LP. Вы можете его заполнить и с его помощью использовать все сервисы GM: создавать
            свои Компании и Сообщества, читать интересные статьи и новости, знакомиться с успешными людьми, находить
            партнеров и проводить сделки. Благодаря другим сервисам, вы можете проводить различные рекламные компании,
            привлекать средства для своих проектов, оказывать профессиональную помощь и зарабатывать на этом.
            <br/><br/>
            Ваш профиль на GM1LP: ' . $SYSTEM['post']['fname'] . ' ' . $SYSTEM['post']['lname'] . '. Вы можете настроить, изменить его данные, указать навыки и т.д.,
            перейдя по
            ссылке:<br/><br/>
            <a href="' . $url . '">Активировать профиль</a>
        </td>
    </tr>
    <tr>
        <td class="grey">
            С наилучшими пожеланиями,
            Команда GM1LP
        </td>
    </tr>
</table>
</body>
</html>';
        sender($SYSTEM['post']['email'], 'Подтвердите регистрацию на GM1LP', $message);
        $json['eval'][] = '$("#reg_go").attr({"data-tip":"Спасибо! Сейчас вы будете перенапраалены на свю страницу. Пожалуйста проверьте свою электронную почту для завершения предварительной регистрации","data-mood":"positive","data-position":"bottom"}); $("#reg_form input").attr("disabled","disabled");tip($("#reg_go"));';
        $json['eval'][] = '   window.setTimeout(function(){window.location.href = "https://gm1lp.ru/";}, 5000);';
    }

    exit(json_encode($json));
}
if (isset($_COOKIE['referer'])) {
    $referer = $_COOKIE['referer'];
} else {
    $referer = '/';
}
?>
<?php
$b = explode('|', file_get_contents('cache/bg'));
if ((time() - $b[0]) > 60 * 100) {
    if ($b[1] >= 13) {
        $b[1] = 1;
    } else {
        $b[1]++;
    }
    $b[0] = time();
    file_put_contents('cache/bg', implode('|', $b));
}
$bg = str_repeat('0', 3 - strlen($b[1])) . $b[1];
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <title><?php echo $title; ?></title>
    <link rel="shortcut icon" href="/favicon.png" type="image/png"/>
    <link rel="icon" href="/animated_favicon.gif" type="image/gif"/>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link media="screen" href="/css/css.css" type="text/css" rel="stylesheet" charset="utf-8"/>
    <link type="text/css" rel="stylesheet" href="/css/rcarousel.css"/>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="/js/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="/js/jquery.ui.rcarousel.js"></script>
    <style>

        @media screen and (max-device-width: 780px) {
            footer {
                height: 40px;
                padding-top: 3px;
                padding-bottom: 10px;
            }

            .wrap {
                padding-top: 100px;
            }
        }

        .send_code {
            margin: 28px 0px;
            font-weight: bold;
            color: lightseagreen;
        }
    </style>
</head>
<body style="background-image: url(/img/backs/<?php echo $bg; ?>.jpg);">

<div class="background">
    <!--div class="bg_a">
        <div class="bg_w">
            <div class="bg_title">Черная пятница <span>2014</span>
                <span class="bg_more">До начала крупнейшей распродажи года осталось</span></div>
            <div class="bg_c"><a href="#">&nbsp;</a></div>
            <div class="bg_t">
                <div class="col1">00</div>
                <div class="col2">00</div>
                <div class="col3">00</div>
                <div class="col4">00</div>
                <div class="col5">00</div>
            </div>
            <div class="bg_footer">Не пропустите все самые выгодные скидки, конкурсы и акции - зарегистрируйтесь для уведомления о начале распродаж и возможности самому использовать сервисы для своих целей!</div>
        </div>
    </div-->
    <div class="wrap">
        <div class="content">
            <div class="login">
                <div class="login1">
                    <img src="/img/logo2.png"/>

                    <div class="form">
                        <span>Авторизация</span>

                        <form method="post" action="#" id="aut_form">
                            <input id="aut_login" name="login" type="text" class="f1"
                                   placeholder="Адрес эл.почты*"/><br/>

                            <div class="pass"><input id="aut_password" name="password" type="password" class="f2"
                                                     placeholder="Пароль*"/><a href="#" id="forgotPass"><img
                                        src="/img/q.png"/></a></div>
                            <label for="member">
                                <input id="member" type="checkbox" name="member" value="1"/> чужой компьютер
                            </label>
                            <br/>
                            <br/>
                            <input type="hidden" name="go" value="1"/>
                            <?php
                            if (isset($_GET['referer'])) {
                                echo '<input type="hidden" name="referer" value="' . $_GET['referer'] . '" />';
                            }
                            ?>
                            <input type="submit" id="aut_go" name="go" class="f3" value="ВОЙТИ"/>
                        </form>

                        <form method="post" action="#" id="aut_form2" style="display: none">
                            <div class="send_code">На ваш номер телефона был выслан код, для подтверждения авторизации
                            </div>
                            <div class="pass">
                                <input type="hidden" name="login" id="aut_login2"/>
                                <input type="hidden" name="password" id="aut_password2"/>
                                <input id="auth_code" name="code" type="text" class="f2"
                                       placeholder="Введите код из смс"/></div>
                            <br/>
                            <input type="hidden" name="go" value="1"/>
                            <input type="submit" id="aut_go2" name="go" class="f3" value="ВОЙТИ"/>
                        </form>
                    </div>
                </div>
                Не являетесь пользователем? <a href="#" class="goReg">Присоединиться</a>
            </div>
        </div>
    </div>
    <!-- Q -->
    <!-- Content -->
    <div class="wrap1" id="reg">
        <!-- Slogan -->
        <div class="slogan">Стать лучшим. <span>Быть лучшим.</span></div>

        <!-- RegForm -->
        <div class="regform">
            <div class="regform_zag">Подключиться к системе.<br/><span>Это бесплатно.</span></div>
            <div class="regform_text">Регистрация займет всего несколько секунд</div>
            <div>
                <form method="post" action="#" id="reg_form">
                    <input name="fname" id="reg_fname" type="text" class="f4" placeholder="Имя" data-tip="Укажите имя"
                           data-position="top" data-mood="negative"/>
                    <input name="lname" id="reg_lname" type="text" class="f5" placeholder="Фамилия"/><br/>
                    <input name="email" id="reg_email" type="text" class="f6" placeholder="Адрес эл.почты"/><br/>
                    <input name="password" id="reg_password" type="password" class="f6"
                           placeholder="Пароль (6 или более символов)"/><br/>
                    <input name="code" id="code" type="text" class="f6"
                           placeholder="Код(можно буквенный) пригласившего вас человека (если есть)"/><br/>
                    <input type="submit" id="reg_go" class="f7" value="ПОДКЛЮЧИТЬСЯ"/><br/>
                </form>
            </div>
            <div class="regform_text">Нажимая "Подключиться", вы соглашаетесь с <a href="http://gm1lp.ru/help/agree"
                                                                                   target="_blank">Пользовательским<br/>соглашением</a>,
                <a href="http://gm1lp.ru/help/agree" target="_blank">Политикой конфиденциальности</a> и <a
                    href="http://gm1lp.ru/help/rules" target="_blank">Правилами сообщества</a> GM1LP.
            </div>
        </div>

        <!-- Thanks -->
        <div class="thanks">
            <div class="thanks_zag">Особая благодарность за помощь в разработке</div>
            <div class="thanks_logo"></div>
        </div>
    </div>
    <div class="wrap1" id="forgot">
        <!-- Slogan -->
        <div class="slogan">Стать лучшим. <span>Быть лучшим.</span></div>

        <!-- RegForm -->
        <div class="regform">
            <div class="regform_zag" style="text-align: center;">Восстановление пароля.</div>
            <div>
                <form method="post" action="#" id="forgot_form">
                    <input name="email" id="forgot_email" type="text" class="f6" placeholder="Адрес эл.почты"/><br/>
                    <input type="hidden" name="forgot" value="1"/>
                    <input type="submit" id="forgot_go" name="forgot" class="f7" value="Восстановить"/><br/>
                </form>
            </div>
        </div>
    </div>

    <!-- Content -->
    <div class='wrap1' id="slider">
        <!-- Slogan -->
        <div class='slogan1'>Один аккаунт. <span>Весь мир GM!</span></div>
        <div class='slogan2'>Один аккаунт для всех сервисов GM</div>

        <!-- Slider -->
        <div id="container">
            <div id="carousel">
                <div id="slide01" class="slide">
                    <div class='slider_div a1'>
                        <div class='slider_s'>
                            <div class='a1a'>
                                <span>У вас сайт или магазин в котором<br/>постоянно проходят скидки?</span><br/><br/>Подключитесь
                                к GM, у нас есть пользователи,<br/>желающие их получить!<br/><br/>Благодаря
                                полностью автоматизированному<br/>сервису скидок, на это уйдет несколько минут.
                            </div>
                            <div class='a1b'>
                                <span>Вы заядлый шопоголик,<br/>очень любящий скидки?</span><br/><br/>Подключитесь к
                                GM, у нас есть для вас<br/>шикарные предложения со всей России и СНГ!<br/><br/>Благодаря
                                полностью автоматизированному<br/>сервису скидок, их у нас очень, очень много!
                            </div>
                        </div>
                        <div class='slider_button'><a href="#" class='slider_button1 goReg'>ПОДКЛЮЧИТЬСЯ</a></div>
                    </div>
                </div>
                <div id="slide02" class="slide">
                    <div class='slider_div a1-2'>
                        <div class='slider_s'>
                            <div class='a1a'>
                                <span>У вас на сайте или в магазине часто проходят<br/>розыгрыши ценных подарков?</span><br/><br/>Подключитесь
                                к GM, у нас есть те,<br/>кто с радостью примет в них участие!<br/><br/>Благодаря
                                полностью автоматизированному<br/>сервису конкурсов, на это уйдет несколько минут
                            </div>
                            <div class='a1b'>
                                <span>Вы очень любите получать различные<br/>подарки и участвовать в конкурсах?</span><br/><br/>Подключитесь
                                к GM, у нас проводятся<br/>тысячи конкурсов каждый день!<br/><br/>Благодаря
                                полностью автоматизированному<br/>сервису конкурсов, их действительно очень много!
                            </div>
                        </div>
                        <div class='slider_button'><a href="#" class='slider_button1 goReg'>ПОДКЛЮЧИТЬСЯ</a></div>
                    </div>
                </div>

                <div id="slide03" class="slide">
                    <div class='slider_div a2'>
                        <div class='slider_s'>
                            <div class='a2a'>
                                <span>Вы поставщик и вам были бы интересны новые<br/>предложения о новых партнерских<br/>отношениях?</span><br/><br/><a
                                    href="#" class="goReg">Подключитесь</a> к GM, у нас есть сервис где вы<br/>легко
                                сможете начать открывать новые<br/>горизонты!<br/><br/>Благодаря полностью
                                автоматизированному<br/>сервису поставщиков, вы быстро ворветесь в<br/>новые города!
                            </div>
                            <div class='a2b'>
                                <span>Вы в поисках надежного поставщика или<br/>сомневаетесь в нынешних?</span><br/><br/><a
                                    href="#" class="goReg">Подключитесь</a> к GM и проверьте их<br/>в списке GM
                                Black List!<br/><br/>Благодаря полностью автоматизированному<br/>сервису
                                поставщиков, вы сразу узнаете кто есть кто!
                            </div>
                        </div>
                        <div class='slider_button'><a href="#" class='slider_button1 goReg'>ПОДКЛЮЧИТЬСЯ</a></div>
                    </div>
                </div>

                <div id="slide04" class="slide">
                    <div class='slider_div a3'>
                        <div class='slider_s'>
                            <div class='a3a'>
                                <span>Вы живете бешеным темпом и вам часто<br/>не хватает пары полезных рук?</span><br/><br/>Подключитесь
                                к GM, у нас как раз есть те, кто<br/>с радостью вам поможет за небольшое
                                вознаграждение!<br/><br/>Благодаря полностью автоматизированному<br/>сервису
                                заданий, вы легко и быстро сможете найти надежного исполнителя!
                            </div>
                            <div class='a3b'>
                                <span>У вас часто бывает свободное время и вы<br/>не прочь заработать в этот момент?</span><br/><br/>Подключитесь
                                к GM и выполняйте несложные поручения,<br/>которые придутся вам по душе! За это у
                                нас платят!<br/><br/>Благодаря полностью автоматизированному<br/>сервису заданий,
                                ваше хобби может стать вашей работой!
                            </div>
                        </div>
                        <div class='slider_button'><a href="#" class='slider_button1 goReg'>ПОДКЛЮЧИТЬСЯ</a></div>
                    </div>
                </div>

                <div id="slide05" class="slide">
                    <div class='slider_div a4'>
                        <div class='slider_s'>
                            <div class='a4a'>
                                <span>Вы постоянно что-то продаете в интернете,<br/>но покупатели часто не забирают свой товар<br/>на почте?</span><br/><br/>Подключитесь
                                к GM, у нас есть система Сделок<br/>Без Риска, которая сама обо всем
                                позаботится.<br/><br/>Благодаря СБР, встроенной в свою социальную<br/>сеть, вы
                                легко, быстро и безопасно сможете<br/>переводить средства другим пользователям.
                            </div>
                            <div class='a4b'>
                                <span>Вы увидели на просторах интернета вещь,<br/>которая вам понравилась, но сомневаетесь в<br/>честности продавца?</span><br/><br/>Пригласите
                                его в GM и переведите средства<br/>через СБР, это убережет ваши средства от<br/>мошенников
                                в сети.<br/><br/>Благодаря СБР, ваши средства всегда в<br/>безопасности.
                            </div>
                        </div>
                        <div class='slider_button'><a href="#" class='slider_button1 goReg'>ПОДКЛЮЧИТЬСЯ</a></div>
                    </div>
                </div>

                <div id="slide06" class="slide">
                    <div class='slider_div a5'>
                        <div class='slider_s'>
                            <div class='a5a'>
                                <span>У вас есть свободные средства и вы хотели бы<br/>их приумножить?</span><br/><br/>Подключитесь
                                к GM, у нас есть сервис<br/>взаимовыгодного кредитования, где вы сами<br/>решаете,
                                кому и сколько дать!<br/><br/>Благодаря полностью автоматизированному<br/>сервису
                                кредитования, вы легко и быстро<br/>преумножите свои средства!
                            </div>
                            <div class='a5b'>
                                <span>У вас запланировалось мероприятие, но на него<br/>недостаточно средств?</span><br/><br/>Подключитесь
                                к GM и попросите других пользователей<br/>дать вам кредит, на хороших для обоих
                                сторон<br/>условиях!<br/><br/>Благодаря полностью автоматизированному<br/>сервису
                                кредитования, вы легко это сделаете!
                            </div>
                        </div>
                        <div class='slider_button'><a href="#" class='slider_button1 goReg'>ПОДКЛЮЧИТЬСЯ</a></div>
                    </div>
                </div>

                <div id="slide07" class="slide">
                    <div class='slider_div a6'>
                        <div class='slider_s'>
                            <div class='a6a'>
                                <span>У вас есть ненужные вещи и вы хотели бы их обменять на<br/>что-то другое?</span><br/><br/>Или
                                вы готовы оказать кому-нибудь услугу за услугу?<br/><br/>Подключитесь к GM и
                                экономьте средства на том, что вы<br/>обмениваетесь вещами и знаниями по
                                принципу:<br/>«ты мне - я тебе.»<br/><br/>Все просто! И это далеко еще не
                                все!<br/><span>Это GM!</span>
                            </div>
                        </div>
                        <div class='slider_button'><a href="#" class='slider_button1 goReg'>ПОДКЛЮЧИТЬСЯ</a></div>
                    </div>
                </div>
            </div>
            <a href="#" id="ui-carousel-next"><span>next</span></a>
            <a href="#" id="ui-carousel-prev"><span>prev</span></a>

            <div id="pages"></div>
        </div>
        <div class='clr'>
        </div>
    </div>
</div>
<script src="/js/jsc.js"></script>
<script type="text/javascript">
    function generatePages() {
        var _total, i, _link;

        _total = $("#carousel").rcarousel("getTotalPages");

        for (i = 0; i < _total; i++) {
            _link = $("<a href='#'></a>");

            $(_link)
                .bind("click", {page: i},
                function (event) {
                    $("#carousel").rcarousel("goToPage", event.data.page);
                    event.preventDefault();
                }
            )
                .addClass("bullet off")
                .appendTo("#pages");
        }

        // mark first page as active
        $("a:eq(0)", "#pages")
            .removeClass("off")
            .addClass("on")
            .css("background-image", "url(/img/page-on.png)");

    }

    function pageLoaded(event, data) {
        $("a.on", "#pages")
            .removeClass("on")
            .css("background-image", "url(/img/page-off.png)");

        $("a", "#pages")
            .eq(data.page)
            .addClass("on")
            .css("background-image", "url(/img/page-on.png)");
    }

    $("#ui-carousel-next")
        .add("#ui-carousel-prev")
        .add(".bullet")
        .hover(
        function () {
            $(this).css("opacity", 0.7);
        },
        function () {
            $(this).css("opacity", 1.0);
        }
    );
</script>
<script>
    $(document).ready(function () {
        /*setTimeout(function () {
         $('.wrap').fadeOut(50);
         $('#slider').fadeIn(500);
         $('.background').css({'background-color': 'rgba(255,255,255,1)'});
         $("#carousel").rcarousel(
         {
         visible: 1,
         step: 1,
         speed: 500,
         auto: {
         enabled: true
         },
         width: 1100,
         height: 550,
         start: generatePages,
         pageLoaded: pageLoaded
         }
         );
         }, 2000);*/
        $(document).on('click', '.goReg', function () {
            $('.wrap').fadeOut(50);
            $('body').css({'background': 'rgba(255,255,255,1)'});
            $('#slider').hide();
            $('#reg').fadeIn(500);

            return false;
        });
        $(document).on('click', '#forgotPass', function () {
            $('.wrap').fadeOut(50);
            $('body').css({'background': 'rgba(255,255,255,1)'});
            $('#slider').hide();
            $('#forgot').fadeIn(500);

            return false;
        });
        //forgotPass
    });
</script>
<div id="log"></div>
<!-- Footer -->
<footer>
    <div class="footer1">
        <div class="footer1a">
            <a href="/"><img src="/img/logo.png" alt="GM1LP"/></a>
            <span class="logo">GM1LP</span>
            <span class="yellow">2013-2017</span>
            <span class="menu">
                <a href="http://gm1lp.ru/help/agree" target="_blank">Пользовательское соглашение</a>
                <a href="http://gm1lp.ru/help/rules" target="_blank">Правила сообщества</a>
                <a href="http://gm1lp.ru/help/power" target="_blank">Возможности</a>
                <a href="http://gm1lp.ru/people" target="_blank">Люди</a>
                <a href="http://gm1lp.ru/company" target="_blank">Компании и Сообщества</a>
            </span>
        </div>
    </div>
</footer>
<script type="text/javascript" src="/js/main.js"></script>
<script>
    jQuery.fn.center = function (onlyLeft, onlyTop) {
        if (onlyLeft == undefined) {
            onlyLeft = true;
        }
        if (onlyTop == undefined) {
            onlyTop = true;
        }
        this.css({"position": "absolute", 'padding-top': '0'});
        if (onlyTop) {
            this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
            $(window).scrollTop()) - 20 + "px");
            console.log(Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
            $(window).scrollTop()));
        }
        if (onlyLeft) {
            this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
            $(window).scrollLeft()) + "px");
        }
        return this;
    };
    $(document).ready(function () {
        $('html,body').resize(function () {
            $('.wrap').center();
        });
        $('html,body').resize();
    });
</script>
</body>
</html>


<!--!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Авторизация</title>

        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/style.css">
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/fonts.css">
        <link rel="stylesheet" type="text/css" media="all" href="/application/views/site/css/lightbox.css">
        <!--<script type="text/javascript" src="/application/views/site/js/jq.js"></script>->
        <script src="/application/views/site/js/jquery-1.11.0.min.js"></script>
        <script src="/application/views/site/js/lightbox-2.6.min.js"></script>
        <script type="text/javascript" src="/application/views/site/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jcarousellite.js"></script>
        <script type="text/javascript" src="/application/views/site/js/jquery.mousewheel.min.js"></script>
        <script src="/application/views/site/js/common.js"></script>
        <script src="/application/views/site/js/jquery-ui.js"></script>
        <script>
            var referer = '<?php echo $referer; ?>';
        </script>
    </head>
    <body class="login-page">
        <div id="register">
            <img src="/application/views/site/images/reg-logo.png" alt="" class="reg-logo">
            <h2>Один аккаунт. Весь мир GM!</h2>
            <h3>Один аккаунт для всех сервисов GM</h3>
            <div class="servises">
                <img src="/application/views/site/img/smlogo1.png" alt="">
                <img src="/application/views/site/img/smlogo2.png" alt="">
                <img src="/application/views/site/img/smlogo3.png" alt="">
                <img src="/application/views/site/img/smlogo4.png" alt="">
                <img src="/application/views/site/img/smlogo5.png" alt="">
                <img src="/application/views/site/img/smlogo6.png" alt="">
                <img src="/application/views/site/img/smlogo7.png" alt="">
                <img src="/application/views/site/img/smlogo8.png" alt="">
                <img src="/application/views/site/img/smlogo9.png" alt="">
                <img src="/application/views/site/img/smlogo10.png" alt="">
                <img src="/application/views/site/img/smlogo11.png" alt="">
            </div>
<!--<h2>Контролируйте всё!</h2>->
<div id="autority" class="modal-win">
    <div class="modal-win-cont">
        <h2 class="modal-win-title">Авторизация</h2>
        <hr>
        <form  action="/page/login" method="post" class="auth-l" id="auth-l">
            <div class="placeholder">
                <label for="auth-email" class="text">E-mail...</label>
                <input type="text" name="login" id="auth-email" class="textField">
            </div>
            <div class="placeholder">
                <label for="auth-pass" class="text">Пароль...</label>
                <input type="password" name="password" id="auth-pass" class="textField">
            </div>
            <div class="send-form-left">
                <a href="/page/new_pass">Забыли пароль?</a>
                <input type="submit" value="Войти на сайт" class="enter" id="auth-enter">
            </div>
            <div class="send-form-right">
                <a href="/page/new_pass">Восстановить пароль</a>
                <div class="check-box">
                    <input type="checkbox" id="showPass"><label for="">Показать пароль</label>
                </div>
                <div class="check-box">
                    <input type="checkbox" id="" name="memeber" value="1"><label for="">Запомнить меня</label>
                </div>
            </div>
        </form>
        <div class="auth-r">
            <h2 class="rs-title">Добро пожаловать в систему GM.</h2>
            <p class="rs-advice" id="auth-status">
<?php echo $message; ?>
            </p>
<!--div style="font-weight: bold">
    <a href="/auth/vk">Войти через <img src="/img/vk.png" alt="Вконтакте" width="150" title="Вконтакте" style="padding-left: 5px;"/></a>
</div->
</div>
<div class="clear"></div>
<hr>
<p class="modal-win-bottom">
<a href="/page/register">Не зарегистрированы?</a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href="/page/register">Регистрируйся прямо сейчас!</a>
</p>
</div>
</div>
<!--<h2>Бросьте вызов всему!</h2>

<p class="italic">
    Зарегистрируйся сейчас и получи Премиум Аккаунт на 1 год - бесплатно!
    <br>Регистрируйся и забирай его уже сейчас!
    <br>Количество ограничено!
    <br>Всего 1 000 000!
</p>->
<div class="ind-rep">
    <div class="rep-inner">
        <img src="/application/views/site/images/rep-ins.jpg"/>
    </div>
</div>
<h2>Удобный и мобильный GM!</h2>
<img src="/application/views/site/images/mobil.png" alt="" class="mobil-gm">
</div>



<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div>
<script src="/application/views/site/js/nx.js"></script>
</body>
</html-->
