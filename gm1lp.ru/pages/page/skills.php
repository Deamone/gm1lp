<?php
/*
  ALTER TABLE  `users` ADD  `mwork` VARCHAR( 128 ) NOT NULL ,
  ADD  `dwork` VARCHAR( 128 ) NOT NULL ,
  ADD  `owork` VARCHAR( 128 ) NOT NULL ,
  ADD  `swork` VARCHAR( 128 ) NOT NULL ,
  ADD  `langs` VARCHAR( 128 ) NOT NULL

  ALTER TABLE  `users` CHANGE  `date`  `date` VARCHAR( 11 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
 *  */
$i = true;
$_USER = $USER;
#$text = new Memcache;
#$text->connect("localhost", 11201);
/* if (isset($_GET['del_link_vk']) && $USER['vk_uid']) {
  updateUserById($USER['id'], array('vk_uid'), array(''));
  $USER['vk_uid'] = $_USER['vk_uid'] = '';
  } */
///
$error = '';
if (isset($_GET['id']) && $_GET['id'] < 0) {
    $group = getGroup($_GET['id']);
    if (!$group || !$group['type']) {
        redirect('/groups?');
    }

    $members = getMembersInGroup($group['group_id']);
    if (isset($members['group'][10][$USER['id']]) || isset($members['group'][11][$USER['id']])) {
        $itsI = true;
    }
    if (!$itsI) {
        redirect('/groups');
    }
    $pid = $group['group_id'] * -1;
} else {
    $pid = $_USER['id'];
}
$skills = getUserSkills($pid);
if (isAjax()) {

    $post = array_flip($_POST['skills']);
    foreach ($skills as $skill) {
        if (!isset($post[$skill['name']])) {
            removeUserSkill($pid, $skill['skill_id']);
        } else {
            unset($post[$skill['name']]);
        }
    }
    if ($post) {
        foreach ($post as $skill => $i) {
            addUserSkill($pid, $skill);
        }
    }

    exit('$("#alertHeader").text("Успешно!");$("#alertMessage").text("Данные успешно сохранены!"); showModal(".alertMessage");');
}
header_($pid > 0 ? 'Мои навыки' : 'Предоставляемые услуги', '', '', ''
    . '<link rel="stylesheet" href="/css/chosen.css">'
    . '<script src="/js/chosen.jquery.js" type="text/javascript"></script>'
    . '<script src="/js/chosen.ajaxaddition.jquery.js" type="text/javascript"></script>');
?>
    <style>
        .chosen-container-multi .chosen-choices {
            width: 733px;
            height: 180px !important;
            padding: 10px 0 0 15px;
            border-radius: 7px;
            border: 1px solid #d3e4f5;
            resize: none;
            font-family: 'PT Sans', sans-serif;
            font-size: 16px;
            font-weight: normal;
        }

        .chosen-container.chosen-container-multi {
            width: 723px;
            height: 180px !important;
        }

        .chosen-container .chosen-drop {
            border: 0;
        }

        .search-field input {
        }

        .chosen-container .chosen-drop {
            top: 92%;
            width: 733px;
        }
    </style>

    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <?php if ($pid < 0) {
                echo '<div class="backme">
                <img src="/images/arrow.png"> <a href="/group?id=' . $group['group_id'] . '"> Вернуться назад </a>
            </div>';
            }
            ?>
            <form id="formSkills">
                <div class="div_left">
                    <h1 class="prof_h1"><?php echo $pid > 0 ? 'Навыки' : 'Предоставляемые услуги'; ?></h1>

                    <div class="prof_blok_left">
                        <h2>Укажите <?php echo $pid > 0 ? 'навыки' : 'предоставляемые услуги'; ?></h2>

                        <div class="prof_blok_pukt punkt_widh">
                            <p>Ваши <?php echo $pid > 0 ? 'навыки' : 'предоставляемые услуги'; ?></p>
                            <select name="skills[]" data-placeholder="Укажите навыки" id="skills"
                                    style="width: 90%;height: 90px"
                                    multiple>
                                <?php foreach ($skills as $skill) {
                                    echo '<option value="' . $skill['name'] . '" selected>' . $skill['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <a class="prof_butt nxSkills" href="#">Сохранить</a>


                </div>
            </form>
        </div>

        <div class="right_block">
            <!-- Contacts -->
            <div class="r_bl_menu">
                <ul>
                    <li><a href="/settings">Профиль</a></li>
                    <li><a href="/skills" class="akt">Навыки</a></li>
                    <li><a href="/private">Конфиденциальность</a></li>
                    <li><a href="/secure">Безопасность</a></li>
                    <li><a href="/balance">Баланс</a></li>
                </ul>
            </div>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('#skills').ajaxChosen({
                    dataType: 'json',
                    type: 'POST',
                    url: '/util/skills'
                }, {
                    loadingImg: '/images/loading.gif',
                    minLength: 1
                },
                {});
            $('#formSkills').submit(function () {
                $.post('', $(this).serialize(), function (data) {
                    eval(data);
                });
                return false;
            });
            $('.nxSkills').click(function () {
                $('#formSkills').submit();
                return false;
            });
        });
    </script>
<?php
footer();

