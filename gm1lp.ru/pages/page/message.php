<?php
if (isset($_GET['id'])) {
    $_USER = getUserById($_GET['id']);
}
if (!$_USER) {
    redirect('/?id=' . (int)$_GET['id']);
}

$uid = $USER['id'];
$peer_id = $_USER['id'];
$flags_in = (1 << 1); // unread
$flags_out = (1 << 2);
$flags_del = (1 << 3);
$legacy_id = 0;
$subject = "";
$peer_msg_id = 0;
///
$random_tag = mt_rand(0, 999999);

$path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/messages/';

if (isset($_GET['addPhoto'])) {
    $file = $_FILES['file'];
    $json = array();
    $ext = getExtension($file['name']);
    if ($ext != 'jpg' && $ext != 'png' && $ext != 'gif') {
        $json['error'] = 'Запрешенный формат файла: ' . $ext;
    } else {
        $file_name = $uid . '_' . $peer_id . '_' . time() . '.' . $ext;
        move_uploaded_file($file['tmp_name'], $path . $file_name);
        create_thumbnail($path . $file_name, $path . $file_name, 1000, 0, 'height');
        $json['file'] = $file_name;
    }

    exit(json_encode($json));
}

$text = new Memcache;
$text->connect("localhost", 11201);

$messages = array();

function getMessages($uid, $peer_id, $_USER, $offset = 1, $setMask = false)
{
    //return;
    global $messages, $text, $flags_out, $flags_in, $flags_del, $USER;
    $new_messages = 0;
    $x = $text->get("peermsglist{$uid}_{$peer_id}#{$offset},-1");
    $e = explode(',', $x);
    // $ccc = count($e) - 20;
    // if ($ccc < 1) {
        $ccc = 1;
    // }
    for ($i = $ccc; $i <= $e[0]; $i++) {
        $local_id = $e[$i];
        $_ = $text->get("message{$uid}_" . $local_id . '#4');

        $nx = explode("\t", $_);
        $ee = explode(',', $nx[0]);
        $peer_msg_id = $ee[3];

        $c_flag = (int)$ee[0];
        if (($c_flag & $flags_del)) {
            continue;
        }
        if (($c_flag & $flags_out)) {
            $ee[2] = $uid;
        } else {
            if (($c_flag & $flags_in)) {
                //echo $_ = $text->get("message{$peer_id}_" . $peer_msg_id.'#4');
                $text->decrement("flags{$uid}_{$local_id}", $flags_in);
                $text->decrement("flags{$peer_id}_{$peer_msg_id}", $flags_in);
                $c_flag = $c_flag ^ $flags_in;
            }
        }
        /*        $abs_mask = 1 << abs($setMask);
                if ($setMask !== false) {
                    if ($setMask < 0 && ($c_flag & $abs_mask)) {
                        $text->decrement("flags{$uid}_{$local_id}", $abs_mask);
                    } elseif ($setMask > 0 && !($c_flag & $abs_mask)) {
                        $text->increment("flags{$uid}_{$local_id}", $abs_mask);
                    }
                }*/
        $_USER = getUserById($ee[2]);
        $nx[2] = trim($nx[2]);
        $message = str_replace('  ', "\n<br/>\n", $nx[2]);
        preg_match_all('/\[(\w+)=(\d+)\]/i', $message, $attachments);
        $message = preg_replace('/\[(\w+)=(\d+)\]/i', '', $message);
        $attachments_ = '';
        $docs = '';
        foreach ($attachments[0] as $k => $v) {
            $key = $attachments[1][$k];
            $val = $attachments[2][$k];
            if ($key == 'video') {
                $video = getVideo($val);
                if (!$video) {
                    continue;
                }
                $attachments_ .= '<div class="gallery rounded_2">
                        <div class="gallery-image" data-video="' . $video['url'] . '">
                        </div>
                        <div class="gallery-details">
                            <a href="' . $video['url'] . '" target="_blank" class="link small">' . $video['title'] . '</a>
                        </div>
                    </div>';
            } elseif ($key == 'doc') {
                $doc = getDoc($val);
                if (!$doc) {
                    continue;
                }
                $docs .= '<br/><a class="wall_icon wall_icon_file normal" href="/uploads/docs/' . $doc['owner_id'] . '/' . $doc['url'] . '" target="blank">' . $doc['url'] . '</a>';
            }
        }
        $att = '<div class="attachments attach">' . $attachments_ . '</div>';
        $att .= '<div class="attachments docs">' . $docs . '</div>';
        # $message .= '<div class="attachments">' . $attachments_ . $docs . '</div>';
        $messages[$ee[1]] = array(
            'fam' => $_USER['fam'],
            'name' => $_USER['name'],
            'ava' => '/uploads/avatars/' . $_USER['ava'],
            'message' => $message,
            'files' => trim($nx[1]),
            'timestamp' => $ee[1],
            'local_id' => $local_id,
            'user_id' => $_USER['id'],
            'flags' => $c_flag,
            'attachments' => $att,
            'peer_msg_id' => $peer_msg_id
        );
        if ($ee[0] == 2) {
            $new_messages++;
        }
    }
    return $new_messages;
}

if (isset($_POST['message'])) {
    $message = trim($_POST['message']);
    $message = preg_replace('/[\ ]{2,}/', ' ', $message);
    if (isset($_POST['img'])) {
        foreach ($_POST['img'] as $v) {
            $_POST['file'][] = $path . $v;
        }
    }
    if (isset($_POST['file'])) {
        foreach ($_POST['file'] as $k => $img) {
            $img = str_replace(array('..', '/mini_'), array('', '/'), $img);
            $img = ltrim($img, '/');

            if (!file_exists($img)) {
                unset($_POST['file'][$k]);
            } else {
                $ext = getExtension($img);
                $file_name = $uid . '_' . $peer_id . '_' . time() . mt_rand(0, 9999) . '.' . $ext;
                rename($img, $path . $file_name);
                $_POST['file'][$k] = $file_name;
            }
        }
        $subject = implode(',', $_POST['file']);
    }

    $hide_data = $SYSTEM['post']['hide_data'];
    preg_match_all('/\[(\w+)=(\d+)\]/i', $hide_data, $arr);
    $hide_data = implode('', $arr[0]);

    $message = $message . $hide_data;


    $x = 0;
    $text->set('newmsg' . $uid . '#' . $random_tag, ($flags_out | $flags_in) . ',' . $peer_id . "\n" . $subject . "\t" . $message); // Исходящему
    $x = $text->get("newmsgid$uid#$random_tag");

    $text->set('newmsg' . $peer_id . '#' . $random_tag, $flags_in . ',' . $uid . ",$x\n" . $subject . "\t" . $message); // Принимающему


    exit();
} elseif (/* isAjax() && */
isset($_GET['get'])
) {
    getMessages($uid, $peer_id, $_USER, 1, -1);
    //getMessages($peer_id, $uid, $USER, 1, false);

    /*if ($messages) {*/
    ksort($messages);

    echo json_encode(array_values($messages));
    /* } else {
         echo '[{"fam":"\u0428\u043e\u0448\u0438\u043d","name":"\u0414\u043c\u0438\u0442\u0440\u0438\u0439","ava":"\/uploads\/avatars\/1423901513.4849.jpg","message":"\u041e, \u0434\u0438\u0430\u043b\u043e\u0433\u0438 \u0437\u0430\u043f\u0438\u043b\u0438\u043b \u0438 \u043f\u043e\u0438\u0441\u043a :-)  ","timestamp":"1421961070","local_id":"37","user_id":"41","flags":"0"},{"fam":"\u041c\u043e\u0440\u043e\u0437\u043e\u0432","name":"\u0420\u043e\u0441\u0442\u0438\u0441\u043b\u0430\u0432","ava":"\/uploads\/avatars\/1423683690.2662.jpg","message":"a?","timestamp":"1421993069","local_id":"138","user_id":"19279","flags":"0"},{"fam":"\u041c\u043e\u0440\u043e\u0437\u043e\u0432","name":"\u0420\u043e\u0441\u0442\u0438\u0441\u043b\u0430\u0432","ava":"\/uploads\/avatars\/1423683690.2662.jpg","message":"\u041f\u043e\u0447\u0442\u0438 \u043a\u0430\u043a \u043d\u0430\u0434\u043e \u0440\u0430\u0431\u043e\u0442\u0430\u0435\u0442","timestamp":"1421993098","local_id":"139","user_id":"19279","flags":"0"},{"fam":"\u0428\u043e\u0448\u0438\u043d","name":"\u0414\u043c\u0438\u0442\u0440\u0438\u0439","ava":"\/uploads\/avatars\/1423901513.4849.jpg","message":"\u0412\u0440\u043e\u0434\u0435 \u0434\u0430, \u043f\u043e\u0447\u0442\u0438...  ","timestamp":"1422013088","local_id":"47","user_id":"41","flags":"0"},{"fam":"\u0428\u043e\u0448\u0438\u043d","name":"\u0414\u043c\u0438\u0442\u0440\u0438\u0439","ava":"\/uploads\/avatars\/1423901513.4849.jpg","message":"\u0422\u0435\u0441\u0442.","timestamp":"1423154075","local_id":"51","user_id":"41","flags":"0"},{"fam":"\u0428\u043e\u0448\u0438\u043d","name":"\u0414\u043c\u0438\u0442\u0440\u0438\u0439","ava":"\/uploads\/avatars\/1423901513.4849.jpg","message":"\u0427\u0435 \u0442\u0430\u043c, \u0447\u0435 \u0442\u0430\u043c?  ","timestamp":"1423474783","local_id":"52","user_id":"41","flags":"0"}]';
     }*/


    exit();
} elseif (isset($_GET['counts'])) {
    $uids = $_POST['ids'];
    $count = array();
    foreach ($uids as $uid) {
        $messages = array();
        $c = getMessages($USER['id'], $uid, $USER, 1, false);
        $count[$uid] = array($uid, $c);
    }
    echo json_encode($count);
    exit;
} elseif (isset($_GET['del_message'])) {
    $id = (int)$_POST['id'];
    $text->delete("message{$uid}_{$id}");
    exit('1');
} elseif (isset($_GET['clear_messages'])) {
    //$peer_id = $_POST['id'];
    $offset = 1;
    $x = $text->get("peermsglist{$uid}_{$peer_id}#{$offset},-1");
    $e = explode(',', $x);
    for ($i = 1; $i <= $e[0]; $i++) {
        $local_id = $e[$i];
        $text->increment("flags{$uid}_{$local_id}", $flags_del);
    }
    exit('1');
}
addDialogCookie($_GET['id']);

/*    exit;
  $x = $text->get("message{$uid}_1");
  var_dump($x);
  $x = $text->get("peermsglist{$peer_id}_{$uid}#1,-1");

  var_dump($x);
  exit();
 * 
 */

header_('Переписка c ' . $_USER['name'], '', '', ''
    . '<link rel="stylesheet" href="/css/alldialog.css" type="text/css" media="screen" />
        <script type="text/javascript" src="/js/resize.area.js"></script>
        <script type="text/javascript" src="/js/vendor/jquery.ui.widget.js"></script>

        <script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
        <script type="text/javascript" src="/js/jquery.fileupload.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>
        <script type="text/javascript" src="/js/jquery.jscrollpane.min.js"></script>
            <link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js"></script>
  ', 'body-message');
$x = getDialogsCookie();
$c = count($x);
?>
    <style>
        .type-0, .type-1 {
            background-color: lightgray;
        }

        div[data-flag="6"] {
            /* Получатель не прочитал */
            background-color: whitesmoke; /*rgba(200,200,200,0.3);*/
        }

        div[data-flag="0"] {
            /* Получатель прочитал */
            background-color: white;
        }

        div[data-flag="2"] {
            /* Новое не прочитанное сообщение от отправителя */
            background-color: whitesmoke; /*rgba(200,200,200,0.3);*/
        }

        body.body-message .left_block_cont {
            padding-left: 0px;
            padding-top: 20px;
        }

        body.body-message .blok_mess {
            padding: 12px 0;
            padding-left: 29px;
        }

        body.body-message .scrollblock {
            overflow: auto;
            width: 780px;
            height: 490px;
        }

        .mess_fut {
            background: #f0f3f5;
            padding-left: 29px;
            margin-left: 0;
            padding-top: 16px;
            padding-bottom: 66px;
            margin-top: 2px;
        }

        .dia_blok a {
            text-decoration: none;
        }

        .body-search #main, .body-messages #main, .body-message #main {
            padding-bottom: 0;
        }

        *[data-mcid] {
            display: none;
        }
    </style>
    <input id="fileuploadForDialog" type="file" name="file" style="display: none;">
    <input id="fileuploadForWall" type="file"  name="file" style="display: none;">
    <div id="main" class="main_block centred">

        <div class="left_block">

            <div class="mess_nav">
                <a href="#" class="arrow"><img src="/images/arrow.png"/></a>
                <a class="h3" href="/messages"><h3>К списку диалогов</h3></a>
                <a class="a_act" href="#"><p>Действия</p></a>

                <div class="set-tooltip" style="display: none;"><a href="#" id="clearDialog"
                                                                   data-id="<?php echo $_USER['id']; ?>">Очистить
                        окно</a></div>
            </div>
            <div class="left_block_cont">
                <div class="list_message scrollblock">
                    <div id="messages">
                        <?php
                        $messages = null;
                        getMessages($uid, $peer_id, $_USER, 1, -1);
                        ksort($messages);
                        $messages_ = array_values($messages);
                        if ($messages) {
                            foreach ($messages_ as $message_) {
                                $f = '';
                                /*
                                 *
                            if (data[i].files) {
                                var files = data[i].files.split(',');
                                var html = '';
                                $.each(files, function (i, src) {
                                    html += '<a class="fancybox" href="/uploads/messages/' + src + '" rel="messages"><img src="/uploads/messages/' + src + '" /></a>';
                                });
                                data[i].message = html + '<span>' + data[i].message + '</span>';
                            }
                                 */
                                if ($message_['user_id'] == $USER['id'] && $message_['flags'] == 2) {
                                    $message_['flags'] = 3;
                                }
                                if ($message_['files']) {
                                    $files = explode(',', $message_['files']);
                                    foreach ($files as $file) {
                                        $f .= '<a class="fancybox" href="/uploads/messages/' . $file . '" rel="messages"><img src="/uploads/messages/' . $file . '" /></a>';
                                    }
                                }
                                $message_['message'] = $f . '<span>' . $message_['message'] . '</span>';
                                echo '<div class="blok_mess" data-flag="' . $message_['flags'] . '" data-timestamp="' . $message_['timestamp'] . '" data-id="' . $message_['user_id'] . '_' . $message_['local_id'] . '">
                                    <a href="/id' . $message_['user_id'] . '"><img class="rounded_3" src="' . $message_['ava'] . '" /></a>
                                    <a href="/id' . $message_['user_id'] . '">' . $message_['name'] . '</a>
                                    <p class="mess_data" data-s_time="' . $message_['timestamp'] . '">' . $date . '
                                        <a href="#" class="deleteMessage" data-id="' . $_USER['id'] . '" data-mid="' . $message_['local_id'] . '"><img src="/images/icon-delete-blue.png" /></a>
                                    </p>
                                    <p class="mess_p">' . $message_['message'] . '</p>
                                    ' . $message_['attachments'] . '
                                </div>';
                            }
                        }

                        ?>
                    </div>
                </div>

                <div class="mess_fut">
                    <a href="/id<?php echo $uid; ?>"><img class="rounded_3"
                                                          src="<?php echo '/uploads/avatars/' . $USER['ava']; ?>"/></a>
                    <a href="/id<?php echo $peer_id; ?>" class="img2_msg"><img class="rounded_3"
                                                                               src="<?php echo '/uploads/avatars/' . $_USER['ava']; ?>"/></a>

                    <form id="formMessage">
                        <textarea class="textarea_msg" id="message" name="message"
                                  placeholder="Введите сообщение"></textarea>

                        <div id="myEmojiField2"></div>
                        <input class="butt_otp" type="submit" value="Отправить" id="send">
                        <!--a href="#" class="smile_butt"><img src="images/butt_smile.png"/></a>
                        <a href="#" class="foto_butt addphoto"><img src="images/butt_foto_load.png"/></a-->
                        <a href="#" id="uploadWall" class="wall_icon wall_icon_photo normal"></a>
                        <a href="#" id="uploadWallVideo" class="wall_icon wall_icon_video normal"></a>
                        <a href="#" id="uploadWallDoc" class="wall_icon wall_icon_file normal"></a>

                        <div class="mphotos">
                        </div>
                        <ul class="photos_main" id="uploadList">
                        </ul>
                        <input type="hidden" class="hide_data" name="hide_data"/>
                    </form>


                </div>

            </div>
        </div>

        <div class="right_block">
            <!-- Contacts -->
            <div>
                <div class="block_header">
                    <h2>Диалоги</h2>
                    <span class="counter rounded_3"><a href="/messages" id="countDialogs"><?php echo $c; ?></a></span>

                    <div class="clear"></div>
                </div>
                <div class="block_content" id="activeDialogs">
                    <?php
                    for ($i = 0; $i < $c; $i++) {
                        echo '<div data-id="' . $x[$i]['id'] . '" class="dia_blok ' . ($x[$i]['id'] == $_GET['id'] ? 'dia_blok_act' : '') . '">
                    <a href="/message?id=' . $x[$i]['id'] . '"><img class="rounded_3" src="' . '/uploads/avatars/' . $x[$i]['ava'] . '" />
                    <p class="dia_name ' . ($x[$i]['id'] == $_GET['id'] ? 'dia_name_act' : '') . '">' . $x[$i]['name'] . ' ' . $x[$i]['fam'] . ' <span class="amount_new" data-mcid="' . $x[$i]['id'] . '">+0</span></p>
                    </a>
                    <a href="#" class="closeDialog"><img class="closer" src="images/close.png" /></a>
                </div>';
                    }
                    ?>
                </div>
            </div>

            <!-- Companies -->

        </div>

        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        var newMessages = 0;
        var scroll = 0;
        $(document).ready(function () {
            $('.fancybox').fancybox({
                'padding': [20, 20, 0, 20],
                'beforeShow': function () {
                    $('.fancybox-skin').after('<div class="loading">Идет загрузка</div>');
                },
                'afterShow': function () {
                    $this = this.element;

                    $.post('/album/album-ajax', 'photo_id=' + $this.attr('data-photo-id'), function (data) {
                        $('.fancybox-skin').after(data);
                        $('.fancybox-skin').parent().find('.loading').remove();
                    });
                    return false;
                }
            });
            $('.addphoto').click(function () {
                getVar = 'addPhoto=1&id=' + current_dialog_id;
                $('#fileuploadForDialog').fileupload({
                    url: '/message?' + getVar,
                    dataType: 'json',
                    done: function (e, data) {
                        if (data.result.error === undefined) {
                            /*$('.mphotos a').each(function () {
                             $(this).click();
                             });*/
                            $('.mphotos').append('<div><img src="/uploads/messages/' + data.result.file + '" /><a data-file="' + data.result.file + '" href="#"></a>');
                            $('form#formMessage').append('<input type="hidden" name="img[]" value="' + data.result.file + '"/>');
                        } else {
                            alert(data.result.error);
                        }
                        console.log(data);
                    },
                    progressall: function (e, data) {

                    }
                }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
                $('#fileuploadForDialog').click();
                return false;
            });
            $(document).on('click', '.mphotos a', function () {
                var file = $(this).attr('data-file');
                $(this).parent().remove();
                $('form#formMessage input[value="' + file + '"]').remove();
                return false;
            });
            /*********
             *
             *
             * **/
            scroll = $('.scrollblock').jScrollPane({});
            scroll = scroll.data('jsp');

            $('.dia_blok').click(function () {
                $('.dia_blok').removeClass("dia_blok_act");
                $(this).addClass("dia_blok_act");

                $('.dia_blok').children('.dia_name').removeClass("dia_name_act");
                $(this).children('.dia_name').addClass("dia_name_act");

                $('.dia_blok').children('.amount_new').removeClass("amount_new_act");
                $(this).children('.amount_new').addClass("amount_new_act");

                $('.dia_blok').children('a').children('.closer').removeClass("closer_act");
                $(this).children('a').children('.closer').addClass("closer_act");
            });
            $(document).on('click', '.closeDialog', function () {
                var id = $(this).parent().attr('data-id');
                $(this).parent().remove();
                cookies.set('dialog_' + id, '0', -1);
                var size = $('#activeDialogs div').size();
                $('#countDialogs').text(size);
                if (!size) {
                    window.location = "/messages";
                }
                return false;
            });

            $('#send').on('click', function () {
                $('#formMessage textarea').val(kemojiMessage.getValue(KEmoji.HTML_VALUE));
                if (!$.trim(($('#formMessage textarea').val().replace('&nbsp;', ''))) && !$('#uploadList li').size()) {
                    return false;
                }
                console.log($.trim(($('#formMessage textarea').val().replace('&nbsp;', ''))));
                $.post('/message?id=<?php echo $peer_id; ?>', $('#formMessage').serialize(), function (data) {
                    $('#formMessage textarea').val('');
                    $('.KEmoji_Input div[contenteditable]').html('');
                    $('#uploadList a').each(function () {
                        $(this).click();
                    });
                    console.log(data);
                    im();
                });
                return false;
            });
            $('.KEmoji_Input div[contenteditable]').focus();
            $('.KEmoji_Input div[contenteditable]')
                .off('keydown')
                .on('keydown', function (event) {
                    if (event.which != 13 || event.ctrlKey || event.shiftKey || event.altKey) {
                        return;
                    }
                    if (event.which == window.KEY.ENTER) {
                        $('#send').click();
                        return;
                    }

                });
            //ajax_mes();
            $('img').load(function () {
                updateScroll();
            });
        });
        var current_dialog_id = '<?php echo $peer_id; ?>';
        var peer_ava = '/uploads/avatars/<?php echo $_USER['ava']; ?>';
        var my_ava = '/uploads/avatars/<?php echo $USER['ava']; ?>';
        //////
        function ajax_mes() {
            //$('#messages').html('');
            $.post("/message?id=" + current_dialog_id + "&get=1", '',
                function (data, textStatus) {
                    for (var i = 0; i < data.length; i++) {
                        var date_ = date_js(data[i].timestamp);
                        if ($('div[data-id="' + data[i].user_id + '_' + data[i].local_id + '"]').size()) {
                            var li = $('li[data-id="' + data[i].user_id + '_' + data[i].local_id + '"]');
                            if (li.attr('data-flag') != data[i].flags) {
                                li.attr('data-flag', data[i].flags);
                                console.log('replace!');
                            }
                        }
                        if (!$('div[data-id="' + data[i].user_id + '_' + data[i].local_id + '"]').size()) {
                            newMessages++;
                            if (data[i].user_id == "<?php echo $USER['id']; ?>" && data[i].flags == 2) {
                                data[i].flags = 3;
                            }
                            if ($('.blok_mess[data-timestamp="' + data[i].timestamp + '"]').size()) {
                                return;
                            }
                            if (data[i].files) {
                                var files = data[i].files.split(',');
                                var html = '';
                                $.each(files, function (i, src) {
                                    html += '<a class="fancybox" href="/uploads/messages/' + src + '" rel="messages"><img src="/uploads/messages/' + src + '" /></a>';
                                });
                                data[i].message = html + '<span>' + data[i].message + '</span>';
                            }
                            $('#messages').append('<div class="blok_mess" data-flag="' + data[i].flags + '" data-timestamp="' + data[i].timestamp + '" data-id="' + data[i].user_id + '_' + data[i].local_id + '"><a href="/id' + data[i].user_id + '"><img class="rounded_3" src="' + data[i].ava + '" /></a><a href="/id' + data[i].user_id + '">' + data[i].name + '</a><p class="mess_data">' + date_ + '<a href="#" class="deleteMessage" data-id="' + current_dialog_id + '" data-mid="' + data[i].local_id + '"><img src="/images/icon-delete-blue.png" /></a></p><p class="mess_p"><span class="realText">' + data[i].message + '</span>' + data[i].attachments + '</p></div>');
                            $('*[data-video]').each(function () {
                                var qthis = $(this);
                                setTimeout(preview_video(qthis), 100);
                            });
                            clickable($('.blok_mess[data-timestamp="' + data[i].timestamp + '"] .mess_p .realText'));
                            $('.blok_mess:last img').load(function () {
                                updateScroll();
                            });
                            //'<li data-flag="' + data[i].flags + '" data-timestamp="' + data[i].timestamp + '" data-id="' + data[i].user_id + '_' + data[i].local_id + '"><div class="avatar"><img src="' + data[i].ava + '" /></div><div class="name">' + data[i].name + ' ' + data[i].fam + '<span class="date">' + date_ + '</span></div><p>' + data[i].message + '</p><!--a href="/page/let/mes?del=274&id=1" OnCLick="del_mes(274);" class="del-message"></a--></li>');
                        }
                    }
                    //clickable($('.blok_mess .mess_p'));
                    updateScroll();
                }, "json");
            ajax_count();
        }

        setInterval('im()', 3000);
    </script>

<?php
footer();
