<?php
$itsI = TRUE;
$_USER = $USER;

header_('Поиск', '', '', ''
    . '<script type="text/javascript" src="/application/views/site/js/nx.js"></script>
', 'body-search all-search');

$myVideos = getVideos($USER['id']);
$count_videos = count($myVideos);

$myContacts = getContacts($USER['id']);
$cMyContacts = getCountContacts($USER['id']);

# Right
$priv_photo = isPrivate($USER['id'], $USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($USER['id'], $USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($USER['id'], $USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($USER['id'], $USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right

$location = isset($_GET['slocation']) ? $_GET['slocation'] : array(0);
$location = array_flip($location);

$querySearch = isset($_GET['query']) ? trim($_GET['query']) : '';

$onlyUsers = false;
$onlyNews = false;
$onlyGroup1 = false;
$onlyGroup2 = false;
$onlyVideo = false;

$fUsers = 0;
$fCompanies = 0;
$fCommunities = 0;
$fVideos = 0;

$users = array();
$companies = array();
$communities = array();
$videos = array();
if ($querySearch) {
    if (isset($location[1])) {
        $users = findUser($querySearch);
        $fUsers = count($users);
        $onlyUsers = true;
    }
    if (isset($location[3])) {
        $companies = findGroup($querySearch, 1);
        $fCompanies = count($companies);
        $onlyGroup1 = true;
    }
    if (isset($location[4])) {
        $communities = findGroup($querySearch, 0);
        $fCommunities = count($communities);
        $onlyGroup2 = true;
    }
    if (isset($location[5])) {
        $videos = findVideo($querySearch);
        $fVideos = count($videos);
        $onlyVideo = true;
    }

    if (isset($location[0])) {
        $_ = findUser($querySearch, 4, true);
        $fUsers = $_[0];
        $users = $_[1];
        $_ = findGroup($querySearch, 1, 4, true);
        $fCompanies = $_[0];
        $companies = $_[1];
        $_ = findGroup($querySearch, 0, 4, true);
        $fCommunities = $_[0];
        $communities = $_[1];
        $_ = findVideo($querySearch, 2, true);
        $fVideos = $_[0];
        $videos = $_[1];
    }
}


?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <img src="/images/arrow.png"> <a href="/">вернуться назад</a>
            </div>
            <div class="block_search">
                <form action="search">
                    <input name="query" type="text" autocomplete="off" class="search_left" id="searchy"
                           value="<?php echo $querySearch; ?>" placeholder="Поиск людей, сообществ, компани..."/>
                </form>
                <?php
                if (strlen($querySearch) < 3) {
                    echo '<small>Укажите больше символов</small>';
                }
                ?>
            </div>
            <div class="block_header searchFilter" style="display: block">
                <?php
                echo '<a href="?query=' . urlencode($querySearch) . '&slocation[]=0"' . (isset($location[0]) ? 'class="active"' : '') . '>Все</a>';
                echo '<a href="?query=' . urlencode($querySearch) . '&slocation[]=1"' . (isset($location[1]) ? 'class="active"' : '') . '>Люди</a>';
                #echo '<a href="?query=' . urlencode($querySearch) . '&slocation[]=2"' . (isset($location[2]) ? 'class="active"' : '') . '>Новости</a>';
                echo '<a href="?query=' . urlencode($querySearch) . '&slocation[]=3"' . (isset($location[3]) ? 'class="active"' : '') . '>Сообщества</a>';
                echo '<a href="?query=' . urlencode($querySearch) . '&slocation[]=4"' . (isset($location[4]) ? 'class="active"' : '') . '>Компании</a>';
                echo '<a href="?query=' . urlencode($querySearch) . '&slocation[]=5"' . (isset($location[5]) ? 'class="active"' : '') . '>Видеозаписи</a>';
                ?>

                <div class="clear"></div>
            </div>

            <div class="block_content">
                <?php
                if ($users) {
                    echo '<div class="searchBlock"><h3>Поиск выдал ' . declOfNum($fUsers, array(' человек', ' человека', ' людей')) . '</h3>';
                    foreach ($users as $k => $user) {
                        $loc = '&nbsp;';
                        $action = '<a href="#" class="icontact" data-id="' . $user['id'] . '" data-status="-1">Установить контакт</a>';
                        $contactStatus = -1;
                        if (isset($myContacts[$user['id']])) {
                            if ($myContacts[$user['id']][1] == 1) {
                                $action = '<a href="/message?id=' . $user['id'] . '" data-id="' . $user['id'] . '">Пообщаться</a>';
                            } elseif ($myContacts[$user['id']][1] == 2) {
                                $action = '<a href="#" class="icontact" data-id="' . $user['id'] . '" data-status="3">Заявка отклонена</a>';
                            } elseif ($myContacts[$user['id']][1] == 0) {
                                $action = '<a href="#" class="icontact" data-id="' . $user['id'] . '" data-status="0">Заявка отправлена</a>';
                            } else {
                                $action = '<a href="#" class="icontact" data-id="' . $user['id'] . '" data-status="-1">Установить контакт</a>';
                            }
                        }
                        if ($user['city']) {
                            if (is_numeric($user['city'])) {
                                $ciy = getCity($user['city']);
                                $loc = $ciy['city'];
                            } else {
                                $loc = $user['city'];
                            }
                        }
                        if ($user['country']) {
                            if (is_numeric($user['country'])) {
                                $coy = getCountry($user['country']);
                                if ($loc) {
                                    $loc = $loc . ', ' . $coy['country'];
                                } else {
                                    $loc = $coy['country'];
                                }
                            } else {
                                if ($loc) {
                                    $loc = $loc . ', ' . $user['country'];
                                } else {
                                    $loc = $user['country'];
                                }
                            }
                        }
                        echo '<div class="sContact">
                <div class="scLeft">
                    <a href="/id' . $user['id'] . '"><img class="rounded_3" src="' . '/uploads/avatars/' . $user['ava'] . '" /></a>
                </div>
                <div class="scRight">
                    <a href="/id' . $user['id'] . '" class="yname">' . $user['name'] . ' ' . $user['fam'] . '</a><br/>
                    <span class="middle">' . $loc . '</span><br/>
                    <span class="href rounded_3">' . $action . '</span>
                </div>
            </div>';
                        if ($k == 0 || ($k % 2) == 0) {
                            echo '<span class="delimer"></span>';
                        }
                    }
                    if (!$onlyUsers) {
                        echo '<div><a href="?query=' . urlencode($querySearch) . '&slocation[]=1" class="more">Перейти ко всем найденным людям</a></div>';
                    }
                    echo '</div>';
                }
                /**
                 * Компании
                 */
                if ($companies) {
                    $iMembers_ = getUserGroups($USER['id'], 1, false, false);
                    $iMembers = array();
                    foreach ($iMembers_ as $groups) {
                        $iMembers[$groups['group_id']] = 1;
                    }
                    echo '<div class="searchBlock companies"><h3>Поиск выдал ' . declOfNum($fCompanies, array(' компания', ' компании', ' компаний')) . '</h3>';
                    foreach ($companies as $k => $comp) {
                        $loc = '&nbsp;';
                        $action = '<a href="/group?id=' . $comp['group_id'] . '&amp;join=1" class="groupJoin">Вступить</a>';
                        $contactStatus = -1;
                        if (isset($iMembers[$comp['group_id']])) {
                            $action = '<a href="/group?id=' . $comp['group_id'] . '&amp;join=0" class="groupUnJoin">Покинуть</a>';
                        }
                        $loc = $comp['type'] ? 'Компания' : 'Сообщество';
                        $podp = $comp['type'] ? declOfNum($comp['members'], array(' сотрудник', ' сотрудника', ' сотрудников')) : declOfNum($comp['members'], array(' подписчик', ' подписчика', ' подписчиков'));
                        echo '<div class="sContact">
                <div class="scLeft">
                    <a href="/group' . $comp['group_id'] . '"><img class="rounded_3" src="' . '/uploads/g_avatars/' . $comp['ava'] . '" /></a>
                </div>
                <div class="scRight">
                    <a href="/group' . $comp['group_id'] . '" class="yname">' . $comp['title'] . '</a><br/>
                    <span class="middle title">' . $loc . '</span>
                    <span class="middle">' . $podp . '</span>
                    <span class="href rounded_3">' . $action . '</span>
                </div>
            </div>';
                        if ($k == 0 || ($k % 2) == 0) {
                            echo '<span class="delimer"></span>';
                        }
                    }
                    if (!$onlyGroup1) {
                        echo '<div><a href="?query=' . urlencode($querySearch) . '&slocation[]=3" class="more">Перейти ко всем найденным компаниям</a></div>';
                    }
                    echo '</div>';
                }
                /**
                 * Сообщества
                 */
                if ($communities) {
                    $iMembers_ = getUserGroups($USER['id'], 0, false, false);
                    $iMembers = array();
                    foreach ($iMembers_ as $groups) {
                        $iMembers[$groups['group_id']] = 1;
                    }
                    echo '<div class="searchBlock companies"><h3>Поиск выдал ' . declOfNum($fCommunities, array(' сообщество', ' сообщества', ' сообществ')) . '</h3>';
                    foreach ($communities as $k => $comp) {
                        $loc = '&nbsp;';
                        $action = '<a href="/group?id=' . $comp['group_id'] . '&amp;join=1" class="groupJoin">Вступить</a>';
                        $contactStatus = -1;
                        if (isset($iMembers[$comp['group_id']])) {
                            $action = '<a href="/group?id=' . $comp['group_id'] . '&amp;join=0" class="groupUnJoin">Покинуть</a>';
                        }
                        $loc = $comp['type'] ? 'Компания' : 'Сообщество';
                        $podp = $comp['type'] ? declOfNum($comp['members'], array(' сотрудник', ' сотрудника', ' сотрудников')) : declOfNum($comp['members'], array(' подписчик', ' подписчика', ' подписчиков'));
                        echo '<div class="sContact">
                <div class="scLeft">
                    <a href="/group' . $comp['group_id'] . '"><img class="rounded_3" src="' . '/uploads/g_avatars/' . $comp['ava'] . '" /></a>
                </div>
                <div class="scRight">
                    <a href="/group' . $comp['group_id'] . '" class="yname">' . $comp['title'] . '</a><br/>
                    <span class="middle title">' . $loc . '</span>
                    <span class="middle">' . $podp . '</span>
                    <span class="href rounded_3">' . $action . '</span>
                </div>
            </div>';
                        if ($k == 0 || ($k % 2) == 0) {
                            echo '<span class="delimer"></span>';
                        }
                    }
                    if (!$onlyGroup1) {
                        echo '<div><a href="?query=' . urlencode($querySearch) . '&slocation[]=4" class="more">Перейти ко всем найденным сообществам</a></div>';
                    }
                    echo '</div>';
                }
                /**
                 * Видео
                 */
                if ($videos) {
                    echo '<div class="searchBlock videos"><h3>Поиск выдал ' . declOfNum($fVideos, array(' видеозапись', ' видеозаписи', ' видеозаписей')) . '</h3>';
                    foreach ($videos as $k => $video) {
                        echo '<div class="gallery rounded_2">
                        <div class="gallery-image" data-video="' . $video['url'] . '">
							<a class="preview_video" href="' . $video['url'] . '" target="_blank"></a>
						</div>
                        <div class="gallery-details">
                            <a href="' . $video['url'] . '" target="_blank" class="link small">' . $video['title'] . '1</a>
                        </div>
                    </div>';

                        if ($k == 0 || ($k % 2) == 0) {
                            echo '<span class="delimer"></span>';
                        }
                    }
                    if(($k%2)==0){
                        echo '<div class="gallery rounded_2" style="border: none"></div>';
                    }
                    if (!$onlyVideo) {
                        echo '<div class="more"><a href="?query=' . urlencode($querySearch) . '&slocation[]=5" class="more">Перейти ко всем найденным компаниям</a></div>';
                    }
                    echo '</div>';
                }
                ?>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($USER['id'], $priv_companies);
            echo show_main_communities($USER['id'], $priv_communities);
            echo show_main_album($USER['id'], $priv_photo);
            echo show_main_videos($USER['id'], $priv_video);
            echo show_main_audios($USER['id'], $priv_audios);
            ?>
        </div>

        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $.each($('.yname'), function () {
                var text = $(this).text();
                if (text.length > 17) {
                    $(this).text(text.substr(0, 14) + '...');
                }
            });
        });
    </script>
<?php
footer();
