<?php
/*
  ALTER TABLE  `users` ADD  `mwork` VARCHAR( 128 ) NOT NULL ,
  ADD  `dwork` VARCHAR( 128 ) NOT NULL ,
  ADD  `owork` VARCHAR( 128 ) NOT NULL ,
  ADD  `swork` VARCHAR( 128 ) NOT NULL ,
  ADD  `langs` VARCHAR( 128 ) NOT NULL

  ALTER TABLE  `users` CHANGE  `date`  `date` VARCHAR( 11 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
 *  */
$i = true;
$_USER = $USER;
#$text = new Memcache;
#$text->connect("localhost", 11201);
/* if (isset($_GET['del_link_vk']) && $USER['vk_uid']) {
  updateUserById($USER['id'], array('vk_uid'), array(''));
  $USER['vk_uid'] = $_USER['vk_uid'] = '';
  } */
///
if (!$_USER['url']) {
    $_USER['url'] = 'id' . $_USER['id'];
}
$error = '';
if (count($_POST) && isAjax()) {
    $keys = array('name', 'fam', 'relation', 'tel', 'skype', 'site', 'muz', 'films', 'books', 'sports',
        'mwork', 'dwork', 'owork', 'swork', 'langs', 'country', 'city', 'gender');
    $values = array($_POST['name'], $_POST['fam'], $_POST['relation'], $_POST['tel'], $_POST['skype'], $_POST['site'], $_POST['muz'],
        $_POST['films'], $_POST['books'], $_POST['sports'],
        $_POST['mwork'], $_POST['dwork'], $_POST['owork'], $_POST['swork'], $_POST['langs'], $_POST['country'], $_POST['city'],
        $_POST['gender'] == 2 ? 'male' : 'female'
    );
    if ($USER['url'] != $_POST['url']) {
        if (preg_match('/id(\d+)/i', $_POST['url']) && $_POST['url'] != 'id' . $USER['id']) {
            $error .= 'Запрещенный Красивый ID';
        } elseif (preg_match('/group(\d+)/i', $_POST['url']) && $_POST['url'] != 'group' . $USER['id']) {
            $error .= 'Запрещенный Красивый ID';
        } elseif (!preg_match('/^[a-z0-9_-]{5,64}$/i', $_POST['url'])) {
            $error .= 'Красивый ID может состоять из букв, цифр, дефисов и подчёркиваний. Длина от 5 до 64 символов';
        } elseif ((($u = getUserByUrl($_POST['url'])) && $u['id'] != $USER['id']) || ($g = getGroupByUrl($_POST['url']))) {
            $error .= 'Красивый ID уже занят';
        } else {
            $keys[] = 'url';
            $values[] = $_POST['url'];
        }
    }
    if (empty($_POST['name']) || empty($_POST['fam'])) {
        $error = 'Укажите фамилию и имя';
    }
    $d = $_POST['b_day'];
    $m = $_POST['b_month'];
    $y = $_POST['b_year'];

    $keys[] = 'date';
    $values[] = mktime(0, 0, 0, $m, $d, $y);

    //$USER = getUserById($USER['id']);
    if ($error) {
        exit('alert("' . str_replace('"', "'", $error) . '");');
    } else {
        updateUserById($USER['id'], $keys, $values);
    }
    exit('$("#alertHeader").text("Успешно!");$("#alertMessage").text("Данные успешно сохранены!"); showModal(".alertMessage");');
}
header_('Настройки', '', '', ''
    . '<link rel="stylesheet" href="/css/chosen.css">'
    . '<script src="/js/chosen.jquery.js" type="text/javascript"></script>'
    . '<script src="/js/chosen.ajaxaddition.jquery.js" type="text/javascript"></script>');
$date = date('d.m.Y', $USER['date']);
$u_date = explode('.', $date);

$USER['gender'] = $USER['gender'] == 'male' ? 2 : 1;
#print_r($USER);
$citys = getCity2($USER['city']);
$countrys = getCountry2($USER['country']);
?>

    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <form class="setForm">
                <div class="div_left">
                    <h1 class="prof_h1">Профиль</h1>
                    <!--1-->
                    <div style="float: right;margin: -55px 20px 0 0">
						<a href="/help/invites" target="_blank">Код для приглашения</a>: <b><?php echo $USER['id']; ?> (или <?php echo $USER['url']; ?>)</b> <br/>
						<a href="/invites" target="_blank">Список приглашенных мною</a><br/>
					</div>
                    <div class="prof_blok_left">
                        <h2>Коротко о главном</h2>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Фамилия</p>
                            <input name="fam" placeholder="Укажите фамилию" type="text"
                                   value="<?php echo $USER['fam']; ?>">
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Имя</p>
                            <input name="name" placeholder="Укажите имя" type="text"
                                   value="<?php echo $USER['name']; ?>">
                        </div>
                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>День рождения</p>

                            <div class="styled-select styled-select_1">
                                <select name="b_day">
                                    <?php
                                    for ($i = 1; $i <= 31; $i++) {
                                        echo '<option value="' . $i . '" ' . ($i == $u_date[0] ? 'selected' : '') . '>' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="styled-select styled-select_2">
                                <select name="b_month">
                                    <?php
                                    for ($i = 1; $i <= 12; $i++) {
                                        echo '<option value="' . $i . '" ' . ($i == $u_date[1] ? 'selected' : '') . '>' . $months[$i] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="styled-select styled-select_3">
                                <select name="b_year">
                                    <?php
                                    for ($i = 1970; $i <= 2013; $i++) {
                                        echo '<option value="' . $i . '" ' . ($i == $u_date[2] ? 'selected' : '') . '>' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Семейное положение</p>

                            <div class="styled-select" id="gender">
                                <select id="sex_2">
                                    <?php
                                    foreach ($_LANG['PROFILE_RELATION'][2] as $k => $v) {
                                        echo '<option  value="' . $k . '" ' . ($k == $USER['relation'] ? 'selected' : '') . '>' . $v . '</option>';
                                    }
                                    ?>
                                </select>
                                <select id="sex_1">
                                    <?php
                                    foreach ($_LANG['PROFILE_RELATION'][1] as $k => $v) {
                                        echo '<option  value="' . $k . '" ' . ($k == $USER['relation'] ? 'selected' : '') . '>' . $v . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Место работы</p>
                            <input name="mwork" placeholder="Введите место" type="text"
                                   value="<?php echo $USER['mwork']; ?>">
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Должность</p>
                            <input name="dwork" placeholder="Введите должность" type="text"
                                   value="<?php echo $USER['dwork']; ?>">
                        </div>
                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Опыт работы на текущей должности</p>
                            <input name="owork" placeholder="1 год" type="text" value="<?php echo $USER['owork']; ?>">
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Общий стаж работы</p>

                            <div class="styled-select">
                                <select name="swork">
                                    <?php
                                    for ($i = 0; $i <= 14; $i++) {
                                        echo '<option value="' . $i . '" ' . ($i == $USER['swork'] ? 'selected' : '') . '>' . $stazh[$i] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Пол</p>

                            <div class="styled-select">
                                <select name="gender" id="gender">
                                    <?php
                                    if ($USER['gender'] == 2) {
                                        echo '<option value="2" selected="selected">Мужской</option>';
                                        echo '<option value="1">Женский</option>';
                                    } else {
                                        echo '<option value="2">Мужской</option>';
                                        echo '<option value="1" selected="selected">Женский</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="prof_blok_pukt relative">
                            <p>Красивый ID/ссылка на страницу</p>
                            <input name="url" min="5" maxlength="64" type="text" data-url="<?php echo $USER['url']; ?>"
                                   value="<?php echo $USER['url']; ?>">

                            <div class="url"></div>
                        </div>
                    </div>

                    <!--2-->
                    <div class="prof_blok_left">
                        <h2>Контактные данные</h2>

                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Страна</p>

                            <div class="">
                                <select name="country" class="chosen-select-country" data-placeholder="Выберите страну">
                                    <option value="0">Не выбран</option>
                                    <?php
                                    if ($countrys) {
                                        echo '<option value="' . $countrys['country_id'] . '" selected>' . $countrys['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Город</p>

                            <div class="">
                                <select name="city" class="chosen-select-city" data-placeholder="Выберите город">
                                    <option value="0">Не выбран</option>
                                    <?php
                                    if ($citys) {
                                        echo '<option value="' . $citys['city_id'] . '" selected>' . $citys['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Мобильный телефон</p>
                            <input placeholder="Ваш телефон" name="tel" type="text" value="<?php echo $USER['tel']; ?>">
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Электронная почта</p>
                            <input placeholder="Ваша почта" type="text" value="<?php echo $USER['login']; ?>"
                                   disabled="">
                        </div>
                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Skype</p>
                            <input placeholder="Ваш скайп" name="skype" type="text"
                                   value="<?php echo $USER['skype']; ?>">
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Сайт</p>
                            <input placeholder="Ваш сайт" name="site" type="text" value="<?php echo $USER['site']; ?>">
                        </div>
                    </div>

                    <!--3-->
                    <!--div class="prof_blok_left">
                        <h2>Образование</h2>
                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Тип заведения</p>
                            <div class="styled-select">
                                <select>
                                    <option value="Высшее учебное заведение">Высшее учебное заведение</option>
                                    <option value="Среднее специльное учебное заведение">Среднее специльное учебное заведение</option>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Учебное заведение</p>
                            <div class="styled-select">
                                <select>
                                    <option value="ЧПИ(филиал МГОУ)">ЧПИ(филиал МГОУ)</option>
                                    <option value="ЧПУ(филиал МГОУ)">ЧПУ(филиал МГОУ)</option>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt punkt_widh">
                            <p>Факультет</p>
                            <textarea placeholder="Введите факультет"></textarea>
                        </div>
                        <div class="prof_blok_pukt prof_blok_pukt_left">
                            <p>Форма обучения</p>
                            <div class="styled-select">
                                <select>
                                    <option value="Заочное отделение">Заочное отделение</option>
                                    <option value="Очное отделение">Очное отделение</option>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Статус</p>
                            <div class="styled-select">
                                <select>
                                    <option value="Выпускник (магистр)">Выпускник (магистр)</option>
                                    <option value="Выпускник">Выпускник</option>
                                </select>
                            </div>
                        </div>
                        <div class="prof_blok_pukt">
                            <p>Период учебы</p>
                            <p class="p_span">с</p>
                            <div class="styled-select p_span styled-select_2">
                                <select>
                                    <option value="2004">2004</option>
                                    <option value="2005">2005</option>
                                </select>
                            </div>
                            <p class="p_span">до</p>
                            <div class="styled-select p_span styled-select_2">
                                <select>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                </select>
                            </div>
                        </div>
                    </div-->

                    <!--4-->
                    <div class="prof_blok_left">
                        <h2>Языки</h2>

                        <div class="prof_blok_pukt punkt_widh">
                            <p>Какие языки вы знаете?</p>
                            <textarea placeholder="Перечислите через запятую"
                                      name="langs"><?php echo $USER['langs']; ?></textarea>
                        </div>
                    </div>

                    <!--5-->
                    <div class="prof_blok_left">
                        <h2>Интересы</h2>

                        <div class="prof_blok_pukt punkt_widh">
                            <p>Музыка</p>
                            <textarea name="muz"><?php echo $USER['muz']; ?></textarea>
                        </div>
                        <div class="prof_blok_pukt punkt_widh">
                            <p>Книги</p>
                            <textarea name="books"><?php echo $USER['books']; ?></textarea>
                        </div>
                        <div class="prof_blok_pukt punkt_widh">
                            <p>Фильмы</p>
                            <textarea name="films"><?php echo $USER['films']; ?></textarea>
                        </div>
                        <div class="prof_blok_pukt punkt_widh">
                            <p>Спорт</p>
                            <textarea name="sports"><?php echo $USER['sports']; ?></textarea>
                        </div>
                    </div>

                    <a class="prof_butt nxSet" href="#">Сохранить</a>


                </div>
            </form>
        </div>

        <div class="right_block">
            <!-- Contacts -->
            <div class="r_bl_menu">
                <ul>
                    <li><a href="/settings" class="akt">Профиль</a></li>
                    <li><a href="/skills">Навыки</a></li>
                    <li><a href="/private">Конфиденциальность</a></li>
                    <li><a href="/secure">Безопасность</a></li>
                    <li><a href="/balance">Баланс</a></li>
                </ul>
            </div>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('.nxSet').click(function (e) {
                $.post('/settings', $('form').serialize(), function (data) {
                    eval(data);
                });

                return false;
            });
            $('select#gender').change(function () {
                $('#sex_1').removeAttr('name').hide();
                $('#sex_2').removeAttr('name').hide();
                $('#sex_' + $(this).val()).attr('name', 'relation').show();
            });
            $('#sex_1').removeAttr('name').hide();
            $('#sex_2').removeAttr('name').hide();
            $('#sex_<?php echo $USER['gender']; ?>').attr('name', 'relation').show();

            $('.chosen-select-city').ajaxChosen({
                dataType: 'json',
                type: 'POST',
                url: '/util/city'
            }, {
                generateUrl: function (q) {
                    return '/util/city?country=' + $('select[name="country"]').val();
                },
                loadingImg: '/images/loading.gif',
                minLength: 1
            });
            $('.chosen-select-country').ajaxChosen({
                dataType: 'json',
                type: 'POST',
                url: '/util/country'
            }, {
                loadingImg: '/images/loading.gif',
                minLength: 1
            });
        });
    </script>
<?php
footer();

