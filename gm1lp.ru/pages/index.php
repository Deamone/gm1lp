<?php
$iUser = true;
if (!$USER) {
#    redirect('/page/login');
    $USER = array(
        'ava' => 'avatar-main.jpg'
    );
    $iUser = false;
}
$_USER = $USER;
$itsI = true;
$class = ' noti';
$_ = false;
$id = isset($_GET['_route_']) ? query_escape($_GET['_route_']) : '';
$uid = $SYSTEM['get']['uid'];

if(empty($uid)) {
	$uid = $_USER['id'];
}

if ($id) {
    $_ = getUserByUrl($id);
    if (!$_) {
        $_ = getGroupByUrl($id);
        if ($_) {
            include $_SERVER['DOCUMENT_ROOT'] . '/pages/groups/group.php';
            exit;
        }
    }
} elseif (isset($_GET['id'])) {
    $_ = getUserById((int)$_GET['id']);
    if ($_ && !isAjax() && !empty($_['url'])) {
        redirect($_['url']);
    }
} elseif (!isAjax() && !empty($USER['url']) && !isset($_GET['nx'])) {
    redirect($USER['url']);
}
if ($_) {
    $_USER = $_;
    if ($_['id'] != $USER['id']) {
        $itsI = false;
    }
}

if (!isset($_USER['id']) && !$iUser) {
    redirect('/page/login');
}

if ($itsI) {
    $class = '';
    if (isAjax() || isset($_GET['nx'])) {
        $json = array();
        if (isset($SYSTEM['get']['upload'])) {
            $file = itOneFile($_FILES['file']);
            $ext = getExtension($file["name"]);
            if (strpos($ext, 'ph') !== false || strpos($ext, 'png') !== false) {
                exit($json);
            }
            if (isset($SYSTEM['get']['iavatar'])) {
                $iavatar = $SYSTEM['get']['iavatar'];
                if ($iavatar) {
                    $file = prepareImg($_POST);
#var_dump($file);
#echo '=='.file_exists($file).'==';
                    $file_name = str_replace('tmp/', '', $file);
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/avatars/';
                    copy($file, $path . $file_name);
                    create_thumbnail($path . $file_name, $path . $file_name, 320, 320, "height");
                    updateUserById($USER['id'], array('ava'), array($file_name));
                    exit('' . $file_name);
                } else { // Слайд после crop
                    $file = prepareImg($_POST);
                    //$file = str_replace('..', '', 'tmp/' . $path);
                    $file_name = str_replace('tmp/', '', $file);
                    $file_name = $USER['id'] . '.png';
                    $new_name = $USER['id'] . '.png';
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/slide/';
                    copy($file, $path . $file_name);
                    #exit('' . $file_name);
                    #crop($path . $file_name, $path . $new_name);
                    create_thumbnail($path . $file_name, $path . $new_name, 1181, 478, "width");
                    exit('' . $file_name);
                }

            } elseif (isset($SYSTEM['get']['avatar'])) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/avatars/';
                $time = time();
                if ($SYSTEM['get']['avatar'] == 1) {
                    /*move_uploaded_file($file["tmp_name"], $path . '' . $json['file']);
                    create_thumbnail($path . '' . $json['file'], $path . '' . $json['file'], 156, 156, "none");

                    updateUserById($USER['id'], array('ava'), array($json['file']));*/
                } else {
                    $file = itOneFile($_FILES['file']);
                    $ext = getExtension($file["name"]);
                    if (strpos($ext, 'ph') !== false) {
                        exit($json);
                    }
                    $json['file'] = 'tmp/' . time() . '.' . $ext;
                    move_uploaded_file($file["tmp_name"], $json['file']);
                    create_thumbnail($json['file'], $json['file'], 1376, 478, "height");
                    //create_thumbnail($json['file'], $json['file'], 960, 529, "height");
                    exit(json_encode($json));
                }
            } else {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/slide/';
                $time = time();
                if ($SYSTEM['get']['slide'] == 1) {
                    /*$json['file'] = $USER['id'] . '.';
                    move_uploaded_file($file["tmp_name"], $path . '' . $json['file'] . getExtension($file["name"]));
                    create_thumbnail($path . '' . $json['file'] . getExtension($file["name"]), $path . '' . $json['file'] . '.jpg', 1181, 470, "none");
                    $json['file'] .= getExtension($file["name"]);*/
                } else { // Загрузка фото, до манипуляций
                    $file = itOneFile($_FILES['file']);
                    $ext = getExtension($file["name"]);
                    if (strpos($ext, 'ph') !== false || strpos($ext, 'png') !== false) {
                        exit($json);
                    }

                    $json['file2'] = 'tmp/' . time() . '.png';
                    $json['file'] = 'tmp/' . time() . '.' . $ext;
                    move_uploaded_file($file["tmp_name"], $json['file']);
                    create_thumbnail($json['file'], $json['file'], 1376, 478, "height");
                    exit(json_encode($json));
                }
            }
            exit(json_encode($json));
        }
    }
}
if (isAjax()) {
    /** Skill */
    if (isset($_GET['copy_video']) && $iUser) {
        copyVideo($USER['id'], $_POST['id']);
        exit();
    }
    if (isset($_GET['likeSkill']) && $iUser) {
        likeSkill($_POST['id']);
        echo showSkill($_POST['id'], false, false, true);
        exit();
    } /** Wall * */
    elseif (isset($_GET['del_post']) || isset($_POST['del_post'])) {
        $id = (int)$_POST['id'];
        $post = getPost($id);
        if ($post && ($post['user_id'] == $USER['id'] || $post['owner_id'] == $USER['id'])) {
            removePost($id);
        }
        exit('удаляем');
    } elseif (isset($_POST['editPost'])) {
        $id = (int)$_POST['id'];
        $post = getPost($id);
        if ($post && ($post['user_id'] == $USER['id'] || $post['owner_id'] == $USER['id'])) {
            $json = array();
            $json['files'] = unserialize($post['images']);
            preg_match_all('/\[(\w+)=(\d+)\]/i', $post['text'], $attachments);
            foreach ($attachments[0] as $k => $v) {
                $key = $attachments[1][$k];
                $val = $attachments[2][$k];
            }
        }
    } elseif (isset($_GET['del_comment']) && isAjax()) {
        $pid = (int)$_POST['pid'];
        $cid = (int)$_POST['cid'];
        $post = getPost($pid);
        $comments = query("SELECT `user_id` FROM `nx_wall_comments` WHERE `post_id`='" . $pid . "' AND `cid`='" . $cid . "';");

        if ($post && ($post['owner_id'] == $USER['id'] || $USER['id']) == $comments['row']['user_id']) {
            query("DELETE FROM `nx_wall_comments` WHERE `post_id`='" . $pid . "' AND `cid`='" . $cid . "';");
        }
        exit('удаляем');
    } elseif (isset($_POST['add_post']) && $iUser) {
        $post = $SYSTEM['post']['text'];
        $file = $SYSTEM['post']['file'];
		$uid  = $SYSTEM['post']['uid'];
		
        if ($file) {
            foreach ($file as $k => $file_) {
                $name = str_replace('mini_', '', substr($file_, strrpos($file_, '/')));
                if (!file_exists('uploads/wall/' . $uid . '/' . $name)) {
                    unset($file[$k]);
                } else {
                    $file[$k] = 'uploads/wall/' . $uid . '/' . $name;
                }
            }
        }
        $hide_data = $SYSTEM['post']['hide_data'];
        preg_match_all('/\[(\w+)=(\d+)\]/i', $hide_data, $arr);
        $hide_data = implode('', $arr[0]);
        addPost($_USER['id'], $post . $hide_data, false, serialize($file));
        exit(outputPost(getPost(query_lastInsertId())));
    } elseif (isset($_POST['all_comments'])) {
        $wid = $SYSTEM['post']['wid'];
        exit(outputCommentsPost($wid, true));
    } elseif ((isset($_GET['hide_photo']) || isset($_POST['hide_photo'])) && $itsI) {
        $pid = $SYSTEM['post']['pid'];
        $uid = $USER['id'];
        query("UPDATE `nx_photos` SET `show_home`=0 WHERE `user_id`='$uid' AND `photo_id`='$pid';");
        exit('1');
    } elseif (isset($_POST['wall_offset'])) { ///////////////////////// TARGET
        $offset = $SYSTEM['post']['wall_offset'];
        $posts = getPosts($_USER['id'], $offset, 5, isset($_COOKIE['ewall']) ? $_COOKIE['ewall'] : 0);
        foreach ($posts as $post) {
            echo outputPost($post, isset($_COOKIE['wallc']) ? $_COOKIE['wallc'] : 0, isset($_COOKIE['wallsc']) ? $_COOKIE['wallsc'] : 0);
        }
        exit();
    } elseif (isset($_POST['add_comment']) && $iUser) {
        $post = $SYSTEM['post']['text'];
        $wid = $SYSTEM['post']['wid'];
        addPostComment($wid, $post);
        exit(outputCommentsPost($wid));
    } elseif (isset($_POST['get_post'])) {
        $id = $_POST['get_post'];
        $post = getPost($id);
        echo $post['text'];
        exit();
    } elseif (isset($_POST['edit_post'])) {
        $id = $_POST['id'];
        $post = getPost($id);
        if ($post && $post['user_id'] == $USER['id']) {
            updatePost($id, $_POST['post']);
        }
        exit('1');
    } elseif (isset($_POST['add_like']) && $iUser) {
        $id = $_POST['id'];
        if (isset($_POST['comment'])) {
            likeComment($id);

            exit('2');
        } else {
            likePost($id);
        }

        exit('1');
    } elseif (isset($_POST['repost']) && $iUser) {
        $id = $_POST['id'];
        $t = rePost($id);

        exit('' . $t);
    }
    /** .Wall * */
    #?like=get&wid='+wall_id
    if (isset($_GET['like']) && isset($_GET['wid']) && $iUser) {
        $likes = getLikesPost($_GET['wid'], false, 9999, true);
        exit(json_encode($likes));
    }
    if (isset($_GET['like']) && isset($_GET['cid']) && $iUser) {
        $likes = getLikesComments($_GET['cid'], false, 999);
        exit(json_encode($likes));
    }
}

if (isAjax() && $iUser) {
    $json = array();
    if (isset($SYSTEM['get']['uploadWall'])) {
        $file = $_FILES['file'];
        $uid = $SYSTEM['get']['uid'];
        $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/wall/' . $uid . '/';
        if (!file_exists($path)) {
            mkdir($path);
        }
		foreach($file['tmp_name'] as $key => $tmp_name) {
			$time = str_replace(' ', '_', microtime());
			$time = str_replace('.', '_', $time);
			
			
			if(!preg_match("/^image/", mime_content_type($tmp_name))){
				http_response_code(400);
				$json[]['error'] = 'Загруженный файл имеет неподдерживаемый формат.';
			} else {

				$ext = getExtension($file['name'][$key]);
				$file_name = $time . '.' . $ext;
				
				
				move_uploaded_file($tmp_name, $path . '' . $file_name);
				create_thumbnail($path . '' . $file_name, $path . 'mini_' . $file_name, 71, 71, "none");

				$json[] = array(
'file' => '' . $file_name,
'thumb'=> 'mini_'.$file_name
);
			}
		}
	
        exit(json_encode(array('files' => $json, '_files' => $file)));
    }
    if (isset($SYSTEM['get']['uploadWallDoc'])) {
        $file = itOneFile($_FILES['file']);
        $uid = $USER['id'];
        $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/docs/' . $uid . '/';
        if (!file_exists($path)) {
            mkdir($path);
        }
        $time = time();

        $ext = getExtension($file["name"]);
        if(!preg_match("/(docx?|xlsx?|pptx?|rtf|pdf|gif|psd|djvu|fb2|ps|txt|cdr|rar|zip|doc|xls|ppt)/", $ext)){
            http_response_code(400);
            exit(json_encode(array('error' => 'Загруженный файл имеет неподдерживаемый формат.')));
            
        }		
		
        if($file["size"] > '2097152'){
            http_response_code(400);
            exit(json_encode(array('error' => 'Размер загруженного файла превышает 2мб.')));
        }

        $file['name'] = str_replace('.' . $ext, '', $file['name']);
        $json['file'] = translit($file['name']) . '.' . $ext;
        move_uploaded_file($file["tmp_name"], $path . '' . $json['file']);

        $docId = addDoc($json['file']);
        $json['doc'] = $docId;
        $json['image'] = 0;
        if (is_image($path . $json['file'])) {
            $json['image'] = 1;
        }

        exit(json_encode($json));
    }
}

/* * ******************* */


if (isset($_GET['getuser'])) {
    $user = getUserById($_POST['id']);
    if (!$user) {
        exit('{avatar:"",name:"",fam:""}');
    } else {
        exit(json_encode(array(
            'name' => $user['name'],
            'fam' => $user['fam'],
            'avatar' => '/uploads/avatars/' . $user['ava']
        )));
    }
}

if (isset($_GET['setAvatar'])) {
    $avatart = str_replace('..', '', $_GET['setAvatar']);
    $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/avatars/';

    if (file_exists($path . $avatart)) {
        updateUserById($USER['id'], array('ava'), array($avatart));
        redirect('/' . $USER['url']);
    }
} elseif (isset($_GET['delphoto'])) {
    updateUserById($USER['id'], array('ava'), array('default.jpg'));
    exit;
}



  if (isset($_GET['add_friend']) && !$itsI) {
  userFriend($USER['id'], $_USER['id']);
  exit('1');
  } elseif (isset($_GET['del_friend']) && !$itsI) {
  userFriend($USER['id'], $_USER['id'], true);
  exit('-1');
  }
////////////////////////////////////////
#$friends = getUserFriends($_USER['id']);
  $__=query("SELECT * FROM `new_1lp`.`nx_friends`");
  $_=array();
  foreach($__['rows'] as $f){
  $_[$f['from']][$f['to']] += $f['status'];
  $_[$f['to']][$f['from']] += $f['status'];
  }
  foreach($_ as $uid=>$friends_id){
  foreach($friends_id as $id=>$status){
  if($status==2){
  $USER['id']=$uid;
  userFriend($uid, $id);
  $USER['id']=$id;
  userFriend($id, $uid);
  }
  }
  }
  /*print_r($_);
  exit; */
 $friend = false;
  if (isset($friends[0][$USER['id']])) {
  $friend = 1;
  }
  if (isset($friends[2][$USER['id']])) {
  $friend = 2;
  }
  if (isset($friends[1][$USER['id']])) {
  $friend = 3;
  }/* */

updateUserById($USER['id'], array('online'), array(time()));
if (!$itsI) {
    updateUserById($_USER['id'], array('see'), array($_USER['see'] + 1));
}
$age = '';
if ($_USER['date'] && is_numeric($_USER['date'])) {
    $d = date('d.m.Y', $_USER['date']);
    $age = declOfNum((getAgeFromDate($d)), array(' год', ' года', ' лет'));
}
$citys = getCity2($_USER['city']);
$countrys = getCountry2($_USER['country']);
$photos = getUserPhotos($_USER['id'], false, true);

$cMyContacts = getCountContacts($_USER['id']);
$myContacts = getContacts($_USER['id']);

# Right
$priv_photo = isPrivate($_USER['id'], $_USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($_USER['id'], $_USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($_USER['id'], $_USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($_USER['id'], $_USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right
$priv_age = isPrivate($_USER['id'], $_USER['private3'], AGE_ALL, AGE_C, $myContacts);
$priv_sp = isPrivate($_USER['id'], $_USER['private'], SP_ALL, SP_C, $myContacts);
$priv_work = isPrivate($_USER['id'], $_USER['private'], WORK_ALL, WORK_C, $myContacts);
$priv_loc = isPrivate($_USER['id'], $_USER['private'], LOCATION_ALL, LOCATION_C, $myContacts);
$priv_phone = isPrivate($_USER['id'], $_USER['private'], PHONE_ALL, PHONE_C, $myContacts);
$priv_wall = isPrivate($_USER['id'], $_USER['private2'], WALL_ALL, WALL_C, $myContacts);
$priv_ewall = isPrivate($_USER['id'], $_USER['private2'], EWALL_ALL, EWALL_C, $myContacts);
$priv_wallc = isPrivate($_USER['id'], $_USER['private2'], WALLC_ALL, WALLC_C, $myContacts);
$priv_wallsc = isPrivate($_USER['id'], $_USER['private3'], WALLSC_ALL, WALLSC_C, $myContacts);

setcookie('wall', $priv_wall ? 1 : 0, time() + time(), '/');
setcookie('ewall', $priv_ewall ? 1 : 0, time() + time(), '/');
setcookie('wallc', $priv_wallc ? 1 : 0, time() + time(), '/');
setcookie('wallsc', $priv_wallsc ? 1 : 0, time() + time(), '/');

$posts = getPosts($_USER['id'], 0, 5, $priv_ewall);
$c = count($posts);

$skills = getUserSkills($_USER['id'], true);
///
$contactStatus = -1;
if (!$itsI) {
    $realMyContact = getContacts($USER['id']);
    if (isset($realMyContact[$_USER['id']])) {
        if ($realMyContact[$_USER['id']][1]) {
            $contactStatus = 1;
        } else {
            $contactStatus = 0;
        }
    }
}
header_($_USER['name'] . ' ' . $_USER['fam'], '', '', ''
    . '<script type="text/javascript" src="/js/vendor/jquery.ui.widget.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="/js/jquery.fileupload.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>
            <link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
            <link rel="stylesheet" href="/css/jquery.Jcrop.min.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="image_src" href="/uploads/avatars/' . $_USER['ava'] . '">
    <meta property="og:image" content="/uploads/avatars/' . $_USER['ava'] . '" />

', 'body-main');
$slide_img = '/images/slide.jpg';
if (file_exists('uploads/slide/' . $_USER['id'] . '.png')) {
    $slide_img = '/uploads/slide/' . $_USER['id'] . '.png';
}
?>
<!--main-->
<div id="main" class="main_block centred">
    <div class="main_slider">
        <div class="slider_toolbox<?php echo $class; ?>"><a href="#"><img src="/images/icon-toolbox-pencil.png"></a></div>
        <div class="slider_info bg" style="background-image: url(<?php echo $slide_img; ?>);">></div>
        <div class="slider_info">
            <?php echo iconStatus($_USER['status'], true); ?>
            <div class="sinfo_img">
                <div class="sinfo_img_toolbox<?php echo $class; ?>"><a href="#"><img src="/images/icon-toolbox-photo.png"></a></div>
                <div class="avaTariff" style="position: relative; display: inline-block;">
                    <img class="circled" src="<?php echo '/uploads/avatars/' . $_USER['ava']; ?>" id="avatar" />
                    <img class="verIco" width="48" src="<?php echo iconVer($_USER); ?>"/>
                </div>
            </div>

            <div class="sinfo_name"><?php echo $_USER['name'] . ' ' . $_USER['fam']; ?></div>

            <div class="sinfo_age">
            <?php echo $priv_age ? $age : ''; ?>
            </div>

            <!--span>Место работы:</span> <a href="#">GM</a><br />
            <span>Место учебы:</span> <a href="#">МГОУ</a><br /-->
            <?php
if ($priv_work) {
    if ($_USER['mwork']) {
        echo '<span>Место работы:</span> <a href="#">' . $_USER['mwork'] . '</a><br />';
    }
    if ($_USER['dwork']) {
        echo '<span>Должность:</span> <a href="#">' . $_USER['dwork'] . '</a><br />';
    }
}
if ($priv_loc) {
    if ($_USER['city']) {
        echo '<span>Место проживания:</span> <a href="#">' . $citys['name'] . '</a><br />';
    }
    if ($_USER['country']) {
        echo '<span>Страна:</span> <a href="#">' . $countrys['name'] . '</a><br />';
    }
}
if ($priv_sp) {
    if ($_USER['relation']) {
        $q = $_USER['gender'] == 'male' ? 2 : 1;
        echo '<a href="#">' . $_LANG['PROFILE_RELATION'][$q][$_USER['relation']] . '</a>';
    }
}
?>
            <div class = "sinfo_amount">
                <?php echo ((time() - $_USER['online']) < 15 * 60) ? '<div class="online">в сети</div>' : '<div class="offline">' . ($_USER['gender'] == 'female' ? 'была' : 'был') . ' в сети <span data-s_time="' . $_USER['online'] . '"></span> </div>'; ?>
                <span class = "sinfo_contacts"><?php echo declOfNum($cMyContacts['count'], array(' контакт', ' контакта', ' контактов')); ?></span>
                <span><?php echo declOfNum($_USER['see'], array(' просмотр', ' просмотра', ' просмотров')); ?></span>
            </div>
        </div>
        <?php

echo '<div class="slideNx" style="background-image: url(' . $slide_img . ');">';

if (!$itsI && $iUser) {
    echo '<div class="cont">
            <button id="addContact" data-id="' . $_USER['id'] . '" data-status="' . $contactStatus . '" class="rounded_3 icontact">Добавить в контакты</button>
            ' . (isPrivate($USER['id'], $_USER['private'], MESSAGE_ALL, MESSAGE_C, $myContacts) ? '<button id="sendMessage" data-id="' . $_USER['id'] . '" class="rounded_3">Отправить сообщение</button>' : '') . '
            <button id="sendMoney" class="rounded_3">Перевести средства</button>
        </div>';
}
?>
    </div>
</div>

<div class="left_block">
<?php if ($photos[1]): ?>
    <div class="block_header">
        <h2>Фотографии</h2>
        <span class="counter rounded_3"><a
                href="/albums?id=<?php echo $_USER['id']; ?>"><?php echo $photos[0]; ?></a></span>

        <div class="clear"></div>
    </div>

    <div class="block_content">
        <ul class="photos_main" style="float: left; height: 113px">
            <?php $max = 100;
            foreach ($photos[1] as $photo) {
                if ($max-- <= 0) {
                    break;
                }
                echo '<li>
                ' . ($itsI ? '<div class="pmain_toolbox"><a href="#" class="hideHomePhoto" data-photo-id="' . $photo['photo_id'] . '"><img src="/images/icon-toolbox-close.png"></a></div>' : '') . '
                <a data-photo-id="' . $photo['photo_id'] . '" href="/uploads/albums/' . $photo['album_id'] . '/' . $photo['photo_id'] . '.jpg" class="fancybox" rel="photos"><img class="rounded_3" src="/uploads/albums/' . $photo['album_id'] . '/thumb' . $photo['photo_id'] . '.jpg" /></a>
            </li>';
            } ?>
        </ul>
        <div class="clear"></div>
    </div>
<?php endif; ?>

    <div class="block_header" id="shortAboutH">
        <h2>Коротко о главном</h2>
        <?php
if ($itsI) {
    echo '<a class="toolbox" href="/settings">Редактировать</a>
        ';
}
?>
        <div class="clear"></div>
    </div>

    <div class="block_content noopen" id="shortAbout">
        <table class="short_info">
            <?php
if (is_numeric($_USER['date']) && $priv_age) {
    echo '<tr>
                <td class="first_col">День рождения</td>
                <td class="second_col">' . sym_date(date('d m Y', $_USER['date']), ' ') . '</td>
            </tr>';
}
if ($_USER['country'] && $priv_loc) {
    echo '<tr>
                <td class="first_col">Страна</td>
                <td class="second_col">' . $countrys['name'] . '</td>
            </tr>';
}
if ($_USER['city'] && $priv_loc) {
    echo '<tr>
                <td class="first_col">Город</td>
                <td class="second_col">' . $citys['name'] . '</td>
            </tr>';
}
if ($_USER['relation'] && $priv_sp) {
    $q = $_USER['gender'] == 'male' ? 2 : 1;
    echo '<tr>
                <td class="first_col">Семейное положение</td>
                <td class="second_col">' . $_LANG['PROFILE_RELATION'][$q][$_USER['relation']] . '</td>
            </tr>';
}
if ($_USER['mwork'] && $priv_work) {
    echo '<tr>
                <td class="first_col">Место работы</td>
                <td class="second_col">' . $_USER['mwork'] . '</td>
            </tr>';
}
if ($_USER['dwork'] && $priv_work) {
    echo '<tr>
                <td class="first_col">Должность</td>
                <td class="second_col">' . $_USER['dwork'] . '</td>
            </tr>';
}
if ($_USER['owork']) {
    echo '<tr>
                <td class="first_col">Опыт работы на текущей должности</td>
                <td class="second_col">' . $_USER['owork'] . '</td>
            </tr>';
}
if ($_USER['owork']) {
    echo '<tr>
                <td class="first_col">Общий стаж работы</td>
                <td class="second_col">' . $stazh[$_USER['swork']] . '</td>
            </tr>';
}
if ($_USER['skype']) {
    echo '<tr style="display: none">
                <td class="first_col">Skype</td>
                <td class="second_col">' . $_USER['skype'] . '</td>
            </tr>';
}
if ($_USER['tel'] && $priv_phone) {
    echo '<tr style="display: none">
                <td class="first_col">Телефон</td>
                <td class="second_col">' . $_USER['tel'] . '</td>
            </tr>';
}
if ($_USER['site']) {
    echo '<tr style="display: none">
                <td class="first_col">Сайт</td>
                <td class="second_col"><a href="http://' . str_replace(array('http://', 'https://'), array('', ''), $_USER['site']) . '" target="blank">' . $_USER['site'] . '</a></td>
            </tr>';
}
if ($_USER['muz']) {
    echo '<tr style="display: none">
                <td class="first_col">Любимая музыка</td>
                <td class="second_col">' . $_USER['muz'] . '</td>
            </tr>';
}
if ($_USER['books']) {
    echo '<tr style="display: none">
                <td class="first_col">Любимые книги</td>
                <td class="second_col">' . $_USER['books'] . '</td>
            </tr>';
}
if ($_USER['films']) {
    echo '<tr style="display: none">
                <td class="first_col">Любимые фильмы</td>
                <td class="second_col">' . $_USER['films'] . '</td>
            </tr>';
}
if ($_USER['sports']) {
    echo '<tr style="display: none">
                <td class="first_col">Спорт</td>
                <td class="second_col">' . $_USER['sports'] . '</td>
            </tr>';
}
if ($_USER['langs']) {
    echo '<tr style="display: none">
                <td class="first_col">Знание языков</td>
                <td class="second_col">' . $_USER['langs'] . '</td>
            </tr>';
}
?>
        <tr>
            <td colspan="2"><a href="#" id="showAllInfo">Показать подробную информацию</a></td>
        </tr>
        </table>

        <!--a href="#" class="dotted">Показать подробную информацию</a-->
    </div>

    <?php if ($skills): ?>
    <div class="block_header">
        <h2>Лучшие навыки и репутация</h2>
        <?php if ($itsI) {
            echo '<a class="toolbox" href="/skills">Редактировать</a>';
        } ?>
        <div class="clear"></div>
    </div>
    <div class="block_content">

        <!--Skills -->

        <table class="skills_table">
            <?php foreach ($skills as $i => $skill) {
                echo showSkill($skill['skill_id'], $skill, $i);
            }
            ?>
        </table>
        <p>
            <?php
            if (count($skills) > 5) {
                echo $_USER['name'] . ' владеет еще ' . declOfNum(count($skills) - 5, array('навыком', 'навыками', 'навыками')) . ' <a href="#" id="showAllSkills" class="dotted">Показать</a>';
            }
            ?>
        </p>

    </div>
<?php endif; ?>


    <div class="wall_wrapper">
    <?php if ($priv_wall && $USER['id']): ?>
    <div class="wall_post_comment">
        <div class="wall_post_profile">
            <img class="width46" src="<?php echo '/uploads/avatars/' . $USER['ava']; ?>">
        </div>
        <div class="wall_post_content">
            <form action="" method="post" id="wallPost">
			
				<input id="fileupload" multiple type="file" name="file[]" style="display: none;">
				<input id="fileuploadForWall" multiple type="file" name="file[]" style="display: none;">
				<input id="fileuploadForDialog" type="file" name="file[]" style="display: none;">
				
				<input id="uid" type="hidden" name="uid" value="<?php echo $uid;?>">
			
                <textarea style="display: none;" class="rounded_3" name="text" rows="1"
                          placeholder="Что у Вас нового?"></textarea>

                <div id="myEmojiField"></div>

                <div class="wall_post_btn">
                    <button class="btn rounded_3">Отправить</button>

                    <div class="wall_post_btn_right">
                        <a href="#" id="uploadWall" class="wall_icon wall_icon_photo normal"></a>
                        <a href="#" id="uploadWallVideo" class="wall_icon wall_icon_video normal"></a>
                        <a href="#" id="uploadWallDoc" class="wall_icon wall_icon_file normal"></a>
                    </div>

                    <div>
                        <div class="clear"></div>
                        <ul class="photos_main" id="uploadList">
                        </ul>
                    </div>
                </div>
                <input type="hidden" class="hide_data" name="hide_data"/>
            </form>
        </div>
    </div>
<?php
endif;
foreach ($posts as $post) {
    //if (!$post['repost']) {
    echo outputPost($post, $priv_wallc, $priv_wallsc);
    //}
}
?>
    </div>
</div>

<div class="right_block">

    <?php
echo show_main_contacts($_USER['id'], $cMyContacts, $myContacts, $priv_contact);
echo show_main_companies($_USER['id'], $priv_companies);
echo show_main_communities($_USER['id'], $priv_communities);
echo show_main_album($_USER['id'], $priv_photo);
echo show_main_videos($_USER['id'], $priv_video);
echo show_main_audios($_USER['id'], $priv_audios);
?>

</div>

<div class="clear"></div>
</div>
<div class="crop" id="divCrop">
    <h1>Выберите область</h1>
    <img src="" id="imgCrop" />
    <div>
        <button id="cancelCrop">Отменить</button>
        <button id="appendCrop">Сохранить</button>
    </div>
</div>
<!--main-->
<script>
    //if ($(window).scrollTop() + $(window).height() >= $('.files').height()-$('.files img:last').height()/*$(document).height() - 700*/) {

    var getVar = null;
    var lockLoad = false;
    var coords;
    var avatar = false;
    var miniDialog=true;
    $(document).ready(function () {
        $(window).scroll(function () {
            var bo = $(window).scrollTop();
            if (bo >= $('.wall_wrapper').scrollTop() + $('.left_block').height() - 300) {
                console.log('load_more_post');
                if (!lockLoad) {
                    lockLoad = true;
                    var offset = $('div[data-wid]').size();
                    $.post('', 'wall_offset=' + offset, function (data) {
                        $('.wall_wrapper').append(data);
                        $('.wall_post[data-wid]').each(function () {
                            console.log($(this).find('.wall_text span').size());
                            if ($(this).find('.wall_text span').size() == 2) {
                                clickable($(this).find('.wall_text span:last'));
                            } else {
                                clickable($(this).find('.wall_text'));
                            }
                        });
                        $('.wall_post[data-wid] .wall_post_comments .wall_post_content').each(function () {
                            clickable($(this).find('.wall_post_message p'));
                        });
                        doInitCommentEmoji();
                        lockLoad = false;
                    });
                }
            }
        });
        //////////////////////////////////
        $(document).ready(function () {
            $('.fancybox').fancybox({
            'padding': [20,20,0,20],
            'beforeShow': function (){
                $('.fancybox-skin').after('<div class="loading">Идет загрузка</div>');
            },
            'afterShow': function (){
                $this = this.element;

                $.post('/album/album-ajax', 'photo_id='+$this.attr('data-photo-id'), function (data){
                    $('.fancybox-skin').after(data);
                    $('.fancybox-skin').parent().find('.loading').remove();
                });
                    return false;
                }
            });
        });
        if (!$('#shortAbout table tr').size()) {
            $('#shortAbout').hide();
            $('#shortAboutH').hide();
        }
        $('.sinfo_img_toolbox a').click(function () {
            getVar = 'upload=1&avatar=2';
            $('#fileupload').fileupload({
                url: '?' + getVar,
                dataType: 'json',
                done: function (e, data) {
                    avatar = true;
                    if (data.result.error === undefined) {
                        //curImage.attr('img-src', data.result.file);
                        //curImage.click();
                        //window.location.reload();
                        $('#divCrop').show();
                        $img = $('#imgCrop');
                        $img.attr('src', '/'+data.result.file);
                                $img.Jcrop({
                                    minSize: [320, 320],
                                    setSelect: [0, 0, 320, 320],
                                      aspectRatio: 1 / 1,
                                    onSelect: function (c) {
                                        // Get coordionates
                                        coords = c;
                                    }
                                });
                        $img.load(function (){
                         $('#divCrop').center(true, false);
                        });
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
            $('#fileupload').click();
            return false;
        });
        $('#cancelCrop').click(function (){
            $img = $('#imgCrop');
            JcropAPI = $img.data('Jcrop');
            JcropAPI.destroy();
            $img.attr('style', '');
            $('#divCrop').hide();
            return false;
        });
        $('#appendCrop').click(function (){
            console.log(coords);
                var post = {
                'file': $('#imgCrop').attr('src'),
                'coords': coords
            };
            $.post('/?upload=2&iavatar='+(avatar?1:0), post, function (data){
                console.log(data);
                window.location.reload();
            });
            $('#cancelCrop').click();
            return false;
        });
        $('.slider_toolbox a').click(function () {
            getVar = 'upload=1&slide=2';
            $('#fileupload').fileupload({
                url: '?' + getVar,
                dataType: 'json',
                done: function (e, data) {
                    avatar=false;
                    if (data.result.error === undefined) {
                        //curImage.attr('img-src', data.result.file);
                        //curImage.click();
                        //window.location.reload();
                        $('#divCrop').show();
                        $img = $('#imgCrop');
                        $img.attr('src', '/'+data.result.file);
                                $img.Jcrop({
                                    minSize: [918, 478],
                                    setSelect: [0, 0, 918, 478],
                                      aspectRatio: 918 / 478,
                                    onSelect: function (c) {
                                        // Get coordionates
                                        coords = c;
                                    }
                                });
                        $img.load(function (){
                         $('#divCrop').center(true, false);
                        });
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
            $('#fileupload').click();
            return false;
        });
        $('.addphoto').click(function () {
            getVar = 'addPhoto=1&id='+current_dialog_id;
            $('#fileuploadForDialog').fileupload({
                url: '/message?' + getVar,
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.error === undefined) {
                        $('.mphotos a').each(function (){
                            $(this).click();
                        });
                        $('.mphotos').html('').append('<div><img src="/uploads/messages/'+data.result.file+'" /><a data-file="'+data.result.file+'" href="#"></a>');
                        $('.mfooter form').append('<input type="hidden" name="img[]" value="'+data.result.file+'"/>');
                        $('.mphotos').show();
                    } else {
                        alert(data.result.error);
                    }
                    console.log(data);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
            $('#fileuploadForDialog').click();
            return false;
        });
        $(document).on('click', '.mphotos a', function (){
            var file = $(this).attr('data-file');
            $(this).parent().remove();
            $('.mfooter form input[value="'+file+'"]').remove();
            if(!$('.mfooter form input').size()){
                 $('.mphotos').hide();
            }
            return false;
        });
        /*********************************************/
        $(document).on('click', '#wallPost button', function () {
            $('#wallPost').find('textarea[name="text"]').val(kemoji1.getValue(KEmoji.HTML_VALUE));
            if (!$.trim($('#wallPost').find('textarea[name="text"]').val()) && !$('#uploadList li').size()) {
                console.log('nini');
                return false;
            }
			
			console.log($('#wallPost').serialize());
			
            $.post('', $('#wallPost').serialize() + '&add_post=1', function (data) {
                $('#wallPost input').val('');
                $('#wallPost textarea').val('');
                $('.wall_wrapper .wall_post_comment:first').after(data);
                clickable($('.wall_post:first').find('.wall_text'));
                if($('.wall_post:first').find('div[data-video]').size()){
                preview_video($('.wall_post:first').find('div[data-video]'));
                }
                $('.wall_post:first .wallComment textarea').show();

                $('')

                $('.grid_wall_images img').load(function (){
                    normalizeWallPost();
                });
                $('#uploadList').html('');
                $('.KEmoji_Input div[contenteditable]').html('');
                $('#myEmojiField').css({height:'auto'});
                $('#myEmojiField').removeClass('h146');

            });
            return false;
        });
        $(document).on('click', '.wallComment button', function () {
            $wall = $(this).parent().parent().parent();

            $kemoji = $wall.find('div[emoji-id]').attr('emoji-id');
            $kemoji = kemoji5[parseInt($kemoji) - 1];
            $wall.find('textarea[name="text"]').val($kemoji.getValue(KEmoji.HTML_VALUE));

            if (!$.trim($wall.find('form textarea[name="text"]').val())) {
            console.log('empty text', $wall.find('form textarea[name="text"]'));
                return false;
            }
            $.post('', $wall.find('form').serialize() + '&add_comment=1', function (data) {
                //$('.wallComment input').val('');
                $('.wallComment textarea').val('');
                $wall.find('.KEmoji_Input div[contenteditable]').html('');
                //$('.wall_wrapper .wall_post_comment:first').after(data);
                $('.uploadListComment').html('');
                var wid = $wall.find('input').val();
                $('.wall_post[data-wid="' + wid + '"] .wall_post_comments')[0].outerHTML = data;
                clickable($('.wall_post[data-wid="' + wid + '"] .wall_post_comments .wall_post_content:first .wall_post_message p'));
            });
            
            return false;
        });
        function normalizeWallPost() {
            var min = 186;
            $('.wall_post .grid_wall_images img').each(function () {
                if (min > $(this).height()) {
                    min = $(this).height();
                }
            });
            $('.wall_post .grid_wall_images a').each(function () {
                $(this).height(min)
            });
        }
        $('.grid_wall_images img').load(function (){
            normalizeWallPost();
        });

    });
    $(document).on('click', '.uploadWallComment', function () {
        $wall = $(this).parent().parent();
        $('#fileuploadForWall').fileupload({
            url: '?uploadWall&uid=<?php echo $_USER['id']; ?>',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.error === undefined) {
                    //curImage.attr('img-src', data.result.file);
                    //curImage.click();
                    console.log(data.result);
		    for(key in data.result.files){
			//var file = data.result._files[key];
			var file = data.result.files[key];
                    $wall.find('.uploadListComment').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
                            + '<a href="#" onlick="return false;"><img class="rounded_3 width71" src="/uploads/wall/<?php echo $_USER['id']; ?>/' + file.thumb + '" /></a>'
                            + '<input type="hidden" name="file[]" value="/uploads/wall/<?php echo $_USER['id']; ?>/' + file.file + '" /></li>');
}
                } else {
                    alert(data.result.error);
                }
                console.log(data);

            }
            ,
            progressall: function (e, data) {

            }
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

        $('#fileuploadForWall').click();

        return false;
    });


    $(document).on('click', '.wall_wrapper .wall_icon_repost_y', function () {
        alert('Запись уже опубликована');
        return false;
    });
    $(document).on('click', '.wall_wrapper .wall_icon_repost,.wall_wrapper .wall_icon_repost', function () {
        console.log('repost');
        if ($(this).hasClass('wall_icon_repost_y')) {
            $(this).addClass('wall_icon_repost');
            $(this).removeClass('wall_icon_repost_y');
        } else {
            $(this).addClass('wall_icon_repost_y');
            $(this).removeClass('wall_icon_repost');
        }
        $this = $(this);
        var id = $(this).parent().parent().parent().parent().attr('data-wid');
        console.log(id);
        $.post("/", "repost=1&id=" + id, function (data, textStatus) {
            if (data == '1') {
                $this.text(parseInt($this.text()) + 1);
                alert('Запись была опубликована на Вашу стену');
            } else {

            }
            console.log(data);
            //listLike(id, undefined, false);
        });

        return false;
    });
</script>
<?php
footer();
