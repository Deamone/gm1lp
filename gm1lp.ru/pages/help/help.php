<?php
/**
  =====================================================
 * created 31.01.2015 NotusX
  -----------------------------------------------------
 * http://notusx.ru/
  -----------------------------------------------------
 * Copyright (c) 2014,2015 NotusX
  =====================================================
 * Данный код защищен авторскими правами
  =====================================================
 * @author NotusX
  -----------------------------------------------------
 * Файл: index.php
  -----------------------------------------------------
 * Назначение: Не задано
  =====================================================
 */
$route = isset($_GET['_route_']) ? str_replace(array('.', 'help/'), array('', ''), $_GET['_route_']) : '';
$page = getPageByAlias($route);
if (!$page) {
    redirect('/help/index');
}
$pages = getPages();
$title = $page['title'];
//header_($page['title'], '', '', '', '');
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo isset($title) ? $title : ''; ?></title>

        <meta charset="utf-8">
        <meta name="description" content="<?php echo isset($desc) ? $desc : 'GM Справочник'; ?>" />
        <meta name="keywords" content="<?php echo isset($key) ? $key : 'GM Справочник'; ?>" />
        <link rel="icon" href="/favicon.ico" type="image/x-icon"> 
        <link rel="shortcut icon" href="/favicon.png" type="image/png"> 

        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="/css/style_help.css"/>
    </head>
    <body class="">
        <div class="wrap">
            <div class="logo"><a href="/help/index"><img src="/images/logo1.jpg" alt="" /></a></div>
            <div class="desc">Один аккаунт. <span>Весь мир GM!</span>
                <br/>
                <p>Один аккаунт для всех сервисов GM</p>
            </div>
            <div class="header">GM СПРАВКА</div>
            <div class="page">
                <div class="left">
                    <h1><?php echo $page['title']; ?></h1>
                    <div class="content"><?php echo htmlspecialchars_decode($page['content']); ?></div>
                </div>
                <div class="right">
                    <?php
                    foreach ($pages as $page_) {
                        echo '<div class="grid ' . ($page['alias'] == $page_['alias'] ? 'active' : '') . '"><a href="/help/' . $page_['alias'] . '"><span><i>' . $page_['title'] . '</i></span></a></div>';
                    }
                    ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <footer>
            <div class="wrap footer1">
                <div class="footer1a">
                    <a href="/"><img src="/images/logo1.png" alt="GM1LP"></a>
                    <span class="logo">GM1LP</span>
                    <span class="yellow">2013-2017</span>
                    <span class="menu">
                        <a href="http://gm1lp.ru/help/agree" target="_blank">Пользовательское соглашение</a>
                        <a href="http://gm1lp.ru/help/rules" target="_blank">Правила сообщества</a>
                        <a href="http://gm1lp.ru/help/power" target="_blank">Возможности</a>
                        <a href="#" onclick="return false;">Люди</a>
                        <a href="#" onclick="return false;">Компании и Сообщества</a>
                    </span>
                </div>
            </div>
        </footer>
        <script>
            $(document).ready(function () {
            });
        </script>
    </body>
</html>
