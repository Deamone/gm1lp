<?php
$querySearch = isset($_GET['query']) ? $_GET['query'] : '';
if ($USER['id']) {
    if (isset($_COOKIE['session_key'])) {
        $session_key = $_COOKIE['session_key'];
    } else {
        $session_key = getSessionKey();
        setcookie('session_key', $session_key, time() + 60, '/', '.' . $_SERVER['HTTP_HOST']);
    }
    $time = getSeesionTime();
    $url = 'feed';
} else {
    $session_key = '';
    $time = -1;
    $url = '';
}
$counterContacts = getCounterContacts($USER['id']);
$counterContacts_ = '';
foreach ($counterContacts as $k => $contact) {
    if ($contact[1] != 0) {
        unset($counterContacts[$k]);
        continue;
    }
    $contact = getUserById($contact[0]);
    $counterContacts_ .= '<div class="popup_row">
                            <div class="popup_col_img"><a href="/id' . $contact['id'] . '"><img class="rounded_3" src="' . '/uploads/avatars/' . $contact['ava'] . '" width="59" height="59"></a></div>
                            <div class="popup_col_text">
                                <span class="title"><a href="#">' . $contact['name'] . ' ' . $contact['fam'] . '</a></span>
                                <button class="btn btn-size-small rounded_3 addToContact" data-id="' . $contact['id'] . '">Установить контакт</button>
                                <button class="btn btn-size-small rounded_3 btn-color-pink unContact" data-id="' . $contact['id'] . '">Отклонить заявку</button>
                            </div>
                        </div>';
}
$notices = getNotices(0, 3, true);
$cNotices = getCountNotices(true);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="<?php echo isset($desc) ? $desc : 'GM: Социальная сеть'; ?>"/>
    <meta name="keywords" content="<?php echo isset($key) ? $key : 'GM: Социальная сеть'; ?>"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.png" type="image/png">
    <title><?php echo isset($title) ? $title : ''; ?></title>

    <link href='https://fonts.googleapis.com/css?family=PT+Sans&subset=cyrillic-ext,latin' rel='stylesheet'
          type='text/css'>
    <!--link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'-->
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/js/placeholders.jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/js/kemoji.min.js"></script>
    <script type="text/javascript" src="/js/main2.js"></script>
    <script type="text/javascript" src="/js/nx.js?v1.0"></script>
    <?php if ($_SERVER['HTTP_HOST'] != 'gm1lp!.com') {
        echo '<script type="text/javascript" src="/js/message.js"></script>';
    } ?>

    <!--script src="//vk.com/js/api/openapi.js" type="text/javascript"></script-->
    <?php echo isset($head) ? $head : ''; ?>
    <link rel="stylesheet" type="text/css" href="/css/nx.css?v1.0"/>
    <link rel="stylesheet" type="text/css" href="/css/emoji.css"/>
    <link rel="stylesheet" type="text/css" href="/css/smiles.css"/>
    <script type="text/javascript">
        var user = {
            uid: '<?php echo $USER['id']; ?>',
            vk_uid: '<?php echo $USER['vk_uid']; ?>'
        };
        var miniDialog = true;

        <?php
$route = isset($_GET['_route_']) ? str_replace('.', '', $_GET['_route_']) : '';
if($route=='message'): ?>
        miniDialog = false;
        <?php endif; ?>
        <?php
        echo 'var session_key="' . $session_key . '",timestamp=' . $time . ',ts=' . $time . ';
            var my_uid=parseInt("' . $USER['id'] . '"); var my_name="' . $USER['name'] . '";';
        ?>
        /*
         VK.init({
         apiId: 4396061
         });*/

        function rsmz(t) {
            if (t.length <= 1 || t <= 9) {
                t = '0' + t;
            }
            return t;
        }
        function date_js(timestamp, lite) {

            console.log(timestamp * 1000);
            var date = new Date(timestamp * 1000);
            //var date_ = rsmz(date.getDate()) + '.' + rsmz(date.getMonth() + 1) + '.' + date.getFullYear() + ' ' + rsmz(date.getHours()) + ':' + rsmz(date.getMinutes()) + ':' + rsmz(date.getSeconds());
            if (lite === undefined) {
                var date_ = '' + rsmz(date.getHours()) + ':' + rsmz(date.getMinutes()) + ':' + rsmz(date.getSeconds());
            } else {
                var date_ = '' + rsmz(date.getHours()) + ':' + rsmz(date.getMinutes());
            }

            return date_;
        }
        function reConsctuctMessages() {
            if ($('#ajax_mes').size()) {
                var mylist = $('#ajax_mes');
                var listitems = mylist.children('li').get();
                listitems.sort(function (a, b) {
                    var compA = $(a).attr('data-timestamp');
                    var compB = $(b).attr('data-timestamp');
                    return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
                })
                $.each(listitems, function (idx, itm) {
                    mylist.append(itm);
                });
            }
        }
    </script>

    <style>
        /*li[data-flag="2"],*/
        a[data-flag="2"] {
            background-color: rgba(0, 0, 0, 0.1);
        }

        /*li[data-flag="3"],*/
        a[data-flag="3"] {
            background-color: rgba(255, 0, 0, 0.1);
        }
    </style>
</head>
<body class="<?php echo $body_class; ?>">
<!--header-->
<div id="header">
    <div class="main_block centred header_wrapper">
        <div class="main_block top_block">
            <div class="tb_part tb_sep_r tb_logo">
                <a href="/<?php echo $url; ?>"><img src="/images/logo.png"></a>
            </div>
            <div popup-target="popup_services" popup-pos="right" class="item_popup_menu tb_part tb_btn tb_services">
                <span>Все сервисы GM</span>
            </div>

            <div class="tb_part tb_search">
                <form action="/search" method="GET">
                    <input name="query" value="<?php echo $querySearch; ?>" type="text" class="search_left"
                           placeholder="Поиск людей, сообществ, компани..." autocomplete="off"/>
                    <button onclick="return false;" popup-target="popup_searchlocation" popup-pos="nx"
                            class="item_popup_menu btn btn-searchlocation"><span>Везде</span></button>
                    <button class="btn btn-icon btn-icon-search rounded_3">Найти</button>

                    <a href="/search" class="extended_search">Расширенный<br/> поиск</a>
                </form>
            </div>

            <div popup-target="popup_profile" popup-pos="left" class="item_popup_menu tb_part_r tb_btn tb_profile"><img
                    src="<?php echo '/uploads/avatars/' . $USER['ava']; ?>" class="rounded_3 mini-avatar"/></div>
            <div popup-target="popup_friends" popup-pos="left"
                 class="item_popup_menu tb_part_r tb_btn tb_ico tb_sep_r tb_friends">
                <div class="tb_amount"><?php echo count($counterContacts); ?></div>
            </div>
            <div popup-target="popup_notice" popup-pos="left"
                 class="item_popup_menu tb_part_r tb_btn tb_ico tb_sep_r tb_notice">
                <div class="tb_amount"><?php echo $cNotices; ?></div>
            </div>
            <div popup-target="popup_messages" popup-pos="left"
                 class="item_popup_menu tb_part_r tb_btn tb_ico tb_sep_l tb_sep_r tb_messages">
                <div class="tb_amount"></div>

            </div>

            <!-- POPUPS -->

            <!-- Friends -->
            <div class="popup tb_popup popup_friends" id="popup_friends">
                <?php echo $counterContacts_ ? $counterContacts_ : 'Входящих заявок нет'; ?>
            </div>
            <!--  -->
            <div id="popup_searchlocation" class="popup popup_searchlocation">
                <input type="checkbox" name="slocation" id="sl_all" class="checkbox_all" value="0" checked> <label
                    for="sl_all">Везде</label>
                <input type="checkbox" name="slocation" id="sl_people" class="checkbox_people" value="1" checked>
                <label for="sl_people">Люди</label>
                <!--input type="checkbox" name="slocation[]" id="sl_vacancies" class="checkbox_vacancies" value="2"> <label
                    for="sl_vacancies">Новости</label-->
                <input type="checkbox" name="slocation" id="sl_companies" class="checkbox_companies" value="3"> <label
                    for="sl_companies">Сообщества</label>
                <input type="checkbox" name="slocation" id="sl_communities" class="checkbox_communities" value="4">
                <label for="sl_communities">Компании</label>
                <!--input type="checkbox" name="slocation[]" id="sl_groups" class="checkbox_groups" value="6"> <label
                    for="sl_groups">Группы</label-->
                <input type="checkbox" name="slocation" id="sl_articles" class="checkbox_articles" value="5"> <label
                    for="sl_articles">Видеозаписи</label>
            </div>

            <!-- Profile -->
            <div class="popup tb_popup popup_profile" id="popup_profile">
                <div class="popup_row header">
                    <div class="popup_left">
                        <img src="<?php echo '/uploads/avatars/' . $USER['ava']; ?>" class="mini-avatar">
                        <span><?php echo $USER['name'] . ' ' . $USER['fam']; ?></span>
                    </div>
                    <div class="popup_right"><a href="/page/logout">Выход</a></div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_left"><span class="popup_ico popup_ico_profile">Профиль</span></div>
                    <div class="popup_right"><a href="/settings">Редактировать</a></div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_left"><span class="popup_ico popup_ico_balance">Баланс</span> <span
                            class="popup_hlighted"><?php echo $USER['money']; ?> руб.</span></div>
                    <div class="popup_right"><a href="/balance">Оперировать</a></div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_left"><span class="popup_ico popup_ico_balance">Учетная запись</span> <span
                            class="popup_hlighted">«<?php echo isVer($USER); ?>»</span></div>
                    <div class="popup_right"><a href="/page/tariff">Повысить</a></div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_left"><span
                            class="popup_ico popup_ico_settings">Настройки и конфиденциальность</span></div>
                    <div class="popup_right"><a href="/private">Проверить</a></div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row last">
                    <div class="popup_left"><span class="popup_ico popup_ico_help">Справка</span></div>
                    <div class="popup_right"><a href="/help/index">Получить помощь</a></div>
                    <div class="clear"></div>
                </div>
            </div>

            <!-- Notices -->
            <div class="popup tb_popup popup_notice" id="popup_notice">
                <?php
                if (!$cNotices) {
                    echo 'Новых новостей нет';
                }
                for ($i = 0; $i < min($cNotices, 3); $i++) {
                    $group = getGroup($notices[$i]['user_id']);
                    $text = strip_tags($notices[$i]['text']);
                    if (mb_strlen($text) > 256) {
                        $text = mb_substr($text, 0, 256) . '...';
                    }
                    $text = trim($text);
                    if (empty($text)) {
                        $text = '<b>файл</b>';
                    }
                    echo '<div class="popup_row ' . ($i + 1 == $cNotices ? 'last' : '') . '">
                    <a class="cover" href="https://gm1lp.ru/feed#post' . $notices[$i]['wall_id'] . '"></a>
                    <div class="popup_toolbox"><!--a href="#"><img src="/images/icon-popup-close.png"/></a--></div>
                    <div class="popup_col_img"><a href="https://gm1lp.ru/' . $group['url'] . '"><img class="rounded_3 width48" src="https://gm1lp.ru//uploads/g_avatars/' . $group['ava'] . '"></a>
                    </div>
                    <div class="popup_col_text">
                        <span class="title"><a href="https://gm1lp.ru/feed#post' . $notices[$i]['wall_id'] . '">' . $group['title'] . '</a></span><br/>
                        <span class="info">Опубликовал(а):</span> <a href="https://gm1lp.ru/feed#post' . $notices[$i]['wall_id'] . '">' . $text . '</a>
                    </div>
                </div>';
                }
                ?>
            </div>

            <!-- Messages -->
            <div class="popup tb_popup popup_messages" id="popup_messages">
            </div>

            <!-- Services -->
            <div class="popup tb_popup popup_services" id="popup_services">
                <div class="popup_row">
                    <div class="popup_col_label">Скидки</div>
                    <div class="popup_col_text">
                        <a href="http://gmcard.ru">посмотреть</a> или <a href="http://gmcard.ru/action/create">запустить</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_col_label">Конкурсы</div>
                    <div class="popup_col_text">
                        <a href="http://rightlike.ru/">участвовать</a> или <a href="http://rightlike.ru/action/create">создать</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_col_label">Задания</div>
                    <div class="popup_col_text">
                        <a href="http://goodmoneys.ru">заработать</a> или <a href="http://goodmoneys.ru">дать задание</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Блэк лист</div>
                    <div class="popup_col_text">
                        проверить себя или исполнителя
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">P2P</div>
                    <div class="popup_col_text">
                        одолжить денег под % или погасить долг
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Бартер</div>
                    <div class="popup_col_text">
                        услуга за услугу, товар за товар
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Краудфандинг</div>
                    <div class="popup_col_text">
                        спонсировать или привлечь средства
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Аренда</div>
                    <div class="popup_col_text">
                        сдать или взять вещь в аренду
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row last disabled">
                    <div class="popup_col_label">ВКонкурсе</div>
                    <div class="popup_col_text">
                        выбрать победителя
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
				
        <div class="main_block bottom_block">
            <ul class="top_menu">
                <li class="item_main"><a href="/<?php echo $USER['id'] ? $USER['url'] : ''; ?>">Главная</a></li>
                <li class="item_contacts"><a href="/contacts">Контакты</a></li>
                <li class="item_message"><a href="/messages">Диалоги</a></li>
                <li class="item_companies"><a href="/groups?type=1">Компании</a></li>
                <li class="item_communities"><a href="/groups?type=0">Сообщества</a></li>
                <li class="item_photos"><a href="/albums">Фотоальбомы</a></li>
                <li class="item_videos"><a href="/video">Видеозаписи</a></li>
                <li class="item_bs"><a href="/bs" onclick="showInfo('К сожалению','Безопасная сделка находится в стадии разработки');return false;">БС</a></li>
            </ul>

            <ul class="top_menu right">
                <li class="item_level"><a href="/page/tariff">Повысить уровень</a></li>
            </ul>
        </div>
    </div>
</div>

<?php
	if(isset($USER) && $USER['id'] && $USER['confirmed'] == 0) {
		echo '<div style="color: #000;font-size: 16px;width: 100%;position: absolute;top: 119px;z-index: 4;font-weight: 600;background-color: rgba(255,242,232,1);border-color: #FAE115;text-align: center;">
			Учетная запись имеет неподтвержденный адрес e-mail. Если вы не получили письмо для подтвеждения, вы можете <a href="/confirm_email">запросить его повторно</a>.
		</div>';
	}
?>

<!--header-->