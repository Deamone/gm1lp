<?php
$route = isset($_GET['_route_']) ? str_replace('.', '', $_GET['_route_']) : '';
if ($route !== 'message'): ?>
    <div class="mini-dialog rounded_5" style="display: none">
        <div class="mheader">
            <span><a href=""></a></span>

            <div class="set-tooltip"><a href="#">Посмотреть всю переписку</a><a href="#" id="clearDialog">Очистить
                    окно</a>
            </div>
            <a href="#" class="set"><i class="micon mdia-set"></i></a>
            <a href="#"><i class="micon mdia-cont"></i></a>
            <a href="#" class="close"><i class="micon mdia-close"></i></a>
        </div>
        <div class="messages" id="messages">
            <!--div class="timeline"><span>21 ноября 2014</span></div>
            <div class="messageI">
                <div class="text rounded_3">TextTextTextText</div>
                <div class="time">13:05</div>
                <div class="clear"></div>
            </div>
            <div class="messageYou">
                <div class="avatar"><img src="/uploads/avatars/ff7a338491ac521a851790e4f86954b0.jpg"
                                         class="width20 rounded_50p"/></div>
                <div class="text rounded_3">TextTextTextText</div>
                <div class="time">13:05</div>
                <div class="clear"></div>
            </div-->
        </div>
        <div class="mfooter">
            <form method="post">
                <textarea name="message"></textarea>

                <div id="myEmojiField3"></div>
                <a href="#"><i class="micon smiles"></i></a>
                <a href="#" class="addphoto"><i class="micon addphoto"></i></a>
                <a href="#" class="send rounded_3"><i class="send"></i></a>
            </form>

            <div class="mphotos">
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="main_block centred forSelect" style="display: none">
    <div class="modal">
        <div class="modal-header">Выберите видеозапись
            <a href="#" class="close"><i class="micon mdia-close"></i></a></div>
        <div class="modal-body"></div>
    </div>
</div>
<!--footer-->
<div id="footer">
    <div class="main_block centred"></div>
</div>
<div class="modal_tariff alertMessage">
    <div class="mt-header">
        <span id="alertHeader">Операция прошла успешно!</span>
        <a href="#" class="mtClose"><img src="/images/mt-close.png"/></a>
    </div>
    <div class="mt-body">
        <div class="mt-content success" id="alertMessage">
            Информация успешно сохранена!
        </div>
    </div>
</div>
<audio id="audioNewMessage">
    <source src="/audio/message.mp3"/>
</audio>
<div id="overlay"></div>
<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter31148601 = new Ya.Metrika({ id:31148601, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/31148601" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<!--footer-->
<script>
    $(document).ready(function () {
        $('.top_block .tb_amount').each(function () {
            if ($(this).text() == '0') {
                $(this).hide();
            }
        });
    });
</script>
</body>
</html>