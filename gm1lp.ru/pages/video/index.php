<?php

$itsI = false;
$_USER = array();


if (isset($_GET['id'])) {
    if ($_GET['id'] < 0) {
        $pid = $_GET['pid'];
        $itsI = false;
        $iUser = true;
        $iMember = false;

        $_USER = $USER;
        $group = getGroup($_GET['id']);
        #$_USER = getUserById($group['owner_id']);
        if (!$group) {
            redirect('/groups');
        }
        $members = getMembersInGroup($group['group_id']);
        if (isset($members['group'][10][$USER['id']]) || isset($members['group'][11][$USER['id']])) {
            $itsI = true;
        }
        if (isset($members[$USER['id']])) {
            $iMember = true;
        }
        $pid = $group['group_id'] * -1;
    } else {
        #$_USER = $USER;
        $pid = $_GET['id'];
        #$pid = $_USER['id'];
        $_USER = getUserById($pid);
        if ($_USER['id'] == $USER['id'] || !$_USER) {
            $_USER = $USER;
            $itsI = true;
        }
    }
} else {
    $_USER = $USER;
    $pid = $_USER['id'];

    if ($_USER['id'] == $USER['id'] || !$_USER) {
        $_USER = $USER;
        $itsI = true;
    }
}


if (isset($_POST['delete']) && $itsI) {
    deleteVideo($pid, $_POST['delete']);
    exit('1');
}
if ($pid > 0) {
    header_($itsI ? 'Мои видеозаписи' : 'Видиозапси пользователя ' . $_USER['name'], '', '', ''
        . '
        <script type="text/javascript" src="/application/views/site/js/nx.js"></script>
', 'body-albums');
} else {
    header_('Видеозаписи', '', '', ''
        . '
        <script type="text/javascript" src="/application/views/site/js/nx.js"></script>
', 'body-albums');
}
$myContacts = getContacts($_USER['id']);
$cMyContacts = getCountContacts($_USER['id']);
# Right
$priv_photo = isPrivate($USER['id'], $_USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($USER['id'], $_USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($USER['id'], $_USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($USER['id'], $_USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right

?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <?php if ($pid < 0) {
                    echo '<img src="/images/arrow.png"> <a href="/group?id=' . ($pid * -1) . '">вернуться назад</a>';
                } else {
                    echo '<img src="/images/arrow.png"> <a href="/' . $_USER['url'] . '">вернуться назад</a>';
                }
                ?>
            </div>
            <div class="block_header">
                <h2>Видеозаписи</h2>
                <?php echo $itsI ? '<a href="/video/add' . ($pid < 0 ? '?id=' . $pid . '' : '') . '" class="actRight">Добавить видеозапись</a><div class="hiddenAct">w</div>' : ''; ?>

                <div class="clear"></div>
            </div>
            <div class="block_content">
                <div class="videos">
                    <?php
                    $videos = getVideos($pid);
                    $url = array();
                    if ($pid != $USER['id']) {
                        $myVideos = getVideos($USER['id']);
                        foreach ($myVideos as $v) {
                            $url[$v['url']] = 1;
                        }
                    }
                    foreach ($videos as $video) {
                        $added = false;
                        if (isset($url[$video['url']]) || $pid==$USER['id']) {
                            $added = true;
                        }
                        echo '<div class="video" data-video="' . $video['url'] . '">' . ($itsI ? '<a href="#" data-vid="' . $video['video_id'] . '" class="delete"><img src="/images/remove.png"></a>' : '') . ($added ? '' : '<a href="#" data-vid="' . $video['video_id'] . '" class="addMe"><img src="/images/plus.png"></a>') . '<div class="video_title">' . $video['title'] . '</div></div>';
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($USER['id'], $priv_companies);
            echo show_main_communities($USER['id'], $priv_communities);
            echo show_main_album($USER['id'], $priv_photo);
            echo show_main_videos($USER['id'], $priv_video);
            echo show_main_audios($USER['id'], $priv_audios);
            ?>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $(document).on('click', '.openAct', function () {
                $(this).next().show();
                return false;
            });
            $('body').click(function () {
                $('.hiddenAct').hide();
            });
        });
    </script>
<?php footer(); ?>