<?php

$itsI = true;
$_USER = $USER;
$pid = $_USER['id'];
if(isset($_GET['id'])){
    $pid = $_GET['pid'];
    $itsI = false;
    $iUser = true;
    $iMember = false;

    $_USER = $USER;
    $group = getGroup($_GET['id']);
    #$_USER = getUserById($group['owner_id']);
    if (!$group) {
        redirect('/groups');
    }
    $members = getMembersInGroup($group['group_id']);
    if (isset($members['group'][10][$USER['id']]) || isset($members['group'][11][$USER['id']])) {
        $itsI = true;
    }
    if (isset($members[$USER['id']])) {
        $iMember = true;
    }
    $pid = $group['group_id'] * -1;
}
$alert = '';
$title = '';
$url = '';
if (isset($_POST['title']) && isset($_POST['url']) && $itsI) {
    $title = trim($_POST['title']);
    $url = trim($_POST['url']);

    if (empty($title) || empty($url)) {
        $alert .= 'Необходимо заполнить все поля';
    } elseif (!filter_var($url, FILTER_VALIDATE_URL)) {
        $alert .= 'Проверьте ссылку';
    } elseif (strpos($url, 'youtu') === false) {
        $alert .= 'На данный момент доступен только сервис YouTube';
    } else {
        addVideo($pid, $title, $url);
        if($pid<0) {
            redirect('/video?id=' . $pid);
        }else{
            redirect('/video');
        }
    }
}
header_('Добавление видеозаписи', '', '', ''
    . '
        <script type="text/javascript" src="/application/views/site/js/nx.js"></script>
', 'body-albums');
$myContacts = getContacts($_USER['id']);
$cMyContacts = getCountContacts($_USER['id']);

# Right
$priv_photo = isPrivate($USER['id'], $_USER['private3'], PHOTO_ALL, PHOTO_C, $myContacts);
$priv_video = isPrivate($USER['id'], $_USER['private3'], VIDEO_ALL, VIDEO_C, $myContacts);
$priv_contact = isPrivate($USER['id'], $_USER['private2'], CONTACT_ALL, CONTACT_C, $myContacts);
$priv_companies = $priv_communities = isPrivate($USER['id'], $_USER['private2'], COMPANY_ALL, COMPANY_C, $myContacts);
$priv_audios = true;
# .Right
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <div class="backme">
                <img src="/images/arrow.png"> <a href="/video?id=<?php echo $pid; ?>">вернуться назад</a>
            </div>
            <div class="block_header">
                <h2>Добавление видеозаписи</h2>

                <div class="clear"></div>
            </div>
            <div class="block_content">
                <?php if ($alert) {
                    echo '<div class="alert danger">' . $alert . '</div>';
                } ?>

                <div class="left_form">
                    <div class="videos">
                        <form method="post" id="formVideo">
                            <span>Название видеозаписи</span>
                            <input type="text" name="title" value="<?php echo $title; ?>" autocomplete="off"
                                   maxlength="80"/>
                            <span>Ссылка на видеозапись</span>
                            <input type="text" name="url" value="<?php echo $url; ?>"
                                   placeholder="Например: http://youtu.be/OQvIstAz5x8" autocomplete="off"/>
                            <small>На данный момент поддерживается только YouTube</small>
                            <br/>
                            <br/>
                            <button class="btn rounded_3">Добавить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="right_block">
            <?php
            echo show_main_contacts($USER['id'], $cMyContacts, $myContacts, $priv_contact);
            echo show_main_companies($USER['id'], $priv_companies);
            echo show_main_communities($USER['id'], $priv_communities);
            echo show_main_album($USER['id'], $priv_photo);
            echo show_main_videos($USER['id'], $priv_video);
            echo show_main_audios($USER['id'], $priv_audios);
            ?>
        </div>


        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $(document).on('click', '.openAct', function () {
                $(this).next().show();
                return false;
            });
            $('body').click(function () {
                $('.hiddenAct').hide();
            });
        });
    </script>
<?php footer(); ?>