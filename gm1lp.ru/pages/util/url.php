<?php
/**
 * =====================================================
 * created 13.05.2015 15:39 NotusX
 * -----------------------------------------------------
 * http://notusx.ru/
 * -----------------------------------------------------
 * Copyright (c) 2014,2015 NotusX
 * =====================================================
 * Данный код защищен авторскими правами
 * =====================================================
 * @author NotusX
 * -----------------------------------------------------
 * Файл: url.php
 * -----------------------------------------------------
 * Назначение: Не задано
 * =====================================================
 */
$error = 1;
if ($USER['url'] != $_POST['url'] && !$_POST['group_id']) {
    if (preg_match('/id(\d+)/i', $_POST['url']) && $_POST['url'] != 'id' . $USER['id']) {
        $error = 'Запрещенный Красивый ID';
    } elseif (preg_match('/group(\d+)/i', $_POST['url']) && $_POST['url'] != 'group' . $USER['id']) {
        $error = 'Запрещенный Красивый ID';
    } elseif (!preg_match('/^[a-z0-9_-]{5,64}$/i', $_POST['url'])) {
        $error = 'Красивый ID может состоять из букв, цифр, дефисов и подчёркиваний. Длина от 5 до 64 символов';
    } elseif ((($u = getUserByUrl($_POST['url'])) && $u['id'] != $USER['id']) || ($g = getGroupByUrl($_POST['url']))) {
        $error = 'Красивый ID уже занят';
    }
}else{
    $group['group_id'] = $_POST['group_id'];
    if (preg_match('/id(\d+)/i', $_POST['url']) && $_POST['url'] != 'id' . $group['group_id']) {
        $error = 'Запрещенный Красивый ID';
    } elseif (preg_match('/group(\d+)/i', $_POST['url']) && $_POST['url'] != 'group' . $group['group_id']) {
        $error = 'Запрещенный Красивый ID';
    } elseif (!preg_match('/^[a-z0-9_-]{5,64}$/i', $_POST['url'])) {
        $error = 'Красивый ID может состоять из букв, цифр, дефисов и подчёркиваний. Длина от 5 до 64 символов';
    } elseif (($u = getUserByUrl($_POST['url'])) || (($g = getGroupByUrl($_POST['url'])) && $g['group_id'] != $group['group_id'])) {
        $error = 'Красивый ID уже занят';
    } else {
        updateGroupUrl($group['group_id'], $_POST['url']);
    }
}
echo $error;