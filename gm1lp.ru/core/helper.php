<?php
mb_internal_encoding("UTF-8");

define('TIMECODE', 10);

define('SP_I', 1 << 1);
define('SP_C', 1 << 2);
define('SP_ALL', 1 << 3);
//
define('PHONE_I', 1 << 4);
define('PHONE_C', 1 << 5);
define('PHONE_ALL', 1 << 6);
//
define('LOCATION_I', 1 << 7);
define('LOCATION_C', 1 << 8);
define('LOCATION_ALL', 1 << 9);
//
define('WORK_I', 1 << 10);
define('WORK_C', (1 << 11));
define('WORK_ALL', (1 << 12));
//
define('MESSAGE_I', 1 << 13);
define('MESSAGE_C', (1 << 14));
define('MESSAGE_ALL', (1 << 15));
//
# 2
//
define('CONTACT_I', 1 << 1);
define('CONTACT_C', (1 << 2));
define('CONTACT_ALL', (1 << 3));
//
define('COMPANY_I', 1 << 4);
define('COMPANY_C', (1 << 5));
define('COMPANY_ALL', (1 << 6));
//
define('EWALL_I', 1 << 7);
define('EWALL_C', (1 << 8));
define('EWALL_ALL', (1 << 9));
//
define('WALL_I', 1 << 10);
define('WALL_C', (1 << 11));
define('WALL_ALL', (1 << 12));
//
define('WALLC_I', 1 << 13);
define('WALLC_C', (1 << 14));
define('WALLC_ALL', (1 << 15));
//
# 3
//
define('WALLSC_I', 1 << 1);
define('WALLSC_C', (1 << 2));
define('WALLSC_ALL', (1 << 3));
//
define('PHOTO_I', 1 << 4);
define('PHOTO_C', (1 << 5));
define('PHOTO_ALL', (1 << 6));
//
define('VIDEO_I', 1 << 7);
define('VIDEO_C', (1 << 8));
define('VIDEO_ALL', (1 << 9));
//
define('AGE_I', 1 << 10);
define('AGE_C', (1 << 11));
define('AGE_ALL', (1 << 12));


if (!class_exists('Memcache')) {

    class Memcache
    {

        function addServer($x, $y)
        {

        }

        function set($x, $y)
        {

        }

        function get($x)
        {

        }

        function connect()
        {

        }

        function  delete()
        {

        }
    }

}

function declOfNum($number, $titles)
{
    $cases = array(2, 0, 1, 1, 1, 2);
    return $number . " " . $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
}

function getExtension($filename)
{
    return strtolower(substr(strrchr($filename, '.'), 1));
}

function getAgeFromDate($date)
{
    return floor((time() - strtotime($date)) / (60 * 60 * 24 * 365.25));
}

function create_thumbnail($original_file, $thumb_file, $new_w, $new_h, $autoSize = 'none')
{
    //GD check
    if (!function_exists('gd_info')) {
        // ERROR - Invalid image
        return 'GD support is not enabled';
    }
    $info = getimagesize($original_file);
    // Create src_img
    if ($info['mime'] == 'image/jpeg') {
        $src_img = imagecreatefromjpeg($original_file);
    } else if ($info['mime'] == 'image/png') {
        $src_img = imagecreatefrompng($original_file);
    } else if ($info['mime'] == 'image/gif') {
        $src_img = imagecreatefromgif($original_file);
    } else {
        return -1;
    }
    if ($thumb_file == false) {
        imagepng($src_img, $thumb_file, 9);
        imagedestroy($src_img);
        return;
    }

    $src_width = imageSX($src_img); //$src_width
    $src_height = imageSY($src_img); //$src_height

    if ($src_width < $new_w || $src_height < $new_h) {
        if (strpos('.png', $thumb_file) !== false) {
            imagepng($src_img, $thumb_file);
        } else {
            imagejpeg($src_img, $thumb_file, 99);
        }
        imagedestroy($src_img);
        return 1;
    }

    if ($src_height > $src_width && $autoSize == 'auto') {
        $autoSize = 'width';
    } elseif ($src_height < $src_width && $autoSize == 'auto') {
        $autoSize = 'height';
    }
    if ($new_h === false) {
        $new_h = $src_height;
    }
    if ($new_w === false) {
        $new_w = $src_width;
    }
    $src_w = $src_width;
    $src_h = $src_height;
    $src_x = 0;
    $src_y = 0;
    $dst_w = $new_w;
    $dst_h = $new_h;
    $src_ratio = $src_w / $src_h;
    $dst_ratio = $new_w / $new_h;

    switch ($autoSize) {
        case "width":
            // AUTO WIDTH
            $dst_w = $dst_h * $src_ratio;
            break;
        case "height":
            // AUTO HEIGHT
            $dst_h = $dst_w / $src_ratio;
            break;
        case "none":
            // If proportion of source image is wider then proportion of thumbnail image, then use full height of source image and crop the width.
            if ($src_ratio > $dst_ratio) {
                // KEEP HEIGHT, CROP WIDTH
                $src_w = $src_h * $dst_ratio;
                $src_x = floor(($src_width - $src_w) / 2);
            } else {
                // KEEP WIDTH, CROP HEIGHT
                $src_h = $src_w / $dst_ratio;
                $src_y = floor(($src_height - $src_h) / 2);
            }
            break;
    }

    $dst_img = imagecreatetruecolor($dst_w, $dst_h);

    // PNG THUMBS WITH ALPHA PATCH
    // Turn off alpha blending and set alpha flag
    imagealphablending($dst_img, false);
    imagesavealpha($dst_img, true);


    imagecopyresampled($dst_img, $src_img, 0, 0, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

    if (strpos('.png', $thumb_file) !== false) {
        imagepng($dst_img, $thumb_file);
    } else {
        imagejpeg($dst_img, $thumb_file, 99);
    }

    imagedestroy($dst_img);
    imagedestroy($src_img);

    return 1;
    /* $info = getimagesize($path); //получаем размеры картинки и ее тип
      $size = array($info[0], $info[1]); //закидываем размеры в массив
      //В зависимости от расширения картинки вызываем соответствующую функцию
      if ($info['mime'] == 'image/png') {
      $src = imagecreatefrompng($path); //создаём новое изображение из файла
      } else if ($info['mime'] == 'image/jpeg') {
      $src = imagecreatefromjpeg($path);
      } else if ($info['mime'] == 'image/gif') {
      $src = imagecreatefromgif($path);
      } else {
      return false;
      }

      $thumb = imagecreatetruecolor($width, $height); //возвращает идентификатор изображения, представляющий черное изображение заданного размера
      $src_aspect = $size[0] / $size[1]; //отношение ширины к высоте исходника
      $thumb_aspect = $width / $height; //отношение ширины к высоте аватарки

      if ($src_aspect < $thumb_aspect) {        //узкий вариант (фиксированная ширина)      $scale = $width / $size[0];         $new_size = array($width, $width / $src_aspect);        $src_pos = array(0, ($size[1] * $scale - $height) / $scale / 2); //Ищем расстояние по высоте от края картинки до начала картины после обрезки   } else if ($src_aspect > $thumb_aspect) {
      //широкий вариант (фиксированная высота)
      $scale = $height / $size[1];
      $new_size = array($height * $src_aspect, $height);
      $src_pos = array(($size[0] * $scale - $width) / $scale / 2, 0); //Ищем расстояние по ширине от края картинки до начала картины после обрезки
      } else {
      //другое
      $new_size = array($width, $height);
      $src_pos = array(0, 0);
      }

      $new_size[0] = max($new_size[0], 1);
      $new_size[1] = max($new_size[1], 1);

      imagecopyresampled($thumb, $src, 0, 0, $src_pos[0], $src_pos[1], $new_size[0], $new_size[1], $size[0], $size[1]);
      //Копирование и изменение размера изображения с ресемплированием

      if ($save === false) {
      return imagepng($thumb); //Выводит JPEG/PNG/GIF изображение
      } else {
      return imagepng($thumb, $save); //Сохраняет JPEG/PNG/GIF изображение
      } */
}

function crop($src, $new_file)
{
    $jpeg_quality = 90;

    $src_width = imageSX($src); //$src_width
    $src_height = imageSY($src); //$src_height

    #$src = 'demo_files/pool.jpg';
    $img_r = imagecreatefromjpeg($src);
    $dst_r = ImageCreateTrueColor($src_width, $src_height);

    imagecopyresampled($dst_r, $img_r, 0, 0, $_POST['x'], $_POST['y'],
        $src_width, $src_height, $_POST['w'], $_POST['h']);

    #header('Content-type: image/jpeg');
    imagejpeg($dst_r, $new_file, $jpeg_quality);
}

header('Content-type: text/html; charset=utf-8');
$_SESSION = array();
$user = array();
$pmc = new Memcache;
$pmc->addServer('localhost', 11209);

function redirect($url = '/')
{
    header('location: ' . $url);
    exit;
}

function login_form()
{
    global $user;
    if ($user) {
        return '<a href="/">Добро пожаловать, ' . $user['first_name'] . '</a>';
    } else {
        return '<a href="/auth_vk">Войти через Вконтакте</a>';
    }
}

function isAjax()
{
    //static $ajax=null;
    //if($ajax==null) {
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        return true;
    }
    return false;
#    }

    #   return $ajax;
}

function getUserById($id)
{
    global $pmc;
    $id = (int)$id;
    $_ = false;// $pmc->get('user' . $id);
    if (!$_) {
        $q = query("SELECT * FROM `users` WHERE `id`='$id';");
        $_ = $q['row'];
        #$pmc->set('user' . $id, $_);
    }

    return $_;
}

/**
 *
 * @global Memcache $pmc
 * @param type $id
 * @return array (to, from) - входящие пользователю, исходящие
 */
function getUserFriends($id)
{
    global $pmc;
    $id = (int)$id;
    $_ = false; //$pmc->get('friends' . $id);
    if (!$_) {
        $fro = FRIEND_REQ_OK;
        $frf = FRIEND_REQ_FROM;
        $frt = FRIEND_REQ_TO;
        // Друзья
        $q1 = query("SELECT * FROM `nx_friends_req` WHERE (`to_id`='$id' OR `from_id`='$id') AND `status`='$fro'");

        // Входящие заявки
        $q2 = query("SELECT * FROM `nx_friends_req` WHERE (`to_id`='$id') AND `status`='$frf' OR `status`='$frt'");
        // Исходящие заявки
        $q3 = query("SELECT * FROM `nx_friends_req` WHERE (`from_id`='$id') AND `status`='$frt' OR `status`='$frf'");


        //$q = query("SELECT `to`,`from`,`status` FROM `nx_friends_req` WHERE (`to`='$id' OR `from`='$id')");
        $_ = array(); // Друзья
        $__ = array(); // Входящие
        $___ = array(); // Исходящие
        for ($i = 0; $i < $q1['num_rows']; $i++) {
            $_[$q1['rows'][$i]['to_id']] = $fro;
            $_[$q1['rows'][$i]['from_id']] = $fro;
        }
        for ($i = 0; $i < $q2['num_rows']; $i++) {
            $__[$q2['rows'][$i]['from_id']] = $frf;
        }
        for ($i = 0; $i < $q3['num_rows']; $i++) {
            $___[$q3['rows'][$i]['to_id']] = $frt;
        }
        unset($_[$id], $__[$id], $___[$id]);

        $pmc->set('friends' . $id, array($_, $__, $___));
    }

    return array($_, $__, $___);
}

function userFriend($from, $to, $remove = false)
{
    global $pmc, $USER;
    $from = (int)$from;
    $to = (int)$to;
    if ($from == $to) {
        return;
    }
    $list = query("SELECT `req_id`,`status`,`from_id` FROM `nx_friends_req` WHERE (`from_id`='$from' AND `to_id`='$to') OR (`to_id`='$from' AND `from_id`='$to');");
    $status = 0;
    $iInit = true;


    $frf = FRIEND_REQ_FROM;
    $frt = FRIEND_REQ_TO;
    if ($list['num_rows']) {
        if ($remove) {
            query("DELETE FROM `nx_friends_req` WHERE `req_id`='" . $list['row']['req_id'] . "';");
            return;
        }
        $status = $list['row']['status'];
        if ($list['row']['from_id'] == $USER['id']) {
            $iInit = 1;
        } else {
            $iInit = 0;
            $frf = FRIEND_REQ_TO;
            $frt = FRIEND_REQ_FROM;
        }
        if ($status == 0) {                     // Если заявок нет вообще
            $status = $frf;
        } elseif ($status & $frt) {            // Если заявка есть с другой стороны
            $status = $frf | $frt;
        }
        if ($list['row']['status'] != $status) {
            query("UPDATE `nx_friends_req` SET `status`='$status' WHERE `req_id`='" . $list['row']['req_id'] . "';");
        }
    } elseif (!$remove) {
        $status = $frf;
        query("INSERT INTO `nx_friends_req` (`from_id`,`to_id`,`status`) VALUES('$from','$to','$status');");
    }

    // DELETE FROM обязательно


    /* $_ = query("SELECT 1 FROM `nx_friends` WHERE (`to`='$to' AND `from`='$from') UNION SELECT 2 FROM `nx_friends` WHERE (`to`='$from' AND `from`='$to')");
      //////
      $from_ = $_['rows'][0][1] == 1 ? true : false;
      $to_ = ($_['rows'][0][1] == 2 || $_['rows'][1][1] == 2) ? true : false;
      if ($remove && ($from_ || $to_)) {
      if ($from_) {
      query("UPDATE `nx_friends` SET `status`='0' WHERE (`to`='$to' AND `from`='$from')");
      }
      if ($to_) {
      query("UPDATE `nx_friends` SET `status`='0' WHERE (`to`='$from' AND `from`='$to')");
      }
      /* $sql = 'DELETE FROM `nx_friends` WHERE ';
      $q = array();
      if ($from_) {
      $q[] = "(`to`='$to' AND `from`='$from')";
      }
      if ($to_) {
      $q[] = "(`to`='$from' AND `from`='$to')";
      }
      query($sql . implode(' AND ', $q));
      } elseif (!$remove) {
      if ($from_) {
      query("UPDATE `nx_friends` SET `status`='1' WHERE (`to`='$to' AND `from`='$from')");
      } else {
      query("INSERT INTO `nx_friends` (`from`,`to`,`status`) VALUES('$from','$to','1');");
      }
      /* if ($to_) {
      query("UPDATE `nx_friends` SET `status`='1' WHERE (`to`='$from' AND `from`='$to')");
      } else {
      query("INSERT INTO `nx_friends` (`to`,`from`,`status`) VALUES('$from','$to','0');");
      }
      }
      $pmc->delete('friends' . $from);
      $pmc->delete('friends' . $to); */
}

function getUserByUrl($url)
{
	
    global $pmc;
    $url = query_escape($url);
    $_ = false;
    if (!$_) {
        $q = query("SELECT * FROM `users` WHERE `url`='$url' OR `id`='" . str_replace('id', '', $url) . "';");
        $_ = $q['row'];
        $pmc->set('user' . $_['id'], $_);
    }

    return $_;
}

function getUserByEmail($email)
{
    global $pmc;
    $email = query_escape($email);
    $_ = false;
    if (!$_) {
        $q = query("SELECT * FROM `users` WHERE `login`='$email';");
        $_ = $q['row'];
        $pmc->set('user' . $_['id'], $_);
    }

    return $_;
}

function updateUserById($id, $keys, $values)
{
    global $pmc;
    $id = (int)$id;
    $sql = '';
    $_ = $pmc->get('user' . $id);

    $count = count($keys);
    for ($i = 0; $i < $count; $i++) {
        $sql .= ",`" . $keys[$i] . "`='" . query_escape($values[$i]) . "'";
        if ($_) {
            $_[$keys[$i]] = $values[$i];
        }
    }
    query("UPDATE `users` SET " . substr($sql, 1) . " WHERE `id`='$id';");
    if ($_) {
        $pmc->set('user' . $id, $_);
    }
    return $_;
}

function getUserAudios($id)
{
    global $pmc, $USER;
    $id = (int)$id;
    if ($id == $USER['id']) {
        $sql = '';
    } else {
        $sql = ' AND `private`=0';
    }
    $_ = false; //$pmc->get('user_audio' . $id);
    if (!$_) {
        $q = query("SELECT * FROM `nx_audios` WHERE `user_id`='$id' $sql ORDER BY `position` ASC;");
        $_ = $q['rows'];
        $pmc->set('user_audio' . $id, $_);
    }

    return $_;
}

function getUserAlbums($id, $random = false)
{
    global $pmc;
    $id = (int)$id;
    $_ = false; // $pmc->get('user_albums' . $id);
    if (!$_) {
        $q = query("SELECT * FROM `nx_albums` WHERE `user_id`='$id' AND `deleted`='0' ORDER BY `created` DESC;");
        for ($i = 0; $i < $q['num_rows']; $i++) {
            if ($q['rows'][$i]['size']) {
                $q2 = query("SELECT * FROM `nx_photos` WHERE `album_id`='" . $q['rows'][$i]['album_id'] . "' AND `deleted`='0' AND `file`=1 ORDER BY `created` DESC LIMIT 0,1;");
                $q['rows'][$i]['last_photo'] = $q2['row'];
            }
        }
        $_ = $q['rows'];

        if ($random) {
            shuffle($_);
        }
        $pmc->set('user_albums' . $id, $_);
    }

    return $_;
}

function removeAlbum($id)
{
    global $pmc, $USER;
    $id = (int)$id;
    $pmc->delete('user_album' . $id);
    $pmc->delete('user_albums' . $USER['id']);
    query("UPDATE `nx_albums` SET `deleted`='1'  WHERE `album_id`='$id';");
    query("UPDATE `nx_photos` SET `deleted`='1'  WHERE `album_id`='$id';");
}

function getUserAlbum($id)
{
    global $pmc, $USER;
    $id = (int)$id;
    $_ = false; //$pmc->get('user_album' . $id);
    if (!$_) {
        $q = query("SELECT * FROM `nx_albums` WHERE `album_id`='$id' AND `deleted`='0';");
        if (!$q['num_rows']) {
            return false;
        }
        $_ = $q['row'];
        $q = query("SELECT * FROM `nx_photos` WHERE `album_id`='$id' AND `deleted`='0' ORDER BY `created` DESC;");
        $_['photos'] = $q['rows'];
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $_['photos'][$i]['ilike'] = getLikesPhoto($_['photos'][$i]['photo_id'], $USER['id']);
        }
        $pmc->set('user_album' . $id, $_);
    }

    return $_;
}

function getUserPhotos($user_id, $random = false, $home = false)
{
    $user_id = (int)$user_id;
    $q1 = query("SELECT COUNT(`photo_id`) as `count` FROM `nx_photos` WHERE `user_id`='$user_id' AND `deleted`='0';");
    if ($home) {
        $q = query("SELECT * FROM `nx_photos` WHERE `user_id`='$user_id' AND `deleted`='0' AND `show_home`=1 ORDER BY `photo_id` DESC LIMIT 0,30;");
    } else {
        $q = query("SELECT * FROM `nx_photos` WHERE `user_id`='$user_id' AND `deleted`='0' ORDER BY `photo_id` DESC;");
    }
    if ($random) {
        shuffle($q['rows']);
    }
    return array($q1['row']['count'], $q['rows']);
}

function getPhoto($photo_id, $id = false, $random = false)
{
    global $pmc, $USER;
    $id = (int)$id;
    $photo_id = (int)$photo_id;
    $_ = $pmc->get('user_album' . $id);
    // if (!$_) {
    $q = query("SELECT * FROM `nx_photos` WHERE /*`album_id`='$id' AND*/ `photo_id`='$photo_id' AND `deleted`='0';");
    if ($q['row']) {
        $q2 = query("SELECT * FROM `nx_albums` WHERE `album_id`='" . $q['row']['album_id'] . "';");
        $q['row']['ilike'] = getLikesPhoto($photo_id, $USER['id']);
        $q['row']['album'] = $q2['row'];
        //$q['row']['list_likes'] = getLikesPhoto($photo_id);
    }
    return $q['row'];
    /* }else{
      $c = count($_);
      for($i=0; $i<$c; $i++){
      if($_[$i]['photo_id']==$photo_id){
      return $_[$i];
      }
      }
      } */

    //return $_;
}

function updateSizeAlbums()
{
    $albums = query("SELECT * FROM `nx_albums` WHERE 1");
    foreach ($albums['rows'] as $album) {
        $aid = $album['album_id'];
        $photos = query("SELECT COUNT(*) as `cc` FROM `nx_photos` WHERE `album_id`='$aid' AND `deleted`=0;");
        $cc = $photos['row']['cc'];
        query("UPDATE `nx_albums` SET `size`='$cc' WHERE `album_id`='$aid';");
    }
}

function createAlbum($id)
{
    global $pmc, $_LANG;
    $id = (int)$id;
    $pmc->delete('user_albums' . $id);
    query("INSERT INTO `nx_albums` (`title`,`mask`,`desc`,`created`,`user_id`) VALUES('" . query_escape($_LANG['ALBUMS_NEW_ALBUM']) . "','" . ALBUM_ALLOW_ALL . "','','" . time() . "','" . $id . "');");
}

function addVideo($id, $title, $url)
{
    global $pmc, $_LANG;
    $id = (int)$id;
    $title = query_escape($title);
    $url = query_escape($url);

    #$pmc->delete('user_video' . $id);
    query("INSERT INTO `nx_videos` (`title`,`url`,`owner_id`) VALUES('" . $title . "','" . $url . "','" . $id . "');");
}

function copyVideo($uid, $id)
{
    global $pmc, $_LANG;
    $uid = (int)$uid;
    $id = (int)$id;
    $video = getVideo($id);
    if (!$video) {
        return;
    }
    $title = query_escape($video['title']);
    $url = query_escape($video['url']);
    #$pmc->delete('user_video' . $id);
    query("INSERT INTO `nx_videos` (`title`,`url`,`owner_id`) VALUES('" . $title . "','" . $url . "','" . $uid . "');");
}

function getVideos($id)
{
    $id = (int)$id;
    $q = query("SELECT * FROM `nx_videos` WHERE `owner_id`='$id' ORDER BY `video_id` DESC;");

    return $q['rows'];
}

function getVideo($id)
{
    $id = (int)$id;
    $q = query("SELECT * FROM `nx_videos` WHERE `video_id`='$id';");

    return $q['row'];
}

function deleteVideo($id, $vid)
{
    $id = (int)$id;
    $vid = (int)$vid;

    query("DELETE FROM `nx_videos` WHERE `owner_id`='$id' AND `video_id`='$vid';");

}


function addPhotoInAlbum($album_id, $file = '', $uid = false)
{
    global $pmc, $_LANG, $USER;
    $album_id = (int)$album_id;
    if (!$uid) {
        $uid = (int)$USER['id'];
    } else {
        $uid = (int)$uid;
    }
    $pmc->delete('user_album' . $album_id);
    $pmc->delete('user_albums' . $uid);
    query("UPDATE `nx_albums` SET `size`=`size`+1 WHERE `album_id`='$album_id';");
    query("INSERT INTO `nx_photos` (`album_id`,`file`,`created`,`likes`,`comments`,`user_id`) VALUES('" . $album_id . "','" . $file . "','" . time() . "','0','0','$uid');");


    return query_lastInsertId();
}

function removePhoto($album_id, $id)
{
    global $pmc, $_LANG, $USER;
    $album_id = (int)$album_id;
    $id = (int)$id;
    $uid = (int)$USER['id'];
    $pmc->delete('user_album' . $album_id);
    $pmc->delete('user_albums' . $uid);
    query("DELETE FROM `nx_photos` WHERE `album_id`='$album_id' AND `photo_id`='$id';");
    query("UPDATE `nx_albums` SET `size`=`size`-1 WHERE `album_id`='$album_id';");
}

function updateAlbum($id, $name, $desc, $mask)
{
    global $pmc, $USER;
    $id = (int)$id;
    $name = query_escape($name);
    $desc = query_escape($desc);
    $pmc->delete('user_album' . $id);
    $pmc->delete('user_albums' . $USER['id']);
    query("UPDATE `nx_albums` SET `title`='$name', `desc`='$desc',`mask`='$mask'  WHERE `album_id`='$id';");
}

function getPosts($owner_id, $offset = 0, $limit = 5, $priv_ewall = true)
{
    global $pmc, $USER;
    $owner_id = (int)$owner_id;
    $offset = (int)$offset;
    if ($priv_ewall) {
        $q = query("SELECT * FROM `nx_wall` WHERE `owner_id`='$owner_id' AND `deleted`='0' ORDER BY `created` DESC LIMIT {$offset},$limit;");
    } else {
        $q = query("SELECT * FROM `nx_wall` WHERE `owner_id`='$owner_id' AND `user_id`='$owner_id' AND `deleted`='0' ORDER BY `created` DESC LIMIT {$offset},$limit;");
    }
    $posts = array();
    for ($i = 0; $i < $q['num_rows']; $i++) {
        $q['rows'][$i]['user'] = getUserById($q['rows'][$i]['user_id']);
        $q['rows'][$i]['ilike'] = getLikesPost($q['rows'][$i]['wall_id'], $USER['id']);
        $posts[] = $q['rows'][$i];
    }

    return $posts;
}

function likePost($wall_id)
{
    global $USER, $pmc;
    $wall_id = (int)$wall_id;
    $uid = $USER['id'];
    $like = getLikesPost($wall_id, $USER['id']);
    if ($like) {
        query("UPDATE `nx_wall` SET `likes`=`likes`-1 WHERE `wall_id`='$wall_id';");
        query("DELETE FROM `nx_wall_likes` WHERE `wall_id`='$wall_id' AND `user_id`='$uid';");
    } else {
        query("UPDATE `nx_wall` SET `likes`=`likes`+1 WHERE `wall_id`='$wall_id';");
        query("INSERT INTO `nx_wall_likes` (`wall_id`,`user_id`)VALUES('$wall_id','$uid');");
    }
}

function rePost($wall_id)
{
    global $USER, $pmc;
    $wall_id = (int)$wall_id;
    $uid = $USER['id'];
    $q = query("SELECT `wall_id` FROM `nx_wall` WHERE (`repost`='{$wall_id}' AND `owner_id`='{$uid}') OR (`wall_id`='{$wall_id}' AND `owner_id`='{$uid}');");
    if (!$q['rows']) {
        $q = query("SELECT * FROM `nx_wall` WHERE `wall_id`='{$wall_id}';");
        if ($q['repost']) {
            $rid = $q['repost'];
            $q = query("SELECT * FROM `nx_wall` WHERE `wall_id`='{$rid}';");
        }
        $owner_id = $uid;
        $post = query_escape($q['row']['text']);
        $user = $uid;
        $img = unserialize($q['row']['images']);
        $files = query_escape(serialize($img));
        query("INSERT INTO `nx_wall` (`owner_id`,`text`,`user_id`,`created`,`images`,`repost`)VALUES('$owner_id','$post','" . $user . "','" . time() . "','" . $files . "','" . $wall_id . "');");
        query("UPDATE `nx_wall` SET `reposts`=`reposts`+1 WHERE `wall_id`='$wall_id';");

        return 1;
    } else {
        return 0;
    }
    #query("INSERT INTO `nx_wall_likes` (`wall_id`,`user_id`)VALUES('$wall_id','$uid');");
}

function likeComment($cid)
{
    global $USER, $pmc;
    $cid = (int)$cid;
    $uid = $USER['id'];
    $like = getLikesComments($cid, $USER['id']);
    if ($like) {
        query("UPDATE `nx_wall_comments` SET `likes`=`likes`-1 WHERE `cid`='$cid';");
        query("DELETE FROM `nx_wall_comments_likes` WHERE `cid`='$cid' AND `uid`='$uid';");
    } else {
        query("UPDATE `nx_wall_comments` SET `likes`=`likes`+1 WHERE `cid`='$cid';");
        query("INSERT INTO `nx_wall_comments_likes` (`cid`,`uid`)VALUES('$cid','$uid');");
    }
}

function likePhotoComment($cid)
{
    global $USER, $pmc;
    $cid = (int)$cid;
    $uid = $USER['id'];
    $like = getLikesPhotoComments($cid, $USER['id']);
    if ($like) {
        query("UPDATE `nx_photo_comments` SET `likes`=`likes`-1 WHERE `cid`='$cid';");
        query("DELETE FROM `nx_photo_comments_likes` WHERE `cid`='$cid' AND `uid`='$uid';");
    } else {
        query("UPDATE `nx_photo_comments` SET `likes`=`likes`+1 WHERE `cid`='$cid';");
        query("INSERT INTO `nx_photo_comments_likes` (`cid`,`uid`)VALUES('$cid','$uid');");
    }
}

function likePhoto($photo_id, $album_id = false)
{
    global $USER, $pmc;
    $photo_id = (int)$photo_id;
    $uid = $USER['id'];
    $like = getLikesPhoto($photo_id, $USER['id']);
    if ($like) {
        query("UPDATE `nx_photos` SET `likes`=`likes`-1 WHERE `photo_id`='$photo_id';");
        query("DELETE FROM `nx_photo_likes` WHERE `photo_id`='$photo_id' AND `user_id`='$uid';");
    } else {
        query("UPDATE `nx_photos` SET `likes`=`likes`+1 WHERE `photo_id`='$photo_id';");
        query("INSERT INTO `nx_photo_likes` (`photo_id`,`user_id`)VALUES('$photo_id','$uid');");
    }
    if ($album_id) {
        $pmc->delete('user_album' . $album_id);
    }

    return $like;
}

function updatePhoto($photo_id, $desc, $album_id)
{
    global $USER, $pmc;
    $photo_id = (int)$photo_id;
    $desc = query_escape($desc);
    $album_id = (int)$album_id;
    query("UPDATE `nx_photos` SET `desc`='$desc' WHERE `photo_id`='$photo_id';");
    $pmc->delete('user_album' . $album_id);
}

function getCommentsPhoto($photo_id, $cid = false)
{
    $photo_id = (int)$photo_id;
    if (!$cid) {
        $q = query("SELECT * FROM `nx_photo_comments` WHERE `photo_id`='$photo_id' ORDER BY `created` ASC;");
    } else {
        $q = query("SELECT * FROM `nx_photo_comments` WHERE `comment_id`='" . intval($cid) . "';");
    }
    for ($i = 0; $i < $q['num_rows']; $i++) {
        $q['rows'][$i]['user'] = getUserById($q['rows'][$i]['user_id']);
    }

    return $q['rows'];
}

function addCommentPhoto($photo_id, $text)
{
    global $USER;

    $photo_id = (int)$photo_id;
    $text = query_escape($text);
    #$album_id = (int)$album_id;
    $created = time();
    $user_id = $USER['id'];

    query("INSERT INTO `nx_photo_comments`(`photo_id`,`user_id`,`message`,`created`)VALUES('$photo_id','$user_id','$text','$created')");
    $cid = query_lastInsertId();
    query("UPDATE `nx_photos` SET `comments`=`comments`+1 WHERE `photo_id`='$photo_id';");

    return $cid;
}

function getLikesPost($wall_id, $user_id = false, $limit = 3, $ava = false)
{
    global $pmc;
    $wall_id = (int)$wall_id;
    $user_id = (int)$user_id;
    if (!$user_id) {
        if (!$limit) {
            $q = query("SELECT * FROM `nx_wall_likes` WHERE `wall_id`='$wall_id';");
        } else {
            $q = query("SELECT * FROM `nx_wall_likes` WHERE `wall_id`='$wall_id' ORDER BY `like_id` DESC LIMIT 0,{$limit};");
        }
    } else {
        $q = query("SELECT * FROM `nx_wall_likes` WHERE `wall_id`='$wall_id' AND `user_id`='$user_id';");
    }
    $likes = array();
    if ($ava) {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $user = getUserById($q['rows'][$i]['user_id']);
            $likes[$q['rows'][$i]['user_id']] = array($user['id'], $user['ava']);
        }
    } else {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $likes[$q['rows'][$i]['user_id']] = $q['rows'][$i]['user_id'];
        }
    }
    if ($user_id) {
        return isset($likes[$user_id]);
    }

    return $likes;
}

function getRepost($wall_id, $user_id = false)
{
    global $pmc;
    $wall_id = (int)$wall_id;
    $user_id = (int)$user_id;
    $q = query("SELECT * FROM `nx_wall` WHERE `repost`='$wall_id' AND `user_id`='$user_id';");


    return $q['row'] ? true : false;
}

function getLikesComments($cid, $user_id = false, $limit = 3, $ava = false)
{
    global $pmc;
    $cid = (int)$cid;
    $user_id = (int)$user_id;
    if (!$user_id) {
        if (!$limit) {
            $q = query("SELECT * FROM `nx_wall_comments_likes` WHERE `cid`='$cid';");
        } else {
            $q = query("SELECT * FROM `nx_wall_comments_likes` WHERE `cid`='$cid' ORDER BY `lid` DESC LIMIT 0,{$limit};");
        }
    } else {
        $q = query("SELECT * FROM `nx_wall_comments_likes` WHERE `cid`='$cid' AND `uid`='$user_id';");
    }
    $likes = array();
    if ($ava) {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $user = getUserById($q['rows'][$i]['uid']);
            $likes[$q['rows'][$i]['uid']] = array($user['id'], $user['ava']);
        }
    } else {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $likes[$q['rows'][$i]['uid']] = $q['rows'][$i]['uid'];
        }
    }
    if ($user_id) {
        return isset($likes[$user_id]);
    }

    return $likes;
}

function getLikesPhotoComments($cid, $user_id = false, $limit = 3, $ava = false)
{
    global $pmc;
    $cid = (int)$cid;
    $user_id = (int)$user_id;
    if (!$user_id) {
        if (!$limit) {
            $q = query("SELECT * FROM `nx_photo_comments_likes` WHERE `cid`='$cid';");
        } else {
            $q = query("SELECT * FROM `nx_photo_comments_likes` WHERE `cid`='$cid' ORDER BY `lid` DESC LIMIT 0,{$limit};");
        }
    } else {
        $q = query("SELECT * FROM `nx_photo_comments_likes` WHERE `cid`='$cid' AND `uid`='$user_id';");
    }
    $likes = array();
    if ($ava) {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $user = getUserById($q['rows'][$i]['uid']);
            $likes[$q['rows'][$i]['uid']] = array($user['id'], $user['ava']);
        }
    } else {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $likes[$q['rows'][$i]['uid']] = $q['rows'][$i]['uid'];
        }
    }
    if ($user_id) {
        return isset($likes[$user_id]);
    }

    return $likes;
}

function getPost($wall_id)
{
    global $pmc;
    $wall_id = (int)$wall_id;
    $q = query("SELECT * FROM `nx_wall` WHERE `wall_id`='$wall_id' AND `deleted`='0' ORDER BY `created` DESC;");
    $posts = array();
    for ($i = 0; $i < $q['num_rows']; $i++) {
        $q['rows'][$i]['user'] = getUserById($q['rows'][$i]['user_id']);
        $posts[] = $q['rows'][$i];
    }

    return $posts[0];
}

function removePost($wall_id)
{
    global $pmc;
    $wall_id = (int)$wall_id;
    #query("DELETE FROM `nx_wall` WHERE `wall_id`='$wall_id';");
    query("UPDATE `nx_wall` SET `deleted`='1' WHERE `wall_id`='$wall_id';");

    return true;
}

function updatePost($wall_id, $post)
{
    $wall_id = (int)$wall_id;
    $post = query_escape($post);
    if (empty($post)) {
        return;
    }
    query("UPDATE `nx_wall` SET `text`='$post' WHERE `wall_id`='$wall_id';");
}

function addPost($onwer_id, $post, $user = false, $files = 'N;')
{
    global $USER;
    if (!$user) {
        $user = $USER['id'];
    }
    $user = (int)$user;
    $onwer_id = (int)$onwer_id;
    $post = query_escape($post);
    $files = query_escape($files);
    if (empty($post)) {
//        return;
    }
    query("INSERT INTO `nx_wall` (`owner_id`,`text`,`user_id`,`created`,`images`)VALUES('$onwer_id','$post','" . $user . "','" . time() . "','" . $files . "');");
}

function enc_passw($password)
{
    return md5($password);
}

function isVer($USER, $asLevel = false)
{
    $status = '';
    $time = time();
    if ($USER['level'] == 0) {
        $status = 'Новичок';
        $level = 0;
    } elseif (($USER['level'] == 1 && $USER['pro_end'] > $time)) {
        $status = 'Мастер';
        $level = 1;
    } elseif (($USER['level'] == 2 && $USER['pro_end'] > $time)) {
        $status = 'Профессионал';
        $level = 2;
    } elseif (($USER['level'] == 3 && $USER['pro_end'] > $time)) {
        $status = 'Эксперт';
        $level = 3;
    } else {
        $status = 'Новичок';
        $level = 0;
        /* $status = 'ПРО ';
          if ($USER['pro_end'] - $time > 24 * 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 24 / 3600) . ' дней';
          } elseif ($USER['pro_end'] - $time > 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 3600) . ' часов';
          } elseif ($USER['pro_end'] - $time > 60) {
          $status .= (int) (($USER['pro_end'] - $time) / 60) . ' минут';
          } elseif ($USER['level'] > 2) {
          $status .= 'навсегда';
          } else {
          $status .= (int) ($USER['pro_end'] - $time) . ' секунд';
          } */
    }

    return !$asLevel ? $status : $level;
}

function vers($level = false)
{
    static $vers = array(
        1 => array(500, array(10, 15, 20, 25)),
        2 => array(6000, array(10, 15, 20, 25)),
        3 => array(12000, array(10, 15, 20, 25)),
    );

    if ($level === false) {
        return $vers;
    }
    return isset($vers[$level]) ? $vers[$level] : false;
}


function iconVer($USER)
{
    $status = '';
    $time = time();
    if ($USER['level'] == 0) {
        $status = '//gm1lp.ru/images/tariffs/tariff_new.png';
    } elseif (($USER['level'] == 1 && $USER['pro_end'] > $time)) {
        $status = '//gm1lp.ru/images/tariffs/tariff_master.png';
    } elseif (($USER['level'] == 2 && $USER['pro_end'] > $time)) {
        $status = '//gm1lp.ru/images/tariffs/tariff_pro.png';
    } elseif (($USER['level'] == 3 && $USER['pro_end'] > $time)) {
        $status = '//gm1lp.ru/images/tariffs/tariff_expert.png';
    } else {
        $status = '//gm1lp.ru/images/tariffs/tariff_new.png';
        /* $status = 'ПРО ';
          if ($USER['pro_end'] - $time > 24 * 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 24 / 3600) . ' дней';
          } elseif ($USER['pro_end'] - $time > 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 3600) . ' часов';
          } elseif ($USER['pro_end'] - $time > 60) {
          $status .= (int) (($USER['pro_end'] - $time) / 60) . ' минут';
          } elseif ($USER['level'] > 2) {
          $status .= 'навсегда';
          } else {
          $status .= (int) ($USER['pro_end'] - $time) . ' секунд';
          } */
    }

    return $status;
}

function sym_date($tdate = '', $sep = '.', $short = false)
{
    $e = explode($sep, $tdate);
    if ($short) {
        $treplace = array(
            '',
            "янв",
            "февр",
            "марта",
            "апр",
            "мая",
            "июн",
            "июл",
            "авг",
            "сент",
            "окт",
            "ноя",
            "дек"
        );
    } else {
        $treplace = array(
            '',
            "января",
            "февраля",
            "марта",
            "апреля",
            "мая",
            "июня",
            "июля",
            "августа",
            "сентября",
            "октября",
            "ноября",
            "декабря"
        );
    }

    if ($e[1]) {
        $e[1] = $treplace[(int)$e[1]];
    }
    return implode(' ', $e);
}

function getSessionKey($id = false)
{
    global $text, $USER;
    if ($_SERVER['HTTP_HOST'] == 'gm1lp.com') {
        return '-1';
    }
    if (!$id) {
        $id = $USER['id'];
    }
    if (!$text) {
        $text = new Memcache;
        $text->connect(MEMCACHE_HOST, MEMCACHE_PORT);
    }
    $text->set('secret' . $id, substr(md5($id), 24)); // Обязательно 8 знаков
    $user_secret = $text->get('secret' . $id);

    $im_secret1 = "0123456789ABCDEF"; // replace with appropriate secret values
    $im_secret2 = "0123456789ABCDEF";
    $im_secret3 = "0123456789ABCDEF";
    $im_secret4 = "0123456789ABCDEF";

    $subnet = extractSubnetwork($_SERVER['HTTP_X_REAL_IP']);
    $nonce = sprintf("%08x", mt_rand(0, 0)); # Перевод случ. числа в 8-ю систему счисления
    $hlam1 = substr(md5($nonce . $im_secret1 . $subnet), 4, 8);

    #echo md5($nonce . $im_secret1 . $nonce)."\n";

    $utime = sprintf("%08x", time());
    $utime_xor = xor_str($utime, $hlam1);
    $uid = sprintf("%08x", $id);
    $uid_xor = xor_str($uid, substr(md5($nonce . $subnet . $utime_xor . $im_secret2), 6, 8));
    $check = substr(md5($utime . $uid . $nonce . $im_secret4 . $subnet . $user_secret), 12, 16);

    #echo $nonce."\n".mt_rand(0x7fffffff, 0x7fffffff)."\n";
    $im_session = $nonce . $uid_xor . $check . $utime_xor;

    return $im_session;
}

function getSeesionTime($id = false)
{
    global $text, $USER;
    if ($_SERVER['HTTP_HOST'] == 'gm1lp.com') {
        return '-1';
    }
    if (!$id) {
        $id = $USER['id'];
    }
    if (!$text) {
        $text = new Memcache;
        $text->connect("localhost", 11201);
    }
    $time = $text->get('timestamp' . $id);
    return !$time ? 0 : $time;
}

function extractSubnetwork($ip)
{
    return substr($ip, 0, strrpos($ip, '.')) . '.';
}

function char_to_hex($c)
{
    $c = ord($c);
    if ($c <= 57) {
        return ($c - 48);
    } else {
        return ($c - 97 + 10);
    }
}

function hex_to_char($c)
{
    if ($c < 10) {
        $c = chr($c + 48);
    } else {
        $c = chr($c - 10 + 97);
    }
    return $c;
}

function xor_str($str1, $str2, $digits = 8)
{
    for ($i = 0, $j = 0; $i < $digits; $i++, $j++) {
        $str1[$i] = hex_to_char(char_to_hex($str1[$i]) ^ char_to_hex($str2[$j]));
    }
    return $str1;
}

function addDialogCookie($user_id)
{
    if (!isset($_COOKIE['dialog_' . $user_id])) {
        setcookie('dialog_' . $user_id, '1', time() + 3600, '/');
        $_COOKIE['dialog_' . $user_id] = 1;
    }
}

function delDialogCookie($user_id)
{
    setcookie('dialog_' . $user_id, '0', time() - 100, '/');
    unset($_COOKIE['dialog_' . $user_id]);
}

function delDialogsCookie()
{
    $keys = array_keys($_COOKIE);
    $c = count($keys);
    for ($i = 0; $i < $c; $i++) {
        if (strpos($keys[$i], 'dialog_') !== false) {
            setcookie($keys[$i], '0', time() - 100, '/');
            unset($_COOKIE[$keys[$i]]);
        }
    }
}

function getDialogsCookie()
{
    $dialogs = array();
    $keys = array_keys($_COOKIE);
    $c = count($keys);
    for ($i = 0; $i < $c; $i++) {
        if (strpos($keys[$i], 'dialog_') !== false) {
            $dialogs[] = getUserById(str_replace('dialog_', '', $keys[$i]));
        }
    }
    return $dialogs;
}

function getArticle($url)
{
    $url = query_escape($url);
    $q = query("SELECT * FROM `nx_articles` WHERE `url`='$url';");
    if (!$q['num_rows']) {
        return false;
    }
    return $q['row'];
}

function getReferals($id)
{
    $id = (int)$id;
    $q = query("SELECT * FROM `users` WHERE `referal`='$id';");

    return array($q['num_rows'], $q['rows']);
}

function moneyForRef($referal)
{
    $referal = (int)$referal;
    query("UPDATE `users` SET `money`=`money`+1 WHERE `id`='$referal';");
}

function addUser($email, $password, $fname, $lname, $referal = false)
{
    $email = query_escape($email);
    $password = md5($password); //md5(SOL.$password.SOL);
    $time = time();
    ///
    $fname = query_escape($fname);
    $lname = query_escape($lname);
    #$referal = (int)$referal;
	

    if ($referal) {
        $referal_ = getUserById($referal);
		if(!$referal_){
			$referal = getUserByUrl($referal);
		}else{
			$referal = $referal_;
		}
    }
    if (!$referal) {
        $referal = array('id' => 0);
    }

    query("INSERT INTO `users` (`name`,`fam`,`password`,`login`,`confirmed`,`created`,`referal`) VALUES('$fname','$lname','" . $password . "','$email','0','" . time() . "','" . $referal['id'] . "');");

    return query_lastInsertId();
}

function outputPost($post, $priv_wallc = true, $priv_wallsc = true, $onwer = false)
{
    global $USER;
    if ($post['user_id'] < 0) {
        $user = getGroup($post['user_id']);
        $user['ava'] = '/uploads/g_avatars/' . $user['ava'];
        $name = $user['title'];
    } else {
        $user = getUserById($post['user_id']);
        $user['ava'] = '/uploads/avatars/' . $user['ava'];
        $name = $user['name'] . ' ' . $user['fam'];
    }
    $owner_id = $post['owner_id'];
    $like = getLikesPost($post['wall_id'], $USER['id']);
    $reposts = getRepost($post['wall_id'], $USER['id']);
    $comments = $priv_wallsc ? query("SELECT * FROM `nx_wall_comments` WHERE `post_id`='" . $post['wall_id'] . "' ORDER BY `created` ASC;") : array('row' => array(), 'rows' => array());
    $post['comments'] = count($comments['rows']);
    $likes = '';
    $images_ = unserialize($post['images']);
    $cImages = count($images_);
    $images = '';
    if ($images_) {
        foreach ($images_ as $image) {
            $images .= '<a href="' . $image . '" rel="img' . $post['wall_id'] . '" class="fancybox"><img src="' . $image . '" /></a>';
        }
    }
    if ($post['likes']) {
        $likesList = getLikesPost($post['wall_id'], false, 9999);
        foreach ($likesList as $like_) {
            $user_ = getUserById($like_);
            $likes .= '<li><a href="/id' . $user_['id'] . '"><img class="width20" src="/uploads/avatars/' . $user_['ava'] . '" /></a></li>';
        }
    }

    preg_match_all('/\[(\w+)=(\d+)\]/i', $post['text'], $attachments);
    $post['text'] = preg_replace('/\[(\w+)=(\d+)\]/i', '', $post['text']);
	
	
	//$pattern = '#(www\.|https?:\/\/){?}[a-zA-Z0-9]{2,254}\.[a-zA-Z0-9]{2,4}(\S*)#i'; 
	//$count = preg_match_all($pattern,  $post['text'], $matches, PREG_PATTERN_ORDER);
	
    $attachments_ = '';
    $docs = '';
    foreach ($attachments[0] as $k => $v) {
        $key = $attachments[1][$k];
        $val = $attachments[2][$k];
        if ($key == 'video') {
            $video = getVideo($val);
            if (!$video) {
                continue;
            }
            $attachments_ .= '<div class="gallery rounded_2">
                        <div class="gallery-image" data-video="' . $video['url'] . '"></div>
                        <div class="gallery-details">
                            <a href="' . $video['url'] . '" target="_blank" class="link small">' . $video['title'] . '</a>
                        </div>
                    </div>';
            $cImages++;
        } elseif ($key == 'doc') {
            $doc = getDoc($val);
            if (!$doc) {
                continue;
            }
            $docs .= '<br/><a class="wall_icon wall_icon_file normal text" href="/uploads/docs/' . $doc['owner_id'] . '/' . $doc['url'] . '" target="blank">' . $doc['url'] . '</a>';
        }
    }
    $del = '';
    if (!$post['repost'] && ((time() - $post['created']) < 7 * 60 * 60) && $post['user_id'] == $USER['id']) {
        $del = '<a href="#" data-post_id="' . $post['wall_id'] . '" class="editWall"><img src="/images/icon-toolbox-pencil11.png" /></a>';
    }

    $output = '<div class="wall_post" data-wid="' . $post['wall_id'] . '">
    <a class="hide" name="post' . $post['wall_id'] . '"></a>
            <div class="wall_post_profile">
                <a href="/' . $user['url'] . '"><img src="' . $user['ava'] . '" class="width46"></a>
            </div>
            <div class="wall_post_content">
                <div class="wall_post_header">
                    <div class="wall_post_toolbox_top" style="' . ($user['id'] != $USER['id'] && $owner_id != $USER['id'] && !$onwer ? 'display:none;' : '') . '">' . $del . '<a href="#" data-post_id="' . $post['wall_id'] . '" class="delWall"><img src="/images/icon-toolbox-close2.png" /></a></div>

                    <a class="wall_post_profile_link" href="/' . $user['url'] . '">' . $name . '</a>
                    <span class="wall_post_date" data-s_time="' . $post['created'] . '"></span>
                   <a class="wall_post_profile_link" href="post?id=' . $post['wall_id'] . '" target="_blank">#</a>
                </div>';
    $class = '';
    if ($attachments_ && $cImages) {
        $class = ' two';
    }
    if ($post['repost']) {
        $repost = getPost($post['repost']);

        if ($repost['user_id'] < 0) {
            $user_repost = getGroup($repost['user_id']);
            $user_repost['ava'] = '/uploads/g_avatars/' . $user_repost['ava'];
            $name = $user_repost['title'];
        } else {
            $user_repost = getUserById($repost['user_id']);
            $user_repost['ava'] = '/uploads/avatars/' . $user_repost['ava'];
            $name = $user_repost['name'] . ' ' . $user_repost['fam'];
        }
        $rimages_ = unserialize($repost['images']);
        $rimages = '';
        if ($rimages_) {
            foreach ($rimages_ as $rimage) {
                $rimages .= '<a href="' . $rimage . '" rel="img' . $repost['wall_id'] . '" class="fancybox"><img src="' . $rimage . '" /></a>';
            }
        }

        $text = preg_replace('/<br[^>]*>/', "\n", $repost['text']);
        $len_text = mb_strlen(strip_tags($text));
        $text = strip_tags($text, '<img>');
        $short = nl2br($text) . $docs;
        if ($len_text > 256) {
            $short = nl2br(mb_substr(strip_tags($text), 0, 256));
            $short = '<span>' . $short . '...</span> <a href="#">Читать полностью</a><span style="display:none;">' . nl2br($text) . $docs . '</span>';
        }
        $output .= '<div class="wall_repost_message rounded_3">
                    <div class="wall_repost_header">
                        <div class="repost_arrow"></div>
                        <img src="' . $user_repost['ava'] . '" class="rounded_3" width="36" height="36">
                        <a href="/' . $user_repost['url'] . '">' . $name . '</a>
                        <span class="wall_post_date" data-s_time="' . $repost['created'] . '"></span>
                    </div>

                    <div class="wall_repost_content">
                        <div class="' . $class . '">
                            <div class="grid_wall_images c' . count($images_) . '">
                                <div class="img_' . count($rimages_) . '">
                                ' . $rimages . '
                                </div>
                            </div>
                            <div class="attachments">' . $attachments_ . '</div>
                        </div>
                        <div class="wall_text">' . $short . '</div>
                    </div>
                </div>';
    } else {
        $text = preg_replace('/<br[^>]*>/', "\n", $post['text']);
        $len_text = mb_strlen(strip_tags($text));
        $text = strip_tags($text, '<img>');
        $short = nl2br($text) . $docs;
        if ($len_text > 256) {
            $short = nl2br(mb_substr(strip_tags($text), 0, 256));
            $short = '<span>' . $short . '...</span> <a href="#">Читать полностью</a><span style="display:none;">' . nl2br($text) . $docs . '</span>';
        }
        $output .= '<div class="wall_post_message">
                    <div class="' . $class . '">
                        <div class="grid_wall_images c' . count($images_) . '">
                            <div class="img_' . $cImages . '">
                            ' . $images . '
                            </div>
                        </div>
                        <div class="attachments">' . $attachments_ . '</div>
                    </div>
                    <div class="wall_text">' . $short . '</div>
                </div>';
    }
    $output .= '<div class="wall_post_toolbox_bottom">
                    <div class="wall_post_toolbox_bottom_left">
                        <a href="#" class="wall_icon wall_icon_note">' . $post['comments'] . '</a>
                    </div>

                    <div class="wall_post_toolbox_bottom_right">
                        <a href="#" class="wall_icon wall_icon_repost' . ($reposts ? '_y' : '') . '" style="' . ($post['owner_id'] == $USER['id'] || $post['user_id'] == $USER['id'] ? 'display:none;' : '') . '">' . $post['reposts'] . '</a>
                        <a href="#" class="wall_icon wall_icon_counter' . ($like ? '_y' : '') . '">' . $post['likes'] . '</a>

                        <ul>
                        ' . $likes . '
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
                
                ' . outputCommentsPost($post['wall_id'], false, $post, $priv_wallsc, $onwer) . '
                
                <!-- new -->
                ' . ($priv_wallc && $USER['id'] ? '<div class="wall_post_comment rounded_3">
                    <div class="wall_post_profile">
                        <img src="/uploads/avatars/' . $USER['ava'] . '" class="width30">
                    </div>
                    <div class="wall_post_content">
                    <form method="POST" class="wallComment">
                        <textarea class="rounded_3" rows="1" placeholder="Комментировать..." name="text"></textarea>
                        <div class="myEmojiFieldComment"></div>
                        <div class="wall_post_btn">
                            <button class="btn rounded_3">Отправить</button>
                            ' . ($onwer ? '<label><input type="checkbox" value="1" name="fromGroup" /> От имени ' . ($onwer - 1 ? 'компании' : 'сообщества') . '</label><br/>' : '') . '
                            <div class="wall_post_btn_right">
                                <a href="#" class="wall_icon wall_icon_photo normal uploadWallComment">Прикрепить фотографию</a>
                            </div>

                            <div>
                                <ul class="photos_main uploadListComment">
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <input name="wid" type="hidden" value="' . $post['wall_id'] . '">
                    </form>
                    </div>
                </div>' : '') . '
            </div>
        </div>';

    return $output;
}

function editPost($post_id)
{
    $post_id = (int)$post_id;

}

function outputCommentsPost($post_id, $all = false, $post = false, $priv_wallsc = true, $owner = false)
{
    global $USER;
    if (!$priv_wallsc) {
        return '';
    }
    $post_id = (int)$post_id;

    $comments = query("SELECT * FROM `nx_wall_comments` WHERE `post_id`='$post_id' ORDER BY `created` ASC;");
    if (!$post) {
        $post = query("SELECT `owner_id` FROM `nx_wall` WHERE `wall_id`='$post_id';");
        $owner_id = $post['row']['owner_id'];
    } else {
        $owner_id = $post['owner_id'];
    }
    $comments_ = array();
    $count = count($comments['rows']);
    foreach ($comments['rows'] as $comment) {
        $like = getLikesComments($comment['cid'], $USER['id']);

        if ($comment['user_id'] > 0) {
            $user = getUserById($comment['user_id']);
            $user['ava'] = '/uploads/avatars/' . $user['ava'];
            $name = $user['name'] . ' ' . $user['fam'];
        } else {
            $user = getGroup($comment['user_id']);
            $user['ava'] = '/uploads/g_avatars/' . $user['ava'];
            $name = $user['title'];
        }

        $comments_[] = '<div class="wall_post" data-cid="' . $comment['cid'] . '">
                        <div class="wall_post_profile">
                            <a href="/' . $user['url'] . '"><img src="' . $user['ava'] . '" class="width46"></a>
                        </div>
                        <div class="wall_post_content">
                            <div class="wall_post_header">
                                ' . ($owner_id == $USER['id'] || $user['id'] == $USER['id'] || $owner ? '<div class="wall_post_toolbox_top">
                                    <a href="#" data-post_id="' . $post_id . '" data-post_cid="' . $comment['cid'] . '" class="delComment"><img src="/images/icon-toolbox-close2.png"></a>
                                </div>' : '') . '

                                <a class="wall_post_profile_link" href="/' . $user['url'] . '">' . $name . '</a>
                                <span class="wall_post_date" data-s_time="' . $comment['created'] . '"></span>
                            </div>
                            <div class="wall_post_message">
                                <p>
                                    ' . nl2br($comment['message']) . '
                                </p>
                                <div class="toolbox_comment">
                                    <a href="#" class="wall_icon wall_icon_counter' . ($like ? '_y' : '') . ' lcomment" data-cid="' . $comment['cid'] . '" data-wid="' . $comment['post_id'] . '">' . $comment['likes'] . '</a>
                                </div>
                            </div>
                        </div>
                    </div>';
    }
    $comments__ = '';
    if ($count > 5 && !$all) {
        $comments__ = $comments_[$count - 3] . $comments_[$count - 2] . $comments_[$count - 1];
    } else {
        $comments__ = implode("\n", $comments_);
    }
    $output = '<!--POST comments -->
                <div class="wall_post_comments">
                    ' . ($count > 5 && !$all ? '<div class="wall_post_more_comments rounded_3><span class="text">Показать все комментарии</span></div>' : '<div class="wall_post_more_comments rounded_3" style="padding: 1px;height: 1px;"></div>') . '

                    ' . $comments__ . '
                </div>';

    return $count ? $output : '<div class="wall_post_comments"></div>';
}

function addPostComment($wid, $post, $user_id = false)
{
    global $USER;

    $wid = (int)$wid;
    $post = query_escape($post);
    if (!$user_id) {
        $user_id = $USER['id'];
    } else {
        $user_id = (int)$user_id;
    }

    query("INSERT INTO `nx_wall_comments` (`user_id`,`message`,`created`,`post_id`)VALUES('$user_id','$post','" . time() . "','$wid');");
}

$stazh = array(
    '',
    'меньше года',
    '2 года',
    '3 года',
    '4 года',
    '5 лет',
    '6 лет',
    '7 лет',
    '8 лет',
    '9 лет',
    '10 лет',
    '11 лет',
    '12 лет',
    '13 лет',
    'больше 14 лет',
);
$months = array(
    '',
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июн",
    "Июл",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
);
$countrys = array(
    '',
    'Россия',
    'Украина'
);
$citys = array(
    '',
    'Барнаул',
    'Чебоксары'
);

function getContacts($user_id)
{
    $user_id = (int)$user_id;
    $cache = cache_get('contacts.' . $user_id);
    if (!$cache) {
        $q = query("SELECT * FROM `nx_contacts` WHERE `owner_id`='{$user_id}';");
        $r = $q['rows'];
        $w = array();
        foreach ($r as $contact) {
            $w[$contact['contact_id']] = array($contact['contact_id'], $contact['counter']);
        }
        $r = $w;
        cache_set('contacts.' . $user_id, $r);
    } else {
        $r = $cache;
    }
    return $r;
}

function getCounterContacts($user_id)
{
    $user_id = (int)$user_id;
    $cache = cache_get('ccontacts.' . $user_id);
    if (!$cache) {
        $q = query("SELECT * FROM `nx_contacts` WHERE `contact_id`='{$user_id}' AND `counter`=0;");
        $r = $q['rows'];
        $w = array();
        foreach ($r as $contact) {
            $w[$contact['owner_id']] = array($contact['owner_id'], $contact['counter']);
        }
        $r = $w;
        cache_set('ccontacts.' . $user_id, $r);
    } else {
        $r = $cache;
    }
    return $r;
}


function getInvitesContacts($user_id)
{
    $user_id = (int)$user_id;
    $cache = cache_get('invites_contacts.' . $user_id);
    if (!$cache) {
        $q = query("SELECT * FROM `users` WHERE `referal`='{$user_id}';");
        $r = $q['rows'];
        $w = array();
        foreach ($r as $contact) {
            $w[$contact['id']] = array($contact['id'], 1);
        }
        $r = $w;
        cache_set('invites_contacts.' . $user_id, $r);
    } else {
        $r = $cache;
    }
    return $r;
}


function getCounterInvitesContacts($user_id)
{
    $user_id = (int)$user_id;
    $cache = cache_get('ccontacts.' . $user_id);
    if (!$cache) {
        $q = query("SELECT * FROM `users` WHERE `referal`='{$user_id}'");
        $r = $q['rows'];
        $w = array();
        foreach ($r as $contact) {
            $w[$contact['owner_id']] = array($contact['owner_id'], $contact['counter']);
        }
        $r = $w;
        cache_set('ccontacts.' . $user_id, $r);
    } else {
        $r = $cache;
    }
    return $r;
}

function addContact($contact_id)
{
    global $USER;
    $r = 0;
    $contact_id = (int)$contact_id;
    $user_id = $USER['id'];
    $counter = query("SELECT * FROM `nx_contacts` WHERE `contact_id`='$user_id' AND `owner_id`='$contact_id';");
    if ($counter['row']) {
        query("INSERT INTO `nx_contacts` (`contact_id`,`owner_id`,`counter`) VALUES('$contact_id','$user_id','1')");
        query("UPDATE `nx_contacts` SET `counter`='1' WHERE `contact_id`='$user_id' AND `owner_id`='$contact_id';");
        $r = 1;
    } else {
        query("INSERT INTO `nx_contacts` (`contact_id`,`owner_id`) VALUES('$contact_id','$user_id')");
        $r = 2;
    }
    cache_delete('contacts.' . $user_id);
    cache_delete('contacts.' . $contact_id);
    cache_delete('ccontacts.' . $user_id);
    cache_delete('ccontacts.' . $contact_id);
    cache_delete('cContacts.' . $user_id);
    cache_delete('cContacts.' . $contact_id);

    return $r;
}

function removeContact($contact_id)
{
    global $USER;
    $r = 0;
    $contact_id = (int)$contact_id;
    $user_id = $USER['id'];
    $counter = query("SELECT * FROM `nx_contacts` WHERE `contact_id`='$user_id' AND `owner_id`='$contact_id';");
    if ($counter['row']) {
        query("DELETE FROM `nx_contacts` WHERE `contact_id`='$contact_id' AND `owner_id`='$user_id';");
        query("UPDATE `nx_contacts` SET `counter`='2' WHERE `contact_id`='$user_id' AND `owner_id`='$contact_id';");
        $r = 1;
    } else {
        query("DELETE FROM `nx_contacts` WHERE `contact_id`='$contact_id' AND `owner_id`='$user_id';");
        $r = 2;
    }
    cache_delete('contacts.' . $user_id);
    cache_delete('contacts.' . $contact_id);
    cache_delete('ccontacts.' . $user_id);
    cache_delete('ccontacts.' . $contact_id);
    cache_delete('cContacts.' . $user_id);
    cache_delete('cContacts.' . $contact_id);

    return $r;
}

function getCountContacts($user_id)
{
    $user_id = (int)$user_id;
    $cache = cache_get('cContacts.' . $user_id);
    if (!$cache) {
        $q = query("SELECT COUNT(*) as `count` FROM `nx_contacts` WHERE `owner_id`='{$user_id}' AND `counter`=1;");
        $r = $q['row'];
        cache_set('cContacts.' . $user_id, $r);
    } else {
        $r = $cache;
    }
    return $r;
}

function findUser($query, $limit = 30, $counter = false)
{
    global $USER;
    $two = explode(" ", $query);
    $arg1 = query_escape($two[0]);
    $arg2 = '';
    $sql = "(`name` like '%{$arg1}%' OR `fam` like '%{$arg1}%')";
    if (isset($two[1])) {
        $arg2 = query_escape($two[1]);
        $sql = " (`name` like '%{$arg1}%' AND `fam` like '%{$arg2}%') OR (`name` like '%{$arg2}%' AND `fam` like '%{$arg1}%')";
    }

    $q = query("SELECT * FROM `users` WHERE `id`!='" . $USER['id'] . "' AND {$sql} LIMIT 0, $limit;");
    if ($counter) {
        $w = query("SELECT COUNT(DISTINCT `id`) as `count` FROM `users` WHERE `id`!='" . $USER['id'] . "' AND {$sql};");

        return array($w['row']['count'], $q['rows']);
    }

    return $q['rows'];
}

function findGroup($query, $type, $limit = 30, $counter = false)
{
    global $USER;
    $arg1 = query_escape($query);
    $type = (int)$type;
    $sql = "(`title` like '%{$arg1}%')";

    $q = query("SELECT * FROM `nx_groups` WHERE `type`='" . $type . "' AND {$sql} LIMIT 0, $limit;");
    if ($counter) {
        $w = query("SELECT COUNT(DISTINCT `group_id`) as `count` FROM `nx_groups` WHERE `type`='" . $type . "' AND {$sql};");

        return array($w['row']['count'], $q['rows']);
    }

    return $q['rows'];
}

function findVideo($query, $limit = 30, $counter = false)
{
    global $USER;
    $arg1 = query_escape($query);
    $sql = "(`title` like '%{$arg1}%')";

    $q = query("SELECT * FROM `nx_videos` WHERE {$sql} LIMIT 0, $limit;");
    if ($counter) {
        $w = query("SELECT COUNT(DISTINCT `video_id`) as `count` FROM `nx_videos` WHERE {$sql};");

        return array($w['row']['count'], $q['rows']);
    }

    return $q['rows'];
}

function getCountry($cid)
{
    $cid = (int)$cid;
    $r = cache_get('country.' . $cid);
    if (!$r) {
        $q = query("SELECT * FROM `country` WHERE `id`='{$cid}'");
        $r = $q['row'];
        cache_set('country.' . $cid, $r, 1800);
    }

    return $r;
}

function getCity($cid)
{
    $cid = (int)$cid;
    $r = cache_get('city.' . $cid);
    if (!$r) {
        $q = query("SELECT * FROM `city` WHERE `id`='{$cid}'");
        $r = $q['row'];
        cache_set('city.' . $cid, $r, 1800);
    }

    return $r;
}

function getCity2($id = false, $offset = 0, $count = 8000)
{
    if ($id === false) {
        $q = query("SELECT `id` as `city_id`,`city` as `name` FROM `city` ORDER BY `id` ASC LIMIT {$offset},{$count};");

        return $q['rows'];
    } else {
        $id = (int)$id;
        $q = query("SELECT `id` as `city_id`,`city` as `name` FROM `city` WHERE `id`='{$id}'");

        return $q['row'];
    }
}

function getCountry2($id = false, $offset = 0, $count = 8000)
{
    if ($id === false) {
        $q = query("SELECT `id` as `country_id`,`country` as `name` FROM `country` ORDER BY `id` ASC LIMIT {$offset},{$count};");

        return $q['rows'];
    } else {
        $id = (int)$id;
        $q = query("SELECT `id` as `country_id`,`country` as `name` FROM `country` WHERE `id`='{$id}'");

        return $q['row'];
    }
}

function getCityByName($city = '', $country = false, $offset = 0, $count = 8000)
{
    $city = query_escape($city);
    $country = (int)$country;

    if (!$country) {
        $q = query("SELECT `id` as `id`,`city` as `text` FROM `city` WHERE `city` like '%{$city}%' ORDER BY `id` ASC LIMIT {$offset},{$count};");
    } else {
        $q = query("SELECT `c`.`id` as `id`,`c`.`city` as `text` 
FROM `city` as `c` 
INNER JOIN `country_region` as `cr` ON `cr`.`id_country`='{$country}' 
INNER JOIN `region_city` as `rc` ON `rc`.`id_region`=`cr`.`id_region` AND `c`.`id`=`rc`.`id_city`
WHERE `c`.`city` like '%{$city}%' GROUP BY `c`.`id` ORDER BY `id` ASC LIMIT {$offset},{$count};");
    }

    return $q['rows'];
}

function getCountryByName($country = '', $offset = 0, $count = 8000)
{
    $country = query_escape($country);

    $q = query("SELECT `id` as `id`,`country` as `text` FROM `country` WHERE `country` like '%{$country}%' ORDER BY `id` ASC LIMIT {$offset},{$count};");

    return $q['rows'];
}

function getPageByAlias($alias)
{
    $alias = query_escape($alias);
    $q = query("SELECT * FROM `nx_help_pages` WHERE `alias`='{$alias}';");

    return $q['row'];
}

function getPageById($id)
{
    $id = (int)$id;
    $q = query("SELECT * FROM `nx_help_pages` WHERE `page_id`='{$id}';");

    return $q['row'];
}

function getPages()
{
    $q = query("SELECT * FROM `nx_help_pages` WHERE 1 ORDER BY `page_id` ASC");

    return $q['rows'];
}

function addPage($alias, $title, $content)
{
    $alias = query_escape($alias);
    $title = query_escape($title);
    $content = query_escape($content);

    $q = query("INSERT INTO `nx_help_pages` (`alias`,`title`,`content`) VALUES ('{$alias}','{$title}','{$content}');");

    return 1;
}

function updatePage($id, $alias, $title, $content)
{
    $id = (int)$id;
    $alias = query_escape($alias);
    $title = query_escape($title);
    $content = query_escape($content);

    query("UPDATE `nx_help_pages` SET `alias`='{$alias}',`title`='{$title}',`content`='{$content}' WHERE `page_id`='{$id}';");
}

function removePage($id)
{
    $id = (int)$id;

    query("DELETE FROM `nx_help_pages` WHERE `page_id`='{$id}';");
}

function prepareImg($data)
{
    global $SYSTEM;

    $path = substr(strchr($data['file'], '/tmp/'), 5);
//var_dump($path);
    $file = str_replace('..', '', 'tmp/' . $path);

    $type = getExtension($file);

    switch ($type) {
        case 'jpg':
        case 'jpeg':
            $src_func = 'imagecreatefromjpeg';
            $write_func = 'imagejpeg';
            $image_quality = 100;
            break;
        case 'gif':
            $src_func = 'imagecreatefromgif';
            $write_func = 'imagegif';
            $image_quality = null;
            break;
        case 'png':
            $src_func = 'imagecreatefrompng';
            $write_func = 'imagepng';
            $image_quality = 9;
            break;
        default:
            return false;
    }
    $src_img = $src_func($file);
    $coords = $data['coords'];
    if (!isset($coords['x1'])) {
        $coords['x1'] = $coords['x'];
        $coords['y1'] = $coords['y'];
    }

    $new_width = $coords['x2'] - $coords['x1'];
    $new_height = $coords['y2'] - $coords['y1'];

    $new_img = imagecreatetruecolor($new_width, $new_height);
    switch ($type) {
        case 'gif':
        case 'png':
            imagecolortransparent($new_img, imagecolorallocate($new_img, 0, 0, 0));
        case 'png':
            imagealphablending($new_img, false);
            imagesavealpha($new_img, true);
            break;
    }

    $new_file_path = 'tmp/' . microtime(true) . '.' . $type;

    list($width, $height) = getimagesize($file);
    $success = imagecopyresampled(
            $new_img, $src_img, 0, 0, $coords['x'], $coords['y'], $new_width, $new_height, $coords['w'], $coords['h']
        ) && $write_func($new_img, $new_file_path, $image_quality);
    return $new_file_path;
}

function itOneFile($file){
	$arr = array();
	foreach($file as $key=>$val){
		$arr[$key] = $val[0];		
	}
	return $arr;
}

function getLikesSkill($skill_id, $user_id = false, $limit = 3, $ava = false)
{
    global $pmc;
    $skill_id = (int)$skill_id;
    $user_id = (int)$user_id;
    if (!$user_id) {
        if (!$limit) {
            $q = query("SELECT * FROM `nx_skill_likes` WHERE `skill_id`='$skill_id';");
        } else {
            $q = query("SELECT * FROM `nx_skill_likes` WHERE `skill_id`='$skill_id' ORDER BY `like_id` DESC LIMIT 0,{$limit};");
        }
    } else {
        $q = query("SELECT * FROM `nx_skill_likes` WHERE `skill_id`='$skill_id' AND `user_id`='$user_id';");
    }
    $likes = array();
    if ($ava) {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $user = getUserById($q['rows'][$i]['user_id']);
            $likes[$q['rows'][$i]['user_id']] = array($user['id'], $user['ava']);
        }
    } else {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $likes[$q['rows'][$i]['user_id']] = $q['rows'][$i]['user_id'];
        }
    }
    if ($user_id) {
        return isset($likes[$user_id]);
    }

    return $likes;
}

function getUserSkills($user_id, $likes = false)
{
    $user_id = (int)$user_id;
    $q = query("SELECT * FROM `nx_skills` WHERE `user_id`='$user_id' ORDER BY `likes` DESC;");
    if ($likes) {
        foreach ($q['rows'] as $i => $row) {
            $q['rows'][$i]['likes_html'] = '';
            if ($row['likes']) {
                $likesList = getLikesSkill($row['skill_id'], false, 4, true);
                foreach ($likesList as $like_) {
                    $q['rows'][$i]['likes_html'] .= '<li><a href="/id' . $like_[0] . '"><img class="width20" src="/uploads/avatars/' . $like_[1] . '" /></a></li>';
                }
            }
        }
    }
    return $q['rows'];
}

function getUserSkill($skill_id, $likes = false)
{
    $skill_id = (int)$skill_id;
    $q = query("SELECT * FROM `nx_skills` WHERE `skill_id`='$skill_id';");
    if ($likes) {
        $q['row']['likes_html'] = '';
        if ($q['row']['likes']) {
            $likesList = getLikesSkill($q['row']['skill_id'], false, 4, true);
            foreach ($likesList as $like_) {
                $q['row']['likes_html'] .= '<li><a href="/id' . $like_[0] . '"><img class="width20" src="/uploads/avatars/' . $like_[1] . '" /></a></li>';
            }
        }
    }

    return $q['row'];
}

function likeSkill($skill_id)
{
    global $USER, $pmc;
    $skill_id = (int)$skill_id;
    $uid = $USER['id'];
    $like = getLikesSkill($skill_id, $USER['id']);
    if ($like) {
        query("UPDATE `nx_skills` SET `likes`=`likes`-1 WHERE `skill_id`='$skill_id';");
        query("DELETE FROM `nx_skill_likes` WHERE `skill_id`='$skill_id' AND `user_id`='$uid';");
    } else {
        query("UPDATE `nx_skills` SET `likes`=`likes`+1 WHERE `skill_id`='$skill_id';");
        query("INSERT INTO `nx_skill_likes` (`skill_id`,`user_id`)VALUES('$skill_id','$uid');");
    }
}

function removeUserSkill($user_id, $skill_id)
{
    $user_id = (int)$user_id;
    $skill_id = (int)$skill_id;

    query("DELETE FROM `nx_skill_likes` WHERE `skill_id`='$skill_id';");
    query("DELETE FROM `nx_skills` WHERE `skill_id`='$skill_id';");
}

function addUserSkill($user_id, $skill)
{
    $user_id = (int)$user_id;
    $skill = query_escape($skill);
    query("INSERT INTO `nx_skills` (`user_id`,`name`) VALUES ('$user_id','$skill');");
}

function showSkill($skill_id, $skill = false, $i = false, $like = false)
{
    global $USER;
    $skill_id = (int)$skill_id;
    if (!$skill) {
        $skill = getUserSkill($skill_id, $like);
    }
    return '<tr data-id="' . $skill['skill_id'] . '" ' . ($i > 4 ? 'style="display:none"' : '') . '>
                <td class="skill_title">' . $skill['name'] . '</td>
                <td class="skill_amount">' . $skill['likes'] . '</td>
                <td class="skill_progress"><div class="progressline" percentage="' . ($skill['likes']) . '"></div></td>
                <td class="skill_profiles">
                    <ul>
                        ' . ($USER['id'] ? '<li class="skill_profile_btn"><a href="#" data-id="' . $skill['skill_id'] . '" data-uid="' . $skill['user_id'] . '"><img src="/images/btn_add.gif" /></a></li>' : '') . '
                        ' . $skill['likes_html'] . '
                    </ul>
                </td>
            </tr>';
}

function showPhoto($photo_id, $photo = false, $album = false)
{
    global $USER;
    $album_id = $photo['album_id'];
    $likes_html = '';
    if ($photo['likes']) {
        $likes = getLikesPhoto($photo_id, false, 3, true);
        $likes_html = '<ul>';
        foreach ($likes as $like) {
            $likes_html .= '<li><a href="/id' . $like[0] . '"><img class="width20 rounded_50p" src="/uploads/avatars/' . $like[1] . '"></a></li>';
        }
        $likes_html .= '</ul>';
    }
    return '<div class="photo-time">
        <div class="p-time">Добавлена <span data-s_time="' . $photo['created'] . '"></span></div>
        <div class="p-album">
            Альбом <a href="/album?id=' . $album_id . '">' . $album['title'] . '</a></div>
        <div class="clear"></div>
    </div>
    <div class="photo-repost-like">
    ' . $photo['desc'] . '
        <div class="likes">
            <a href="#" data-id="' . $photo_id . '" class="like_photo wall_icon wall_icon_counter' . (isset($likes) && isset($likes[$USER['id']]) ? '_y' : '') . '">' . $photo['likes'] . '</a>
            ' . $likes_html . '
        </div>
        <div class="clear"></div>
    </div>' . getPhotoComments($photo_id);
}

function getPhotoComments($photo_id, $write = true)
{
    global $USER;
    $output_w = '<div class="photo-write">
                <div class="photo-profile">
                    <a href="/' . $USER['url'] . '"><img src="/uploads/avatars/' . $USER['ava'] . '" class="width46 circled" /></a>
                </div>
                <div class="photo-content">
                    <form id="photoComment">
                        <textarea style="display: none;" class="rounded_3" name="text" rows="1" placeholder="Что у Вас нового?"></textarea>
                        <div id="myEmojiFieldPhoto" class="short"></div>
                        <div class="wall_post_btn">
                            <button class="btn rounded_3">Отправить</button>
                        </div>
                        <input type="hidden" name="photo_id" value="' . $photo_id . '" />
                    </form>
                </div>
                <div class="clear"></div>
            </div>';
    $output = '<div class="photo-comments wall_wrapper">';
    $comments = getCommentsPhoto($photo_id);
    $count = count($comments);
    $limit = $count > 4 ? $count - 4 : 0;
    $hide = 'hide';
    if ($limit) {
        $output .= '<div class="showAll">показать все комментарии</div>';
    }
    foreach ($comments as $i => $comment) {
        if ($i > $limit) {
            $hide = '';
        }
        $output .= getPhotoComment($comment['cid'], $comment, $hide);
    }
    $output .= '</div>
    ' . ($write ? $output_w : '') . '
    <div class="clear"></div>';


    return $output;
}

function getPhotoComment($cid, $data = false, $hide = '')
{
    global $USER;
    $cid = (int)$cid;
    if (!$data) {
        $data = getCommentsPhoto(false, $cid);
        $data = $data[0];
    }
    $like = getLikesPhotoComments($cid, $USER['id']);

    $output = '<div class="photo-comment ' . $hide . '">
            <div class="photo-profile">
                <a href="/' . $data['user']['url'] . '"><img src="/uploads/avatars/' . $data['user']['ava'] . '" class="width46 circled" /></a>
            </div>
            <div class="photo-content">
                <div class="photo-header">
                    <a class="profile_link" href="/' . $data['user']['url'] . '">' . $data['user']['name'] . ' ' . $data['user']['fam'] . '</a>
                    <span class="photo-date" data-s_time="' . $data['created'] . '"></span>
                </div>
                <div class="photo-message">
                    <p>' . $data['message'] . '</p>
                    <div class="toolbox_comment">
                        <a href="#" class="photo_like wall_icon wall_icon_counter' . ($like ? '_y' : '') . ' lcomment" data-cid="' . $cid . '" data-pid="' . $data['photo_id'] . '">' . $data['likes'] . '</a>
                    </div>
                </div>
            </div>
        </div>';

    return $output;
}

function getLikesPhoto($photo_id, $user_id = false, $limit = 3, $ava = false)
{
    global $pmc;
    $photo_id = (int)$photo_id;
    $user_id = (int)$user_id;
    if (!$user_id) {
        if (!$limit) {
            $q = query("SELECT * FROM `nx_photo_likes` WHERE `photo_id`='$photo_id';");
        } else {
            $q = query("SELECT * FROM `nx_photo_likes` WHERE `photo_id`='$photo_id' ORDER BY `like_id` DESC LIMIT 0,{$limit};");
        }
    } else {
        $q = query("SELECT * FROM `nx_photo_likes` WHERE `photo_id`='$photo_id' AND `user_id`='$user_id';");
    }
    $likes = array();
    if ($ava) {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $user = getUserById($q['rows'][$i]['user_id']);
            $likes[$q['rows'][$i]['user_id']] = array($user['id'], $user['ava']);
        }
    } else {
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $likes[$q['rows'][$i]['user_id']] = $q['rows'][$i]['user_id'];
        }
    }
    if ($user_id) {
        return isset($likes[$user_id]);
    }

    return $likes;
}

function gen_password($max = 6)
{
    $symbols = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    $cS = count($symbols) - 1;

    $newpass = '';
    for ($i = 0; $i < $max; $i++) {
        $newpass .= rand(0, 1) == 1 ? $symbols[rand(0, $cS)] : strtoupper($symbols[rand(0, $cS)]);
    }

    return $newpass;
}

function isPrivate($user_id, $private, $flag, $flag2, $contacts)
{
    global $USER;
    if ($user_id == $USER['id']) {
        return true;
    }
    if ($private & $flag) {
        return true;
    }
    if ($private & $flag2 && isset($contacts[$USER['id']]) && $contacts[$USER['id']][1]) {
        return true;
    }

    return false;
}

function show_main_album($uid, $priv_photo = true, $ignoreEmpty = false)
{
    global $USER;

    if ($priv_photo) {
        $albums = getUserAlbums($uid, true);
        $count_albums = count($albums);
        if (!$count_albums && !$ignoreEmpty) {
            return '';
        }
        $last_album_photo = '/images/no_scrn_mov.jpg';
        if ($albums && isset($albums[0]['last_photo']) && $albums[0]['last_photo']['photo_id']) {
            $last_album_photo = '/uploads/albums/' . $albums[0]['album_id'] . '/preview' . $albums[0]['last_photo']['photo_id'] . '.jpg';
        }
        return '<!--Photo gallery -->
    <div>
        <div class="block_header">
            <h2><a href="/albums?id=' . $uid . '">Фотоальбомы</a></h2>
            <span class="counter rounded_3"><a
                    href="/albums?id=' . $uid . '">' . $count_albums . '</a></span>

            <div class="clear"></div>
        </div>
        ' . ($count_albums ? '
        <div class="block_content">
            <div class="gallery rounded_2">
                <div class="gallery-image">
                    <a href="/album?id=' . $albums[0]['album_id'] . '"><img
                            src="' . $last_album_photo . '"></a>
                </div>
                <div class="gallery-details">
                    <a href="/album?id=' . $albums[0]['album_id'] . '"
                       class="link">' . $albums[0]['title'] . '</a><br/>
                    <span
                        class="description">' . declOfNum($albums[0]['size'], array('фотогафия', 'фотографии', 'фотографий')) . '</span>
                </div>
            </div>
        </div>' : '') . '
    </div>';
    }

    return '';

}

function show_main_videos($uid, $priv_video = true, $ignoreEmpty = false)
{
    global $USER;
    $myVideos = getVideos($uid);
    $count_videos = count($myVideos);
    if ($priv_video) {
        if ($count_videos) {
            return '
            <!--Videos -->
            <div>
                <div class="block_header">
                    <h2><a href="/video?id=' . $uid . '">Видеозаписи</a></h2>
                    <span class="counter rounded_3"><a
                            href="/video?id=' . $uid . '">' . $count_videos . '</a></span>

                    <div class="clear"></div>
                </div>

                <div class="block_content">
                    <div class="gallery rounded_2">
                        <div class="gallery-image" data-video="' . $myVideos[0]['url'] . '">
                        </div>
                        <div class="gallery-details">
                            <a href="' . $myVideos[0]['url'] . '" target="_blank" class="link small">' . $myVideos[0]['title'] . '</a>
                        </div>
                    </div>
                </div>
            </div>';
        } elseif ($ignoreEmpty) {
            return '
            <!--Videos -->
            <div>
                <div class="block_header">
                    <h2><a href="/video?id=' . $uid . '">Видеозаписи</a></h2>
                    <span class="counter rounded_3"><a
                            href="/video?id=' . $uid . '">' . $count_videos . '</a></span>

                    <div class="clear"></div>
                </div>
            </div>';
        }
    }

    return '';

}

function show_main_contacts($uid, $cMyContacts, $myContacts, $priv_contacts = true)
{
    global $USER;

    $itsI = false;
    if ($USER['id'] == $uid) {
        $itsI = true;
    }
    $output = '';

    if ($cMyContacts && $priv_contacts) {
        $output .= '<!--Contacts -->
    <div>
        <div class="block_header">
            <h2><a href="/contacts' . ($itsI ? '' : '?id=' . $uid) . '">Контакты</a></h2>
            <span class="counter rounded_3"><a href="/contacts' . ($itsI ? '' : '?id=' . $uid) . '">' . $cMyContacts['count'] . '</a></span>
            <div class="clear"></div>
        </div>

        <div class="block_content">
            <div class="photos_contacts">';

        $showContacts = 9;
        shuffle($myContacts);
        foreach ($myContacts as $contact) {
            if ($contact[1] != 1) {
                continue;
            }
            if ($showContacts <= 0) {
                break;
            }
            $showContacts--;
            $contact = getUserById($contact[0]);
            $output .= '<div>
                    <a href="/' . $contact['url'] . '"><img class="rounded_3" src="' . '/uploads/avatars/' . $contact['ava'] . '" /></a><br />
                    <a href="/' . $contact['url'] . '">' . $contact['name'] . '<br /> ' . $contact['fam'] . '</a>
                </div>';
        }
        $output .= '</div>
        </div>
        <div class="clear"></div>
    </div>';
    }

    return $output;
}

function show_main_companies($uid, $priv_companies = true)
{
    global $USER;
    $output = '';

    if ($priv_companies) {
        $groups = getUserGroups($uid, 1);
        if (!count($groups)) {
            return '';
        }
        $output .= '<!--Communities -->
        <div>
            <div class="block_header">
                <h2><a href="/groups?type=1&id=' . $uid . '">Компании</a></h2>
                <span class="counter rounded_3"><a href="/groups?type=1&id=' . $uid . '">' . count($groups) . '</a></span>
                <div class="clear"></div>
            </div>

            <div class="block_content">';
        foreach ($groups as $i => $group) {
            if ($i > 4) {
                break;
            }
            $output .= '<div>
                    <div class="iblock">
                        <a href="/' . $group['url'] . '"><img width="77" class="rounded_3" src="/uploads/g_avatars/' . $group['ava'] . '" /></a>
                    </div>

                    <div class="idetails">
                        <a href="/' . $group['url'] . '" class="link">' . $group['title'] . '</a><br />
                        <span class="title">' . $group['area'] . '</span><br />
                        <span class="description">' . declOfNum($group['members'], array('отслеживающий', 'отслеживающих', 'отслеживающих')) . '</span>
                    </div>
                    <div class="clear"></div>
                </div>';
        }
        $output .= '</div>
        </div>';
    }

    return $output;
}


function show_main_communities($uid, $priv_communities = true)
{
    global $USER;
    $output = '';

    if ($priv_communities) {
        $groups = getUserGroups($uid, 0);
        if (!count($groups)) {
            return '';
        }
        $output .= '<!--Communities -->
        <div>
            <div class="block_header">
                <h2><a href="/groups?type=0&id=' . $uid . '">Сообщества</a></h2>
                <span class="counter rounded_3"><a href="/groups?type=0&id=' . $uid . '">' . count($groups) . '</a></span>
                <div class="clear"></div>
            </div>

            <div class="block_content">';
        foreach ($groups as $i => $group) {
            if ($i > 4) {
                break;
            }
            $output .= '<div>
                    <div class="iblock">
                        <a href="/' . $group['url'] . '"><img width="77" class="rounded_3" src="/uploads/g_avatars/' . $group['ava'] . '" /></a>
                    </div>

                    <div class="idetails">
                        <a href="/' . $group['url'] . '" class="link">' . $group['title'] . '</a><br />
                        <span class="title">' . $group['area'] . '</span><br />
                        <span class="description">' . declOfNum($group['members'], array('подписчик', 'подписчика', 'подписчиков')) . '</span>
                    </div>
                    <div class="clear"></div>
                </div>';
        }
        $output .= '</div>
        </div>';
    }

    return $output;
}

function show_main_audios($uid, $priv_audios = true)
{
    global $USER;
    $output = '';

    if ($priv_audios) {
        $output .= '<!--Audios -->
        <div>
            <div class="block_header">
                <h2><a href="#" class="showAllRadio">Радиостанции</a></h2>
                <span class="counter rounded_3"><a href="#" class="showAllRadio">10</a></span>
                <div class="clear"></div>
            </div>

            <div class="block_content audio">

                <div>
                    <div class="iblock">
                         <a href="http://www.loveradio.ru/player.htm" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="http://www.loveradio.ru/player.htm" target="_blank">Love Radio</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <div class="iblock">
                         <a href="http://www.europaplus.ru" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="http://www.europaplus.ru" target="_blank">Europa Plus</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <div class="iblock">
                        <a href="http://www.dfm.ru/listen/dfmonline/dfmdeep" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="http://www.dfm.ru/listen/dfmonline/dfmdeep" target="_blank">DFM Deep</a>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="radioHide">
                    <div class="iblock">
                        <a href="http://www.energyfm.ru/?an=nrj_online_page" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="http://www.energyfm.ru/?an=nrj_online_page" target="_blank">Energy</a>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="radioHide">
                    <div class="iblock">
                       <a href="http://www.moskva.fm/play/4015/translation?adv=echomsk" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="http://www.moskva.fm/play/4015/translation?adv=echomsk" target="_blank">Эхо Москвы</a>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="radioHide">
                    <div class="iblock">
                        <a href="http://www.relax-fm.ru" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="http://www.relax-fm.ru" target="_blank">Relax FM</a>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="radioHide">
                    <div class="iblock">
                        <a href="http://www.dfm.ru/listen/dfmonline/dfm" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="http://www.dfm.ru/listen/dfmonline/dfm" target="_blank">DFM</a>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="radioHide">
                    <div class="iblock">
                      <a href="http://www.kommersant.ru/fm/radio" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="http://www.kommersant.ru/fm/radio" target="_blank">Коммерсантъ</a>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="radioHide">
                    <div class="iblock">
                       <a href="//www.moskva.fm/stations/FM_95.2" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="//www.moskva.fm/stations/FM_95.2" target="_blank">ROCK FM</a>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="radioHide">
                    <div class="iblock">
                        <a href="//www.moskva.fm/stations/FM_100.5" target="_blank"><img src="/images/btn-play.gif" /></a>
                    </div>
                    <div class="idetails">
                        <a href="//www.moskva.fm/stations/FM_100.5" target="_blank">BEST FM</a>
                    </div>
                    <div class="clear"></div>
                </div>

            </div>
        </div>';
    }

    return $output;
}


function getGroup($group_id, $onlyType = false)
{
    $group_id = (int)abs($group_id);
    if ($onlyType !== false) {
        $onlyType = (int)$onlyType;
        $q = query("SELECT * FROM `nx_groups` WHERE `group_id`='$group_id' AND `type`='$onlyType';");
    } else {
        $q = query("SELECT * FROM `nx_groups` WHERE `group_id`='$group_id';");
    }
    return $q['row'];
}

function typeMember($type = false, $onlyKeys = false)
{
    static $types = array(
        0 => 'Ожидает подтверждения',
        1 => 'Подписчик',
        2 => 'Сотрудник',
        3 => 'Модератор',
        10 => 'Администратор',
        11 => 'Создатель'
    );
    if ($onlyKeys) {
        return array(0 => array(), 1 => array(), 2 => array(), 3 => array(), 10 => array(), 11 => array());
    }
    if ($type === false) {
        return $types;
    }

    return isset($types[$type]) ? $types[$type] : '#';
}

function createGroup($title, $type, $area, $country, $city, $desc, $founding = false)
{
    global $USER;
    $title = query_escape($title);
    $type = (int)$type;
    $area = query_escape($area);
    $city = query_escape($city);
    $country = query_escape($country);
    $desc = query_escape($desc);
    $founding = (int)$founding;
    $created = time();
    if (!$founding) {
        $founding = $created;
    }
    $owner_id = $USER['id'];
    query("INSERT INTO `nx_groups` (`owner_id`,`title`,`type`,`area`,`city`,`country`,`desc`,`founding`,`created`)
VALUES ('$owner_id','$title','$type','$area','$city','$country','$desc','$founding','$created')");

    $group_id = query_lastInsertId();
    addMemberInGroup($group_id, $USER['id'], 11);
    updateGroupUrl($group_id, 'group' . $group_id);

    return $group_id;
}

function updateGroupUrl($group_id, $url)
{
    $group_id = (int)$group_id;
    $url = query_escape($url);

    query("UPDATE `nx_groups` SET `url`='$url' WHERE `group_id`='$group_id';");
}

function getGroupByUrl($url)
{
    $url = query_escape($url);
    $q = query("SELECT * FROM `nx_groups` WHERE `url`='$url';");

    return $q['row'];
}

function addMemberInGroup($group_id, $user_id, $type = 1)
{
    $group_id = (int)$group_id;
    $user_id = (int)$user_id;
    $type = (int)$type;
    $created = time();

    query("INSERT INTO `nx_members` (`group_id`,`user_id`,`type`,`created`)
VALUES ('$group_id','$user_id','$type','$created')");
    query("UPDATE `nx_groups` SET `members`=`members`+1 WHERE `group_id`='$group_id';");
}

function updateMemberInGroup($group_id, $user_id, $type = 1, $role = '', $contacts = '')
{
    $group_id = (int)$group_id;
    $user_id = (int)$user_id;
    $type = (int)$type;
    $role = query_escape($role);
    $contacts = query_escape($contacts);
    $sql = '';
    if ($role) {
        $sql .= ",`role`='$role'";
    }
    if ($contacts) {
        $sql .= ",`contacts`='$contacts'";
    }

    query("UPDATE `nx_members` SET `type`='$type'$sql WHERE `group_id`='$group_id' AND `user_id`='$user_id';");
}

function removeMemberInGroup($group_id, $user_id)
{
    $group_id = (int)$group_id;
    $user_id = (int)$user_id;

    query("DELETE FROM `nx_members` WHERE `user_id`='$user_id' AND `group_id`='$group_id';");
    query("UPDATE `nx_groups` SET `members`=`members`-1 WHERE `group_id`='$group_id';");
}

function updateGroupAva($group_id, $ava)
{
    $group_id = (int)$group_id;
    $ava = query_escape($ava);

    query("UPDATE `nx_groups` SET `ava`='$ava' WHERE `group_id`='$group_id';");
}

function updateGroupSettings($group_id, $title, $desc, $area, $country, $city, $site, $founding = false)
{

    $group_id = (int)$group_id;
    $title = query_escape($title);
    $desc = query_escape($desc);
    $area = query_escape($area);
    $country = query_escape($country);
    $city = query_escape($city);
    $site = query_escape($site);
    $founding = (int)$founding;

    $sql = '';
    if ($founding) {
        $sql = "`founding`='$founding',";
    }

    query("UPDATE `nx_groups` SET $sql`title`='$title',`desc`='$desc',`area`='$area',`country`='$country',`city`='$city',`site`='$site' WHERE `group_id`='$group_id';");
}

function upSeeInGroup($group_id)
{
    $group_id = (int)$group_id;

    query("UPDATE `nx_groups` SET `see`=`see`+1 WHERE `group_id`='$group_id';");
}

function getMembersInGroup($group_id, $grouping = true)
{
    $group_id = (int)$group_id;
    $q = query("SELECT * FROM `nx_members` WHERE `group_id`='$group_id' AND `type`>0;");
    if ($grouping) {
        $members = array('group' => typeMember(false, true));
        foreach ($q['rows'] as $row) {
            $members[$row['user_id']] = $row;
            $members['group'][$row['type']][$row['user_id']] = $row;
        }
        return $members;
    }
    return $q['rows'];
}

function getUserGroups($user_id, $type, $onlyAdmin = false, $getGroups = true)
{
    $user_id = (int)$user_id;
    $type = (int)$type;
    $q = query("SELECT *, `group_id`*-1 as `nabs` FROM `nx_members` WHERE `user_id`='$user_id' AND `type`>=0 ORDER BY `created` DESC;");
    $groups = array();
    if (!$getGroups) {
        return $q['rows'];
    }
    foreach ($q['rows'] as $group) {
        $group = getGroup($group['group_id'], $type);
        if ($group) {
            $groups[] = $group;
        }
    }
    return $groups;
}

function show_membersInGroup($group, $members, $private = false, $right = false, $type = 0)
{
    global $USER;

    $itsI = false;
    if ($USER['id'] == $group['']) {
        $itsI = true;
    }
    if ($type == 0) {
        $type = 'Подписчики';
    } else {
        $type = 'Отслеживают';
    }
    $output = '';

    if ($group['members']) {
        $output .= '<!--Contacts -->
    <div>
        <div class="block_header">
            <h2>' . $type . '</h2>
            <span class="counter rounded_3"><a href="/memberList?id=' . $group['group_id'] . '">' . $group['members'] . '</a></span>
            ' .
            ($right ? '<!--a href="/members/set?id=' . $group['group_id'] . '" style="display:inline-block;margin:13px 0 0 10px;">Ред.</a-->' : '')
            . '
            <div class="clear"></div>
        </div>

        <div class="block_content">
            <div class="photos_contacts">';

        $showContacts = 9;
        unset($members['group']);
        shuffle($members);
        foreach ($members as $member) {
            if ($showContacts <= 0) {
                break;
            }
            $showContacts--;
            $contact = getUserById($member['user_id']);
            $output .= '<div>
                    <a href="/' . $contact['url'] . '"><img class="rounded_3" src="' . '/uploads/avatars/' . $contact['ava'] . '" /></a><br />
                    <a href="/' . $contact['url'] . '">' . $contact['name'] . '<br /> ' . $contact['fam'] . '</a>
                </div>';
        }
        $output .= '</div>
        </div>
        <div class="clear"></div>
    </div>';
    }

    return $output;
}

function show_ContactsInGroup($group, $members, $private = false, $right = false, $type = 0)
{
    global $USER;

    $itsI = false;
    if ($USER['id'] == $group['']) {
        $itsI = true;
    }
    $output = '';
    foreach ($members as $k => $member) {
        if ($member['type'] < 3) {
            unset($members[$k]);
        }
    }
    if ($type == 0) {
        $type = 'Контакты';
    } else {
        $type = 'Представители';
    }
    if ($group['members']) {
        $output .= '<!--Contacts -->
    <div>
        <div class="block_header">
            <h2>' . $type . '</h2>
            <span class="counter rounded_3"><a href="#">' . count($members) . '</a></span>
            ' .
            ($right ? '<!--a href="/members/set?id=' . $group['group_id'] . '" style="display:inline-block;margin:13px 0 0 10px;">Ред.</a-->' : '')
            . '
            <div class="clear"></div>
        </div>

        <div class="block_content">
            <div class="photos_contacts rukv">';

        $showContacts = 9;
        unset($members['group']);
        #shuffle($members);
        foreach ($members as $member) {
            $contact = getUserById($member['user_id']);
            $output .= '<div>
                    <a href="/' . $contact['url'] . '"><img class="rounded_3" src="' . '/uploads/avatars/' . $contact['ava'] . '" /></a>
                    <div class="right_info">
                    <a href="/' . $contact['url'] . '">' . $contact['name'] . ' ' . $contact['fam'] . '</a>
                    <br/>
                    <span>' . $member['role'] . '</span><br/>
                    <span>' . nl2br($member['contacts']) . '</span>
                    </div>
                </div>';
        }
        $output .= '</div>
        </div>
        <div class="clear"></div>
    </div>';
    }

    return $output;
}

function getNotices($offset = 0, $limit = 3, $onlyNew = false)
{
    global $USER;
    $uid = $USER['id'];
    $offset = (int)$offset;
    $limit = (int)$limit;
    $user_groups = getUserGroups($USER['id'], 'all', false, false);
    if (!$user_groups) {
        return array();
    }
    $groups = array();
    foreach ($user_groups as $group) {
        $groups[] = $group['nabs'];
    }
    $sql = '';
    if ($onlyNew) {
        $sql = " AND `wall_id`>'" . $USER['last_wid'] . "'";
    }
    $q = query("SELECT * FROM `nx_wall` WHERE `user_id` IN (" . implode(',', $groups) . ") AND `repost`=0 AND `deleted`=0 AND `owner_id`!='$uid' $sql ORDER BY `created` DESC LIMIT $offset,$limit");

    return $q['rows'];
}


function getCountNotices($onlyNew = false)
{
    global $USER;
    $uid = $USER['id'];
    $user_groups = getUserGroups($USER['id'], 'all', false, false);
    if (!$user_groups) {
        return 0;
    }
    $groups = array();
    foreach ($user_groups as $group) {
        $groups[] = $group['nabs'];
    }
    $sql = '';
    if ($onlyNew) {
        $sql = " AND `wall_id`>'" . $USER['last_wid'] . "'";
    }
    $q = query("SELECT COUNT(*) as `count` FROM `nx_wall` WHERE `user_id` IN (" . implode(',', $groups) . ") AND `repost`=0 AND `deleted`=0 AND `owner_id`!='$uid' $sql ORDER BY `created` DESC");

    return $q['row']['count'];
}

function forOwnerReferal($owner_id, $amount)
{
    $owner_id = (int)$owner_id;
    $amount = (float)$amount;
    static $costs = array(5, 15, 40, 70);

    $user = getUserById($owner_id);
    if (!$amount || !$owner_id || !$user || $user['referal']) {
        return false;
    }
    $level = isVer($user, true);
    if (!isset($costs[$level])) {
        return false;
    }
    $proc = $costs[$level];
    $amount = ($amount / 100) * $proc;

    query("INSERT INTO `nx_balance_log` (`user_id`,`amount`,`type`,`created`,`comment`) VALUES('$owner_id','$amount','1','" . time() . "','Отчисления от реферала');");

    updateUserById($user['id'], array('money'), array(($user['money'] + $amount)));

    return true;
}

function iconStatus($status, $main = false)
{
    if ($status < 1) {
        return '';
    }
    if ($main) {
        return '<div class="sinfo_success_wrapper s' . $status . ' ' . ($main ? 'small' : '') . '">
                    <a href="/help/exp" title="Страница подтверждена"><span class="sinfo_success"></span></a>
                </div>';
    }
    return '<div class="sinfo_success_wrapper s' . $status . ' ' . ($main ? 'small' : '') . '">
                    <span class="sinfo_success"><a href="/help/exp">Страница подтверждена</a></span>
                </div>';
}

function Signature($params, $api_key)
{
    ksort($params);
    reset($params);

    return md5(implode($params) . $api_key);
}

function translit($urlstr, $isk = '_')
{
    if (preg_match('/[^A-Za-z0-9_\-]/', $urlstr)) {

        $tr = array(
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g",
            "Д" => "d", "Е" => "e", "Ж" => "j", "З" => "z", "И" => "i",
            "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "yi", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
            "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            " " => "_", "." => "", "/" => "_", "ё" => "e", ";" => "", ":" => "", "," => ""
        );
        $tr[$isk] = $isk;
        $urlstr = strtr($urlstr, $tr);

        $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
    }

    return $urlstr;

}

function addDoc($url, $user_id = false)
{
    global $USER;
    $url = query_escape($url);
    $user_id = (int)$user_id;
    $time = time();
    if (!$user_id) {
        $user_id = $USER['id'];
    }

    query("INSERT INTO `nx_docs` (`owner_id`,`url`,`created`)VALUES('$user_id','$url','$time');");

    return query_lastInsertId();
}

function getDoc($doc_id)
{
    $doc_id = (int)$doc_id;

    $q = query("SELECT * FROM `nx_docs` WHERE `doc_id`='$doc_id';");

    return $q['row'];
}

function is_image($filename)
{
    $is = @getimagesize($filename);
    if (!$is) return false;
    elseif (!in_array($is[2], array(1, 2, 3))) return false;
    else return true;
}

function sendMessage($phone, $message)
{
    $params = array(
        'timestamp' => file_get_contents('https://new.sms16.ru/get/timestamp.php'),
        'login' => 'Deamone',
        'phone' => $phone,
        'text' => $message,
        'sender' => 'GM1LP'
    );
    $signature = Signature($params, 'cc664a303c2a9d42e19c5b09dc5d6418533491ba');
    $params['signature'] = $signature;
    $q = file_get_contents('https://new.sms16.ru/get/send.php?' . http_build_query($params));
    $q = json_decode($q, true);
    if (isset($q['error']) || (isset($q[0]) && isset($q[0][0]) && isset($q[0][0]['error']))) {
        return false;
    }
    return true;
}

function checkPhoneNumber($phoneNumber)
{

    $phoneNumber = preg_replace('/\s|\+|-|\(|\)/', '', $phoneNumber); // удалим пробелы, и прочие не нужные знаки

    if (is_numeric($phoneNumber)) {
        if (strlen($phoneNumber) < 5) // если длина номера слишком короткая, вернем false
        {
            return FALSE;
        } else {
            return $phoneNumber;
        }
    } else {
        return FALSE;
    }
}
