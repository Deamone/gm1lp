<?php
class Session
{
    private static $_instance = null;

    private static $id = null;
    private static $uid = null;
    private static $login = null;
    private static $hash = null;

    private function __construct(){
        self::restore((int)$_COOKIE['session'], $_COOKIE['hash']);
    }
    private function __clone(){}

    public static function get_instance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function restore($id, $hash) {
        $q = query("SELECT * FROM `session` WHERE `id`='$id';");
        $s = $q['row'];
        if (count($s) && $hash !== null && $s['hash'] === $hash) {
            self::$id = $s['id'];
            self::$uid = $s['uid'];
            self::$login = $s['login'];
            self::$hash = $s['hash'];

            setcookie('session', $id, $s['expires'], '/');
            setcookie('hash', $hash, $s['expires'], '/');
        } else
            self::destroy(false);
    }

    public function create($uid, $time = null, $domain = null) {
        $user = getUserById($uid);
        if($user) {
            $uid = $user['id'];
            $login = $user['login'];
            $hash = md5(md5($uid * -1) . SOL . md5($login));
            if($time === null)
                $expires = "NULL";
            else {
                $time += time();
                $expires = "$time";
            }

            query("INSERT INTO `session`(`uid`,`login`,`hash`,`expires`) VALUES($uid,'$login','$hash',$expires)");
            $id = query_lastInsertId();

            self::$id = $id;
            self::$uid = $uid;
            self::$login = $login;
            self::$hash = $hash;

            setcookie('session', $id, $time, '/', $domain);
            setcookie('hash', $hash, $time, '/', $domain);
        }
    }

    public function destroy($redirect = true) {
        $id = self::get_id();
        query("DELETE FROM `session` WHERE `id` = $id");

        self::$id = null;
        self::$uid = null;
        self::$login = null;
        self::$hash = null;

        setcookie('session', '', time() - 3600, '/');
        setcookie('hash', '', time() - 3600, '/');

        if($redirect)
            redirect('/page/login');
    }

    public function get_id() {
        return (int)self::$id;
    }

    public function get_uid() {
        return (int)self::$uid;
    }

    public function get_hash() {
        return self::$hash;
    }
}