/* 
 =====================================================
 * created 22.01.2015 NotusX
 -----------------------------------------------------
 * http://notusx.ru/
 -----------------------------------------------------
 * Copyright (c) 2014,2015 NotusX
 =====================================================
 * Данный код защищен авторскими правами
 =====================================================
 * @author NotusX
 -----------------------------------------------------
 * Файл: message.js
 -----------------------------------------------------
 * Назначение: Не задано
 =====================================================
 */

var server = 'https://gm1lp.ru/im';
var wait = 3;
var open = false;
var cache_user = [];
var friends_online = [];
function log(txt) {
    console.log(txt);
}
function updateScroll() {
    if (scroll.destroy === undefined) {
        return;
    }
    scroll.destroy();
    scroll = $('.scrollblock').jScrollPane();
    scroll = scroll.data('jsp');
    scroll.scrollToPercentY(100);
}
function im() {
    if (open || !server) {
        return false;
    }
    open = true;
    $.post(server,
        {
            act: "a_check",
            key: session_key,
            mode: "64",
            ts: ts,
            "wait": wait

        }, function (data) {
            console.log(data);
            if (data === undefined || !data) {
                wait = 10000;
                return;
            }
            if (data.failed === 2) {
                wait = 10000;
                alert('ошибка, попробуйте авторизироваться заново');
            }
            ts = data.ts;
            if (!data.updates.length) {
                if (wait <= 30) {
                    wait += 2;
                }


            } else {
                wait = 3;
                for (var i = 0; i < data.updates.length; i++) {
                    var event = data.updates[i];
                    if (event[0] === 3) {
                        var local_id = event[1];
                        var mask = event[2];
                        var flags = parseInt($('div[data-id$="_' + local_id + '"]').attr('data-flag')); // TODO: repeat
                        $('div[data-id$="_' + local_id + '"]').attr('data-flag', flags &= ~mask);
                    }
                    if (event[0] === 4) {
                        updateNoticeMessage();
                        log('New message', true);
                        //ajax_mes();
                        updateScroll();
                        //playSound();
                        text = event[6];
                        time = event[4];
                        if (event[3] == current_dialog_id) {
                            $('.msg-wrap .type-1,.msg-wrap .type-0').removeClass('type-1 type-0');
                            $('div[data-dialog-id="' + (event[3]) + '"]').removeClass('type-1 type-0');

                            if (miniDialog === undefined || !miniDialog) {
                                if (event[2] == 2) { //current_dialog_id
                                    var audio = document.getElementById("audioNewMessage");
                                    audio.play();
                                    //$('div[data-dialog-id="' + (event[3]) + '"]').addClass('type-1');
                                    //$('div[data-dialog-id="' + (event[3]) + '"] .media-body small').text(text);
                                    //$('.msg-wrap').append('<div class="media msg type-1"><a class="pull-left" href="#"><img class="media-object" data-uid-src="' + my_uid + '" style="width: 32px; height: 32px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABWpJREFUeNrcV1lIo1cUPjExGneNWpfWukRBURG1LuCCFKEPikKFYVygtfjSaV/sU8GHgSlMX+ZFUOYplhYrSGFGtOBGsYWCjiuDC6igKGoc9zVqYtLvXP8/xJiJ0XkQeuFw///+955z7ne+c+79FVarlR6yedADtwd3QHWXycvLy5+g+xLyuUKhiEcfDPFEGE8gG3iesFgsr9H/lZCQcOmOToU7HIDhMHQ/wuhXfn5+wd7e3uTp6UkeHh5CYJTMZjOdnZ3R7u4uVFrfQH7S6XQ9H+wAjBfAcFtgYKAuICBAGLRvCwsLNDs7S+np6bS0tERzc3NUUVHBzliguxXyQ1JS0sW9HIDxchj/LTQ0NNDX19fpnO6ebiKoYD3z8/NUWlpKp6enlJubSzs7O4zIKyD0ODk5+dypAl7oTLCbHDiwfXLC4XXejo6OrCMjI7b3/v5+a3Nzs3Vtbc02trm5aQVC+vfZcYoADGuw83+0Wm02Yn7j++HhIa2urtLk5KSIf0lJCUVGRpJKpbpSqlBc2+D6+jrt7e3Vp6amtrkVAuy+EfF+AQdsBnt6eojDwPDCBEVERJC/n68g3+7BIW0ZDBQaqhXvGo2G8vPziTnDDSiyzgU4mwOu7LtMQ0zk3X/j7+9vG+vs7ITyUFpcXKSv6+spJJizjx2Xd2oRJWVsbIwGBwcpMzOTurq6qK6uTnxlx0HiRHCiGq+tLgsRECmGJKvVatsYFgu2f/vd9xTiryHz80oy//70yrTxkMzPysnyx8+UnZ1NZeXlNDQ0RI6kZTSgt3JiYiLitkpYGRISorAfKCwsJA6Hxkst4PeITiblR59exVCJevBxCpE2Wrwn6nRiLmeDPQ84LOg/g3xxWyVMt989t/DwcPLy8roimMqLzh89JYWHkrzYAbWGjI+fEa/h3SADKCcnhziEMtOZqExM8CYI37Mw7Zf3OoAFWqVSef3AQPHhuHZ0dFB1dTW929yk3t5eio6OJiNIaUQFrKmpoe3tbUHWhoYGury8vJlycAJ9mEsE5MmODYVEpFlLSwvFxsaKd2Z3APgRgRRsb28nNcpzbW2tmOfMAUaC5TYS7vBiZy0xMZGOj49penraVvvPz89FPzAwQHHx8YJ8vN5eZMMXFxesf8slApj4FkrzfXx8bjgwMzMjDqGsrCwqLi6+hhgXm9HRUYqLixNO2O+cGzu8sbHBNWD8NgS6uHw6QwDjwgDvWjYuOS36sLAwWllZse2YRXaCw4X3MUivSwcw4W8smGNYHRvSkw8XQUp7nrBDbIzDExUVdcM4y8HBAfevCwoKDC4dSElJOcViPWr3DXKilotnPvXsxxneqakpwrFLHDpH8rFj0LeA6e1uXcngQOvW1tYYH6cyxDLMVVVVIt329/dtKAwPD1NeXh6lpaXdyCJ+Zj3on6Og7bvlABQZYfAJ4rnDSMgpxM98LsTExFBfX5+Yy2N8IWG28zngeMSDeAx/G4y33elOmJGR8WZ8fLweh9OvKDiBQUFBBFTEmcBk4zCwYXaAOcEpZjQaqaioSNQBdphJC7ReoQA9udONqBwHityampqKYEAPowlc42UC2pPMPiO4MeFwX7AAlZcIVWNlZeX5fRzwhHC598SuwlDfG1HvHyETgphoXPvlks3wm0wmgYDBYLDifQLIvNDr9QP8GWKCcFqZu7u73XKAz1KN43hZWVl0fHx8OYwXAtY4SKB0LT8GIu+QDW9xefkTd4F/ka5mJxs+hQOn7nDgQiKo2u7WwQfNGrqXktylWSWdJndJaJJEJYWCe6XklDt/UxZJZPhN2Pnlff6MzJIoJMNKSRQOYnWQS0ksMGz94D+j//XP6X8CDAAiTMZ1tWcJwQAAAABJRU5ErkJggg==" /></a><div class="media-body"><!--small class="pull-right time"><i class="fa fa-clock-o"></i></small--><h5 class="media-heading" data-uid-text="' + my_uid + '">name</h5><small class="col-lg-10">' + text + '</small></div></div>');
                                    //$('#messages').append('<div class="blok_mess" data-flag="3" class="type-1"><a href="/id' + my_uid + '"><img class="rounded_3" src="' + peer_ava + '" /></a><a href="/id' + current_dialog_id + '">' + my_name + '</a><p class="mess_data">' + time + '</p><p class="mess_p">' + text + '</p></div>');
                                } else { // inbox
                                    //$('div[data-dialog-id="' + (event[3]) + '"]').addClass('type-0');
                                    //$('div[data-dialog-id="' + (event[3]) + '"] .media-body small').text(text);
                                    //$('.msg-wrap').append('<div class="media msg type-0"><a class="pull-left" href="#"><img class="media-object" data-uid-src="' + current_dialog_id + '" style="width: 32px; height: 32px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABWpJREFUeNrcV1lIo1cUPjExGneNWpfWukRBURG1LuCCFKEPikKFYVygtfjSaV/sU8GHgSlMX+ZFUOYplhYrSGFGtOBGsYWCjiuDC6igKGoc9zVqYtLvXP8/xJiJ0XkQeuFw///+955z7ne+c+79FVarlR6yedADtwd3QHWXycvLy5+g+xLyuUKhiEcfDPFEGE8gG3iesFgsr9H/lZCQcOmOToU7HIDhMHQ/wuhXfn5+wd7e3uTp6UkeHh5CYJTMZjOdnZ3R7u4uVFrfQH7S6XQ9H+wAjBfAcFtgYKAuICBAGLRvCwsLNDs7S+np6bS0tERzc3NUUVHBzliguxXyQ1JS0sW9HIDxchj/LTQ0NNDX19fpnO6ebiKoYD3z8/NUWlpKp6enlJubSzs7O4zIKyD0ODk5+dypAl7oTLCbHDiwfXLC4XXejo6OrCMjI7b3/v5+a3Nzs3Vtbc02trm5aQVC+vfZcYoADGuw83+0Wm02Yn7j++HhIa2urtLk5KSIf0lJCUVGRpJKpbpSqlBc2+D6+jrt7e3Vp6amtrkVAuy+EfF+AQdsBnt6eojDwPDCBEVERJC/n68g3+7BIW0ZDBQaqhXvGo2G8vPziTnDDSiyzgU4mwOu7LtMQ0zk3X/j7+9vG+vs7ITyUFpcXKSv6+spJJizjx2Xd2oRJWVsbIwGBwcpMzOTurq6qK6uTnxlx0HiRHCiGq+tLgsRECmGJKvVatsYFgu2f/vd9xTiryHz80oy//70yrTxkMzPysnyx8+UnZ1NZeXlNDQ0RI6kZTSgt3JiYiLitkpYGRISorAfKCwsJA6Hxkst4PeITiblR59exVCJevBxCpE2Wrwn6nRiLmeDPQ84LOg/g3xxWyVMt989t/DwcPLy8roimMqLzh89JYWHkrzYAbWGjI+fEa/h3SADKCcnhziEMtOZqExM8CYI37Mw7Zf3OoAFWqVSef3AQPHhuHZ0dFB1dTW929yk3t5eio6OJiNIaUQFrKmpoe3tbUHWhoYGury8vJlycAJ9mEsE5MmODYVEpFlLSwvFxsaKd2Z3APgRgRRsb28nNcpzbW2tmOfMAUaC5TYS7vBiZy0xMZGOj49penraVvvPz89FPzAwQHHx8YJ8vN5eZMMXFxesf8slApj4FkrzfXx8bjgwMzMjDqGsrCwqLi6+hhgXm9HRUYqLixNO2O+cGzu8sbHBNWD8NgS6uHw6QwDjwgDvWjYuOS36sLAwWllZse2YRXaCw4X3MUivSwcw4W8smGNYHRvSkw8XQUp7nrBDbIzDExUVdcM4y8HBAfevCwoKDC4dSElJOcViPWr3DXKilotnPvXsxxneqakpwrFLHDpH8rFj0LeA6e1uXcngQOvW1tYYH6cyxDLMVVVVIt329/dtKAwPD1NeXh6lpaXdyCJ+Zj3on6Og7bvlABQZYfAJ4rnDSMgpxM98LsTExFBfX5+Yy2N8IWG28zngeMSDeAx/G4y33elOmJGR8WZ8fLweh9OvKDiBQUFBBFTEmcBk4zCwYXaAOcEpZjQaqaioSNQBdphJC7ReoQA9udONqBwHityampqKYEAPowlc42UC2pPMPiO4MeFwX7AAlZcIVWNlZeX5fRzwhHC598SuwlDfG1HvHyETgphoXPvlks3wm0wmgYDBYLDifQLIvNDr9QP8GWKCcFqZu7u73XKAz1KN43hZWVl0fHx8OYwXAtY4SKB0LT8GIu+QDW9xefkTd4F/ka5mJxs+hQOn7nDgQiKo2u7WwQfNGrqXktylWSWdJndJaJJEJYWCe6XklDt/UxZJZPhN2Pnlff6MzJIoJMNKSRQOYnWQS0ksMGz94D+j//XP6X8CDAAiTMZ1tWcJwQAAAABJRU5ErkJggg==" /></a><div class="media-body"><!--small class="pull-right time"><i class="fa fa-clock-o"></i></small--><h5 class="media-heading" data-uid-text="' + current_dialog_id + '">name</h5><small class="col-lg-10">' + text + '</small></div></div>');
                                    //$('#messages').append('<div class="blok_mess" data-flag="2" class="type-0"><a href="/id' + my_uid + '"><img class="rounded_3" src="' + my_ava + '" /></a><a href="/id' + my_uid + '">' + my_name + '</a><p class="mess_data">' + time + '</p><p class="mess_p">' + text + '</p></div>');
                                }
                                ajax_mes();
                            } else {
                                ajax_mes_mini();
                            }
                        } else {
                            if (event[2] == 2) {
                                var audio = document.getElementById("audioNewMessage");
                                audio.play();
                            }
                        }
//$('div[data-uid="' + event[3] + '"] p').html(text);
                    }
                    if (event[0] === 8) {
                        if (cache_user[event[1] * -1] !== undefined) {
                            cache_user[event[1] * -1].online = 1;
                        }
                        if (friends_online[event[1] * -1] === undefined) {
                            friends_online[event[1] * -1] = event[1] * -1;
                            updateContent();
                        }
                        log('Friend ' + event[1] * -1 + ' online', true);
                    }
                    if (event[0] === 9) {
                        if (cache_user[event[1] * -1] !== undefined) {
                            cache_user[event[1] * -1].online = 0;
                        }
                        if (friends_online[event[1] * -1] !== undefined) {
                            delete friends_online[event[1] * -1];
                            //updateContent();
                        }
                        log('Friend ' + event[1] * -1 + ' offline', true);
                    }
                    if (event[0] === 61) {
                        log('User ' + event[1] * -1 + ' typing', true);
                    }
                    if (event[0] === 80) {
                        log('New messages ' + event[1] + '', true);
                    }
                    console.log(event);
                }
                updateScroll();
            }
            open = false;
            updateScroll();
        }, 'json').fail(function () {
            wait += 1000;
            open = false;
        });
}
function ajax_count() {
    var ids = {ids: []};
    var cont = $('*[data-mcid]');
    $.each(cont, function (i) {
        ids.ids[i] = $(this).attr('data-mcid');
    });
    $.post('https://gm1lp.ru/message?id=1&counts', ids, function (data) {
        $.each(data, function (i, elem) {
            if (elem[1]) {
                $('*[data-mcid="' + elem[0] + '"]').text('+' + elem[1]);
                $('*[data-mcid="' + elem[0] + '"]').css({'display': 'block'});
            } else {
                $('*[data-mcid="' + elem[0] + '"]').text('0');
                $('*[data-mcid="' + elem[0] + '"]').hide();
            }
        });
    }, 'json');
}

setInterval(im, 3000);