(function ($) {
    // Количество секунд в каждом временном отрезке
    var days = 24 * 60 * 60,
            hours = 60 * 60,
            minutes = 60;
    // Создаем плагин
    $.fn.countdown = function (prop) {
        var options = $.extend({
            callback: function () {
            },
            timestamp: 0
        }, prop);
        var left, w, d, h, m, s, positions;
        // инициализируем плагин
        positions = this.find('.bg_t');
        (function tick() {
            // Осталось времени
            left = Math.floor((options.timestamp - (new Date())) / 1000);
            if (left < 0) {
                left = 0;
            }
            // Осталось дней
            d = Math.floor(left / days);
            updateDuo(0, 1, d);
            left -= d * days;

            // Weeks
            w = Math.floor(d / 7);
            d = d-(w*7);
            if(d<0){
                d=0;
            }
            updateDuo(0, 1, d);
            
            // Осталось часов
            h = Math.floor(left / hours);
            updateDuo(2, 3, h);
            left -= h * hours;
            // Осталось минут
            m = Math.floor(left / minutes);
            updateDuo(4, 5, m);
            left -= m * minutes;
            // Осталось секунд
            s = left;
            updateDuo(6, 7, s);
            // Вызываем возвратную функцию пользователя
            options.callback(w, d, h, m, s);
            // Планируем следующий вызов данной функции через 1 секунду
            setTimeout(tick, 1000);
        })();
        // Данная функция обновляет две цифоровые позиции за один раз
        function updateDuo(minor, major, value) {
        }
        return this;
    };
    // Здесь размещаются две вспомогательные функции
})(jQuery);

function addNull(dig) {
    if (dig <= 9) {
        return '0' + dig;
    }

    return dig;
}
$(function () {
    var ts = new Date(2014, 10, 28);

    $('#countdown').countdown({
        timestamp: ts,
        callback: function (weeks, days, hours, minutes, seconds) {
            var message = "";
            $('.bg_t div:eq(0)').html(addNull(weeks));
            $('.bg_t div:eq(1)').html(addNull(days));
            $('.bg_t div:eq(2)').html(addNull(hours));
            $('.bg_t div:eq(3)').html(addNull(minutes));
            $('.bg_t div:eq(4)').html(addNull(seconds));
        }
    });
    $('.bg_c').click(function () {
        $('.bg_a').stop().animate({
            'top': '-1000px'

        }, 1000);
        return false;
    });
});
setTimeout(function () {
    $('.bg_a').show();
    $('.bg_a').stop().animate({
        'top': '0px'

    }, 1000);
}, 4500);