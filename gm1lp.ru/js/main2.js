/* PROGRESSLINE */
function renderProgressLine() {
    $('.progressline').each(function (i) {
        var percentage = $(this).attr('percentage');
        var max = 100;
        if (percentage > 100) {
            for (var i = 100; i <= 100000; i = ((i * 2) + 100)) {
                if (percentage >= i && percentage < ((i * 2) + 100)) {
                    max = ((i * 2) + 100);
                    break;
                }
            }
        }
        var width = $(this).width();
        var progressWidth = width * (percentage / max);

        $(this).html($('<div>').addClass('status').addClass('rounded_2').width(progressWidth));
    });
}

jQuery(document).ready(function ($) {
    $(document).on('click', '.wall_post_comment textarea, .myEmojiFieldComment', function () {
        $(this).attr('rows', 3).autosize();
        $(this).next('.wall_post_btn').show();
        $(this).next().next('.wall_post_btn').show();
    });

    /* MORE COMMENTS BUTTONS */
    var toggleMoreBtn = function (btn, count) {
        var $this = btn;
        if (count == 0) {
            $this.removeClass('wall_post_more_comments').removeClass('rounded_3').removeClass('wall_post_hide_comments');
            $this.children('.text').html('');
        } else {
            if ($this.hasClass('wall_post_more_comments')) {
                $this.removeClass('wall_post_more_comments').removeClass('rounded_3').addClass('wall_post_hide_comments');
                $this.children('.text').html('Скрыть комментарии');
            } else {
                $this.removeClass('wall_post_hide_comments').addClass('wall_post_more_comments').addClass('rounded_3');
                $this.children('.text').html('Показать все ' + count + ' комментария');
            }
        }
    }
    $('.wall_post_hide_comments, .wall_post_more_comments').click(function () {
        var $this = $(this);
        if ($this.hasClass('wall_post_more_comments')) {
            var wid = $(this).parent().parent().parent().attr('data-wid');
            $.post('', 'wid=' + wid + '&all_comments=1', function (data) {
                $('.wall_post[data-wid="' + wid + '"] .wall_post_comments')[0].outerHTML = data;
            });
        } else {
            var $parent = $(this).parent();
            var posts = $('.wall_post', $parent);
            var count = posts.length - 2;

            posts = posts.slice(0, count);
            posts.remove();

            toggleMoreBtn($this, count);
        }
    });

    /* POPUP */
    var hidePopups = function () {
        $('.popup').hide();
        $('.item_popup_menu').removeClass('active');
    }
    var updatePopup = function (id, succCb) {
        succCb();
    }
    $('html').click(function () {
        hidePopups();
    });
    $('.item_popup_menu').click(function (event) {
        event.stopPropagation();
        //event.preventDefault();
        var $this = $(this);
        var popup = $('#' + $this.attr('popup-target')), pos = $this.attr('popup-pos');

        if ($this.hasClass('active'))
            return;

        hidePopups();

        $this.addClass('active');
        if (pos != 'nx') {
            if (pos == 'left') {
                popup.css({'right': 0});
            } else {
                popup.css({'left': 0});
            }
        }
        $this.append(popup);
        updatePopup('', function () {
            popup.fadeIn(200);
        });
    });

    /* Search location */
    $('#popup_searchlocation').each(function () {
        var $this = $(this);
        var wrapper = $('<ul>');
        $this.children('input[type=checkbox]').each(function () {
            var $input = $(this);
            var $label = $this.children('label[for=' + $input.attr('id') + ']');
            console.log($input.attr("class"));
            var $element = $('<li>').attr('input-id', $input.attr('id')).attr('class', $input.attr("class")).html($label.html());

            var checked = $input.is(':checked');

            $input.hide();
            $label.hide();

            if (checked)
                $element.addClass('checked');

            /* Events */
            $input.change(function () {
                var ch = $input.is(':checked');
                if (ch)
                    $(this).addClass('checked');
                else
                    $(this).removeClass('checked');
            });

            $element.click(function () {
                var ch = $(this).hasClass('checked');
                $input.prop('checked', !ch);
                if (ch)
                    $(this).removeClass('checked');
                else
                    $(this).addClass('checked');
                $li = $(this).attr('input-id');
                $('#popup_searchlocation li').each(function () {
                    if ($li == $(this).attr('input-id')) {
                        return;
                    }
                    $input.prop('checked', false);
                    $(this).removeClass('checked');
                });
                if(!$('#popup_searchlocation li.checked').size()){
                    $('#popup_searchlocation li:first').click();
                }
            });

            wrapper.append($element);
        });
        $this.append(wrapper);
    });


    $('#popup_profile a').click(function (event) {
        console.log($(this));
    });

    renderProgressLine();
});