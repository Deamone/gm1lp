function tip(data_) {
    if (data_.parent().hasClass('tip_wrap')) {
        data_.parent().empty().before(data_);
        data_.parent().find('tip_wrap').remove();
    }


    /*if (data_.parent().parent().hasClass('tip_wrap')) {
     console.log(data_.parent().parent().html());
     var q = data_.parent().parent().html();
     data_.parent().parent().empty().before(q);
     }*/
    var data = data_.attr("data-tip");
    var mood = data_.attr("data-mood");
    var pos = data_.attr("data-position");

    if (!data) {
        return false;
    }

    if (data_.is("select")) {
        if (!data_.parent().parent().hasClass('tip_wrap')) {
            data_.parent().wrap("<div class='tip_wrap'></div>");
        } else {
            data_.parent().parent().find('.tooltip').remove();
        }
    } else {
        if (!data_.parent().hasClass('tip_wrap')) {
            data_.wrap("<div class='tip_wrap'></div>");
        } else {
            data_.parent().find('.tooltip').remove();
        }
    }
    var innerjeight = data_.innerHeight();
    var innerwidth = data_.innerWidth();
    var outerheight = data_.outerHeight(true);


    var mTop = data_.css("margin-top");
    var mLeft = data_.css("margin-left");
    var oSimg = data_.position();
    var topposition = parseInt(mTop, 10);
    var leftposition = parseInt(mLeft, 10);

    if (pos == "bottom") {
        topposition = topposition + innerjeight + 8;
        direction = "top";
    } else if (pos == "right") {
        if (data_.hasClass('round_button')) {
            leftposition += 10;
            topposition -= 6;
        }
        leftposition = leftposition + innerwidth + 10;
        direction = "top"; // nx old top
    } else if (pos == "top") {
        topposition = outerheight - parseInt(mTop, 10) + 8;
        direction = "bottom";
    }

    // console.log($(this).parent());

    data_.after("<div class='tooltip " + mood + " " + pos + "' style='" + direction + ": " + topposition + "px; left: " + leftposition + "px;'>" + data + "<div class='triangle'></div></div>");

    var k = data_.next();

    function nt() {
        $(k).hide();
        $(this).unbind('click');
    }

    data_.next().bind('click', nt);
    data_.bind('click', nt);
}
function parseErrors(data, separator, return_) {
    if (data.errors === undefined) {
        return;
    }
    if (separator === undefined) {
        var separator = "\n";
    }
    if (return_ === undefined) {
        return_ = false;
    }
    var errors = '';
    var i = '';
    var text = '';
    var position = 'right';
    if ((typeof data.errors) == 'string') {
        data.errors = {0: data.errors};
    }
    for (i in data.errors) {
        if ($(i).size()) {
            console.log();
            if ((typeof data.errors[i]) === 'object') {
                text = data.errors[i][0];
                position = data.errors[i][1];
            } else {
                text = data.errors[i];
                position = 'right';
            }
            $(i).addClass('tip').attr('data-position', position).attr('data-mood', 'negative').attr('data-tip', text);
            tip($(i));
            $(i).addClass('error').bind('click', function () {
                $(this).removeClass('error');
                /*if ($(this).next().hasClass('tooltip')) {
                 $(this).next().remove();
                 }*/
                var dep = 4;
                var container = $(this);
                while (dep > 0) {
                    if (container.hasClass('tip_wrap')) {
                        console.log(dep, container);
                        container.find('.tooltip').remove();
                        break;
                    }
                    container = container.parent();
                    dep--;
                }
                if ($(this).parent().hasClass('tip_wrap')) {
                    $(this).parent().empty().before($(this));
                    $(this).parent().find('tip_wrap').remove();
                    $(this).focus();
                }
            });
        }
        errors = errors + text + separator;
    }

    return return_ ? errors : null;
}
function parseSuccess(data, separator) {
    if (data.success === undefined) {
        return;
    }
    if (separator === undefined) {
        var separator = "\n";
    }
    var success = '';
    var i = '';
    if ((typeof data.success) == 'string') {
        data.success = {0: data.success};
    }
    for (i in data.success) {
        if ($(i).size()) {
            $(i).addClass('success').bind('click', function () {
                $(this).removeClass('success');
            });
        }
        success = success + data.success[i] + separator;
    }

    return success;
}
function parseEval(data) {
    if (data.eval === undefined) {
        return;
    }
    if ((typeof data.eval) == 'string') {
        data.eval = {0: data.eval};
    }
    var i = '';
    for (i in data.eval) {
        try {
            eval(data.eval[i]);
        } catch (e) {
            console.log('eval fail', e);
        }
    }
}

$(document).on('click', '#newCode', function () {
    $('#aut_login').append('<input type="hidden" name="newCode" val="1" />');
    $('#aut_login').submit();
    //$(this).unbind('click');
    return false;
});
var timeCode = 300;
var timeCode_ = null;
function startNewCode() {
    //timeCode = 300;
    clearInterval(timeCode_);
    timeCode_ = setInterval(function () {
        timeCode--;
        if (timeCode <= 0) {
            $('#textErrCode').html('<a href="#" id="newCode">Выслать новый код</a>');
            clearInterval(timeCode_);
        }
        $('#newCodeTime').text(timeCode);
    }, 1000);
    console.log('start');

}

$(document).ready(function () {
    $(document).on('submit', '#reg_form', function () {
        $.post('?', $(this).serialize(), function (data) {
            var errors = parseErrors(data);
            var success = parseSuccess(data);
            var eval = parseEval(data);
        }, 'json');
        return false;
    });
    $(document).on('submit', '#aut_form', function () {
        $.post('?', $(this).serialize(), function (data) {
            var errors = parseErrors(data);
            var success = parseSuccess(data);
            var eval = parseEval(data);
        }, 'json');
        return false;
    });
    $(document).on('submit', '#aut_form2', function () {
        $.post('?', $(this).serialize(), function (data) {
            var errors = parseErrors(data);
            var success = parseSuccess(data);
            var eval = parseEval(data);
        }, 'json');
        return false;
    });
    $(document).on('submit', '#forgot_form', function () {
        $.post('?', $(this).serialize(), function (data) {
            var errors = parseErrors(data);
            var success = parseSuccess(data);
            var eval = parseEval(data);
        }, 'json');
        return false;
    });
});