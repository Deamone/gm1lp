/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var KEY = window.KEY = {
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    DEL: 8,
    TAB: 9,
    RETURN: 13,
    ENTER: 13,
    ESC: 27,
    PAGEUP: 33,
    PAGEDOWN: 34,
    SPACE: 32,
    SHIFT: 16,
    CTRL: 17,
    BACKSPACE: 8
};

$.fn.center = function (onlyLeft, onlyTop) {
    if (onlyLeft == undefined) {
        onlyLeft = true;
    }
    if (onlyTop == undefined) {
        onlyTop = true;
    }
    this.css("position", "absolute");
    if (onlyTop) {
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
        $(window).scrollTop()) + "px");
    }
    if (onlyLeft) {
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
        $(window).scrollLeft()) + "px");
    }
    return this;
};
cookies = {
    cookies: null,
    set: function (name, value, days) {
        if (!this.cookies)
            this.init();
        this.cookies[name] = value;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else
            var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    get: function (name) {
        if (!this.cookies)
            this.init();
        return this.cookies[name];
    },
    init: function () {
        this.cookies = {};
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].split("=");
            if (c.length == 2)
                this.cookies[c[0].match(/^[\s]*([^\s]+?)$/i)[1]] = c[1].match(/^[\s]*([^\s]+?)$/i)[1];
        }
    }
};

if (newMessages === undefined) {
    var newMessages = 0;
}
if (miniDialog === undefined) {
    var miniDialog = false;
}
if (current_dialog_id === undefined) {
    var current_dialog_id = 0;
}
function ajax_mes_mini() {
    var peer_id = $('.mini-dialog').attr('data-id');
    $.post('/message?get&id=' + peer_id, '', function (data) {
        console.log(data);
        for (var i = 0; i < data.length; i++) {
            var date_ = date_js(data[i].timestamp, 1);
            if ($('div[data-id="' + data[i].user_id + '_' + data[i].local_id + '"]').size()) {
                var li = $('li[data-id="' + data[i].user_id + '_' + data[i].local_id + '"]');
                if (li.attr('data-flag') != data[i].flags) {
                    li.attr('data-flag', data[i].flags);
                    console.log('replace!');
                }
            }
            if (!$('div[data-id="' + data[i].user_id + '_' + data[i].local_id + '"]').size()) {
                newMessages++;
                var iSend = false;
                if (data[i].user_id == user.uid && data[i].flags == 2) {
                    data[i].flags = 3;
                }
                if (data[i].user_id == user.uid) {
                    iSend = true;
                }
                if (!miniDialog) {
                    $('#messages').append('<div class="blok_mess" data-flag="' + data[i].flags + '" data-timestamp="' + data[i].timestamp + '" data-id="' + data[i].user_id + '_' + data[i].local_id + '"><a href="/id' + data[i].user_id + '"><img class="rounded_3" src="' + data[i].ava + '" /></a><a href="/id' + data[i].user_id + '">' + data[i].name + '</a><p class="mess_data">' + date_ + '</p><p class="mess_p"><span class="realText">' + data[i].message + '</span></p></div>');
                } else {
                    if (data[i].files) {
                        var files = data[i].files.split(',');
                        var html = '';
                        $.each(files, function (i, src) {
                            html += '<a class="fancybox" href="/uploads/messages/' + src + '" rel="messages"><img src="/uploads/messages/' + src + '" /></a>';
                        });
                        data[i].message = html + '<span>' + data[i].message + '</span>';
                    }
                    if (iSend) {
                        $('#messages').append('<div class="messageI" data-flag="' + data[i].flags + '" data-timestamp="' + data[i].timestamp + '" data-id="' + data[i].user_id + '_' + data[i].local_id + '"><div class="text rounded_3">' + data[i].message + '</div><div class="time">' + date_ + '</div><div class="clear"></div></div>');
                        clickable($('.messageI[data-timestamp="' + data[i].timestamp + '"] .text .realText'));
                    } else {/*
                     var audio = document.getElementById("audioNewMessage");
                     audio.play();*/
                        $('#messages').append('<div class="messageYou" data-flag="' + data[i].flags + '" data-timestamp="' + data[i].timestamp + '" data-id="' + data[i].user_id + '_' + data[i].local_id + '"><div class="avatar"><img src="' + data[i].ava + '" class="width20 rounded_50p"></div><div class="text rounded_3"><span class="realText">' + data[i].message + '</span>' + data[i].attachments + '</div><div class="time">' + date_ + '</div><div class="clear"></div></div>');
                        clickable($('.messageYou[data-timestamp="' + data[i].timestamp + '"] .text .realText'));
                    }
                    $('*[data-video]').each(function () {
                        var qthis = $(this);
                        setTimeout(preview_video(qthis), 100);
                    });
                    $('#messages>div img').each(function () {
                        $(this).load(function () {
                            $('#messages').scrollTop($('#messages')[0].scrollHeight);
                        });
                    });
                    $('#messages').scrollTop($('#messages')[0].scrollHeight);

                }
            }
            $('#messages').scrollTop($('#messages')[0].scrollHeight);
        }
    }, 'json');
}
(function (global, name, def) {
    if (typeof (define) == 'function' && define['amd']) define(name, def);
    else global[name] = def();
})(this, 'loadImage', function () {
    return function (imageSrc, done) {
        var q = new Promise(function (res, rej) {
            var img = new Image();
            img.src = imageSrc;
            img.onerror = function (e) {
                rej(e);
            };
            img.onload = function (e) {
                res(img);
            };
            if (img.complete) res(img);
        });
        return q;
    }
});
function preview_video(qthis2) {
    var qw = qthis2;
    var url = qw.attr('data-video');
    if (url.indexOf('youtube.ru/') === -1 && url.indexOf('youtube.com/') === -1 &&
        url.indexOf('youtu.be/') === -1) {
        console.log('unsupported video: ' + url);
        qw.remove();
    } else {
        url = url.replace('www.', '');
        url = url.replace('https://', 'http://');
        url = url.replace(/http?:\/\/(?:youtube(?:-nocookie)?\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*["?&]v=)|youtu\.be\/)([^&?\/ ]{11})/i, "$1");

        var a = qw.find('a');
        if (!a.html()) {
            a = qw.find('div');
        }


        var href = 'https://img.youtube.com/vi/' + url + '/sddefault.jpg',
            imgWidth,
            img = new Image();

        img.onload = function () {
            if (img.width == 120) {
                if (a.html()) {
                    a.before('<a class="preview_video" href="' + qw.attr('data-video') + '" target="_blank" style="background: url(https://img.youtube.com/vi/' + url + '/mqdefault.jpg) no-repeat;/*background-position: 1px -35px;*/"></a>');

                } else {
                    qw.html('<a class="preview_video" href="' + qw
                        .attr('data-video') + '" target="_blank" style="background: url(https://img.youtube.com/vi/' + url + '/mqdefault.jpg) no-repeat;/*background-position: 1px -35px;*/"></a>');
                }
            } else {
                if (a.html()) {
                    a.before('<a class="preview_video" href="' + qw.attr('data-video') + '" target="_blank" style="background: url(https://img.youtube.com/vi/' + url + '/sddefault.jpg) no-repeat;background-position: 1px -35px;"></a>');
                    console.log('before');

                } else {
                    qw.html('<a class="preview_video" href="' + qw.attr('data-video') + '" target="_blank" style="background: url(https://img.youtube.com/vi/' + url + '/sddefault.jpg) no-repeat;background-position: 1px -35px;"></a>');
                    console.log(qw.attr('data-video'));
                }
            } //console.log(a,$this);
        };
        img.src = href;

        /*<iframe style="display: none;" width="' + $(this).width() + '" height="' + $(this).height() + '" src="https://www.youtube.com/embed/' + url + '?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>*/
    }
}
$(document).load(function () {
    console.log('go');
});
window.kemojiComment = null;


function listLike(wall_id, likes, notAva) {
    if (likes === undefined && !notAva) {
        $.post('?like=get&wid=' + wall_id, '', function (data) {
            console.log(data);
            $('.wall_post[data-wid="' + wall_id + '"] .wall_icon_counter_y, .wall_post[data-wid="' + wall_id + '"] .wall_icon_counter').text(Object.keys(data).length);
            var list = '';
            if (!notAva) {
                $.each(data, function (i, item) {
                    if (i > 3) {
                        return;
                    }
                    list = list + '<li><a href="/id' + item[0] + '"><img class="width20" src="/uploads/avatars/' + item[1] + '" /></a></li>';
                });
            }
            $('.wall_post[data-wid="' + wall_id + '"] .wall_icon_counter_y, .wall_post[data-wid="' + wall_id + '"] .wall_icon_counter').next().html(list);
        }, 'json');
    } else {
        $.post('?like=get&cid=' + wall_id, '', function (data) {
            console.log(data);
            $('.wall_post_comments .wall_post[data-cid="' + wall_id + '"] .wall_icon_counter_y, .wall_post_comments .wall_post[data-cid="' + wall_id + '"] .wall_icon_counter').text(Object.keys(data).length);
        }, 'json');
    }
}
$(document).ready(function () {

    $(document).on('click', '.wall_wrapper .wall_icon_counter,.wall_wrapper .wall_icon_counter_y', function () {
        console.log('like');
        var isLike = false;
        if ($(this).hasClass('wall_icon_counter_y')) {
            $(this).addClass('wall_icon_counter');
            $(this).removeClass('wall_icon_counter_y');
            isLike = true;
        } else {
            $(this).addClass('wall_icon_counter_y');
            $(this).removeClass('wall_icon_counter');
        }
        if ($(this).hasClass('lcomment')) {
            var id = $(this).attr('data-cid');
            var wid = $(this).attr('data-wid');
            console.log(id);
            if ($(this).hasClass('photo_like')) {
                if (!isLike) {
                    $(this).text(parseInt($(this).text()) + 1);
                } else {
                    $(this).text(parseInt($(this).text()) - 1);
                }
                $.post('/album/album-ajax', 'photo_id=' + $(this).attr('data-pid') + '&commentLike=' + $(this).attr('data-cid'), function () {

                });
                return false;
            } else {
                $.post("/", "add_like=1&id=" + id + '&comment=2', function (data, textStatus) {
                    listLike(id, undefined, true, wid);
                });
            }
            return false;
        }
        var id = $(this).parent().parent().parent().parent().attr('data-wid');
        console.log(id);
        $.post("/", "add_like=1&id=" + id, function (data, textStatus) {
            listLike(id, undefined, false);
        });

        return false;
    });

    $(document).on('click', '.showAll', function () {
        $('.photo-comment').each(function () {
            $(this).show();
        });
        $(this).remove();
    });

    doInitCommentEmoji();

    if ($('#myEmojiField').size()) {
        window.kemoji1 = KEmoji.init('myEmojiField', {});
        $(document).on('keyup', '#myEmojiField .KEmoji_Input div[contenteditable="true"]', function () {
            console.log($(this).parent().parent().prev());
            $(this).parent().parent().prev().val($(this).html());
        });
        $(document).on('click', '#myEmojiField .KEmoji_Input div[contenteditable="true"]', function () {
            $('#myEmojiField').addClass('h146');
            //$(this).addClass('h140');
        });
    }
    if ($('#myEmojiField2').size()) {
        window.kemojiMessage = KEmoji.init('myEmojiField2', {height: '68px'});
        $('#myEmojiField2 .KEmoji_Input div[contenteditable="true"]').addClass('h68');
        $(document).on('keyup', '#myEmojiField2 .KEmoji_Input div[contenteditable="true"]', function () {
            console.log($(this).parent().parent().prev());
            $(this).parent().parent().prev().val($(this).html());
        });
        $(document).on('click', '#myEmojiField2 .KEmoji_Input div[contenteditable="true"]', function () {
            $('#myEmojiField2').addClass('h68');
            $(this).addClass('h68');
        });
    }
    if ($('#myEmojiField3').size()) {
        window.kemoji = KEmoji.init('myEmojiField3', {height: '54px'});
        $('#myEmojiField3 .KEmoji_Input div[contenteditable="true"]').addClass('h54');
        $(document).on('keyup', '#myEmojiField3 .KEmoji_Input div[contenteditable="true"]', function () {
            console.log($(this).parent().parent().prev());
            $(this).parent().parent().prev().val($(this).html());
        });
        $(document).on('click', '#myEmojiField3 .KEmoji_Input div[contenteditable="true"]', function () {
            $('#myEmojiField3').addClass('h54');
            $(this).addClass('h54');
        });
    }
    $(document).on('click', '.photo-write button', function () {
        photo_ = $(this).parent().parent().parent();

        //$kemoji = $wall.find('div[emoji-id]').attr('emoji-id');
        kemoji_ = window.kemojiComment;
        photo_.find('textarea[name=text]').val(kemoji_.getValue(KEmoji.HTML_VALUE));

        if (!$.trim(photo_.find('form textarea[name=text]').val())) {
            return false;
        }
        $.post('/album/album-ajax', photo_.find('form').serialize() + '&add_comment=1', function (data) {
            //$('.wallComment input').val('');
            photo_.find('textarea').val('');
            photo_.find('.KEmoji_Input div[contenteditable]').html('');
            //$photo.find('.KEmoji_Input div[contenteditable]').html('');
            //$('.wall_wrapper .wall_post_comment:first').after(data);
            //var wid = $photo.find('input').val();
            $('.photo-comments').append(data);
            //clickable($('.wall_post[data-wid=' + wid + '] .wall_post_comments .wall_post_content:first .wall_post_message p'));
        });
        return false;
    });
    $(document).on('error', 'img', function (e) {
        console.log('wwwww');
    });

    $('*[data-video]').each(function () {
        var qthis = $(this);
        setTimeout(preview_video(qthis), 100);
    });


    $('.video .delete').click(function () {
        $.post('', 'delete=' + $(this).attr('data-vid'), function () {

        });
        $(this).parent().remove();

        return false;
    });

    $(document).on('click', '.delComment', function () {
        $post = $(this).parent().parent().parent().parent();
        $.post("/?del_comment=1", "pid=" + $(this).attr('data-post_id') + '&cid=' + $(this).attr('data-post_cid'), function (data, textStatus) {
            $post.remove();
        });
        return false;
    });
    $(document).on('click', '.hideHomePhoto', function () {
        $photo = $(this).parent().parent();
        $.post('', 'hide_photo=1&pid=' + $(this).attr('data-photo-id'), function () {
            $photo.remove();
        });
        return false;
    });


    $('.KEmoji_Input div[contenteditable]').off('keydown')
        .on('keydown', function (event) {
            if (event.which != 13 || event.ctrlKey || event.shiftKey || event.altKey) {
                return;
            }
            if (event.which == window.KEY.ENTER) {
                $('.mini-dialog .send').click();
                return;
            }

        });
    $('.mini-dialog .send').on('click', function () {
        var peer_id = $('.mini-dialog').attr('data-id');

        $('.mini-dialog textarea').val(kemoji.getValue(KEmoji.HTML_VALUE));
        if (!$.trim(($('.mini-dialog textarea').val().replace('&nbsp;', ''))) && !$('.mphotos div').size()) {
            return false;
        }
        console.log($.trim(($('.mini-dialog textarea').val())));
        $.post('/message?id=' + peer_id, $('.mini-dialog form').serialize(), function (data) {
            $('.mini-dialog textarea').val('');
            $('.KEmoji_Input div[contenteditable]').html('');

            $('.mphotos a').each(function () {
                $(this).click();
            });
            console.log(data);
            im();
        });
        return false;
    });
    $('.mini-dialog .close').on('click', function () {
        $('.mini-dialog').hide();
        return false;
    });
    $('#popup_messages').on('click', '.sendMessageMiniDialog', function () {

        $('.mini-dialog').show();
        var user_id = $(this).attr('data-id');
        $('.mini-dialog .mheader span').html('<a href="/?id=' + user_id + '">' + $(this).find('.title a').text() + '</a>');

        current_dialog_id = user_id;
        $('.mini-dialog').attr('data-id', user_id);
        $('.mini-dialog .set-tooltip a:first').attr('href', '/message?id=' + user_id);
        $('.mini-dialog .set-tooltip a#clearDialog').attr('data-id', user_id);
        $('.mini-dialog textarea').focus();
        $('#myEmojiField3 .KEmoji_Input div[contenteditable]').focus();
        ajax_mes_mini();
        return false;
    });
    $(document).on('click', '#sendMessage', function () {

        $('.mini-dialog').show();
        $('.mini-dialog .mheader span').text($('.sinfo_name').text());
        var user_id = $(this).attr('data-id');
        $('.mini-dialog .mheader span').html('<a href="/?id=' + user_id + '">' + $('.sinfo_name').text() + '</a>');
        current_dialog_id = user_id;
        $('.mini-dialog').attr('data-id', user_id);
        $('.mini-dialog .set-tooltip a:first').attr('href', '/message?id=' + user_id);
        $('.mini-dialog .set-tooltip a#clearDialog').attr('data-id', user_id);
        $('.mini-dialog textarea').focus();
        $('#myEmojiField3 .KEmoji_Input div[contenteditable]').focus();
        ajax_mes_mini();
        return false;
    });
    $('#showAllInfo').click(function () {
        $('#shortAbout.noopen').addClass('open');
        $('#shortAbout.noopen').find('tr').show();
        $('#shortAbout.noopen').removeClass('noopen');
        $('#showAllInfo').remove();

        return false;
    });
    $('#showAllSkills').click(function () {
        $('.skills_table').find('tr').show();
        $('#showAllSkills').parent().remove();
        renderProgressLine();
        return false;
    });
    $('.popup.popup_searchlocation *').click(function () {
        return false;
    });
    $('.addToContact2').on('click', function () {
        var id = $(this).attr('data-id');
        var vthis = $(this);
        $.post('/contacts?act=add', 'id=' + id, function (data) {
            if (data == 1) {
                vthis.removeClass('addToContact');
                vthis.attr('href', '#');
                vthis.text('Заявка принята');
            }
            if (data == 2) {
                vthis.removeClass('addToContact');
                vthis.attr('href', '#');
                vthis.text('Заявка отправлена');
            }
            vthis.unbind('click');
        });

        return false;
    });
    function removeContact() {
        var id = $(this).attr('data-id');
        var vthis = $(this);
        $.post('/contacts?act=remove', 'id=' + id, function (data) {
            vthis.removeClass('unContact');
            vthis.attr('href', '#');
            vthis.attr('data-status', 3);
            vthis.text('Заявка отклонена');
            vthis.unbind('click');
            resetContact();
        });

        return false;

    }

    function addContact() {
        var id = $(this).attr('data-id');
        var vthis = $(this);
        $.post('/contacts?act=add', 'id=' + id, function (data) {
            /***
             * 1 = Заяка принята
             * 2 = Заявка отправлена
             */
            if (data == 1) {
                vthis.removeClass('addToContact');
                vthis.attr('href', '#');
                vthis.text('Заявка принята');
                vthis.attr('data-status', 1);
            }
            if (data == 2) {
                vthis.removeClass('addToContact');
                vthis.attr('href', '#');
                vthis.text('Заявка отправлена');
                vthis.attr('data-status', 0);
            }
            resetContact();
            vthis.unbind('click');
        });

        return false;
    }

    $('.unContact').on('click', removeContact);
    ////////////////////////

    $('.addToContact').on('click', addContact);
    function resetContact() {
        $('.icontact').each(function () {
            var status = $(this).attr('data-status');
            if (status === undefined) {
                return;
            }
            $this = $(this);
            $(this).unbind('mouseenter').unbind('mouseleave');
            switch (parseInt(status)) {
                case 1:
                    $(this).text('Контакт установлен');
                    $(this).hover(function () {
                        $(this).unbind('click');
                        $(this).bind('click', removeContact);
                        $(this).text('Удалить контакт');
                    }, function () {
                        $(this).unbind('click');
                        $(this).text('Контакт установлен');
                    });
                    break;
                case 0:
                    $(this).text('Отменить заявку');
                    $(this).hover(function () {
                        $(this).unbind('click');
                        $(this).bind('click', removeContact);
                        $(this).text('Отменить заявку');
                    }, function () {
                        $(this).unbind('click');
                        $(this).text('Отменить заявку');
                    });
                    break;
                case -1:
                    $(this).text('Установить контакт');
                    $(this).unbind('click');
                    $(this).bind('click', addContact);
                    break;
                case 3:
                    $(this).text('Заявка отклонена');
                    $(this).unbind('click');
                    $(this).hover(function () {
                        $this.text('Заявка отклонена');
                    }, function () {
                        $this.text('Заявка отклонена');
                    });
                    break;
            }
        });
    }

    resetContact();
    $('.openDialog').on('click', function () {
        var id = $(this).attr('data-id');
        if (id === undefined) {
            return;
        }
        window.location.href = '/message?id=' + id;

        return false;
    });
    resetTime();
    $(document).on('click', '.skill_profile_btn a', function () {
        var id = $(this).attr('data-id');
        var uid = $(this).attr('data-uid');
        $.post('/id' + uid + '?likeSkill', 'id=' + id, function (data) {
            $('.skills_table tr[data-id="' + id + '"]')[0].outerHTML = data;
            renderProgressLine();
        });

        return false;
    });
    $(document).on('click', '.mheader .set, .mess_nav .a_act', function () {
        if ($(this).hasClass('open')) {
            return false;
        }
        $('.set-tooltip').show();
        $(this).addClass('open');
        return false;
    });
    $(document).on('click', '.mheader .set.open, .mess_nav .a_act.open', function () {
        $('.set-tooltip').hide();
        $(this).removeClass('open');
        return false;
    });
    $(document).on('click', 'a#clearDialog', function () {
        var user_id = $(this).attr('data-id');
        if (miniDialog === undefined || !miniDialog) {
            console.log('clear from dialogs');
            var id = $('#messages div[data-id^="' + user_id + '_"]').attr('data-id');
            $.post('/message?id=' + user_id + '&clear_messages', '', function () {
            });
            $('#messages').html('');
            console.log(id);
        } else {
            console.log('clear from min-dialog');
            var id = $('#messages div[data-id^="' + user_id + '_"]').attr('data-id');
            $.post('/message?id=' + user_id + '&clear_messages', '', function () {
            });
            $('#messages').html('');
            console.log(id);
        }
        return false;
    });
    $(document).on('click', 'a.deleteMessage', function () {
        var user_id = $(this).attr('data-id');
        var id = $(this).attr('data-mid');
        $.post('/message?id=' + user_id + '&del_message', 'id=' + id, function () {
        });
        $(this).parent().parent().remove();

        return false;
    });
    $(document).on('click', 'a.clearDialog', function () {
        var user_id = $(this).attr('data-id');
        $.post('/message?id=' + user_id + '&clear_messages', function () {
        });
        $(this).parent().remove();

        return false;
    });
    $(document).on('click', '.wall_text a[href="#"]', function () {
        $(this).next().show();
        $(this).prev().remove();
        $(this).remove();
        return false;
    });

    if (!user.uid) {
        $('input,textarea').attr('disabled', 'disabled');
        $('.cont').hide();
        $('form').unbind('submit');
        $('.popup_profile .popup_row').remove();
        $('.popup_profile').append('<div class="popup_row header" style="text-align: center;font-size: 20px;"><a href="/page/login">Присоединиться</a></div>');
        $('form').bind('submit', function () {
            return false;
        });
    }
    $('.wall_post[data-wid]').each(function () {
        console.log($(this).find('.wall_text span').size());
        if ($(this).find('.wall_text span').size() == 2) {
            clickable($(this).find('.wall_text span:last'));
        } else {
            clickable($(this).find('.wall_text'));
        }
    });
    $('.wall_post[data-wid] .wall_post_comments .wall_post_content').each(function () {
        clickable($(this).find('.wall_post_message p'));
    });
    $('.settAdmin').click(function () {
        $(this).next().fadeToggle();
        return false;
    });
    $('.showAllRadio').click(function () {
        $('.radioHide').fadeToggle();
        return false;
    });
    $(document).on('click', '.all-search .groupJoin,.all-search .groupUnJoin', function () {
        $.post($(this).attr('href'), '', function () {

        });
        if ($(this).hasClass('groupJoin')) {
            $(this).removeClass('groupJoin').addClass('groupUnJoin').text('Покинуть');
        } else {
            $(this).removeClass('groupUnJoin').addClass('groupJoin').text('Вступить');
        }
        return false;
    });
    $(document).on('keyup', 'input[name="url"]', function () {
        $val = $(this).val();
        if ($val == $(this).attr('data-url')) {
            $(this).next().removeClass('fail loading').show();
            return false;
        }
        if ($val.length < 5) {
            $(this).next().removeClass('loading').addClass('fail').show();
        } else {
            $this = $(this);
            $this.next().addClass('loading').show();
            if ($this.data('data-group-id')) {
                var data = {
                    'url': $val,
                    'group_id': $this.data('data-group-id')
                };
            } else {
                var data = {
                    'url': $val
                };
            }
            $.post('/util/url', data, function (data) {
                if (data == 1) {
                    $this.next().removeClass('fail loading').show();
                } else {
                    $this.next().removeClass('loading').addClass('fail').show();
                }
            });
        }
    });
    $(document).on('click', '.forSelect .modal-body .gallery a', function () {
        var id = $(this).parent().attr('data-id');
        addHideData('video', id);
        hideModal('.forSelect');
        var img = $(this).css('background-image');
        img = img.replace('sddefault', 'mqdefault');
        img = img.replace(/\"/g, "'");
        $('#uploadList').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();removeHideData(\'video\', ' + id + ');return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
        + '<a href="#" onlick="return false;"><div class="rounded_3 width71 videoPreview" style="background-image:' + img + '" /></div>'
        + '<input type="hidden" name="blank" /></li>');
        return false;
    });
    $(document).on('click', '#uploadWallVideo', function () {
        var id = $(this).data('id');
        if (!id) {
            loadVideoInBlockSelect(user.uid);
        } else {
            loadVideoInBlockSelect(id);
        }
        showModal('.forSelect', function () {
            hideModal('.forSelect');
        });
        return false;
    });

    $(document).on('click', '#uploadWallDoc', function () {
        if ($('#uploadList li').size() >= 5) {
            return false;
        }
        $('#fileuploadForWall').fileupload({
            url: '/?uploadWallDoc',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.error === undefined) {
                    //curImage.attr('img-src', data.result.file);
                    //curImage.click();
                    console.log(data.result);
                    $('#uploadList').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();removeHideData(\'doc\', ' + data.result.doc + ');return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
                    + '<a href="#" onlick="return false;" class="doc">' + (data.result.image ? '<img class="rounded_3 width71" src="/uploads/docs/' + user.uid + '/' + data.result.file + '" />' : data.result.file) + '</a>'
                    + '<input type="hidden"/></li>');
                    addHideData('doc', data.result.doc);
                } else {
                    alert(data.result.error);
                }
                console.log(data);

            },
	    fail: function (e,data){
	      //data = $.parseJSON(data);
	      alert(data.jqXHR.responseJSON.error);
	      //console.log(e);
	     // alert(data.error);
	    }
            ,
            progressall: function (e, data) {

            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

        $('#fileuploadForWall').click();

        return false;
    });
    $(document).on('click', '#uploadWall', function () {
        if ($('#uploadList li').size() >= 10) {
            return false;
        }
        $('#fileuploadForWall').fileupload({
            url: '/?uploadWall&uid=' + user.uid,
            dataType: 'json',
            done: function (e, data) {
				console.log(data);
				
				$.each(data.result.files, function (index, file) {
					if (file.error === undefined) {
						//curImage.attr('img-src', data.result.file);
						//curImage.click();
						console.log(file);
						$('#uploadList').append('<li><div class="pmain_toolbox"><a href="#" onclick="$(this).parent().parent().remove();return false;"><img src="/images/icon-toolbox-close.png"></a></div>'
						+ '<a href="#" onlick="return false;"><img class="rounded_3 width71" src="/uploads/wall/' + user.uid + '/' + file.file + '" /></a>'
						+ '<input type="hidden" name="file[]" value="/uploads/wall/' + user.uid + '/' + file.file + '" /></li>');
					} else {
						alert(file.error);
					}
				});

            }
            ,
            progressall: function (e, data) {

            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

        $('#fileuploadForWall').click();

        return false;
    });

    $(document).on('click', '.delWall', function () {
        $post = $(this).parent().parent().parent().parent();
        $.post("", "del_post=1&id=" + $(this).attr('data-post_id'), function (data, textStatus) {
            $post.remove();
        });
        return false;
    });
    $(document).on('click', '.editWall', function () {
        $post = $(this).parent().parent().parent().parent();
        var id = $(this).attr('data-post_id');
        console.log('Hello', $post);
        $post.find('.wall_post_toolbox_bottom').hide();
        $post.find('.wall_post_comment').hide();
        var text = $post.find('.wall_text').html();
        var files = '';
        var hide = '';
        $.post('/?editPost', 'id=' + id, function (data) {
            text = data.text;
            files = data.files;
            hide = data.hide;
        }, 'json');
        $post.find('.wall_post_message .wall_text').html('<form method="POST" class="wallEdit"><textarea class="rounded_3" rows="1" placeholder="Введите текст" name="message"></textarea><div class="myEmojiFieldEdit"></div><input type="hidden" class="hide_data" name="hide_data" value=""></form>');
        window.kemoji6 = KEmoji.initByClass('myEmojiFieldEdit', {});

        kemoji6[0].setValue(text, KEmoji.HTML_VALUE);


        return false;
    });
    $(document).on('click', '.addMe', function () {
        var id = $(this).data('vid');
        $.post('/?copy_video', 'id=' + id, function () {
            showInfo('Успешно!', 'Видеозапись успешно добавлена в ваши видеозаписи');
        });
        return false;
    });
    updateNoticeMessage();
    renderProgressLine();
});
window.kemoji5;
function doInitCommentEmoji() {
    if (!$('.myEmojiFieldComment').size()) {
        return;
    }
    $('.myEmojiFieldComment').each(function () {
        if ($(this).hasClass('KEmoji_Block')) {
            return;
        }
        window.kemoji5 = KEmoji.initByClass('myEmojiFieldComment', {});
        $('.myEmojiFieldComment').click(function () {
            $(this).addClass('h86');
        });
        $(this).on('keyup', '.KEmoji_Input div[contenteditable="true"]', function () {
            $(this).parent().parent().prev().val($(this).html());
        });

    });
}
function removeHideData(atr, id) {
    var val = $('.hide_data').val();
    val = val.replace('[' + atr + '=' + id + ']');
    $('.hide_data').val(val);
}
function addHideData(atr, id) {
    var val = $('.hide_data').val();
    val = val.replace('[' + atr + '=' + id + ']');
    $('.hide_data').val(val + '[' + atr + '=' + id + ']');
}
function loadVideoInBlockSelect(id) {
    $.post('/video/ajax', 'id=' + id, function (data) {
        $('.forSelect .modal-body').html('');
        console.log(data);
        $.each(data, function (i, video) {
            var q = $('.forSelect .modal-body').append('<div class="gallery rounded_2"><div data-id="' + video.video_id + '" class="gallery-image" data-video="' + video.url + '"><a class="preview_video" href="' + video.url + '"></a></div><div class="gallery-details"><a href="' + video.url + '" target="_blank" class="link small">' + video.title + '</a></div></div>');
            setTimeout(preview_video($('.forSelect .modal-body .gallery:last .gallery-image')), 100);

        });
    }, 'json');
    return;
}
function updateNoticeMessage() {
    $counter = $('.tb_messages .tb_amount');
    //$counter.hide();
    $.ajax({
        xhrFields: {
            withCredentials: true
        },
        type: "POST",
        dataType: 'json',
        url: "https://gm1lp.ru/messages?notice"
    }).done(function (data) {
        if (data.counter && data.counter != parseInt($counter.text())) {
            $counter.text(data.counter).show();
        }
        if (data.counter = 0) {
            $counter.text(data.counter).hide();
        }
        try {
            var length = Math.min(data.notice.length, 7);
            for (var i = 0; i < length; i++) {
                var notice = data.notice[i];
                var text = notice.message;
                var tmp = text.replace(/<.*?>/g, '');
                if (tmp.length > 121) {
                    text = text.substring(0, 121) + '...';
                }
                if ($('#popup_messages div[data-id="' + notice.id + '"]').size()) {
                    var time = parseInt($('#popup_messages div[data-id="' + notice.id + '"]').attr('data-time'));
                    var flag = parseInt($('#popup_messages div[data-id="' + notice.id + '"]').attr('data-flag'));
                    if (notice.time > time || notice.flags != flag) {
                        $('#popup_messages div[data-id="' + notice.id + '"]').remove();
                    } else {
                        continue;
                    }
                }
                $('#popup_messages').append('<div data-id="' + notice.id + '" data-flag="' + notice.flags + '" data-time="' + notice.time + '" class="popup_row sendMessageMiniDialog' + (i + 1 == length ? ' last ' : '') + (/*notice.flags == 6 || */notice.flags == 2 ? ' hlighted' : '') + '"><div class="popup_col_img"><a href="/message?id=' + notice.id + '"><img class="rounded_3" src="' + notice.ava + '" width="59" height="59"></a></div><div class="popup_col_text"> <span class="title"><a href="/id' + notice.id + '">' + notice.name + '</a></span><br><span class="text">' + text + '</span><br><span class="date" data-s_time="' + notice.time + '"></span> </div></div>');
            }
        } catch (e) {
        }
    });
}
function showInfo(header, text) {
    $("#alertHeader").text(header);
    $("#alertMessage").text(text);
    showModal(".alertMessage");

    return false;
}
setInterval(updateNoticeMessage, 5000);
function resetTime() {
    $.each($('*[data-s_time]'), function () {
        var time = $(this).attr('data-s_time');
        $(this).text(dateFormat(time));
    });
}
function startCheckAuth() {
    $('#auth').change(function () {
        if ($(this).val() == 1) {
            $('#auth_block').show();
        } else {
            $('#auth_block').hide();
        }
    });
    $('#auth').change();
}
function tip(data_) {
    if (data_.parent().hasClass('tip_wrap')) {
        data_.parent().empty().before(data_);
        data_.parent().find('tip_wrap').remove();
    }


    /*if (data_.parent().parent().hasClass('tip_wrap')) {
     console.log(data_.parent().parent().html());
     var q = data_.parent().parent().html();
     data_.parent().parent().empty().before(q);
     }*/
    var data = data_.attr("data-tip");
    var mood = data_.attr("data-mood");
    var pos = data_.attr("data-position");

    if (!data) {
        return false;
    }

    if (data_.is("select")) {
        if (!data_.parent().parent().hasClass('tip_wrap')) {
            data_.parent().wrap("<div class='tip_wrap'></div>");
        } else {
            data_.parent().parent().find('.tooltip').remove();
        }
    } else {
        if (!data_.parent().hasClass('tip_wrap')) {
            data_.wrap("<div class='tip_wrap'></div>");
        } else {
            data_.parent().find('.tooltip').remove();
        }
    }
    var innerjeight = data_.innerHeight();
    var innerwidth = data_.innerWidth();
    var outerheight = data_.outerHeight(true);


    var mTop = data_.css("margin-top");
    var mLeft = data_.css("margin-left");
    var oSimg = data_.position();
    var topposition = parseInt(mTop, 10);
    var leftposition = parseInt(mLeft, 10);

    if (pos == "bottom") {
        topposition = topposition + innerjeight + 8;
        direction = "top";
    } else if (pos == "right") {
        if (data_.hasClass('round_button')) {
            leftposition += 10;
            topposition -= 6;
        }
        leftposition = leftposition + innerwidth + 10;
        direction = "top"; // nx old top
    } else if (pos == "top") {
        topposition = outerheight - parseInt(mTop, 10) + 8;
        direction = "bottom";
    }

    // console.log($(this).parent());

    data_.after("<div class='tooltip " + mood + " " + pos + "' style='" + direction + ": " + topposition + "px; left: " + leftposition + "px;'>" + data + "<div class='triangle'></div></div>");

    var k = data_.next();

    function nt() {
        $(k).hide();
        $(this).unbind('click');
    }

    data_.next().bind('click', nt);
    data_.bind('click', nt);
}
setInterval(resetTime, 100);
/**
 * @author ximik777
 * @param {type} datetime
 * @param {type} show_sec
 * @returns {String|Boolean}
 */
var lang = {
    "months": ["\u042f\u043d\u0432\u0430\u0440\u044c", "\u0424\u0435\u0432\u0440\u0430\u043b\u044c", "\u041c\u0430\u0440\u0442", "\u0410\u043f\u0440\u0435\u043b\u044c", "\u041c\u0430\u0439", "\u0418\u044e\u043d\u044c", "\u0418\u044e\u043b\u044c", "\u0410\u0432\u0433\u0443\u0441\u0442", "\u0421\u0435\u043d\u0442\u044f\u0431\u0440\u044c", "\u041e\u043a\u0442\u044f\u0431\u0440\u044c", "\u041d\u043e\u044f\u0431\u0440\u044c", "\u0414\u0435\u043a\u0430\u0431\u0440\u044c"],
    "months_of": ["\u042f\u043d\u0432\u0430\u0440\u044f", "\u0424\u0435\u0432\u0440\u0430\u043b\u044f", "\u041c\u0430\u0440\u0442\u0430", "\u0410\u043f\u0440\u0435\u043b\u044f", "\u041c\u0430\u044f", "\u0418\u044e\u043d\u044f", "\u0418\u044e\u043b\u044f", "\u0410\u0432\u0433\u0443\u0441\u0442\u0430", "\u0421\u0435\u043d\u0442\u044f\u0431\u0440\u044f", "\u041e\u043a\u0442\u044f\u0431\u0440\u044f", "\u041d\u043e\u044f\u0431\u0440\u044f", "\u0414\u0435\u043a\u0430\u0431\u0440\u044f"],
    "months_short": ["\u044f\u043d\u0432", "\u0444\u0435\u0432", "\u043c\u0430\u0440", "\u0430\u043f\u0440", "\u043c\u0430\u044f", "\u0438\u044e\u043d", "\u0438\u044e\u043b", "\u0430\u0432\u0433", "\u0441\u0435\u043d", "\u043e\u043a\u0442", "\u043d\u043e\u044f", "\u0434\u0435\u043a"],
    "today": "\u0441\u0435\u0433\u043e\u0434\u043d\u044f",
    "yesterday": "\u0432\u0447\u0435\u0440\u0430",
    "at": "\u0432",
    "ampm": false
};
function getLang(arg) {
    return lang[arg] !== undefined ? lang[arg] : arg;
}
function dateFormat(datetime, show_sec, onlyTime) {
    if (!datetime)
        return false;
    datetime = parseInt(datetime, 10);
    show_sec = !!(show_sec);
    var n = new Date();
    var d = new Date(datetime * 1000);
    var y = new Date((datetime + 86400) * 1000);
    var str = "";

    if (y.getDate() + '' + y.getMonth() + '' + y.getFullYear() == n.getDate() + '' + n.getMonth() + '' + n.getFullYear()) {
        str += getLang('yesterday');
    } else if (d.getDate() + '' + d.getMonth() + '' + d.getFullYear() == n.getDate() + '' + n.getMonth() + '' + n.getFullYear()) {
        str += getLang('today');
    } else {
        str += d.getDate() + " " + lang.months_short[d.getMonth()];
    }
    str += (d.getFullYear() !== n.getFullYear()) ? " " + d.getFullYear() : "";
    str += " " + getLang('at') + " ";
    var hours = d.getHours();
    var p = '';
    if (lang['ampm']) {
        p = "am";
        if (hours >= 12) {
            hours = hours - 12;
            p = "pm";
        }
        if (hours == 0) {
            hours = 12;
        }
        p = " " + p;
        str += hours;
    } else {
        str += hours;
    }
    str += ":" + (d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes());
    str += show_sec ? ":" + (d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds()) : "";
    return str + '' + p;
}
function clickable(codeElems) {
    //var codeElems = $(".code"); //jQuery
    for (var c = 0; c < codeElems.length; c++) {
        var content = codeElems[c].innerHTML;
        if (content.indexOf('<noindex>') !== -1) {
            continue;
        }
		if(content.indexOf('http') == -1){
			continue;
		}
		content = content.replace('&nbsp;', '');
        //var pattern = /((https?\:\/\/|ftp\:\/\/)|(www\.))(\S+)(\w{2,4})?(\/|\/([\w#!:.?+=&%@!\-\/]))/gi;
        var pattern = /(.?)([-a-zA-Z0-9@:%_\+.~#?&\/\/=]{2,256}\.[a-z]{2,4}\b(\/?[-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)?)/gi;
        //var pattern = /(http|https|ftp|ftps)://(.*?)(\s|\n|[,.?!](\s|\n)|$)/;
		content = content.replace(pattern, function (url) {
            nice = url;
			console.log(nice);
			if(nice.match('"') || nice.match("'")){
				return url;
			}
            if (url.match('^https?:\/\/'))
                nice = nice.replace(/^https?:\/\//i, '')
            else
                url = 'http://' + url;
            /* Вовзращаем кликабельный урл, завернутый в nofollow и яндексовый noindex: */
            return '<noindex><a target="_blank" rel="nofollow" href="' + url + '">' + nice.replace(/^www./i, '') + '</a></noindex>';
        });
        codeElems[c].innerHTML = content;
    }
}

$(document).on("click", ".like_photo", function (e) {
    var id = parseInt($(this).attr("data-id"));
    $.post("/album/album-ajax", "add_like=1&photo_id=" + id + "", function (data, textStatus) {
            $(".photoWrap").html(data);
        }
    );

    return false;
});

$(document).on('click', '.toggleMt', function () {
    var mtId = $(this).attr('data-id');
    $('.modal_tariff').hide();
    $('.modal_tariff.tariff' + mtId).show();
    $('.modal_tariff.tariff' + mtId).center();
    $('#overlay').show();
    $('body').addClass('lock');
    return false;
});
function showModal(par, func) {
    $(par).show().center();
    $('#overlay').show();
    $('body').addClass('lock');
    $(par).find('.close').click(function () {
        hideModal(par);
        return false;
    });
    if (func !== undefined) {
        $('#overlay').click(func);
    }
}
function hideModal(par) {
    $(par).hide();
    $('#overlay').hide();
    $('body').removeClass('lock');
}
function closeError() {

    $('.error').hide();
    $('#overlay').hide();
    $('body').removeClass('lock');
}
function closeSuccess() {

    $('.success').hide();
    $('#overlay').hide();
    $('body').removeClass('lock');
}

$(document).on('click', '.mtClose,#overlay', function () {
    $('.modal_tariff').hide();
    $('#overlay').hide();
    $('body').removeClass('lock');
    return false;
});
$(document).on('change', 'input[name="level"]', function () {
    $('label.bold').removeClass('bold');
    $(this).parent().toggleClass('bold');
});
$(document).on('submit', '.modal_tariff form', function () {
    if ($(this).serialize()) {
        $.post('', $(this).serialize(), function (data) {
            eval(data);
        });
    }
    return false;
});

$(document).on('click', '#newCode', function () {
    if (!$('input[name="newCode"]').size()) {
        $('#formPrivate').append('<input type="hidden" name="newCode" value="1" />');
    } else {
        $('input[name="newCode"]').val('1');
    }
    $('#formPrivate').submit();
    setTimeout(function () {
        $('input[name="newCode"]').val('0');
    }, 500);
    //$(this).unbind('click');
    return false;
});
var timeCode = 300;
var timeCode_ = null;
function startNewCode() {
    //timeCode = 300;
    clearInterval(timeCode_);
    timeCode_ = setInterval(function () {
        timeCode--;
        if (timeCode <= 0) {
            $('#textErrCode').html('<a href="#" id="newCode">Выслать новый код</a>');
            clearInterval(timeCode_);
        }
        $('#newCodeTime').text(timeCode);
    }, 1000);
    console.log('start');

}