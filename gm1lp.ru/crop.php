<?php
/***
 * послать на /?setphoto=имя_файла.png
 */
if (!isset($_FILES['photo'])) {
    exit();
}

$iWidth = $iHeight = 300;
$iJpgQuality = 90;

$sTempFileName = $_FILES['photo']['tmp_name'];
$sz = getimagesize($sTempFileName);
$oldh = $sz[1];
$oldw = $sz[0];

if ($oldw > 500) {
    //echo $_POST['w'];
    $inx = $oldw / 500;
    $_POST['w'] *= $inx;
    $_POST['h'] *= $inx;
    $_POST['x1'] *= $inx;
    $_POST['y1'] *= $inx;

    //die('='.$_POST['w']);
}

if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
    $aSize = getimagesize($sTempFileName); // try to obtain image info
    if (!$aSize) {
        @unlink($sTempFileName);
        die('Глюк!');
    }

    // check for image type
    switch ($aSize[2]) {
        case IMAGETYPE_JPEG:
            $sExt = '.jpg';

            // create a new image from file
            $vImg = @imagecreatefromjpeg($sTempFileName);
            break;
        case IMAGETYPE_PNG:
            $sExt = '.png';

            // create a new image from file
            $vImg = @imagecreatefrompng($sTempFileName);
            break;
        default:
            @unlink($sTempFileName);
            die('Не тот формат!');
    }

    // create a new true color image
    $vDstImg = @imagecreatetruecolor($iWidth, $iHeight);

    // copy and resize part of an image with resampling
    imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

    $newName = md5(microtime()) . $sExt;
    // define a result image filename
    $sResultFileName = 'uploads/avatars/' . $newName;

    // output image to file
    imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
    @unlink($sTempFileName);

    header('location: /?setAvatar=' . $newName . '');
    exit;
} else
    die('Картинки нет!');
