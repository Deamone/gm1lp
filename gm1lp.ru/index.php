<?php
$_SERVER['ORIGIN_HOST'] = $_SERVER[''];

$SYSTEM = array(
    'headers' => array(),
    'get' => $_GET,
    'post' => $_POST,
    'files' => $_FILES,
    'db' => null
);

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/core/lang/ru.php';
require $_SERVER['DOCUMENT_ROOT'] . '/core/php-init.php';

header('Access-Control-Allow-Origin: *');

if (isset($_POST['authorize'])) {
    if (Session::get_instance()->get_uid() == 0)
        die(json_encode(array('status' => 'Fail')));
    else
        die(json_encode(array('status' => 'OK', 'session' => Session::get_instance()->get_id(), 'hash' => Session::get_instance()->get_hash())));
}

if (isset($_GET['auth']) && !isAjax() && !isset($_GET['callback'])) {
    if (isset($_COOKIE['PHPSESSID'])) {
        unset($_COOKIE['PHPSESSID']);
    }
    if (isset($_COOKIE['session']) && !empty($_COOKIE['session'])) {
        $auth = array();
        foreach ($_COOKIE as $k => $v) {
            if ($k !== 'session' && $k !== 'hash') {
                continue;
            }
            $auth[] = '"' . $k . '":"' . $v . '"';
        }
        $auth = implode(',', $auth);
        echo <<<JS
            var auth_data = { $auth };
            var href = (window.location.href).replace('www.','');
            if(!window.jt)
                jt = {};
            jt['cookies'] = '1.0.0';
            cookies = {
                cookies: null,
                set: function (name, value, days) {
                    if (!this.cookies)
                        this.init();
                    this.cookies[name] = value;
                    var cookie = name + '=' + value + '; path=/';
                    if (days) {
                        var date = new Date();
                        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                        cookie += '; expires=' + date.toGMTString();
                    }
                    document.cookie = cookie;
                },
                get: function (name) {
                    if (!this.cookies)
                        this.init();
                    return this.cookies[name];
                },
                unset: function (name) {
                    if (!this.cookies)
                        this.init();
                    var value = this.cookies[name];
                    delete this.cookies[name];
                    this.set(name, value, -1);
                },
                init: function () {
                    this.cookies = {};
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i].split('=');
                        if (c.length == 2)
                            this.cookies[c[0].match(/^[\s]*([^\s]+?)$/i)[1]] = c[1].match(/^[\s]*([^\s]+?)$/i)[1];
                    }
                }
            };
            if(auth_data) {
                for(i in auth_data){
                    cookies.set(i, auth_data[i]);
                }
                window.location.href='/';
            }
JS;
    }
    exit();
}


/* * *
 * TODO: Настроить mysql сервер на кодировку utf8 по умолчанию
 */
$route = isset($_GET['_route_']) ? str_replace('.', '', $_GET['_route_']) : '';
if ($route != 'page/login' && $route != 'page/register' && $route != 'page/logout' && strpos($route, 'vk') === false && strpos($route, 'album') === false) {
    setcookie('referer', $_SERVER['REQUEST_URI'], time() + 360, '/');
} else {
    setcookie('referer', '', time() - 360, '/');
}
if (isset($_GET['ref'])) {
    setcookie('referal', (int)$_GET['ref'], time() + 3600, '/');
}

$USER = getUserById(Session::get_instance()->get_uid());
if ($USER === false){
    $USER = array();
    $usr = array('ava' => 'avatar-main.jpg', 'id' => 0);
}
/* * ***************************************************************** */

if ($USER && isset($_GET['referer'])) {
    $session = Session::get_instance()->get_id();
    $hash = Session::get_instance()->get_hash();
    $_GET['referer'] = str_replace(array('http://', 'https://'), array('', ''), $_GET['referer']);
    redirect('http://' . $_GET['referer'] . "/?session=$session&hash=$hash");
}

if (!$route || $route == 'index' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/index.php';
} ///////////
elseif ($route == 'admin/index' && $USER && ($USER['id'] == 41 || $USER['id'] == 19279)) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/admin/index.php';
} elseif ($route == 'admin/help_index' && $USER && ($USER['id'] == 41 || $USER['id'] == 19279)) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/admin/help_index.php';
} elseif ($route == 'admin/help_page_create' && $USER && ($USER['id'] == 41 || $USER['id'] == 19279)) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/admin/help_page_create.php';
} elseif ($route == 'admin/help_page_edit' && $USER && ($USER['id'] == 41 || $USER['id'] == 19279)) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/admin/help_page_edit.php';
} elseif ($route == 'admin/help_page_remove' && $USER && ($USER['id'] == 41 || $USER['id'] == 19279)) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/admin/help_page_remove.php';
} elseif ($route == 'admin/trc_index' && $USER && ($USER['id'] == 41 || $USER['id'] == 19279)) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/admin/trc_index.php';
} elseif ($route == 'page/new_pass' && !$USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/new_pass.php';
} elseif ($route == 'util/city') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/util/city.php';
} elseif ($route == 'util/country') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/util/country.php';
} elseif ($route == 'util/skills') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/util/skills.php';
} elseif ($route == 'util/url') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/util/url.php';
}/* elseif (preg_match('/id(\d+)/', $route, $arr)) {
  $_GET['id'] = $arr[1];
  include $_SERVER['DOCUMENT_ROOT'] . '/pages/index.php';
  } */ elseif ($route == 'page/tariff' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/tariff.php';
} elseif ($route == 'page/login') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/login.php';
} elseif ($route == 'page/register' && !$USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/register.php';
} elseif ($route == 'page/logout' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/logout.php';
} elseif ($route == 'albums' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/albums/albums.php';
} elseif ($route == 'album/set' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/albums/set.php';
} elseif ($route == 'video' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/video/index.php';
} elseif ($route == 'video/add' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/video/add.php';
} elseif ($route == 'video/ajax' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/video/ajax.php';
} elseif ($route == 'album/album-ajax') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/albums/album.ajax.php';
} elseif ($route == 'messages' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/messages.php';
} elseif ($route == 'message' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/message.php';
} elseif ($route == 'friends' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/friends.php';
} elseif ($route == 'contacts' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/contacts.php';
} elseif ($route == 'search' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/search.php';
} elseif ($route == 'settings' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/settings.php';
} elseif ($route == 'skills' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/skills.php';
} elseif ($route == 'private' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/private.php';
} elseif ($route == 'secure' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/secure.php';
} elseif ($route == 'balance') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/balance.php';
} elseif ($route == 'album' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/albums/album.php';
} elseif ($route == 'invites' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/invites.php';
} elseif ($route == 'confirm_email' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/confirm_email.php';
} elseif ($route == 'post') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/post.php';
} ############################################
elseif ($route == 'group') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/groups/group.php';
} elseif ($route == 'group/set' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/groups/set.php';
} elseif ($route == 'memberList' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/groups/members.php';
} elseif ($route == 'groups' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/groups.php';
} elseif ($route == 'group/create' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/groups/create.php';
} ############################################
elseif ($route == 'veripro' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/veripro.php';
} elseif ($route == 'feed' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/feed.php';
} elseif ($route == 'referals' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/referals.php';
} elseif ($route == 'auth/vk' && !$USER) { // Auth
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/auth/vk.php';
} elseif ($route == 'auth/vk_link' && $USER) { // Auth
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/auth/vk_link.php';
} elseif (strpos($route, 'article/') !== false) {
    if (!$USER) {
        $USER = $usr;
    }
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/article.php';
} elseif ($route == 'help/index') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/help/index.php';
} elseif (strpos($route, 'help/') !== false) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/help/help.php';
} else {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/index.php';
}
?>
