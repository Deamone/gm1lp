<?php
/*
  if (isset($_POST) && isset($_GET['vq'])) {
  file_put_contents('cat', serialize($_POST));
  }
  if (isset($_GET['v'])) {
  $v = file_get_contents('cat');
  $cat = print_r(unserialize($v));

  exit;
  } */
foreach ($_POST as $k => $v) {
    if (!is_array($v)) {
        $_POST[$k] = htmlspecialchars($v, ENT_COMPAT);
    }
}
$SYSTEM = array(
    'headers' => array(),
    'get' => $_GET,
    'post' => $_POST,
    'files' => $_FILES,
    'db' => null
);

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
require $_SERVER['DOCUMENT_ROOT'] . '/core/lang/ru.php';
require $_SERVER['DOCUMENT_ROOT'] . '/core/php-init.php';

if (isset($_GET['session']) && isset($_GET['hash'])) {
    Session::get_instance()->restore($_GET['session'], $_GET['hash']);
    redirect();
}

if (isset($_GET['auth']) && !isAjax() && !isset($_GET['callback'])) {
    if (isset($_COOKIE['PHPSESSID'])) {
        unset($_COOKIE['PHPSESSID']);
    }
    if (isset($_COOKIE['session']) && !empty($_COOKIE['session'])) {
        $auth = array();
        foreach ($_COOKIE as $k => $v) {
            if ($k !== 'session' && $k !== 'hash') {
                continue;
            }
            $auth[] = '"' . $k . '":"' . $v . '"';
        }
        $auth = implode(',', $auth);
        echo <<<JS
            var auth_data = { $auth };
            var href = (window.location.href).replace('www.','');
            if(!window.jt)
                jt = {};
            jt['cookies'] = '1.0.0';
            cookies = {
                cookies: null,
                set: function (name, value, days) {
                    if (!this.cookies)
                        this.init();
                    this.cookies[name] = value;
                    var cookie = name + '=' + value + '; path=/';
                    if (days) {
                        var date = new Date();
                        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                        cookie += '; expires=' + date.toGMTString();
                    }
                    document.cookie = cookie;
                },
                get: function (name) {
                    if (!this.cookies)
                        this.init();
                    return this.cookies[name];
                },
                unset: function (name) {
                    if (!this.cookies)
                        this.init();
                    var value = this.cookies[name];
                    delete this.cookies[name];
                    this.set(name, value, -1);
                },
                init: function () {
                    this.cookies = {};
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i].split('=');
                        if (c.length == 2)
                            this.cookies[c[0].match(/^[\s]*([^\s]+?)$/i)[1]] = c[1].match(/^[\s]*([^\s]+?)$/i)[1];
                    }
                }
            };
            if(auth_data) {
                for(i in auth_data){
                    cookies.set(i, auth_data[i]);
                }
                window.location.href='/';
            }
JS;
    }
    exit();
}

/* * *
 * TODO: Настроить mysql сервер на кодировку utf8 по умолчанию
 */
$route = isset($_GET['_route_']) ? str_replace('.', '', $_GET['_route_']) : '';
if ($route != 'page/login' && $route != 'page/register' && $route != 'page/logout' && strpos($route, 'vk') === false && strpos($route, 'album') === false) {
    setcookie('referer', $_SERVER['REQUEST_URI'], time() + 360, '/', '.' . $_SERVER['HTTP_HOST']);
} else {
    setcookie('referer', '', time() - 360, '/', '.' . $_SERVER['HTTP_HOST']);
}
if (isset($_GET['ref'])) {
    setcookie('referal', (int) $_GET['ref'], time() + 3600, '/', '.' . $_SERVER['HTTP_HOST']);
}

$USER = getUserById(Session::get_instance()->get_uid());
if ($USER === false){
    $USER = array();
    $usr = array('ava' => 'default.jpg', 'id' => 0);
}
/* * ***************************************************************** */





if (!$route || $route == 'index') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/index.php';
}
///////////////////////////
elseif ($route == 'action/create' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/action/create.php';
}elseif ($route == 'util/city') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/util/city.php';
} elseif ($route == 'action/list' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/action/list.php';
} elseif ($route == 'action/action') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/action/action.php';
} elseif ($route == 'action/search') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/action/search.php';
} elseif ($route == 'action/preview' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/action/preview.php';
} elseif ($route == 'action/my' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/action/my.php';
} elseif ($route == 'user/lk' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/user/lk.php';
} elseif ($route == 'howto') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/howto.php';
} elseif ($route == 'country') {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/country.php';
}
///////////////////////
elseif ($route == 'page/login' && !$USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/login.php';
} elseif ($route == 'page/logout' && $USER) {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/page/logout.php';
} elseif ($route == 'auth/vk' && !$USER) { // Auth
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/auth/vk.php';
} elseif ($route == 'auth/vk_link' && $USER) { // Auth
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/auth/vk_link.php';
} else {
    include $_SERVER['DOCUMENT_ROOT'] . '/pages/index.php';
}
?>