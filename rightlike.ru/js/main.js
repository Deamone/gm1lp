jQuery(document).ready(function ($) {
    $(window).resize(function () {
        if (this.resizeTO)
            clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function () {
            $(this).trigger('resizeEnd');
        }, 500);
    });

    $('.wall_post_comment textarea').click(function () {
        $(this).attr('rows', 3).autosize();
        $(this).next('.wall_post_btn').show();
    });

    /* MORE COMMENTS BUTTONS */
    var toggleMoreBtn = function (btn, count) {
        var $this = btn;
        if (count == 0) {
            $this.removeClass('wall_post_more_comments').removeClass('rounded_3').removeClass('wall_post_hide_comments');
            $this.children('.text').html('');
        } else {
            if ($this.hasClass('wall_post_more_comments')) {
                $this.removeClass('wall_post_more_comments').removeClass('rounded_3').addClass('wall_post_hide_comments');
                $this.children('.text').html('Скрыть комментарии');
            } else {
                $this.removeClass('wall_post_hide_comments').addClass('wall_post_more_comments').addClass('rounded_3');
                $this.children('.text').html('Показать все ' + count + ' комментария');
            }
        }
    }
    $('.wall_post_hide_comments, .wall_post_more_comments').click(function () {
        var $this = $(this);
        if ($this.hasClass('wall_post_more_comments')) {
            $.getJSON("test.json", function (data) {
                var count = data.comments.length;
                toggleMoreBtn($this, count);
                for (var key in data.comments) {
                    var comment = data.comments[key];
                    var user = comment.user;
                    var months = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
                    var date = new Date(parseInt(comment.date) * 1000);
                    var dateFrmt = date.getDay() + " " + months[date.getMonth()] + " " + date.getFullYear() + " в " + date.getHours() + ":" + date.getMinutes();

                    var post = $('<div>').addClass('wall_post')
                    var profileLogo = $('<div>').addClass('wall_post_profile').append(
                        $('<a>').attr('href', '#id' + user.id).append($('<img>').attr('src', user.logo))
                    );
                    var content = $('<div>').addClass('wall_post_content');
                    var header = $('<div>').addClass('wall_post_header');
                    var counter = $('<a>').attr('href', '#').addClass('wall_icon').addClass('wall_icon_counter' + (comment.likes > 0 ? '_y' : '')).html(comment.likes);
                    var profile = $('<a>').attr('href', '#id' + user.id).addClass('wall_post_profile_link').html(user.surname + " " + user.name);
                    var dateBlock = $('<span>').addClass('wall_post_date').html(dateFrmt);
                    var message = $('<div>').addClass('wall_post_message').html(comment.text);

                    header.append($('<div>').addClass('wall_post_toolbox_top').append(counter)).append(profile).append(dateBlock);
                    content.append(header).append(message);

                    post.append(profileLogo).append(content);

                    $this.after(post);
                }
            });
        } else {
            var $parent = $(this).parent();
            var posts = $('.wall_post', $parent);
            var count = posts.length - 2;

            posts = posts.slice(0, count);
            posts.remove();

            toggleMoreBtn($this, count);
        }
    });

    /* POPUP */
    $(window).bind('resizeEnd', function () {
        adjustPopups();
    })

    var hidePopups = function () {
        $('.popup').hide();
        $('.item_popup_menu').removeClass('active');
    }
    var adjustPopups = function () {
        $('.item_popup_menu').each(function () {
            var $this = $(this);
            if (!$this.hasClass('active'))
                return;
            var popup = $('#' + $this.attr('popup-target')), position = $this.attr('popup-pos'),
                boundary = $this.attr('popup-boundary'), fixed = $this.attr('popup-fixed');
            adjustPopup(popup, $this, position, boundary, fixed)
        });
    }

    var adjustPopup = function (popup, target, position, boundary, fixed) {
        var pos = target.position();
        var width = target.outerWidth();
        var height = target.outerHeight();

        var widthGap = target.outerWidth(true) - width;
        var heightGap = target.outerHeight(true) - height;
        var leftMargin = parseInt(target.css("marginLeft"), 10);
        var topMargin = parseInt(target.css("marginTop"), 10);

        var topOffset = 0, leftOffset = 0;
        if (popup.css('top') !== undefined || popup.css('left') !== undefined) {
            if (popup.attr('top-offset') !== undefined || popup.attr('left-offset') !== undefined) {
                topOffset = popup.attr('top-offset') !== undefined ? parseInt(popup.attr('top-offset')) : 0;
                leftOffset = popup.attr('left-offset') !== undefined ? parseInt(popup.attr('left-offset')) : 0;
            } else {
                var cssTop = parseInt(popup.css('top'), 10), cssLeft = parseInt(popup.css('left'), 10);

                topOffset = !isNaN(cssTop) ? cssTop : 0;
                leftOffset = !isNaN(cssLeft) ? cssLeft : 0;
                popup.attr('top-offset', topOffset);
                popup.attr('left-offset', leftOffset);
            }
        }

        if (position == 'left') {
            popup.css({
                top: (pos.top + height + topOffset) + "px",
                left: (pos.left + (width - popup.outerWidth()) + leftOffset) + "px"
            })
        } else if (position == 'center') {
            var calcLeftOffset = (pos.left + ((width + widthGap) / 2 - popup.outerWidth() / 2) + leftOffset);
            var resultLeftOffset = calcLeftOffset;
            if (boundary !== undefined) {
                var bWidth = $(boundary).outerWidth(), bPos = $(boundary).offset();
                if (calcLeftOffset < bPos.left)
                    resultLeftOffset = bPos.left;
                if (calcLeftOffset + popup.outerWidth() > bPos.left + bWidth)
                    resultLeftOffset = bPos.left + (bWidth - popup.outerWidth());
            }
            popup.css({
                top: (pos.top + height + topOffset) + "px",
                left: resultLeftOffset + "px"
            });

            if (fixed !== undefined) {
                var fixedMarginLeft = 0;
                if ($(fixed).attr('left-offset') !== undefined)
                    fixedMarginLeft = parseInt($(fixed).attr('left-offset'));
                else {
                    fixedMarginLeft = parseInt($(fixed).css('marginLeft'), 10);
                    $(fixed).attr('left-offset', fixedMarginLeft);
                }
                $(fixed).css('marginLeft', fixedMarginLeft + (calcLeftOffset - resultLeftOffset) + "px");
            }

        } else {
            popup.css({
                top: (pos.top + height + topOffset) + "px",
                left: (pos.left + leftOffset) + "px"
            })
        }

        // popup.insertAfter(target);
    }
    var updatePopup = function (id, succCb) {
        succCb();
    }
    $('html').click(function () {
        hidePopups();
    });
    $('.item_popup_menu').click(function (event) {
        event.stopPropagation();
        event.preventDefault();

        var $this = $(this);
        var popup = $('#' + $this.attr('popup-target')), position = $this.attr('popup-pos'),
            boundary = $this.attr('popup-boundary'), fixed = $this.attr('popup-fixed');

        if ($this.hasClass('active'))
            return;

        hidePopups();
        adjustPopup(popup, $this, position, boundary, fixed);

        $this.addClass('active');

        popup.click(function (event) {
            event.stopPropagation();
        })

        updatePopup('', function () {
            popup.fadeIn(200);
        });
    });

    /* Search location */
    $('.styled_checkbox').each(function () {
        var $this = $(this);
        var wrapper = $('<ul>');
        $this.children('input[type=checkbox], input[type=radio]').each(function () {
            var $input = $(this);
            var $label = $this.children('label[for=' + $input.attr('id') + ']');
            var type = $input.attr('type');
            // console.log($input.attr("class"));
            var $element = $('<li>').attr('input-id', $input.attr('id')).attr('class', $input.attr("class")).addClass('checkbox_wrapper').html($label.html());

            var checked = $input.is(':checked');

            $input.hide();
            $label.hide();

            if (checked)
                $element.addClass('checked');

            /* Events */
            $input.change(function () {
                var ch = $input.prop('checked');
                if (type == 'radio') {
                    $element.parent().children('li.checkbox_wrapper').removeClass('checked');
                }

                if (ch)
                    $element.addClass('checked');
                else
                    $element.removeClass('checked');
            });

            $element.click(function () {
                console.log('$element.click');
                var ch = $(this).hasClass('checked');
                if (type == 'radio') {
                    if (ch)
                        return;
                    $input.prop('checked', true).trigger("change");
                    $element.parent().children('li.checkbox_wrapper').removeClass('checked');
                    $(this).addClass('checked');
                } else {
                    $input.prop('checked', !ch).trigger("change");
                    if (ch)
                        $(this).removeClass('checked');
                    else
                        $(this).addClass('checked');
                }
            });

            wrapper.append($element);
        });
        $this.append(wrapper);
    });

    /* PROGRESSLINE */
    $('.progressline').each(function () {
        var percentage = $(this).attr('percentage');
        var width = $(this).width();
        var progressWidth = width * (percentage / 100);

        $(this).append($('<div>').addClass('status').addClass('rounded_2').width(progressWidth));
    });

    $('[name=city]').change(function () {
        $input = $('[name=city]:checked').first();
        $label = $('label[for=' + $input.attr('id') + ']');

        $('#city_text').html($label.html());
        hidePopups();
    });

    /* Example to change subcategories */
    // $('[name=categories]').change(function() {
    // 	$('.subcategory input[type=checkbox]').prop('checked', false).trigger('change');

    // 	$('#clothing_skirt, #underwear_tights, #shoes_6, #accessory_7').prop('checked', true).trigger('change');
    // });

    // /* Example to set category */
    // $('#category_8').prop('checked', true).trigger('change');

    $('#chooseImage1_').click(function (event) {
        var chooseId = $(this).attr('id');
        event.preventDefault();
        var img = $(this).attr('img-src');

        // var img = "images/avatar-main.jpg";
        // http://jquery.malsup.com/block /
        $.blockUI({
            message: "Пожалуйста, подождите...",
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .7,
                color: '#fff'
            },
            onBlock: function () {
                setTimeout(function () {
                    $('<img>').attr('src', img).one('load', function () {
                        // Clear data
                        $('#cancel_btn, #next_btn').unbind("click");
                        $('.ic_image').html('');
                        $('#step_cur').html('1');
                        $('#step_info').html('Выберите обложку');
                        $('#next_btn').html('Далее');
                        $('.ic_steps').show();

                        $img = $(this);

                        var wrapper = $('#image_crop');
                        $('.ic_image', wrapper).append($img);

                        wrapper.css({position: "absolute", visibility: "hidden", display: "block"});

                        wrapper.width(960);
                        wrapper.height(630);
                        var width = wrapper.outerWidth(),
                            height = wrapper.outerHeight();

                        wrapper.css({position: "", visibility: "", display: ""});

                        $.blockUI({
                            message: $('#image_crop'),
                            css: {
                                cursor: 'default',
                                border: 'none',
                                marginLeft: '-' + width / 2 + 'px',
                                marginTop: '-' + height / 2 + 'px',
                                width: width + 'px',
                                height: height + 'px',
                                top: '50%',
                                left: '50%'
                            },
                            onBlock: function () {
                                $('#cancel_btn').click(function (event) {
                                    event.preventDefault();
                                    $.unblockUI()
                                });

                                // http://deepliquid.com/projects/Jcrop/ /
                                var coordsMin;
                                var coords;
                                $img.Jcrop({
                                    minSize: [291, 327],
                                    setSelect: [0, 0, 291, 327],
                                    aspectRatio: 291 / 327,
                                    boxWidth: 960,
                                    boxHeight: 529,
                                    onSelect: function (c) {
                                        // Get coordionates
                                        coordsMin = c;
                                    }
                                });

                                $('#next_btn').click(function () {
                                    JcropAPI = $img.data('Jcrop');
                                    JcropAPI.destroy();
                                    $img.Jcrop({
                                        minSize: [724, 453],
                                        setSelect: [0, 0, 724, 453],
                                        aspectRatio: 724 / 453,
                                        boxWidth: 960,
                                        boxHeight: 529,
                                        onSelect: function (c) {
                                            // Get coordionates
                                            coords = c;
                                        }
                                    });


                                    $('#step_cur').html('2');
                                    $('#step_info').html('Выберите длинное изображение');
                                    $('#next_btn').html('Сохранить');

                                    $(this).unbind("click");
                                    $(this).click(function () {
                                        // Save 
                                        callbackChoose(chooseId, coords, coordsMin);
                                        $.unblockUI();
                                    });
                                });
                            },
                            onUnblock: function () {
                                JcropAPI = $img.data('Jcrop');
                                JcropAPI.destroy();
                            }
                        })
                    }).each(function () {
                        if (this.complete)
                            $(this).load();
                    });
                }, 0)
            }
        });

    });

    $('#chooseImage2_,#chooseImage3_').click(function (event) {
        event.preventDefault();
        var chooseId = $(this).attr('id');
        var img = $(this).attr('img-src');

        // var img = "images/avatar-main.jpg";
        // http://jquery.malsup.com/block /
        $.blockUI({
            message: "Пожалуйста, подождите...",
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .7,
                color: '#fff'
            },
            onBlock: function () {
                setTimeout(function () {
                    $('<img>').attr('src', img).one('load', function () {
                        // Clear data
                        $('#cancel_btn, #next_btn').unbind("click");
                        $('.ic_image').html('');
                        $('#step_cur').html('1');
                        $('#step_info').html('Выберите длинное изображение');
                        $('#next_btn').html('Сохранить');
                        $('.ic_steps').hide();

                        $img = $(this);

                        var wrapper = $('#image_crop');
                        $('.ic_image', wrapper).append($img);

                        wrapper.css({position: "absolute", visibility: "hidden", display: "block"});

                        wrapper.width(960);
                        wrapper.height(630);
                        var width = wrapper.outerWidth(),
                            height = wrapper.outerHeight();

                        wrapper.css({position: "", visibility: "", display: ""});

                        $.blockUI({
                            message: $('#image_crop'),
                            css: {
                                cursor: 'default',
                                border: 'none',
                                marginLeft: '-' + width / 2 + 'px',
                                marginTop: '-' + height / 2 + 'px',
                                width: width + 'px',
                                height: height + 'px',
                                top: '50%',
                                left: '50%'
                            },
                            onBlock: function () {
                                $('#cancel_btn').click(function (event) {
                                    event.preventDefault();
                                    $.unblockUI()
                                });

                                // http://deepliquid.com/projects/Jcrop/ /
                                var coords;
                                $img.Jcrop({
                                    minSize: [724, 453],
                                    setSelect: [0, 0, 724, 453],
                                    aspectRatio: 724 / 453,
                                    boxWidth: 960,
                                    boxHeight: 529,
                                    onSelect: function (c) {
                                        // Get coordionates
                                        coords = c;
                                    }
                                });

                                $('#next_btn').click(function () {
                                    // Save 
                                    callbackChoose(chooseId, coords);
                                    $.unblockUI();
                                });
                            },
                            onUnblock: function () {
                                JcropAPI = $img.data('Jcrop');
                                JcropAPI.destroy();
                            }
                        })
                    }).each(function () {
                        if (this.complete)
                            $(this).load();
                    });
                }, 0)
            }
        });

    });

    $('.tariff_table tr td, .tariff_table tr th').each(function () {
        var index = $(this).index();
        if (index == 1 || index == 4)
            $(this).addClass('td_col_lgray')
        else if (index == 2)
            $(this).addClass('td_col_gray')
        else if (index == 3)
            $(this).addClass('td_col_yellow')
    });

    $('#action_slider').bjqs({
        animtype: 'slide',
        height: 401,
        width: 724,
        keyboardnav: false,
        nexttext: '',
        prevtext: '',
        usecaptions: false,
        showmarkers: false
    });

    $('#main_slider').bjqs({
        animtype: 'slide',
        height: 377,
        width: 1165,
        keyboardnav: false,
        nexttext: '',
        prevtext: '',
        usecaptions: false,
        showmarkers: false
    });


    $('.action_small_wrapper').hover(function () {
        $(this).children('.a_detailed').stop().slideDown('fast');
        $(this).children('.a_description').hide();
    }, function () {
        $(this).children('.a_detailed').stop().slideUp('fast');
        $(this).children('.a_description').show();
    });
});
