<?php
require $_SERVER['DOCUMENT_ROOT'] . '/core/helper.php';
require $_SERVER['DOCUMENT_ROOT'] . '/core/session.php';
#session_start();
$self = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$_SERVER['PHP_SELF'] = $self;
$_SERVER['SCRIPT_URL'] = $self;
$_SERVER['SCRIPT_NAME'] = $self;
/////
$_DBLink = null;
$_SYSTEM['db'] = &$_DBLink;

function query($sql = '') {
    global $_DBLink;

    if ($_DBLink === null) {
        if (!($_DBLink = mysqli_connect(DB_HOST, DB_USER, DB_PASSW, DB_NAME, DB_PORT))) {
            exit('Connect to database: Fail'.mysqli_connect_error());
        }

        mysqli_query($_DBLink, "SET NAMES 'utf8'");
        mysqli_query($_DBLink, "SET CHARACTER SET utf8");
        mysqli_query($_DBLink, "SET CHARACTER_SET_CONNECTION=utf8");
        mysqli_query($_DBLink, "SET SQL_MODE = ''");
        mysqli_set_charset($_DBLink, 'utf8');
    }
    if (!$sql) {
        return;
    }
    $resource = mysqli_query($_DBLink, $sql);

    if ($resource) {
        if ($resource instanceof mysqli_result) {
            $i = 0;
            $data = array();
            while ($result = mysqli_fetch_assoc($resource)) {
                $data[$i] = $result;
                $i++;
            }
            mysqli_free_result($resource);

            $query = array();
            $query['row'] = isset($data[0]) ? $data[0] : array();
            $query['rows'] = $data;
            $query['num_rows'] = $i;
            unset($data);

            return $query;
        } else {
            return true;
        }
    } else {
        exit('Query error: ' . mysqli_errno($_DBLink) . '-->' . mysqli_error($_DBLink));
    }
}

function query_escape($string) {
    global $_DBLink;
    if ($_DBLink === null) {
        if (!($_DBLink = mysqli_connect(DB_HOST, DB_USER, DB_PASSW, DB_NAME, DB_PORT))) {
            exit('Connect to database: Fail'.mysqli_connect_error());
        }

        mysqli_query($_DBLink, "SET NAMES 'utf8'");
        mysqli_query($_DBLink, "SET CHARACTER SET utf8");
        mysqli_query($_DBLink, "SET CHARACTER_SET_CONNECTION=utf8");
        mysqli_query($_DBLink, "SET SQL_MODE = ''");
        mysqli_set_charset($_DBLink, 'utf8');
    }
    return mysqli_escape_string($_DBLink, $string);
}
function query_lastInsertId() {
    global $_DBLink;
    return mysqli_insert_id($_DBLink);
}

function header_($title, $key='', $desc='', $head='', $body_class=''){
    global $USER,$pmc;
    if(!isAjax()){
        include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/header.php';
    }
}

function footer(){
    global $USER,$pmc;
    if(!isAjax()){
        include $_SERVER['DOCUMENT_ROOT'] . '/pages/inc/footer.php';
    }
}

function sender($to, $subject, $message) {
    $headers = 'From: noreply@gm1lp.ru' . "\r\n" .
        'Reply-To: noreply@gm1lp.ru' . "\r\n" .
        'X-Mailer: PHP/' . phpversion() . "\r\n";
    $headers .= 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

    mail($to, $subject, $message, $headers);
}

function cache_set($key, $value, $expire = 360) {
    if (!count($value)) {
        return;
    }
    $file = './cache/cache.' . $key . '.' . (time() + $expire);
    $handle = fopen($file, 'w');
    fwrite($handle, serialize($value));
    fclose($handle);
}

function cache_get($key) {
    $files = glob('./cache/cache.' . $key . '.*');
    if ($files) {
        $cache = file_get_contents($files[0]);
        $data = unserialize($cache);

        /**
         * TODO: Нужна очистка старого кеша???
         */
        foreach ($files as $file) {
            $time = substr(strrchr($file, '.'), 1);
            if ($time < time()) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }

        return $data;
    }

    return false;
}
function cache_delete($key){
    $files = glob('./cache/cache.'.$key.'.*');
    if($files){
        foreach ($files as $file){
            if(file_exists($file)){
                unlink($file);
            }
        }
    }
}