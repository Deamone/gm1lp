<?php

mb_internal_encoding("UTF-8");
if (!class_exists('Memcache')) {

    class Memcache
    {

        function addServer($x, $y)
        {

        }

        function set($x, $y)
        {

        }

        function get($x)
        {

        }

        function connect()
        {

        }

    }

}

function getExtension($filename)
{
    return strtolower(substr(strrchr($filename, '.'), 1));
}

header('Content-type: text/html; charset=utf-8');
$_SESSION = array();
$user = array();
$pmc = new Memcache;
$pmc->addServer('localhost', 11209);

function redirect($url = '/')
{
    header('location: ' . $url);
    exit;
}

function login_form()
{
    global $user;
    if ($user) {
        return '<a href="/">Добро пожаловать, ' . $user['first_name'] . '</a>';
    } else {
        return '<a href="/auth_vk">Войти через Вконтакте</a>';
    }
}

function isAjax()
{
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        return true;
    }
    return false;
}

function getUserById($id)
{
    global $pmc;
    $id = (int)$id;
    #$_ = $pmc->get('user' . $id);
    #if (!$_) {
    $q = query("SELECT * FROM `users` WHERE `id`='$id';");
    $_ = $q['row'];
    #$pmc->set('user' . $id, $_);
    #}

    return $_;
}

/**
 *
 * @global Memcache $pmc
 * @param type $id
 * @return array (to, from) - входящие пользователю, исходящие
 */
function getUserFriends($id)
{
    global $pmc;
    $id = (int)$id;
    $_ = false; //$pmc->get('friends' . $id);
    if (!$_) {
        $fro = FRIEND_REQ_OK;
        $frf = FRIEND_REQ_FROM;
        $frt = FRIEND_REQ_TO;
        // Друзья
        $q1 = query("SELECT * FROM `nx_friends_req` WHERE (`to_id`='$id' OR `from_id`='$id') AND `status`='$fro'");

        // Входящие заявки
        $q2 = query("SELECT * FROM `nx_friends_req` WHERE (`to_id`='$id') AND `status`='$frf' OR `status`='$frt'");
        // Исходящие заявки
        $q3 = query("SELECT * FROM `nx_friends_req` WHERE (`from_id`='$id') AND `status`='$frt' OR `status`='$frf'");


        //$q = query("SELECT `to`,`from`,`status` FROM `nx_friends_req` WHERE (`to`='$id' OR `from`='$id')");
        $_ = array(); // Друзья
        $__ = array(); // Входящие
        $___ = array(); // Исходящие
        for ($i = 0; $i < $q1['num_rows']; $i++) {
            $_[$q1['rows'][$i]['to_id']] = $fro;
            $_[$q1['rows'][$i]['from_id']] = $fro;
        }
        for ($i = 0; $i < $q2['num_rows']; $i++) {
            $__[$q2['rows'][$i]['from_id']] = $frf;
        }
        for ($i = 0; $i < $q3['num_rows']; $i++) {
            $___[$q3['rows'][$i]['to_id']] = $frt;
        }
        unset($_[$id], $__[$id], $___[$id]);

        $pmc->set('friends' . $id, array($_, $__, $___));
    }

    return array($_, $__, $___);
}

function userFriend($from, $to, $remove = false)
{
    global $pmc, $USER;
    $from = (int)$from;
    $to = (int)$to;
    if ($from == $id) {
        return;
    }
    $list = query("SELECT `req_id`,`status`,`from_id` FROM `nx_friends_req` WHERE (`from_id`='$from' AND `to_id`='$to') OR (`to_id`='$from' AND `from_id`='$to');");
    $status = 0;
    $iInit = true;


    $frf = FRIEND_REQ_FROM;
    $frt = FRIEND_REQ_TO;
    if ($list['num_rows']) {
        if ($remove) {
            query("DELETE FROM `nx_friends_req` WHERE `req_id`='" . $list['row']['req_id'] . "';");
            return;
        }
        $status = $list['row']['status'];
        if ($list['row']['from_id'] == $USER['id']) {
            $iInit = 1;
        } else {
            $iInit = 0;
            $frf = FRIEND_REQ_TO;
            $frt = FRIEND_REQ_FROM;
        }
        if ($status == 0) {                     // Если заявок нет вообще
            $status = $frf;
        } elseif ($status & $frt) {            // Если заявка есть с другой стороны
            $status = $frf | $frt;
        }
        if ($list['row']['status'] != $status) {
            query("UPDATE `nx_friends_req` SET `status`='$status' WHERE `req_id`='" . $list['row']['req_id'] . "';");
        }
    } elseif (!$remove) {
        $status = $frf;
        query("INSERT INTO `nx_friends_req` (`from_id`,`to_id`,`status`) VALUES('$from','$to','$status');");
    }

    // DELETE FROM обязательно


    /* $_ = query("SELECT 1 FROM `nx_friends` WHERE (`to`='$to' AND `from`='$from') UNION SELECT 2 FROM `nx_friends` WHERE (`to`='$from' AND `from`='$to')");
      //////
      $from_ = $_['rows'][0][1] == 1 ? true : false;
      $to_ = ($_['rows'][0][1] == 2 || $_['rows'][1][1] == 2) ? true : false;
      if ($remove && ($from_ || $to_)) {
      if ($from_) {
      query("UPDATE `nx_friends` SET `status`='0' WHERE (`to`='$to' AND `from`='$from')");
      }
      if ($to_) {
      query("UPDATE `nx_friends` SET `status`='0' WHERE (`to`='$from' AND `from`='$to')");
      }
      /* $sql = 'DELETE FROM `nx_friends` WHERE ';
      $q = array();
      if ($from_) {
      $q[] = "(`to`='$to' AND `from`='$from')";
      }
      if ($to_) {
      $q[] = "(`to`='$from' AND `from`='$to')";
      }
      query($sql . implode(' AND ', $q));
      } elseif (!$remove) {
      if ($from_) {
      query("UPDATE `nx_friends` SET `status`='1' WHERE (`to`='$to' AND `from`='$from')");
      } else {
      query("INSERT INTO `nx_friends` (`from`,`to`,`status`) VALUES('$from','$to','1');");
      }
      /* if ($to_) {
      query("UPDATE `nx_friends` SET `status`='1' WHERE (`to`='$from' AND `from`='$to')");
      } else {
      query("INSERT INTO `nx_friends` (`to`,`from`,`status`) VALUES('$from','$to','0');");
      }
      }
      $pmc->delete('friends' . $from);
      $pmc->delete('friends' . $to); */
}

function getUserByUrl($url)
{
    global $pmc;
    $url = query_escape($url);
    $_ = false;
    if (!$_) {
        $q = query("SELECT * FROM `users` WHERE `url`='$url';");
        $_ = $q['row'];
        $pmc->set('user' . $_['id'], $_);
    }

    return $_;
}

function getUserByEmail($email)
{
    global $pmc;
    $email = query_escape($email);
    $_ = false;
    if (!$_) {
        $q = query("SELECT * FROM `users` WHERE `login`='$email';");
        $_ = $q['row'];
        $pmc->set('user' . $_['id'], $_);
    }

    return $_;
}

function updateUserById($id, $keys, $values)
{
    global $pmc;
    $id = (int)$id;
    $sql = '';
    $_ = $pmc->get('user' . $id);

    $count = count($keys);
    for ($i = 0; $i < $count; $i++) {
        $sql .= ",`" . $keys[$i] . "`='" . query_escape($values[$i]) . "'";
        if ($_) {
            $_[$keys[$i]] = $values[$i];
        }
    }
    query("UPDATE `users` SET " . substr($sql, 1) . " WHERE `id`='$id';");
    if ($_) {
        $pmc->set('user' . $id, $_);
    }
    return $_;
}

function getUserAudios($id)
{
    global $pmc, $USER;
    $id = (int)$id;
    if ($id == $USER['id']) {
        $sql = '';
    } else {
        $sql = ' AND `private`=0';
    }
    $_ = false; //$pmc->get('user_audio' . $id);
    if (!$_) {
        $q = query("SELECT * FROM `nx_audios` WHERE `user_id`='$id' $sql ORDER BY `position` ASC;");
        $_ = $q['rows'];
        $pmc->set('user_audio' . $id, $_);
    }

    return $_;
}

function getUserAlbums($id)
{
    global $pmc;
    $id = (int)$id;
    $_ = false; // $pmc->get('user_albums' . $id);
    if (!$_) {
        $q = query("SELECT * FROM `nx_albums` WHERE `user_id`='$id' AND `deleted`='0' ORDER BY `created` DESC;");
        for ($i = 0; $i < $q['num_rows']; $i++) {
            if ($q['rows'][$i]['size']) {
                $q2 = query("SELECT * FROM `nx_photos` WHERE `album_id`='" . $q['rows'][$i]['album_id'] . "' AND `deleted`='0' ORDER BY `created` DESC LIMIT 0,1;");
                $q['rows'][$i]['last_photo'] = $q2['row'];
            }
        }
        $_ = $q['rows'];
        $pmc->set('user_albums' . $id, $_);
    }

    return $_;
}

function removeAlbum($id)
{
    global $pmc, $USER;
    $id = (int)$id;
    $pmc->delete('user_album' . $id);
    $pmc->delete('user_albums' . $USER['id']);
    query("UPDATE `nx_albums` SET `deleted`='1'  WHERE `album_id`='$id';");
    query("UPDATE `nx_photos` SET `deleted`='1'  WHERE `album_id`='$id';");
}

function getUserAlbum($id)
{
    global $pmc, $USER;
    $id = (int)$id;
    $_ = false; //$pmc->get('user_album' . $id);
    if (!$_) {
        $q = query("SELECT * FROM `nx_albums` WHERE `album_id`='$id' AND `deleted`='0';");
        if (!$q['num_rows']) {
            return false;
        }
        $_ = $q['row'];
        $q = query("SELECT * FROM `nx_photos` WHERE `album_id`='$id' AND `deleted`='0' ORDER BY `created` DESC;");
        $_['photos'] = $q['rows'];
        for ($i = 0; $i < $q['num_rows']; $i++) {
            $_['photos'][$i]['ilike'] = getLikesPhoto($_['photos'][$i]['photo_id'], $USER['id']);
        }
        $pmc->set('user_album' . $id, $_);
    }

    return $_;
}

function getUserPhotos($user_id)
{
    $user_id = (int)$user_id;
    $q = query("SELECT * FROM `nx_photos` WHERE `user_id`='$user_id' AND `deleted`='0' ORDER BY `photo_id` DESC;");

    return $q['rows'];
}

function getPhoto($photo_id, $id)
{
    global $pmc, $USER;
    $id = (int)$id;
    $photo_id = (int)$photo_id;
    $_ = $pmc->get('user_album' . $id);
    // if (!$_) {
    $q = query("SELECT * FROM `nx_photos` WHERE /*`album_id`='$id' AND*/ `photo_id`='$photo_id' AND `deleted`='0';");
    if ($q['row']) {
        $q2 = query("SELECT * FROM `nx_albums` WHERE `album_id`='" . $q['row']['album_id'] . "';");
        $q['row']['ilike'] = getLikesPhoto($photo_id, $USER['id']);
        $q['row']['album'] = $q2['row'];
        //$q['row']['list_likes'] = getLikesPhoto($photo_id);
    }
    return $q['row'];
    /* }else{
      $c = count($_);
      for($i=0; $i<$c; $i++){
      if($_[$i]['photo_id']==$photo_id){
      return $_[$i];
      }
      }
      } */

    //return $_;
}

function updateSizeAlbums()
{
    $albums = query("SELECT * FROM `nx_albums` WHERE 1");
    foreach ($albums['rows'] as $album) {
        $aid = $album['album_id'];
        $photos = query("SELECT COUNT(*) as `cc` FROM `nx_photos` WHERE `album_id`='$aid' AND `deleted`=0;");
        $cc = $photos['row']['cc'];
        query("UPDATE `nx_albums` SET `size`='$cc' WHERE `album_id`='$aid';");
    }
}

function createAlbum($id)
{
    global $pmc, $_LANG;
    $id = (int)$id;
    $pmc->delete('user_albums' . $id);
    query("INSERT INTO `nx_albums` (`title`,`mask`,`desc`,`created`,`user_id`) VALUES('" . query_escape($_LANG['ALBUMS_NEW_ALBUM']) . "','" . ALBUM_ALLOW_ALL . "','','" . time() . "','" . $id . "');");
}

function addPhotoInAlbum($id, $file)
{
    global $pmc, $_LANG, $USER;
    $id = (int)$id;
    $uid = (int)$USER['id'];
    $pmc->delete('user_album' . $id);
    $pmc->delete('user_albums' . $USER['id']);
    query("INSERT INTO `nx_photos` (`album_id`,`file`,`created`,`likes`,`comments`,`user_id`) VALUES('" . $id . "','" . query_escape($file) . "','" . time() . "','0','0','$uid');");
    query("UPDATE `nx_albums` SET `size`=`size`+1 WHERE `album_id`='$id';");
}

function updateAlbum($id, $name, $desc, $mask)
{
    global $pmc, $USER;
    $id = (int)$id;
    $name = query_escape($name);
    $desc = query_escape($desc);
    $pmc->delete('user_album' . $id);
    $pmc->delete('user_albums' . $USER['id']);
    query("UPDATE `nx_albums` SET `title`='$name', `desc`='$desc',`mask`='$mask'  WHERE `album_id`='$id';");
}

function getPosts($owner_id, $limit = 50)
{
    global $pmc, $USER;
    $owner_id = (int)$owner_id;
    $q = query("SELECT * FROM `nx_wall` WHERE `owner_id`='$owner_id' AND `deleted`='0' ORDER BY `created` DESC LIMIT 0,$limit;");
    $posts = array();
    for ($i = 0; $i < $q['num_rows']; $i++) {
        $q['rows'][$i]['user'] = getUserById($q['rows'][$i]['user_id']);
        $q['rows'][$i]['ilike'] = getLikesPost($q['rows'][$i]['wall_id'], $USER['id']);
        $posts[] = $q['rows'][$i];
    }

    return $posts;
}

function likePost($wall_id)
{
    global $USER, $pmc;
    $wall_id = (int)$wall_id;
    $uid = $USER['id'];
    $like = getLikesPost($wall_id, $USER['id']);
    if ($like) {
        query("UPDATE `nx_wall` SET `likes`=`likes`-1 WHERE `wall_id`='$wall_id';");
        query("DELETE FROM `nx_wall_likes` WHERE `wall_id`='$wall_id' AND `user_id`='$uid';");
    } else {
        query("UPDATE `nx_wall` SET `likes`=`likes`+1 WHERE `wall_id`='$wall_id';");
        query("INSERT INTO `nx_wall_likes` (`wall_id`,`user_id`)VALUES('$wall_id','$uid');");
    }
}

function likePhoto($photo_id, $album_id = false)
{
    global $USER, $pmc;
    $photo_id = (int)$photo_id;
    $uid = $USER['id'];
    $like = getLikesPhoto($photo_id, $USER['id']);
    if ($like) {
        query("UPDATE `nx_photos` SET `likes`=`likes`-1 WHERE `photo_id`='$wall_id';");
        query("DELETE FROM `nx_photo_likes` WHERE `photo_id`='$photo_id' AND `user_id`='$uid';");
    } else {
        query("UPDATE `nx_photos` SET `likes`=`likes`+1 WHERE `photo_id`='$photo_id';");
        query("INSERT INTO `nx_photo_likes` (`photo_id`,`user_id`)VALUES('$photo_id','$uid');");
    }
    if ($album_id) {
        $pmc->delete('user_album' . $album_id);
    }
}

function updatePhoto($photo_id, $desc, $album_id)
{
    global $USER, $pmc;
    $photo_id = (int)$photo_id;
    $desc = query_escape($desc);
    $album_id = (int)$album_id;
    query("UPDATE `nx_photos` SET `desc`='$desc' WHERE `photo_id`='$photo_id';");
    $pmc->delete('user_album' . $album_id);
}

function removePhoto($photo_id, $desc, $album_id)
{
    global $USER, $pmc;
    $photo_id = (int)$photo_id;
    $desc = query_escape($desc);
    $album_id = (int)$album_id;
    query("UPDATE `nx_photos` SET `deleted`='1' WHERE `photo_id`='$photo_id';");
    query("UPDATE `nx_albums` SET `size`=`size`-1 WHERE `album_id`='$album_id';");
    $pmc->delete('user_album' . $album_id);
}

function getCommentsPhoto($photo_id)
{
    $photo_id = (int)$photo_id;
    $q = query("SELECT * FROM `nx_photo_comments` WHERE `photo_id`='$photo_id' ORDER BY `created` DESC;");
    for ($i = 0; $i < $q['num_rows']; $i++) {
        $q['rows'][$i]['user'] = getUserById($q['rows'][$i]['user_id']);
    }

    return $q['rows'];
}

function addCommentPhoto($photo_id, $text)
{
    global $USER;

    $photo_id = (int)$photo_id;
    $text = query_escape($text);
    $album_id = (int)$album_id;
    $created = time();
    $user_id = $USER['id'];

    query("INSERT INTO `nx_photo_comments`(`photo_id`,`user_id`,`message`,`created`)VALUES('$photo_id','$user_id','$text','$created')");
    query("UPDATE `nx_photos` SET `comments`=`comments`+1 WHERE `photo_id`='$photo_id';");
}

function getLikesPhoto($photo_id, $user_id = false)
{
    global $pmc;
    $photo_id = (int)$photo_id;
    $user_id = (int)$user_id;
    if (!$user_id) {
        $q = query("SELECT * FROM `nx_photo_likes` WHERE `photo_id`='$photo_id';");
    } else {
        $q = query("SELECT * FROM `nx_photo_likes` WHERE `photo_id`='$photo_id' AND `user_id`='$user_id';");
    }
    $likes = array();
    for ($i = 0; $i < $q['num_rows']; $i++) {
        $likes[$q['rows'][$i]['user_id']] = $q['rows'][$i]['user_id'];
    }
    if ($user_id) {
        return isset($likes[$user_id]) ? 1 : 0;
    }

    return $likes;
}

function getLikesPost($wall_id, $user_id = false)
{
    global $pmc;
    $wall_id = (int)$wall_id;
    $user_id = (int)$user_id;
    if (!$user_id) {
        $q = query("SELECT * FROM `nx_wall_likes` WHERE `wall_id`='$wall_id';");
    } else {
        $q = query("SELECT * FROM `nx_wall_likes` WHERE `wall_id`='$wall_id' AND `user_id`='$user_id';");
    }
    $likes = array();
    for ($i = 0; $i < $q['num_rows']; $i++) {
        $likes[$q['rows'][$i]['user_id']] = $q['rows'][$i]['user_id'];
    }
    if ($user_id) {
        return isset($likes[$user_id]);
    }

    return $likes;
}

function getPost($wall_id)
{
    global $pmc;
    $wall_id = (int)$wall_id;
    $q = query("SELECT * FROM `nx_wall` WHERE `wall_id`='$wall_id' AND `deleted`='0' ORDER BY `created` DESC;");
    $posts = array();
    for ($i = 0; $i < $q['num_rows']; $i++) {
        $q['rows'][$i]['user'] = getUserById($q['rows'][$i]['user_id']);
        $posts[] = $q['rows'][$i];
    }

    return $posts[0];
}

function removePost($wall_id)
{
    global $pmc;
    $wall_id = (int)$wall_id;
    #query("DELETE FROM `nx_wall` WHERE `wall_id`='$wall_id';");
    query("UPDATE `nx_wall` SET `deleted`='1' WHERE `wall_id`='$wall_id';");

    return true;
}

function updatePost($wall_id, $post)
{
    $wall_id = (int)$wall_id;
    $post = query_escape($post);
    if (empty($post)) {
        return;
    }
    query("UPDATE `nx_wall` SET `text`='$post' WHERE `wall_id`='$wall_id';");
}

function addPost($onwer_id, $post, $user = false)
{
    global $USER;
    if (!$user) {
        $user = $USER['id'];
    }
    $user = (int)$user;
    $onwer_id = (int)$onwer_id;
    $post = query_escape($post);
    if (empty($post)) {
        return;
    }
    query("INSERT INTO `nx_wall` (`owner_id`,`text`,`user_id`,`created`)VALUES('$onwer_id','$post','" . $user . "','" . time() . "');");
}

function enc_passw($password)
{
    return $password;
}

function isVer($USER, $asLevel = false)
{
    $status = '';
    $time = time();
    if ($USER['level'] == 0) {
        $status = 'Новичок';
        $level = 0;
    } elseif (($USER['level'] == 1 && $USER['pro_end'] > $time)) {
        $status = 'Мастер';
        $level = 1;
    } elseif (($USER['level'] == 2 && $USER['pro_end'] > $time)) {
        $status = 'Профессионал';
        $level = 2;
    } elseif (($USER['level'] == 3 && $USER['pro_end'] > $time)) {
        $status = 'Эксперт';
        $level = 3;
    } else {
        $status = 'Новичок';
        $level = 0;
        /* $status = 'ПРО ';
          if ($USER['pro_end'] - $time > 24 * 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 24 / 3600) . ' дней';
          } elseif ($USER['pro_end'] - $time > 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 3600) . ' часов';
          } elseif ($USER['pro_end'] - $time > 60) {
          $status .= (int) (($USER['pro_end'] - $time) / 60) . ' минут';
          } elseif ($USER['level'] > 2) {
          $status .= 'навсегда';
          } else {
          $status .= (int) ($USER['pro_end'] - $time) . ' секунд';
          } */
    }

    return !$asLevel ? $status : $level;
}

function iconVer($USER)
{
    $status = '';
    $time = time();
    if ($USER['level'] == 0) {
        $status = '//gm1lp.ru/images/tariffs/tariff_new.png';
    } elseif (($USER['level'] == 1 && $USER['pro_end'] > $time)) {
        $status = '//gm1lp.ru/images/tariffs/tariff_master.png';
    } elseif (($USER['level'] == 2 && $USER['pro_end'] > $time)) {
        $status = '//gm1lp.ru/images/tariffs/tariff_pro.png';
    } elseif (($USER['level'] == 3 && $USER['pro_end'] > $time)) {
        $status = '//gm1lp.ru/images/tariffs/tariff_expert.png';
    } else {
        $status = '//gm1lp.ru/images/tariffs/tariff_new.png';
        /* $status = 'ПРО ';
          if ($USER['pro_end'] - $time > 24 * 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 24 / 3600) . ' дней';
          } elseif ($USER['pro_end'] - $time > 3600) {
          $status .= (int) (($USER['pro_end'] - $time) / 3600) . ' часов';
          } elseif ($USER['pro_end'] - $time > 60) {
          $status .= (int) (($USER['pro_end'] - $time) / 60) . ' минут';
          } elseif ($USER['level'] > 2) {
          $status .= 'навсегда';
          } else {
          $status .= (int) ($USER['pro_end'] - $time) . ' секунд';
          } */
    }

    return $status;
}

function sym_date($tdate = '')
{
    $e = explode('.', $tdate);
    $treplace = array(
        '',
        "января",
        "февраля",
        "марта",
        "апреля",
        "мая",
        "июня",
        "июля",
        "августа",
        "сентября",
        "октября",
        "ноября",
        "декабря"
    );

    if ($e[1]) {
        $e[1] = $treplace[(int)$e[1]];
    }
    return implode(' ', $e);
}

function getSessionKey($id = false)
{
    global $text, $USER;
    if (!$id) {
        $id = $USER['id'];
    }
    if (!$text) {
        $text = new Memcache;
        $text->connect("localhost", 11201);
    }
    $text->set('secret' . $id, substr(md5($id), 24)); // Обязательно 8 знаков
    $user_secret = $text->get('secret' . $id);

    $im_secret1 = "0123456789ABCDEF"; // replace with appropriate secret values
    $im_secret2 = "0123456789ABCDEF";
    $im_secret3 = "0123456789ABCDEF";
    $im_secret4 = "0123456789ABCDEF";

    $subnet = extractSubnetwork($_SERVER['HTTP_X_REAL_IP']);
    $nonce = sprintf("%08x", mt_rand(0, 0x7fffffff));
    $hlam1 = substr(md5($nonce . $im_secret1 . $subnet), 4, 8);

    $utime = sprintf("%08x", time());
    $utime_xor = xor_str($utime, $hlam1);
    $uid = sprintf("%08x", $id);
    $uid_xor = xor_str($uid, substr(md5($nonce . $subnet . $utime_xor . $im_secret2), 6, 8));
    $check = substr(md5($utime . $uid . $nonce . $im_secret4 . $subnet . $user_secret), 12, 16);

    $im_session = $nonce . $uid_xor . $check . $utime_xor;

    return $im_session;
}

function getSeesionTime($id = false)
{
    global $text, $USER;
    if (!$id) {
        $id = $USER['id'];
    }
    if (!$text) {
        $text = new Memcache;
        $text->connect("localhost", 11201);
    }
    $time = $text->get('timestamp' . $id);
    return !$time ? 0 : $time;
}

function extractSubnetwork($ip)
{
    return substr($ip, 0, strrpos($ip, '.')) . '.';
}

function char_to_hex($c)
{
    $c = ord($c);
    if ($c <= 57) {
        return ($c - 48);
    } else {
        return ($c - 97 + 10);
    }
}

function hex_to_char($c)
{
    if ($c < 10) {
        $c = chr($c + 48);
    } else {
        $c = chr($c - 10 + 97);
    }
    return $c;
}

function xor_str($str1, $str2, $digits = 8)
{
    for ($i = 0, $j = 0; $i < $digits; $i++, $j++) {
        $str1[$i] = hex_to_char(char_to_hex($str1[$i]) ^ char_to_hex($str2[$j]));
    }
    return $str1;
}

function addDialogCookie($user_id)
{
    if (!isset($_COOKIE['dialog_' . $user_id])) {
        setcookie('dialog_' . $user_id, '1', time() + 3600, '/');
        $_COOKIE['dialog_' . $user_id] = 1;
    }
}

function delDialogCookie($user_id)
{
    setcookie('dialog_' . $user_id, '0', time() - 100, '/');
    unset($_COOKIE['dialog_' . $user_id]);
}

function delDialogsCookie()
{
    $keys = array_keys($_COOKIE);
    $c = count($keys);
    for ($i = 0; $i < $c; $i++) {
        if (strpos($keys[$i], 'dialog_') !== false) {
            setcookie($keys[$i], '0', time() - 100, '/');
            unset($_COOKIE[$keys[$i]]);
        }
    }
}

function getDialogsCookie()
{
    $dialogs = array();
    $keys = array_keys($_COOKIE);
    $c = count($keys);
    for ($i = 0; $i < $c; $i++) {
        if (strpos($keys[$i], 'dialog_') !== false) {
            $dialogs[] = getUserById(str_replace('dialog_', '', $keys[$i]));
        }
    }
    return $dialogs;
}

function getArticle($url)
{
    $url = query_escape($url);
    $q = query("SELECT * FROM `nx_articles` WHERE `url`='$url';");
    if (!$q['num_rows']) {
        return false;
    }
    return $q['row'];
}

function getReferals($id)
{
    $id = (int)$id;
    $q = query("SELECT * FROM `users` WHERE `referal`='$id';");

    return array($q['num_rows'], $q['rows']);
}

function moneyForRef($referal)
{
    $referal = (int)$referal;
    query("UPDATE `users` SET `money`=`money`+1 WHERE `id`='$referal';");
}

function addUser($email, $password, $fname, $lname)
{
    $email = query_escape($email);
    $password = md5($password); //md5(SOL.$password.SOL);
    $time = time();
    ///
    $fname = query_escape($fname);
    $lname = query_escape($lname);

    query("INSERT INTO `users` (`name`,`fam`,`password`,`login`,`confirmed`,`created`) VALUES('$fname','$lname','" . $password . "','$email','0','" . time() . "');");

    return query_lastInsertId();
}

function getCity($id = false, $offset = 0, $count = 30)
{
    if ($id === false) {
        $q = query("SELECT * FROM `nx_city` ORDER BY `city_id` ASC LIMIT {$offset},{$count};");

        return $q['rows'];
    } else {
        $id = (int)$id;
        $q = query("SELECT * FROM `nx_city` WHERE `city_id`='{$id}'");

        return $q['row'];
    }
}

function getCity2($id = false, $offset = 0, $count = 8000)
{
    if ($id === false) {
        $q = query("SELECT `id` as `city_id`,`city` as `name` FROM `city` ORDER BY `id` ASC LIMIT {$offset},{$count};");

        return $q['rows'];
    } else {
        $id = (int)$id;
        $q = query("SELECT `id` as `city_id`,`city` as `name` FROM `city` WHERE `id`='{$id}'");

        return $q['row'];
    }
}

function getCityByName($city = '', $country = false, $offset = 0, $count = 8000)
{
    $city = query_escape($city);
    $country = (int)$country;

    if (!$country) {
        $q = query("SELECT `c`.`id` as `id`,CONCAT(`c`.`city`,', ', `cy`.`country`,', ', `r`.`region`)  as `text`
FROM `city` as `c` 
INNER JOIN `country_region` as `cr` 
INNER JOIN `region_city` as `rc` ON `rc`.`id_region`=`cr`.`id_region` AND `c`.`id`=`rc`.`id_city`
INNER JOIN `country` as `cy` ON `cr`.`id_country`=`cy`.`id`
INNER JOIN `region` as `r` ON `r`.`id`=`rc`.`id_region`
WHERE `c`.`city` like '%{$city}%' GROUP BY `c`.`id`, `cy`.`id` ORDER BY `id` ASC LIMIT {$offset},{$count};");
    } else {
        $q = query("SELECT `c`.`id` as `id`,`c`.`city` as `text` 
FROM `city` as `c` 
INNER JOIN `country_region` as `cr` ON `cr`.`id_country`='{$country}' 
INNER JOIN `region_city` as `rc` ON `rc`.`id_region`=`cr`.`id_region` AND `c`.`id`=`rc`.`id_city`
WHERE `c`.`city` like '%{$city}%' GROUP BY `c`.`id` ORDER BY `id` ASC LIMIT {$offset},{$count};");
    }

    return $q['rows'];
}

function getCityById($id)
{
    $id = (int)$id;
    if (!$id) {
        return array('text' => 'Все');
    }

    $q = query("SELECT `c`.`id` as `id`,CONCAT(`c`.`city`,', ', `cy`.`country`,', ', `r`.`region`)  as `text`
FROM `city` as `c`
INNER JOIN `country_region` as `cr`
INNER JOIN `region_city` as `rc` ON `rc`.`id_region`=`cr`.`id_region` AND `c`.`id`=`rc`.`id_city`
INNER JOIN `country` as `cy` ON `cr`.`id_country`=`cy`.`id`
INNER JOIN `region` as `r` ON `r`.`id`=`rc`.`id_region`
WHERE `c`.`id`='$id';");
    return $q['row'];
}

function create_thumbnail($original_file, $thumb_file, $new_w, $new_h, $autoSize = 'none')
{
    //GD check
    if (!function_exists('gd_info')) {
        // ERROR - Invalid image
        return 'GD support is not enabled';
    }
    $info = getimagesize($original_file);
    // Create src_img
    if ($info['mime'] == 'image/jpeg') {
        $src_img = imagecreatefromjpeg($original_file);
    } else if ($info['mime'] == 'image/png') {
        $src_img = imagecreatefrompng($original_file);
    } else if ($info['mime'] == 'image/gif') {
        $src_img = imagecreatefromgif($original_file);
    } else {
        return -1;
    }
    if ($thumb_file == false) {
        imagepng($src_img, $thumb_file);
        imagedestroy($src_img);
        return;
    }

    $src_width = imageSX($src_img); //$src_width
    $src_height = imageSY($src_img); //$src_height

    if ($src_width < $new_w || $src_height < $new_h) {
        imagejpeg($src_img, $thumb_file);
        imagedestroy($src_img);
        return 1;
    }

    if ($src_height > $src_width && $autoSize == 'auto') {
        $autoSize = 'width';
    } elseif ($src_height < $src_width && $autoSize == 'auto') {
        $autoSize = 'height';
    }
    if ($new_h === false) {
        $new_h = $src_height;
    }
    if ($new_w === false) {
        $new_w = $src_width;
    }
    $src_w = $src_width;
    $src_h = $src_height;
    $src_x = 0;
    $src_y = 0;
    $dst_w = $new_w;
    $dst_h = $new_h;
    $src_ratio = $src_w / $src_h;
    $dst_ratio = $new_w / $new_h;

    switch ($autoSize) {
        case "width":
            // AUTO WIDTH
            $dst_w = $dst_h * $src_ratio;
            break;
        case "height":
            // AUTO HEIGHT
            $dst_h = $dst_w / $src_ratio;
            break;
        case "none":
            // If proportion of source image is wider then proportion of thumbnail image, then use full height of source image and crop the width.
            if ($src_ratio > $dst_ratio) {
                // KEEP HEIGHT, CROP WIDTH
                $src_w = $src_h * $dst_ratio;
                $src_x = floor(($src_width - $src_w) / 2);
            } else {
                // KEEP WIDTH, CROP HEIGHT
                $src_h = $src_w / $dst_ratio;
                $src_y = floor(($src_height - $src_h) / 2);
            }
            break;
        case "none2":
            // If proportion of source image is wider then proportion of thumbnail image, then use full height of source image and crop the width.
            if ($src_ratio > $dst_ratio) {
                // KEEP HEIGHT, CROP WIDTH
                $src_w = $src_h * $dst_ratio;
                $src_x = floor(($src_width - $src_w) / 2);
            } else {
                // KEEP WIDTH, CROP HEIGHT
                $src_h = $src_w / $dst_ratio;
                $src_y = floor(($src_height - $src_h) / 2);
            }
            break;
    }

    $dst_img = imagecreatetruecolor($dst_w, $dst_h);

    // PNG THUMBS WITH ALPHA PATCH
    // Turn off alpha blending and set alpha flag
    imagealphablending($dst_img, false);
    imagesavealpha($dst_img, true);


    imagecopyresampled($dst_img, $src_img, 0, 0, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);


    imagejpeg($dst_img, $thumb_file, 99);

    imagedestroy($dst_img);
    imagedestroy($src_img);

    return 1;
    /* $info = getimagesize($path); //получаем размеры картинки и ее тип
      $size = array($info[0], $info[1]); //закидываем размеры в массив
      //В зависимости от расширения картинки вызываем соответствующую функцию
      if ($info['mime'] == 'image/png') {
      $src = imagecreatefrompng($path); //создаём новое изображение из файла
      } else if ($info['mime'] == 'image/jpeg') {
      $src = imagecreatefromjpeg($path);
      } else if ($info['mime'] == 'image/gif') {
      $src = imagecreatefromgif($path);
      } else {
      return false;
      }

      $thumb = imagecreatetruecolor($width, $height); //возвращает идентификатор изображения, представляющий черное изображение заданного размера
      $src_aspect = $size[0] / $size[1]; //отношение ширины к высоте исходника
      $thumb_aspect = $width / $height; //отношение ширины к высоте аватарки

      if ($src_aspect < $thumb_aspect) {        //узкий вариант (фиксированная ширина)      $scale = $width / $size[0];         $new_size = array($width, $width / $src_aspect);        $src_pos = array(0, ($size[1] * $scale - $height) / $scale / 2); //Ищем расстояние по высоте от края картинки до начала картины после обрезки   } else if ($src_aspect > $thumb_aspect) {
      //широкий вариант (фиксированная высота)
      $scale = $height / $size[1];
      $new_size = array($height * $src_aspect, $height);
      $src_pos = array(($size[0] * $scale - $width) / $scale / 2, 0); //Ищем расстояние по ширине от края картинки до начала картины после обрезки
      } else {
      //другое
      $new_size = array($width, $height);
      $src_pos = array(0, 0);
      }

      $new_size[0] = max($new_size[0], 1);
      $new_size[1] = max($new_size[1], 1);

      imagecopyresampled($thumb, $src, 0, 0, $src_pos[0], $src_pos[1], $new_size[0], $new_size[1], $size[0], $size[1]);
      //Копирование и изменение размера изображения с ресемплированием

      if ($save === false) {
      return imagepng($thumb); //Выводит JPEG/PNG/GIF изображение
      } else {
      return imagepng($thumb, $save); //Сохраняет JPEG/PNG/GIF изображение
      } */
}

function prepareImg($data)
{
    global $SYSTEM;

    $path = substr(strchr($data['file'], '/tmp/'), 5);

    $file = str_replace('..', '', 'tmp/' . $path);
    $type = getExtension($file);
    switch ($type) {
        case 'jpg':
        case 'jpeg':
            $src_func = 'imagecreatefromjpeg';
            $write_func = 'imagejpeg';
            $image_quality = 100;
            break;
        case 'gif':
            $src_func = 'imagecreatefromgif';
            $write_func = 'imagegif';
            $image_quality = null;
            break;
        case 'png':
            $src_func = 'imagecreatefrompng';
            $write_func = 'imagejpeg';
            $image_quality = 100;
            $type = 'jpg';
            break;
        default:
            return false;
    }
    $src_img = $src_func($file);
    $coords = $data['coords'];

    if (!isset($coords['x1'])) {
        $coords['x1'] = $coords['x'];
        $coords['y1'] = $coords['y'];
    }

    $new_width = $coords['x2'] - $coords['x1'];
    $new_height = $coords['y2'] - $coords['y1'];

    $new_img = imagecreatetruecolor($new_width, $new_height);
    switch ($type) {
        case 'gif':
        case 'png':
            imagecolortransparent($new_img, imagecolorallocate($new_img, 0, 0, 0));
        case 'png':
            imagealphablending($new_img, false);
            imagesavealpha($new_img, true);
            break;
    }

    $new_file_path = 'tmp/' . microtime(true) . mt_rand(0, 999) . '.' . $type;

    list($width, $height) = getimagesize($file);
    $success = imagecopyresampled(
            $new_img, $src_img, 0, 0, $coords['x'], $coords['y'], $new_width, $new_height, $coords['w'], $coords['h']
        ) && $write_func($new_img, $new_file_path, $image_quality);
    if (isset($SYSTEM['post']['mini'])) {
        create_img($new_file_path, $new_file_path, 291, 327);
    } else {
        create_img($new_file_path, $new_file_path, 724, 453);
    }
    return $new_file_path;
}

function addComp($title, $condition, $scondition, $tcondition, $dcondition, $act, $site, $day, $desire, $images)
{
    global $USER;

    $title = query_escape($title);
    $condition = query_escape($condition);
    $scondition = query_escape($scondition);
    $tcondition = query_escape($tcondition);
    $dcondition = query_escape($dcondition);
    $act = query_escape(serialize($act));
    $site = query_escape(serialize($site));
    $end = time() + ($day * 3600 * 24);
    $desire = (int)$desire;
    $created = time();
    $images = query_escape(serialize($images));

    $uid = $USER['id'];
	
    query("INSERT INTO `nx_comps` (`title`,`condition`,`scondition`,`tcondition`,`dcondition`,`act`,`site`,`end`,`desire`,`created`,`images`,`user_id`, `upping`)"
        . "VALUES ('{$title}','{$condition}','{$scondition}','{$tcondition}','{$dcondition}','{$act}','{$site}','{$end}','{$desire}','{$created}','{$images}','{$uid}',UNIX_TIMESTAMP());");

    return query_lastInsertId();
}

function resetComp($action_id, $title, $condition, $scondition, $tcondition, $dcondition, $act, $site, $day, $desire, $images)
{
    global $USER;

    $title = query_escape($title);
    $condition = query_escape($condition);
    $scondition = query_escape($scondition);
    $tcondition = query_escape($tcondition);
    $dcondition = query_escape($dcondition);
    $act = query_escape(serialize($act));
    $site = query_escape(serialize($site));
    $end = time() + ($day * 3600 * 24);
    $desire = (int)$desire;
    $created = time();
    $images = query_escape(serialize($images));

    $uid = $USER['id'];

    query("UPDATE 
		`nx_comps` 
	SET 
		`title`='{$title}',
		`condition`='{$condition}',
		`scondition`='{$scondition}',
		`tcondition`='{$tcondition}',
		`dcondition`='{$dcondition}',
		`act`='{$act}',
		`site`='{$site}',
		`end`='{$end}',
		`desire`='{$desire}',
		`images`='{$images}'
	WHERE `comp_id`='$action_id';");

    return $action_id;
}


function uppingComp($action_id) {
    query("UPDATE `nx_comps` SET `upping`= UNIX_TIMESTAMP() WHERE `comp_id`='$action_id';");

    return $action_id;
}

function delComp($action_id) {
	$q = query("DELETE FROM `nx_comps` WHERE `comp_id`='".(int)$action_id."'");
}

function resetAction($action_id, $title, $condition, $site, $day, $desire, $graph, $phone, $address, $cost, $images, $cat, $city)
{
    global $USER;

    $action_id = (int)$action_id;
    $title = query_escape($title);
    $condition = query_escape($condition);
    $site = serialize(query_escape($site));
    $end = time() + ($day * 3600 * 24);
    $desire = (int)$desire;
    $graph = query_escape($graph);
    $phone = query_escape($phone);
    $address = query_escape($address);
    $cost = serialize(query_escape($cost));
    $created = time();
    $images = serialize(query_escape($images));
    $cat = query_escape(',' . implode(',', $cat) . ',');
    $city = query_escape(',' . implode(',', $city) . ',');

    $uid = $USER['id'];

    query("UPDATE `nx_actions` SET `title`='$title',`condition`='$condition',`site`='$site',`end`='$end',`desire`='$desire',`graph`='$graph',`phone`='$phone',`address`='$address',`cost`='$cost',`created`='$created',`images`='$images',`user_id`='$uid',`cat`='$cat',`city`='$city' WHERE `action_id`='$action_id';");

    return $action_id;
}

function getComp($id)
{
    $id = (int)$id;
    $q = query("SELECT * FROM `nx_comps` WHERE `comp_id`='{$id}'");

    return $q['row'];
}

function upLikeComp($id)
{
    query("UPDATE `nx_comps` SET `like`=`like`+1 WHERE `comp_id`='{$id}'");
}

function getCompByUid($id)
{
    $id = (int)$id;
    $q = query("SELECT * FROM `nx_comps` WHERE `user_id`='{$id}'");

    return $q['rows'];
}

function getComps($cat, $limit = false, $offset = 0)
{
    $cat = (int)$cat;
    $offset = (int)$offset;
    if ($limit !== false) {
        $limit = " LIMIT {$offset}, " . (int)$limit;
    } else {
        $limit = '';
    }
    if (!$cat) {
        $q = query("SELECT * FROM `nx_comps` WHERE `end` > UNIX_TIMESTAMP(NOW()) ORDER BY upping DESC" . $limit);
    } else {
        $q = query("SELECT * FROM `nx_comps` WHERE `end` > UNIX_TIMESTAMP(NOW()) ORDER BY upping DESC" . $limit);
    }

    return $q['rows'];
}

function searchComps($cat = array(), $q, $limit = false)
{
    $time = time();
    #$city = isset($_COOKIE['city']) ? (int)$_COOKIE['city'] : 0;
    $q = query_escape($q);
    if ($limit !== false) {
        $limit = " LIMIT 0, " . $limit;
    } else {
        $limit = '';
    }
    if (!$cat || ($cat[0] == 0)) {
        $qw = query("SELECT * FROM `nx_comps` WHERE (`title` LIKE '%{$q}%' OR `condition` LIKE '%{$q}%') ORDER BY `end` DESC" . $limit);
    } else {
        foreach ($cat as $k => $v) {
            $cat[$k] = " `cat` LIKE '%,{$v},%' ";
        }
        $qw = query("SELECT * FROM `nx_comps` WHERE (`title` LIKE '%{$q}%' OR `condition` LIKE '%{$q}%') ORDER BY `end` DESC " . $limit);
        #echo "SELECT * FROM `nx_actions` WHERE `end`>{$time} AND (`city` like '%,{$city},%') AND (".  implode(' OR ', $cat).") AND (`title` LIKE '%{$q}%' OR `condition` LIKE '%{$q}%') ".$limit;
    }

    return $qw['rows'];
}


function sortByCount($f1, $f2)
{
    if (count($f1) < count($f2))
        return 1;
    elseif (count($f1) > count($f2))
        return -1;
    else
        return 0;
}

function forOwnerReferal($owner_id, $amount)
{
    $owner_id = (int)$owner_id;
    $amount = (float)$amount;
    static $costs = array(5, 15, 40, 70);

    $user = getUserById($owner_id);
    if (!$amount || !$owner_id || !$user || $user['referal']) {
        return false;
    }
    $level = isVer($user, true);
    if (!isset($costs[$level])) {
        return false;
    }
    $proc = $costs[$level];
    $amount = ($amount / 100) * $proc;

    query("INSERT INTO `nx_balance_log` (`user_id`,`amount`,`type`,`created`,`comment`) VALUES('$owner_id','$amount','1','" . time() . "','Отчисления от реферала');");

    updateUserById($user['id'], array('money'), array(($user['money'] + $amount)));

    return true;
}

function getCounterContacts($user_id)
{
    $user_id = (int)$user_id;
    $cache = cache_get('ccontacts.' . $user_id);
    if (!$cache) {
        $q = query("SELECT * FROM `nx_contacts` WHERE `contact_id`='{$user_id}' AND `counter`=0;");
        $r = $q['rows'];
        $w = array();
        foreach ($r as $contact) {
            $w[$contact['owner_id']] = array($contact['owner_id'], $contact['counter']);
        }
        $r = $w;
        cache_set('ccontacts.' . $user_id, $r);
    } else {
        $r = $cache;
    }
    return $r;
}

function getNotices($offset = 0, $limit = 3, $onlyNew = false)
{
    global $USER;
    $uid = $USER['id'];
    $offset = (int)$offset;
    $limit = (int)$limit;
    $user_groups = getUserGroups($USER['id'], 'all', false, false);
    if (!$user_groups) {
        return array();
    }
    $groups = array();
    foreach ($user_groups as $group) {
        $groups[] = $group['nabs'];
    }
    $sql = '';
    if ($onlyNew) {
        $sql = " AND `wall_id`>'" . $USER['last_wid'] . "'";
    }
    $q = query("SELECT * FROM `nx_wall` WHERE `user_id` IN (" . implode(',', $groups) . ") AND `repost`=0 AND `deleted`=0 AND `owner_id`!='$uid' $sql ORDER BY `created` DESC LIMIT $offset,$limit");

    return $q['rows'];
}


function getCountNotices($onlyNew = false)
{
    global $USER;
    $uid = $USER['id'];
    $user_groups = getUserGroups($USER['id'], 'all', false, false);
    if (!$user_groups) {
        return 0;
    }
    $groups = array();
    foreach ($user_groups as $group) {
        $groups[] = $group['nabs'];
    }
    $sql = '';
    if ($onlyNew) {
        $sql = " AND `wall_id`>'" . $USER['last_wid'] . "'";
    }
    $q = query("SELECT COUNT(*) as `count` FROM `nx_wall` WHERE `user_id` IN (" . implode(',', $groups) . ") AND `repost`=0 AND `deleted`=0 AND `owner_id`!='$uid' $sql ORDER BY `created` DESC");

    return $q['row']['count'];
}


function getUserGroups($user_id, $type, $onlyAdmin = false, $getGroups = true)
{
    $user_id = (int)$user_id;
    $type = (int)$type;
    $q = query("SELECT *, `group_id`*-1 as `nabs` FROM `nx_members` WHERE `user_id`='$user_id' AND `type`>=0 ORDER BY `created` DESC;");
    $groups = array();
    if (!$getGroups) {
        return $q['rows'];
    }
    foreach ($q['rows'] as $group) {
        $group = getGroup($group['group_id'], $type);
        if ($group) {
            $groups[] = $group;
        }
    }
    return $groups;
}

function getGroup($group_id, $onlyType = false)
{
    $group_id = (int)abs($group_id);
    if ($onlyType !== false) {
        $onlyType = (int)$onlyType;
        $q = query("SELECT * FROM `nx_groups` WHERE `group_id`='$group_id' AND `type`='$onlyType';");
    } else {
        $q = query("SELECT * FROM `nx_groups` WHERE `group_id`='$group_id';");
    }
    return $q['row'];
}


function image_check_memory_usage($img, $max_breedte, $max_hoogte)
{
    if (file_exists($img)) {
        $K64 = 65536; // number of bytes in 64K
        $memory_usage = memory_get_usage();
        $memory_limit = abs(intval(str_replace('M', '', ini_get('memory_limit')) * 1024 * 1024));
        $image_properties = getimagesize($img);
        $image_width = $image_properties[0];
        $image_height = $image_properties[1];
        $image_bits = $image_properties['bits'];
        $image_memory_usage = $K64 + ($image_width * $image_height * ($image_bits) * 2);
        $thumb_memory_usage = $K64 + ($max_breedte * $max_hoogte * ($image_bits) * 2);
        $memory_needed = intval($memory_usage + $image_memory_usage + $thumb_memory_usage);

        if ($memory_needed > $memory_limit) {
            ini_set('memory_limit', (intval($memory_needed / 1024 / 1024) + 5) . 'M');
            if (ini_get('memory_limit') == (intval($memory_needed / 1024 / 1024) + 5) . 'M') {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function fix_strtoupper($str)
{
    if (function_exists('mb_strtoupper')) {
        return mb_strtoupper($str);
    } else {
        return strtoupper($str);
    }
}

/**
 * Correct strtolower handling
 *
 * @param  string $str
 *
 * @return  string
 */
function fix_strtolower($str)
{
    if (function_exists('mb_strtoupper')) {
        return mb_strtolower($str);
    } else {
        return strtolower($str);
    }
}

function create_img($imgfile, $imgthumb, $newwidth, $newheight = null, $option = "crop")
{
    $timeLimit = ini_get('max_execution_time');
    set_time_limit(30);
    $result = false;
    if (image_check_memory_usage($imgfile, $newwidth, $newheight)) {
        require_once('core/library/include/php_image_magician.php');
        $magicianObj = new imageLib($imgfile);
        $magicianObj->resizeImage($newwidth, $newheight, $option);
        if (strpos($imgthumb, '.png') !== false) {
            $magicianObj->saveImage($imgthumb, 9);
        } else {
            $magicianObj->saveImage($imgthumb, 100);
        }
        $result = true;
    }
    set_time_limit($timeLimit);

    return $result;
}


function soc_ico($soc, $url, $writeId = false, $data = array())
{
    static $socs = array(
        'vk' => '<i class="pe-so-vk pe-color"></i>',
        'fb' => '<i class="pe-so-facebook pe-color"></i>',
        'tw' => '<i class="pe-so-twitter pe-color"></i>',
        'pt' => '<i class="pe-so-pinterest pe-color"></i>',
        'od' => '<i class="pe-so-odnolassniki pe-color"></i>',
        'yo' => '<i class="pe-so-youtube-1 pe-color"></i>',
        'ia' => '<i class="pe-so-instagram pe-color"></i>',
        'gm' => '<i class="pe-so-gm pe-color"></i>',
        'www' => '<i class="pe-so-www pe-color"></i>',
    );
    $id = '';
    if ($writeId) {
        $id = 'id="' . $soc . '"';
    }
    if ($data) {
        $urls = array(
            'vk' => 'vk.com/share.php?title=' . urlencode($data[0]) . '&url=',
            'fb' => 'www.facebook.com/sharer/sharer.php?u=',
            'tw' => 'twitter.com/intent/tweet?text=' . urlencode($data[0]) . '&url=',
            'od' => 'connect.ok.ru/dk?st.cmd=WidgetSharePreview&service=odnoklassniki&st.shareUrl='
        );
        if (isset($urls[$soc])) {
            $url = $urls[$soc] . urlencode($data[1]);
        }
    }
    return '<a class="iconSoc" ' . $id . ' href="//' . $url . '" target="_blank">' . $socs[$soc] . '</a>';
}

function addMember($comp_id, $vk_id=0)
{
    global $USER;
    $comp_id = (int)$comp_id;
    $vk_id = (int)$vk_id;
    $uid = $USER['id'];

    $q = query("SELECT * FROM `nx_comp_members` WHERE `comp_id`='$comp_id' AND (`vk_id`='$vk_id' OR (`user_id`='$uid' AND `user_id`!=0))");
    if (!$q['row']) {
        query("INSERT INTO `nx_comp_members` (`vk_id`,`user_id`,`comp_id`) VALUES ('$vk_id','$uid','$comp_id')");
    }
}

function getMembers($comp_id)
{
    $comp_id = (int)$comp_id;
    $q = query("SELECT * FROM `nx_comp_members` WHERE `comp_id`='$comp_id' ORDER BY `created` ASC;");

    return $q['rows'];
}

function winner($comp_id, $id)
{
    $comp_id = (int)$comp_id;
    $id = (int)$id;
    query("UPDATE `nx_comp_members` SET `winner`=0 WHERE `comp_id`='$comp_id';");
    query("UPDATE `nx_comp_members` SET `winner`=1 WHERE `member_id`='$id' AND `comp_id`='$comp_id';");
}

function socs($type, $data)
{
    global $USER;
    $uid = $USER['id'];
    $data_ = query_escape(serialize($data));
    if ($type == 'vk') {
        $vk_id = (int)$data['id'];
        if ($uid) {
            $q = query("SELECT * FROM `socs` WHERE `vk_id`='$vk_id';");
            if ($q['row']) {
                if (!$q['row']['user_id']) {
                    query("UPDATE `socs` SET `user_id`='$uid' WHERE `vk_id`='$vk_id';");
                }
            } else {
                query("INSERT INTO `socs` (`vk_id`,`vk_data`,`user_id`) VALUES ('$vk_id','$data_','$uid');");
            }
        } else {
            query("INSERT INTO `socs` (`vk_id`,`vk_data`,`user_id`) VALUES ('$vk_id','$data_','$uid');");
        }
    }
}

function getVkUser($vk_id)
{
    $vk_id = (int)$vk_id;
    $q = query("SELECT * FROM `socs` WHERE `vk_id`='$vk_id';");
    if (!$q['row']) {
        return array();
    }
    $q['row']['vk_data'] = unserialize($q['row']['vk_data']);
    $_ = $q['row'];
    if ($q['row']['user_id']) {
        $q['row'] = getUserById($q['row']['user_id']);
        $q['row']['first_name'] = $q['row']['name'];
        $q['row']['last_name'] = $q['row']['fam'];
    } else {
        $q['row']['first_name'] = $q['row']['vk_data']['first_name'];
        $q['row']['last_name'] = $q['row']['vk_data']['last_name'];
    }
    $q['row']['vk'] = $_;
    return $q['row'];
}
