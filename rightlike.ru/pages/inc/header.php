<?php
if (isset($_GET['qCategories'])) {
    $qCat = explode(',', urldecode($_GET['qCategories']));
    $qCat = array_flip($qCat);
} else {
    $qCat = array();
}

$counterContacts = getCounterContacts($USER['id']);
$counterContacts_ = '';
foreach ($counterContacts as $k => $contact) {
    if ($contact[1] != 0) {
        unset($counterContacts[$k]);
        continue;
    }
    $contact = getUserById($contact[0]);
    $counterContacts_ .= '<div class="popup_row">
                            <div class="popup_col_img"><a href="//gm1lp.ru/id' . $contact['id'] . '"><img class="rounded_3" src="//gm1lp.ru' . '/uploads/avatars/' . $contact['ava'] . '" width="59" height="59"></a></div>
                            <div class="popup_col_text">
                                <span class="title"><a href="#">' . $contact['name'] . ' ' . $contact['fam'] . '</a></span>
                                <button class="btn btn-size-small rounded_3 addToContact" data-id="' . $contact['id'] . '">Установить контакт</button>
                                <button class="btn btn-size-small rounded_3 btn-color-pink unContact" data-id="' . $contact['id'] . '">Отклонить заявку</button>
                            </div>
                        </div>';
}
$notices = getNotices(0, 3, true);
$cNotices = getCountNotices(true);
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo isset($title) ? $title : ''; ?></title>

    <meta charset="utf-8">
    <meta name="description" content="<?php echo isset($desc) ? $desc : 'GM Card'; ?>"/>
    <meta name="keywords" content="<?php echo isset($key) ? $key : 'GM Card'; ?>"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.png" type="image/png">

    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

    <!--link rel="stylesheet" type="text/css" href="//gm1lp.ru/css/style.css"/-->
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style_action.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style_external.css"/>
    <link rel="stylesheet" type="text/css" href="/css/jquery.Jcrop.min.css">

    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/js/placeholders.jquery.min.js"></script>

    <script type="text/javascript" src="/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/js/jquery.Jcrop.min.js"></script>

    <script type="text/javascript" src="/js/jquery.autosize.min.js"></script>

    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript" src="/js/nx.js"></script>
    <?php
    echo $head;
    ?>
    <link rel="stylesheet" type="text/css" href="/css/nx.css">
    <link rel="stylesheet" type="text/css" href="/css/overwrite.css">
</head>
<body class="action <?php echo $body_class; ?>">
<!--header-->
<div id="header">
    <div class="main_block centred header_wrapper">
        <div class="main_block top_block">
            <div class="tb_part tb_sep_r tb_logo">
                <a href="/"><img src="/images/logo_small.png"><span>Конкурсы</span></a>
            </div>

            <div popup-target="popup_services" popup-pos="right" class="item_popup_menu tb_part tb_btn tb_services">
                <span>Все сервисы GM</span>
            </div>
			<div class="item_popup_menu tb_part tb_services" style="padding-top: 6px;"><!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?117"></script>
<script type="text/javascript">
  VK.init({apiId: 5079102, onlyWidgets: true});
</script>
<!-- Put this div tag to the place, where the Like block will be -->
<div id="vk_like"></div>
<script type="text/javascript">
VK.Widgets.Like("vk_like", {type: "button"});
</script></div>

            <?php
            if ($USER):
                ?>
                <div popup-target="popup_profile" popup-pos="left" class="item_popup_menu tb_part_r tb_btn tb_profile"><img src="//gm1lp.ru<?php echo '/uploads/avatars/' . $USER['ava']; ?>" class="rounded_3 mini-avatar"/></div>
                <div popup-target="popup_friends" popup-pos="left"
                     class="item_popup_menu tb_part_r tb_btn tb_ico tb_sep_r tb_friends">
                    <div class="tb_amount"><?php echo count($counterContacts); ?></div>
                </div>
                <div popup-target="popup_notice" popup-pos="left"
                     class="item_popup_menu tb_part_r tb_btn tb_ico tb_sep_r tb_notice">
                    <div class="tb_amount"><?php echo $cNotices; ?></div>
                </div>
                <div popup-target="popup_messages" popup-pos="left"
                     class="item_popup_menu tb_part_r tb_btn tb_ico tb_sep_l tb_sep_r tb_messages">
                    <div class="tb_amount"></div>

                </div>
                <div class="tb_part_r tb_actbtn">
                    <form action="/action/create">
                        <button class="btn btn-color-lblue btn-size-small rounded_5">Запустить конкурс</button>
                    </form>
                </div>
                <div class="tb_part_r tb_lcabinet"><a href="/user/lk">Личный кабинет</a></div>
            <?php else: ?>
                <script type="text/javascript">
                    jQuery(function($){
                        $.ajax({
                            data: {authorize: true},
                            dataType: 'json',
                            method: 'POST',
                            success: function(data){
                                if(data.status == 'OK'){
                                    window.location.href = '/?session=' + data.session + '&hash=' + data.hash;
                                }
                            },
                            url: 'https://gm1lp.ru/',
                            xhrFields: {
                                withCredentials: true
                            }
                        });
                    });
                </script>
                <div class="item_popup_menu tb_part_r tb_profile"><img src="/images/not-authorized-logo.png"/></div>

                <div popup-target="popup_registration" popup-pos="left" class="tb_part_r tb_sep_r item_popup_menu tb_actbtn">
                    <button class="btn btn-color-lblue btn-size-small rounded_5">Регистрация</button>
                </div>
                <div class="tb_part_r tb_lcabinet tb_btn"><a href="//gm1lp.ru/page/login?referer=rightlike.ru">Вход</a></div>

                <div class="tb_part_r tb_sep_r tb_actbtn">
                    <button class="btn btn-color-lyellow btn-size-small rounded_5">Запустить конкурс</button>
                </div>
            <?php endif; ?>
            <!-- POPUPS -->
            <!--  -->
            <div id="popup_searchlocation" class="popup popup_searchlocation styled_checkbox">
                <input type="checkbox" name="slocation[]" id="sl_0" class="checkbox_all" value="0" <?php echo isset($qCat[0]) ? ' checked' : ''; ?>> <label for="sl_0">Везде</label>
                <!--input type="checkbox" name="slocation[]" id="sl_1" class="checkbox_all" value="1" <?php echo isset($qCat[1]) ? ' checked' : ''; ?>> <label for="sl_1">Для нее</label>
                <input type="checkbox" name="slocation[]" id="sl_2" class="checkbox_all" value="2" <?php echo isset($qCat[2]) ? ' checked' : ''; ?>> <label for="sl_2">Для него</label>
                <input type="checkbox" name="slocation[]" id="sl_3" class="checkbox_all" value="3" <?php echo isset($qCat[3]) ? ' checked' : ''; ?>> <label for="sl_3">Детям</label>
                <input type="checkbox" name="slocation[]" id="sl_4" class="checkbox_all" value="4" <?php echo isset($qCat[4]) ? ' checked' : ''; ?>> <label for="sl_4">Для дома</label>
                <input type="checkbox" name="slocation[]" id="sl_5" class="checkbox_all" value="5" <?php echo isset($qCat[5]) ? ' checked' : ''; ?>> <label for="sl_5">Техника</label>
                <input type="checkbox" name="slocation[]" id="sl_6" class="checkbox_all" value="6" <?php echo isset($qCat[6]) ? ' checked' : ''; ?>> <label for="sl_6">Красота</label>
                <input type="checkbox" name="slocation[]" id="sl_7" class="checkbox_all" value="7" <?php echo isset($qCat[7]) ? ' checked' : ''; ?>> <label for="sl_7">Услуги</label>
                <input type="checkbox" name="slocation[]" id="sl_8" class="checkbox_all" value="8" <?php echo isset($qCat[8]) ? ' checked' : ''; ?>> <label for="sl_8">Отели</label>
                <input type="checkbox" name="slocation[]" id="sl_9" class="checkbox_all" value="9" <?php echo isset($qCat[9]) ? ' checked' : ''; ?>> <label for="sl_9">Туры</label>
                <input type="checkbox" name="slocation[]" id="sl_10" class="checkbox_all" value="10" <?php echo isset($qCat[10]) ? ' checked' : ''; ?>> <label for="sl_10">Магазины</label>
                <input type="checkbox" name="slocation[]" id="sl_11" class="checkbox_all" value="11" <?php echo isset($qCat[11]) ? ' checked' : ''; ?>> <label for="sl_11">Кафе/Бары</label>-->
            </div>

            <?php
            if ($USER):
                ?>
                <!-- Profile -->
                <div class="popup tb_popup popup_profile" id="popup_profile">
                    <div class="popup_row header">
                        <div class="popup_left">
                            <img src="//gm1lp.ru<?php echo '/uploads/avatars/' . $USER['ava']; ?>" class="mini-avatar">
                            <span><?php echo $USER['name'] . ' ' . $USER['fam']; ?></span>
                        </div>
                        <div class="popup_right"><a href="/page/logout">Выход</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row">
                        <div class="popup_left"><span class="popup_ico popup_ico_profile">Профиль</span></div>
                        <div class="popup_right"><a href="#" onclick="confirm('Редактирование профиля происходит на главном сайте.  Подтверждаете переход?') ? window.location.href = 'http://gm1lp.ru/user/profile' : '';
                                        return false;">Редактировать</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row">
                        <div class="popup_left"><span class="popup_ico popup_ico_balance">Баланс</span> <span class="popup_hlighted"><?php echo $USER['money']; ?> руб.</span></div>
                        <div class="popup_right"><a href="#" onclick="confirm('Пополнение счета происходит на главном сайте. Подтверждаете переход?') ? window.location.href = 'http://gm1lp.ru/user/profile' : '';
                                        return false;">Оперировать</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row">
                        <div class="popup_left"><span class="popup_ico popup_ico_balance">Учетная запись</span> <span class="popup_hlighted">«<?php echo isVer($USER); ?>»</span></div>
                        <div class="popup_right"><a href="//gm1lp.ru/page/tariff" target="_blank">Повысить</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row">
                        <div class="popup_left"><span class="popup_ico popup_ico_settings">Настройки и конфиденциальность</span></div>
                        <div class="popup_right"><a href="//gm1lp.ru/private">Проверить</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup_row last">
                        <div class="popup_left"><span class="popup_ico popup_ico_help">Справка</span></div>
                        <div class="popup_right"><a href="//gm1lp.ru/help/index">Получить помощь</a></div>
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Notices -->
                <div class="popup tb_popup popup_notice" id="popup_notice">
                    <?php
                    if (!$cNotices) {
                        echo 'Новых новостей нет';
                    }
                    for ($i = 0; $i < $cNotices; $i++) {
                        $group = getGroup($notices[$i]['user_id']);
                        $text = strip_tags($notices[$i]['text']);
                        if (mb_strlen($text) > 256) {
                            $text = mb_substr($text, 0, 256) . '...';
                        }
                        $text = trim($text);
                        if (empty($text)) {
                            $text = '<b>файл</b>';
                        }
                        echo '<div class="popup_row ' . ($i + 1 == $cNotices ? 'last' : '') . '">
                    <a class="cover" href="https://gm1lp.ru/feed#post' . $notices[$i]['wall_id'] . '"></a>
                    <div class="popup_toolbox"><!--a href="#"><img src="/images/icon-popup-close.png"/></a--></div>
                    <div class="popup_col_img"><a href="https://gm1lp.ru/' . $group['url'] . '"><img class="rounded_3 width48" src="https://gm1lp.ru//uploads/g_avatars/' . $group['ava'] . '"></a>
                    </div>
                    <div class="popup_col_text">
                        <span class="title"><a href="https://gm1lp.ru/feed#post' . $notices[$i]['wall_id'] . '">' . $group['title'] . '</a></span><br/>
                        <span class="info">Опубликовал(а):</span> <a href="https://gm1lp.ru/feed#post' . $notices[$i]['wall_id'] . '">' . $text . '</a>
                    </div>
                </div>';
                    }
                    ?>
                </div>

                <!-- Messages -->
                <div class="popup tb_popup popup_messages" id="popup_messages">
                </div>
                <!-- Friends -->
                <div class="popup tb_popup popup_friends" id="popup_friends">
                    <?php echo $counterContacts_ ? $counterContacts_ : 'Входящих заявок нет'; ?>
                </div>
            <?php else: ?>
                <!-- Registration -->
                <div class="popup tb_popup popup_registration" id="popup_registration">
                    <p class="reg_label">Зарегистрироваться</p>

                    <p>
                        У нас единая система сервисов, а т.е., для всех сервисов системы GM действует 1 логин и 1 пароль,
                        которые вы указываете при регистрации в системе GM, на главном ресурсе - www.gm1lp.ru
                    </p>

                    <form action="//gm1lp.ru/page/login?referer=rightlike.ru%2Fpage%2Flogin" style="display: inline-block;">
                        <button class="btn rounded_3">Регистрация</button>
                    </form>
                    <button class="btn btn-type-link btn-style-dotted" onclick="$('body').click();"><span>Пройду позже</span></button>
                </div>

                <!-- Login -->
                <div class="popup tb_popup popup_auth" id="popup_auth">
                    <label>Логин</label>
                    <input type="text" class="inp inp_full inp-size-small rounded_3" placeholder="Ваша почта"/>

                    <label>Пароль</label>
                    <input type="text" class="inp inp_full inp-size-small rounded_3" placeholder="Ваш пароль"/>

                    <button class="btn rounded_3">Войти</button>
                    <button class="btn btn-type-link btn-style-dotted btn-forget"><span>Не помню пароль</span></button>
                </div>
            <?php endif; ?>
            <!-- Services -->
            <div class="popup tb_popup popup_services" id="popup_services">
                <div class="popup_row">
                    <div class="popup_col_label">GM1LP</div>
                    <div class="popup_col_text">
                        <a href="//gm1lp.ru/">вернуться домой</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_col_label">Скидки</div>
                    <div class="popup_col_text">
                        <a href="//gmcard.ru/">посмотреть</a> или <a href="//gmcard.ru/action/create">запустить</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_col_label">Конкурсы</div>
                    <div class="popup_col_text">
                        <a href="//rightlike.ru/">участвовать</a> или <a href="//rightlike.ru/action/create">создать</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row">
                    <div class="popup_col_label">Задания</div>
                    <div class="popup_col_text">
                        <a href="//goodmoneys.ru">заработать</a> или <a href="//goodmoneys.ru">дать задание</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Блэк лист</div>
                    <div class="popup_col_text">
                        проверить себя или исполнителя
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">P2P</div>
                    <div class="popup_col_text">
                        одолжить денег под % или погасить долг
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Бартер</div>
                    <div class="popup_col_text">
                        услуга за услугу, товар за товар
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Краудфандинг</div>
                    <div class="popup_col_text">
                        спонсировать или привлечь средства
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row disabled">
                    <div class="popup_col_label">Аренда</div>
                    <div class="popup_col_text">
                        сдать или взять вещь в аренду
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup_row last disabled">
                    <div class="popup_col_label">ВКонкурсе</div>
                    <div class="popup_col_text">
                        выбрать победителя
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <!-- Menus -->


            <div class="popup popup_menu" id="popup_menu_forwomen">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=1">Одежда</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=2">Большие размеры</a></li>
                        <li><a href="/?category=3">Брюки</a></li>
                        <li><a href="/?category=4">Верхняя одежда</a></li>
                        <li><a href="/?category=5">Джемперы, свитеры</a></li>
                        <li><a href="/?category=6">Джинсы</a></li>
                        <li><a href="/?category=7">Костюмы, пиджаки</a></li>
                        <li><a href="/?category=8">Кофты, водолазки</a></li>
                        <li><a href="/?category=9">Одежда для дома</a></li>
                        <li><a href="/?category=10">Платья</a></li>
                        <li><a href="/?category=11">Спорт</a></li>
                        <li><a href="/?category=12">Толстовки</a></li>
                        <li><a href="/?category=13">Туники</a></li>
                        <li><a href="/?category=14">Футболки, поло</a></li>
                        <li><a href="/?category=15">Шорты</a></li>
                        <li><a href="/?category=16">Юбки</a></li>
                        <li><a href="/?category=17">Блузки, рубашки</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="?category_id=345">Спорт</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=346">Абонементы</a></li>
                        <li><a href="?category_id=347">Тренажерные залы</a></li>
                        <li><a href="?category_id=348">Плавание</a></li>
                        <li><a href="?category_id=349">Танцы</a></li>
                        <li><a href="?category_id=350">Пластика</a></li>
                        <li><a href="?category_id=351">Спортивное питание</a></li>
                        <li><a href="?category_id=352">Аминокислоты</a></li>
                        <li><a href="?category_id=353">Трежеры</a></li>
                        <li><a href="?category_id=354">Спортивное оборудование</a></li>
                        <li><a href="?category_id=355">Одежда для тренировок</a></li>
                        <li><a href="?category_id=356">Индивидуальная защита <br> от травм</a></li>
                        <li><a href="?category_id=357">Индивидуальный тренер</a></li>
                    </ul>

                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=19">Обувь</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=20">Сапоги</a></li>
                        <li><a href="/?category=21">Ботинки</a></li>
                        <li><a href="/?category=22">Балетки</a></li>
                        <li><a href="/?category=23">Босоножки</a></li>
                        <li><a href="/?category=24">Ботильоны</a></li>
                        <li><a href="/?category=25">Кеды, кроссовки</a></li>
                        <li><a href="/?category=26">Мокасины</a></li>
                        <li><a href="/?category=27">Тапочки</a></li>
                        <li><a href="/?category=28">Туфли</a></li>
                        <li><a href="/?category=29">Шлепанцы, сандалии</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=30">Для авто</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=31">Запчасти и аксессуары</a></li>
                        <li><a href="/?category=32">Навигаторы</a></li>
                        <li><a href="/?category=33">Тонировка</a></li>
                        <li><a href="/?category=34">Карбоновая пленка</a></li>
                        <li><a href="/?category=35">Регистраторы</a></li>
                        <li><a href="/?category=36">Сигнализация</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=18">Прочее</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=37">Белье</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=38">Колготки, носки</a></li>
                        <li><a href="/?category=39">Купальники</a></li>
                        <li><a href="/?category=40">Эротическое белье</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=41">Аксессуары</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=42">Бижутерия</a></li>
                        <li><a href="/?category=43">Головные уборы</a></li>
                        <li><a href="/?category=44">Зонты</a></li>
                        <li><a href="/?category=45">Кошельки</a></li>
                        <li><a href="/?category=46">Очки</a></li>
                        <li><a href="/?category=47">Перчатки</a></li>
                        <li><a href="/?category=48">Платки, шарфы</a></li>
                        <li><a href="/?category=49">Ремни</a></li>
                        <li><a href="/?category=50">Сумки, клатчи</a></li>
                        <li><a href="/?category=51">Часы</a></li>
                        <li><a href="?category_id=335">Золотые украшения</a></li>
                        <li><a href="?category_id=336">Серьги</a></li>
                        <li><a href="?category_id=337">Кольца</a></li>
                        <li><a href="?category_id=338">Цепи и браслеты</a></li>
                        <li><a href="?category_id=339">Цепочки</a></li>
                        <li><a href="?category_id=340">Шкатулки</a></li>
                        <li><a href="?category_id=341">Броши</a></li>
                        <li><a href="?category_id=342">Калье</a></li>
                        <li><a href="?category_id=343">Подвески</a></li>
                        <li><a href="?category_id=344">Кресты и иконки</a></li>
                    </ul>

                </div>
                <div class="clear"></div>
            </div>
            <!-- forMen -->
            <div class="popup popup_menu" id="popup_menu_formen">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=52">Одежда</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=53">Брюки</a></li>
                        <li><a href="/?category=54">Верхняя одежда</a></li>
                        <li><a href="/?category=55">Джемперы, свитеры</a></li>
                        <li><a href="/?category=56">Джинсы</a></li>
                        <li><a href="/?category=57">Костюмы, пиджаки</a></li>
                        <li><a href="/?category=58">Одежда для дома</a></li>
                        <li><a href="/?category=59">Рубашки</a></li>
                        <li><a href="/?category=60">Спорт</a></li>
                        <li><a href="/?category=61">Толстовки</a></li>
                        <li><a href="/?category=62">Футболки, поло</a></li>
                        <li><a href="/?category=63">Шорты</a></li>
                    </ul>

                    <h3 class="cat_link first"><a href="?category_id=534">Спорт</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=373">Абонементы</a></li>
                        <li><a href="?category_id=374">Тренажерные залы</a></li>
                        <li><a href="?category_id=375">Плавание</a></li>
                        <li><a href="?category_id=376">Танцы</a></li>
                        <li><a href="?category_id=377">Пластика</a></li>
                        <li><a href="?category_id=378">Спортивное питание</a></li>
                        <li><a href="?category_id=379">Аминокислоты</a></li>
                        <li><a href="?category_id=380">Тренажеры</a></li>
                        <li><a href="?category_id=381">Спортивное оборудование</a></li>
                        <li><a href="?category_id=382">Одежда для тренировок</a></li>
                        <li><a href="?category_id=383">Индивидуальная защита<br> от травм</a></li>
                        <li><a href="?category_id=384">Индивидуальный тренер</a></li>
                    </ul>
                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=65">Обувь</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=66">Ботинки</a></li>
                        <li><a href="/?category=67">Кеды, кроссовки</a></li>
                        <li><a href="/?category=68">Мокасины</a></li>
                        <li><a href="/?category=69">Сапоги</a></li>
                        <li><a href="/?category=70">Тапочки</a></li>
                        <li><a href="/?category=71">Туфли</a></li>
                        <li><a href="/?category=72">Шлепанцы, сандалии</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=73">Для авто</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=74">Запчасти и аксессуары</a></li>
                        <li><a href="/?category=75">Навигаторы</a></li>
                        <li><a href="/?category=76">Тонировка</a></li>
                        <li><a href="/?category=77">Карбоновая пленка</a></li>
                        <li><a href="/?category=78">Регистраторы</a></li>
                        <li><a href="/?category=79">Сигнализация</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=64">Прочее</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=80">Белье</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=81">Нижнее белье</a></li>
                        <li><a href="?category_id=358">Эротическое белье</a></li>
                        <li><a href="?category_id=359">Плавки</a></li>
                        <li><a href="/?category=82">Носки</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=83">Аксессуары</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=360">Бижутерия</a></li>
                        <li><a href="/?category=85">Головные уборы</a></li>
                        <li><a href="/?category=86">Зонты</a></li>
                        <li><a href="?category_id=361">Кошельки</a></li>
                        <li><a href="/?category=87">Очки</a></li>
                        <li><a href="/?category=88">Перчатки</a></li>
                        <li><a href="?category_id=362">Платки, шарфы</a></li>
                        <li><a href="/?category=90">Ремни</a></li>
                        <li><a href="/?category=91">Сумки, клатчи</a></li>
                        <li><a href="/?category=92">Часы</a></li>
                        <li><a href="?category_id=363">Золотые украшения</a></li>
                        <li><a href="?category_id=364">Серьги</a></li>
                        <li><a href="?category_id=365">Кольца</a></li>
                        <li><a href="?category_id=366">Цепи и браслеты</a></li>
                        <li><a href="?category_id=367">Цепочки</a></li>
                        <li><a href="?category_id=368">Шкатулки</a></li>
                        <li><a href="?category_id=369">Броши</a></li>
                        <li><a href="?category_id=370">Калье</a></li>
                        <li><a href="?category_id=371">Подвески</a></li>
                        <li><a href="?category_id=372">Кресты и иконки</a></li>
                    </ul>

                </div>
                <div class="clear"></div>
            </div>
            <!---------------

for Kids

------------>
            <div class="popup popup_menu" id="popup_menu_forkid">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=93">Новорожденным</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=94">Боди, песочники</a></li>
                        <li><a href="/?category=95">Верхняя одежда</a></li>
                        <li><a href="/?category=96">Комбинезоны</a></li>
                        <li><a href="/?category=97">Ползунки, штанишки</a></li>
                        <li><a href="/?category=98">Распашонки, кофты</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=99">Игрушки</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=100">Деревянные игрушки</a></li>
                        <li><a href="/?category=101">Игровые наборы</a></li>
                        <li><a href="/?category=102">Конструкторы</a></li>
                        <li><a href="/?category=103">Куклы и аксессуары</a></li>
                        <li><a href="/?category=104">Машинки, ж/д дороги</a></li>
                        <li><a href="/?category=105">Музыкальные игрушки</a></li>
                        <li><a href="/?category=106">Мягкие игрушки</a></li>
                        <li><a href="/?category=107">Наборы для творчества</a></li>
                        <li><a href="/?category=108">Настольные игры</a></li>
                        <li><a href="/?category=109">Радиоуправляемые игрушки</a></li>
                        <li><a href="/?category=110">Развивающие игрушки</a></li>
                        <li><a href="/?category=111">Роботы, трансформеры</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=112">Для детской</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=113">Прочее</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=114">Спорт</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=115">Одежда для мальчиков</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=116">Рубашки</a></li>
                        <li><a href="/?category=117">Белье, носки, колготки</a></li>
                        <li><a href="/?category=118">Верхняя одежда</a></li>
                        <li><a href="/?category=119">Джинсы, брюки</a></li>
                        <li><a href="/?category=120">Для дома и отдыха</a></li>
                        <li><a href="/?category=121">Костюмы, комбинезоны</a></li>
                        <li><a href="/?category=122">Кофты, джемперы, свитеры</a></li>
                        <li><a href="/?category=123">Футболки, шорты</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=124">Обувь для мальчиков</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=125">Ботинки, полуботинки</a></li>
                        <li><a href="/?category=126">Валенки</a></li>
                        <li><a href="/?category=127">Кеды</a></li>
                        <li><a href="/?category=128">Кроссовки</a></li>
                        <li><a href="/?category=129">Мокасины</a></li>
                        <li><a href="/?category=130">Орто-обувь</a></li>
                        <li><a href="/?category=131">Пинетки, первый шаг</a></li>
                        <li><a href="/?category=132">Резиновые сапоги</a></li>
                        <li><a href="/?category=133">Сандалии</a></li>
                        <li><a href="/?category=134">Сапоги</a></li>
                        <li><a href="/?category=135">Сланцы</a></li>
                        <li><a href="/?category=136">Тапочки</a></li>
                        <li><a href="/?category=137">Туфли</a></li>
                        <li><a href="/?category=138">Угги, унты</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=139">Аксессуары</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=140">Одежда для девочек</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=141">Верхняя одежда</a></li>
                        <li><a href="/?category=142">Белье, носки, колготки</a></li>
                        <li><a href="/?category=143">Джинсы, брюки</a></li>
                        <li><a href="/?category=144">Для дома и отдыха</a></li>
                        <li><a href="/?category=145">Костюмы, комбинезоны</a></li>
                        <li><a href="/?category=146">Кофты, джемперы, свитеры</a></li>
                        <li><a href="/?category=147">Платья, блузки</a></li>
                        <li><a href="/?category=148">Футболки, топы</a></li>
                        <li><a href="/?category=149">Юбки, шорты</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=150">Обувь для девочек</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=151">Ботинки, полуботинки</a></li>
                        <li><a href="/?category=152">Валенки</a></li>
                        <li><a href="/?category=153">Кеды</a></li>
                        <li><a href="/?category=154">Кроссовки</a></li>
                        <li><a href="/?category=155">Мокасины</a></li>
                        <li><a href="/?category=156">Орто-обувь</a></li>
                        <li><a href="/?category=157">Пинетки, первый шаг</a></li>
                        <li><a href="/?category=158">Резиновые сапоги</a></li>
                        <li><a href="/?category=159">Сандалии, босоножки</a></li>
                        <li><a href="/?category=160">Сапоги</a></li>
                        <li><a href="/?category=161">Сланцы</a></li>
                        <li><a href="/?category=162">Тапочки, балетки</a></li>
                        <li><a href="/?category=163">Туфли</a></li>
                        <li><a href="/?category=164">Угги, унты</a></li>
                    </ul>
                </div>
            </div>
            <!---------------------------
            for home
            ------------------------------>
            <div class="popup popup_menu" id="popup_menu_forhome">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=165">Спальня</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=166">Детское постельное белье</a></li>
                        <li><a href="/?category=167">Наматрасники</a></li>
                        <li><a href="/?category=168">Одеяла</a></li>
                        <li><a href="/?category=169">Подушки</a></li>
                        <li><a href="/?category=170">Покрывала и пледы</a></li>
                        <li><a href="/?category=171">Постельное белье</a></li>
                        <li><a href="/?category=172">Рулонные шторы и жалюзи</a></li>
                        <li><a href="/?category=173">Шторы и гардины</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=174">Гостиная</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=175">Вазы, кашпо и горшки</a></li>
                        <li><a href="/?category=176">Декоративные подушки</a></li>
                        <li><a href="/?category=177">Декоретто и фотообои</a></li>
                        <li><a href="/?category=178">Зеркала</a></li>
                        <li><a href="/?category=179">Картины и постеры</a></li>
                        <li><a href="/?category=180">Покрывала и пледы</a></li>
                        <li><a href="/?category=181">Рулонные шторы и жалюзи</a></li>
                        <li><a href="/?category=182">Стильные мелочи</a></li>
                        <li><a href="/?category=183">Фоторамки</a></li>
                        <li><a href="/?category=184">Часы</a></li>
                        <li><a href="/?category=185">Шкатулки</a></li>
                        <li><a href="/?category=186">Шторы и гардины</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="?category_id=535">Все для ремонта</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=399">Обои</a></li>
                        <li><a href="?category_id=400">Люстры</a></li>
                        <li><a href="?category_id=401">Краска</a></li>
                        <li><a href="?category_id=402">Расходные материалы</a></li>
                        <li><a href="?category_id=403">Кафель</a></li>
                        <li><a href="?category_id=404">Забор</a></li>
                        <li><a href="?category_id=405">Счетчики</a></li>
                        <li><a href="?category_id=406">Отделочные материалы</a></li>
                        <li><a href="?category_id=407">Деревянные срубы</a></li>
                        <li><a href="?category_id=408">Решетки на окна</a></li>
                        <li><a href="?category_id=409">Двери</a></li>
                        <li><a href="?category_id=410">Пластиковые окна</a></li>
                        <li><a href="?category_id=411">Инструменты</a></li>
                        <li><a href="?category_id=412">Станки</a></li>
                        <li><a href="?category_id=413">Прочее</a></li>
                    </ul>
                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=187">Кухня</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=188">Полотенца и фартуки</a></li>
                        <li><a href="/?category=189">Скатерти</a></li>
                        <li><a href="/?category=190">Сковороды</a></li>
                        <li><a href="/?category=191">Столовые приборы</a></li>
                        <li><a href="/?category=192">Хранение</a></li>
                        <li><a href="/?category=193">Шторы и занавески</a></li>
                        <li><a href="/?category=194">Ванная комната</a></li>
                        <li><a href="/?category=195">Декоративные мелочи</a></li>
                        <li><a href="/?category=196">Для стирки, глажки и сушки</a></li>
                        <li><a href="/?category=197">Для уборки</a></li>
                        <li><a href="/?category=198">Коврики</a></li>
                        <li><a href="/?category=199">Полотенца</a></li>
                        <li><a href="/?category=200">Штора для ванной</a></li>
                        <li><a href="/?category=201">Кастрюли и казаны</a></li>
                        <li><a href="/?category=202">Кухонная утварь и аксессуары</a></li>
                        <li><a href="/?category=203">Наборы посуды</a></li>
                        <li><a href="/?category=204">Наборы посуды для сервировки</a></li>
                        <li><a href="/?category=205">Ножи и разделочные доски</a></li>
                        <li><a href="/?category=206">Овощерезки и терки</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=207">Мебель, свет и дача</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=208">Дача</a></li>
                        <li><a href="/?category=209">Мебель</a></li>
                        <li><a href="/?category=210">Свет</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=218">Гардеробная</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=219">Системы для хранения белья</a></li>
                        <li><a href="/?category=220">Системы для хранения обуви</a></li>
                        <li><a href="/?category=221">Системы хранения для одежды</a></li>
                    </ul>
                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=211">Ванная комната</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=212">Декоративные мелочи</a></li>
                        <li><a href="/?category=213">Для стирки, глажки и сушки</a></li>
                        <li><a href="/?category=214">Для уборки</a></li>
                        <li><a href="/?category=215">Коврики</a></li>
                        <li><a href="/?category=216">Полотенца</a></li>
                        <li><a href="/?category=217">Штора для ванной</a></li>
                    </ul>

                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=222">Текстиль</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=223">Декоративные подушки</a></li>
                        <li><a href="/?category=224">Детское постельное белье</a></li>
                        <li><a href="/?category=225">Наматрасники</a></li>
                        <li><a href="/?category=226">Одеяла</a></li>
                        <li><a href="/?category=227">Подушки</a></li>
                        <li><a href="/?category=228">Покрывала и пледы</a></li>
                        <li><a href="/?category=229">Постельное белье</a></li>
                        <li><a href="/?category=230">Рулонные шторы и жалюзи</a></li>
                        <li><a href="/?category=231">Шторы и гардины</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=232">Прочее</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=385">Корм для животных</a></li>
                        <li><a href="?category_id=386">Корм для собак</a></li>
                        <li><a href="?category_id=387">Корм для кошек</a></li>
                        <li><a href="?category_id=388">Корм для аквариумных рыбок</a></li>
                        <li><a href="?category_id=389">Живой корм</a></li>
                        <li><a href="?category_id=390">Корм для морских свинок</a></li>
                        <li><a href="?category_id=391">Корм для грызунов</a></li>
                        <li><a href="?category_id=392">Корм для птиц</a></li>
                        <li><a href="?category_id=393">Принадлежности для животных</a></li>
                        <li><a href="?category_id=394">Игрушки для животных</a></li>
                        <li><a href="?category_id=395">Аквариумы</a></li>
                        <li><a href="?category_id=396">Растения для аквариума</a></li>
                        <li><a href="?category_id=397">Принадлежности для аквариумов</a></li>
                        <li><a href="?category_id=398">Лекарства для животных</a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
            <!-------------------
            for teh
            -------------------->
            <div class="popup popup_menu" id="popup_menu_forteh">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=233">Электроника</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=234">Аксессуары</a></li>
                        <li><a href="/?category=235">Аудио, Видео, Фото</a></li>
                        <li><a href="/?category=236">Для авто</a></li>
                        <li><a href="/?category=237">Планшеты, Ноутбуки</a></li>
                        <li><a href="/?category=238">Телефоны</a></li>
                    </ul>
                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=239">Техника</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=240">Для дома</a></li>
                        <li><a href="/?category=241">Для красоты и здоровья</a></li>
                        <li><a href="/?category=242">Для кухни</a></li>
                    </ul>
                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=243">Прочее</a></h3>
                </div>
                <div class="clear"></div>
            </div>
            <!------------------------ for
            kras
            -------------------------->
            <div class="popup popup_menu" id="popup_menu_beauty">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=244">Парфюмерия</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=245">Женская парфюмерия</a></li>
                        <li><a href="/?category=246">Мужская парфюмерия</a></li>
                        <li><a href="/?category=247">Унисекс парфюмерия</a></li>
                        <li><a href="?category_id=414">Косметика</a></li>
                        <li><a href="?category_id=415">Краски</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=248">Макияж</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=249">Для глаз</a></li>
                        <li><a href="/?category=250">Для лица</a></li>
                        <li><a href="/?category=251">Для ногтей</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=252">Уход за телом</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=253">Антицеллюлитные средства</a></li>
                        <li><a href="/?category=254">Дезодоранты</a></li>
                        <li><a href="/?category=255">Депиляция</a></li>
                        <li><a href="/?category=256">Питательные средства</a></li>
                        <li><a href="/?category=257">Средства для загара</a></li>
                        <li><a href="/?category=258">Средства для очищения</a></li>
                        <li><a href="/?category=259">Средства для увлажнения</a></li>
                    </ul>
                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=260">Уход за волосами</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=261">Кондиционеры для волос</a></li>
                        <li><a href="/?category=262">Маски для волос</a></li>
                        <li><a href="/?category=263">Фиксация и защита волос</a></li>
                        <li><a href="/?category=264">Шампуни для волос</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=265">Техника для красоты</a></h3>

                    <h3 class="cat_link"><a href="/?category=266">Красота и здоровье</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=267">Спорт</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>

                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=268">Уход за лицом</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=269">Питательные средства</a></li>
                        <li><a href="/?category=270">Средства для контура глаз</a></li>
                        <li><a href="/?category=271">Средства для очищения</a></li>
                        <li><a href="/?category=272">Средства для увлажнения</a></li>
                        <li><a href="/?category=273">Средства против старения</a></li>
                    </ul>


                    <h3 class="cat_link"><a href="/?category=274">Гигиена</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=275">Средства гигиены</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=276">18+</a></h3>

                    <h3 class="cat_link"><a href="/?category=277">Прочее</a></h3>
                </div>
                <div class="clear"></div>
            </div>
            <!-------------------------------
            for uslug
            -------------------------------->
            <div class="popup popup_menu" id="popup_menu_foruslug">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=278">Красота</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=279">Уход за волосами</a></li>
                        <li><a href="/?category=280">Эпиляция</a></li>
                        <li><a href="/?category=281">Уход за лицом</a></li>
                        <li><a href="/?category=282">SPA, массаж, уход за телом</a></li>
                        <li><a href="/?category=283">Маникюр, педикюр</a></li>
                        <li><a href="/?category=284">Другое</a></li>
                    </ul>

                    <h3 class="cat_link"><a href="/?category=285">Здоровье</a></h3>
                    <ul class="subcat_links">
                        <li><a href="/?category=286">Стоматология</a></li>
                        <li><a href="/?category=287">Диагностика, обследование</a></li>
                        <li><a href="?category_id=416">Лечение зубов под наркозом</a></li>
                        <li><a href="?category_id=417">Лечение зубов под закисью азота</a></li>
                        <li><a href="?category_id=418">Исправление прикуса</a></li>
                        <li><a href="?category_id=419">Ортодонтия</a></li>
                        <li><a href="?category_id=420">Компьютерное моделирование протезов</a></li>
                        <li><a href="?category_id=421">Имплантация, протезирование</a></li>
                        <li><a href="?category_id=422">3D-компьютерная томография зубов</a></li>
                        <li><a href="?category_id=423">Ортопантомография</a></li>
                        <li><a href="?category_id=424">Лечение заболеваний десен</a></li>
                        <li><a href="?category_id=425">Лечение под микроскопом</a></li>
                        <li><a href="?category_id=426">Отбеливание зубов</a></li>
                        <li><a href="?category_id=427">Детская стоматология</a></li>
                        <li><a href="?category_id=428">Хирургия</a></li>
                        <li><a href="?category_id=429">Гинекология</a></li>
                        <li><a href="?category_id=430">Венерические заболевания</a></li>
                        <li><a href="?category_id=431">Услуги ветеринара</a></li>
                        <li><a href="?category_id=432">Животный доктор</a></li>
                        <li><a href="/?category=288">Другое</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=293">Ремонт</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=468">Ремонт компьютеров</a></li>
                        <li><a href="?category_id=469">Ремонт ноутбуков</a></li>
                        <li><a href="?category_id=470">Ремонт телефонов</a></li>
                        <li><a href="?category_id=471">Восстановление информации</a></li>
                        <li><a href="?category_id=472">Переустановка Windows</a></li>
                        <li><a href="?category_id=473">Установка программ</a></li>
                        <li><a href="?category_id=474">Удаление вирусов</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="?category_id=517">По дому</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=475">Уборка квартир, домов</a></li>
                        <li><a href="?category_id=476">Ремонт квартир</a></li>
                        <li><a href="?category_id=477">Монтажные работы</a></li>
                        <li><a href="?category_id=478">Полный цикл работ</a></li>
                        <li><a href="?category_id=479">Ремонт техники</a></li>
                        <li><a href="?category_id=480">Проведение труб</a></li>
                        <li><a href="?category_id=481">Сантехнические работы</a></li>
                        <li><a href="?category_id=482">Облицовка кафелем и керамогранитом<br> стен и пола</a></li>
                        <li><a href="?category_id=483">Малярно-штукатурные работы</a></li>
                        <li><a href="?category_id=484">Электромонтажные работы</a></li>
                        <li><a href="?category_id=485">Возведение из гипсокартона одноуровневых <br>и многоуровневых потолков</a></li>
                        <li><a href="?category_id=486">Выравнивание стен, пола, потолка под маяк</a></li>
                        <li><a href="?category_id=487">Штукатурные работы <br>(шпатлевка стен, потолков)</a></li>
                        <li><a href="?category_id=488">Монтаж перегородок</a></li>
                        <li><a href="?category_id=489">Стяжка пола, наливной пол</a></li>
                        <li><a href="?category_id=490">Настил ламината, линолеума, <br>коврового покрытия</a></li>
                    </ul>
                </div>
                <div class="column">

                    <h3 class="cat_link"><a href="?category_id=516">Юридические услуги</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=536">Земельное право</a></li>
                        <li><a href="?category_id=537">Семейное право</a></li>
                        <li><a href="?category_id=538">Трудовое право</a></li>
                        <li><a href="?category_id=539">Уголовное право</a></li>
                        <li><a href="?category_id=540">Налоговое право</a></li>
                        <li><a href="?category_id=541">Жилищное право</a></li>
                        <li><a href="?category_id=542">Гражданское право</a></li>
                        <li><a href="?category_id=543">Корпоративное право</a></li>
                        <li><a href="?category_id=544">Хозяйственное право</a></li>
                        <li><a href="?category_id=545">Наследственное право</a></li>
                        <li><a href="?category_id=546">Административное право</a></li>
                        <li><a href="?category_id=547">Защита прав потребителей</a></li>
                        <li><a href="?category_id=548">Оформление гражданства РФ</a></li>
                        <li><a href="?category_id=549">Гражданский, уголовный <br>и арбитражный процесс</a></li>
                        <li><a href="?category_id=550">Регистрация индивидуальных предпринимателей<br> и юридических лиц</a></li>
                        <li><a href="?category_id=551">Составление полного пакета документов</a></li>
                        <li><a href="?category_id=552">Предоставление документов в <br>регистрирующие органы</a></li>
                        <li><a href="?category_id=553">Получение Свидетельства<br> о внесении записи в ЕГРЮЛ</a></li>
                        <li><a href="?category_id=554">Получение кодов статистики в Облкомстате</a></li>
                        <li><a href="?category_id=555">Регистрация изменений в учредительные <br>документы юридических лиц</a></li>
                        <li><a href="?category_id=556">Реорганизация юридических лиц</a></li>
                        <li><a href="?category_id=557">Ликвидация юридических лиц</a></li>
                        <li><a href="?category_id=558">Прекращение деятельности (ликвидация)<br> индивидуальных предпринимателей</a></li>
                        <li><a href="?category_id=559">Открытие расчетных счетов</a></li>
                        <li><a href="?category_id=560">Регистрационные услуги</a></li>
                        <li><a href="?category_id=561">Юридические консультации</a></li>
                        <li><a href="?category_id=562">Составлению договоров, исковых <br>заявлений, иных документов</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="?category_id=515">Сайты, продвижение, реклама</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=491">Дизайн сайтов</a></li>
                        <li><a href="?category_id=492">Дизайн логотипов</a></li>
                        <li><a href="?category_id=493">Верстка</a></li>
                        <li><a href="?category_id=494">Разработка сайтов</a></li>
                        <li><a href="?category_id=495">Разработка одностраничников</a></li>
                        <li><a href="?category_id=496">SMM</a></li>
                        <li><a href="?category_id=497">SEO</a></li>
                        <li><a href="?category_id=498">SMO</a></li>
                        <li><a href="?category_id=499">Оформление сообществ в соц., сетях</a></li>
                        <li><a href="?category_id=500">Баннеры</a></li>
                        <li><a href="?category_id=501">Брендирование</a></li>
                        <li><a href="?category_id=502">Сервера</a></li>
                        <li><a href="?category_id=503">Облачные хранилища</a></li>
                        <li><a href="?category_id=504">Устранение уязвимостей</a></li>
                        <li><a href="?category_id=505">Подключение модулей</a></li>
                        <li><a href="?category_id=506">Написание программ</a></li>
                        <li><a href="?category_id=507">Настройка Яндекс.Директ</a></li>
                        <li><a href="?category_id=508">Настройка Google AdWords</a></li>
                        <li><a href="?category_id=509">Настройка Google Analytics</a></li>
                        <li><a href="?category_id=510">Обучение Photoshop</a></li>
                        <li><a href="?category_id=511">Обучение CorelDRAW</a></li>
                        <li><a href="?category_id=512">Обучение в других программах</a></li>
                    </ul>
                </div>
                <div class="column">

                    <h3 class="cat_link"><a href="?category_id=514">Для авто</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=433">Чип-тюнинг</a></li>
                        <li><a href="?category_id=434">Сход-развал</a></li>
                        <li><a href="?category_id=435">Балансировка колес</a></li>
                        <li><a href="?category_id=436">Шиномонтаж</a></li>
                        <li><a href="?category_id=437">Лакокрасочные услуги</a></li>
                        <li><a href="?category_id=438">Ремонт кузова</a></li>
                        <li><a href="?category_id=439">Перетяжка салона</a></li>
                        <li><a href="?category_id=440">Диагностика</a></li>
                        <li><a href="?category_id=441">Мойка</a></li>
                        <li><a href="?category_id=442">Экспресс-мойка</a></li>
                        <li><a href="?category_id=443">Полировка кузова</a></li>
                        <li><a href="?category_id=444">Жидкая полировка</a></li>
                        <li><a href="?category_id=445">Горячий или холодный воск</a></li>
                        <li><a href="?category_id=446">Мойка и полировка салона</a></li>
                        <li><a href="?category_id=447">Чернение резины</a></li>
                        <li><a href="?category_id=448">Кондиционер кожи и др.</a></li>
                        <li><a href="?category_id=449">Прочее</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="?category_id=513">Свадьба</a></h3>
                    <ul class="subcat_links">
                        <li><a href="?category_id=451">Свадебный фотограф</a></li>
                        <li><a href="?category_id=452">Свадебный видеооператор</a></li>
                        <li><a href="?category_id=453">Тамада</a></li>
                        <li><a href="?category_id=454">Оформление зала</a></li>
                        <li><a href="?category_id=455">Платья</a></li>
                        <li><a href="?category_id=456">Костюмы</a></li>
                        <li><a href="?category_id=457">Сопутствующие товары</a></li>
                        <li><a href="?category_id=458">Свадебные аксессуары</a></li>
                        <li><a href="?category_id=459">Свадебные товары для невесты</a></li>
                        <li><a href="?category_id=460">Свадебные товары для жениха</a></li>
                        <li><a href="?category_id=461">Заказ автобомиля</a></li>
                        <li><a href="?category_id=462">Организация торжеств</a></li>
                        <li><a href="?category_id=463">Развлечения для гостей</a></li>
                        <li><a href="?category_id=464">Свадебный кортеж</a></li>
                        <li><a href="?category_id=465">Цветы на свадьбу</a></li>
                        <li><a href="?category_id=466">Свадебный танец</a></li>
                        <li><a href="?category_id=467">Проведение фейерверков</a></li>
                    </ul>
                    <h3 class="cat_link"><a href="/?category=294">Разное</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="/?category=289">Развлечения</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=290">Концерты</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=291">Фитнес</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="/?category=292">Обучение</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=518">Тату</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=519">Пирсинг</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=520">Фотоуслуги</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=521">Видеоуслуги</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=522">Свидания на крышах</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=523">Организация встреч</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=524">Гостиницы</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=525">Сауны</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=526">Аренда квартир</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=527">Хостелы</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=528">Встреча в аэропорту</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=529">Охрана</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=530">Сопровождение</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=531">Вооруженное сопровождение</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link first"><a href="?category_id=532">Обеспечение безопасности</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
            <!-------------------------------
            for otel
            -------------------------------->
            <div class="popup popup_menu" id="popup_menu_otel">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=295">Москва и область</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=296">Санкт-Петербург и область</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=297">Золотое кольцо</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=298">Алтай</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=299">Байкал</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=300">Балтика</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=301">Поволжье</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=302">Сибирь</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=303">Урал</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=304">Юг России</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=305">Другие города</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=306">Украина</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=307">Казахстан</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=308">Беларусь</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    <h3 class="cat_link"><a href="/?category=309">Другие страны</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
            <!-------------------------------
            for tours
            -------------------------------->

            <div class="popup popup_menu" id="popup_menu_tours">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=310">Россия</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=311">Египет</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=312">Турция</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=313">Италия</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=314">Греция</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=315">Чехия</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=316">Испания</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=317">Англия</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=318">Другие страны</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=319">Швейцария</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=320">США</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=321">Франция</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="?category_id=533">Германия</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>

                <div class="clear"></div>
            </div>
            <!----------------------
            for shop
            ------------------------->

            <div class="popup popup_menu" id="popup_menu_forshop">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=322">Скидки</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=323">Распродажи</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=324">Подарки</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>

                <div class="clear"></div>
            </div>
            <!---------------------------------------------------
            for bars
            ------------------>
            <div class="popup popup_menu" id="popup_menu_bars">
                <div class="popup_menu_arrow"></div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=325">Скидки</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=326">Подарки</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=327">Скидки на все меню</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=328">Скидки на все меню и напитки</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=329">Суши</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=330">Скидки на барную карту</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=331">Меню и напитки</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=332">Воки</a></h3>
                    <ul class="subcat_links">
                    </ul>
                    </ul>
                </div>
                <div class="column">
                    <h3 class="cat_link first"><a href="/?category=333">Фуршеты</a></h3>
                    <ul class="subcat_links">
                    </ul>

                    <h3 class="cat_link"><a href="/?category=334">Другая кухня</a></h3>
                    <ul class="subcat_links">
                    </ul>
                </div>

                <div class="clear"></div>
            </div>

            <!---------------------------- ----------------------->
            <div class="image_crop_block" id="image_crop">
                <div class="ic_image"></div>
                <div class="ic_toolbox">
                    <div class="ic_info">
                        <span class="ic_steps"><span id="step_cur">1</span> из <span id="step_amount">2</span></span>
                        <span class="ic_steps_text" id="step_info">Выберите основное изображение</span>
                    </div>
                    <div class="ic_btns">
                        <button class="btn btn-color-white btn-size-small rounded_3" id="cancel_btn">Отмена</button>
                        <button class="btn btn-size-small rounded_3" id="next_btn">Далее</button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--header-->
<!--navigation-->
<div id="navigation" class="main_block centred">
    <div class="top_toolbox rounded_15">
        <div class="tt_block">
            <ul class="top_menu">
                <li><a href="/">Главная</a></li>
                <li><a href="/howto">Как это работает</a></li>
                <li><a href="//gm1lp.ru/page/tariff" target="_blank">Цены</a></li>
                <li><a href="https://gm1lp.ru/help/agree" target="_blank">Правила</a></li>
            </ul>
            <div class="clear"></div>
        </div>

        <div class="tt_block tt_search">
            <form action="/action/search" method="GET" id="searchForm">
                <input autocomplete="off" name="qSearch" type="text" class="search_left" placeholder="Что вы хотите найти" value="<?php echo isset($_GET['qSearch']) ? $_GET['qSearch'] : ''; ?>"/>
                <input type="hidden" name="qCategories" value="" id="qCategories"/>
                <button popup-target="popup_searchlocation" popup-pos="left" class="item_popup_menu btn btn-searchlocation"><span>Везде</span></button>
                <button class="btn btn-icon btn-icon-search btn-color-lyellow rounded_3">Найти</button>
            </form>
        </div>
    </div>

    <div class="categories_popup">
        <ul class="large_menu">
            <li class="menu_forher item_popup_menu" popup-target="popup_menu_forwomen" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Для неё</a></li>
            <li class="menu_forhim item_popup_menu" popup-target="popup_menu_formen" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Для него</a></li>
            <li class="menu_forchildren item_popup_menu" popup-target="popup_menu_forkid" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Детям</a></li>
            <li class="menu_forhome item_popup_menu" popup-target="popup_menu_forhome" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Для дома</a></li>
            <li class="menu_techs item_popup_menu" popup-target="popup_menu_forteh" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Техника</a></li>
            <li class="menu_beauty item_popup_menu" popup-target="popup_menu_beauty" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Красота</a></li>
            <li class="menu_services item_popup_menu" popup-target="popup_menu_foruslug" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Услуги</a></li>
            <li class="menu_hotels item_popup_menu" popup-target="popup_menu_otel" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Отели</a></li>
            <li class="menu_tours item_popup_menu" popup-target="popup_menu_tours" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Туры</a></li>
            <li class="menu_shops item_popup_menu" popup-target="popup_menu_forshop" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Магазины</a></li>
            <li class="menu_bars item_popup_menu" popup-target="popup_menu_bars" popup-pos="center"
                popup-boundary="#main" popup-fixed=".popup_menu_arrow"><a href="#">Кафе/бары</a></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<!--navigation-->

