<script>
    $(document).ready(function () {
        $('#searchForm').submit(function () {
            var values = '';
            $('#popup_searchlocation :checked').each(function () {
                values += $(this).val() + ',';
            });
            values = values.substr(0, values.length - 1);
            $('#qCategories').val(values);
        });
        $('.top_block .tb_amount').each(function (){
            if($(this).text()=='0'){
                $(this).hide();
            }
        });
    });
</script>
</body>
</html>