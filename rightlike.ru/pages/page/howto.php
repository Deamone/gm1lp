<?php
header_('RightLike.ru', '', '', ''
    . '', 'external howto');
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <h1>Как это работает</h1>

            <p>
                Главная цель нашего сервиса – упростить Вам задачу поиска интересных акций и предложений скидок по купонам! Каждый день мы собираем и систематизируем для Вас скидочные купоны со всех сайтов скидок. Мы помогаем Вам следить за наиболее интересными предложениями и выбирать из них самые выгодные.
            </p>

            <p class="large">
                Ежедневно на сайте выкладываются<br/>
                сотни новых акций, скидок, бонусов и<br/>
                привилегий.
            </p>


            <div class="column_half first">
                <ul class="styled_list">
                    <li>
                        Главная цель нашего сервиса – упростить<br/> Вам задачу поиска интересных акций и предложений скидок по купонам!
                    </li>
                    <li>
                        Каждый день мы собираем и систематизируем для Вас скидочные купоны со всех сайтов скидок.
                    </li>
                    <li>
                        Мы помогаем Вам следить за наиболее интересными предложениями и выбирать из них самые выгодные.
                    </li>
                </ul>
            </div>

            <div class="column_half">
                <ul class="styled_list">
                    <li>
                        На нашем сервисе любой желающий может создать свой конкурс всего за несколько минут, а конструктор конкурсов в этом поможет.
                    </li>

                    <li>
                        Попробуйте акции с несколькими победителями, т.е., разыграйте не один подарок, а несколько, чтобы подбодрить аудиторию к решительным действиям. Ошеломляйте!
                    </li>

                    <li>
                        Нравится сервис конкурсов? Попробуйте наш сервис скидок – <a href="//gmcard.ru" target="_blank">gmcard.ru</a>
                    </li>
                </ul>
            </div>

            <div class="clear"></div>

            <div class="howto_buttons">
                <form action="/action/create">
                    <button class="btn rounded_3">Запустить конкурс</button>
                </form>
            </div>
        </div>

        <div class="right_block">
            <ul class="right_menu">
                <li class="active"><a class="rounded_3" href="/howto">Как это работает</a></li>
                <li><a class="rounded_3" href="//gm1lp.ru/page/tariff" target="_blank">Цены</a></li>
                <li><a class="rounded_3" href="https://gm1lp.ru/help/agree" target="_blank">Правила</a></li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
<?php
footer();
