<?php
if (!$USER) {
    redirect('/page/login');
}
if(isset($_GET['getuser'])){
    $user = getUserById($_POST['id']);
    if(!$user){
        exit('{avatar:"",name:"",fam:""}');
    }else{
        exit(json_encode(array(
            'name'=>$user['name'],
            'fam'=>$user['fam'],
            'avatar'=>'/uploads/avatars/' . $user['ava']
        )));
    }
}

if (isset($_GET['setAvatar'])) {
    $avatart = str_replace('..', '', $_GET['setAvatar']);
    if (KPHP) {
        $path = '../uploads/avatars/';
    } else {
        $path = './uploads/avatars/';
    }

    if (file_exists($path . $avatart)) {
        updateUserById($USER['id'], array('ava'), array($avatart));
        redirect('/' . $USER['url']);
    }
} elseif (isset($_GET['delphoto'])) {
    updateUserById($USER['id'], array('ava'), array('default.jpg'));
    exit;
}

$_USER = $USER;
$itsI = true;
$_ = false;
$id = isset($_GET['_route_'])?query_escape($_GET['_route_']):'';
if ($id) {
    $_ = getUserByUrl($id);
} elseif (isset($_GET['id'])) {
    $_ = getUserById((int) $_GET['id']);
    if ($_ && !isAjax() && !empty($_['url'])) {
        redirect($_['url']);
    }
}elseif(!isAjax() && !empty ($USER['url'])){
    redirect($USER['url']);
}
if ($_) {
    $_USER = $_;
    if ($_['id'] != $USER['id']) {
        $itsI = false;
    }
}


if (isset($_GET['del_post']) && isAjax()) {
    $id = (int) $_POST['id'];
    $post = getPost($id);
    if ($post && ($post['user_id'] == $USER['id'] || $post['owner_id'] == $USER['id'])) {
        removePost($id);
    }
    exit('удаляем');
} elseif (isset($_POST['add_post']) && isAjax()) {
    $post = $_POST['post'];
    addPost($_USER['id'], $post);
    exit('1');
} elseif (isset($_POST['get_post']) && isAjax()) {
    $id = $_POST['get_post'];
    $post = getPost($id);
    echo $post['text'];
    exit();
} elseif (isset($_POST['edit_post']) && isAjax()) {
    $id = $_POST['id'];
    $post = getPost($id);
    if ($post && $post['user_id'] == $USER['id']) {
        updatePost($id, $_POST['post']);
    }
    exit('1');
} elseif (isset($_POST['add_like']) && isAjax()) {
    $id = $_POST['id'];
    likePost($id);

    exit('1');
}

if (isset($_GET['add_friend']) && !$itsI) {
    userFriend($USER['id'], $_USER['id']);
    exit('1');
} elseif (isset($_GET['del_friend']) && !$itsI) {
    userFriend($USER['id'], $_USER['id'], true);
    exit('-1');
}
////////////////////////////////////////
$friends = getUserFriends($_USER['id']);
/*$__ = query("SELECT * FROM `new_1lp`.`nx_friends`");
$_ = array();
foreach($__['rows'] as $f){
    $_[$f['from']][$f['to']] += $f['status'];
    $_[$f['to']][$f['from']] += $f['status'];
}
foreach($_ as $uid=>$friends_id){
    foreach($friends_id as $id=>$status){
        if($status==2){
            $USER['id'] = $uid;
            userFriend($uid, $id);
            $USER['id'] = $id;
            userFriend($id, $uid);
        }
    }
}
print_r($_);
exit;*/
$friend = false;
if (isset($friends[0][$USER['id']])) {
    $friend = 1;
}
if (isset($friends[2][$USER['id']])) {
    $friend = 2;
}
if (isset($friends[1][$USER['id']])) {
    $friend = 3;
}
updateUserById($USER['id'], array('online'), array(time()));
header_($_USER['name'] . ' ' . $_USER['fam']);
?>
<div class="container-menu-bottom"></div>
<!-- END container-menu --> <div id="shadow"></div>
<div id="content" class="user-page">
    <div class="leftsidebar">
        <?php
        if ($itsI) {
            echo '<div class="widget widget-user-info">
            <div class="main-info">
                <div class="avatar"><img src="/uploads/avatars/' . $_USER['ava'] . '" />
                    <a href="/' . $_USER['url'] . '?delfoto=1" id="del-fot"></a>
                    <form action="/crop.php" method="post" id="upload_form" enctype="multipart/form-data" onsubmit="return checkForm()" class="dropped" style="display: block; overflow: hidden;">
                        <label for="upload-photo" id="upfot">Выбрать файл</label>
                        <input id="x1" type="hidden" name="x1">
                        <input id="y1" type="hidden" name="y1">
                        <input id="x2" type="hidden" name="x2">
                        <input id="y2" type="hidden" name="y2">
                        <input class="file" id="upload-photo" type="file" size="28" name="photo" onchange="fileSelectHandler();
                                showMod();" style="visibility: hidden; position: absolute;">                                    
                        <div class="step2">                                  
                            <h2>Шаг 2: Указать новый размер</h2>                                     
                            <img id="preview" alt="">
                            <div class="info">
                                <input id="filesize" type="hidden" name="filesize">
                                <input id="filetype" type="hidden" name="filetype">
                                <input id="filedim" type="hidden" name="filedim">
                                <input id="w" type="hidden" name="w">
                                <input id="h" type="hidden" name="h"></div>
                            <input type="submit" value="Готово">
                            <a href="" class="close-mod">Закрыть</a>
                        </div>
                    </form>
                </div>
            </div>
            <ul>
                <li class="my-balance"><a href="/balance">Баланс<span>' . $_USER['money'] . ' GM</span></a></li>     
                <!--li class="my-thanks"><a href="javascript:void(0)">Спасибо<span>' . $_USER['forwork'] . '</span></a></li>
                <div class="ver-stat">
                    <h2><a href="/veripro">' . isVer($_USER) . '</a></h2>
                    <strong>GM Card:</strong>  <small>Не подключена</small> <a href="" class="open-modal">Подключить</a>                                    <br><strong>GM Банк:</strong> <small>Не подключена</small> <a href="" class="open-modal">Подключить</a>                                </div>
                <li class="my-plans"><a href="javascript:void(0)">Мои планы</a></li-->
                <li class="edit-page"><a href="/settings">Настройки</a></li>
            </ul>
        </div><!-- .widget-user-info -->';
        } else {
            echo '<div class="widget widget-user-info">
            <div class="main-info">
                <div class="avatar"><img src="/uploads/avatars/' . $_USER['ava'] . '">
                </div>
            </div>
            <ul class="friend-page">
                ';
            if (!$friend) {
                echo '<li id="pAddFriend"><a href="?add_friend=1">Добавить в друзья</a></li>';
            } elseif ($friend == 2) {
                echo '<li id="pConfirmFriend"><a href="?add_friend=1">Подтвердить, что вы друзья</a></li>';
            } elseif ($friend == 3) {
                echo '<li id="pDelFriend"><a href="?del_friend=1">Отменить заявку</a></li>';
            } elseif ($friend == 1) {
                echo '<li id="pDelFriend"><a href="?del_friend=1">Удалить из друзей</a></li>';
            }
            echo '<li><a href="/message?id=' . $_USER['id'] . '">Написать сообщение</a></li>
                <li><a href="/send_gm?id=' . $_USER['id'] . '" oclick="return false;">Перевести GM</a></li>
                <li><a href="/friends?id=' . $_USER['id'] . '">Друзья</a></li>
            </ul>
        </div>';
        }
        ?>
        <?php
        $body = '';
        $f_users = array_keys($friends[0]);
        shuffle($f_users);
        $count = count($f_users);
        $j = 0;
        for ($i = 0; $i < $count; $i++) {
            $j++;
            if ($j > 9) {
                continue;
            }
            $_ = getUserById($f_users[$i]);
            $body .= '<a href = "/' . $_['url'] . '"><img src = "/uploads/avatars/' . $_['ava'] . '" title = "' . $_['fam'] . ' ' . $_['name'] . '" alt = "' . $_['fam'] . ' ' . $_['name'] . '" width = "67" height = "67" /></a>';
        }
        $count = $j;
        $header = '<div class="widget widget-friends">
            <h2><a href="/friends?id=' . $_USER['id'] . '">Друзья</a> <span>' . $count . '</span></h2>
            <p>-------------------</p>
            <div>';
        $footer = '</div>
            </div><!-- .widget-friends -->';
        if ($count) {
            echo $header . $body . $footer;
        }
        ?>
        <div class="widget widget-presents" style="display: none;">
            <div class="wl-h"></div>
            <div class="wl-c">
                <h2>Мне подарили</h2>
                <p>------------------</p>
                <div>
                    <img src="/application/views/site/images/presents/img1.png" class="img1" />
                    <img src="/application/views/site/images/presents/img2.png" class="img2" />
                    <img src="/application/views/site/images/presents/img3.png" class="img3" />
                </div>
                <a href="#" class="all">всего <span>27</span> подарков</a>
            </div>
            <div class="wl-f"></div>
        </div><!-- ..widget-presents -->

        <div class="widget widget-guide">
            <h2>Сообщества</h2>
            <span> <a href="#">3 сообщества</a></span>
            <p>-------------------</p>
            <div>
                <a href="#"><img src="/application/views/site/images/participants/img6.png" /></a>
                <a href="#"><img src="/application/views/site/images/participants/img1.png" /></a>
                <a href="#"><img src="/application/views/site/images/participants/img4.png" /></a>
            </div>
        </div><!-- .widget-guide -->

        <div class="widget widget-fotoalbum" style="display: none">
            <div class="carousel carousel-foto">
                <ul>
                </ul>
            </div>
            <h2>Фотоальбомы</h2>
            <span id="foto-count">все 3 альбома</span>
            <p>------------------</p>
            <div class="buttons">
                <a href="#" class="foto-left left"></a>
                <a href="#" class="foto-right right"></a>
            </div>
        </div><!-- .widget-fotoalbum -->


        <div class="widget widget-video" style="display: none">
            <h2>Видео <span>(48)</span></h2>
            <div class="carousel carousel-video">
                <ul>
                    <li>
                        <img src="/application/views/site/images/fotoalbum/img2.png" />
                        <a href="#">Fritz Kalkbrenner: Right in The Dark 2010</a>
                    </li>
                    <!---li>
                        <img src="/application/views/site/images/fotoalbum/img2.png" />
                        <a href="#">Fritz Kalkbrenner: Right in The Dark 2010</a>
                    </li>
                    <li>
                        <img src="/application/views/site/images/fotoalbum/img2.png" />
                        <a href="#">Fritz Kalkbrenner: Right in The Dark 2010</a>
                    </li-->
                </ul>
            </div>
            <div class="buttons">
                <a href="#" class="video-left left"></a>
                <a href="#" class="video-right right"></a>
            </div>

        </div><!-- .widget-video -->


    </div><!-- .leftsidebar -->
    <div class="content">

        <div id="user-information">
            <h2><?php echo $_USER['name'] . ' ' . $_USER['fam']; ?></h2>
            <!--p class="place"><?php echo $_USER['country']; ?></p-->
            <div class="information">
                <div class="inf-item" style="<?php echo ((!$_USER['date'] || $_USER['date'] == '0000-00-00') && !$_USER['relation']) ? 'display:none;' : ''; ?>">
                    <?php
                    if ($_USER['date'] && $_USER['date'] != '0000-00-00') {
                        echo '<p><span>День рождения</span><small>' . sym_date($_USER['date']) . '</small></p>';
                    }
                    if ($_USER['relation']) {
                        echo '<p><span>Семейное положение</span><small>' . $_LANG['PROFILE_RELATION'][$_USER['gender']][$_USER['relation']] . '</small></p>';
                    }
                    ?>
                    <a href="#" id="hideProfileInfo" class="grey hide-more">Скрыть подробную информацию</a>
                </div>
                <div class="inf-item to-hide" style="<?php echo (!$_USER['city'] && !$_USER['tel'] && !$_USER['skype'] && !$_USER['site']) ? 'display:none;' : ''; ?>">
                    <p>
                        <span><b>Контакты:</b></span>
                    </p>
                    <?php
                    if ($_USER['city']) {
                        echo '<p><span>Город</span><small>' . $_USER['city'] . '</small></p>';
                    }
                    if ($_USER['tel']) {
                        echo '<p><span>Мобильный телефон</span><small>' . $_USER['tel'] . '</small></p>';
                    }
                    if ($_USER['skype']) {
                        echo '<p><span>Skype</span><small>' . $_USER['skype'] . '</small></p>';
                    }
                    if ($_USER['site']) {
                        echo '<p><span>Сайт</span><small>' . $_USER['site'] . '</small></p>';
                    }
                    if ($itsI) {
                        echo '<a href="/settings" class="grey">Редактировать</a>';
                    }
                    ?>

                </div>
                <div class="inf-item to-hide" style="<?php echo (!$_USER['vuz'] && !$_USER['scool']) ? 'display:none;' : ''; ?>">
                    <p>
                        <span><b>Образование:</b></span>
                    </p>
                    <?php
                        if($_USER['vuz']){
                            echo '<p><span>ВУЗ</span><small>'.$_USER['vuz'].'</small></p>';
                        }
                        if($_USER['scool']){
                            echo '<p><span>Школа</span><small>'.$_USER['scool'].'</small></p>';
                        }
                        if($itsI){
                            echo '<a href="/settings" class="grey">Редактировать</a>';
                        }
                    ?>
                </div>
                
                <div class="inf-item to-hide" style="<?php echo (!$_USER['muz'] && !$_USER['films'] && !$_USER['books'] && !$_USER['games'] && !$_USER['sports'] && !$_USER['kul'] && !$_USER['cit']) ? 'display:none;' : ''; ?>">
                    <p>
                        <span><b>Интересы:</b></span>
                    </p>
                    <?php
                        if($_USER['muz']){
                            echo '<p><span>Музыка</span><small>'.$_USER['muz'].'</small></p>';
                        }
                        if($_USER['films']){
                            echo '<p><span>Фильмы</span><small>'.$_USER['films'].'</small></p>';
                        }
                        if($_USER['books']){
                            echo '<p><span>Книги</span><small>'.$_USER['books'].'</small></p>';
                        }
                        if($_USER['games']){
                            echo '<p><span>Игры</span><small>'.$_USER['games'].'</small></p>';
                        }
                        if($_USER['sports']){
                            echo '<p><span>Спорт</span><small>'.$_USER['sports'].'</small></p>';
                        }
                        if($_USER['kul']){
                            echo '<p><span>Кулинария</span><small>'.$_USER['kul'].'</small></p>';
                        }
                        if($_USER['cit']){
                            echo '<p><span>Цитаты</span><small>'.$_USER['cit'].'</small></p>';
                        }
                        if($itsI){
                            echo '<a href="/settings" class="grey">Редактировать</a>';
                        }
                    ?>
                </div>
                <!--div class="inf-item to-hide">
                    <p>
                        <span><b>Личная информация:</b></span>
                    </p>
                    <p>
                        <span>Группы</span><small></small>
                    </p>
                    <a href="/settings" class="grey">Редактировать</a>
                </div-->
            </div>
        </div>


        <div class="foto-block">
            <?php
            $photos = getUserPhotos($_USER['id']);
            $c = count($photos);
            echo '<h2><a href="/albums?id=' . $_USER['id'] . '">Фотографии</a> <span>всего ' . $c . '</span></h2>
            <div class="photos">';
            $c = min($c, 5);
            for ($i = 0; $i < $c; $i++) {
                echo '<div class="ph1"><div class="plaster"></div><a href="#" data-pid="' . $photos[$i]['photo_id'] . '" class="nx-photo-file"><img src="' . $photos[$i]['file'] . '"></a></div>';
            }
            echo '</div>';
            ?>
        </div><!-- foto-block -->

        <div class="message-desc">
            <?php
            if (isset($_POST['limit'])) {
                $limit = (int) $_POST['limit'];
            } else {
                $limit = 3;
            }
            $posts = getPosts($_USER['id']);
            $c = count($posts);
            echo '<h2>Доска <span>всего ' . $c . ' записей</span></h2>
            <div class="send-message-form">
                <form action="" id="newPostForm" method="post" >
                    <textarea name="post" placeholder="Ваше сообщение"></textarea>
                    <input type="submit" value="НАПИСАТЬ" />
                </form>
                <form method="post" id="editPostForm" style="display: none;" action=""  >
                    <textarea name="post" placeholder="Укажите текст"></textarea>
                    <input type="hidden" name="id" value="" id="editPostFormId" /><input type="hidden" name="edit" value="1" />
                    <input type="submit" id="editPostSubmit" value="НАПИСАТЬ" /> <input type="submit" value="ОТМЕНИТЬ" style="margin-left: 10px;" />
                </form>
            </div>
            <div class="comment-list">
                <ul class="list" id="posts" >';

            for ($i = 0; $i < $limit; $i++) {
                if (!isset($posts[$i])) {
                    break;
                }
                echo '
                    <li class="first">
                        <div class="wallPostNx">
                            <div class="avatar"><a href="/?id=' . $posts[$i]['user']['id'] . '"><img src="/uploads/avatars/' . $posts[$i]['user']['ava'] . '" /></a></div>
                            <div class="head">
                                <a href="/?id=' . $posts[$i]['user']['id'] . '">' . $posts[$i]['user']['name'] . ' ' . $posts[$i]['user']['fam'] . '</a>
                                <span>' . date('d.m.Y H:i:s', $posts[$i]['created']) . '</span>
                            </div>
                            <div class="text">
                                <p>' . $posts[$i]['text'] . '</p>
                            </div>
                            <div class="info" style="height: 85%;" data-id="' . $posts[$i]['wall_id'] . '">
                                ' . ($itsI ? '<a class="close" href="#"></a>' : '') . '
                                    ' . ($USER['id'] == $posts[$i]['user']['id'] ? '<a class="editPost" data-id="' . $posts[$i]['wall_id'] . '" href="#"></a>' : '') . '
                                <div class="clearfix"></div>
                                <div class="count" style="right:0;position: absolute;bottom: 0;" data-ilike="' . $posts[$i]['ilike'] . '" data-id="' . $posts[$i]['wall_id'] . '" data-owner="' . $posts[$i]['owner_id'] . '">' . $posts[$i]['likes'] . '</div>
                            </div> 
                        </div>
                    </li>';
            }
            ?>
            <script>
                function recosnstructPost(selector){
                    var list = $(selector);
                    list.each(function() {
                        var p = $(this).text();
                        p = p.replace(/www\./ig, 'http://');
                        p = p.replace(/http(.?)\:\/\//ig, 'http://');
                        ///p =p.replace(/http(s|)\:/ig, "$1";
                        p = p.replace(/http(s|)\:\/\/(\S+[\w\-\/])/ig, "<a href='http$1://$2' target=\"blank\">$2</a>");
                        p = p.replace(/\n/ig, "<br/>");
                        var length = p.length;
                        if (length > 300) {
                            var span = p.substr(300);
                            p = p.substr(0, 300);
                            $(this).html(p + '<span style="display:none;">' + span + '</span><span class="fullWallPost">... <a href="#" onclick="return false;">Показать полностью</a></span>');
                        } else {
                            $(this).html(p);
                        }
                        $('.wallPostNx .text p').click(function() {
                            $(this).find('span:first').show();
                            $(this).find('span.fullWallPost').remove();
                        });
                    });
                }
                $(document).ready(function() {
                    recosnstructPost('.wallPostNx .text p');
                    $(document).on('click', '.close', function(e) {
                        var id = $(this).parent().attr('data-id');
                        $this = $(this).parent().parent().parent().remove();
                        $.post('/?del_post', 'id=' + id, function(data) {
                            console.log(data);
                        });
                        return false;
                    });
                    $(document).on('click', '.count', function(e) {
                        var id = parseInt($(this).attr('data-id'));
                        var ilike = parseInt($(this).attr('data-ilike'));
                        var count = parseInt($(this).html());
                        if (!ilike) {
                            $(this).html(count + 1);
                            $(this).attr('data-ilike', 1)
                        } else {
                            $(this).html(count - 1);
                            $(this).attr('data-ilike', 0)
                        }
                        $.post("/", "add_like=1&id=" + id, function(data, textStatus) {
                        }
                        );
                    });
                    $(document).on('submit', '#newPostForm', function(e) {
                        $this = $(this);
                        $submit = $(this).find('input[type=submit]');
                        $submit.attr('disabled', 'disabled').attr('value', 'ОТПРАВЛЯЕТСЯ');
                        $.post('', $(this).serialize() + '&add_post=1', function(data) {
                            $this.find('textarea').val('');
                            load_posts();
                            $submit.removeAttr('disabled').attr('value', 'НАПИСАТЬ');
                        });

                        return false;
                    });
                    $(document).on('click', '#editPostSubmit', function(e) {
                        $this = $(this).parent();
                        $submit = $this.find('input[type=submit]:first');
                        $submit.attr('disabled', 'disabled').attr('value', 'СОХРАНЯЕМ');
                        $.post('', $this.serialize() + '&edit_post=1', function(data) {
                            $this.find('textarea').val('');
                            load_posts();
                            $submit.removeAttr('disabled').attr('value', 'НАПИСАТЬ');
                            $('#editPostForm input:last').click();
                        });

                        return false;
                    });
                    $(document).on('click', '.editPost', function(e) {
                        var id = parseInt($(this).attr('data-id'));
                        $('#editPostFormId').val(id);
                        $('#editPostForm').show();
                        $('#newPostForm').hide();
                        $('#editPostForm').attr("data-id", id);
                        $.post("", "get_post=" + id, function(data, textStatus) {
                            $('#editPostForm textarea').focus().val('').val(data).attr('style', 'box-shadow: 0 0 10px rgba(255,0,0,0.7);');

                            console.log(data);
                        });
                        $('#editPostForm input[name="postId"]').val(id);
                        e.preventDefault();
                        return false;
                    });
                    $(document).on('click', '#editPostForm input:last', function(e) {
                        $('#editPostForm').hide();
                        $('#newPostForm').show();
                        e.preventDefault();
                        return false;
                    });
                });
                var limit = 3;
                function load_posts() {
                    limit = limit + 3;
                    $.post('?wall', 'limit=' + limit, function(data) {
                        $('#posts').html($(data).find('#posts').html());
                        recosnstructPost('.wallPostNx .text p');
                    }, 'html');
                }
            </script>

            </ul>
            <script>
                /*post_st = 5;
                 post_status = 0;
                 function load_posts()
                 {
                 
                 if (post_status == 0)
                 {
                 
                 post_status = 1;
                 $.post("/page/let/posts",
                 "id=18090&st=" + post_st,
                 function(data, textStatus) {
                 $('#posts').append(data);
                 post_st = post_st + 5;
                 post_status = 0;
                 
                 },
                 "html" // "xml", "script", "json", "jsonp", "text"
                 );
                 }
                 
                 }
                 
                 posts
                 function openModalBalance() {
                 console.log('open');
                 }*/
            </script>
            <?php
            if ($c > $limit) {
                echo '<a href="javascript:" OnCLick="load_posts();" class="load-more-message-button">Загрузить еще</a>';
            }
            ?>
        </div>
    </div><!-- message-desc ../ WALL -->

    <?php
    $audios = array(); getUserAudios($_USER['id']);
    $count = min(3, count($audios));
    ?>
    <div class="audio-block" style="display: none;">
        <?php
        echo '<h2><a href="#">Аудио</a> <span>всего ' . count($audios) . '</span></h2>';
        ?>
        <div class="music-player" style="<?php echo (!$count ? 'display:none;' : ''); ?>">
            <!--div class="mp-head">
                <div>
                    <a href="javascript:void(0)" class="random"></a>
                    <a href="javascript:void(0)" class="add"></a>
                </div>
            </div-->
            <script>
                $(function() {
                    /*$(".progress_bar").slider({
                     range: true,
                     min: 0,
                     max: 100,
                     values: [0, 0],
                     slide: function(event, ui) {
                     $("#amount").val("$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ]);
                     }
                     });*/
                    $(".volume").slider({
                        range: true,
                        min: 0,
                        max: 100,
                        values: [0, 100],
                        slide: function(event, ui) {
                            $("#volume").val(ui.values[1] - ui.values[0]);
                            soundManager.setVolume('mySound', ui.values[1] - ui.values[0]);
                        }
                    });
                    //                                    $( "#amount" ).val( "$" + $( ".progress_bar" ).slider( "values", 0 ) +
                    //                                        " - $" + $( ".progress_bar" ).slider( "values", 1 ) );
                });
            </script>

            <ul class="audios">
                <input type="hidden" id="volume" value="100" />
                <?php
                for ($i = 0; $i < $count; $i++) {
                    $strlen = strlen($audios[$i]['artist'] . ' - ' . $audios[$i]['title']);
                    $duration = floor($audios[$i]['duration'] / 60);
                    echo '<li class="noactive" data-url="' . $audios[$i]['url'] . '" data-duration="' . (int) $audios[$i]['duration'] . '" data-vk-id="' . (int) $audios[$i]['vk_id'] . '">
                                        <div>
                                            <a href="javascript:void(0)" class="play_stop"></a>
                                            <div class="song_name" style="width:358px"><a href="javascript:void(0)">' . htmlspecialchars($audios[$i]['artist'], ENT_COMPAT) . '</a> - <span>' . htmlspecialchars($audios[$i]['title'], ENT_COMPAT) . '</span></div>
                                            <div class="progress_bar"></div>
                                            <div class="volume"></div>
                                            <div class="song_time">' . $duration . ':' . ($audios[$i]['duration'] - ($duration * 60)) . '</div>
                                            <div class="wire"></div>
                                        </div>
                                    </li>';
                }
                ?>
            </ul>
        </div>
        <script>
            if(!user){
            var user = {
                vk_uid: '<?php echo $_USER['vk_uid']; ?>'
            };
            }
            var q, sm2, slider, tracks = new Array(), position = 0, vk_ids = '';
            $(document).ready(function() {
                $('#del-fot').click(function(e) {
                    $.post('/?delphoto=1', '', function(data) {
                        $('.avatar img,.ava img').attr('src', '/uploads/avatars/default.jpg');
                    });
                    return false;
                });
                /**** Friends ****/
                $(document).on('click', 'li#pAddFriend a', function(e) {
                    $this = $(this);
                    $.get('?add_friend', '', function(data) {
                        showMessage('Предложение дружбы отправлено', 2500);
                        $this.html('Отменить заявку').attr('href', '?del_friend');
                        $this.parent().attr('id', 'pDelFriend');
                    });
                    e.preventDefault();
                    return false;
                });
                $(document).on('click', 'li#pDelFriend a', function(e) {
                    $this = $(this);
                    $.get('?del_friend', '', function(data) {
                        showMessage('Вы отказались дружить', 2500);
                        $this.html('Добавить в друзья').attr('href', '?add_friend');
                        $this.parent().attr('id', 'pAddFriend');
                    });
                    e.preventDefault();
                    return false;
                });
                $(document).on('click', 'li#pConfirmFriend a', function(e) {
                    $this = $(this);
                    $.get('?add_friend', '', function(data) {
                        showMessage('Теперь вы друзья', 2500);
                        $this.html('Удалить из друзей').attr('href', '?del_friend');
                        $this.parent().attr('id', 'pDelFriend');
                    });
                    e.preventDefault();
                    return false;
                });
                /***** Albums *******/

                $.post('/albums?albums=1&id=<?php echo $_USER['id']; ?>', '', function(data) {
                    if (!data[0]) {
                        return;
                    }
                    $('.widget-fotoalbum ul').empty();
                    for (var i = 1; i <= data[0]; i++) {
                        $('.widget-fotoalbum ul').append('<li><img src="' + data[i].preview + '" /><a href="/album?id=' + data[i].id + '">' + data[i].title + '</a></li>');
                    }
                    $('.widget-fotoalbum #foto-count').text('всего ' + data[0]);
                    setTimeout(function() {
                        $('.widget-fotoalbum').show();
                        $(".carousel-foto").jCarouselLite({
                            visible: 1,
                            btnNext: ".foto-right",
                            btnPrev: ".foto-left"
                        });
                        $(".carousel-video").jCarouselLite({
                            visible: 1,
                            btnNext: ".video-right",
                            btnPrev: ".video-left"
                        });
                    }, 500);
                }, 'json');


                /**** Audio *****/
                $('.audios li').each(function(k) {
                    $(this).attr('data-position', k);
                    tracks[k] = $(this);
                    var id = $(this).attr('data-vk-id');
                    var sName = $(this).find('.song_name');
                    var len = $(sName).text().length;
                    if (len > 48) {
                        $(sName).find('span').text($(sName).find('span').text().substr(0, len - 48));
                    }
                    if (id) {
                        vk_ids = vk_ids + ',' + id;
                    }
                });
                if (vk_ids) {
                    VK.Api.call('audio.get', {owner_id: user.vk_uid, audio_ids: vk_ids.substr(1)}, function(r) {

                        var count = r.response[0];
                        for (var i = 1; i < count; i++) {
                            if (r.response[i] === undefined) {
                                continue;
                            }
                            $('.audios li[data-vk-id="' + r.response[i].aid + '"]').attr('data-url', r.response[i].url);
                        }
                    });
                }
                $(document).on('click', '.audios li.active', function(e) {
                    $(this).removeClass('active').addClass('noactive');
                    soundManager.stopAll();
                    soundManager.destroySound('mySound');
                    clearInterval(q);
                    e.preventDefault();
                    return false;
                });
                $(document).on('click', '.audios li.noactive', function(e) {
                    $('.audios li.active').click();
                    position = $(this).attr('data-position');
                    var $this = $(this);
                    sm2 = soundManager.createSound({
                        id: 'mySound',
                        url: $this.attr('data-url'),
                        autoPlay: true,
                        volume: parseInt($("#volume").val()),
                        onfinish: playNextSound
                    });
                    slider = $(this).find(".progress_bar").slider({
                        range: true,
                        min: 0,
                        max: parseInt($this.attr('data-duration')),
                        values: [0, 0],
                        slide: function(event, ui) {
                            soundManager.setPosition('mySound', (ui.values[1] - ui.values[0]) * 1000);
                        }
                    });
                    q = setInterval(function() {
                        slider.slider("option", "values", [0, parseInt(sm2.position / 1000)]);
                    }, 500);
                    $(this).addClass('active').removeClass('noactive');
                    e.preventDefault();
                    return false;
                });
            });
            soundManager.setup({
                url: '/application/views/site/swf/',
                debugMode: false,
                onready: function() {
                }
            });
            function playNextSound() {
                console.log(position);
                if (tracks[position++] !== undefined) {
                    tracks[position].click();
                } else {
                    position = 0;
                    tracks[position].click();
                }
            }
        </script>
    </div><!-- audio-block -->

    <!--     <div class="cards-block">
             <h2>Открытки <span>всего 18 открыток</span></h2>
             <div class="cards">
                 <div><img src="images/cards/img1.png" /></div>
                 <div><img src="images/cards/img2.png" /></div>
                 <div><img src="images/cards/img3.png" /></div>
                 <div><img src="images/cards/img4.png" /></div>
                 <div><img src="images/cards/img5.png" /></div>
             </div>
         </div>-->

</div><!-- .content -->









<div class="clearfix"></div><!-- .clearfix -->

</div>
</div><!-- END CONTAINER -->

<?php
footer();
