<?php

$q = query("SELECT * FROM `tariff`;");
$tariffs = $q['rows'];

$prices = array();

foreach($tariffs as $tariff){
    $prices[$tariff['level']] = floatval($tariff['competition_price']);
}

$_USER = $USER;
$price = $prices[$USER['level']];

$edit = false;
if (isset($_GET['edit'])) {
    $action_id = (int)$_GET['edit'];
    $action_edit = getComp($action_id);
    $edit = true;
    if (!$action_edit || $action_edit['end'] > time() || $action_edit['user_id'] != $_USER['id']) {
        //redirect('/action/my');
    }

    $action_edit['images'] = unserialize($action_edit['images']);
    $action_edit['site'] = unserialize($action_edit['site']);
    $action_edit['cost'] = unserialize($action_edit['cost']);
    $_ = trim($action_edit['cat'], ',');
    $action_edit['cat'] = explode(',', $_);
    $_ = trim($action_edit['city'], ',');
    $action_edit['city'] = explode(',', $_);
}

if (isset($_GET['delete'])) {
    $action_id = (int)$_GET['delete'];
    $action_edit = getComp($action_id);
	if ($action_edit['user_id'] != $_USER['id']) {
		exit('deny');
	}
	delComp($action_id);
	redirect('/action/my');
}

if (isset($_GET['up'])) {
	uppingComp($_GET['up']);
	redirect('/action/my');
}



//$USER['money'] = 9999999;
header_('Создание конкурса', '', '', ''
    . '<script type="text/javascript" src="/js/vendor/jquery.ui.widget.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.fileupload.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>'
    . '<link rel="stylesheet" href="/css/chosen.css">'
    . '<script src="/js/chosen.jquery.js" type="text/javascript"></script>'
    . '<script src="/js/chosen.ajaxaddition.jquery.js" type="text/javascript"></script>');

function is_url($url)
{
    $url = preg_replace('/http(.?):\/\//i', '', $url);
    $url = 'http://' . trim($url, '/');
    return preg_match('|^http(s)?://[а-яa-z0-9-_]+(.[а-яa-z0-9-_]+)+(.[а-яa-z0-9-_]+)(:[0-9_]+)?(/.)?$|iu', $url);
}

if (isAjax()) {
    $json = array();
    if (isset($SYSTEM['get']['img'])) {
        echo prepareImg($SYSTEM['post']);
        exit();
    }
    if (isset($SYSTEM['get']['upload'])) {
        $file = $_FILES['file'];
        $info = getimagesize($file["tmp_name"]);
        if(!preg_match("/^image/", $info['mime'])){
            http_response_code(400);
            exit(json_encode(array('error' => 'Загруженный файл имеет неподдерживаемый формат.')));
        }

        $json['file'] = 'tmp/' . time() . '.' . getExtension($file["name"]);
        if (isset($SYSTEM['get']['dop'])) {
            $_ = 'tmp/' . time() . '.jpg';
            move_uploaded_file($file["tmp_name"], $json['file']);
            #//create_img($json['file'], $_, 960, 529);
            #$json['file'] = $_;
        } else {
            $_ = 'tmp/' . time() . '.jpg';
            move_uploaded_file($file["tmp_name"], $json['file']);

            #//create_img($json['file'], $_, 960, 529);
            # $json['file'] = $_;
        }
#        create_thumbnail($json['file'], $json['file'], 960, 529, "height");

        exit(json_encode($json));
    }
    $title = $SYSTEM['post']['title'];
    $scondition = $SYSTEM['post']['scondition'];
    $condition = $SYSTEM['post']['condition'];
    $tcondition = $SYSTEM['post']['tcondition'];
    $dcondition = $SYSTEM['post']['dcondition'];
    $site = $SYSTEM['post']['site'];
    $day = (int)$SYSTEM['post']['day'];
    $desire = $SYSTEM['post']['desire'] == 'y' ? 1 : 0;
    #$graph = $SYSTEM['post']['graph'];
    #$phone = $SYSTEM['post']['phone'];
    #$address = $SYSTEM['post']['address'];
    #$cost = $SYSTEM['post']['cost'];
    $preview = $SYSTEM['post']['preview'];
    $agreement = $SYSTEM['post']['agreement'];
    #$cat = $SYSTEM['post']['cat'];
    #$city = $SYSTEM['post']['city'];
    $dopImages = $SYSTEM['post']['images'];
    $yt = $SYSTEM['post']['yt'];
    $act = $SYSTEM['post']['act'];

    if (strlen($title) < 3) {
        $json['errors']['input[name="title"]'] = array('Короткое название', 'bottom');
    }
    if (strlen($condition) < 3) {
        $json['errors']['textarea[name="condition"]'] = array('Опишите более подробно', 'bottom');
    }
	/*
    foreach ($site as $k => $v) {
        //if ($v && !filter_var('http://' . $v, FILTER_VALIDATE_URL)) {
        $site[$k] = trim($v);
        //if ($v && !is_url($v)) {
            $json['errors']['input[name="site[' . $k . ']"]'] = array('Имеет некорректный формат', 'bottom');
        //}
    }
	*/
    if ($day < 1 || $day > 99) {
        $json['errors']['input[name="day"]'] = array('Минимум 1 день, максимум 99', 'bottom');
    }

    $cost_ = $day * $price;
    if ($desire) {
        $cost_ = $cost_ * 3;
    }
    $dopErr = '';
    /*if (count($cat) < 2) {
        $dopErr .= 'Необходимо выбрать больше категорий<br/>';
    }

    if (count($city) < 1) {
        $dopErr .= 'Город выбран некорректно<br/>';
    } elseif (count($city) > 20) {
        $dopErr .= 'Запрщено выбирать больше 20 городов<br/>';
    }*/
    foreach ($act as $i => $act_) {
        if ($i < 1 || $i > 3) {
            continue;
        }
        $enable = $act_[0];
        $url = $act_[1];
        if ($enable && !is_url($url)) {
            $json['errors']['#act' . $i . '1'] = array('Имеет некорректный формат', 'bottom');
        } elseif (!$enable) {
            $act[$i] = array(0, '');
        }
    }
    if (!$preview) {
        if (!$agreement) {
            $dopErr = 'Необходимо принять соглашение<br/>';
        }
        if ($cost_ > $USER['money']) {
            $dopErr .= 'Баланса недостаточно<br/>';
        }
    }
    $video_id = '';
    if ($yt && (!is_url($yt) || strpos('you', $yt) === false)) {
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $yt, $match)) {
            $video_id = $match[1];
        } else {
            $dopErr .= 'Была указана ссылка не на видео с yotube<br/>';
        }
    } else {
        $yt = '';
    }
    if (isset($json['errors']) || !empty($dopErr)) {
        if ($preview) {
            $json['errors']['#previewAction'] = array('Найдены ошибки', 'bottom');
        } else {
            $json['errors']['#runAction'] = array($dopErr . 'Найдены ошибки', 'bottom');
        }
    } else {
        $images = array();
		
        if ($dopImages) {
            $i = 4;
            foreach ($dopImages as $img) {
                $img = trim($img, '/');
				
                if (empty($img) || $img == '/') {
                    continue;
                }
                $t = getExtension($img);
                $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                copy($img, $name);
                $images[$i++] = $name;
            }
        }
        $SYSTEM['post']['images'] = $images;
		
        if ($preview) {
            $time = time();
            file_put_contents('tmp/' . $time . '.txt', serialize($SYSTEM['post']));
            $json['eval'][] = 'window.open("http://rightlike.ru/action/preview?token=' . $time . '");';
        } else {
            if (!empty($SYSTEM['post']['chooseImage1__'])) {
                $info = getimagesize($_SERVER['DOCUMENT_ROOT'] . "/" . $SYSTEM['post']['chooseImage1__']);
                if (preg_match("/^image/", $info['mime'])) {
                    $t = getExtension($SYSTEM['post']['chooseImage1__']);
                    $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                    copy($SYSTEM['post']['chooseImage1__'], $name);
                    $images[1] = $name;
                }
                if (preg_match("/^image/", $info['mime'])) {
                    $t = getExtension($SYSTEM['post']['chooseImage1___']);
                    $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                    copy($SYSTEM['post']['chooseImage1___'], $name);
                    $images[11] = $name;
                }
            }
            if (!empty($SYSTEM['post']['chooseImage2__'])) {
                $info = getimagesize($_SERVER['DOCUMENT_ROOT'] . "/" . $SYSTEM['post']['chooseImage2__']);
                if (preg_match("/^image/", $info['mime'])) {
                    $t = getExtension($SYSTEM['post']['chooseImage2__']);
                    $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                    copy($SYSTEM['post']['chooseImage2__'], $name);
                    $images[2] = $name;
                }
            }
            if (!empty($SYSTEM['post']['chooseImage3__'])) {
                $info = getimagesize($_SERVER['DOCUMENT_ROOT'] . "/" . $SYSTEM['post']['chooseImage3__']);
                if (preg_match("/^image/", $info['mime'])) {
                    $t = getExtension($SYSTEM['post']['chooseImage3__']);
                    $name = 'upload/action/' . microtime(true) . mt_rand(0, 999) . '.' . $t;
                    copy($SYSTEM['post']['chooseImage3__'], $name);
                    $images[3] = $name;
                }
            }
            $images[12] = $yt;
            $city[] = 0;
            if ($edit) {
                $id = resetComp($action_id, $title, $condition, $scondition, $tcondition, $dcondition, $act, $site, $day, $desire, $images);
            } else {
                $id = addComp($title, $condition, $scondition, $tcondition, $dcondition, $act, $site, $day, $desire, $images);
            }
            updateUserById($USER['id'], array('money'), array($USER['money'] - $cost_));
            forOwnerReferal($USER['referal'], $cost_);
            $json['eval'][] = 'window.location.href="/action/action?id=' . $id . '"';
        }
    }

    exit(json_encode($json));
}
#$v = file_get_contents('cat');
#$cat = unserialize($v);

$title = '';
$image1_ = '';
$image1__ = '';
$image2_ = '';
$image3_ = '';
$image4_ = '';
$image5_ = '';
$image6_ = '';
$image7_ = '';
$image8_ = '';
$condition = '';
$scondition = '';
$tcondition = '';
$dcondition = '';
$act = array(array(0, ''), array(0, ''), array(0, ''), array(0, ''));
$site = array(
    'main' => '',
    'gm' => '',
    'vk' => '',
    'fb' => '',
    'ok' => '',
    'tw' => '',
    'yt' => '',
    'ig' => '',
    'pr' => '',
    'over' => '',
    'group' => '',
);
$days = 10;
$desire = 'y';
$graph = '';
$phone = '';
$address = '';
$cost = array(
    'current' => 10,
    'discount' => 50,
    'new' => 1000
);
$city = array();
$cat = array();
$yt = '';
#####
if ($edit) {
    $title = $action_edit['title'];
    $image1_ = $action_edit['images'][1];
    $image1__ = $action_edit['images'][11];
    $image2_ = $action_edit['images'][2];
    $image3_ = $action_edit['images'][3];
    $image4_ = isset($action_edit['images'][4]) ? $action_edit['images'][4] : '';
    $image5_ = isset($action_edit['images'][5]) ? $action_edit['images'][5] : '';
    $image6_ = isset($action_edit['images'][6]) ? $action_edit['images'][6] : '';
    $image7_ = isset($action_edit['images'][7]) ? $action_edit['images'][7] : '';
    $image8_ = isset($action_edit['images'][8]) ? $action_edit['images'][8] : '';
    $condition = $action_edit['condition'];
    $scondition = $action_edit['scondition'];
    $tcondition = $action_edit['tcondition'];
    $dcondition = $action_edit['dcondition'];
    $site = $action_edit['site'];
    $days = ($action_edit['end'] - $action_edit['created']) / (60 * 60 * 24);
    $desire = $action_edit['desire'] ? 'y' : 'n';
    $graph = $action_edit['graph'];
    $phone = $action_edit['phone'];
    $address = $action_edit['address'];
    $cost = $action_edit['cost'];
    $city = $action_edit['city'];
    $cat = $action_edit['cat'];
    $yt = isset($action_edit['images'][12]) ? $action_edit['images'][12] : '';
    $act = $action_edit['act'] ? unserialize($action_edit['act']) : array(array(0, ''), array(0, ''), array(0, ''), array(0, ''));
}
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <h1>Запустить конкурс</h1>

            <form id="formAction" action="" method="POST">
                <div class="content_block rounded_8">
                    <h3 class="first">Название конкурса</h3>
                    <input autocomplete="off" type="text" name="title" class="inp inp_full rounded_3" placeholder="Введите заголовок" maxlength="64" value="<?php echo $title; ?>"/>

                    <div style="position: relative;float: right;margin-top: -35px;text-align: right;padding-right: 10px;">
                        <span id="countS" style="background-color: #ff6e56;color: #FFF;padding: 5px;">64</span>
                    </div>

                    <h3>
                        Вы можете выбрать до 3-х различных фотографий для загрузки<br/>
                        <span class="details">(Для загрузки обязательна только одна фотография, остальные по желанию)</span>
                    </h3>

                    <div>
                        <div class="action_photo_block">
                            <button class="btn rounded_8" img-src="/images/test_image.png" id="chooseImage1">Выберите файл</button>
                            <button style="display: none;" img-src="/images/avatar-main.jpg" id="chooseImage1_"></button>
                            <span class="photo_file">Файл не выбран</span>
                            <span class="photo_req">Обложка акции<br/>(обязательно)</span>
                        </div>

                        <div class="action_photo_block">
                            <button class="btn rounded_8" id="chooseImage2">Выберите файл</button>
                            <button style="display: none;" img-src="/images/avatar-main.jpg" id="chooseImage2_"></button>
                            <span class="photo_file">Файл не выбран</span>
                            <span class="photo_req">Не обязательно</span>
                        </div>

                        <div class="action_photo_block last">
                            <button class="btn rounded_8" id="chooseImage3">Выберите файл</button>
                            <button style="display: none;" img-src="/images/avatar-main.jpg" id="chooseImage3_"></button>
                            <span class="photo_file">Файл не выбран</span>
                            <span class="photo_req">Не обязательно</span>
                        </div>
                        <div class="clear"></div>
                        <input id="fileupload" type="file" name="file" style="display: none;">
                        <input id="fileupload2" type="file" name="file" style="display: none;">
                        <input id="chooseImage1__" type="hidden" name="chooseImage1__" value="<?php echo $image1_; ?>">
                        <input id="chooseImage1___" type="hidden" name="chooseImage1___" value="<?php echo $image1__; ?>">
                        <input id="chooseImage2__" type="hidden" name="chooseImage2__" value="<?php echo $image2_; ?>">
                        <input id="chooseImage3__" type="hidden" name="chooseImage3__" value="<?php echo $image3_; ?>">
                        <br/>
                        <span class="details">Вы можете выбрать дополнительные фотографии(необязательно):</span>

                        <div class="attachments input">
                            <a href="#" class="dop" data-img="/<?php echo $image4_; ?>"><img src="/<?php echo $image4_ ? $image4_ : 'images/no-image.png'; ?>"/></a>
                            <a href="#" class="dop" data-img="/<?php echo $image5_; ?>"><img src="/<?php echo $image5_ ? $image5_ : 'images/no-image.png'; ?>"/></a>
                            <a href="#" class="dop" data-img="/<?php echo $image6_; ?>"><img src="/<?php echo $image6_ ? $image6_ : 'images/no-image.png'; ?>"/></a>
                            <a href="#" class="dop" data-img="/<?php echo $image7_; ?>"><img src="/<?php echo $image7_ ? $image7_ : 'images/no-image.png'; ?>"/></a>
                            <a href="#" class="dop" data-img="/<?php echo $image8_; ?>"><img src="/<?php echo $image8_ ? $image8_ : 'images/no-image.png'; ?>"/></a>
                        </div>
                        <div style="display: none" id="inpForImg"></div>
                        <br/>
                        <span class="details">И ссылку на видео youtube(необязательно):</span>
                        <input autocomplete="off" type="text" name="yt" class="inp inp_full rounded_3" placeholder="Пример: https://www.youtube.com/embed/h9eWY6m2HAM" maxlength="64" value="<?php echo $yt; ?>"/>
                    </div>

                </div>

                <div class="content_block rounded_8">
                    <h3 class="first">Коротко о конкурсе:</h3>
                    <textarea name="scondition" class="inp inp_full rounded_3" rows="5" maxlength="512"
                              placeholder="Опишите коротко конкурс, данный текст будет выводится в малом блоке..."><?php echo $scondition; ?></textarea>

                    <h3>Описание конкурса:</h3>
                        <textarea name="condition" class="inp inp_full rounded_3" rows="5"
                                  placeholder="Опишите свой конкурса более детально, что вы продаете или предлагаете, подарок, а также прочие условия..."><?php echo $condition; ?></textarea>

                    <h3>
                        Укажите ссылки на ваш сайт, а также на ваши сообщества в социальных сетях:<br/>
                        <span class="details">(Не обязательный пункт)</span>
                    </h3>

                    <div class="site_block">
                        <div class="site_block_label"><label>Наш сайт: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[main]" class="inp_full" type="text" value="<?php echo $site['main']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в GM: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[gm]" class="inp_full" type="text" value="<?php echo $site['gm']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы ВКонтакте: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[vk]" class="inp_full" type="text" value="<?php echo $site['vk']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы на Facebook: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[fb]" class="inp_full" type="text" value="<?php echo $site['fb']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Одноклассниках: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[ok]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['ok']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Twitter: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[tw]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['tw']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы на Youtube: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[yt]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['yt']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Instagram: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[ig]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['ig']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Pinterest: <span class="link_start">www.</span></label></div>
                        <div class="site_block_link"><input name="site[pr]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['pr']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Viber: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[viber]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['viber']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в LinkedIn: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[linkedin]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['linkedin']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Whatsapp: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[whatsapp]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['whatsapp']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Foursquare: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[foursquare]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['foursquare']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Tumblr: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[tumblr]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['tumblr']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Skype: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[skype]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['skype']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Мы в Google+: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[googleplus]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['googleplus']; ?>"></div>
                    </div>

                    <div class="site_block">
                        <div class="site_block_label"><label>Email: <span class="link_start"></span></label></div>
                        <div class="site_block_link"><input name="site[email]" class="inp_full" type="text" style="width: 100%;" value="<?php echo $site['email']; ?>"></div>
                    </div>

                </div>

                <div class="content_block rounded_8">
                    <div class="column_half">
                        <h3 class="first">
                            Укажите на какое количество дней запускается конкурс, максимум 99 дней:<br/>
                            <span class="details">(по умолчанию 10 дней)</span>
                        </h3>

                        <div class="days_block">
                            <img src="/images/ico_clock.png">
                            <input id="days" name="day" type="text" class="inp inp-days rounded_3" size="2" value="<?php echo $days; ?>"/>
                            <label>дней</label>
                        </div>
                    </div>

                    <div class="column_half">
                        <h3 class="first">
                            Желаете ли вы запустить конкурс в баннере, в самом верху сайта главной страницы?<br/>
                            <span class="details">(От этого зависит стоимость запуска конкурса, по умолчанию стоит “Да, желаю”)</span>
                        </h3>

                        <div class="styled_checkbox desire_checkbox">
                            <label for="desire_y">Да, желаю</label> <input type="radio" id="desire_y" name="desire" value="y" <?php echo $desire == 'y' ? 'checked' : ''; ?>>
                            <label for="desire_n">Нет, не желаю</label> <input type="radio" id="desire_n" name="desire" value="n" <?php echo $desire == 'n' ? 'checked' : ''; ?>>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="content_block rounded_8 cb_worktime">
                    <h3 class="first">Выберите, что должен сделать пользователь, чтобы принять участие в вашем конкурсе<br/><span class="details">(можно выбрать не более 2-х вариантов)</span></h3>

                    <div class="m10">
                        <div class="styled_checkbox more desire_checkbox">
                            <label for="act1_y">Вступить в группу/сообщество (
                                <small>Допускаются сети: GM1LP, ВКонтакте, Одноклассники, Facebook, Google+</small>
                                )</label> <input type="checkbox" id="act1_y" name="act[1][0]" value="1" <?php echo isset($act[1]) && $act[1][0] ? 'checked' : ''; ?>>
                        </div>
                        <input id="act11" name="act[1][1]" type="text" class="inp inp_full rounded_3" placeholder="укажите ссылку, например: https://vk.com/team" value="<?php echo $act[1][1]; ?>"/>
                    </div>
                    <div class="m10">
                        <div class="styled_checkbox more desire_checkbox">
                            <label for="act2_y">Подписаться на страницу (
                                <small>Допускаются сети: GM1LP, ВКонтакте, Одноклассники, Facebook, Google+</small>
                                )</label> <input type="checkbox" id="act2_y" name="act[2][0]" value="2" <?php echo isset($act[2]) && $act[2][0] ? 'checked' : ''; ?>>
                        </div>
                        <input id="act21" name="act[2][1]" type="text" class="inp inp_full rounded_3" placeholder="укажите ссылку, например: https://vk.com/team" value="<?php echo $act[2][1]; ?>"/>
                    </div>
                    <div class="m10" style="margin-bottom: 0">
                        <div class="styled_checkbox more desire_checkbox">
                            <label for="act3_y">Поделиться записью(репост) (
                                <small>Допускаются сети: GM1LP, ВКонтакте, Одноклассники, Facebook, Twitter Google+</small>
                                )</label> <input type="checkbox" id="act3_y" name="act[3][0]" value="3" <?php echo isset($act[3]) && $act[3][0] ? 'checked' : ''; ?>>
                        </div>
                        <input id="act31" name="act[3][1]" type="text" class="inp inp_full rounded_3" placeholder="укажите ссылку, например: https://vk.com/team?wall1_1" value="<?php echo $act[3][1]; ?>"/>
                    </div>
                    <h3>Дополнительные условия конкурса<br/><span class="details">(не обязательный пункт)</span></h3>
                    <textarea name="dcondition" class="inp inp_full rounded_3" rows="13" maxlength="1024"
                              placeholder="Напишите здесь любое дополнительное условие или пожелание.

Например:
-Установить в статус до «такого-то» числа «такой-то» текст.
-Установить себе на аватарку, аватарку «такой-то группы»
-Закрепить конкурс у себя на стене

И т.д., все зависит лишь от ваших пожеланий и фантазии, но главное - не переборщите. Пользователи не любят, когда им
навязывают сразу множество условий...

И конечно же - удачи!
Ваш GM.
"><?php echo $dcondition; ?></textarea>
                    <br/>
                    <!--- --->
                    <h3>Укажите текст, который будет размещен от имени пользователя и который увидят все его друзья</h3>
                    <textarea name="tcondition" class="inp inp_full rounded_3" rows="5" maxlength="512"
                              placeholder="Текст который разместит участник..."><?php echo $tcondition; ?></textarea>
                    <br/>

                    <div class="text-center">
                        <img src="/images/exl.jpg"/>
                    </div>
                    <div class="clear"></div>

                    <!--h3>
                        Укажите город, в котором будет проходить данная акция, если акция будет проходить в нескольких городах,
                        то укажите их по порядку <span class="details">(не более 20)</span>
                    </h3>
                    <select name="city[]" data-placeholder="Город или несколько городов..." class="chosen-select" multiple style="width:728px;" tabindex="4">
                        <?php
                    if ($edit) {
                        foreach ($action_edit['city'] as $cid) {

                            $_ = getCityById($cid);
                            echo '<option value="' . $cid . '" selected>' . $_['text'] . '</option>';
                        }
                    }
                    /* $city = //getCity2();
                     foreach ($city as $_) {
                         echo '<option value="' . $_['city_id'] . '">' . $_['name'] . '</option>';
                     }*/
                    ?>
                    </select-->
                </div>
                <!--
                                <div class="content_block rounded_8"><h3 class="first"> Выберите наиболее подходящую вам категорию, в которой будет отражаться ваша акция: <span class="details">(Обязательный пункт, минимум одна категория с подкатегорией)</span></h3>

                                    <div class="column_third column_categories">
                                        <div class="categories_checkboxes styled_checkbox"><label for="category_1">Для неё</label>
                                            <input type="radio" class="rounded_5" id="category_1" name="categories" value="1"><label for="category_2">Для него</label>
                                            <input type="radio" class="rounded_5" id="category_2" name="categories" value="2"><label for="category_3">Детям</label>
                                            <input type="radio" class="rounded_5" id="category_3" name="categories" value="3"><label for="category_4">Для дома</label>
                                            <input type="radio" class="rounded_5" id="category_4" name="categories" value="4"><label for="category_5">Техника</label>
                                            <input type="radio" class="rounded_5" id="category_5" name="categories" value="5"><label for="category_6">Красота</label>
                                            <input type="radio" class="rounded_5" id="category_6" name="categories" value="6"><label for="category_7">Услуги</label>
                                            <input type="radio" class="rounded_5" id="category_7" name="categories" value="7"><label for="category_8">Отели</label>
                                            <input type="radio" class="rounded_5" id="category_8" name="categories" value="8"><label for="category_9">Туры</label>
                                            <input type="radio" class="rounded_5" id="category_9" name="categories" value="9"><label for="category_10">Магазины</label>
                                            <input type="radio" class="rounded_5" id="category_10" name="categories" value="10"><label for="category_11">Кафе/бары</label>
                                            <input type="radio" class="rounded_5" id="category_11" name="categories" value="11"></div>
                                    </div>
                                    <div id="tab_1" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat1">Одежда</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat1" value="1"/>
                                                <label for="cat2" data-cat="1">Большие размеры</label>
                                                <input type="checkbox" name="cat[]" id="cat2" value="2"/>
                                                <label for="cat3" data-cat="1">Брюки</label>
                                                <input type="checkbox" name="cat[]" id="cat3" value="3"/>
                                                <label for="cat4" data-cat="1">Верхняя одежда</label>
                                                <input type="checkbox" name="cat[]" id="cat4" value="4"/>
                                                <label for="cat5" data-cat="1">Джемперы, свитеры</label>
                                                <input type="checkbox" name="cat[]" id="cat5" value="5"/>
                                                <label for="cat6" data-cat="1">Джинсы</label>
                                                <input type="checkbox" name="cat[]" id="cat6" value="6"/>
                                                <label for="cat7" data-cat="1">Костюмы, пиджаки</label>
                                                <input type="checkbox" name="cat[]" id="cat7" value="7"/>
                                                <label for="cat8" data-cat="1">Кофты, водолазки</label>
                                                <input type="checkbox" name="cat[]" id="cat8" value="8"/>
                                                <label for="cat9" data-cat="1">Одежда для дома</label>
                                                <input type="checkbox" name="cat[]" id="cat9" value="9"/>
                                                <label for="cat10" data-cat="1">Платья</label>
                                                <input type="checkbox" name="cat[]" id="cat10" value="10"/>
                                                <label for="cat11" data-cat="1">Спорт</label>
                                                <input type="checkbox" name="cat[]" id="cat11" value="11"/>
                                                <label for="cat12" data-cat="1">Толстовки</label>
                                                <input type="checkbox" name="cat[]" id="cat12" value="12"/>
                                                <label for="cat13" data-cat="1">Туники</label>
                                                <input type="checkbox" name="cat[]" id="cat13" value="13"/>
                                                <label for="cat14" data-cat="1">Футболки, поло</label>
                                                <input type="checkbox" name="cat[]" id="cat14" value="14"/>
                                                <label for="cat15" data-cat="1">Шорты</label>
                                                <input type="checkbox" name="cat[]" id="cat15" value="15"/>
                                                <label for="cat16" data-cat="1">Юбки</label>
                                                <input type="checkbox" name="cat[]" id="cat16" value="16"/>
                                                <label for="cat17" data-cat="1">Блузки, рубашки</label>
                                                <input type="checkbox" name="cat[]" id="cat17" value="17"/>
                                            </div>

                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat41">Аксессуары</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat41" value="41"/>
                                                <label for="cat42" data-cat="1">Бижутерия</label>
                                                <input type="checkbox" name="cat[]" id="cat42" value="42"/>
                                                <label for="cat43" data-cat="1">Головные уборы</label>
                                                <input type="checkbox" name="cat[]" id="cat43" value="43"/>
                                                <label for="cat44" data-cat="1">Зонты</label>
                                                <input type="checkbox" name="cat[]" id="cat44" value="44"/>
                                                <label for="cat45" data-cat="1">Кошельки</label>
                                                <input type="checkbox" name="cat[]" id="cat45" value="45"/>
                                                <label for="cat46" data-cat="1">Очки</label>
                                                <input type="checkbox" name="cat[]" id="cat46" value="46"/>
                                                <label for="cat47" data-cat="1">Перчатки</label>
                                                <input type="checkbox" name="cat[]" id="cat47" value="47"/>
                                                <label for="cat48" data-cat="1">Платки, шарфы</label>
                                                <input type="checkbox" name="cat[]" id="cat48" value="48"/>
                                                <label for="cat49" data-cat="1">Ремни</label>
                                                <input type="checkbox" name="cat[]" id="cat49" value="49"/>
                                                <label for="cat50" data-cat="1">Сумки, клатчи</label>
                                                <input type="checkbox" name="cat[]" id="cat50" value="50"/>
                                                <label for="cat51" data-cat="1">Часы</label>
                                                <input type="checkbox" name="cat[]" id="cat51" value="51"/>
                                                <label for="cat335" data-cat="1">Золотые украшения</label>
                                                <input type="checkbox" name="cat[]" id="cat335" value="335"/>
                                                <label for="cat336" data-cat="1">Серьги</label>
                                                <input type="checkbox" name="cat[]" id="cat336" value="336"/>
                                                <label for="cat337" data-cat="1">Кольца</label>
                                                <input type="checkbox" name="cat[]" id="cat337" value="337"/>
                                                <label for="cat338" data-cat="1">Цепи и браслеты</label>
                                                <input type="checkbox" name="cat[]" id="cat338" value="338"/>
                                                <label for="cat339" data-cat="1">Цепочки</label>
                                                <input type="checkbox" name="cat[]" id="cat339" value="339"/>
                                                <label for="cat340" data-cat="1">Шкатулки</label>
                                                <input type="checkbox" name="cat[]" id="cat340" value="340"/>
                                                <label for="cat341" data-cat="1">Броши</label>
                                                <input type="checkbox" name="cat[]" id="cat341" value="341"/>
                                                <label for="cat342" data-cat="1">Калье</label>
                                                <input type="checkbox" name="cat[]" id="cat342" value="342"/>
                                                <label for="cat343" data-cat="1">Подвески</label>
                                                <input type="checkbox" name="cat[]" id="cat343" value="343"/>
                                                <label for="cat344" data-cat="1">Кресты и иконки</label>
                                                <input type="checkbox" name="cat[]" id="cat344" value="344"/>
                                            </div>
                                        </div>
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat19">Обувь</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat19" value="19"/>
                                                <label for="cat20" data-cat="1">Сапоги</label>
                                                <input type="checkbox" name="cat[]" id="cat20" value="20"/>
                                                <label for="cat21" data-cat="1">Ботинки</label>
                                                <input type="checkbox" name="cat[]" id="cat21" value="21"/>
                                                <label for="cat22" data-cat="1">Балетки</label>
                                                <input type="checkbox" name="cat[]" id="cat22" value="22"/>
                                                <label for="cat23" data-cat="1">Босоножки</label>
                                                <input type="checkbox" name="cat[]" id="cat23" value="23"/>
                                                <label for="cat24" data-cat="1">Ботильоны</label>
                                                <input type="checkbox" name="cat[]" id="cat24" value="24"/>
                                                <label for="cat25" data-cat="1">Кеды, кроссовки</label>
                                                <input type="checkbox" name="cat[]" id="cat25" value="25"/>
                                                <label for="cat26" data-cat="1">Мокасины</label>
                                                <input type="checkbox" name="cat[]" id="cat26" value="26"/>
                                                <label for="cat27" data-cat="1">Тапочки</label>
                                                <input type="checkbox" name="cat[]" id="cat27" value="27"/>
                                                <label for="cat28" data-cat="1">Туфли</label>
                                                <input type="checkbox" name="cat[]" id="cat28" value="28"/>
                                                <label for="cat29" data-cat="1">Шлепанцы, сандалии</label>
                                                <input type="checkbox" name="cat[]" id="cat29" value="29"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat30">Для авто</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat30" value="30"/>
                                                <label for="cat31" data-cat="1">Запчасти и аксессуары</label>
                                                <input type="checkbox" name="cat[]" id="cat31" value="31"/>
                                                <label for="cat32" data-cat="1">Навигаторы</label>
                                                <input type="checkbox" name="cat[]" id="cat32" value="32"/>
                                                <label for="cat33" data-cat="1">Тонировка</label>
                                                <input type="checkbox" name="cat[]" id="cat33" value="33"/>
                                                <label for="cat34" data-cat="1">Карбоновая пленка</label>
                                                <input type="checkbox" name="cat[]" id="cat34" value="34"/>
                                                <label for="cat35" data-cat="1">Регистраторы</label>
                                                <input type="checkbox" name="cat[]" id="cat35" value="35"/>
                                                <label for="cat36" data-cat="1">Сигнализация</label>
                                                <input type="checkbox" name="cat[]" id="cat36" value="36"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat345">Спорт</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat345" value="345"/>
                                                <label for="cat346" data-cat="1">Абонементы</label>
                                                <input type="checkbox" name="cat[]" id="cat346" value="346"/>
                                                <label for="cat347" data-cat="1">Тренажерные залы</label>
                                                <input type="checkbox" name="cat[]" id="cat347" value="347"/>
                                                <label for="cat348" data-cat="1">Плавание</label>
                                                <input type="checkbox" name="cat[]" id="cat348" value="348"/>
                                                <label for="cat349" data-cat="1">Танцы</label>
                                                <input type="checkbox" name="cat[]" id="cat349" value="349"/>
                                                <label for="cat350" data-cat="1">Пластика</label>
                                                <input type="checkbox" name="cat[]" id="cat350" value="350"/>
                                                <label for="cat351" data-cat="1">Спортивное питание</label>
                                                <input type="checkbox" name="cat[]" id="cat351" value="351"/>
                                                <label for="cat352" data-cat="1">Аминокислоты</label>
                                                <input type="checkbox" name="cat[]" id="cat352" value="352"/>
                                                <label for="cat353" data-cat="1">Трежеры</label>
                                                <input type="checkbox" name="cat[]" id="cat353" value="353"/>
                                                <label for="cat354" data-cat="1">Спортивное оборудование</label>
                                                <input type="checkbox" name="cat[]" id="cat354" value="354"/>
                                                <label for="cat355" data-cat="1">Одежда для тренировок</label>
                                                <input type="checkbox" name="cat[]" id="cat355" value="355"/>
                                                <label for="cat356" data-cat="1">Индивидуальная защита <br> от травм</label>
                                                <input type="checkbox" name="cat[]" id="cat356" value="356"/>
                                                <label for="cat357" data-cat="1">Индивидуальный тренер</label>
                                                <input type="checkbox" name="cat[]" id="cat357" value="357"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat37">Белье</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat37" value="37"/>
                                                <label for="cat38" data-cat="1">Колготки, носки</label>
                                                <input type="checkbox" name="cat[]" id="cat38" value="38"/>
                                                <label for="cat39" data-cat="1">Купальники</label>
                                                <input type="checkbox" name="cat[]" id="cat39" value="39"/>
                                                <label for="cat40" data-cat="1">Эротическое белье</label>
                                                <input type="checkbox" name="cat[]" id="cat40" value="40"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat18">Прочее</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat18" value="18"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_2" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat52">Одежда</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat52" value="52"/>
                                                <label for="cat53" data-cat="2">Брюки</label>
                                                <input type="checkbox" name="cat[]" id="cat53" value="53"/>
                                                <label for="cat54" data-cat="2">Верхняя одежда</label>
                                                <input type="checkbox" name="cat[]" id="cat54" value="54"/>
                                                <label for="cat55" data-cat="2">Джемперы, свитеры</label>
                                                <input type="checkbox" name="cat[]" id="cat55" value="55"/>
                                                <label for="cat56" data-cat="2">Джинсы</label>
                                                <input type="checkbox" name="cat[]" id="cat56" value="56"/>
                                                <label for="cat57" data-cat="2">Костюмы, пиджаки</label>
                                                <input type="checkbox" name="cat[]" id="cat57" value="57"/>
                                                <label for="cat58" data-cat="2">Одежда для дома</label>
                                                <input type="checkbox" name="cat[]" id="cat58" value="58"/>
                                                <label for="cat59" data-cat="2">Рубашки</label>
                                                <input type="checkbox" name="cat[]" id="cat59" value="59"/>
                                                <label for="cat60" data-cat="2">Спорт</label>
                                                <input type="checkbox" name="cat[]" id="cat60" value="60"/>
                                                <label for="cat61" data-cat="2">Толстовки</label>
                                                <input type="checkbox" name="cat[]" id="cat61" value="61"/>
                                                <label for="cat62" data-cat="2">Футболки, поло</label>
                                                <input type="checkbox" name="cat[]" id="cat62" value="62"/>
                                                <label for="cat63" data-cat="2">Шорты</label>
                                                <input type="checkbox" name="cat[]" id="cat63" value="63"/>
                                            </div>

                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat83">Аксессуары</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat83" value="83"/>
                                                <label for="cat360" data-cat="2">Бижутерия</label>
                                                <input type="checkbox" name="cat[]" id="cat360" value="360"/>
                                                <label for="cat85" data-cat="2">Головные уборы</label>
                                                <input type="checkbox" name="cat[]" id="cat85" value="85"/>
                                                <label for="cat86" data-cat="2">Зонты</label>
                                                <input type="checkbox" name="cat[]" id="cat86" value="86"/>
                                                <label for="cat361" data-cat="2">Кошельки</label>
                                                <input type="checkbox" name="cat[]" id="cat361" value="361"/>
                                                <label for="cat87" data-cat="2">Очки</label>
                                                <input type="checkbox" name="cat[]" id="cat87" value="87"/>
                                                <label for="cat88" data-cat="2">Перчатки</label>
                                                <input type="checkbox" name="cat[]" id="cat88" value="88"/>
                                                <label for="cat362" data-cat="2">Платки, шарфы</label>
                                                <input type="checkbox" name="cat[]" id="cat362" value="362"/>
                                                <label for="cat89" data-cat="2">Портмоне</label>
                                                <input type="checkbox" name="cat[]" id="cat89" value="89"/>
                                                <label for="cat90" data-cat="2">Ремни</label>
                                                <input type="checkbox" name="cat[]" id="cat90" value="90"/>
                                                <label for="cat91" data-cat="2">Сумки, портфели</label>
                                                <input type="checkbox" name="cat[]" id="cat91" value="91"/>
                                                <label for="cat92" data-cat="2">Часы</label>
                                                <input type="checkbox" name="cat[]" id="cat92" value="92"/>
                                                <label for="cat363" data-cat="2">Золотые украшения</label>
                                                <input type="checkbox" name="cat[]" id="cat363" value="363"/>
                                                <label for="cat364" data-cat="2">Серьги</label>
                                                <input type="checkbox" name="cat[]" id="cat364" value="364"/>
                                                <label for="cat365" data-cat="2">Кольца</label>
                                                <input type="checkbox" name="cat[]" id="cat365" value="365"/>
                                                <label for="cat366" data-cat="2">Цепи и браслеты</label>
                                                <input type="checkbox" name="cat[]" id="cat366" value="366"/>
                                                <label for="cat367" data-cat="2">Цепочки</label>
                                                <input type="checkbox" name="cat[]" id="cat367" value="367"/>
                                                <label for="cat368" data-cat="2">Шкатулки</label>
                                                <input type="checkbox" name="cat[]" id="cat368" value="368"/>
                                                <label for="cat369" data-cat="2">Броши</label>
                                                <input type="checkbox" name="cat[]" id="cat369" value="369"/>
                                                <label for="cat370" data-cat="2">Калье</label>
                                                <input type="checkbox" name="cat[]" id="cat370" value="370"/>
                                                <label for="cat371" data-cat="2">Подвески</label>
                                                <input type="checkbox" name="cat[]" id="cat371" value="371"/>
                                                <label for="cat372" data-cat="2">Кресты и иконки</label>
                                                <input type="checkbox" name="cat[]" id="cat372" value="372"/>
                                            </div>
                                        </div>
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat65">Обувь</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat65" value="65"/>
                                                <label for="cat66" data-cat="2">Ботинки</label>
                                                <input type="checkbox" name="cat[]" id="cat66" value="66"/>
                                                <label for="cat67" data-cat="2">Кеды, кроссовки</label>
                                                <input type="checkbox" name="cat[]" id="cat67" value="67"/>
                                                <label for="cat68" data-cat="2">Мокасины</label>
                                                <input type="checkbox" name="cat[]" id="cat68" value="68"/>
                                                <label for="cat69" data-cat="2">Сапоги</label>
                                                <input type="checkbox" name="cat[]" id="cat69" value="69"/>
                                                <label for="cat70" data-cat="2">Тапочки</label>
                                                <input type="checkbox" name="cat[]" id="cat70" value="70"/>
                                                <label for="cat71" data-cat="2">Туфли</label>
                                                <input type="checkbox" name="cat[]" id="cat71" value="71"/>
                                                <label for="cat72" data-cat="2">Шлепанцы, сандалии</label>
                                                <input type="checkbox" name="cat[]" id="cat72" value="72"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat73">Для авто</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat73" value="73"/>
                                                <label for="cat74" data-cat="2">Запчасти и аксессуары</label>
                                                <input type="checkbox" name="cat[]" id="cat74" value="74"/>
                                                <label for="cat75" data-cat="2">Навигаторы</label>
                                                <input type="checkbox" name="cat[]" id="cat75" value="75"/>
                                                <label for="cat76" data-cat="2">Тонировка</label>
                                                <input type="checkbox" name="cat[]" id="cat76" value="76"/>
                                                <label for="cat77" data-cat="2">Карбоновая пленка</label>
                                                <input type="checkbox" name="cat[]" id="cat77" value="77"/>
                                                <label for="cat78" data-cat="2">Регистраторы</label>
                                                <input type="checkbox" name="cat[]" id="cat78" value="78"/>
                                                <label for="cat79" data-cat="2">Сигнализация</label>
                                                <input type="checkbox" name="cat[]" id="cat79" value="79"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat534">Спорт</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat534" value="534"/>
                                                <label for="cat373" data-cat="2">Абонементы</label>
                                                <input type="checkbox" name="cat[]" id="cat373" value="373"/>
                                                <label for="cat374" data-cat="2">Тренажерные залы</label>
                                                <input type="checkbox" name="cat[]" id="cat374" value="374"/>
                                                <label for="cat375" data-cat="2">Плавание</label>
                                                <input type="checkbox" name="cat[]" id="cat375" value="375"/>
                                                <label for="cat376" data-cat="2">Танцы</label>
                                                <input type="checkbox" name="cat[]" id="cat376" value="376"/>
                                                <label for="cat377" data-cat="2">Пластика</label>
                                                <input type="checkbox" name="cat[]" id="cat377" value="377"/>
                                                <label for="cat378" data-cat="2">Спортивное питание</label>
                                                <input type="checkbox" name="cat[]" id="cat378" value="378"/>
                                                <label for="cat379" data-cat="2">Аминокислоты</label>
                                                <input type="checkbox" name="cat[]" id="cat379" value="379"/>
                                                <label for="cat380" data-cat="2">Тренажеры</label>
                                                <input type="checkbox" name="cat[]" id="cat380" value="380"/>
                                                <label for="cat381" data-cat="2">Спортивное оборудование</label>
                                                <input type="checkbox" name="cat[]" id="cat381" value="381"/>
                                                <label for="cat382" data-cat="2">Одежда для тренировок</label>
                                                <input type="checkbox" name="cat[]" id="cat382" value="382"/>
                                                <label for="cat383" data-cat="2">Индивидуальная защита<br> от травм</label>
                                                <input type="checkbox" name="cat[]" id="cat383" value="383"/>
                                                <label for="cat384" data-cat="2">Индивидуальный тренер</label>
                                                <input type="checkbox" name="cat[]" id="cat384" value="384"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat80">Белье</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat80" value="80"/>
                                                <label for="cat81" data-cat="2">Нижнее белье</label>
                                                <input type="checkbox" name="cat[]" id="cat81" value="81"/>
                                                <label for="cat358" data-cat="2">Эротическое белье</label>
                                                <input type="checkbox" name="cat[]" id="cat358" value="358"/>
                                                <label for="cat359" data-cat="2">Плавки</label>
                                                <input type="checkbox" name="cat[]" id="cat359" value="359"/>
                                                <label for="cat82" data-cat="2">Носки</label>
                                                <input type="checkbox" name="cat[]" id="cat82" value="82"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat64">Прочее</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat64" value="64"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_3" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat124">Обувь для мальчиков</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat124" value="124"/>
                                                <label for="cat125" data-cat="3">Ботинки, полуботинки</label>
                                                <input type="checkbox" name="cat[]" id="cat125" value="125"/>
                                                <label for="cat126" data-cat="3">Валенки</label>
                                                <input type="checkbox" name="cat[]" id="cat126" value="126"/>
                                                <label for="cat127" data-cat="3">Кеды</label>
                                                <input type="checkbox" name="cat[]" id="cat127" value="127"/>
                                                <label for="cat128" data-cat="3">Кроссовки</label>
                                                <input type="checkbox" name="cat[]" id="cat128" value="128"/>
                                                <label for="cat129" data-cat="3">Мокасины</label>
                                                <input type="checkbox" name="cat[]" id="cat129" value="129"/>
                                                <label for="cat130" data-cat="3">Орто-обувь</label>
                                                <input type="checkbox" name="cat[]" id="cat130" value="130"/>
                                                <label for="cat131" data-cat="3">Пинетки, первый шаг</label>
                                                <input type="checkbox" name="cat[]" id="cat131" value="131"/>
                                                <label for="cat132" data-cat="3">Резиновые сапоги</label>
                                                <input type="checkbox" name="cat[]" id="cat132" value="132"/>
                                                <label for="cat133" data-cat="3">Сандалии</label>
                                                <input type="checkbox" name="cat[]" id="cat133" value="133"/>
                                                <label for="cat134" data-cat="3">Сапоги</label>
                                                <input type="checkbox" name="cat[]" id="cat134" value="134"/>
                                                <label for="cat135" data-cat="3">Сланцы</label>
                                                <input type="checkbox" name="cat[]" id="cat135" value="135"/>
                                                <label for="cat136" data-cat="3">Тапочки</label>
                                                <input type="checkbox" name="cat[]" id="cat136" value="136"/>
                                                <label for="cat137" data-cat="3">Туфли</label>
                                                <input type="checkbox" name="cat[]" id="cat137" value="137"/>
                                                <label for="cat138" data-cat="3">Угги, унты</label>
                                                <input type="checkbox" name="cat[]" id="cat138" value="138"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat150">Обувь для девочек</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat150" value="150"/>
                                                <label for="cat151" data-cat="3">Ботинки, полуботинки</label>
                                                <input type="checkbox" name="cat[]" id="cat151" value="151"/>
                                                <label for="cat152" data-cat="3">Валенки</label>
                                                <input type="checkbox" name="cat[]" id="cat152" value="152"/>
                                                <label for="cat153" data-cat="3">Кеды</label>
                                                <input type="checkbox" name="cat[]" id="cat153" value="153"/>
                                                <label for="cat154" data-cat="3">Кроссовки</label>
                                                <input type="checkbox" name="cat[]" id="cat154" value="154"/>
                                                <label for="cat155" data-cat="3">Мокасины</label>
                                                <input type="checkbox" name="cat[]" id="cat155" value="155"/>
                                                <label for="cat156" data-cat="3">Орто-обувь</label>
                                                <input type="checkbox" name="cat[]" id="cat156" value="156"/>
                                                <label for="cat157" data-cat="3">Пинетки, первый шаг</label>
                                                <input type="checkbox" name="cat[]" id="cat157" value="157"/>
                                                <label for="cat158" data-cat="3">Резиновые сапоги</label>
                                                <input type="checkbox" name="cat[]" id="cat158" value="158"/>
                                                <label for="cat159" data-cat="3">Сандалии, босоножки</label>
                                                <input type="checkbox" name="cat[]" id="cat159" value="159"/>
                                                <label for="cat160" data-cat="3">Сапоги</label>
                                                <input type="checkbox" name="cat[]" id="cat160" value="160"/>
                                                <label for="cat161" data-cat="3">Сланцы</label>
                                                <input type="checkbox" name="cat[]" id="cat161" value="161"/>
                                                <label for="cat162" data-cat="3">Тапочки, балетки</label>
                                                <input type="checkbox" name="cat[]" id="cat162" value="162"/>
                                                <label for="cat163" data-cat="3">Туфли</label>
                                                <input type="checkbox" name="cat[]" id="cat163" value="163"/>
                                                <label for="cat164" data-cat="3">Угги, унты</label>
                                                <input type="checkbox" name="cat[]" id="cat164" value="164"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat99">Игрушки</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat99" value="99"/>
                                                <label for="cat100" data-cat="3">Деревянные игрушки</label>
                                                <input type="checkbox" name="cat[]" id="cat100" value="100"/>
                                                <label for="cat101" data-cat="3">Игровые наборы</label>
                                                <input type="checkbox" name="cat[]" id="cat101" value="101"/>
                                                <label for="cat102" data-cat="3">Конструкторы</label>
                                                <input type="checkbox" name="cat[]" id="cat102" value="102"/>
                                                <label for="cat103" data-cat="3">Куклы и аксессуары</label>
                                                <input type="checkbox" name="cat[]" id="cat103" value="103"/>
                                                <label for="cat104" data-cat="3">Машинки, ж/д дороги</label>
                                                <input type="checkbox" name="cat[]" id="cat104" value="104"/>
                                                <label for="cat105" data-cat="3">Музыкальные игрушки</label>
                                                <input type="checkbox" name="cat[]" id="cat105" value="105"/>
                                                <label for="cat106" data-cat="3">Мягкие игрушки</label>
                                                <input type="checkbox" name="cat[]" id="cat106" value="106"/>
                                                <label for="cat107" data-cat="3">Наборы для творчества</label>
                                                <input type="checkbox" name="cat[]" id="cat107" value="107"/>
                                                <label for="cat108" data-cat="3">Настольные игры</label>
                                                <input type="checkbox" name="cat[]" id="cat108" value="108"/>
                                                <label for="cat109" data-cat="3">Радиоуправляемые игрушки</label>
                                                <input type="checkbox" name="cat[]" id="cat109" value="109"/>
                                                <label for="cat110" data-cat="3">Развивающие игрушки</label>
                                                <input type="checkbox" name="cat[]" id="cat110" value="110"/>
                                                <label for="cat111" data-cat="3">Роботы, трансформеры</label>
                                                <input type="checkbox" name="cat[]" id="cat111" value="111"/>
                                            </div>
                                        </div>
                                        <div class="column_third subcategory">
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat140">Одежда для девочек</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat140" value="140"/>
                                                <label for="cat141" data-cat="3">Верхняя одежда</label>
                                                <input type="checkbox" name="cat[]" id="cat141" value="141"/>
                                                <label for="cat142" data-cat="3">Белье, носки, колготки</label>
                                                <input type="checkbox" name="cat[]" id="cat142" value="142"/>
                                                <label for="cat143" data-cat="3">Джинсы, брюки</label>
                                                <input type="checkbox" name="cat[]" id="cat143" value="143"/>
                                                <label for="cat144" data-cat="3">Для дома и отдыха</label>
                                                <input type="checkbox" name="cat[]" id="cat144" value="144"/>
                                                <label for="cat145" data-cat="3">Костюмы, комбинезоны</label>
                                                <input type="checkbox" name="cat[]" id="cat145" value="145"/>
                                                <label for="cat146" data-cat="3">Кофты, джемперы, свитеры</label>
                                                <input type="checkbox" name="cat[]" id="cat146" value="146"/>
                                                <label for="cat147" data-cat="3">Платья, блузки</label>
                                                <input type="checkbox" name="cat[]" id="cat147" value="147"/>
                                                <label for="cat148" data-cat="3">Футболки, топы</label>
                                                <input type="checkbox" name="cat[]" id="cat148" value="148"/>
                                                <label for="cat149" data-cat="3">Юбки, шорты</label>
                                                <input type="checkbox" name="cat[]" id="cat149" value="149"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat115">Одежда для мальчиков</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat115" value="115"/>
                                                <label for="cat116" data-cat="3">Рубашки</label>
                                                <input type="checkbox" name="cat[]" id="cat116" value="116"/>
                                                <label for="cat117" data-cat="3">Белье, носки, колготки</label>
                                                <input type="checkbox" name="cat[]" id="cat117" value="117"/>
                                                <label for="cat118" data-cat="3">Верхняя одежда</label>
                                                <input type="checkbox" name="cat[]" id="cat118" value="118"/>
                                                <label for="cat119" data-cat="3">Джинсы, брюки</label>
                                                <input type="checkbox" name="cat[]" id="cat119" value="119"/>
                                                <label for="cat120" data-cat="3">Для дома и отдыха</label>
                                                <input type="checkbox" name="cat[]" id="cat120" value="120"/>
                                                <label for="cat121" data-cat="3">Костюмы, комбинезоны</label>
                                                <input type="checkbox" name="cat[]" id="cat121" value="121"/>
                                                <label for="cat122" data-cat="3">Кофты, джемперы, свитеры</label>
                                                <input type="checkbox" name="cat[]" id="cat122" value="122"/>
                                                <label for="cat123" data-cat="3">Футболки, шорты</label>
                                                <input type="checkbox" name="cat[]" id="cat123" value="123"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat93">Новорожденным</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat93" value="93"/>
                                                <label for="cat94" data-cat="3">Боди, песочники</label>
                                                <input type="checkbox" name="cat[]" id="cat94" value="94"/>
                                                <label for="cat95" data-cat="3">Верхняя одежда</label>
                                                <input type="checkbox" name="cat[]" id="cat95" value="95"/>
                                                <label for="cat96" data-cat="3">Комбинезоны</label>
                                                <input type="checkbox" name="cat[]" id="cat96" value="96"/>
                                                <label for="cat97" data-cat="3">Ползунки, штанишки</label>
                                                <input type="checkbox" name="cat[]" id="cat97" value="97"/>
                                                <label for="cat98" data-cat="3">Распашонки, кофты</label>
                                                <input type="checkbox" name="cat[]" id="cat98" value="98"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat139">Аксессуары</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat139" value="139"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat114">Спорт</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat114" value="114"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat112">Для детской</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat112" value="112"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat113">Прочее</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat113" value="113"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_4" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat187">Кухня</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat187" value="187"/>
                                                <label for="cat188" data-cat="4">Полотенца и фартуки</label>
                                                <input type="checkbox" name="cat[]" id="cat188" value="188"/>
                                                <label for="cat189" data-cat="4">Скатерти</label>
                                                <input type="checkbox" name="cat[]" id="cat189" value="189"/>
                                                <label for="cat190" data-cat="4">Сковороды</label>
                                                <input type="checkbox" name="cat[]" id="cat190" value="190"/>
                                                <label for="cat191" data-cat="4">Столовые приборы</label>
                                                <input type="checkbox" name="cat[]" id="cat191" value="191"/>
                                                <label for="cat192" data-cat="4">Хранение</label>
                                                <input type="checkbox" name="cat[]" id="cat192" value="192"/>
                                                <label for="cat193" data-cat="4">Шторы и занавески</label>
                                                <input type="checkbox" name="cat[]" id="cat193" value="193"/>
                                                <label for="cat194" data-cat="4">Ванная комната</label>
                                                <input type="checkbox" name="cat[]" id="cat194" value="194"/>
                                                <label for="cat195" data-cat="4">Декоративные мелочи</label>
                                                <input type="checkbox" name="cat[]" id="cat195" value="195"/>
                                                <label for="cat196" data-cat="4">Для стирки, глажки и сушки</label>
                                                <input type="checkbox" name="cat[]" id="cat196" value="196"/>
                                                <label for="cat197" data-cat="4">Для уборки</label>
                                                <input type="checkbox" name="cat[]" id="cat197" value="197"/>
                                                <label for="cat198" data-cat="4">Коврики</label>
                                                <input type="checkbox" name="cat[]" id="cat198" value="198"/>
                                                <label for="cat199" data-cat="4">Полотенца</label>
                                                <input type="checkbox" name="cat[]" id="cat199" value="199"/>
                                                <label for="cat200" data-cat="4">Штора для ванной</label>
                                                <input type="checkbox" name="cat[]" id="cat200" value="200"/>
                                                <label for="cat201" data-cat="4">Кастрюли и казаны</label>
                                                <input type="checkbox" name="cat[]" id="cat201" value="201"/>
                                                <label for="cat202" data-cat="4">Кухонная утварь и аксессуары</label>
                                                <input type="checkbox" name="cat[]" id="cat202" value="202"/>
                                                <label for="cat203" data-cat="4">Наборы посуды</label>
                                                <input type="checkbox" name="cat[]" id="cat203" value="203"/>
                                                <label for="cat204" data-cat="4">Наборы посуды для сервировки</label>
                                                <input type="checkbox" name="cat[]" id="cat204" value="204"/>
                                                <label for="cat205" data-cat="4">Ножи и разделочные доски</label>
                                                <input type="checkbox" name="cat[]" id="cat205" value="205"/>
                                                <label for="cat206" data-cat="4">Овощерезки и терки</label>
                                                <input type="checkbox" name="cat[]" id="cat206" value="206"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat174">Гостиная</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat174" value="174"/>
                                                <label for="cat175" data-cat="4">Вазы, кашпо и горшки</label>
                                                <input type="checkbox" name="cat[]" id="cat175" value="175"/>
                                                <label for="cat176" data-cat="4">Декоративные подушки</label>
                                                <input type="checkbox" name="cat[]" id="cat176" value="176"/>
                                                <label for="cat177" data-cat="4">Декоретто и фотообои</label>
                                                <input type="checkbox" name="cat[]" id="cat177" value="177"/>
                                                <label for="cat178" data-cat="4">Зеркала</label>
                                                <input type="checkbox" name="cat[]" id="cat178" value="178"/>
                                                <label for="cat179" data-cat="4">Картины и постеры</label>
                                                <input type="checkbox" name="cat[]" id="cat179" value="179"/>
                                                <label for="cat180" data-cat="4">Покрывала и пледы</label>
                                                <input type="checkbox" name="cat[]" id="cat180" value="180"/>
                                                <label for="cat181" data-cat="4">Рулонные шторы и жалюзи</label>
                                                <input type="checkbox" name="cat[]" id="cat181" value="181"/>
                                                <label for="cat182" data-cat="4">Стильные мелочи</label>
                                                <input type="checkbox" name="cat[]" id="cat182" value="182"/>
                                                <label for="cat183" data-cat="4">Фоторамки</label>
                                                <input type="checkbox" name="cat[]" id="cat183" value="183"/>
                                                <label for="cat184" data-cat="4">Часы</label>
                                                <input type="checkbox" name="cat[]" id="cat184" value="184"/>
                                                <label for="cat185" data-cat="4">Шкатулки</label>
                                                <input type="checkbox" name="cat[]" id="cat185" value="185"/>
                                                <label for="cat186" data-cat="4">Шторы и гардины</label>
                                                <input type="checkbox" name="cat[]" id="cat186" value="186"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat222">Текстиль</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat222" value="222"/>
                                                <label for="cat223" data-cat="4">Декоративные подушки</label>
                                                <input type="checkbox" name="cat[]" id="cat223" value="223"/>
                                                <label for="cat224" data-cat="4">Детское постельное белье</label>
                                                <input type="checkbox" name="cat[]" id="cat224" value="224"/>
                                                <label for="cat225" data-cat="4">Наматрасники</label>
                                                <input type="checkbox" name="cat[]" id="cat225" value="225"/>
                                                <label for="cat226" data-cat="4">Одеяла</label>
                                                <input type="checkbox" name="cat[]" id="cat226" value="226"/>
                                                <label for="cat227" data-cat="4">Подушки</label>
                                                <input type="checkbox" name="cat[]" id="cat227" value="227"/>
                                                <label for="cat228" data-cat="4">Покрывала и пледы</label>
                                                <input type="checkbox" name="cat[]" id="cat228" value="228"/>
                                                <label for="cat229" data-cat="4">Постельное белье</label>
                                                <input type="checkbox" name="cat[]" id="cat229" value="229"/>
                                                <label for="cat230" data-cat="4">Рулонные шторы и жалюзи</label>
                                                <input type="checkbox" name="cat[]" id="cat230" value="230"/>
                                                <label for="cat231" data-cat="4">Шторы и гардины</label>
                                                <input type="checkbox" name="cat[]" id="cat231" value="231"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat207">Мебель, свет и дача</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat207" value="207"/>
                                                <label for="cat208" data-cat="4">Дача</label>
                                                <input type="checkbox" name="cat[]" id="cat208" value="208"/>
                                                <label for="cat209" data-cat="4">Мебель</label>
                                                <input type="checkbox" name="cat[]" id="cat209" value="209"/>
                                                <label for="cat210" data-cat="4">Свет</label>
                                                <input type="checkbox" name="cat[]" id="cat210" value="210"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat218">Гардеробная</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat218" value="218"/>
                                                <label for="cat219" data-cat="4">Системы для хранения белья</label>
                                                <input type="checkbox" name="cat[]" id="cat219" value="219"/>
                                                <label for="cat220" data-cat="4">Системы для хранения обуви</label>
                                                <input type="checkbox" name="cat[]" id="cat220" value="220"/>
                                                <label for="cat221" data-cat="4">Системы хранения для одежды</label>
                                                <input type="checkbox" name="cat[]" id="cat221" value="221"/>
                                            </div>
                                        </div>
                                        <div class="column_third subcategory">
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat165">Спальня</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat165" value="165"/>
                                                <label for="cat166" data-cat="4">Детское постельное белье</label>
                                                <input type="checkbox" name="cat[]" id="cat166" value="166"/>
                                                <label for="cat167" data-cat="4">Наматрасники</label>
                                                <input type="checkbox" name="cat[]" id="cat167" value="167"/>
                                                <label for="cat168" data-cat="4">Одеяла</label>
                                                <input type="checkbox" name="cat[]" id="cat168" value="168"/>
                                                <label for="cat169" data-cat="4">Подушки</label>
                                                <input type="checkbox" name="cat[]" id="cat169" value="169"/>
                                                <label for="cat170" data-cat="4">Покрывала и пледы</label>
                                                <input type="checkbox" name="cat[]" id="cat170" value="170"/>
                                                <label for="cat171" data-cat="4">Постельное белье</label>
                                                <input type="checkbox" name="cat[]" id="cat171" value="171"/>
                                                <label for="cat172" data-cat="4">Рулонные шторы и жалюзи</label>
                                                <input type="checkbox" name="cat[]" id="cat172" value="172"/>
                                                <label for="cat173" data-cat="4">Шторы и гардины</label>
                                                <input type="checkbox" name="cat[]" id="cat173" value="173"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat211">Ванная комната</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat211" value="211"/>
                                                <label for="cat212" data-cat="4">Декоративные мелочи</label>
                                                <input type="checkbox" name="cat[]" id="cat212" value="212"/>
                                                <label for="cat213" data-cat="4">Для стирки, глажки и сушки</label>
                                                <input type="checkbox" name="cat[]" id="cat213" value="213"/>
                                                <label for="cat214" data-cat="4">Для уборки</label>
                                                <input type="checkbox" name="cat[]" id="cat214" value="214"/>
                                                <label for="cat215" data-cat="4">Коврики</label>
                                                <input type="checkbox" name="cat[]" id="cat215" value="215"/>
                                                <label for="cat216" data-cat="4">Полотенца</label>
                                                <input type="checkbox" name="cat[]" id="cat216" value="216"/>
                                                <label for="cat217" data-cat="4">Штора для ванной</label>
                                                <input type="checkbox" name="cat[]" id="cat217" value="217"/>
                                            </div>

                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat535">Все для ремонта</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat535" value="535"/>
                                                <label for="cat399" data-cat="4">Обои</label>
                                                <input type="checkbox" name="cat[]" id="cat399" value="399"/>
                                                <label for="cat400" data-cat="4">Люстры</label>
                                                <input type="checkbox" name="cat[]" id="cat400" value="400"/>
                                                <label for="cat401" data-cat="4">Краска</label>
                                                <input type="checkbox" name="cat[]" id="cat401" value="401"/>
                                                <label for="cat402" data-cat="4">Расходные материалы</label>
                                                <input type="checkbox" name="cat[]" id="cat402" value="402"/>
                                                <label for="cat403" data-cat="4">Кафель</label>
                                                <input type="checkbox" name="cat[]" id="cat403" value="403"/>
                                                <label for="cat404" data-cat="4">Забор</label>
                                                <input type="checkbox" name="cat[]" id="cat404" value="404"/>
                                                <label for="cat405" data-cat="4">Счетчики</label>
                                                <input type="checkbox" name="cat[]" id="cat405" value="405"/>
                                                <label for="cat406" data-cat="4">Отделочные материалы</label>
                                                <input type="checkbox" name="cat[]" id="cat406" value="406"/>
                                                <label for="cat407" data-cat="4">Деревянные срубы</label>
                                                <input type="checkbox" name="cat[]" id="cat407" value="407"/>
                                                <label for="cat408" data-cat="4">Решетки на окна</label>
                                                <input type="checkbox" name="cat[]" id="cat408" value="408"/>
                                                <label for="cat409" data-cat="4">Двери</label>
                                                <input type="checkbox" name="cat[]" id="cat409" value="409"/>
                                                <label for="cat410" data-cat="4">Пластиковые окна</label>
                                                <input type="checkbox" name="cat[]" id="cat410" value="410"/>
                                                <label for="cat411" data-cat="4">Инструменты</label>
                                                <input type="checkbox" name="cat[]" id="cat411" value="411"/>
                                                <label for="cat412" data-cat="4">Станки</label>
                                                <input type="checkbox" name="cat[]" id="cat412" value="412"/>
                                                <label for="cat413" data-cat="4">Прочее</label>
                                                <input type="checkbox" name="cat[]" id="cat413" value="413"/>

                                            </div>

                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat232">Прочее</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat232" value="232"/>
                                                <label for="cat385" data-cat="4">Корм для животных</label>
                                                <input type="checkbox" name="cat[]" id="cat385" value="385"/>
                                                <label for="cat386" data-cat="4">Корм для собак</label>
                                                <input type="checkbox" name="cat[]" id="cat386" value="386"/>
                                                <label for="cat387" data-cat="4">Корм для кошек</label>
                                                <input type="checkbox" name="cat[]" id="cat387" value="387"/>
                                                <label for="cat388" data-cat="4">Корм для аквариумных рыбок</label>
                                                <input type="checkbox" name="cat[]" id="cat388" value="388"/>
                                                <label for="cat389" data-cat="4">Живой корм</label>
                                                <input type="checkbox" name="cat[]" id="cat389" value="389"/>
                                                <label for="cat390" data-cat="4">Корм для морских свинок</label>
                                                <input type="checkbox" name="cat[]" id="cat390" value="390"/>
                                                <label for="cat391" data-cat="4">Корм для грызунов</label>
                                                <input type="checkbox" name="cat[]" id="cat391" value="391"/>
                                                <label for="cat392" data-cat="4">Корм для птиц</label>
                                                <input type="checkbox" name="cat[]" id="cat392" value="392"/>
                                                <label for="cat393" data-cat="4">Принадлежности для животных</label>
                                                <input type="checkbox" name="cat[]" id="cat393" value="393"/>
                                                <label for="cat394" data-cat="4">Игрушки для животных</label>
                                                <input type="checkbox" name="cat[]" id="cat394" value="394"/>
                                                <label for="cat395" data-cat="4">Аквариумы</label>
                                                <input type="checkbox" name="cat[]" id="cat395" value="395"/>
                                                <label for="cat396" data-cat="4">Растения для аквариума</label>
                                                <input type="checkbox" name="cat[]" id="cat396" value="396"/>
                                                <label for="cat397" data-cat="4">Принадлежности для аквариумов</label>
                                                <input type="checkbox" name="cat[]" id="cat397" value="397"/>
                                                <label for="cat398" data-cat="4">Лекарства для животных</label>
                                                <input type="checkbox" name="cat[]" id="cat398" value="398"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_5" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat233">Электроника</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat233" value="233"/>
                                                <label for="cat234" data-cat="5">Аксессуары</label>
                                                <input type="checkbox" name="cat[]" id="cat234" value="234"/>
                                                <label for="cat235" data-cat="5">Аудио, Видео, Фото</label>
                                                <input type="checkbox" name="cat[]" id="cat235" value="235"/>
                                                <label for="cat236" data-cat="5">Для авто</label>
                                                <input type="checkbox" name="cat[]" id="cat236" value="236"/>
                                                <label for="cat237" data-cat="5">Планшеты, Ноутбуки</label>
                                                <input type="checkbox" name="cat[]" id="cat237" value="237"/>
                                                <label for="cat238" data-cat="5">Телефоны</label>
                                                <input type="checkbox" name="cat[]" id="cat238" value="238"/>
                                            </div>
                                        </div>
                                        <div class="column_third subcategory">
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat239">Техника</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat239" value="239"/>
                                                <label for="cat240" data-cat="5">Для дома</label>
                                                <input type="checkbox" name="cat[]" id="cat240" value="240"/>
                                                <label for="cat241" data-cat="5">Для красоты и здоровья</label>
                                                <input type="checkbox" name="cat[]" id="cat241" value="241"/>
                                                <label for="cat242" data-cat="5">Для кухни</label>
                                                <input type="checkbox" name="cat[]" id="cat242" value="242"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat243">Прочее</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat243" value="243"/>

                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_6" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat252">Уход за телом</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat252" value="252"/>
                                                <label for="cat253" data-cat="6">Антицеллюлитные средства</label>
                                                <input type="checkbox" name="cat[]" id="cat253" value="253"/>
                                                <label for="cat254" data-cat="6">Дезодоранты</label>
                                                <input type="checkbox" name="cat[]" id="cat254" value="254"/>
                                                <label for="cat255" data-cat="6">Депиляция</label>
                                                <input type="checkbox" name="cat[]" id="cat255" value="255"/>
                                                <label for="cat256" data-cat="6">Питательные средства</label>
                                                <input type="checkbox" name="cat[]" id="cat256" value="256"/>
                                                <label for="cat257" data-cat="6">Средства для загара</label>
                                                <input type="checkbox" name="cat[]" id="cat257" value="257"/>
                                                <label for="cat258" data-cat="6">Средства для очищения</label>
                                                <input type="checkbox" name="cat[]" id="cat258" value="258"/>
                                                <label for="cat259" data-cat="6">Средства для увлажнения</label>
                                                <input type="checkbox" name="cat[]" id="cat259" value="259"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat268">Уход за лицом</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat268" value="268"/>
                                                <label for="cat269" data-cat="6">Питательные средства</label>
                                                <input type="checkbox" name="cat[]" id="cat269" value="269"/>
                                                <label for="cat270" data-cat="6">Средства для контура глаз</label>
                                                <input type="checkbox" name="cat[]" id="cat270" value="270"/>
                                                <label for="cat271" data-cat="6">Средства для очищения</label>
                                                <input type="checkbox" name="cat[]" id="cat271" value="271"/>
                                                <label for="cat272" data-cat="6">Средства для увлажнения</label>
                                                <input type="checkbox" name="cat[]" id="cat272" value="272"/>
                                                <label for="cat273" data-cat="6">Средства против старения</label>
                                                <input type="checkbox" name="cat[]" id="cat273" value="273"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat260">Уход за волосами</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat260" value="260"/>
                                                <label for="cat261" data-cat="6">Кондиционеры для волос</label>
                                                <input type="checkbox" name="cat[]" id="cat261" value="261"/>
                                                <label for="cat262" data-cat="6">Маски для волос</label>
                                                <input type="checkbox" name="cat[]" id="cat262" value="262"/>
                                                <label for="cat263" data-cat="6">Фиксация и защита волос</label>
                                                <input type="checkbox" name="cat[]" id="cat263" value="263"/>
                                                <label for="cat264" data-cat="6">Шампуни для волос</label>
                                                <input type="checkbox" name="cat[]" id="cat264" value="264"/>
                                            </div>
                                        </div>
                                        <div class="column_third subcategory">
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat244">Парфюмерия</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat244" value="244"/>
                                                <label for="cat245" data-cat="6">Женская парфюмерия</label>
                                                <input type="checkbox" name="cat[]" id="cat245" value="245"/>
                                                <label for="cat246" data-cat="6">Мужская парфюмерия</label>
                                                <input type="checkbox" name="cat[]" id="cat246" value="246"/>
                                                <label for="cat247" data-cat="6">Унисекс парфюмерия</label>
                                                <input type="checkbox" name="cat[]" id="cat247" value="247"/>
                                                <label for="cat414" data-cat="6">Косметика</label>
                                                <input type="checkbox" name="cat[]" id="cat414" value="414"/>
                                                <label for="cat415" data-cat="6">Краски</label>
                                                <input type="checkbox" name="cat[]" id="cat415" value="415"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat248">Макияж</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat248" value="248"/>
                                                <label for="cat249" data-cat="6">Для глаз</label>
                                                <input type="checkbox" name="cat[]" id="cat249" value="249"/>
                                                <label for="cat250" data-cat="6">Для лица</label>
                                                <input type="checkbox" name="cat[]" id="cat250" value="250"/>
                                                <label for="cat251" data-cat="6">Для ногтей</label>
                                                <input type="checkbox" name="cat[]" id="cat251" value="251"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat274">Гигиена</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat274" value="274"/>
                                                <label for="cat275" data-cat="6">Средства гигиены</label>
                                                <input type="checkbox" name="cat[]" id="cat275" value="275"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat276">18+</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat276" value="276"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat277">Прочее</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat277" value="277"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat265">Техника для красоты</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat265" value="265"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat266">Красота и здоровье</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat266" value="266"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat267">Спорт</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat267" value="267"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_7" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat278">Красота</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat278" value="278"/>
                                                <label for="cat279" data-cat="7">Уход за волосами</label>
                                                <input type="checkbox" name="cat[]" id="cat279" value="279"/>
                                                <label for="cat280" data-cat="7">Эпиляция</label>
                                                <input type="checkbox" name="cat[]" id="cat280" value="280"/>
                                                <label for="cat281" data-cat="7">Уход за лицом</label>
                                                <input type="checkbox" name="cat[]" id="cat281" value="281"/>
                                                <label for="cat282" data-cat="7">SPA, массаж, уход за телом</label>
                                                <input type="checkbox" name="cat[]" id="cat282" value="282"/>
                                                <label for="cat283" data-cat="7">Маникюр, педикюр</label>
                                                <input type="checkbox" name="cat[]" id="cat283" value="283"/>
                                                <label for="cat284" data-cat="7">Другое</label>
                                                <input type="checkbox" name="cat[]" id="cat284" value="284"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat285">Здоровье</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat285" value="285"/>
                                                <label for="cat286" data-cat="7">Стоматология</label>
                                                <input type="checkbox" name="cat[]" id="cat286" value="286"/>
                                                <label for="cat287" data-cat="7">Диагностика, обследование</label>
                                                <input type="checkbox" name="cat[]" id="cat287" value="287"/>
                                                <label for="cat416" data-cat="7">Лечение зубов под наркозом</label>
                                                <input type="checkbox" name="cat[]" id="cat416" value="416"/>
                                                <label for="cat417" data-cat="7">Лечение зубов под закисью азота</label>
                                                <input type="checkbox" name="cat[]" id="cat417" value="417"/>
                                                <label for="cat418" data-cat="7">Исправление прикуса</label>
                                                <input type="checkbox" name="cat[]" id="cat418" value="418"/>
                                                <label for="cat419" data-cat="7">Ортодонтия</label>
                                                <input type="checkbox" name="cat[]" id="cat419" value="419"/>
                                                <label for="cat420" data-cat="7">Компьютерное моделирование протезов</label>
                                                <input type="checkbox" name="cat[]" id="cat420" value="420"/>
                                                <label for="cat421" data-cat="7">Имплантация, протезирование</label>
                                                <input type="checkbox" name="cat[]" id="cat421" value="421"/>
                                                <label for="cat422" data-cat="7">3D-компьютерная томография зубов</label>
                                                <input type="checkbox" name="cat[]" id="cat422" value="422"/>
                                                <label for="cat423" data-cat="7">Ортопантомография</label>
                                                <input type="checkbox" name="cat[]" id="cat423" value="423"/>
                                                <label for="cat424" data-cat="7">Лечение заболеваний десен</label>
                                                <input type="checkbox" name="cat[]" id="cat424" value="424"/>
                                                <label for="cat425" data-cat="7">Лечение под микроскопом</label>
                                                <input type="checkbox" name="cat[]" id="cat425" value="425"/>
                                                <label for="cat426" data-cat="7">Отбеливание зубов</label>
                                                <input type="checkbox" name="cat[]" id="cat426" value="426"/>
                                                <label for="cat427" data-cat="7">Детская стоматология</label>
                                                <input type="checkbox" name="cat[]" id="cat427" value="427"/>
                                                <label for="cat428" data-cat="7">Хирургия</label>
                                                <input type="checkbox" name="cat[]" id="cat428" value="428"/>
                                                <label for="cat429" data-cat="7">Гинекология</label>
                                                <input type="checkbox" name="cat[]" id="cat429" value="429"/>
                                                <label for="cat430" data-cat="7">Венерические заболевания</label>
                                                <input type="checkbox" name="cat[]" id="cat430" value="430"/>
                                                <label for="cat431" data-cat="7">Услуги ветеринара</label>
                                                <input type="checkbox" name="cat[]" id="cat431" value="431"/>
                                                <label for="cat432" data-cat="7">Животный доктор</label>
                                                <input type="checkbox" name="cat[]" id="cat432" value="432"/>
                                                <label for="cat288" data-cat="7">Другое</label>
                                                <input type="checkbox" name="cat[]" id="cat288" value="288"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat514">Для авто</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat514" value="514"/>
                                                <label for="cat433" data-cat="7">Чип-тюнинг</label>
                                                <input type="checkbox" name="cat[]" id="cat433" value="433"/>
                                                <label for="cat434" data-cat="7">Сход-развал</label>
                                                <input type="checkbox" name="cat[]" id="cat434" value="434"/>
                                                <label for="cat435" data-cat="7">Балансировка колес</label>
                                                <input type="checkbox" name="cat[]" id="cat435" value="435"/>
                                                <label for="cat436" data-cat="7">Шиномонтаж</label>
                                                <input type="checkbox" name="cat[]" id="cat436" value="436"/>
                                                <label for="cat437" data-cat="7">Лакокрасочные услуги</label>
                                                <input type="checkbox" name="cat[]" id="cat437" value="437"/>
                                                <label for="cat438" data-cat="7">Ремонт кузова</label>
                                                <input type="checkbox" name="cat[]" id="cat438" value="438"/>
                                                <label for="cat439" data-cat="7">Перетяжка салона</label>
                                                <input type="checkbox" name="cat[]" id="cat439" value="439"/>
                                                <label for="cat440" data-cat="7">Диагностика</label>
                                                <input type="checkbox" name="cat[]" id="cat440" value="440"/>
                                                <label for="cat441" data-cat="7">Мойка</label>
                                                <input type="checkbox" name="cat[]" id="cat441" value="468"/>
                                                <label for="cat442" data-cat="7">Экспресс-мойка</label>
                                                <input type="checkbox" name="cat[]" id="cat442" value="468"/>
                                                <label for="cat443" data-cat="7">Полировка кузова</label>
                                                <input type="checkbox" name="cat[]" id="cat443" value="468"/>
                                                <label for="cat444" data-cat="7">Жидкая полировка</label>
                                                <input type="checkbox" name="cat[]" id="cat444" value="468"/>
                                                <label for="cat445" data-cat="7">Горячий или холодный воск</label>
                                                <input type="checkbox" name="cat[]" id="cat445" value="468"/>
                                                <label for="cat446" data-cat="7">Мойка и полировка салона</label>
                                                <input type="checkbox" name="cat[]" id="cat446" value="468"/>
                                                <label for="cat447" data-cat="7">Чернение резины</label>
                                                <input type="checkbox" name="cat[]" id="cat447" value="468"/>
                                                <label for="cat448" data-cat="7">Кондиционер кожи и др.</label>
                                                <input type="checkbox" name="cat[]" id="cat448" value="468"/>
                                                <label for="cat449" data-cat="7">Прочее</label>
                                                <input type="checkbox" name="cat[]" id="cat449" value="468"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat515">Сайты, продвижение, реклама</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat515" value="515"/>
                                                <label for="cat491" data-cat="7">Дизайн сайтов</label>
                                                <input type="checkbox" name="cat[]" id="cat491" value="491"/>
                                                <label for="cat492" data-cat="7">Дизайн логотипов</label>
                                                <input type="checkbox" name="cat[]" id="cat492" value="492"/>
                                                <label for="cat493" data-cat="7">Верстка</label>
                                                <input type="checkbox" name="cat[]" id="cat493" value="493"/>
                                                <label for="cat494" data-cat="7">Разработка сайтов</label>
                                                <input type="checkbox" name="cat[]" id="cat494" value="494"/>
                                                <label for="cat495" data-cat="7">Разработка одностраничников</label>
                                                <input type="checkbox" name="cat[]" id="cat495" value="495"/>
                                                <label for="cat496" data-cat="7">SMM</label>
                                                <input type="checkbox" name="cat[]" id="cat496" value="496"/>
                                                <label for="cat497" data-cat="7">SEO</label>
                                                <input type="checkbox" name="cat[]" id="cat497" value="497"/>
                                                <label for="cat498" data-cat="7">SMO</label>
                                                <input type="checkbox" name="cat[]" id="cat498" value="498"/>
                                                <label for="cat499" data-cat="7">Оформление сообществ в соц., сетях</label>
                                                <input type="checkbox" name="cat[]" id="cat499" value="499"/>
                                                <label for="cat500" data-cat="7">Баннеры</label>
                                                <input type="checkbox" name="cat[]" id="cat500" value="500"/>
                                                <label for="cat501" data-cat="7">Брендирование</label>
                                                <input type="checkbox" name="cat[]" id="cat501" value="501"/>
                                                <label for="cat502" data-cat="7">Сервера</label>
                                                <input type="checkbox" name="cat[]" id="cat502" value="502"/>
                                                <label for="cat503" data-cat="7">Облачные хранилища</label>
                                                <input type="checkbox" name="cat[]" id="cat503" value="503"/>
                                                <label for="cat504" data-cat="7">Устранение уязвимостей</label>
                                                <input type="checkbox" name="cat[]" id="cat504" value="504"/>
                                                <label for="cat505" data-cat="7">Подключение модулей</label>
                                                <input type="checkbox" name="cat[]" id="cat505" value="505"/>
                                                <label for="cat506" data-cat="7">Написание программ</label>
                                                <input type="checkbox" name="cat[]" id="cat506" value="506"/>
                                                <label for="cat507" data-cat="7">Настройка Яндекс.Директ</label>
                                                <input type="checkbox" name="cat[]" id="cat507" value="507"/>
                                                <label for="cat508" data-cat="7">Настройка Google AdWords</label>
                                                <input type="checkbox" name="cat[]" id="cat508" value="508"/>
                                                <label for="cat509" data-cat="7">Настройка Google Analytics</label>
                                                <input type="checkbox" name="cat[]" id="cat509" value="509"/>
                                                <label for="cat510" data-cat="7">Обучение Photoshop</label>
                                                <input type="checkbox" name="cat[]" id="cat510" value="510"/>
                                                <label for="cat511" data-cat="7">Обучение CorelDRAW</label>
                                                <input type="checkbox" name="cat[]" id="cat511" value="511"/>
                                                <label for="cat512" data-cat="7">Обучение в других программах</label>
                                                <input type="checkbox" name="cat[]" id="cat512" value="512"/>

                                            </div>

                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat513">Свадьба</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat513" value="513"/>
                                                <label for="cat451" data-cat="7">Свадебный фотограф</label>
                                                <input type="checkbox" name="cat[]" id="cat451" value="451"/>
                                                <label for="cat452" data-cat="7">Свадебный видеооператор</label>
                                                <input type="checkbox" name="cat[]" id="cat452" value="452"/>
                                                <label for="cat453" data-cat="7">Тамада</label>
                                                <input type="checkbox" name="cat[]" id="cat453" value="453"/>
                                                <label for="cat454" data-cat="7">Оформление зала</label>
                                                <input type="checkbox" name="cat[]" id="cat454" value="454"/>
                                                <label for="cat455" data-cat="7">Платья</label>
                                                <input type="checkbox" name="cat[]" id="cat455" value="455"/>
                                                <label for="cat456" data-cat="7">Костюмы</label>
                                                <input type="checkbox" name="cat[]" id="cat456" value="456"/>
                                                <label for="cat457" data-cat="7">Сопутствующие товары</label>
                                                <input type="checkbox" name="cat[]" id="cat457" value="457"/>
                                                <label for="cat458" data-cat="7">Свадебные аксессуары</label>
                                                <input type="checkbox" name="cat[]" id="cat458" value="458"/>
                                                <label for="cat459" data-cat="7">Свадебные товары для невесты</label>
                                                <input type="checkbox" name="cat[]" id="cat459" value="459"/>
                                                <label for="cat460" data-cat="7">Свадебные товары для жениха</label>
                                                <input type="checkbox" name="cat[]" id="cat460" value="460"/>
                                                <label for="cat461" data-cat="7">Заказ автобомиля</label>
                                                <input type="checkbox" name="cat[]" id="cat461" value="461"/>
                                                <label for="cat462" data-cat="7">Организация торжеств</label>
                                                <input type="checkbox" name="cat[]" id="cat462" value="462"/>
                                                <label for="cat463" data-cat="7">Развлечения для гостей</label>
                                                <input type="checkbox" name="cat[]" id="cat463" value="463"/>
                                                <label for="cat464" data-cat="7">Свадебный кортеж</label>
                                                <input type="checkbox" name="cat[]" id="cat464" value="464"/>
                                                <label for="cat465" data-cat="7">Цветы на свадьбу</label>
                                                <input type="checkbox" name="cat[]" id="cat465" value="465"/>
                                                <label for="cat466" data-cat="7">Свадебный танец</label>
                                                <input type="checkbox" name="cat[]" id="cat466" value="466"/>
                                                <label for="cat467" data-cat="7">Проведение фейерверков</label>
                                                <input type="checkbox" name="cat[]" id="cat467" value="467"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat293">Ремонт</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat293" value="293"/>
                                                <label for="cat468" data-cat="7">Ремонт компьютеров</label>
                                                <input type="checkbox" name="cat[]" id="cat468" value="468"/>
                                                <label for="cat469" data-cat="7">Ремонт ноутбуков</label>
                                                <input type="checkbox" name="cat[]" id="cat469" value="469"/>
                                                <label for="cat470" data-cat="7">Ремонт телефонов</label>
                                                <input type="checkbox" name="cat[]" id="cat470" value="470"/>
                                                <label for="cat471" data-cat="7">Восстановление информации</label>
                                                <input type="checkbox" name="cat[]" id="cat471" value="471"/>
                                                <label for="cat472" data-cat="7">Переустановка Windows</label>
                                                <input type="checkbox" name="cat[]" id="cat472" value="472"/>
                                                <label for="cat473" data-cat="7">Установка программ</label>
                                                <input type="checkbox" name="cat[]" id="cat473" value="473"/>
                                                <label for="cat474" data-cat="7">Удаление вирусов</label>
                                                <input type="checkbox" name="cat[]" id="cat474" value="474"/>
                                            </div>
                                        </div>

                                        <div class="column_third subcategory">

                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat516">Юридические услуги</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat516" value="516"/>
                                                <label for="cat536" data-cat="7">Земельное право</label>
                                                <input type="checkbox" name="cat[]" id="cat536" value="536"/>
                                                <label for="cat537" data-cat="7">Семейное право</label>
                                                <input type="checkbox" name="cat[]" id="cat537" value="537"/>
                                                <label for="cat538" data-cat="7">Трудовое право</label>
                                                <input type="checkbox" name="cat[]" id="cat538" value="538"/>
                                                <label for="cat539" data-cat="7">Уголовное право</label>
                                                <input type="checkbox" name="cat[]" id="cat539" value="539"/>
                                                <label for="cat540" data-cat="7">Налоговое право</label>
                                                <input type="checkbox" name="cat[]" id="cat540" value="540"/>
                                                <label for="cat541" data-cat="7">Жилищное право</label>
                                                <input type="checkbox" name="cat[]" id="cat541" value="541"/>
                                                <label for="cat542" data-cat="7">Гражданское право</label>
                                                <input type="checkbox" name="cat[]" id="cat542" value="542"/>
                                                <label for="cat543" data-cat="7">Корпоративное право</label>
                                                <input type="checkbox" name="cat[]" id="cat543" value="543"/>
                                                <label for="cat544" data-cat="7">Хозяйственное право</label>
                                                <input type="checkbox" name="cat[]" id="cat544" value="544"/>
                                                <label for="cat545" data-cat="7">Наследственное право</label>
                                                <input type="checkbox" name="cat[]" id="cat545" value="545"/>
                                                <label for="cat546" data-cat="7">Административное право</label>
                                                <input type="checkbox" name="cat[]" id="cat546" value="546"/>
                                                <label for="cat547" data-cat="7">Защита прав потребителей</label>
                                                <input type="checkbox" name="cat[]" id="cat547" value="547"/>
                                                <label for="cat548" data-cat="7">Оформление гражданства РФ</label>
                                                <input type="checkbox" name="cat[]" id="cat548" value="548"/>
                                                <label for="cat549" data-cat="7">Гражданский, уголовный <br>и арбитражный процесс</label>
                                                <input type="checkbox" name="cat[]" id="cat549" value="549"/>
                                                <label for="cat550" data-cat="7">Регистрация индивидуальных предпринимателей<br> и юридических лиц</label>
                                                <input type="checkbox" name="cat[]" id="cat550" value="550"/>
                                                <label for="cat551" data-cat="7">Составление полного пакета документов</label>
                                                <input type="checkbox" name="cat[]" id="cat551" value="551"/>
                                                <label for="cat552" data-cat="7">Предоставление документов в <br>регистрирующие органы</label>
                                                <input type="checkbox" name="cat[]" id="cat552" value="552"/>
                                                <label for="cat553" data-cat="7">Получение Свидетельства<br> о внесении записи в ЕГРЮЛ</label>
                                                <input type="checkbox" name="cat[]" id="cat553" value="553"/>
                                                <label for="cat554" data-cat="7">Получение кодов статистики в Облкомстате</label>
                                                <input type="checkbox" name="cat[]" id="cat554" value="554"/>
                                                <label for="cat555" data-cat="7">Регистрация изменений в учредительные <br>документы юридических лиц</label>
                                                <input type="checkbox" name="cat[]" id="cat555" value="555"/>
                                                <label for="cat556" data-cat="7">Реорганизация юридических лиц</label>
                                                <input type="checkbox" name="cat[]" id="cat556" value="556"/>
                                                <label for="cat557" data-cat="7">Ликвидация юридических лиц</label>
                                                <input type="checkbox" name="cat[]" id="cat557" value="557"/>
                                                <label for="cat558" data-cat="7">Прекращение деятельности (ликвидация)<br> индивидуальных предпринимателей</label>
                                                <input type="checkbox" name="cat[]" id="cat558" value="558"/>
                                                <label for="cat559" data-cat="7">Открытие расчетных счетов</label>
                                                <input type="checkbox" name="cat[]" id="cat559" value="559"/>
                                                <label for="cat560" data-cat="7">Регистрационные услуги</label>
                                                <input type="checkbox" name="cat[]" id="cat560" value="560"/>
                                                <label for="cat561" data-cat="7">Юридические консультации</label>
                                                <input type="checkbox" name="cat[]" id="cat561" value="561"/>
                                                <label for="cat562" data-cat="7">Составлению договоров, исковых <br>заявлений, иных документов</label>
                                                <input type="checkbox" name="cat[]" id="cat562" value="562"/>

                                            </div>

                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat517">По дому</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat517" value="517"/>
                                                <label for="cat475" data-cat="7">Уборка квартир, домов</label>
                                                <input type="checkbox" name="cat[]" id="cat475" value="475"/>
                                                <label for="cat476" data-cat="7">Ремонт квартир</label>
                                                <input type="checkbox" name="cat[]" id="cat476" value="476"/>
                                                <label for="cat477" data-cat="7">Монтажные работы</label>
                                                <input type="checkbox" name="cat[]" id="cat477" value="477"/>
                                                <label for="cat478" data-cat="7">Полный цикл работ</label>
                                                <input type="checkbox" name="cat[]" id="cat478" value="478"/>
                                                <label for="cat479" data-cat="7">Ремонт техники</label>
                                                <input type="checkbox" name="cat[]" id="cat479" value="479"/>
                                                <label for="cat480" data-cat="7">Проведение труб</label>
                                                <input type="checkbox" name="cat[]" id="cat480" value="480"/>
                                                <label for="cat481" data-cat="7">Сантехнические работы</label>
                                                <input type="checkbox" name="cat[]" id="cat481" value="481"/>
                                                <label for="cat482" data-cat="7">Облицовка кафелем и керамогранитом<br> стен и пола</label>
                                                <input type="checkbox" name="cat[]" id="cat482" value="482"/>
                                                <label for="cat483" data-cat="7">Малярно-штукатурные работы</label>
                                                <input type="checkbox" name="cat[]" id="cat483" value="483"/>
                                                <label for="cat484" data-cat="7">Электромонтажные работы</label>
                                                <input type="checkbox" name="cat[]" id="cat484" value="484"/>
                                                <label for="cat485" data-cat="7">Возведение из гипсокартона одноуровневых <br>и многоуровневых потолков</label>
                                                <input type="checkbox" name="cat[]" id="cat485" value="485"/>
                                                <label for="cat486" data-cat="7">Выравнивание стен, пола, потолка под маяк</label>
                                                <input type="checkbox" name="cat[]" id="cat486" value="486"/>
                                                <label for="cat487" data-cat="7">Штукатурные работы <br>(шпатлевка стен, потолков)</label>
                                                <input type="checkbox" name="cat[]" id="cat487" value="487"/>
                                                <label for="cat488" data-cat="7">Монтаж перегородок</label>
                                                <input type="checkbox" name="cat[]" id="cat488" value="488"/>
                                                <label for="cat489" data-cat="7">Стяжка пола, наливной пол</label>
                                                <input type="checkbox" name="cat[]" id="cat489" value="489"/>
                                                <label for="cat490" data-cat="7">Настил ламината, линолеума, <br>коврового покрытия</label>
                                                <input type="checkbox" name="cat[]" id="cat490" value="490"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat294">Разное</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat294" value="294"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat292">Обучение</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat292" value="292"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat290">Концерты</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat290" value="290"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat289">Развлечения</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat289" value="289"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat291">Фитнес</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat291" value="291"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat518">Тату</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat518" value="518"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat519">Пирсинг</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat519" value="519"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat520">Фотоуслуги</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat520" value="520"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat521">Видеоуслуги</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat521" value="521"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat522">Свидания на крышах</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat522" value="522"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat523">Организация встреч</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat523" value="523"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat524">Гостиницы</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat524" value="524"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat525">Сауны</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat525" value="525"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat526">Аренда квартир</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat526" value="526"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat527">Хостелы</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat527" value="527"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat528">Встреча в аэропорту</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat528" value="528"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat529">Охрана</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat529" value="529"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat530">Сопровождение</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat530" value="530"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat531">Вооруженное сопровождение</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat531" value="531"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat532">Обеспечение безопасности</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat532" value="532"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_8" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat305">Другие города</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat305" value="305"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat304">Юг России</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat304" value="304"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat306">Украина</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat306" value="306"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat307">Казахстан</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat307" value="307"/>
                                            </div>
                                        </div>
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat309">Другие страны</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat309" value="309"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat308">Беларусь</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat308" value="308"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat303">Урал</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat303" value="303"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat301">Поволжье</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat301" value="301"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat296">Санкт-Петербург и область</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat296" value="296"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat295">Москва и область</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat295" value="295"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat297">Золотое кольцо</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat297" value="297"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat298">Алтай</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat298" value="298"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat300">Балтика</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat300" value="300"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat299">Байкал</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat299" value="299"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat302">Сибирь</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat302" value="302"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_9" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat317">Англия</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat317" value="317"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat315">Чехия</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat315" value="315"/>
                                            </div>

                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat319">Швейцария</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat319" value="319"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat321">Франция</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat321" value="321"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat320">США</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat320" value="320"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat316">Испания</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat316" value="316"/>
                                            </div>

                                        </div>
                                        <div class="column_third subcategory">

                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat310">Россия</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat310" value="310"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat311">Египет</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat311" value="311"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat312">Турция</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat312" value="312"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat313">Италия</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat313" value="313"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat314">Греция</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat314" value="314"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat533">Германия</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat533" value="533"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat318">Другие страны</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat318" value="318"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_10" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat324">Подарки</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat324" value="324"/>
                                            </div>
                                        </div>
                                        <div class="column_third subcategory">
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat322">Скидки</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat322" value="322"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat323">Распродажи</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat323" value="323"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_11" class="tab_hide">
                                        <div class="column_third subcategory">
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat331">Меню и напитки</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat331" value="331"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat332">Воки</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat332" value="332"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat333">Фуршеты</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat333" value="333"/>
                                            </div>
                                        </div>
                                        <div class="column_third subcategory">
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat334">Другая кухня</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat334" value="334"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat330">Скидки на барную карту</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat330" value="330"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat329">Суши</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat329" value="329"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat325">Скидки</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat325" value="325"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat326">Подарки</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat326" value="326"/>
                                            </div>
                                            <div class="accessories_checkboxes styled_checkbox"><label for="cat327">Скидки на все меню</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat327" value="327"/>
                                            </div>
                                            <div class="clothing_checkboxes styled_checkbox"><label for="cat328">Скидки на все меню и напитки</label>
                                                <input type="checkbox" class="checkbox_category" name="cat[]" id="cat328" value="328"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                    -->
                <div class="action_result">
                    <div class="column_third">
                        <div class="amount_label">Ваш баланс</div>
                        <div class="amount_value"><span id="actMyBalance"><?php echo $USER['money']; ?></span> руб.</div>
                    </div>
                    <div class="column_third">
                        <div class="amount_label">Стоимость конкурса составит</div>
                        <div class="amount_value"><span id="actCost">9999</span> руб.</div>
                    </div>
                    <div class="column_third column_preview">
                        <button id="previewAction" class="btn btn-icon btn-browser rounded_8"><span>Предпросмотр конкурса</span></button>
                    </div>
                    <div class="clear"></div>

                    <div class="styled_checkbox checkbox_agreement">
                        <label for="agreement">С <a href="http://gm1lp.ru/help/agree" target="_blank"">условиями пользования сервисом</a> и стоимостью ознакомлен и полностью согласен</label> <input type="checkbox" name="agreement" id="agreement">
                    </div>

                    <button id="runAction" class="btn inp_full btn-big rounded_8">Запустить конкурс</button>
                </div>
                <input type="hidden" id="preview" name="preview" value="0"/>
            </form>
        </div>

        <div class="right_block">
            <ul class="right_menu">
                <li><a class="rounded_3" href="/user/lk">Личный кабинет</a></li>
                <li class="active"><a class="rounded_3" href="/action/create">Запустить конкурс</a></li>
                <li><a class="rounded_3" href="/action/my">Мои конкурсы</a></li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
    <script>
        var curImage = null;
        var clickForPopup = false;
        //var
        $(document).ready(function () {
            $('.styled_checkbox.more input[type="checkbox"]').change(function () {
                if ($('#act1_y').prop('checked') && $('#act2_y').prop('checked')) {
                    alert('Запрещено выбирать вступление в группу и подписку одновременно');
                    setTimeout( function() {
                        $('#act2_y').prop('checked', false).change();
                    }, 0);
                    return false;
                }
                if ($('.styled_checkbox.more input[type="checkbox"]:checked').size() > 2) {
                    alert('Запрещено выбирать больше 2х условий');
                    setTimeout( function() {
                        $('#act3_y').prop('checked', false).change();
                    }, 0);
                    return false;
                }
            });
            $('input[name="title"]').keyup(function () {
                var v = $(this).val();
                $('#countS').text(64 - v.length);
            });
            $('form#formAction').submit(function () {
                $('#inpForImg').html('');
                var j = 0;
                $('.attachments a').each(function (i) {
                    if ($(this).attr('data-img')) {
                        $('#inpForImg').append('<input type="hidden" name="images[' + (j++) + ']" value="' + $(this).attr('data-img') + '" />');
                    }
                });
                $.post('', $(this).serialize(), function (data) {
                    console.log(data);
                    $('#preview').val('0');
                    var errors = parseErrors(data);
                    var success = parseSuccess(data);
                    var eval = parseEval(data);
                }, 'json');
                return false;
            });
            $('#runAction').click(function () {
                $('#preview').val('0');
                setTimeout(function () {
                    $('form#formAction').submit();
                }, 300);
                return false;
            });
            $('#previewAction').click(function () {
                $('#preview').val('1');
                setTimeout(function () {
                    $('form#formAction').submit();
                }, 300);
                return false;
            });
            var dop = null;
            $('.dop').click(function () {
                dop = $(this);
                $('#fileupload2').click();
                return false;
            });
            $('#fileupload2').fileupload({
                url: '?upload&dop',
                dataType: 'json',
                done: function (e, data) {
                    dop.attr('data-img', '/' + data.result.file);
                    dop.find('img').attr('src', '/' + data.result.file);
                    console.log(data);
                },
                error: function (e,data) {
                    alert(e.responseJSON ? e.responseJSON.error : e.responseText);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
            ////
            $('#fileupload').fileupload({
                url: '?upload',
                dataType: 'json',
                done: function (e, data) {
                    curImage.attr('img-src', '/' + data.result.file);
                    curImage.click();
                    console.log(data);
                },
                error: function (e,data) {
                    alert(e.responseJSON ? e.responseJSON.error : e.responseText);
                },
                progressall: function (e, data) {

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
            $('#chooseImage1,#chooseImage2,#chooseImage3').click(function () {
                curImage = $('#' + $(this).attr('id') + '_');
                $('#fileupload').click();
                return false;
            });
            $('input[name^="cost"]').change(function () {
                var current = $('input[name="cost[current]"]');
                var discount = $('input[name="cost[discount]"]');
                var new_ = $('input[name="cost[new]"]');
                if ($(this).attr('name') == 'cost[current]' && discount.val()) {
                    var c = current.val() - ((current.val() / 100) * discount.val());
                    new_.val(c.toFixed(2));
                }
                if ($(this).attr('name') == 'cost[new]' && discount.val()) {
                    var c = (100 / discount.val()) * parseInt(new_.val());
                    current.val(c.toFixed(2));
                }
                if ($(this).attr('name') == 'cost[discount]' && current.val()) {
                    var c = current.val() - ((current.val() / 100) * discount.val());
                    new_.val(c.toFixed(2));
                }

            });
            $('#desire_y,#desire_n,#days').change(function () {
                cost();
            });
            cost();
            $('input[name="cost[current]"]').val(1000);
            $('input[name="cost[current]"]').change();


            /*$(".chosen-select").chosen({
             disable_search_threshold: 10,
             max_selected_options: 5,
             no_results_text: "Город не найден"
             });*/
            //
            $('.chosen-select').ajaxChosen({
                dataType: 'json',
                type: 'POST',
                url: '/util/city'
            }, {
                loadingImg: '/images/loading.gif',
                minLength: 1
            });
            $('.chosen-select').change(function () {
                if ($(this).find('option:selected').size() > 20) {
                    alert('Разрешено указывать до 20 городов');
                    $(this).find('option:selected:last').remove();
                    $('.chosen-select').trigger("chosen:updated");
                }
            });
            ///

            /*$('.checkbox_category').click(function () {
             if ($(this).hasClass('checked')) {
             $(this).parent().find('li').each(function (i) {
             if (i == 0 || $(this).hasClass('checked')) {
             return;
             }
             $(this).click();
             });
             } else {
             $(this).parent().find('li').each(function (i) {
             if (i == 0 || !$(this).hasClass('checked')) {
             return;
             }
             $(this).click();
             });
             }
             });*/
            $.each($('.clothing_checkboxes.styled_checkbox, .accessories_checkboxes.styled_checkbox'), function () {
                $(this).find('ul li:first').click(function () {
                    var checked = $(this).parent().find('.checked');
                    if (!$(this).hasClass('checked') && checked.size()) {
                        $.each(checked, function () {
                            $(this).click();
                        });
                    }
                    if ($(this).hasClass('checked') && checked.size() <= 1) {
                        $.each($(this).parent().find('li'), function (i) {
                            if (i == 0) {
                                return;
                            }
                            $(this).click();
                        });
                    }
                    console.log(checked.size());
                });
                ///

                $(this).find('ul li:gt(0)').click(function () {
                    var firstLi = $(this).parent().find('li:first');
                    if ($(this).hasClass('checked') && !firstLi.hasClass('checked')) {
                        firstLi.click();
                    }
                    var checked = $(this).parent().find('.checked');
                    if (checked.size() <= 1 && firstLi.hasClass('checked')) {
                        firstLi.click();
                    }
                });
                ////
            });
        });
        function callbackChoose(a, b, c) {
            var file = curImage.attr('img-src');
            var post = {
                'file': file,
                'coords': b
            };
            $.post('?img', post, function (data) {
                $('#' + a + '_').val(data);
            });
            curImage.next().html('Файл загружен');
            if (c !== undefined) {
                var post = {
                    'file': file,
                    'coords': c,
                    'mini': 0
                };
                $.post('?img', post, function (data) {
                    $('#' + a + '__').val(data);
                });
            }
            //console.log(a, b, c);
        }

        var tariff = <?php echo $price;?>;
        function cost() {
            var balance = parseFloat($('#actMyBalance').html());
            var day = parseInt($('#days').val());
            var desire = $('#desire_y').is(':checked') ? 1 : 0;
            var summ = day * tariff;
            if (desire) {
                summ = summ * 3;
            }

            $('#actCost').html(summ.toFixed(2));
        }
        <?php if($edit):
        echo 'var cats=['.implode(',', $action_edit['cat']).'];'
        ?>
        $.each(cats, function (e, v) {
            console.log(v);
            $('label[for=cat' + v + ']').click();
        });
        $('.photo_file').text('Файл выбран');
        <?php endif; ?>
    </script>
<?php
footer();

