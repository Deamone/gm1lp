<?php
$_USER = $USER;

if (isset($SYSTEM['get']['token'])) {
    $token = $SYSTEM['get']['token'];
    if (!file_exists('tmp/' . $token . '.txt')) {
        exit('Превью недоступно');
    }
    $data = unserialize(file_get_contents('tmp/' . $token . '.txt'));
} else {
    exit('Превью недоступно');
}
foreach ($data['site'] as $k => $site) {
    $data['site'][$k] = preg_replace('/http(.?)(\/{1,})/i', '', $data['site'][$k]);
}
header_('Предпросмотр акции', '', '', ''
    . '<link rel="stylesheet" type="text/css" href="/css/bjqs.css">'
    . '<link rel="stylesheet" type="text/css" href="/css/style_action.css"/>'
    . '<link rel="stylesheet" type="text/css" href="/css/style_external.css"/>'
    . '<script type="text/javascript" src="/js/bjqs-1.3.min.js"></script>'
    . '            <link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
            <link rel="stylesheet" href="/css/jquery.Jcrop.min.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.countdown.js"></script>', 'action_view');
?>
    <div id="main" class="main_block centred" xmlns="http://www.w3.org/1999/html">
        <div class="action_wrapper">
            <div class="a_left_block">
                <div class="a_slider">
                    <div id="action_slider">
                        <ul class="bjqs">
                            <?php if ($data['chooseImage1__']) {
                                echo '<li><a href="#"><img src="/' . $data['chooseImage1__'] . '" title="title"></a></li>';
                            }
                            if ($data['chooseImage3__']) {
                                echo '<li><a href="#"><img src="/' . $data['chooseImage2__'] . '" title="title"></a></li>';
                            }
                            if ($data['chooseImage2__']) {
                                echo '<li><a href="#"><img src="/' . $data['chooseImage3__'] . '" title="title"></a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?php if (isset($data['images'][4])) {
                    echo '<div class="attachments">';
                    foreach ($data['images'] as $k => $img) {
                        if (!$img || $img == '/') {
                            continue;
                        }
                        echo '<a class="attachment fancybox" rel="dop" href="/' . $img . '"><img src="/' . $img . '"></a>';
                    }

                    echo '</div>';
                }
                ?>
                <?php if (isset($data['yt']) && !empty($data['yt'])) {
                    #echo '<h2>Видео</h2>';
                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data['yt'], $match)) {
                        $video_id = $match[1];

                        echo '<div class="video"><iframe width="724" height="479" src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0" allowfullscreen></iframe></div>';
                    }
                }
                ?>
                <div class="a_description al_block last">
                    <h2><?php echo $data['title']; ?></h2>

                    <p id="condition">
                        <?php echo nl2br($data['condition']); ?>
                    </p>

                    <ul class="a_our_socials">
                        <?php
                        if ($data['site']['over']) {
                            echo '<li>Наш сайт: <a href="//' . $data['site']['over'] . '" target="_blank">' . $data['site']['over'] . '</a></li>';
                        }
                        if ($data['site']['main']) {
                            echo '<li class="iconSocs">' . soc_ico('www', $data['site']['main']) . '</li>';
                        }
                        #if ($data['site']['gm']) {
                        #    echo '<li>Мы в GM: <a href="//' . $data['site']['gm'] . '" target="_blank">' . $data['site']['gm'] . '</a></li>';
                        #}
                        ?>
                    </ul>
                </div>
            </div>

            <div class="a_right_block">
                <div class="obv">
                    <div class="a_time ar_block" id="a_time">
                        <span>До конца акции</span> <span class="a_time_days">4 дня</span> <span class="a_time_time">15:13:15</span>
                    </div>

                    <div class="a_discount_description ar_block">
                        <p>
                            <?php echo $data['title']; ?>
                        </p>

                        <div class="a_socials al_block">
                            <ul class="a_our_socials">
                                <?php
                                if ($data['site']['gm']) {
                                    echo '<li class="iconSocs">' . soc_ico('gm', $data['site']['gm']) . '</li>';
                                }
                                if ($data['site']['vk']) {
                                    echo '<li class="iconSocs">' . soc_ico('vk', $data['site']['vk']) . '</li>';
                                }
                                if ($data['site']['fb']) {
                                    echo '<li class="iconSocs">' . soc_ico('fb', $data['site']['fb']) . '</li>';
                                }
                                //
                                if ($data['site']['ok']) {
                                    echo '<li class="iconSocs">' . soc_ico('od', $data['site']['ok']) . '</li>';
                                }
                                if ($data['site']['tw']) {
                                    echo '<li class="iconSocs">' . soc_ico('tw', $data['site']['tw']) . '</li>';
                                }
                                if ($data['site']['ig']) {
                                    echo '<li class="iconSocs">' . soc_ico('ia', $data['site']['ig']) . '</li>';
                                }
                                ?>
                            </ul>
                            <br/>

                            <div class="a_like">
                                <button class="btn btn-color-orange btn-size-small btn-icon btn-icon-heart rounded_3">Мне нравится</button>
                                <span class="a_like_amount"><?php echo $data['like']; ?></span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="obv2">
                    <div class="but" style="display: none">
                        <button data-fancybox-type="iframe" href="/action/action?id=<?php echo $data['comp_id']; ?>&iframe" class="fancybox btn button-br5 member2">Участвовать в конкурсе</button>
                    </div>
<!--                    <div class="members">-->
<!--                        --><?php //$members = getMembers($data['comp_id']); ?>
<!--                        <a href="#">Участники: --><?php //echo count($members); ?><!--</a>-->
<!---->
<!--                        <div class="list">-->
<!--                            <input type="text" id="searchMember" placeholder="Поиск участника"/>-->
<!--                            <ul>-->
<!--                                --><?php
//                                $page = 1;
//                                $style = '';
//                                $li = array();
//                                $pages = '<b>Страницы</b> <a href="#" class="active">1</a>';
//                                foreach ($members as $i => $member) {
//                                    if ($member['user_id']) {
//                                        $user = getUserById($member['user_id']);
//                                        $name = $user['fam'] . '<br/>' . $user['name'];
//                                        $url = 'https://gm1lp.ru/' . $user['url'];
//                                    } else {
//                                        $user = getVkUser($member['vk_id']);
//                                        $name = $user['last_name'] . '<br/>' . $user['first_name'];
//                                        $url = 'https://vk.com/id' . $user['vk']['vk_data']['id'];
//                                    }
//                                    if ($member['winner']) {
//                                        $li[] = '';
//                                        echo '<li class="winner" data-page="1">
//                                    <a href="' . $url . '" target="_blank">
//                                        <div class="user">
//                                            <div class="img">
//                                                <img src="/images/avatar-main.jpg"/>
//                                                <span class="num">' . ($i + 1) . '</span>
//                                            </div>
//                                            <div class="name">' . $name . '</div>
//                                        </div>
//                                    </a>
//                                </li>';
//                                    } else {
//                                        $li[] = '<li data-page="' . $page . '" ' . $style . '>
//                                    <a href="' . $url . '" target="_blank">
//                                        <div class="user">
//                                            <div class="img">
//                                                <img src="/images/avatar-main.jpg"/>
//                                                <span class="num">' . ($i + 1) . '</span>
//                                            </div>
//                                            <div class="name">' . $name . '</div>
//                                        </div>
//                                    </a>
//                                </li>';
//                                    }
//                                    if ((count($li) % 16) == 0) {
//                                        $page++;
//                                        $pages .= '<a href="#">' . $page . '</a>';
//                                        $style = 'style="display:none;"';
//                                    }
//                                }
//                                echo implode('', $li);
//                                ?>
<!--                            </ul>-->
<!--                            <br/>-->
<!---->
<!--                            <div class="pages">-->
<!--                                --><?php //if ($page > 1) {
//                                    echo $pages;
//                                } ?>
<!--                            </div>-->
<!--                            <br/>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <!--h2>Другие акции</h2>

        <div id="smActions">
            <!--div class="action_small_wrapper action_double_wrapper action_text_wrapper">
                <p class="large">У вас на сайте или в магазине часто<br />проходят розыгрыши ценных подарков?</p>

                <p>Подключитесь к GM, у нас есть те,<br />кто с радостью примет в них участие!</p>

                <p>Благодаря полностью автоматизированному<br />сервису конкурсов, на это уйдет несколько минут</p>
            </div->
        </div>

        <div class="clear"></div>
        <div class="action_more_btn">
            <div class="action_more_btn_wrapper">
                <button>Показать еще</button>
            </div>
        </div-->
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('.a_like button').click(function () {
                $.post('?id=<?php echo $id; ?>', 'like=1', function (data) {
                    $('.a_like_amount').html(data);
                });
            });


            $('#counter').countdown({
                timestamp: '<?php echo ($end = time() + ($data['day'] * 3600 * 24)); ?>000',
                start: '<?php
        /*if (time() < mktime(0, 0, 0, 1, 25, 2015)) { STOP
            $time = $data['created'];
        }*/echo time();
        ?>000',
                callback: function (a, b, c, d, timer) {
                    var day = '';
                    if (a) {
                        day = ' <span class="a_time_days">' + a + ' ' + declOfNum(a, ['день', 'дня', 'дней']) + '</span> ';
                    }
                    $('#a_time').html('<span>До конца акции</span>' + day + '<span class="a_time_time">' + b + ':' + c + ':' + d + '</span>');
                    <?php
                    //if (time() < mktime(0, 0, 0, 1, 25, 2015)) { STOP
                        //echo 'clearInterval(timer);';
                    //}
                    ?>
                }
            });
        });
    </script>
    <script>
        $(".action_small_wrapper img").load(function () {
            var imageHeight = $(this).height();
            console.log(imageHeight);
            if (imageHeight < 208) {
                $(this).css({'height': '100%'});
            }
            //$(this).parent().append(imageHeight);
        });
        var loader = false;
        function loadMoreAction() {
            if (!loader) {
                loader = true;
                var data = {
                    'count': $('#smActions .action_small_wrapper').size(),
                    'lines': $('#smActions .action_small_wrapper[data-lines]:last').attr('data-lines'),
                    'all': $('#smActions .action_small_wrapper[data-lines]:last').attr('data-all')
                };
                $.get('/', data, function (data) {
                    //$(data).html();
                    $('#smActions .action_small_wrapper[data-lines]:last').after($(data).clone());
                    $('.action_small_wrapper').hover(function () {
                        $(this).children('.a_detailed').stop().slideDown('fast');
                        $(this).children('.a_description').hide();
                    }, function () {
                        $(this).children('.a_detailed').stop().slideUp('fast');
                        $(this).children('.a_description').show();
                    });
                    runCountDown();
                    loader = false;
                    if (!data) {
                        $('.action_more_btn_wrapper').parent().hide();
                    }
                });
            }
        }
        $(document).ready(function () {
            $('.action_more_btn_wrapper').click(function () {
                loadMoreAction();
                return false;
            });
            $.post('?id=<?php echo $id; ?>&sc', '', function (data) {
                $('#smActions').html(data);
            });

            clickable($('#condition'));
        });
    </script>
<?php
footer();
