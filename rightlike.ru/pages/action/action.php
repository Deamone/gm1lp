<?php

$_USER = $USER;
if (!isset($SYSTEM['get']['id'])) {
    redirect();
}
$id = $SYSTEM['get']['id'];
$data = getComp($id);
if (!$data) {
    redirect();
}
$data['site'] = unserialize($data['site']);
$data['act'] = $data['act'] ? unserialize($data['act']) : array();

foreach ($data['site'] as $k => $site) {
    $data['site'][$k] = preg_replace('/http(.?)(\/{1,})/i', '', $data['site'][$k]);
    $data['site'][$k] = preg_replace('/http(.?)(\/{1,})www./i', '', $data['site'][$k]);
}
$ars = array('', 'Вступить в группу/сообщество', 'Подписаться на страницу', 'Поделиться записью (сделать репост)');
$res = array();
for ($i = 1; $i <= 3; $i++) {
    if (!isset($data['act'][$i]) || !$data['act'][$i][0]) {
        $data['act'][$i] = array(0, '');
        continue;
    } else {
        $res[] = (count($res) + 1) . '. ' . $ars[$i] . ': <a href="' . $data['act'][$i][1] . '" target="_blank">' . $data['act'][$i][1] . '</a><br/>';
    }
}

$data['images'] = unserialize($data['images']);
if (isset($_GET['iframe'])) {
    $soc = '';

    $array_ = array($data['tcondition'], 'http://rightlike.ru/action/action?id=' . $data['comp_id'] . '');
    if ($data['site']['vk']) {
        $soc .= '<li class="iconSocs">' . soc_ico('vk', $data['site']['vk'], 1, $array_) . '</li>';
    }
    if ($data['site']['fb']) {
        $soc .= '<li class="iconSocs">' . soc_ico('fb', $data['site']['fb'], 1, $array_) . '</li>';
    }
    //
    if ($data['site']['ok']) {
        $soc .= '<li class="iconSocs">' . soc_ico('od', $data['site']['ok'], 1, $array_) . '</li>';
    }
    if ($data['site']['tw']) {
        $soc .= '<li class="iconSocs">' . soc_ico('tw', $data['site']['tw'], 1, $array_) . '</li>';
    }    
	
	if ($data['site']['googleplus']) {
        $soc .= '<li class="iconSocs">' . soc_ico('googleplus', $data['site']['googleplus'], 1, $array_) . '</li>';
    }
    echo '<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/style_external.css">
    <link rel="stylesheet" type="text/css" href="/css/overwrite.css">
    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/js/post.js"></script>
    <script>var data = $.parseJSON(\'' . json_encode(array('comp_id' => $data['comp_id'], 'text' => $data['tcondition'])) . '\');</script>
</head>
<body style="padding: 30px">
<script src="http://vk.com/js/api/openapi.js" type="text/javascript"></script>
<script type="text/javascript">
    VK.init({
        apiId: 4396061
    });
</script>';


    if (!$_GET['iframe']) {

        $auth = '<ul class="a_our_socials fShare">' . $soc . '</ul>';
        if (!$USER['id']) {
            $auth = '<br/><br/><a href="//gm1lp.ru/page/login?referer=rightlike.ru">Пожалуйста войдите на наш сайт, что бы увидеть сслыки</a><br/><br/>';
        }
        echo '<h1 style="color: #373737;font-family: monospace;">Для участия в конкурсе Вам необходимо сделать два простых шага:</h1>

    <div class="member2block">
		1. Разместить запись на странице 
		<span class="cVk">ВКонтакте</span>, 
		<span class="cVk">Facebook</span>,
		<span class="cTW">Twitter</span>, 
		<span class="cOk">Одноклассники</span> или 
		<span class="cGl">Google+</span>, нажав соответствующую кнопку ниже (необязательное условие):
		<br/>
		' . $auth . '
		<br/>
		<a data-fancybox-type="iframe" href="/action/action?id=50&amp;iframe=2">у меня не получается участвовать в конкурсе</a>
	</div>    
	<br/>
	<div class="member2block">';
		$i = 1;
	
		if($data['act'][1][0]) {
			echo ++$i.'. Вступить в группу: <a href="'.$data['act'][1][1].'" target="_blank">Вступить</a><br/>';
		}
	
		if($data['act'][2][0]) {
			echo ++$i.'. Подписаться на страницу: <a href="'.$data['act'][2][1].'" target="_blank">Подписаться</a><br/>';
		}
		if($data['act'][3][0]) {
			echo ++$i.'. Сделать репост записи: <a href="'.$data['act'][3][1].'" target="_blank">сделать репост</a><br/>';
		}
		if($data['dcondition']){
			echo '<br/>'.nl2br($data['dcondition']);
		}
		
		echo '</div>';
    } else {
		
        echo '<a data-fancybox-type="iframe" href="/action/action?id=' . $data['comp_id'] . '&iframe" class="fancybox back"><img src="/images/back.jpg"> назад </a>';
        echo '<h1 style="font-size: 1.5em;">У меня не получаутся участвовать в конкурсе. Как быть?</h1>
    <div class="member2block">
1. Возможно Вам нужно разрешить всплывающее окно. Или установит новую версию
браузера Google Chrome<br/><div style="text-align: center"><img src="/images/help1.jpg" ></div></div>
<div class="member2block mt30">
2. Возможно Вам необходимо зарегистрироваться или авторизоваться в соц.сети.<br/><div style="text-align: center"><img src="/images/help2.jpg" ></div>
</div>';
    }
    ?>
    <script type="text/javascript">
        <!--
        /*document.write(VK.Share.button({
         url: 'http://mysite.com',
         title: 'Good site',
         description: 'This is my own site, I spent a lot of time creating it',
         image: 'http://mysite.com/mypic.jpg';
         ,
         noparse: true
         }))
         ;*/
        -->
    </script>
    <?
    echo '</body>
</html>';
    exit;
}
if (isAjax()) {
    if (isset($_GET['member'])) {
        addMember($data['comp_id'], 0);
        exit;
    }
    if (isset($_POST['like'])) {
        $data['like']++;
        upLikeComp($data['comp_id']);
        $data = getComp($id);
        echo $data['like'];
    }
    if (isset($_GET['sc'])) {
        $actions = getComps(false, 16);
        shuffle($actions);
        $stop = 0;

        $j = 0;
        $all = 0;
        $lines = 0;
        foreach ($actions as $k => $action) {
            $images = unserialize($action['images']);
            $cost = unserialize($action['cost']);
            if (time() < mktime(0, 0, 0, 7, 1, 2015)) {
                # STOP
                $time = $action['created'];
                $stop = ' data-stop="1"';
            } else {
                $time = time();
                $stop = ' data-stop="0"';
            }


            $image_ = $images[11];

            $j++;
            $all++;

            echo '<div class="action_small_wrapper ' . $css . '" data-lines="' . $lines . '" data-all="' . $all . '">
            <div class="left_act">
                <div class="a_image">
                    <a href="/action/action?id=' . $action['comp_id'] . '"><img src="/' . $image_ . '" /></a>
                </div>
            </div>
            <div class="right_act">
                <div class="a_description">
                    <a href="/action/action?id=' . $action['comp_id'] . '">' . $action['title'] . '</a>
                    <div class="desc">' . nl2br($action['condition']) . '</div>
                </div>

                <div class="a_detailed bottom">
                    <div class="a_detailed_toolbox actionTime" ' . $stop . ' data-ctime="' . $time . '" data-etime="' . $action['end'] . '">
                        <div class="a_time">4 дня 15:13:15</div>
                        <div class="a_like">' . $action['like'] . '</div>
                        <div class="clear"></div>
                    </div>
                    <button class="btn button-br5 member2">Принять участие</button>
                    <button class="btn button2-br5 move">Подробнее</button>
                </div>

                </div>
        </div>';
        }
    }
    exit;
}

#$data['cost'] = unserialize($data['cost']);
$time = $data['created'];
header_($data['title'], '', '', ''
    . '<link rel="stylesheet" type="text/css" href="/css/bjqs.css">'
    . '<link rel="stylesheet" type="text/css" href="/css/style_action.css"/>'
    . '<link rel="stylesheet" type="text/css" href="/css/style_external.css"/>'
    . '<script type="text/javascript" src="/js/bjqs-1.3.min.js"></script>'
    . '
            <link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
            <link rel="stylesheet" href="/css/jquery.Jcrop.min.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.countdown.js"></script>', 'action_view');
?>
    <!--main-->
    <div id="main" class="main_block centred" xmlns="http://www.w3.org/1999/html">
        <div class="action_wrapper">
            <div class="a_left_block">
                <div class="a_slider">
                    <div id="action_slider">
                        <ul class="bjqs">
                            <?php if ($data['images'][1]) {
                                echo '<li><a href="#"><img src="/' . $data['images'][1] . '" title="title"></a></li>';
                            }
                            if ($data['images'][2]) {
                                echo '<li><a href="#"><img src="/' . $data['images'][2] . '" title="title"></a></li>';
                            }
                            if ($data['images'][3]) {
                                echo '<li><a href="#"><img src="/' . $data['images'][3] . '" title="title"></a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?php if (isset($data['images'][4])) {
                    echo '<div class="attachments">';
                    foreach ($data['images'] as $k => $img) {
                        if ($k < 4 || $k > 10) {
                            continue;
                        }
                        echo '<a class="attachment fancybox" rel="dop" href="/' . $img . '"><img src="/' . $img . '"></a>';
                    }

                    echo '</div>';
                }
                ?>
                <?php if (isset($data['images'][12]) && !empty($data['images'][12])) {
                    #echo '<h2>Видео</h2>';
                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data['images'][12], $match)) {
                        $video_id = $match[1];

                        echo '<div class="video"><iframe width="724" height="479" src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0" allowfullscreen></iframe></div>';
                    }
                }
                ?>
                <div class="a_description al_block last">
                    <h2><?php echo $data['title']; ?></h2>

                    <p id="condition">
                        <?php echo nl2br($data['condition']); ?>
                    </p>

                    <ul class="a_our_socials">
                        <?php
                        if ($data['site']['over']) {
                            echo '<li>Наш сайт: <a href="//' . $data['site']['over'] . '" target="_blank">' . $data['site']['over'] . '</a></li>';
                        }
                        if ($data['site']['main']) {
                            echo '<li class="iconSocs">' . soc_ico('www', $data['site']['main']) . '</li>';
                        }
                        if ($data['site']['gm']) {
                            echo '<li class="iconSocs">' . soc_ico('gm', $data['site']['gm']) . '</li>';
                        }
                        if ($data['site']['vk']) {
                            echo '<li class="iconSocs">' . soc_ico('vk', $data['site']['vk']) . '</li>';
                        }
                        if ($data['site']['fb']) {
                            echo '<li class="iconSocs">' . soc_ico('fb', $data['site']['fb']) . '</li>';
                        }
                        //
                        if ($data['site']['ok']) {
                            echo '<li class="iconSocs">' . soc_ico('od', $data['site']['ok']) . '</li>';
                        }
                        if ($data['site']['tw']) {
                            echo '<li class="iconSocs">' . soc_ico('tw', $data['site']['tw']) . '</li>';
                        }
                        if ($data['site']['ig']) {
                            echo '<li class="iconSocs">' . soc_ico('ia', $data['site']['ig']) . '</li>';
                        }
                        if ($data['site']['yt']) {
                            echo '<li class="iconSocs">' . soc_ico('yo', $data['site']['yt']) . '</li>';
                        }
                        #if ($data['site']['gm']) {
                        #    echo '<li>Мы в GM: <a href="//' . $data['site']['gm'] . '" target="_blank">' . $data['site']['gm'] . '</a></li>';
                        #}
                        ?>
                    </ul>
                </div>
            </div>

            <div class="a_right_block">
                <div class="obv">
                    <div class="a_time ar_block" id="a_time">
                        <span>До конца акции</span> <span class="a_time_days">4 дня</span> <span class="a_time_time">15:13:15</span>
                    </div>

                    <div class="a_discount_description ar_block">
                        <p>
                            <?php echo $data['title']; ?>
                        </p>

                        <div class="a_socials al_block">
                            <ul class="a_our_socials">
                                <?php
                                if ($data['site']['gm']) {
                                    echo '<li class="iconSocs">' . soc_ico('gm', $data['site']['gm']) . '</li>';
                                }
                                if ($data['site']['vk']) {
                                    echo '<li class="iconSocs">' . soc_ico('vk', $data['site']['vk']) . '</li>';
                                }
                                if ($data['site']['fb']) {
                                    echo '<li class="iconSocs">' . soc_ico('fb', $data['site']['fb']) . '</li>';
                                }
                                //
                                if ($data['site']['ok']) {
                                    echo '<li class="iconSocs">' . soc_ico('od', $data['site']['ok']) . '</li>';
                                }
                                if ($data['site']['tw']) {
                                    echo '<li class="iconSocs">' . soc_ico('tw', $data['site']['tw']) . '</li>';
                                }
                                if ($data['site']['ig']) {
                                    echo '<li class="iconSocs">' . soc_ico('ia', $data['site']['ig']) . '</li>';
                                }
								if ($data['site']['main']) {
									echo '<li class="iconSocs">' . soc_ico('www', $data['site']['main']) . '</li>';
								}
                                ?>
                            </ul>
                            <br/>

                            <div class="a_like">
                                <button class="btn btn-color-orange btn-size-small btn-icon btn-icon-heart rounded_3">Мне нравится</button>
                                <span class="a_like_amount"><?php echo $data['like']; ?></span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="obv2">
                    <div class="but">
                        <button id="various5" href="/action/action?id=<?php echo $data['comp_id']; ?>&iframe" class="btn button-br5 member2">Участвовать в конкурсе</button>
                    </div>
					<!-- 
                    <div class="members">
                        <?php $members = getMembers($data['comp_id']); ?>
                        <a href="#">Участники: <?php echo count($members); ?></a>
                        <div class="list">
                            <input type="text" id="searchMember" placeholder="Поиск участника"/>
                            <ul>
                                <?php
                                $page = 1;
                                $style = '';
                                $li = array();
                                $pages = '<b>Страницы</b> <a href="#" class="active">1</a>';
                                foreach ($members as $i => $member) {
                                    if ($member['user_id']) {
                                        $user = getUserById($member['user_id']);
                                        $name = $user['fam'] . '<br/>' . $user['name'];
                                        $url = 'https://gm1lp.ru/' . $user['url'];
                                    } else {
                                        $user = getVkUser($member['vk_id']);
                                        $name = $user['last_name'] . '<br/>' . $user['first_name'];
                                        $url = 'https://vk.com/id' . $user['vk']['vk_data']['id'];
                                    }
                                    if ($member['winner']) {
                                        $li[] = '';
                                        echo '<li class="winner" data-page="1">
                                    <a href="' . $url . '" target="_blank">
                                        <div class="user">
                                            <div class="img">
                                                <img src="/images/avatar-main.jpg"/>
                                                <span class="num">' . ($i + 1) . '</span>
                                            </div>
                                            <div class="name">' . $name . '</div>
                                        </div>
                                    </a>
                                </li>';
                                    } else {
                                        $li[] = '<li data-page="' . $page . '" ' . $style . '>
                                    <a href="' . $url . '" target="_blank">
                                        <div class="user">
                                            <div class="img">
                                                <img src="/images/avatar-main.jpg"/>
                                                <span class="num">' . ($i + 1) . '</span>
                                            </div>
                                            <div class="name">' . $name . '</div>
                                        </div>
                                    </a>
                                </li>';
                                    }
                                    if ((count($li) % 16) == 0) {
                                        $page++;
                                        $pages .= '<a href="#">' . $page . '</a>';
                                        $style = 'style="display:none;"';
                                    }
                                }
                                echo implode('', $li);
                                ?>
                            </ul>
					 
                            <br/>

                            <div class="pages">
                                <?php if ($page > 1) {
                                    echo $pages;
                                } ?>
                            </div>
                            <br/>
                        </div>
                    </div>
					-->
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <h2>Другие конкурсы</h2>

        <div id="smActions">
            <!--div class="action_small_wrapper action_double_wrapper action_text_wrapper">
                <p class="large">У вас на сайте или в магазине часто<br />проходят розыгрыши ценных подарков?</p>

                <p>Подключитесь к GM, у нас есть те,<br />кто с радостью примет в них участие!</p>

                <p>Благодаря полностью автоматизированному<br />сервису конкурсов, на это уйдет несколько минут</p>
            </div-->
        </div>

        <div class="clear"></div>
        <div class="action_more_btn">
            <div class="action_more_btn_wrapper">
                <button>Показать еще</button>
            </div>
        </div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('.a_like button').click(function () {
                $.post('?id=<?php echo $id; ?>', 'like=1', function (data) {
                    $('.a_like_amount').html(data);
                });
            });


            $('#counter').countdown({
                timestamp: '<?php echo $data['end']; ?>000',
                start: '<?php
        if (time() < mktime(0, 0, 0, 7, 1, 2015)) { //STOP
            $time = $data['created'];
        }else{$time = time();}
        echo $time;
        ?>000',
                callback: function (a, b, c, d, timer) {
                    var day = '';
                    if (a) {
                        day = ' <span class="a_time_days">' + a + ' ' + declOfNum(a, ['день', 'дня', 'дней']) + '</span> ';
                    }
                    $('#a_time').html('<span>До конца акции</span>' + day + '<span class="a_time_time">' + b + ':' + c + ':' + d + '</span>');
                    <?php
                    if (time() < mktime(0, 0, 0, 7, 1, 2015)) {# STOP
                        echo 'clearInterval(timer);';
                    }
                    ?>
                }
            });
        });
    </script>
    <script>
		$("#various5").fancybox({
			'width'				: '45%',
			'height'			: '50%',
			'autoScale'     	: false,
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'type'				: 'iframe'
		});
	
	
        $(".action_small_wrapper img").load(function () {
            var imageHeight = $(this).height();
            console.log(imageHeight);
            if (imageHeight < 208) {
                $(this).css({'height': '100%'});
            }
            //$(this).parent().append(imageHeight);
        });
        var loader = false;
        function loadMoreAction() {
            if (!loader) {
                loader = true;
                var data = {
                    'count': $('#smActions .action_small_wrapper').size(),
                    'lines': $('#smActions .action_small_wrapper[data-lines]:last').attr('data-lines'),
                    'all': $('#smActions .action_small_wrapper[data-lines]:last').attr('data-all')
                };
                $.get('/', data, function (data) {
                    //$(data).html();
                    $('#smActions .action_small_wrapper[data-lines]:last').after($(data).clone());
                    $('.action_small_wrapper').hover(function () {
                        $(this).children('.a_detailed').stop().slideDown('fast');
                        $(this).children('.a_description').hide();
                    }, function () {
                        $(this).children('.a_detailed').stop().slideUp('fast');
                        $(this).children('.a_description').show();
                    });
                    runCountDown();
                    loader = false;
                    if (!data) {
                        $('.action_more_btn_wrapper').parent().hide();
                    }
                });
            }
        }
        $(document).ready(function () {
            $('.action_more_btn_wrapper').click(function () {
                loadMoreAction();
                return false;
            });
            $.post('?id=<?php echo $id; ?>&sc', '', function (data) {
                $('#smActions').html(data);
                runCountDown();
            });
            var h = $('.attachments').height() + $('.video').height() + $('.a_description.al_block.last').height() + 100;
            $('.members').css({'min-height': h + 'px'});
            clickable($('#condition'));
        });
    </script>
<?php
footer();
