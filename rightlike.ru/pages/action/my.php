﻿<?php
$_USER = $USER;
if (isset($_GET['iframe'])) {
    $id = $SYSTEM['get']['id'];
    $data = getComp($id);
    if (!$data || $data['user_id'] != $_USER['id']) {
        redirect();
    }
    $data['site'] = unserialize($data['site']);
    foreach ($data['site'] as $k => $site) {
        $data['site'][$k] = preg_replace('/http(.?)(\/{1,})/i', '', $data['site'][$k]);
    }
    $data['images'] = unserialize($data['images']);
    $soc = '';

    if ($data['site']['vk']) {
        $soc .= '<li class="iconSocs">' . soc_ico('vk', $data['site']['vk'], 1) . '</li>';
    }

    if (isset($_GET['member'])) {
        winner($id, $_GET['member']);
        exit;
    }
    /*if ($data['site']['fb']) {
        $soc .= '<li class="iconSocs">' . soc_ico('fb', $data['site']['fb'], 1) . '</li>';
    }
    //
    if ($data['site']['ok']) {
        $soc .= '<li class="iconSocs">' . soc_ico('ok', $data['site']['ok'], 1) . '</li>';
    }
    if ($data['site']['tw']) {
        $soc .= '<li class="iconSocs">' . soc_ico('tw', $data['site']['tw'], 1) . '</li>';
    }*/
    echo '<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/style_external.css">
<link rel="stylesheet" href="/icons/pe-icon-social/css/pe-icon-social.css">
    <link rel="stylesheet" href="/icons/pe-icon-social/css/helper.css">
    <link rel="stylesheet" href="/icons/pe-icon-social/css/social-style.css">
    <link rel="stylesheet" type="text/css" href="/css/overwrite.css">
    <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/js/nx.js"></script>
    <script>var data = $.parseJSON(\'' . json_encode(array('comp_id' => $data['comp_id'], 'text' => $data['tcondition'])) . '\');</script>
</head>
<body style="padding: 0 10px">';
    ?>
    <div class="members">
        <?php $members = getMembers($data['comp_id']); ?>
        <a href="#">Участники: <?php echo count($members); ?></a>

        <div class="list">
            <input type="text" id="searchMember" placeholder="Поиск участника"/>
            <table width="100%" class="sltWinner_">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Пользователь</th>
                    <th>VK</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $page = 1;
                $style = '';
                foreach ($members as $i => $member) {
                    if ($member['user_id']) {
                        $user = getUserById($member['user_id']);
                        $name = $user['fam'] . ' ' . $user['name'];
                        $url = 'https://gm1lp.ru/' . $user['url'];
                    } else {
                        $user = getVkUser($member['vk_id']);
                        $name = $user['last_name'] . ' ' . $user['first_name'];
                        $url = 'https://vk.com/id' . $user['vk']['vk_data']['id'];
                    }
                    $a = '<a href="#" class="winner" data-comp="' . $member['comp_id'] . '" data-id="' . $member['member_id'] . '">Выбрать победителем</a>';
                    $class = '';
                    if ($member['winner']) {
                        $class = 'winner';
                        $a = '';
                    }
                    echo '<tr class="' . $class . '">
<td>' . ($i + 1) . '</td>
<td><a href="' . $url . '" target="_blank" class="name">' . $name . '</a></td>
<td><a href="' . $url . '" target="_blank" class="name">' . $name . '</a></td>
<td>' . $a . '</td>
</tr>';
                    /*echo '<li data-page="' . $page . '"" ' . $style . '>
                                    <a href="' . $url . '" target="_blank">
                                        <div class="user">
                                            <div class="img">
                                                <img src="/images/avatar-main.jpg"/>
                                                <span class="num">' . ($i + 1) . '</span>
                                            </div>
                                            <div class="name">' . $name . '</div>
                                        </div>
                                    </a>
                                </li>';
                    if ((($i + 1) % 16) == 0) {
                        $page++;
                        $pages .= '<a href="#">' . $page . '</a>';
                        $style = 'style="display:none;"';
                    }*/
                } ?>
                </tbody>
            </table>
            <br/>
        </div>
    </div>
    <?php
    echo '</body>
</html>';
    exit;
}
header_('Мои конкурсы', '', '', '
            <link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
            <link rel="stylesheet" href="/css/fix.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/jquery.fancybox.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.countdown.js"></script>', 'external action_list');

$actions = getCompByUid($USER['id']);

$act = array();
$unact = array();
$time = time();
foreach ($actions as $action) {
    if ($action['end'] > $time) {
        $act[] = $action;
    } else {
        $unact[] = $action;
    }
}
?>
<style>
.action_edit_wrapper {
    width: 100%;
    background-color: #FFF;
    height: 310px;
    padding-left: 0;
    padding-top: 0;
}

.a_image {
    float: left;
    width: 290px;
}

.a_description {
    display: inline-block !important;
    float: left;
    width: auto !important;
    padding-left: 10px !important;
}

.action_small_wrapper {
    width: 100% !important;
    float: none;
    position: absolute;
}

.a_price {
    display: none;
}

.a_discount.a_discount_orange {
    display: none;
}

.ae_toolbox {
    bottom: 20px;
    position: absolute;
    width: 100%;
}

.ae_date {
    margin-left: 300px;
}
.a_detailed.bottom {
    width: 100%;
    bottom: 0;
}

.a_detailed_toolbox.actionTime.countdownHolder {
    padding: 15px 30px;
}
.left_act {
    width: 40%;
}

.right_act {
    width: 60%;
    padding-left: 0;
}
.action_small_wrapper {
    z-index: 9;
}

.actions {
    position: absolute;
    z-index: 2;
    right: -25px;
}

.action_edit_wrapper:hover .actions {
    display: block;
}
</style>
    <!--main-->
    <div id="main" class="main_block centred">
        <div class="left_block">
            <h1>Мои конкурсы</h1>
            <?php
            if (!$act && !$unact) {
                echo '<h3 class="blue">У вас нет конкурсов</h3>';
            }
            if ($act) {
                echo '<h3 class="blue">Активные конкурсы</h3>';
            }
            foreach ($act as $k => $action) {
                $images = unserialize($action['images']);
                $cost = unserialize($action['cost']);
                /* if (time() < mktime(0, 0, 0, 1, 25, 2015)) { STOP
                  $time = $action['created'];
                  $stop = ' data-stop="1"';
                  } */
                $time = time();


                $image_ = $images[11];

                $j++;
                $all++;

                echo '<div class="action_edit_wrapper ' . $css . '" data-lines="' . $lines . '" data-all="' . $all . '">
		<div class="action_small_wrapper ' . $css . '" data-lines="' . $lines . '" data-all="' . $all . '">
			<div class="left_act">
				<div class="a_image">
					<a href="/action/action?id=' . $action['comp_id'] . '"><img src="/' . $image_ . '" /></a>
				</div>
			</div>
			<div class="right_act">
				<div class="a_description">
					<a href="/action/action?id=' . $action['comp_id'] . '">' . $action['title'] . '</a>
					<div class="desc">' . nl2br($action['condition']) . '</div>
				</div>
			</div>
			<div class="a_detailed bottom">
				<div class="a_detailed_toolbox actionTime" ' . $stop . ' data-ctime="' . $time . '" data-etime="' . $action['end'] . '">
					<div class="a_time">4 дня 15:13:15</div>
					<div class="a_like">' . $action['like'] . '</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="actions">
			<div class="ae_edit">
				<a href="/action/create?edit=' . $action['comp_id'] . '">
					<img src="/images/edit.png">
				</a>
			</div>
			<div class="ae_remove">
				<a id="various1" href="#delete' . $action['comp_id'] . '">
					<img src="/images/remove.png">
				</a>
			</div>
			<div class="ae_remove">
				<a  id="various1" href="#pickup' . $action['comp_id'] . '">
					<img src="/images/up.png">
				</a>
			</div>	
		</div>
		<!--
        <div class="dop_act">
            <div class="action_result">
                <div class="amount_label white">Участников <span class="amount_label">596</span></div>
            </div>
            <div class="action_result">
                <a data-fancybox-type="iframe" href="/action/my?id=' . $action['comp_id'] . '&iframe=1" class="fancybox sltWinner">Выбрать победителя</a>
            </div>
        </div>
		-->
        </div>
		<div id="pickup' . $action['comp_id'] . '" style="display:none">
			<div class="modal" style="background-image: url(\'/images/modal.png\');height: 222px;width: 500px;">
				<div class="title">Поднять</div>
				<div class="text">
				Вы действительно хотите поднять свой конкурс на первое место?
				<br/>
				<br/>
					<a href="/action/create?up=' . $action['comp_id'] . '" class="btn_new ">
						Потверждаю
					</a>						
					<a href="javascript:$.fancybox.close();" class="btn_new silver">
						Отмена
					</a>
				</div>
			</div>
		</div>	
		
		<div id="delete' . $action['comp_id'] . '" style="display:none">
			<div class="modal" style="background-image: url(\'/images/modal.png\');height: 222px;width: 500px;">
				<div class="title">Удаление</div>
				<div class="text">
				Вы действительно хотите удалить свой конкурс безвозвратно?
				<br/>
				<br/>
					<a href="/action/create?delete=' . $action['comp_id'] . '" class="btn_new ">
						Потверждаю
					</a>						
					<a href="javascript:$.fancybox.close();" class="btn_new silver">
						Отмена
					</a>
				</div>
			</div>
		</div>

		';
            }
            ?>
            <div class="clear"></div>


            <?php
            if ($unact) {
                echo '<h3 class="orange">Завершенные</h3>';
            }
            foreach ($unact as $action) {
                $images = unserialize($action['images']);
                $cost = unserialize($action['cost']);
                echo '<div class="action_edit_wrapper">
            <div class="action_small_wrapper">
                <div class="a_image">
                    <a href="/action/action?id=' . $action['comp_id'] . '"><img src="/' . $images[11] . '" /></a>
                </div>
                <div class="a_description">' . nl2br(mb_substr($action['title'], 0, 155)) . '...</div>
                <div class="a_price"><span class="a_price_old">' . $cost['current'] . '</span> <span class="a_price_new">' . $cost['new'] . ' руб.</span></div>

                <div class="a_discount a_discount_orange">
                    <div class="a_discount_label">скидка</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">' . $cost['discount'] . '</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>
			<div class="actions">
				<div class="ae_edit">
					<a href="/action/create?edit=' . $action['comp_id'] . '">
						<img src="/images/edit.png">
					</a>
				</div>
				<div class="ae_remove">
					<a id="various1" href="#delete' . $action['comp_id'] . '">
						<img src="/images/remove.png">
					</a>
				</div>
				<div class="ae_remove">
					<a  id="various1" href="#pickup' . $action['comp_id'] . '">
						<img src="/images/up.png">
					</a>
				</div>	
			</div>
            <div class="clear"></div>

            <div class="ae_toolbox">
                <div class="ae_date">' . date('d.m.Y H:i:s') . '</div>
                <div class="ae_like">' . $action['like'] . '</div>
            </div>
        </div>
			
			
		<div id="pickup' . $action['comp_id'] . '" style="display:none">
			<div class="modal" style="background-image: url(\'/images/modal.png\');height: 222px;width: 500px;">
				<div class="title">Поднять</div>
				<div class="text">
				Вы действительно хотите поднять свой конкурс на первое место?
				<br/>
				<br/>
					<a href="/action/create?up=' . $action['comp_id'] . '" class="btn_new ">
						Потверждаю
					</a>						
					<a href="javascript:$.fancybox.close();" class="btn_new silver">
						Отмена
					</a>
				</div>
			</div>
		</div>	
		
		<div id="delete' . $action['comp_id'] . '" style="display:none">
			<div class="modal" style="background-image: url(\'/images/modal.png\');height: 222px;width: 500px;">
				<div class="title">Удаление</div>
				<div class="text">
				Вы действительно хотите удалить свой конкурс безвозвратно?
				<br/>
				<br/>
					<a href="/action/create?delete=' . $action['comp_id'] . '" class="btn_new ">
						Потверждаю
					</a>						
					<a href="javascript:$.fancybox.close();" class="btn_new silver">
						Отмена
					</a>
				</div>
			</div>
		</div>
		';
            }
            ?>
        </div>

        <div class="right_block">
            <ul class="right_menu">
                <li><a class="rounded_3" href="/user/lk">Личный кабинет</a></li>
                <li><a class="rounded_3" href="/action/create">Запустить конкурс</a></li>
                <li class="active"><a class="rounded_3" href="/action/my">Мои конкурсы</a></li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
			$("#various1").fancybox({
				'width'				: '500px',
				'height'			: '222px',
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'closeBtn'          : true,
				
				'tpl': {
					'wrap'     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
					'image'    : '<img class="fancybox-image" src="{href}" alt="" />',
					'iframe'   : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>',
					'error'    : '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
					'closeBtn' : '<a title="Close" class="closes fancybox-item fancybox-close" href="javascript:;"></a>',
					'next'     : '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
					'prev'     : '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>',
					'loading'  : '<div id="fancybox-loading"><div></div></div>'
				}
			});			
			
            $('.action_small_wrapper').hover(function () {
                $('.a_description').show();
                return false;
            }, function () {
                return false;
            });
        });
    </script>
<?php
footer();
