<?php
$_USER = $USER;
$q = urldecode($_GET['qSearch']);
if (isset($_GET['qCategories'])) {
    $qCat = explode(',', urldecode($_GET['qCategories']));
    #var_dump($qCat);
    if (isset($qCat[0]) && $qCat[0] == 0) {
        $qCat = array(0);
    }
} else {
    $qCat = array();
}

header_('Поиск конкурсов', '', '', ''
    . '<link rel="stylesheet" type="text/css" href="/css/bjqs.css">'
    . '<link rel="stylesheet" type="text/css" href="/css/style_action.css"/>'
    . '<link rel="stylesheet" type="text/css" href="/css/style_external.css"/>'
    . '<script type="text/javascript" src="/js/bjqs-1.3.min.js"></script>'
    . '<script type="text/javascript" src="/js/jquery.countdown.js"></script>', 'action_view');

$actions = searchComps($qCat, $q, 27);
?>
    <!--main-->
    <div id="main" class="main_block centred">
        <?php
        if (!$actions) {
            echo '<h2 style="margin-top: 0;">Сожелеем, но конкурсы удовлетворяющие запросу не найдены.</h2>';
        } elseif (!$qCat) {
            echo '<h2 style="margin-top: 0;">Вы не указали категорию. Посмотрите все конкурсы вашего города.</h2>';
        } else {
            echo '<h2 style="margin-top: 0;">Список конкурсов по запросу &quot;' . $q . '&quot;</h2>';
        }
        ?>
        <div>
            <?php
            $j = 0;
            $all = 0;
            $lines = 0;
            foreach ($actions as $k => $action) {
                $images = unserialize($action['images']);
                $cost = unserialize($action['cost']);
                /* if (time() < mktime(0, 0, 0, 1, 25, 2015)) { STOP
                  $time = $action['created'];
                  $stop = ' data-stop="1"';
                  } */
                $time = time();


                $image_ = $images[11];

                $j++;
                $all++;

                echo '<div class="action_small_wrapper ' . $css . '" data-lines="' . $lines . '" data-all="' . $all . '">
            <div class="left_act">
                <div class="a_image">
                    <a href="/action/action?id=' . $action['comp_id'] . '"><img src="/' . $image_ . '" /></a>
                </div>
            </div>
            <div class="right_act">
                <div class="a_description">
                    <a href="/action/action?id=' . $action['comp_id'] . '">' . $action['title'] . '</a>
                    <div class="desc">' . nl2br($action['condition']) . '</div>
                </div>

                <div class="a_detailed bottom">
                    <div class="a_detailed_toolbox actionTime" ' . $stop . ' data-ctime="' . $time . '" data-etime="' . $action['end'] . '">
                        <div class="a_time">4 дня 15:13:15</div>
                        <div class="a_like">' . $action['like'] . '</div>
                        <div class="clear"></div>
                    </div>
                    <button class="btn button-br5 member2">Принять участие</button>
                    <button class="btn button2-br5 move">Подробнее</button>
                </div>

                </div>
        </div>';
            }
            ?>




            <!--div class="action_small_wrapper action_double_wrapper action_text_wrapper">
                <p class="large">У вас на сайте или в магазине часто<br />проходят розыгрыши ценных подарков?</p>

                <p>Подключитесь к GM, у нас есть те,<br />кто с радостью примет в них участие!</p>

                <p>Благодаря полностью автоматизированному<br />сервису конкурсов, на это уйдет несколько минут</p>
            </div-->
        </div>

        <div class="clear"></div>

        <!--div class="action_more_btn">
            <div class="action_more_btn_wrapper">
                <button>Показать еще</button>
            </div>
        </div-->
    </div>
    <!--main-->
    <script>
        $(document).ready(function () {
            $('.a_like button').click(function () {
                $.post('?id=<?php echo $id; ?>', 'like=1', function (data) {
                    $('.a_like_amount').html(data);
                });
            });


            $('#counter').countdown({
                timestamp: '<?php echo $data['end']; ?>000',
                start: '<?php
        if (time() < mktime(0, 0, 0, 1, 25, 2015)) {
            $time = $data['created'];
        }echo $time;
        ?>000',
                callback: function (a, b, c, d, timer) {
                    var day = '';
                    if (a) {
                        day = ' <span class="a_time_days">' + a + ' ' + declOfNum(a, ['день', 'дня', 'дней']) + '</span> ';
                    }
                    $('#a_time').html('<span>До конца конкурса</span>' + day + '<span class="a_time_time">' + b + ':' + c + ':' + d + '</span>');
                    <?php
                    if (time() < mktime(0, 0, 0, 1, 25, 2015)) {
                        echo 'clearInterval(timer);';
                    }
                    ?>
                }
            });
        });
    </script>
    <script>
        $(".action_small_wrapper img").load(function () {
            var imageHeight = $(this).height();
            console.log(imageHeight);
            if (imageHeight < 208) {
                $(this).css({'height': '100%'});
            }
            //$(this).parent().append(imageHeight);
        });
    </script>
<?php
footer();
