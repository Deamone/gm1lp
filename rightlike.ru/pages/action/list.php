<?php
$_USER = $USER;

header_('gmcard.ru', '', '', ''
        . '<script type="text/javascript" src="js/bjqs-1.3.min.js"></script>');
?>
<!--main-->
<div id="main" class="main_block centred">
    <div class="action_wrapper">
        <div class="a_left_block">
            <div class="a_slider">
                <div id="action_slider">
                    <ul class="bjqs">
                        <li><a href="#"><img src="/images/slider/slider-image1.jpg" title="title"></a></li>
                        <li><a href="#"><img src="/images/slider/slider-image2.jpg" title="title"></a></li>
                        <li><a href="#"><img src="/images/slider/slider-image3.jpg" title="title"></a></li>
                    </ul>
                </div>
                <div class="a_discount">
                    <div class="a_discount_label">скидка</div>
                    <div class="a_discount_bottom"><span class="a_discount_amount">50</span> <span class="a_discount_per">%</span></div>
                </div>
            </div>	

            <div class="a_socials al_block">
                <div class="a_socials_icons">
                    <span class="external_socials_label">Поделиться</span>
                    <ul class="external_socials">
                        <li class="social_vk"><a href="#"><span class="social_count">100</span></a></li>
                        <li class="social_fb"><a href="#"><span class="social_count">1</span></a></li>
                        <li class="social_twitter"><a href="#"><span class="social_count">32</span></a></li>
                        <li class="social_odnoklassniki"><a href="#"></a></li>
                        <li class="social_mail"><a href="#"><span class="social_count">2</span></a></li>
                    </ul>
                </div>

                <div class="a_like">
                    <button class="btn btn-color-orange btn-size-small btn-icon btn-icon-heart rounded_3">Мне нравится</button>
                    <span class="a_like_amount">1258</span>
                </div>
                <div class="clear"></div>
            </div>	

            <div class="a_description al_block last">
                <p>
                    Слуховой аппарат Super Mini Amplifier - поможет Вам слышать намного лучше и позволит постоянно наслаждаться окружающими звуками,он настолько мал, 
                    что почти незаметен. При этом силен настолько, что позволит Вам заново услышать шепот и тихие разговоры, ведь он усиливает звук до 50 
                    Децибел (dB).Super Mini Amplifier прост в использовании, просто вставьте его в ухо и Вы будете слышать.Особенности:- качественно обрабатывает 
                    звук- компенсирует практически любую потерю слуха- подходит для людей разного возраста- компактный, незаметный.
                </p>

                <ul class="a_our_socials">
                    <li>Наш сайт: <a href="#">www.gm1lp.ru</a></li>
                    <li>Мы в GM: <a href="#">www.gm1lp.ru/gm</a></li>
                    <li>Мы ВКонтакте: <a href="#">www.vk.com/gm1lp</a></li>
                    <li>Мы на Facebook: <a href="#">www.facebook.com/landRoverLif</a></li>
                </ul>
            </div>
        </div>

        <div class="a_right_block">
            <div class="a_time ar_block">
                <span>До конца акции</span> <span class="a_time_days">4 дня</span> <span class="a_time_time">15:13:15</span>
            </div>

            <div class="a_discount_description ar_block">
                <p>
                    Скидка 50% на брендовые зимние аксессуары: шапочки UGG с натуральным мехом кролика и вязаные наборы шапка + шарфик!
                </p>
                <div class="a_price"><span class="a_price_old">1000</span> <span class="a_price_new">500 руб.</span></div>
                <button class="btn rounded_3">Заказать карту GM</button>
            </div>

            <div class="a_contacts ar_block last">
                <h2>Контакты</h2>

                <h3>График работы:</h3>
                <span class="work_day">пн-сб:</span>  с 9:00   до  20:00<br />
                <span class="work_day">вс:</span> с 10:00  до 18:00

                <h3>Телефоны:</h3>
                8-800-2000-600-700

                <h3>Адрес:</h3>
                г.Москва, пр. Мира, 82

                <h3>Сайт:</h3>
                <a href="#">www.example.com</a>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <h2>Другие акции</h2>
    <div>
        <div class="action_small_wrapper first">
            <div class="a_image">
                <a href="#"><img src="/images/actions/action1.jpg" /></a>
            </div>
            <div class="a_description">Обувь известных спортивных брендов</div>
            <div class="a_price"><span class="a_price_old">300</span> <span class="a_price_new">150 руб.</span></div>

            <div class="a_discount a_discount_orange">
                <div class="a_discount_label">скидка</div>
                <div class="a_discount_bottom"><span class="a_discount_amount">50</span> <span class="a_discount_per">%</span></div>
            </div>
        </div>

        <div class="action_small_wrapper">
            <div class="a_image">
                <a href="#"><img src="/images/actions/action2.jpg" /></a>
            </div>
            <div class="a_description">Парикмахерские услуги в студии красоты Юлии Рудых</div>
            <div class="a_price"><span class="a_price_old">1500</span> <span class="a_price_new">1950 руб.</span></div>

            <div class="a_discount a_discount_lgreen">
                <div class="a_discount_label">скидка</div>
                <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
            </div>
        </div>

        <div class="action_small_wrapper">
            <div class="a_image">
                <a href="#"><img src="/images/actions/action3.jpg" /></a>
            </div>
            <div class="a_description">Сушилки для обуви Dr. Dry и осушители Sorbis для защиты салон...</div>
            <div class="a_price"><span class="a_price_old">300</span> <span class="a_price_new">150 руб.</span></div>

            <div class="a_discount a_discount_dgreen">
                <div class="a_discount_label">скидка</div>
                <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
            </div>
        </div>

        <div class="action_small_wrapper">
            <div class="a_image">
                <a href="#"><img src="/images/actions/action4.jpg" /></a>
            </div>
            <div class="a_description">Целый день посещения аквапарка «Фэнтази Парк»</div>
            <div class="a_price"><span class="a_price_old">900</span> <span class="a_price_new">1700 руб.</span></div>

            <div class="a_discount a_discount_ping">
                <div class="a_discount_label">скидка</div>
                <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
            </div>
        </div>

        <div class="action_small_wrapper first">
            <div class="a_image">
                <a href="#"><img src="/images/actions/action5.jpg" /></a>
            </div>
            <div class="a_description">7 дней проживания с питанием в санатории «Солнечный»...</div>
            <div class="a_price"><span class="a_price_old">6000</span> <span class="a_price_new">3000 руб.</span></div>

            <div class="a_discount a_discount_dorange">
                <div class="a_discount_label">скидка</div>
                <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
            </div>
        </div>

        <div class="action_small_wrapper">
            <div class="a_image">
                <a href="#"><img src="/images/actions/action6.jpg" /></a>
            </div>
            <div class="a_description">Полный курс обучения вождению автомобиля в автошколе при МГТУ...</div>
            <div class="a_price"><span class="a_price_old">900</span> <span class="a_price_new">1700 руб.</span></div>

            <div class="a_discount a_discount_borange">
                <div class="a_discount_label">скидка</div>
                <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
            </div>
        </div>

        <div class="action_small_wrapper action_small_info">
            <p class="large">У вас на сайте или в магазине часто<br />проходят розыгрыши ценных подарков?</p>

            <p>Подключитесь к GM, у нас есть те,<br />кто с радостью примет в них участие!</p>

            <p>Благодаря полностью автоматизированному<br />сервису конкурсов, на это уйдет несколько минут</p>
        </div>

        <div class="action_small_wrapper first">
            <div class="a_image">
                <a href="#"><img src="/images/actions/action5.jpg" height="100" /></a>
            </div>
            <div class="a_description">7 дней проживания с питанием в санатории «Солнечный»...</div>
            <div class="a_price"><span class="a_price_old">6000</span> <span class="a_price_new">3000 руб.</span></div>

            <div class="a_discount a_discount_dorange">
                <div class="a_discount_label">скидка</div>
                <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
            </div>
        </div>

        <div class="action_small_wrapper">
            <div class="a_image">
                <a href="#"><img src="/images/actions/action5.jpg" height="100" width="285" /></a>
            </div>
            <div class="a_description">7 дней проживания с питанием в санатории «Солнечный»...</div>
            <div class="a_price"><span class="a_price_old">6000</span> <span class="a_price_new">3000 руб.</span></div>

            <div class="a_discount a_discount_dorange">
                <div class="a_discount_label">скидка</div>
                <div class="a_discount_bottom"><span class="a_discount_amount">10</span> <span class="a_discount_per">%</span></div>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="action_more_btn">
        <div class="action_more_btn_wrapper">
            <button>Показать еще</button>
        </div>
    </div>
</div>
<!--main-->
<?php
footer();
